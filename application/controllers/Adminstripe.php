<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//use OpenTok\OpenTok;

class Adminstripe extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		

	}
	
	public function ecc(){
		echo md5('new@new.com');
	}
	
	
/*=======================================
		webhook
========================================*/
	public function webhook(){
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		
		// Retrieve the request's body and parse it as JSON
		$input = @file_get_contents("php://input");
		$event_json = json_decode($input);
		
		// Do something with $event_json
		
		//var_dump($event_json);

		if($this->master_model->insertRecord('stripe_webhook',array('content'=>serialize($event_json)))){
			
			if($event_json->type == 'customer.subscription.updated'){
				//get weebhook data
				$new_subs_object = $event_json->data->object;
				$subscription_id = $new_subs_object->id;
				$customer_id = $new_subs_object->customer;
				$plan_name = $new_subs_object->plan->name;
				$previous_attributes = $event_json->data->previous_attributes;

				//get period end date
				$current_period_start = $new_subs_object->current_period_start;
				$date = new DateTime("@$current_period_start");
				$current_period_start = $date->format('Y-m-d H:i:s');
				
				$current_period_end = $new_subs_object->current_period_end;
				$date = new DateTime("@$current_period_end");
				$current_period_end = $date->format('Y-m-d H:i:s');
				
				//get numbers of users in plan name
				$user_count = preg_replace("/[^0-9]/","",$plan_name);
				
				//get user information
				$cc_info = $this->master_model->getRecords('cf_login_master', array('stripe_customer_id'=>$customer_id));
				
				if(count($cc_info) > 0){
					//update sub end date
					$this->master_model->updateRecord('cf_login_master', array('current_sub_end_date'=>$current_period_end), array('login_id'=>$cc_info[0]['login_id']));

					//update number of recall users in org
					$this->master_model->updateRecord('organization_master', array('recall_max'=>$user_count), array('organization_id'=>$cc_info[0]['org_id']));
					
				}
				
				
				//create array of subscription data
				$subscription_data = $this->common_model->subscription_data($new_subs_object);
				
				//store to subscription db
				$sub_arr = array(
					'customer_id'=>$customer_id,
					'subscription_id'=>$subscription_id,
					'content'=>serialize($new_subs_object),
					'previous_attributes'=>serialize($previous_attributes)
				);
				$this->master_model->insertRecord('stripe_subscriptions', $sub_arr);
			
			}
			
			
			echo 'stored';
		}
		else{
			echo 'not stored';
		}
		
		http_response_code(200); // PHP 5.4 or greater	
	}
	
	//test retrieve stripe_webhook
	public function stripe_webhook(){
		$hook_id = $this->uri->segment(3);
		$ee = $this->master_model->getRecords('stripe_webhook', array('id'=>$hook_id));
		$content = unserialize($ee[0]['content']);

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');

		echo json_encode($content, JSON_PRETTY_PRINT);
	}
	
	
	
/*=======================================
		manage
========================================*/
	public function manage() {

		$this->load->model('admin_model');
		$stripe_cust = $this->admin_model->stripe_get_cust();
	//	$stripe_plans = $this->all_plans();
		
		if($this->session->userdata('stripe_plans') != ''){
			$stripe_plans = $this->session->userdata('stripe_plans');
		}
		else{
			$stripe_plans = $this->common_model->all_plans();
			$this->session->set_userdata('stripe_plans', $stripe_plans);
		}
		
		
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		$events = Stripe_Event::all();									

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		$cust_cc = $this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.cust_id !='=>'0','cc.subscription !='=>'0'));

		$data=array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-stripe-customer',
			'cust_cc'=>$cust_cc,
			'stripe_cust'=>$stripe_cust,
			'stripe_plans'=>$stripe_plans,
			'events'=>$events
		);

		$this->load->view('admin/admin-view',$data);

		
		
 	}
	
	
/*=======================================
		stripe_checkout
========================================*/
	public function add_pre_plan(){
		
		if(isset($_POST['save_preplan'])){
			$this->session->set_flashdata('success',' test.');
			$cust_date_id = $_POST['cust_date_id'];
			$plan_id = explode('__',$_POST['plan_id']);
			
			Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
			$the_data = Stripe_Plan::retrieve($plan_id[0]);	
					
			
			
			$arr = array(
				'pre_selected_plan_id'=>$plan_id[0],
				'pre_selected_plan'=>$plan_id[1],
				'pre_selected_plan_data'=>serialize($the_data),
				
			);
			$this->master_model->updateRecord('stripe_customers', $arr, array('created'=>$cust_date_id));
			
			$this->session->set_flashdata('success',' Pre-selected plan for selected customer successfully added.');

			redirect(base_url().'webmanager/stripe/manage');
		}
		else{
			$this->session->set_flashdata('error',' No input.');
			redirect(base_url().'webmanager/stripe/manage');
		}
	}
	
/*=======================================
		stripe_checkout
========================================*/
	public function stripe_checkout(){
		$this->load->view('testcheckout');
	}
	
	
/*=======================================
		addcustomer
========================================*/
	public function addcustomer(){
		Stripe::setApiKey('sk_test_zC4nyIzsM6pXKLXhsrJZHiB0');
		
		$cust = Stripe_Customer::create(array(
		  "description" => "Customer for test@example.com",
		  "email" => "test@exampe.com" // obtained with Stripe.js
		));	
		
		echo $cust;
	}
	
/*=======================================
		all_plans
========================================*/
	public function all_plans(){
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		$all_plans = Stripe_Plan::all();
		return $all_plans;
	}
	

	public function getplan(){
		$stripe_cust = $this->master_model->getRecords('stripe_customers',array('id'=>'26'));
		$cust_data =  unserialize($stripe_cust[0]['pre_selected_plan_data']);// unserialize($stripe_cust[0]['cust_data']);
		echo $cust_data->id;
	}

/*=======================================
		subscribe_user
========================================*/
	public function subscribe_user(){
		
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		
		// Get the credit card details submitted by the form
		$token_id = $_POST['token_id'];
		$token_email = $_POST['token_email'];
		$subs_plan_id = $_POST['subs_plan_id'];
		
		$cust_id = $_POST['cust_id'];
		$stripe_cust = $this->master_model->getRecords('stripe_customers',array('id'=>$cust_id));
		
		$result = 'error';
		if(count($stripe_cust) > 0){
			$cu = Stripe_Customer::retrieve($stripe_cust[0]['stripe_id']);
			$cu->sources->create(array("source" => $token_id));
			$cu->subscriptions->create(array("plan" => $subs_plan_id));	
			//$cu->save();
			$result = 'success';
		}
		else{
			
			$customer = Stripe_Customer::create(array(
			  "card" => $token_id,
			  "plan" => $subs_plan_id,
			  "email" => $token_email)
			);	
			
			$result = 'success';
	
		}
		
			
		echo $result;
		
	}


/*=======================================
		request
========================================*/
	public function request()

	{

		$cust = $this->uri->segment(3);

		$cust_info = $this->master_model->getRecords('stripe_customers',array('created'=>$cust));
		
		$whr = array( 'id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

		

		$info_arr = array(
			'from'=>$adminemail[0]['recovery_email'],
			'to'=>$cust_info[0]['email'],
			'subject'=>'CrisisFlo Stripe Payment Subscription',
			'view'=>'customer-stripe-form'
		);

		$other_info = array(
			'first_name'=>$cust_info[0]['email'], //$cust_info[0]['name'],
			'cust_id'=>$cust_info[0]['id'],
			'email'=> md5($cust_info[0]['email'])
		);


		$this->email_sending->sendmail($info_arr,$other_info);

		$this->master_model->updateRecord('stripe_customers',array('granted'=>'1'),array('created'=>$cust));
		
		$this->session->set_flashdata('success',' Stripe subscription form successfully sent.');

		redirect(base_url().'webmanager/stripe/manage');

	}	
	

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apiv2 extends CI_Controller {


	public function __construct(){

		parent::__construct();
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
	}
	
	public function logged_info(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('my_id');

		$user_login_for_session = $this->common_model->user_login_for_session($cc_id);
		$format_crt = $this->common_model->format_crt($cc_id);
		$combined = array_merge($user_login_for_session, $format_crt);
		
		echo json_encode($combined);
	}

	public function verify_user(){
		$key = $_GET['key'];


		$cc_info = $this->master_model->getRecords('cf_login_master',array('onetime_key'=>$key));

		$arr = array(
			'count'=>count($cc_info),
			'user'=>(count($cc_info) > 0) ? $this->common_model->format_crt($cc_info[0]['login_id']) : array()
		);

		echo json_encode($arr);
		
	}

	public function check_verify_user(){
		$data = $_POST;
		$cc_login_id = $data['login_id'];
		$nowtime = date("Y-m-d h:i:s");

		$cc_arr = array(
			'date_profile_activated'=>$nowtime,
			'pass_word'=>$data['password2'],
			'user_status'=>'1'
		);
		$succ_mess ='You have successfully activated your new account.';

		$data['result'] = 'error';
		$data['message'] = 'Something went wrong.';

		//check if demo user
		$cc = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_login_id));
			
		if($this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$cc_login_id))) {
			$countryformat = $this->common_model->countryformat(array('country_id'=>$data['country_id']));
			$data['countrycode'] = $countryformat[0]['calling_code'];
			$mob = $data['countrycode'].$data['crt_digits'];

			$arr = array(
				'crt_first_name'=>$this->master_model->encryptIt($data['first_name']),
				'crt_last_name'=>$this->master_model->encryptIt($data['last_name']),
				'crt_position'=>$data['crt_position'],
				'location'=>$data['location'],
				// 'location_lat'=>$location_lat,
				// 'location_lng'=>$location_lng,
				'countrycode'=>$this->master_model->encryptIt($data['countrycode']),
				'crt_digits'=>$this->master_model->encryptIt($data['crt_digits']),
				'crt_mobile'=>$this->master_model->encryptIt($mob),
				'crisis_function'=>$data['crisis_function'],
			);
			$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$cc_login_id));
		
		
			/**script to send email after confirmation*/
			$whr = array('id'=>'1');
			$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

				
			$info_arr = array(
				'from'=>$adminemail[0]['recovery_email'],
				'to'=>$data['crt_email'],
				'subject'=>'CrisisFlo account successfully activated',
				'view'=>'registration-mail-to-cc-success'
			);
			
			$other_info = array(
				'first_name'=>$this->master_model->decryptIt($data['first_name']),
				'last_name'=>''//$this->master_model->decryptIt($cc_lastname)
			);
			
			$this->email_sending->sendmail($info_arr, $other_info);

			$user_login_for_session = $this->common_model->user_login_for_session($cc_login_id);
			
			$this->session->set_userdata($user_login_for_session);

			$format_crt = $this->common_model->format_crt($cc_login_id);
			$combined = array_merge($user_login_for_session, $format_crt);

			$data['user_info'] = $combined;
			$data['result'] = 'ok';
			$data['message'] = $succ_mess;
			
		}

		echo json_encode($data);
		
	}

	public function login(){

		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '37';
		$action_message = $this->common_model->get_message($action_message_id);

		$error = $action_message['error'];
		
		$email = $_POST['email'];
		$password = $_POST['password'];
		$linkedin_id = (isset($_POST['linkedin_id'])) ? $_POST['linkedin_id'] : '';
		$gplus_id = (isset($_POST['gplus_id'])) ? $_POST['gplus_id'] : '';
		
		$chk_arr = array(
			'email_id'=>$this->master_model->encryptIt($email),
			'pass_word'=>md5($password)
		);


		$data['result'] = 'error';
		$data['message'] = $action_message['error'];
		
		if($linkedin_id != ''){
			$chk_arr = array(
				'login.linkedin_id'=>$linkedin_id
			);
		}
		else if($gplus_id != ''){
			$chk_arr = array(
				'login.gplus_id'=>$gplus_id
			);
		}
	

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');
		
		$chk_arr['login.date_deleted'] = '0000-00-00 00:00:00';					
		$row = $this->master_model->getRecords('cf_login_master as login',$chk_arr);


		if(count($row) > 0){

			$name_of_org = $this->master_model->getRecords('organization_master',array('organization_id'=>$row[0]['default_org']),'*');
			
			if(count($name_of_org)){
				//check if org disabled
				if($name_of_org[0]['enabled'] == 'N'){
					$error = 'Organization disabled.';
					$data['message'] = $error;
					$data['result'] = 'no_user';
				}
				else{
					$user_login_for_session = $this->common_model->user_login_for_session($row[0]['login_id']);
					
					$this->session->set_userdata($user_login_for_session);

					$format_crt = $this->common_model->format_crt($row[0]['login_id']);
					$combined = array_merge($user_login_for_session, $format_crt);

					$data['user_info'] = $combined;
					$data['result'] = 'ok';
					$data['message'] = $action_message['success'];

					$nowtime = $this->common_model->userdatetime();
					
					$arr = array(
						'org_id'=>$row[0]['default_org'],
						'cc_id'=>$name_of_org[0]['cc_id'], //name_of_org[0]['cc_id'],
						'crt_id'=>$row[0]['login_id'],
						'module_id'=>'5', //recall
						'type'=>'0', //cc_login
						'date_added'=>$nowtime
					);

					//insert arr to audit_log table
					$this->master_model->insertRecord('audit_log', $arr);
					
				}//end if org not enabled
					
			}
				
		}
		
		echo json_encode($data);
		
	}


	public function trial_customer(){

		$message = $this->master_model->getRecords('validation_messages', array('name'=>'trial_request'));

		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$organization = $_POST['organization'];
		
		$address = (isset($_POST['address'])) ? $_POST['address'] : '';
		$lat = (isset($_POST['lat'])) ? $_POST['lat'] : '';
		$lng = (isset($_POST['lng'])) ? $_POST['lng'] : '';
		$country_code = (isset($_POST['country_code'])) ? $_POST['country_code'] : '';
		$calling_code = (isset($_POST['calling_code'])) ? $_POST['calling_code'] : '';
		$mobile = (isset($_POST['mobile'])) ? $_POST['mobile'] : '';
		$mobile = ltrim($mobile, '0');
		$complete_mobile = $calling_code.$mobile;
		$country_short = (isset($_POST['country_short'])) ? $_POST['country_short']  : '';
		$plan = (isset($_POST['plan'])) ? $_POST['plan'] : '5';
		$domain = (isset($_POST['domain'])) ? $_POST['domain'] : '';

		$recall_max = 1000; //$plan;


		//google plus
		$gplus_id = '';
		if(isset($_POST['gplus_id'])){
			$gplus_id = $_POST['gplus_id'];
		}

		//linkedin_id
		$linkedin_id = '';
		if(isset($_POST['linkedin_id'])){
			$linkedin_id = $_POST['linkedin_id'];
		}

		//default demo data
		$stripe_cust_id = '0';
		$purchaser_id = '';
		$user_type = 'demo';
		$org_status = 'Y'; //'N';

		//data form array
		$form_data = array(); //$_POST['form_data'];
		$form_data[] = array(
			'name'=>'mobile_number',
			'value'=>$complete_mobile
		);

		//data for paid user
		if(isset($_POST['stripe_cust_id'])){
			$stripe_cust_id = $_POST['stripe_cust_id'];
			$user_type = 'live';
			$org_status = 'Y';
		}

		if(isset($_POST['purchaser_id'])){
			$purchaser_id = $_POST['purchaser_id'];
		}

		$onetime_key = md5(microtime());

		$cc = $this->master_model->getRecords('cf_login_master', array('user_level'=>'0'));

		//check cc email duplicate
		$data['result'] = 'ok';

		if(count($cc) > 0){
			foreach($cc as $r=>$value){
				$cc_email = $this->master_model->decryptIt($value['email_id']);
				if($cc_email == $email){
					$data['result'] = 'duplicate';
				}
			}
		}

		if($data['result'] == 'ok'){
			$admin_info = $this->master_model->getRecords('admin_login', array('user_name'=>'admin'));
			$login_arr = array(
				'email_id'=>$this->master_model->encryptIt($email),
				'user_type'=>$user_type,
				'user_level'=>'0',
				'onetime_key'=>$onetime_key,
				'user_status'=>'0',
				'gplus_id'=>$gplus_id,
				'linkedin_id'=>$linkedin_id,
				'stripe_customer_id'=>$stripe_cust_id,
				'single_user'=>($recall_max == 1000) ? 'y' : 'n',
				'single_price'=>($recall_max == 1000) ? $admin_info[0]['single_rate'] : ''
			);

			$data['login_arr'] = $login_arr;

			if($login_id = $this->master_model->insertRecord('cf_login_master',$login_arr,TRUE)){

				$mods = $this->common_model->preselected_modules();

				$active_module = $mods['active_module']; //'m5m-m3m-m6m-m7m-m9m-m10m-m12m';
				$active_mod = $mods['active_mod']; //serialize($modules);

				//insert data to organization_master
				$org_arr = array(
					'organization_name'=>$organization,
					'domain'=>$organization,
					'active_module'=>$active_module, //$themods,
					'active_mod'=>$active_mod,
					'cust_type'=>'1', //not_aig $cust_type,
					'cc_id'=>$login_id,
					'enabled'=>$org_status,
					'recall_max'=>$recall_max,
					'date_added'=>date('Y-m-d H:i:s'),
					// 'domain'=>$domain
				);

				$data['org_arr'] = $org_arr;


				$org_id = $this->master_model->insertRecord('organization_master',$org_arr,TRUE);

				//add default org for cc
				$this->master_model->updateRecord('cf_login_master',array('default_org'=>$org_id),array('login_id'=>$login_id));

				$cc_arr = array(
					'login_id'=>$login_id,
					'crt_first_name'=>$this->master_model->encryptIt($first_name),
					'crt_last_name'=>$this->master_model->encryptIt($last_name),
					'crt_organisation'=>$organization,
					'crt_email'=>$this->master_model->encryptIt($email),
					'location'=>$address,
					'location_lat'=>$lat,
					'location_lng'=>$lng,
					'cc_id'=>$login_id,
					'country_id'=>$country_code,
					'countrycode'=>$this->master_model->encryptIt($calling_code),
					'crt_digits'=>$this->master_model->encryptIt($mobile),
					'crt_mobile'=>$this->master_model->encryptIt($complete_mobile),
					'org_id'=>0
				);

				$data['cc_arr'] = $cc_arr;

				//create text file for organization chat room
				$room_name = 'chat_room/org';
				$room_name .= $org_id;
				$room_name .= '.txt';
				$myfile = fopen($room_name, "w");

				if($this->master_model->insertRecord('cf_crisis_response_team', $cc_arr)){


					//email to admin for demo signup
					// if($stripe_cust_id == '0'){

						//send email to admin
						$admin_email = $this->master_model->getRecords('email_id_master');

						$info_arr = array(
							'to'=>'admin@crisisflo.com', //snailbob01@gmail.com',//$email,
							'from'=>$admin_email[0]['info_email'],
							'subject'=>'A CrisisFlo Visitor signed up for a trial account.',
							'view'=>'mail-to-admin-for-trial-customer'
						);

						$other_info = array(
							'form_data'=>$form_data,
							'org_id'=>$org_id,
							'email'=>$email
						);

						$this->email_sending->sendmail($info_arr,$other_info);
						$data['info_arr'] = $info_arr;
						$data['other_info'] = $other_info;

						//send customer the link for activation
						$data['activate_customer_demo'] = $this->common_model->activate_customer_demo($login_id);

					// }
					// else{
						//update purchaser data
						// $chaser_data = array(
						// 	'cc_id'=>$login_id,
						// 	'org_id'=>$org_id,
						// 	'status'=>'Y'
						// );

						// $this->master_model->updateRecord('organization_signups', $chaser_data, array('id'=>$purchaser_id));

						//send verification email to cc
						$admin_email = $this->master_model->getRecords('email_id_master');

						$info_arr = array(
							'to'=>$email,
							'from'=>$admin_email[0]['info_email'],
							'subject'=>'Welcome to CrisisFlo',
							'view'=>'registration-mail-to-cc'
						);

						$other_info = array(
							'first_name'=>$first_name,
							'last_name'=>$last_name,
							'onetime_key'=>$onetime_key,
							'email'=>$email
						);

						$this->email_sending->sendmail($info_arr,$other_info);
						$data['info_arr'] = $info_arr;
						$data['other_info'] = $other_info;

					// }//paid user

				}//end if cf_crisis_response_team created

				$data['message'] = "We've just sent you an email with details on how to complete your registration, please check your inbox."; //$message[0]['success_message'];

			}//end if login master created




		}//no duplicate

		echo json_encode($data);
	}


	public function action_messages(){
		$action_messages = $this->common_model->get_message();
		echo json_encode($action_messages);
	}

	public function header_tasks(){
		$incident_id = (isset($_GET['incident_id'])) ? $_GET['incident_id']: '';
		$template_id = (isset($_GET['template_id'])) ? $_GET['template_id']: '18';

		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id'] : $this->session->userdata('cc_selected_orgnaization');

		$the_crts = $this->common_model->get_org_crts($cc_id,$org_id);
	
		//fetch incident date
		$incident = $this->master_model->getRecords('cf_recall', array('id'=>$incident_id));
				

		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$pack_id = (!empty($template_id)) ? $template_id : $this->common_model->pack_id();
		

		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id, 'deleted'=>'0'),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		$arr = array();

		if(count($packsteps)>0) {
			$i = 0;
			foreach($packsteps as $r => $value) {
				$step_no = $value['step_no'];

				$countqq = 0;
				$curr_sub_c = '';
				
				//get tasks
				$tasks = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id, 'date_deleted'=>'0000-00-00 00:00:00'),''); //,array('category'=>'ASC','arrangement'=>'ASC'));
				
				$value['tasks'] = array();
				// $value['show'] = true;
			

				$todo = array();
				$in_progress = array();
				$donetask = array();
				
				if(!empty($tasks)){
					foreach($tasks as $tr=>$tvalue){
						//get answer of task
						$whr_ans = array(
							'incident_id'=>$incident_id,
							'task_id'=>$tvalue['id']
						);

						if(!empty($incident)){
							$tvalue['incident_date'] = $incident[0]['initiation_date'];
						}

						$task_answer = $this->common_model->task_answer_bool($whr_ans);
						$tvalue['checked'] = $task_answer;
						
						$task_answer_progress = $this->common_model->task_answer_progress($whr_ans);
						$tvalue['in_progress'] = $task_answer_progress['answer'];
						$tvalue['task_answer'] = $task_answer_progress['task_answer'];

						$tvalue['format_task_id'] = $this->common_model->format_task_id($tvalue['id']);
						
						$contents = $this->master_model->getRecords('checklist_contents',array('checklist_id'=>$tvalue['id']),'',array('sort'=>'ASC'));

						$arr_content = array();

						//get assigned
						$tvalue['assigned'] = $this->common_model->format_crt($tvalue['assigned_id']);
						$tvalue['assigned_info'] = $this->common_model->format_crt($tvalue['assigned_id']);


						// $nowtime = date("m/d/y");
						$tvalue['due_date_format'] = $tvalue['due_date']; 
						$tvalue['due_date'] = ($tvalue['due_date']) ? date_format(date_create($tvalue['due_date']), 'm/d/y') : '';

						$tvalue['due_time'] = ($tvalue['due_time']) ? $tvalue['due_time'] : '7:00pm';
						

						if(!empty($contents)){
							foreach($contents as $cr=>$cvalue){
								$whr = array(
									'content_id'=>$cvalue['id']
								);

								$email = $this->master_model->getRecords('content_emails', $whr);

								$cvalue['email'] = (!empty($email)) ? $email[0] : null;


								$cvalue['field'] = $this->master_model->getRecords('content_fields', $whr);

								$cvalue['options'] = $this->master_model->getRecords('content_options', $whr);

								$arr_content[] = $cvalue;
							}
						}

						//comments
						$comments = $this->master_model->getRecords('recall_task_comments', array('recall_id'=>$incident_id, 'task_id'=>$tvalue['id']));

						$carr = array();
						if(!empty($comments)){
							foreach($comments as $cr=>$cvalue){
								$cvalue['created_at'] = date_format(date_create($cvalue['created_at']), 'M d, Y');
								$cvalue['author'] = $this->common_model->format_crt($cvalue['user_id']);
								$carr[] = $cvalue;
		
							}
						}
		
						$tvalue['comments'] = $carr;
						$tvalue['incident_id'] = $incident_id;
						

						
						$tvalue['contents'] = $arr_content;
						$value['tasks'][] = $tvalue;
						$value['incident_id'] = $incident_id;
						

					}
				}


				$arr[] = $value;

				$i++;
			}
		}

		echo json_encode($arr);
	}

	//save by known name
	public function save_task_headers(){ //cf_recall_packs_steps
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$recall_pack_id = $_POST['recall_pack_id'];
		$step_no = $_POST['step_no'];
		$order = $_POST['order'];
		$name = $_POST['name'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'order'=>$order,
			'name'=>$name
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_packs_steps', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['recall_pack_id'] = $recall_pack_id;
			$arr['step_no'] = $step_no;
			$arr['date'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_packs_steps', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}


	public function save_task_answer(){
		$data = $_POST;
		$nowtime = $this->common_model->userdatetime();
		
		$data['created_at'] = $nowtime;
		$this->master_model->insertRecord('recall_task_answers', $data);

		echo json_encode($data);
		
	}

	public function update_incident_status(){
		$status = $_POST['status'];
		$incident_id = $_POST['incident_id'];
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			'incident_status'=>$status,
			'closed_date'=>$nowtime
		);

		$this->master_model->updateRecord('cf_recall', $arr, array('id'=>$incident_id));
		echo json_encode($arr);

	}

	public function send_content_email(){
		$email = $_POST['email'];
		$subject = $_POST['subject'];
		$body = $_POST['body'];
		
		$admin_email_sender = $this->common_model->admin_email_sender();

		$info_arr = array(
			'from'=>$admin_email_sender,
			'to'=>$email,
			'subject'=>$subject,
			'view'=>'task_email_sending'
		);

		$other_info = array(
			'body'=>$body
		);

		$arr['info_arr'] = $info_arr;
		$arr['other_info'] = $other_info;
		
		// $this->load->view('email/task_email_sending', $other_info);
		$this->email_sending->sendmail($info_arr,$other_info);

		echo json_encode($arr);
	}

	public function delete_content(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id,
		);

		$this->master_model->deleteRecord('checklist_contents','id',$id);
		echo json_encode($arr);
	}

	public function get_templates(){
		$user_type = (isset($_GET['user_type'])) ? $_GET['user_type'] : 'cc';
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id'] : $this->session->userdata('cc_selected_orgnaization');
		
		$all = $this->common_model->format_templates($org_id, $user_type);

		echo json_encode($all);
	}
	
	public function save_template(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$user_type = (isset($_POST['user_type'])) ? $_POST['user_type'] : 'cc';
		
		$name = $_POST['name'];
		$org_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('cc_selected_orgnaization');
		$user_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('user_id');
	
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : $user_id;
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : $org_id;
		
		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'name'=>$name,
			'org_id'=>$org_id,
			'user_id'=>$user_id
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_packs', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['date_created'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_packs', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);
	}

	public function get_incidents(){
		$incident_id = (isset($_GET['incident_id'])) ? $_GET['incident_id']: '';
		$incident_type = (isset($_GET['incident_type'])) ? $_GET['incident_type']: 'dashboard';
		$incident_status = ($incident_type == 'log') ? '1' : '0';
		
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id']: $this->session->userdata('cc_selected_orgnaization');
		
		//paginate recall config
		$where_recall = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'date_deleted'=>'0000-00-00 00:00:00',
			'incident_status'=>$incident_status
		);

		if(!empty($incident_id)){
			$where_recall = array(
				'id'=>$incident_id
			);
		}
		
		$settings = array();
		

		$arr = $this->common_model->format_incident($where_recall);
		$arr = (!empty($incident_id)) ? $arr[0] : $arr;
		
		echo json_encode($arr);
	}

	public function get_myteam(){

		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id']: $this->session->userdata('cc_selected_orgnaization');

		$whr = array(
			'login.default_org'=>$org_id,
			// 'login.date_deleted'=>'0000-00-00 00:00:00'
		);

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');
		$all_crt = $this->master_model->getRecords('cf_login_master as login', $whr);

		$the_all = array();

		if(!empty($all_crt)){
			foreach($all_crt as $r=>$value){
				$the_all[] = $this->common_model->format_crt($value['login_id']);
			}
		}
		echo json_encode($the_all);
		
	}

	public function get_wiki(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id']: $this->session->userdata('cc_selected_orgnaization');

		$id = (isset($_GET['id'])) ? $_GET['id'] : '';
		
		$whr = array(
			'org_id'=>$org_id,
			'subparent_id'=>'0',
			'parent_id'=>'0'
		);

		if(!empty($id)){
			$whr['id'] = $id;
		}

		$wiki_pages = $this->common_model->format_pages($whr);
		
		$arr = array();

		if(!empty($wiki_pages)){
			foreach($wiki_pages as $r=>$value){
				//include all history
				$whr2 = array(
					'parent_id'=>$value['id'],
				);
				$sort = array(
					'id'=>'DESC'
				);
				$wiki_pages2 = $this->common_model->format_pages($whr2, $sort);

				$value['histories'] = $wiki_pages2;


				//include subpages
				$whr3 = array(
					'subparent_id'=>$value['id'],
				);

				// $wiki_pages3 = $this->common_model->format_pages($whr3);
				// $w3 = array();

				// if(!empty($wiki_pages3)){
				// 	foreach($wiki_pages3 as $r3=>$value3){
				// 		//include all history
				// 		$whr2 = array(
				// 			'parent_id'=>$value3['id'],
				// 		);
				// 		$sort = array(
				// 			'id'=>'DESC'
				// 		);
				// 		$wiki_pages2 = $this->common_model->format_pages($whr2, $sort);

				// 		$value3['histories'] = $wiki_pages2;
				// 		$w3[] = $value3;
				// 	}
				// }

				// $value['subpages'] = $w3;


				$arr[] = $value;
			}
		}

		echo json_encode($arr);

	}

	public function delete_page(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id,
		);

		$this->master_model->deleteRecord('wiki_pages','id',$id);
		
		echo json_encode($arr);
	}
	
	public function get_files(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id']: $this->session->userdata('cc_selected_orgnaization');

		$whr = array(
			'org_id'=>$org_id,
		);
		$wiki_files = $this->common_model->format_files($whr);

		echo json_encode($wiki_files);
	}

	public function get_messages(){
		$user_id = (isset($_GET['user_id'])) ? $_GET['user_id']: $this->session->userdata('user_id');
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id']: $this->session->userdata('cc_selected_orgnaization');

		$whr = array(
			'org_id'=>$org_id,
			'cc_id'=>$cc_id,
		);

		$this->common_model->visited_messages($user_id);
		
		$format_messages = $this->common_model->format_messages(null, $whr);
		echo json_encode($format_messages);

	}

	
	public function upload_gmessage_file(){

		if ( !empty( $_FILES ) ) {
			$uploads_dir = 'uploads/gmessage-files/';
			$file_name = $_FILES[ 'file' ][ 'name' ];
			$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
			$uploadPath = 'uploads/wiki-files/';//dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];

			move_uploaded_file( $tempPath, $uploads_dir.$file_name );


			$user_id = (isset($_POST['user_id'])) ? $_POST['user_id']: $this->session->userdata('user_id');
			$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');
			$nowtime = $this->common_model->userdatetime($user_id);

			$arr = array(
				'file_name'=>$file_name,
				'org_id'=>$org_id,
				'cc_id'=>$user_id,
				'date_added'=>$nowtime,
			);

			$id = $this->master_model->insertRecord('group_messages_files', $arr, true);
			// $arr['id'] = $id;

			$the_file = $this->common_model->format_gm_files(array('id'=>$id));
			$the_file = (!empty($the_file)) ? $the_file[0] : null;

			$answer = array( 'answer' => 'File transfer completed', 'file'=>$the_file );
			$json = json_encode( $answer );

			echo $json;

		} else {

			echo 'No files';

		}
	}

	public function save_gm_files(){
		$data = $_POST;

		$arr = array(
			'message_id'=>$data['message_id']
		);

		$this->master_model->updateRecord('group_messages_files', $arr, array('id'=>$data['id']));
		echo json_encode($data);
	}


	public function save_task_comment(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$message = $_POST['message'];
		$recall_id = $_POST['recall_id'];
		$task_id = $_POST['task_id'];
		
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id']: $this->session->userdata('user_id');
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');

		$nowtime = $this->common_model->userdatetime($user_id);

		$arr = array(
			'message'=>$message,
			'task_id'=>$task_id,
			'recall_id'=>$recall_id,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('recall_task_comments', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('recall_task_comments', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

		// echo json_encode($_POST);
	}
	
	public function get_unread_message_count(){
		$user_id = (isset($_GET['user_id'])) ? $_GET['user_id']: $this->session->userdata('user_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id']: $this->session->userdata('cc_selected_orgnaization');
		$unread_message_count = $this->common_model->unread_message_count($user_id, $org_id);
		$arr = array(
			'count'=>$unread_message_count
		);
		echo json_encode($arr);
	}

	public function save_gmessage(){
		$message = $_POST['message'];
		$files = (isset($_POST['files'])) ? $_POST['files'] : array();
		
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id']: $this->session->userdata('user_id');
		
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			'message'=>$message,
			'org_id'=>$org_id,
			'user_id'=>$user_id,
			'created_at'=>$nowtime
		);

		$id = $this->master_model->insertRecord('group_messages', $arr, true);
		$arr['id'] = $id;
		$arr['date_format'] = date_format(date_create($arr['created_at']), 'm/d/Y');
		$arr['time_sent'] = date_format(date_create($arr['created_at']), 'h:i A');
		$arr['owner'] = 'me';
		$arr['author'] = $this->common_model->format_crt($arr['user_id']);
		$arr['files'] = $files;
		
		echo json_encode($arr);
		
	}

	public function save_name(){
		$data = $_POST;

		$arr = array(
			'crt_first_name'=>$this->master_model->encryptIt($data['first_name']),
			'crt_last_name'=>$this->master_model->encryptIt($data['last_name'])
		);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		echo json_encode($arr);

	}

	public function save_email(){
		$data = $_POST;

		$u = $this->master_model->getRecords('cf_login_master', array('login_id'=>$data['login_id'], 'pass_word'=>md5($data['password'])));

		$arr = array(
			'email_id'=>$this->master_model->encryptIt($data['crt_email']),
		);
		
		if(!empty($u)){
			$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$data['login_id']));

			$arr = array(
				'crt_email'=>$this->master_model->encryptIt($data['crt_email']),
			);
			$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		}

		$arr['count'] = count($u);

		echo json_encode($arr);
	}

	public function save_password(){
		$data = $_POST;

		$u = $this->master_model->getRecords('cf_login_master', array('login_id'=>$data['login_id'], 'pass_word'=>md5($data['password'])));

		$arr = array(
			'pass_word'=>md5($data['password2'])
		);
		
		if(!empty($u)){
			$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$data['login_id']));
		}

		$arr['count'] = count($u);

		echo json_encode($arr);
	}

	public function save_uinfo(){
		$data = $_POST;
		$nowtime = $this->common_model->userdatetime($data['login_id']);
		
		if($data['type'] == 'position'){
			$arr = array(
				'crt_position'=>$data['crt_position']
			);
		}
		else if($data['type'] == 'suite'){
			$arr = array(
				'crisis_function'=>$data['crisis_function']
			);
		}
		else if($data['type'] == 'timezone'){
			$time_zone = $this->master_model->getRecords('time_zone', array('ci_timezone'=>$data['timezone']));
			$data['timezone_details'] = $time_zone[0];
			$arr = array(
				'timezone'=>$data['timezone'],
				'timezone_updated'=>$nowtime
			);
		}
		else if($data['type'] == 'location'){
			$arr = array(
				'location'=>$data['location'],
				'location_lat'=>$data['location_lat'],
				'location_lng'=>$data['location_lng'],
			);
		}
		else if($data['type'] == 'mobile'){
			$countryformat = $this->common_model->countryformat(array('country_id'=>$data['country_id']));
			$data['countrycode'] = $countryformat[0]['calling_code'];
			$mob = $data['countrycode'].$data['crt_digits'];
			$arr = array(
				'countrycode'=>$this->master_model->encryptIt($data['countrycode']),
				'crt_digits'=>$this->master_model->encryptIt($data['crt_digits']),
				'crt_mobile'=>$this->master_model->encryptIt($mob),
			);
		}
		else{
			$arr = array(
				'crisis_function'=>$data['crisis_function']
			);
		}
		
		
		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));

		echo json_encode($data);
	}
	
	public function get_timezone(){
		$time_zone = $this->master_model->getRecords('time_zone');
		echo json_encode($time_zone);

	}

	public function get_countries(){
		$countryformat = $this->common_model->countryformat();
		echo json_encode($countryformat);
	}

	public function save_timezone(){
		$data = $_POST;

		$arr = array(
			'timezone'=>$data['timezone']
		);

		$this->session->set_userdata($arr);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		
		echo json_encode($arr);

	}


	public function save_page(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$subparent_id = $_POST['subparent_id'];
		$content = $_POST['content'];
		$title = $_POST['title'];

		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id']: $this->session->userdata('user_id');
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');


		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'subparent_id'=>$subparent_id,
			'content'=>$content,
			'title'=>$title,
		);

		if(!empty($id)){
			$prev = $this->master_model->getRecords('wiki_pages', array('id'=>$id));
			//save old to new entry
			$old_arr = array(
				'content'=>$prev[0]['content'],
				'title'=>$prev[0]['title'],
			);

			$old_arr['org_id'] = $org_id;
			$old_arr['parent_id'] = $id;
			$old_arr['user_id'] = $user_id;
			$old_arr['created_at'] = $nowtime;
			$old_id = $this->master_model->insertRecord('wiki_pages', $old_arr, true);
			$old_arr['id'] = $old_id;

			$this->master_model->updateRecord('wiki_pages', $arr, array('id'=>$id));

			//get old
			$whr = array(
				'id'=>$old_id
			);
			$history = $this->common_model->format_pages($whr);
			$arr['history'] = (!empty($history)) ? $history[0] : null;

		}
		else{
			$arr['org_id'] = $org_id;
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('wiki_pages', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

		// echo json_encode($_POST);
	}
	
	public function save_comment(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$message = $_POST['message'];
		$page_id = $_POST['page_id'];

		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id']: $this->session->userdata('user_id');
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');


		$nowtime = $this->common_model->userdatetime($user_id);

		$arr = array(
			'message'=>$message,
			'page_id'=>$page_id,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('wiki_comments', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('wiki_comments', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

		// echo json_encode($_POST);
	}


	public function save_incident(){

		$cc_id = (isset($_POST['logged_cc_login_id'])) ? $_POST['logged_cc_login_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_POST['cc_selected_orgnaization'])) ? $_POST['cc_selected_orgnaization']: $this->session->userdata('cc_selected_orgnaization');

		$description = $_POST['description'];
		$template_id = $_POST['template_id'];
		$generate_incident_id = $this->common_model->generate_incident_id($cc_id, $org_id);
		$format_inci_id = $this->common_model->format_inci_id($generate_incident_id);
		
		//timezone datetime
		$nowtime = $this->common_model->userdatetime($cc_id);
		$r_pack_id = '18';

		//insert scenario log 
		$recall_arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'template_id'=>$template_id,
			'incident_no'=>$format_inci_id,
			'cc_incident_id'=>$generate_incident_id,
			'description'=>$description,
			'incident_status'=>'0',
			'initiation_date'=>$nowtime,
			'input_pack_id'=>$r_pack_id,
		);


		$incident_id = $this->master_model->insertRecord('cf_recall',$recall_arr,true);
		$incident = $this->common_model->format_incident(array('id'=>$incident_id));
		
		echo json_encode($incident[0]);
		
	}	

	public function create_teammate(){
		$emails = $_POST['email'];
		$utype = $_POST['type'];
		$message = ''; //$_POST['message'];

		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');

		$active_module = '8'; //$this->session->userdata('org_module');
		$current_module = '8'; //substr($active_module, 0, 1);
		$timezone = (isset($_POST['timezone'])) ? $_POST['timezone'] : $this->session->userdata('timezone');
		
		$cct_firstname = '';
		$cct_lastname = '';



		$whr = array(
			'organization_id'=>$org_id
		);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');

		// $the_data = array();
		// $count = 0;
		// foreach($emails as $r=>$value){
			$crt_email = $emails;

			$onetime_key = md5(microtime());


			//check existing crt on a module and org
			$whr_same_exist = array(
				'date_deleted'=>'0000-00-00 00:00:00',
				'default_org'=>$org_id,
				'email_id'=>$this->master_model->encryptIt($crt_email)
			);
			$the_same_crt = $this->master_model->getRecordCount('cf_login_master', $whr_same_exist);	
			
			$crt_duplicate = (!empty($the_same_crt)) ? true : false;

			//team count
			$whr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id
			);
			$accountActivated = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
			$all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);
			$total_crt_count = ($accountActivated[0]['stripe_customer_id'] == '0') ? count($all_crt) : 0;

			$the_data = array();
			if($total_crt_count < 4){
				
				if(!$crt_duplicate && $crt_email){
					
					//check existing email
					$users = $this->master_model->getRecords('cf_login_master');	
					$user_count = 0;

					foreach($users as $ss=>$usr){
						if($usr['email_id'] == $this->master_model->encryptIt($crt_email)){
							$user_count++;
						}
					}

					$nowtime = date("Y-m-d h:i:s");

					$admin_array = array(
						'email_id'=>$this->master_model->encryptIt($crt_email),
						'user_level'=>$utype, //[$count],
						'default_org'=>$org_id,
						'onetime_key'=>$onetime_key,
						'date_added'=>$nowtime
					);

					$login_id = $this->master_model->insertRecord('cf_login_master', $admin_array, true);


					//response_team inputs
					$input_array = array(
						'login_id'=>$login_id,
						// 'group_id'=>$group,
						'crt_first_name'=>$this->master_model->encryptIt($cct_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cct_lastname),
						'crt_organisation'=>$org_name[0]['organization_name'],
						// 'crt_position'=>$crt_position,
						'crt_email'=>$this->master_model->encryptIt($crt_email),

						// 'module'=>$active_module,
						'timezone'=>$timezone,
						'cc_id'=>$cc_id,
						'org_id'=>$org_id
					);

					$new_crt_id = $this->master_model->insertRecord('cf_crisis_response_team', $input_array, true);


					//insert im room
					$wherrr = array(
						'org_id'=>$org_id,
						'login_id !='=>$login_id
					);
					
					$crt_memb = $this->master_model->getRecords('cf_crisis_response_team',$wherrr,'*');
					
					$chat_id = 'im_'.$login_id.'-'.$cc_id;
					$im_cc = array(
						'sender_id'=>$login_id,
						'receiver_id'=>$cc_id,
						'chat_id'=>'im_'.$login_id.'-'.$cc_id,
						
					);
					
					$this->master_model->insertRecord('private_instant_messages',$im_cc);
					
					$im_cc2 = array(
						'sender_id'=>$cc_id,
						'receiver_id'=>$login_id,
						'chat_id'=>'im_'.$login_id.'-'.$cc_id,
						
					);

					$this->master_model->insertRecord('private_instant_messages',$im_cc2);
					
					//create file room for crt and cc
					$room_name = 'chat_room/im/';
					$room_name .= $chat_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	
					
					// if(count($crt_memb) > 0){
					// 	foreach($crt_memb as $ccc => $memb){
					// 		$chat_id = 'im_'.$login_id.'-'.$memb['login_id'];
					// 		$im = array(
					// 			'sender_id'=>$login_id,
					// 			'receiver_id'=>$memb['login_id'],
					// 			'chat_id'=>$chat_id,
								
					// 		);
							
					// 		$this->master_model->insertRecord('private_instant_messages',$im);
							
					// 		$im2 = array(
					// 			'sender_id'=>$memb['login_id'],
					// 			'receiver_id'=>$login_id,
					// 			'chat_id'=>$chat_id,
								
					// 		);
							
					// 		$this->master_model->insertRecord('private_instant_messages',$im2);
							
							
					// 		//create file room for crt and crts
					// 		$room_name = 'chat_room/im/';
					// 		$room_name .= $chat_id;
					// 		$room_name .= '.txt';
							
					// 		$myfile = fopen($room_name, "w");	
							
					// 	}
						
					// } //.insert im room


					$whr = array('id'=>'1');
					$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');


					$info_arr = array(
						'from'=>$adminemail[0]['recovery_email'],
						'to'=>$crt_email,
						'subject'=>'Welcome to CrisisFlo',
						'view'=>'registration-mail-to-cc' //crt_registration_on_crisesflo'
					);


					if($user_count > 0){
						$info_arr['view'] = 'registration-mail-to-cc'; //crt_registration_on_crisesflo_2';
					}

					$other_info = array(
						'first_name'=>$cct_firstname,
						'last_name'=>$cct_lastname,
						'login_id'=>$onetime_key,
						'onetime_key'=>$onetime_key,
						'email'=>$crt_email
					);

					$this->email_sending->sendmail($info_arr,$other_info);
					$the_data[] = 'added';

					$action_message_id = '55';
					$action_message = $this->common_model->get_message($action_message_id);
					
					$message = $action_message['success'];
					$result = 'success';
				}

				if($crt_duplicate){
					$result = 'error';
					$message = 'User already registered';
				}
			}
			else{
				$result = 'error';
				$message = 'Please upgrade your account in order to add more than 5 team members.';
			}
	
			// $count++;

		// }
		
		$arr = array(
			'message'=>$message,
			'result'=>$result,
			'emails'=>$emails,
			'the_data'=>$the_data
		);
		echo json_encode($arr);

	}

	public function remove_user(){
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');
		$nowtime = date("Y-m-d h:i:s");
		
		//check count of admin
		$all_admin = $this->master_model->getRecordCount('cf_login_master', array('default_org'=>$org_id, 'user_level'=>'0', 'date_deleted'=>'0000-00-00 00:00:00'));
		
		$data = array(
			'success'=>'',
			'error'=>''
		);
		$login_id = $_POST['login_id'];

		//get validation message from admin
		$action_message_id = '52';
		$action_message = $this->common_model->get_message($action_message_id);


		//check if user is admin
		$is_admin = $this->master_model->getRecordCount('cf_login_master', array('login_id'=>$login_id, 'user_level'=>'0'));

		$del_data = array(
			'date_deleted'=>$nowtime
		);
		if(!empty($is_admin)){
			if($all_admin > 1){
				$data['success'] = 'Administrator successfully deleted.';
				$this->master_model->updateRecord('cf_login_master', $del_data, array('login_id'=>$login_id));
			}
			else{
				$data['error'] = $action_message['error'];
			}
		}
		else{
			$data['success'] = $action_message['success'];
			$this->master_model->updateRecord('cf_login_master', $del_data, array('login_id'=>$login_id));
		}
		echo json_encode($data);
		
	}

	public function save_user(){
		$login_id = $_POST['login_id'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$crt_email = $_POST['crt_email'];
		$crt_position = $_POST['crt_position'];
		$location = $_POST['location'];
		$location_lat = $_POST['location_lat'];
		$location_lng = $_POST['location_lng'];
		$country_id = $_POST['country_id'];
		$countrycode = $_POST['countrycode'];
		$crt_digits = ltrim($_POST['crt_digits'], '0');
		$crt_no = $countrycode.$crt_digits;

		$data = array(
			'crt_first_name'=>$this->master_model->encryptIt($first_name),
			'crt_last_name'=>$this->master_model->encryptIt($last_name),
			'crt_email'=>$this->master_model->encryptIt($crt_email),
			'crt_position'=>$crt_position,
			'location'=>$location,
			'location_lat'=>$location_lat,
			'location_lng'=>$location_lng,
			'countrycode'=>$this->master_model->encryptIt($countrycode),
			'crt_digits'=>$this->master_model->encryptIt($crt_digits),
			'crt_mobile'=>$this->master_model->encryptIt($crt_no)
		);

		$this->master_model->updateRecord('cf_crisis_response_team', $data, array('login_id'=>$login_id));
		echo json_encode($data);
	}


	public function upload_file(){

		if ( !empty( $_FILES ) ) {
			$uploads_dir = 'uploads/wiki-files/';
			$file_name = $_FILES[ 'file' ][ 'name' ];
			$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
			$uploadPath = 'uploads/wiki-files/';//dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];

			move_uploaded_file( $tempPath, $uploads_dir.$file_name );


			$user_id = (isset($_POST['user_id'])) ? $_POST['user_id']: $this->session->userdata('user_id');
			$org_id = (isset($_POST['org_id'])) ? $_POST['org_id']: $this->session->userdata('cc_selected_orgnaization');
			$nowtime = $this->common_model->userdatetime($user_id);

			$arr = array(
				'file_name'=>$file_name,
				'org_id'=>$org_id,
				'cc_id'=>$user_id,
				'date_added'=>$nowtime,
			);

			$id = $this->master_model->insertRecord('wiki_files', $arr, true);
			// $arr['id'] = $id;

			$the_file = $this->common_model->format_files(array('id'=>$id));
			$the_file = (!empty($the_file)) ? $the_file[0] : null;

			$answer = array( 'answer' => 'File transfer completed', 'file'=>$the_file );
			$json = json_encode( $answer );

			echo $json;

		} else {

			echo 'No files';

		}
	}
	
	public function delete_file(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id,
		);

		$this->master_model->deleteRecord('wiki_files','id',$id);
		
		echo json_encode($arr);
	}

	public function get_crts(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id'] : $this->session->userdata('cc_selected_orgnaization');
		
		$the_crts = $this->common_model->get_org_crts($cc_id,$org_id, null, 'assign');
		echo json_encode($the_crts);
	}
	
	public function delete_header(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			'deleted'=>'1',
			'date_deleted'=>$nowtime
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_packs_steps', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		echo json_encode($arr);

	}

	public function save_tasks(){ //cf_recall_guidance
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$recall_pack_id = $_POST['recall_pack_id'];
		$arrangement = $_POST['arrangement'];
		$question = $_POST['question'];
		$step_no = $_POST['step_no'];
		$assigned_id = $_POST['assigned_id'];
		$months = ($_POST['months'] != 'undefined') ? $_POST['months'] : '';
		$days = ($_POST['days'] != 'undefined') ? $_POST['days'] : '';
		$hours = ($_POST['hours'] != 'undefined') ? $_POST['hours'] : '';
		$mins = ($_POST['mins'] != 'undefined') ? $_POST['mins'] : '';
		
		$user_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : $this->session->userdata('logged_cc_login_id');
		
		// $due_date = $_POST['due_date'];
		// $due_time = $_POST['due_time'];

		$nowtime = $this->common_model->userdatetime($user_id);

		$arr = array(
			'arrangement'=>$arrangement,
			'assigned_id'=>$assigned_id,
			'months'=>$months,
			'days'=>$days,
			'hours'=>$hours,
			'mins'=>$mins,
			// 'due_date'=>$due_date,
			// 'due_time'=>$due_time,
			'question'=>$question,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_guidance', $arr, array('id'=>$id));
			$arr['id'] = $id;			
		}
		else{
			$arr['recall_pack_id'] = $recall_pack_id;
			$arr['step_no'] = $step_no;
			$arr['date_created'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_guidance', $arr, true);
			$arr['id'] = $id;
			$arr['format_task_id'] = $this->common_model->format_task_id($id);
		}

		$this->master_model->updateRecord('cf_crisis_response_team',array('tasks_review_date'=>$nowtime),array('login_id'=>$user_id));

		echo json_encode($arr);

	}

	public function delete_task(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			// 'deleted'=>'1',
			'date_deleted'=>$nowtime
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_guidance', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		echo json_encode($arr);

	}
	public function save_contents(){ //checklist_contents
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$recall_pack_id = '18';
		$checklist_id = $_POST['checklist_id'];
		$sort = $_POST['sort'];
		$content = $_POST['content'];
		$module = $_POST['module'];
		$answer = $_POST['answer'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'checklist_id'=>$checklist_id,
			'module'=>$module,
			'sort'=>$sort,
			'content'=>$content,
			'answer'=>$answer,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('checklist_contents', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('checklist_contents', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_fields(){ //save_fields
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$text = $_POST['text'];
		$content_id = $_POST['content_id'];
		$type = $_POST['type'];
		$answer = $_POST['answer'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'text'=>$text,
			'content_id'=>$content_id,
			'type'=>$type,
			'answer'=>$answer,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('content_fields', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('content_fields', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_options(){ //save_options
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$text = $_POST['text'];
		$content_id = $_POST['content_id'];
		$type = $_POST['type'];
		$answer = $_POST['answer'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'text'=>$text,
			'content_id'=>$content_id,
			'type'=>$type,
			'answer'=>$answer,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('content_options', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('content_options', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_emails(){ //save_emails
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$content_id = $_POST['content_id'];
		$email = $_POST['email'];
		$cc = $_POST['cc'];
		$bcc = $_POST['bcc'];
		$subject = $_POST['subject'];
		$body = $_POST['body'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'content_id'=>$content_id,
			'email'=>$email,
			'cc'=>$cc,
			'bcc'=>$bcc,
			'subject'=>$subject,
			'body'=>$body,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('content_emails', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('content_emails', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_stripe_card(){
		$data = $_POST;
	
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : $this->session->userdata('cc_selected_orgnaization');
		$logged_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : $this->session->userdata('user_id');
		
		$nowtime = $this->common_model->userdatetime($cc_id);


		$user_id = $cc_id;
		$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$username = $this->common_model->getname($user_id);
		$email = $this->master_model->decryptIt($cc_info[0]['email_id']);
		$orgname = $this->common_model->getorgname($cc_info[0]['default_org']);
		$token = $data['token'];
		
		$data2 = array(
			'token'=>$token,
			'username'=>$username,
			'email'=>$email,
			'orgname'=>$orgname
		);

		//save card details to customer stripe table
		$arr = array(
			'card_info'=>serialize($data),
			'user_id'=>$cc_id,
			'logged_id'=>$logged_id,
			'org_id'=>$org_id
		);

		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; // (base_url() != 'https://www.crisisflo.com/') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';
		
		Stripe::setApiKey($apikey);
	
	
		try {
			// Create a Customer
			$customer = Stripe_Customer::create(array(
			  "description" => $orgname,
			  "email" => $email,
			  //"plan" => $subs_plan_id, //subscrib user to plan 
			  "card" => $token // obtained with Stripe.js
			));
			
			if($customer->id){
				$login_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
			
				$cc_arr = array(
					'stripe_customer_id'=>$customer->id,
					'user_type'=>'live'
				);
				
				if($login_info[0]['trial_end'] == '0000-00-00 00:00:00'){
					$nowtime = date("Y-m-d h:i:s");
					$cc_arr['trial_end'] = $nowtime;
				}
				
				$this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$user_id));


				//save card details to customer stripe table
				$arr['stripe_id'] = $customer->id;
				$id = $this->master_model->insertRecord('stripe_customers', $arr, true);
				$arr['id'] = $id;

				$format_billing = $this->common_model->format_billing($cc_id);
				$this->session->set_userdata('billing', $format_billing);

				$data2['result'] = 'success';
			}
			
		} catch(Stripe_CardError $e) {
		  // The card has been declined
			//$this->session->set_flashdata('error','The card has been declined.');
			$data2['result'] = 'declined';
		}		
		
		echo json_encode($arr);
	}

	public function save_form_payment(){
		$data = $_POST;
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : $this->session->userdata('logged_cc_login_id');
		
		$data['number'] = (isset($data['number'])) ? $this->master_model->encryptIt($data['number']) : '';

		$arr = array(
			'form_info'=>serialize($data),
		);
		$this->master_model->updateRecord('stripe_customers', $arr, array('id'=>$data['id']));
		
		$format_billing = $this->common_model->format_billing($cc_id);
		$this->session->set_userdata('billing', $format_billing);

		$arr['format_billing'] = $format_billing;
		echo json_encode($arr);
	}
	
	public function get_billing_info(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('logged_cc_login_id');
		$format_billing = $this->common_model->format_billing($cc_id);
		$this->session->set_userdata('billing', $format_billing);
		echo json_encode($format_billing);
	}

	public function get_billing_contact(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('logged_cc_login_id');
	
		$billing_contact = $this->common_model->get_billing_contact($cc_id);
		echo json_encode($billing_contact);
	}

	public function get_transactions(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id'] : $this->session->userdata('cc_selected_orgnaization');

		$whr = array(
			'org_id'=>$org_id
		);
		
		$format_transaction_history = $this->common_model->format_transaction_history($whr);

		$billing = $this->common_model->single_payment($cc_id, 'preview');
		echo json_encode($billing); return false;
		$whr = array(
			'default_org'=>$org_id,
		);
		$all_crt = $this->master_model->getRecords('cf_login_master', $whr);

		$users_count = array(
			'users'=>count($all_crt),
			'percent'=>round(count($all_crt)/5 * 100, 0)
		);

		$trial_days = $this->common_model->trial_days($cc_id);

		$arr = array(
			'billing'=>$billing,
			'users_count'=>$users_count,
			'trial_days'=>$trial_days,
			'history'=>$format_transaction_history
		);
		echo json_encode($arr);

	}


	public function cancel_subscription(){
		$cc_id = $_POST['cc_id'];
		$nowtime = $this->common_model->userdatetime($cc_id);
		
		$arr['subscription_cancelled_date'] = $nowtime;
		$arr['subscription_cancelled'] = '1';
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$cc_id));
		echo json_encode($arr);
	}

	public function reactivate_subscription(){
		$cc_id = $_POST['cc_id'];
		$nowtime = $this->common_model->userdatetime($cc_id);
		
		// $arr['subscription_cancelled_date'] = $nowtime;
		$arr['subscription_cancelled'] = '0';
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$cc_id));
		echo json_encode($arr);
	}

	public function save_billing_contact(){
		$data = $_POST;

		$arr = array(
			'billing_contact'=>serialize($data)
		);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		echo json_encode($arr);
	}


	public function cron_extend_trial(){
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		
		$this->db->join('organization_master as org','cc.login_id=org.cc_id');

		$all_cc = $this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0', 'login.user_type'=>'demo', 'org.enabled'=>'Y'));

		$arr = array();

		if(!empty($all_cc)){

			foreach($all_cc as $r => $value) {


				if($value['trial_end'] != '0000-00-00 00:00:00'){

					$date1= time();
					$date2= strtotime($value['trial_end']);

					//Calculate the difference.
					$difference = $date2 - $date1;
					//Convert seconds into days.
					$remaining = floor($difference / (60*60*24) );
					$remaining = max($remaining,0);
					
					if($remaining == 3){
						//send email for extension

						/**script to send email */
						$whr = array('id'=>'1');
						$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

							
						$info_arr = array(
							'from'=>$adminemail[0]['recovery_email'],
							'to'=>$this->master_model->decryptIt($value['crt_email']),
							'subject'=>'CrisisFlo account successfully activated',
							'view'=>'cron_extend_trial'
						);
						
						$other_info = array(
							'first_name'=>$this->master_model->decryptIt($value['crt_first_name']),
							'last_name'=>$this->master_model->decryptIt($value['crt_last_name']),
							'login_id'=>$value['login_id']
						);
						
						$this->email_sending->sendmail($info_arr, $other_info);

						$arr[] = array(
							'info_arr'=>$info_arr,
							'other_info'=>$other_info
						);
					}
				}
				
						
			}

		}
		echo json_encode($arr);
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//use OpenTok\OpenTok;

class Conference extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		

	}
	
	public function stripe() {
/*		Stripe::setApiKey('sk_test_zC4nyIzsM6pXKLXhsrJZHiB0 ');
		$myCard = array('number' => '4242424242424242', 'exp_month' => 5, 'exp_year' => 2015);
		$charge = Stripe_Charge::create(array('card' => $myCard, 'amount' => 2000, 'currency' => 'usd'));
		$all_cust = Stripe_Customer::all();
		echo $charge;
		
		
*/
		$this->load->model('admin_model');
		$all_cust = $this->admin_model->stripe_get_cust();
		


		echo $all_cust;

		
		
 	}
	
	public function stripe_checkout(){
		$this->load->view('testcheckout');
	}
	
	public function add_cust(){
		Stripe::setApiKey('sk_test_zC4nyIzsM6pXKLXhsrJZHiB0 ');
		
		$cust = Stripe_Customer::create(array(
		  "description" => "Customer for test@example.com",
		  "email" => "test@exampe.com" // obtained with Stripe.js
		));	
		
		echo $cust;
	}
	
	public function opentok()
	{
		$apiKey = "45109742";
		$apiSecret = "701543f01063363cc2271f5edbdd6cd8889dfc28";
        $opentok = new OpenTok($apiKey, $apiSecret);
        $session = $opentok->createSession();
        $data = array(
            'sessionId' => $session->getSessionId(),
            'token' => $session->generateToken()
        );
        $this->load->view('opentok', $data);
		
		//$this->load->view('welcome_message');
	}
	
	

	public function index()

	{
		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();
		

		$data=array('page_title'=>'Add Crisis Responce Team Member','error'=>'','middle_content'=>'conference-view',

		'success'=>'');

		$this->load->view('cc/cc-view',$data);

	}

	

	public function team() {
		$cc_id = $this->uri->segment(3);
		$org_id = $this->uri->segment(4);

		if ($cc_id == $this->session->userdata('logged_parent_cc') || $cc_id == $this->session->userdata('logged_parent_crt') && $org_id == $this->session->userdata('cc_selected_orgnaization') ){
			
			$this->load->view('conference-window-view');
		}

		else{
			echo '<h3 style="text-align: center; margin-top: 130px; color: #ccc;">You\'re session expired already.</p>';	
		}



	}

	
	public function store_meeting()
	{

		$setup_date = $this->common_model->userdatetime();
		
		//adding setup up meeting
		$data = array(
					   'cc_id' => $_POST['cc_id'],
					   'org_id' => $_POST['org_id'],
					   'meeting_name' => $_POST['meeting_name'],
					   'setup_date' => $setup_date
					);
		
		$meeting_id = $this->master_model->insertRecord('video',$data,true);

		$curmeeting = $this->master_model->getRecords('organization_master',array('organization_id'=>$_POST['org_id']));
		
		$totalstreamed = $curmeeting[0]['conf_stream'] + 1;
		
		$this->master_model->updateRecord('organization_master',array('current_meeting'=>$meeting_id,'conf_stream'=> $totalstreamed),array('cc_id'=>$_POST['cc_id'],'organization_id'=>$_POST['org_id']));


	}
	
	public function add_peers()
	{
	
		$curmeeting = $this->master_model->getRecords('organization_master',array('organization_id'=>$_POST['org_id']));
		
		$totalpeer = $_POST['peer'];
		
		$this->master_model->updateRecord('video',array('peers'=>$totalpeer),array('id'=>$curmeeting[0]['current_meeting'],'org_id'=>$_POST['org_id']));
		
	}



	public function peersdata()
	{
		
	
		$curmeeting = $this->master_model->getRecords('organization_master',array('organization_id'=>$_POST['org_id']));
		
		$data = array(
			'crt_id'=>$_POST['crt_id'],
			'curr_meeting_id'=>$curmeeting[0]['current_meeting'],
			'org_id'=>$_POST['org_id'],
			'name'=>$_POST['name'],
			'time_joined'=>$_POST['time_joined']
			
		);
		
		$this->master_model->insertRecord('conference_participants',$data);
		
		
		$vidstreamed = $this->master_model->getRecords('video',array('org_id'=>$_POST['org_id'], 'id'=>$curmeeting[0]['current_meeting']));
		
		$totalstreamed = $vidstreamed[0]['streamed'] + 1;
		//add streamed vid
		$this->master_model->updateRecord('video',array('streamed'=>$totalstreamed),array('id'=>$curmeeting[0]['current_meeting'],'org_id'=>$_POST['org_id']));
		
		$remainingstream = $curmeeting[0]['conf_stream'] - 1;
		//deduct streamed vid
		$this->master_model->updateRecord('organization_master',array('conf_stream'=>$remainingstream),array('organization_id'=>$_POST['org_id']));
		

	}


	//record end time 
	public function stop_meeting()
	{
		$curmeeting = $this->master_model->getRecords('organization_master',array('cc_id'=>$_POST['cc_id'],'organization_id'=>$_POST['org_id']));
		
	//	if ($curmeeting[0]['current_meeting'] !="0"){
			$setup_date = $this->master_model->getRecords('video',array('id'=>$curmeeting[0]['current_meeting'],'cc_id'=>$_POST['cc_id'],'org_id'=>$_POST['org_id']));
			
			$thesetup_date = new DateTime($setup_date[0]['setup_date']);
	
	
			$end_date = $this->common_model->userdatetime();
			
			//$_POST['end_date'];
			
			
			$v_closed = new DateTime($end_date);
			$difference = $thesetup_date->diff($v_closed);
			
			$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';
	
	
			//get the interval in minutes
			$minutes = $difference->days * 24 * 60;
			$minutes += $difference->h * 60;
			$minutes += $difference->i;
			
			//get time alloc
			//$timealoc = $this->master_model->getRecords('organization_master',array('organization_id'=>$_POST['org_id']));
	
			//add end date
			$this->master_model->updateRecord('video',array('end_date'=>$end_date,'lapse_time'=>$interval),array('id'=>$curmeeting[0]['current_meeting'],'cc_id'=>$_POST['cc_id'],'org_id'=>$_POST['org_id']));
	
			//	$newminutes = $curmeeting[0]['conf_time'] - $minutes;
			
			
			//remove curr meeting in org
			$this->master_model->updateRecord('organization_master',array('current_meeting'=>'0'),array('organization_id'=>$_POST['org_id']));
	
	//	}
	}

	public function onlines()
	{
		if ( $this->session->userdata('cc_selected_orgnaization') !='' || $this->session->userdata('crt_selected_orgnaization') !=''){ 

							
			$this->db->order_by("last_activity", "desc"); 
			$this->db->distinct();
			$this->db->group_by('user_data');
			$onlines = $this->db->get('ci_sessions');

			//$this->master_model->getRecords('ci_sessions');
			if ($onlines->num_rows() > 0){
				$ccid = $this->session->userdata('team_cc');
				$check_duplicate = array();
				foreach ($onlines->result() as $value){
				
					$userdata = unserialize($value->user_data);
					
					$idle = time() - $value->last_activity;
				  
					$dele = time() - 7200;
					$this->db->where('last_activity <', $dele);
					$this->db->delete('ci_sessions'); 

					
					
				  if (isset($userdata['logged_display_name']) && isset($userdata['team_cc']) && $userdata['team_cc'] == $ccid){
					  
					if( isset($userdata['cc_selected_orgnaization']) && $userdata['cc_selected_orgnaization'] == $this->session->userdata('cc_selected_orgnaization')){
					  
						if(in_array($userdata['my_id'], $check_duplicate) == false && $userdata['my_id'] != $this->session->userdata('my_id')){
						
//							if($idle > 300){
//							  echo '<li><i class="fa fa-circle" style="color: #0C3"></i> ';
//							}
//							else{
							  echo '<li><i class="fa fa-circle text-green"></i> ';
//							}
							  echo $userdata['logged_display_name'];
							 // echo '<br>'.$idle.'<br>';
							  //echo time();.
				
							  echo '</li>';
							array_push($check_duplicate, $userdata['my_id']);
							  
						}
					 }
				  }
				  
				  if(count($check_duplicate) == '0'){
						//echo '<span class="text-muted">No online users.</span>';
				  }
				  


			  }
			}
		}
		else{
			echo '<li><span class="text-muted"><i class="fa fa-warning"></i> Something went wrong. Please check your session.</span></li>';
		}

	}


	public function joined()
	{
		if ( $this->session->userdata('cc_selected_orgnaization') !='' || $this->session->userdata('crt_selected_orgnaization') !=''){ 
		
			if ( $this->session->userdata('cc_selected_orgnaization') !='') {
				$org = array(
					'organization_id' =>$this->session->userdata('cc_selected_orgnaization')
				);
			}
			else{
				$org = array(
					'organization_id' =>$this->session->userdata('crt_selected_orgnaization')
				);
			}
			
			$currmeeting = $this->master_model->getRecords('organization_master',$org);
			
			if ( $this->session->userdata('cc_selected_orgnaization') !='') {
				
				$orgg = $this->session->userdata('cc_selected_orgnaization');
			}
			else{
				
				$orgg = $this->session->userdata('crt_selected_orgnaization');
			}
			
			$conf_name = $this->master_model->getRecords('video',array('id'=>$currmeeting[0]['current_meeting'], 'org_id'=>$orgg));

			
			if ($currmeeting[0]['current_meeting'] != '0'){
				$peers = $this->master_model->getRecords('conference_participants',array('curr_meeting_id'=>$currmeeting[0]['current_meeting']));
				
				if (count($peers) > '0'){
					echo '<p>'.$conf_name[0]['meeting_name'].'<ol style="margin-left: 25px; padding-left: 0px;">';
					foreach($peers as $r => $value){
						$timejoined = date('d M Y - H:i:s',strtotime($value['time_joined']));
						echo '<li title="Joined at: '.$timejoined.'">'.$value['name'].'</li>';
					}
					echo '</ol>';
				}
				else{
					

					echo '<span class="text-muted">Empty. No one joined '.$conf_name[0]['meeting_name'].'.</span>';
				
				}
				
			}
			
			else{
				echo '<span class="text-muted">Conference not started</span>';
			}
		
		
		}
		else{
			echo '<span class="text-muted"><i class="fa fa-warning"></i> Something went wrong. Please check your session.</span>';
		}
	}
	
	
	//process chat 
	public function process_chat()
	{

		$function = $_POST['function'];
		$org = $_POST['org'];
		
		
		$log = array();
		
		$org_room = 'chat_room/org';
		$org_room .= $org;
		$org_room .= '.txt';
		
		switch($function) {
		
			 case('getState'):
				 if(file_exists($org_room)){
				   $lines = file($org_room);
				 }
				 $log['state'] = count($lines); 
				 break;	
			
			 case('update'):
				$state = $_POST['state'];
				if(file_exists($org_room)){
				   $lines = file($org_room);
				 }
				 $count =  count($lines);
				 if($state == $count){
					 $log['state'] = $state;
					 $log['text'] = false;
					 
					 }
					 else{
						 $text= array();
						 $log['state'] = $state + count($lines) - $state;
						 foreach ($lines as $line_num => $line)
						   {
							   if($line_num >= $state){
							 $text[] =  $line = str_replace("\n", "", $line);
							   }
			 
							}
						 $log['text'] = $text; 
					 }
				  
				 break;
			 
			 case('send'):
			  $nickname = htmlentities(strip_tags($_POST['nickname']));
			  
			$nowsenttime = time();
			$timezone = $this->session->userdata('timezone');
			$thesenttime = gmt_to_local($nowsenttime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
			
			//$time_sent = gmdate("g:i:sa d/m/Y", $thesenttime);
			$time_sent = gmdate("g:i:a", $thesenttime); //g:i:sa d/m/Y
			$time_sent_hover_title = gmdate("g:i:sa d/m/Y", $thesenttime); //g:i:sa d/m/Y
			
				 $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
				  $message = htmlentities(strip_tags($_POST['message']));
			 if(($message) != "\n"){
				
				 if(preg_match($reg_exUrl, $message, $url)) {
					$message = preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $message);
					} 
				 
				
				 fwrite(fopen($org_room, 'a'), "<span>". $nickname . "</span>" . $message = str_replace("\n", " ", $message) . "<br><span class='pull-right small chat_date' title='".$time_sent_hover_title."'>" . $time_sent . "</span>" . "\n"); 
			 }
				 break;
			
		}
		
		echo json_encode($log);	
		
	}
	
	//record end time 
	public function cc_stop()
	{

		$curmeeting = $this->master_model->getRecords('organization_master',array('cc_id'=>$_POST['cc_id'],'organization_id'=>$_POST['org_id']));
		
		$setup_date = $this->master_model->getRecords('video',array('id'=>$curmeeting[0]['current_meeting'],'cc_id'=>$_POST['cc_id'],'org_id'=>$_POST['org_id']));
		
		$thesetup_date = new DateTime($setup_date[0]['setup_date']);

		
		$end_date = $_POST['end_date'];
		
		
		$v_closed = new DateTime($end_date);
		$difference = $thesetup_date->diff($v_closed);
		
		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';


		//get the interval in minutes
		$minutes = $difference->days * 24 * 60;
		$minutes += $difference->h * 60;
		$minutes += $difference->i;
		
		//get time alloc
		//$timealoc = $this->master_model->getRecords('organization_master',array('organization_id'=>$_POST['org_id']));

		//add end date
		$this->master_model->updateRecord('video',array('end_date'=>$end_date,'lapse_time'=>$interval),array('id'=>$curmeeting[0]['current_meeting'],'cc_id'=>$_POST['cc_id'],'org_id'=>$_POST['org_id']));

			$newminutes = $curmeeting[0]['conf_time'] - $minutes;
		
		
		//update org time alloc
		$this->master_model->updateRecord('organization_master',array('conf_time'=>$newminutes),array('organization_id'=>$_POST['org_id']));


	}
	
	
}

?>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Example extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
	
	
	public function emtpy_db(){
		/*do not include
		admin_login,
		cf_recall_guidance,
		cf_recall_packs,
		cf_recall_packs_steps,
		cf_recall_steps_category,
		countries_currency,
		country_t,
		email_id_master,
		email_messages_master,
		faq,
		forum_category_master,
		module_master,
		organization_master
		reminder_master
		sms_messages
		stripe_webhook
		time_zone
		
		*/
		
		//custom delete organization_master
		$dbname = 'organization_master';
		$this->master_model->deleteRecord($dbname,'organization_id !=','1');
		$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 2';
		$this->db->query($reset_query);
		echo $dbname.' - reset/empty success<br>';


		//custom delete for cf_recall_packs
		$dbname = 'cf_recall_packs';
		$this->master_model->deleteRecord($dbname,'continuity !=','1');
		$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 19';
		$this->db->query($reset_query);
		echo $dbname.' - reset/empty success<br>';
		
		
		$dbs = array(
			'audit_log',
			'bulk_attachments',
			'bulk_data',
			'bulk_kpi',
			'bulk_kpi_owners',
			'bulk_notification',
			'case_document_report',
			'cf_cc_reminders',
			'cf_continuity',
			'cf_continuity_steps',
			'cf_coordinator_master',
			'cf_cost_category',
			'cf_cost_category_item',
			'cf_crisis_response_team',
			'cf_file_history',
			'cf_file_upload',
			'cf_login_master',
			'cf_recall',
			'cf_recall_blockers',
			'cf_recall_markers',
			'cf_recall_steps',
			'cf_recall_steps_file',
			'cf_scenario',
			'cf_stakeholder',
			'cf_standby_messages',
			'cf_task',
			'ci_sessions',
			'common_messages',
			'conference_participants',
			'conference_tokbox',
			'employee_master',
			'forum_post_master',
			'input_packs',
			'log_scene_master',
			'log_task_master',
			'private_instant_messages',
			'private_messages',
			'private_message_file',
			'reporting',
			'reseller_master',
			'sms',
			'states_master',
			'stripe_customers',
			'subscribe_request',
			'user_group',
			'video',
		);
		
		
		foreach($dbs as $dbname){
			
			$query = 'DELETE FROM '.$dbname;
			$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 1';
			$this->db->query($query);
			$this->db->query($reset_query);
			echo $dbname.' -  reset/empty success<br>';
		}

	}
	

	public function upload(){
		
		
		if(isset($_FILES["file"])){
			print_r($_FILES);
			$new_image_name = "namethisimage.jpg";
			move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/recallsteps-uploads/".$new_image_name);	

			$arr = array(
				'base64_img'=>'yteah',
			);
		}
		else {
			$arr = array(
				'base64_img'=>'failed',
			);
		}
		
		
		$this->master_model->insertRecord('cf_recall_steps_file', $arr);
		

		
		
	}
	
	

	
	public function get_price(){
		$arr = array(
			'ph',
			'be'
		);
		
		$prices = array();
		foreach($arr as $value){
			$prices[] =  $this->send_price_req($value);
		}
		
		print_r($prices);
		
//		$json_string = json_encode($response, JSON_PRETTY_PRINT);
//		
//		echo $json_string;
	}
	
	public function send_price_req($countrycode){
        // load library
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');
		
		$response = $this->nexmo->get_pricing($countrycode);
		
		return $response->mt;

	}


	public function decrypt(){
		echo $this->master_model->decryptIt('J0KhMCOO/4PjW2EEYUb5iTJaTRsmPUDop8mMGYgyLKg=');
	}

    public function open()
    {
	
//		require_once 'SDK/OpenTok/OpenTok.php';
//	 
//		$apiObj = new OpenTok($API_KEY, $API_SECRET);
//		$session = $apiObj->create_session();
//		echo $session->getSessionId();	

					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));


//echo count($remind_result);

						foreach($remind_result as $rem)
						{
							echo $rem['crt_mobile'] .'<br />';
						}

	}

    public function index()
    {
        // load library
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');

        // **********************************Text Message*************************************
   /*     $to = array('63123234');
		foreach ($to as $to) {

			$from = 'Test';
			$message = array(
				'text' => 'test'
			);
			$response = $this->nexmo->send_message($from, $to, $message);

foreach ($response->messages as $messageinfo) {
  echo $messageinfo->{'message-id'}.'<br />';
  $gggg = $messageinfo->{'to'};
}


			echo "<h1>Text Message</h1>";
			$this->nexmo->d_print($response);
			echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";
			echo "<h3>to: " . $response->messages->{'to'} . "</h3>";
		}*/
		
		
		
        // *********************************Binary Message**************************************
        /*
        $from = 'xxxxxxxx';
        $to = 'xxxxxxxxxx';
        $message = array(
            'body' => 'body message',
            'udh' => 'xxxxxxx'
        );
        $response = $this->nexmo->send_message($from, $to, $message);
        echo "<h1>Binary Message</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";
        */
        // *********************************Wappush Message**************************************
        /*
        $from = 'xxxxxxxx';
        $to = 'xxxxxxxxxx';
        $message = array(
            'title' => 'xxxxxx',
            'url' => 'xxxxxxx',
            'validity' => 'xxxxxx'
        );
        $response = $this->nexmo->send_message($from, $to, $message);
        echo "<h1>Wappush Message</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

       */// *********************************Account - Get Balance*********************************
        /*$response = $this->nexmo->get_balance();
        echo "<h1>Account - Get Balance</h1>";
        echo $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";
		

		echo $response->value;*/
		
        /*/ ********************************Account - Get Pricing*********************************
        $response = $this->nexmo->get_pricing('TW');
        echo "<h1>Account - Get Pricing</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Account - Settings*********************************
        $response = $this->nexmo->get_account_settings(NULL, 'http://mlb.mlb.com', 'http://www.facebook.com');
        echo "<h1>Account - Settings</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Account - Numbers*********************************
        $response = $this->nexmo->get_numbers();
        echo "<h1>Account - Numbers</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Account - Top-up*********************************
        $trx = 'xxxxxxxxxxxxx';
        $response = $this->nexmo->get_top_up($trx);
        echo "<h1>Account - Top-up</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Number - Search*********************************
        $response = $this->nexmo->get_number_search('US', NULL);
        echo "<h1>Number - Search</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Number - Buy*********************************
        $response = $this->nexmo->get_number_buy('US', 'xxxxxxxxxxxxx');
        echo "<h1>Number - Buy</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Number - Cancel*********************************
        $response = $this->nexmo->get_number_cancel('US', 'xxxxxxxxxxxxx');
        echo "<h1>Number - Cancel</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Number - Update*********************************
        $params = array(
            'moHttpUrl' => 'http://xxxxxx'
            'moSmppSysType' => 'inbound'
        );
        $response = $this->nexmo->get_number_update('TW', 'xxxxxxxxxxxxx', null);
        echo "<h1>Number - Update</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        */// ********************************Search - Message*********************************
		
$json = <<<JSON
{ "programmers": [
  { "firstName": "Brett", "lastName":"McLaughlin" },
  { "firstName": "Jason", "lastName":"Hunter" },
  { "firstName": "Elliotte", "lastName":"Harold" }
 ],
"authors": [
  { "firstName": "Isaac", "lastName": "Asimov" },
  { "firstName": "Tad", "lastName": "Williams" },
  { "firstName": "Frank", "lastName": "Peretti" }
 ],
"musicians": [
  { "firstName": "Eric", "lastName": "Clapton" },
  { "firstName": "Sergei", "lastName": "Rachmaninoff" }
 ]
}
JSON;

$data = json_decode($json);

//var_dump($data);
		
		
        $response = $this->nexmo->search_message('050000000BCCFBE2');
        echo "<h1>Search - Message</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

		echo $response->price;
		echo $response->{'message-id'};


        /*/ ********************************Search - Messages*********************************
        $ids = array('xxxxxxxxxxxxx', 'xxxxxxxxxxxxx');
        $response = $this->nexmo->search_messages($ids);
        echo "<h1>Search - Messages</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        $response = $this->nexmo->search_messages(null, '2013-01-23', 'xxxxxxxxxxxxx');
        echo "<h1>Search - Messages</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // ********************************Search - Rejections*********************************
        $response = $this->nexmo->search_rejections('2013-01-23', 'xxxxxxxxxxxxx');
        echo "<h1>Search - Message</h1>";
        $this->nexmo->d_print($response);
        echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";
        */
    }
	
	public function read_rss($display=0,$url='') {
		$doc = new DOMDocument();
		$doc->load($url);
		$itemArr = array();
		foreach ($doc->getElementsByTagName('accountBalance') as $node) {
			if ($display == 0) {
				break;
			}
			$itemRSS = array (
				'value'       => $node->getElementsByTagName('value')->item(0)->nodeValue,
	//			'description' => $node->getElementsByTagName('description')->item(0)->nodeValue,
	//			'link'        => $node->getElementsByTagName('link')->item(0)->nodeValue,
	//			'pubdate'     => $node->getElementsByTagName('pubDate')->item(0)->nodeValue
			);
			array_push($itemArr, $itemRSS);
			$display--;
		}
		return $itemArr;
	}
	
}
/* End of file example.php */
/* Location: ./application/controllers/example.php */

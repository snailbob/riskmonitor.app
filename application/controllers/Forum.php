<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Forum extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

	
	
		$scene_id = $this->uri->segment(3);
		if($scene_id == ''){
			redirect(base_url());
		}
		
		if(is_numeric($scene_id)){
			$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));

		}
		else{

			$recall_id = substr($scene_id,1);

			$scenario = $this->master_model->getRecords('cf_recall', array ('id'=>$recall_id));
		}

		if ($scenario[0]['cc_id'] != $this->session->userdata('team_cc') && $scenario[0]['org_id'] != $this->session->userdata('cc_selected_orgnaization')){
	
			
			if ($this->session->userdata('logged_parent_cc') != ''){
				
				$this->session->set_flashdata('error','You do not have permission to access the link.');
				redirect('cc/dashboard');
				
			}
			else{
				
				$this->session->set_flashdata('error','You do not have permission to access the link.');
				redirect('crt/dashboard');
				
			}
			
		}
		
		else{
		
			$crt_login_id = $this->session->userdata('logged_crt_login_id');
			$cc_login_id = $this->session->userdata('logged_cc_login_id');
			if ( $crt_login_id != ''){
			
				//timezone datetime
				$nowinitiatetime = time();
				$timezone = $this->session->userdata('timezone');
				$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
				
				//timezone datetime
				$nowinitiatetime = time();
				$timezone = $this->session->userdata('timezone');
				$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
				
				$date = gmdate("Y-m-d H:i:s", $theinitiatetime);
						
				$insrt_arr = array(
					'forum_last_visit'=>$date
				);
				
				$this->master_model->updateRecord('cf_crisis_response_team',$insrt_arr,array('login_id'=>$crt_login_id));
				
			}
			else{
			
				//timezone datetime
				$nowinitiatetime = time();
				$timezone = $this->session->userdata('timezone');
				$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
				
				//timezone datetime
				$nowinitiatetime = time();
				$timezone = $this->session->userdata('timezone');
				$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
				
				$date = gmdate("Y-m-d H:i:s", $theinitiatetime);
				
				$insrt_arr = array(
					'forum_last_visit'=>$date
				);
				
				$this->master_model->updateRecord('cf_crisis_response_team',$insrt_arr,array('login_id'=>$cc_login_id));
			
			}
		}
		
	}

	public function charmus(){
		
	


	
	}

	public function index()

	{

		$scene_id = $this->uri->segment(3);

		//identify if scenario or incident
		if(is_numeric($scene_id)){
			$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));

		}
		else{

			$recall_id = substr($scene_id,1);

			$scenario = $this->master_model->getRecords('cf_recall', array ('id'=>$recall_id));
		}
		
		
		$fc_category = $this->master_model->getRecords('forum_category_master','','*',array('fc_id'=>'ASC'));

		
		$data=array(
			'fc_category'=>$fc_category,
			'scenario'=>$scenario,
			'success'=>''
		);

		$this->load->view('forum/forum-header',$data);
		$this->load->view('forum/forum-view',$data);
		$this->load->view('forum/forum-footer',$data);
		
		/*if ($scenario[0]['cc_id'] == $this->session->userdata('team_cc') && $scenario[0]['org_id'] == $this->session->userdata('cc_selected_orgnaization')){
	
			
			$fc_category = $this->master_model->getRecords('forum_category_master','','*',array('fc_id'=>'ASC'));

			
			$data=array(
				'fc_category'=>$fc_category,
				'success'=>''
			);
			//$this->session->set_flashdata('disp','Display message');
			$this->load->view('forum/forum-view',$data);
			
		}
		
		else{
			
			if ($this->session->userdata('logged_parent_cc') != ''){
				
				$this->session->set_flashdata('error','You do not have permission to access the link.');
				redirect('cc/dashboard');
				
			}
			else{
				
				$this->session->set_flashdata('error','You do not have permission to access the link.');
				redirect('crt/dashboard');
				
			}
		}*/

	}


	public function topics()

	{
		
		$scene_id = $this->uri->segment(3);
		$category_id = $this->uri->segment(4);
		
		//identify if scenario or incident
		if(is_numeric($scene_id)){
			$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));

		}
		else{

			$recall_id = substr($scene_id,1);

			$scenario = $this->master_model->getRecords('cf_recall', array ('id'=>$recall_id));
		}
		$fc_category = $this->master_model->getRecords('forum_category_master', array ('fc_id'=>$category_id));
		$posts = $this->master_model->getRecords('forum_post_master', array ('fp_category_id'=>$category_id, 'post_parent'=> '0','scenario_id'=>$scene_id),'*',array('date_created'=>'DESC'));
		
		$data=array(
			'fc_category'=>$fc_category,
			'posts'=>$posts,
			'scenario'=>$scenario,
			'success'=>''
		);

		$this->load->view('forum/forum-header',$data);
		$this->load->view('forum/forum-topics',$data);
		$this->load->view('forum/forum-footer',$data);


	}

	public function post()

	{
		$scene_id = $this->uri->segment(3);
		$post_id = $this->uri->segment(4);
		
		$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));
		$post = $this->master_model->getRecords('forum_post_master', array ('fp_id'=>$post_id));
		$post_reply = $this->master_model->getRecords('forum_post_master', array ('post_level'=>'1','post_parent'=>$post_id));
		
		$data=array(
			'post'=>$post,
			'scenario'=>$scenario,
			'post_reply'=>$post_reply,
			'success'=>''
		);

		$this->load->view('forum/forum-header',$data);
		$this->load->view('forum/forum-posts',$data);
		$this->load->view('forum/forum-footer',$data);

	}
	
	public function addpost()

	{
		
		$scene_id = $this->uri->segment(3);
		$category_id = $this->uri->segment(4);
		
		$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));
		$fc_category = $this->master_model->getRecords('forum_category_master', array ('fc_id'=>$category_id));
		$posts = $this->master_model->getRecords('forum_post_master', array ('fp_category_id'=>$category_id, 'post_parent'=> '0'));
		
		$data=array(
			'fc_category'=>$fc_category,
			'posts'=>$posts,
			'scenario'=>$scenario,
			'success'=>''
		);

		$this->load->view('forum/forum-header',$data);
		$this->load->view('forum/forum-addpost',$data);
		$this->load->view('forum/forum-footer',$data);

		
	}
	
	
	
	/*ajax call submit add*/
	public function addpost_submit()
	
	{
		$scenario_id = $_POST['scenario_id'];
		$topic_id = $_POST['topic_id'];
		$post_title = $_POST['post_title'];
		$post_level = $_POST['post_level'];
		$post_parent = $_POST['post_parent'];
		$reply_parent = $_POST['reply_parent'];
		$post_slogan = $_POST['post_slogan'];
		$post_content = $_POST['post_content'];
			
		if ($this->session->userdata('logged_crt_login_id') != ''){
			$author = $this->session->userdata('logged_crt_login_id');
		}
		else{
			$author = $this->session->userdata('logged_cc_login_id');
		}
		
		//timezone datetime
		$nowinitiatetime = time();
		$timezone = $this->session->userdata('timezone');
		$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
		
		$date = gmdate("Y-m-d H:i:s", $theinitiatetime);
		
		$insrt_arr = array(
			'cc_id'=>$this->session->userdata('team_cc'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
			'fp_title'=>$post_title,
			'fp_slogan'=>$post_slogan,
			'fp_content'=>$post_content,
			'fp_author_id'=>$author,
			'fp_category_id'=>$topic_id,
			'post_level'=>$post_level,
			'post_parent'=>$post_parent,
			'reply_parent'=>$reply_parent,
			'scenario_id'=>$scenario_id,
			'date_created'=>$date,
		);
		if ($post_id = $this->master_model->insertRecord('forum_post_master',$insrt_arr,TRUE)){

			if ($post_parent != '0'){
				$this->session->set_flashdata('success','Reply successfully published.');
				echo $post_parent;
			}
			else{
			$this->session->set_flashdata('success','Post successfully published.');
				echo $post_id;
			}
		}
		else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			echo 'failed';
		}
		
		
	}
	
	
	
	
	public function updatepost()
	
	{
		
		$scene_id = $this->uri->segment(3);
		$category_id = $this->uri->segment(4);
		$post_id = $this->uri->segment(5);
		
		$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));
		$fc_category = $this->master_model->getRecords('forum_category_master', array ('fc_id'=>$category_id));
		$post = $this->master_model->getRecords('forum_post_master', array ('fp_category_id'=>$category_id, 'fp_id'=> $post_id));
		
		$data=array(
			'fc_category'=>$fc_category,
			'post'=>$post,
			'scenario'=>$scenario,
			'success'=>''
		);

		$this->load->view('forum/forum-header',$data);
		$this->load->view('forum/forum-updatepost',$data);
		$this->load->view('forum/forum-footer',$data);

	}
	
	/*ajax call submit update*/
	public function updatepost_submit()
	
	{
		$scenario_id = $_POST['scenario_id'];
		$topic_id = $_POST['topic_id'];
		$post_title = $_POST['post_title'];
		$post_slogan = $_POST['post_slogan'];
		$post_parent = $_POST['post_parent'];
		$post_content = $_POST['post_content'];
		$post_id = $_POST['post_id'];
			
		if ($this->session->userdata('logged_crt_login_id') != ''){
			$author = $this->session->userdata('logged_crt_login_id');
		}
		else{
			$author = $this->session->userdata('logged_cc_login_id');
		}
		
		//timezone datetime
		$nowinitiatetime = time();
		$timezone = $this->session->userdata('timezone');
		$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
		
		$date = gmdate("Y-m-d H:i:s", $theinitiatetime);
		
		$insrt_arr = array(
			'fp_title'=>$post_title,
			'fp_slogan'=>$post_slogan,
			'fp_content'=>$post_content,
			'fp_author_id'=>$author,
			'fp_category_id'=>$topic_id,
			'scenario_id'=>$scenario_id,
			'date_updated'=>$date,
		);
		
		
		if($this->master_model->updateRecord('forum_post_master',$insrt_arr,array('fp_id'=>$post_id))){

			if ($post_parent != '0'){
				$this->session->set_flashdata('success','Reply successfully updated.');
				echo $post_parent;
			}
			else{
				$this->session->set_flashdata('success','Post successfully updated.');
				echo $post_id;
			}

		}
		else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			echo 'failed';
		}
		
		
	}	
	
	
	public function reply()
	
	{
		
		$scene_id = $this->uri->segment(3);
		$category_id = $this->uri->segment(4);
		$post_id = $this->uri->segment(5);
		$reply_parent_id = $this->uri->segment(6);
		
		$scenario = $this->master_model->getRecords('cf_scenario', array ('scenario_id'=>$scene_id));
		$fc_category = $this->master_model->getRecords('forum_category_master', array ('fc_id'=>$category_id));
		$post = $this->master_model->getRecords('forum_post_master', array ('fp_category_id'=>$category_id, 'fp_id'=> $post_id));
		
		$data=array(
			'fc_category'=>$fc_category,
			'post'=>$post,
			'scenario'=>$scenario,
			'success'=>''
		);

		$this->load->view('forum/forum-header',$data);
		$this->load->view('forum/forum-replypost',$data);
		$this->load->view('forum/forum-footer',$data);

	}	
	
	
	
	public function delete()
	
	{
		
		$scene_id = $this->uri->segment(3);
		$category_id = $this->uri->segment(4);
		$post_id = $this->uri->segment(5);
		
		$post = $this->master_model->getRecords('forum_post_master', array ('fp_category_id'=>$category_id, 'fp_id'=> $post_id));
		
		$reply_parent_id = $post[0]['post_parent'];
		
		if($post[0]['post_parent'] == '0'){

			if($this->master_model->deleteRecord('forum_post_master','fp_id',$post_id)){
				
				$this->master_model->deleteRecord('forum_post_master','post_parent',$post_id);
				$this->session->set_flashdata('success','Post thread successfully deleted.');
				
				redirect(base_url().'forum/topics/'.$scene_id.'/'.$category_id);
			}
		
		}
		else{
			
			$this->master_model->deleteRecord('forum_post_master','fp_id',$post_id);
			
			$this->session->set_flashdata('success','Your reply successfully deleted.');
			
			redirect(base_url().'forum/post/'.$scene_id.'/'.$reply_parent_id);
		
		}



	}	
	
	
}

?>
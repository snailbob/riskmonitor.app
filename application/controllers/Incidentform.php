<?php

class Incidentform extends CI_Controller

{

	public function toshare()

	{
		$cc_id = $this->uri->segment(3);

		$contents = $this->master_model->getRecords('checklist_contents',array('cc_id'=>$cc_id),'*',array('sort'=>'ASC'));
		$arr = array();

		$arr_content = array();

		if(!$cc_id){
			redirect('/');
		}

		$inci = $this->master_model->getRecords('incident_form', array('user_id'=>$cc_id));

		if(!empty($contents)){
			foreach($contents as $cr=>$cvalue){
				$whr = array(
					'content_id'=>$cvalue['id']
				);

				$email = $this->master_model->getRecords('content_emails', $whr);

				$cvalue['email'] = (!empty($email)) ? $email[0] : null;

				$sublist = $this->master_model->getRecords('content_fields', $whr);;
				$cvalue['field'] = $this->common_model->format_sublist($sublist);

				$cvalue['options'] = $this->master_model->getRecords('content_options', $whr);

				$arr_content[] = $cvalue;
			}
		}

		$data = array(
			'title'=>(count($inci)) ? $inci[0]['name'] : 'Company Incident Form',
			'page_content'=>'wow',
			'contents'=>$arr_content,
		);
		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/incident_form',$data);
		$this->load->view('frontpage/footer_view');
	}
}

?>
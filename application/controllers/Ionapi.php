<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ionapi extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('api_model');
	}
	
	
	public function check_api_key(){


		//require api key
		if(!isset($_GET['api_key'])){
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			$allusers = array(
				'result'=>'error',
				'message'=>'No api key'
			);
			echo json_encode($allusers, JSON_PRETTY_PRINT);
			return false;
		}
		else{
			if($_GET['api_key'] != 'insurance101'){ //password is 
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				$allusers = array(
					'result'=>'error',
					'message'=>'Wrong api key'
				);
				echo json_encode($allusers, JSON_PRETTY_PRINT);
				return false;
			}
		}	   
	   	
	}

	public function resetpass() {
		
		//$this->check_api_key();
		
		$data['result'] = 'error';
		
		if(isset($_POST['emailadd'])){
			$emailadd = $_POST['emailadd'];
			$data = array(
				'email_id'=>$this->master_model->encryptIt($emailadd)
			);
			$userdata = $this->master_model->getRecords('cf_login_master', $data);
			
			if(count($userdata) > 0){

				$adminemail = $this->master_model->getRecords('admin_login', array('id'=>'1'));

				//generate random password

				$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

				$pass = array(); //remember to declare $pass as an array

				$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

				for ($i = 0; $i < 8; $i++) {

					$n = rand(0, $alphaLength);
					$pass[] = $alphabet[$n];
					
				}

				$random_pass = implode($pass);
				
				
				$arr = array(
					'pass_word'=>md5($random_pass)
				);
				
				$this->master_model->updateRecord('cf_login_master',$arr, array('login_id'=>$userdata[0]['login_id']));

				

				$info_arr = array(
					'from'=>$adminemail[0]['recovery_email'],
					'to'=>$emailadd,
					'subject'=>'Password Recovery',
					'view'=>'forget-password-mail-to-cc'
				);


				$other_info = array(
					'password'=>$random_pass,
					'email'=>$emailadd,
					'first_name'=>'',//$cc_name[0]['cc_first_name'],
					'last_name'=>''//$email_id[0]['cc_last_name']
				);

				$this->email_sending->sendmail($info_arr,$other_info);
				
				$data['count'] = count($userdata);
				$data['result'] = 'success';
				
			}
			
		}

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	
	
	public function usergroup(){
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$access_format = (isset($_POST['access_format'])) ? $_POST['access_format'] : array();
		$group_name = (isset($_POST['group_name'])) ? $_POST['group_name'] : '';
		$group_id = (isset($_POST['group_id'])) ? $_POST['group_id'] : '';
	
		$data = array();
		
		//check if add/update
		if(count($access_format) > 0){
			//format access
			$access = array();
			foreach($access_format as $r=>$value){
				if($value['enabled'] == 'true'){
					$access[] = $value['type'];
				}
			}
			$data['access'] = $access;
			
			$arr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'group_name'=>$group_name,
				'access'=>serialize($access)
			);
			
			if($group_id != ''){
				$this->master_model->updateRecord('user_group', $arr, array('id'=>$group_id));
			}
			else{
				$this->master_model->insertRecord('user_group', $arr);
			}
		
		}
		
		
		
		$groups = $this->master_model->getRecords('user_group', array('cc_id'=>$cc_id, 'org_id'=>$org_id));
		$data['access_format'] = $access_format;
		$data['group_name'] = $group_name;
		$data['group_id'] = $group_id;
		$data['group'] = $this->common_model->format_group($groups);
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function mysession(){
		$arr = $this->session->all_userdata();
		echo json_encode($arr);
	}
	
	public function crts(){
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		
		$data = array();
		
//		$form_inputs = array(
//			'alt_member'=>"asdf",
//			'country_id'=>"174",
//			'crisis_function'=>"asdf",
//			'crt_digits'=>"9369830471",
//			'crt_email'=>"a4@a.com",
//			'crt_first_name'=>"adsfs",
//			'crt_last_name'=>"sadf",
//			'crt_position'=>"asdf",
//			'group'=>"9"
//		);

		if(isset($_POST['form_input'])){ //isset($form_inputs['group'])){ //
			$form_input = $_POST['form_input']; // $form_inputs; //
			$newcrt = $this->api_model->newcrt($cc_id, $org_id, $form_input);
			$data['newcrt'] = $newcrt;
		}		
		
		
		//get crts
		$the_org_crts = $this->common_model->get_org_crts($cc_id, $org_id, '5'); //, 'add_cc'

		$data['crts'] = $the_org_crts; // $crts;
		
		$countries = $this->common_model->countryformat();
	
		$access_format = (isset($_POST['access_format'])) ? $_POST['access_format'] : array();
		$group_name = (isset($_POST['group_name'])) ? $_POST['group_name'] : '';
		$group_id = (isset($_POST['group_id'])) ? $_POST['group_id'] : '';

		$groups = $this->master_model->getRecords('user_group', array('cc_id'=>$cc_id, 'org_id'=>$org_id));
		$data['access_format'] = $access_format;
		$data['group_name'] = $group_name;
		$data['group_id'] = $group_id;
		$data['countries'] = $countries;
		$data['group'] = $this->common_model->format_group($groups);
		
		
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function addchat(){
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$text = (isset($_POST['text'])) ? $_POST['text'] : 'fdsf';

		$chats = $this->api_model->process_chat($org_id, 'send', $text);
	
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($chats, JSON_PRETTY_PRINT);
	}
	
	public function onlines(){

		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';

		$data = $this->api_model->onlines($cc_id, $login_id, $org_id);
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	
	public function stks(){
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		
		//get crts
		$stks = $this->common_model->get_org_stks($cc_id, $org_id);
		
		$data['stks'] = $stks;
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	}
	
	public function docs(){
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		
		
		$data = $this->common_model->get_org_documents($cc_id, $org_id, $login_id);

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	}

	public function userdetails() {
		
		//$this->check_api_key();
		
		$data['result'] = 'error';
		
		$user_level = $_POST['user_level'];
		$login_id = $_POST['login_id'];
		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
		
		
		if(isset($_POST['mobileno'])){
		
			$mobileno = $_POST['mobileno'];
			$address = $_POST['address'];
			$city = $_POST['city'];
			$state = $_POST['state'];
			$zip = $_POST['zip'];
			
			
			$data = array(
				'crt_digits'=>$this->master_model->encryptIt($mobileno),
				'crt_address'=>$this->master_model->encryptIt($address),
				'crt_city'=>$this->master_model->encryptIt($city),
				'crt_state'=>$this->master_model->encryptIt($state),
				'crt_zip_code'=>$this->master_model->encryptIt($zip),
			);
			
			$this->master_model->updateRecord('cf_crisis_response_team',$data, array('login_id'=>$login_id));

			$data['result'] = 'success';
			$data['mobileno'] = $mobileno;

			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
			return false;
		}//end update details
		
		
		$user_details = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
		$more_details = $this->master_model->getRecords('cf_login_master', array('login_id'=>$login_id));
		
		if(count($user_details) > 0){
			foreach($user_details as $r=>$value){
				$the_user = array(
					'first_name'=>$this->master_model->decryptIt($value['crt_first_name']),
					'crt_last_name'=>$this->master_model->decryptIt($value['crt_last_name']),
					'crt_organisation'=>$value['crt_organisation'],
					'crt_position'=>$value['crt_position'],
					'crt_email'=>$this->master_model->decryptIt($value['crt_email']),
					'location'=>$value['location'],
					'countrycode'=>$this->master_model->decryptIt($value['countrycode']),
					'crt_digits'=>$this->master_model->decryptIt($value['crt_digits']),
					'crt_mobile'=>$this->master_model->decryptIt($value['crt_mobile']),
					'crisis_function'=>$value['crisis_function'],
					'alt_member'=>$value['alt_member'],
					'crt_address'=>$this->master_model->decryptIt($value['crt_address']),
					'crt_city'=>$this->master_model->decryptIt($value['crt_city']),
					'crt_state'=>$this->master_model->decryptIt($value['crt_state']),
					'crt_zip_code'=>$this->master_model->decryptIt($value['crt_zip_code']),
					'crt_availability'=>$value['crt_availability'],
					'timezone'=>$value['timezone'],
					'activated'=>$value['activated'],
					'date_activated'=>$value['date_activated'],
					'tasks_review_date'=>$value['tasks_review_date'],
					'forum_last_visit'=>$value['forum_last_visit'],
					'user_type'=>$more_details[0]['user_type'],
					'user_level'=>$more_details[0]['user_level'],
					'user_status'=>$more_details[0]['user_status'],
					'contact_verified'=>$more_details[0]['contact_verified'],
					'received_sms'=>$more_details[0]['received_sms']
				);		
			}
			
			$data['result'] = 'success';
		}
		$data['user'] = $the_user;
		
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);

	}
	public function login() {
		
		//$this->check_api_key();
		
		$username = (isset($_POST['username'])) ? $_POST['username'] : 'snailbob01@gmail.com';
		$password = (isset($_POST['password'])) ? $_POST['password'] : 'Darknite1';
		$module = (isset($_POST['module'])) ? $_POST['module'] : '5';
		$chk_arr = array(
			'email_id'=>$this->master_model->encryptIt($username),
			//'pass_word'=>md5($password)
		);

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');

		$row = $this->master_model->getRecords('cf_login_master as login',$chk_arr);


		$data = array(
			'email'=>$username
		);
		
		
		//check if not empty
		if(count($row) > 0) {
			
			if($row[0]['pass_word'] == md5($password)){
			
			
				//check if module active
				$checkorg = $this->master_model->getRecords('organization_master',array('cc_id'=>$row[0]['cc_id']));
				
				if(strpos($checkorg[0]['active_module'], $module) === false){//check module in org
					$data['result'] = 'no_module';
			
					header('Content-Type: application/json');
					header('Access-Control-Allow-Origin: *');
					echo json_encode($data, JSON_PRETTY_PRINT);
					return false;
				}
				
				$data['login_id'] = $row[0]['login_id'];
				$the_orgs = array();
				//set user data
	
				$userinfo = array(
					'login_id'=>$row[0]['login_id'],
					'user_email'=>$this->master_model->decryptIt($row[0]['crt_email']),
					'team_cc'=>$row[0]['cc_id'],
					'timezone'=>$row[0]['timezone'],
					'country_id'=>$row[0]['country_id'],
					'forum_last_visit'=>$row[0]['forum_last_visit'],
					'location'=>$row[0]['location'],
					'user_type'=>$row[0]['user_type'],
					'full_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
					'name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
					'first_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']),
					'last_name'=>$this->master_model->decryptIt($row[0]['crt_last_name']),
					'user_level'=>$row[0]['user_level'],
					'task_review_date'=>$row[0]['tasks_review_date']
	
				);
	
				if($row[0]['user_level'] == 0) {
					$whr_org = array(
						'organization_id'=>$row[0]['default_org']
					);
					$name_of_org = $this->master_model->getRecords('organization_master',$whr_org,'*');
					
					
					$data['result'] = 'OK';
					$data['usertype_name'] = 'cc';
					$data['usertype']=$row[0]['user_level'];
					$data['consultant_id'] = '';//for crt consultant
					
					$userinfo['org_id'] = $row[0]['default_org'];
					$userinfo['org_name'] = $name_of_org[0]['organization_name'];
					$userinfo['org_info'] = $this->common_model->org_details($row[0]['default_org']);
					
				}//end if cc
				else{
					
					$data['result'] = 'OK';
					$data['usertype_name'] = 'crt';
					$data['usertype']=$row[0]['user_level'];
					
					//check if a consultant
					$resellers = $this->master_model->getRecords('reseller_master', array('email_id'=>$row[0]['crt_email']));
					
					if(count($resellers) > 0){
	
						$cons_orgs = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$row[0]['login_id']));
						
						$theconorg = unserialize($cons_orgs[0]['org_list']);
						
						
						$the_orgs[] = array(
							'org_id'=>$cons_orgs[0]['org_id'],
							'org_name'=>ucfirst($cons_orgs[0]['crt_organisation'])
						);
						
										
						//select one occuring org
						$curr_org = '';
						if(count($theconorg) > 0){
							foreach($theconorg as $info){
								if($curr_org != $info['org_id']){
									if($info['active_module'] == '5'){ //check if recall module
										
										$the_orgs[] = array(
											'org_id'=>$info['org_id'],
											'org_name'=>ucfirst($this->common_model->getorgname($info['org_id']))									
										);
									}
										
								}
								
								$curr_org = $info['org_id'];
	
							}
						}
	
						$data['consultant_id'] = $resellers[0]['login_id'];//for crt consultant
						
						$userinfo['org_id'] = '';
						$userinfo['org_name'] = '';
							
	
					}//end check count of resellers
							
					else{//a crt not a consultant
						$whr_org = array(
							'organization_id'=>$row[0]['org_id']
						);
						$name_of_org = $this->master_model->getRecords('organization_master',$whr_org,'*');
				
						
						$data['consultant_id'] = '';//for crt consultant
						
						$userinfo['org_id'] = $row[0]['org_id'];
						$userinfo['org_name'] = $name_of_org[0]['organization_name'];
						$userinfo['org_info'] = $this->common_model->org_details($row[0]['org_id']);
					}
				
				}//end if crt
				
				
				
				$data['user'] = $userinfo;
				$data['my_orgs'] = $the_orgs;
				
			}
			else{
				$data['result'] = 'wrong_pass'; 
			}
		}//end of check if not empty
		else{
			$data['result'] = 'not_exist'; 
		}
		
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
		
	}
	
	
	public function changepassword(){

		$user_level = $_POST['user_level'];
		$login_id = $_POST['login_id'];
		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
		
		$data['result'] = 'err';
		
		
		if(isset($_POST['pass'])){
			$password = $_POST['pass'];

			$this->master_model->updateRecord('cf_login_master', array('pass_word'=>md5($password)), array('login_id'=>$login_id));
			
			$data['result'] = 'success'; 
			$data['login_id'] = $login_id; 
			
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
			
		}	
	}
	
	public function updateprofile(){
		$user_level = $_POST['user_level'];
		$login_id = $_POST['login_id'];
		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
	
	
		if(isset($_POST['info'])){
			$user_info = $_POST['info'];
			$data['user_info'] = $user_info;
			
			$country_calling = $this->api_model->db_field_id('country_t', 'calling_code', $user_info['country_id'], 'country_id');
			$crt_digits = ltrim($user_info['crt_digits'], '0');
			$crt_mobile = $country_calling.$crt_digits;
			$data_array = array(
				'country_id'=>$user_info['country_id'],
				'countrycode'=>$this->master_model->encryptIt($country_calling),
				'crt_digits'=>$this->master_model->encryptIt($crt_digits),
				'crt_mobile'=>$this->master_model->encryptIt($crt_mobile),
				'crt_address'=>$this->master_model->encryptIt($user_info['crt_address']),
				'crt_city'=>$this->master_model->encryptIt($user_info['crt_city']),
				'crt_state'=>$this->master_model->encryptIt($user_info['crt_state']),
				'crt_state'=>$this->master_model->encryptIt($user_info['crt_state']),
				'crt_zip_code'=>$this->master_model->encryptIt($user_info['crt_zip_code']),
				'crt_position'=>(isset($user_info['crt_position'])) ? $user_info['crt_position'] : ''
			);
			
			$this->master_model->updateRecord('cf_crisis_response_team', $data_array, array('login_id'=>$login_id));
			
			$data['user_info_crypted'] = $data_array;
						
		}
	
		$personal_info = $this->api_model->user_format($login_id);
	
		$countries = $this->common_model->countryformat();
		
		$data['info'] = $personal_info[0];
		$data['countries'] = $countries;
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	
	
	public function settimezone(){
		$user_level = $_POST['user_level'];
		$login_id = $_POST['login_id'];
		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
		
		
		if(isset($_POST['timezone'])){
			$tz = $_POST['timezone'];
			
			$arrr = array(
				'timezone'=>$tz,
				'timezone_updated'=>date('Y-m-d H:i:s')
				
			);
			
			if($this->master_model->updateRecord('cf_crisis_response_team', $arrr, array('cc_id'=>$cc_id))) {

				$this->master_model->updateRecord('organization_master', array('timezone_set'=>'1'), array('organization_id'=>$org_id));
				
			}
			
			$data['tz'] = $arrr;			
			
		}
	
		$personal_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
		$timezones = $this->master_model->getRecords('time_zone');
	
	
		$data['timezones'] = $timezones;
		$data['info'] = $personal_info[0];
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	
	}
	
	
	public function taskupdate(){
	
		
		//$this->check_api_key();
		
		if (isset($_POST['id'])){
			$task_id = $_POST['id'];
			$actiontype = $_POST['actiontype'];
			$assignto = $_POST['assignto'];
			$cc_id = $_POST['cc_id'];
			$login_id = $_POST['login_id'];
			$org_id = $_POST['org_id'];
			
			$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
			$task_info = $this->master_model->getRecords('cf_recall_steps', array('id'=>$task_id));
			$recall_info = $this->master_model->getRecords('cf_recall', array('id'=>$task_info[0]['recall_id']));
			
			$nowtime = $this->common_model->usertime($user_info[0]['timezone']);
			//check if pickup task
			if($actiontype == 'pickup'){
				
				//if was updated
				if($task_info[0]['assigned'] != ''){
					$data['result'] = 'was_updated';
					header('Content-Type: application/json');
					header('Access-Control-Allow-Origin: *');
					echo json_encode($data, JSON_PRETTY_PRINT);
				}
				
				$arr = array(
					'assigned'=>$assignto,
					'date_assigned'=>$nowtime
				);
				
				$this->master_model->updateRecord('cf_recall_steps', $arr, array('id'=>$task_id));
				
			}
			
			//if assign
			else if ($actiontype == 'assign'){
				
				$arr = array(
					'assigned'=>$assignto,
					'date_assigned'=>$nowtime
				);
				
				$this->master_model->updateRecord('cf_recall_steps', $arr, array('id'=>$task_id));
			
			}
			
			//if mark task completed
			else if ($actiontype == 'complete'){
			
					
				//check if the task has been marked completed
				if($task_info[0]['status'] == '1'){
					$data['result'] = 'was_updated';
					header('Content-Type: application/json');
					header('Access-Control-Allow-Origin: *');
					echo json_encode($data, JSON_PRETTY_PRINT);

					return false;
				}
				else{
				
				
					$initiation_date = new DateTime($recall_info[0]['initiation_date']);
	
					$task_completed = new DateTime($nowtime);
					$difference = $initiation_date->diff($task_completed);
			
					$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i; //.' '.$difference->s.' ';
					
					
					$task_tot = explode(' ',$interval);
					$unit_time = array(
						'Y : ',
						'Mo : ',
						'D : ',
						'H : ',
						'M' // : ',
					//	'S'
					);
					$u = 0; 
					$time_lapse = '';
					foreach($task_tot as $tot){
						if ($tot == 0 ){
						}
						else if ($tot == 1 ){
							$time_lapse .= $tot.' '.$unit_time[$u].' ';
						}
						else{
							$time_lapse .= $tot.' '.$unit_time[$u].' ';
						}
						$u++;
					}
						
					$arr = array(
						'status'=>'1',
						'date_completed'=>$nowtime,
						'time_lapse'=>$time_lapse
						
					);
										
	
					$this->master_model->updateRecord('cf_recall_steps',$arr,array('id'=>$task_id));
				
				
				
				}

			}
			
			//if task mark incomplete
			
			else if($actiontype == 'incomplete'){
				//check if the task has been marked completed
				if($task_info[0]['status'] == '0'){
					$data['result'] = 'was_updated';
					header('Content-Type: application/json');
					header('Access-Control-Allow-Origin: *');
					echo json_encode($data, JSON_PRETTY_PRINT);

					return false;
				}
			
				$this->master_model->updateRecord('cf_recall_steps',array('status'=>'0'),array('id'=>$task_id));

			}
			
			
			$data['result'] = 'success';
			$data['task_id'] = $task_id;
			$data['actiontype'] = $actiontype;
			$data['assignto'] = $assignto;
			$data['cc_id'] = $cc_id;
			$data['org_id'] = $org_id;
			
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
		}
		else{
			$data['result'] = 'no_data';
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
		}
		
		
	}
	public function notification(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '1';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '1';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '2';

		$inci_name = 'recall';

		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type'=>$inci_name
		);
		
		$logs = $this->master_model->getRecords('bulk_notification', $whr, '*', array('date_added'=>'DESC'));
		$data['logs'] = $logs;
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	}
	
	public function bulknoti(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$bulk_id = (isset($_POST['bulk_id'])) ? $_POST['bulk_id'] : '48';
		$active_module = '5';
		
		$data = $this->common_model->bulknoti($cc_id, $org_id, $bulk_id, $active_module);
				
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function topup(){
		$amount = (isset($_POST['amount'])) ? $_POST['amount'] : '2';
		$nexmo_balance = (isset($_POST['nexmo_balance'])) ? $_POST['nexmo_balance'] : '1';
		
		$user_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		
		$sms_credit = $org_info[0]['sms_credit'] + $amount;
		$waiting_credit = $org_info[0]['waiting_sms_credit'] + $amount;
		
		$stripe_customer_id = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		
		$stripe_cust = $stripe_customer_id[0]['stripe_customer_id'];
		$data['result'] = 'error';

		//get validation message from admin - generic delete id = 63
		$action_message_id = '68';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '73';
		$action_message2 = $this->common_model->get_message($action_message_id2);


		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0 ");
		
		if($stripe_cust != '0'){
			try{
				Stripe_Charge::create(array(
				  "amount" => $amount*100,
				  "currency" => "usd",
				  "customer" => $stripe_cust, // obtained with Stripe.js
				  "description" => "purchase sms credit"
				));
				
					
				//store if nexmo balance larger than purchased
				if($nexmo_balance > $amount){
					
					$this->session->set_flashdata('success', $action_message['success']);
					
					$this->master_model->updateRecord('organization_master', array('sms_credit'=>$sms_credit), array('organization_id'=>$org_id));
					
				}
				else{
					
					$this->session->set_flashdata('success', $action_message2['success']);
					$this->master_model->updateRecord('organization_master', array('waiting_sms_credit'=>$waiting_credit), array('organization_id'=>$org_id));
				
				}
				
				$data['result'] = 'success';
				
			}
			
			catch(Stripe_CardError $e){
				$data['result'] = 'declined';
			}
		}//end customer exist
		
		else{
			$data['result'] = 'notcustomer';
		}//end not a stripe customer


		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data);
	}
		

	public function sessions(){
		$sess = $this->session->all_userdata();
		echo json_encode($sess);
	}

	public function remindcrt(){

		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '1';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '1';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '2';

		$whr_arr = array('login.user_status'=>'1','crt.cc_id'=>$cc_id,'crt.org_id'=>$org_id,'crt.login_id !='=>$cc_id);

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$remind_result = $this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr);



		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '87';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$data['message'] = $action_message['error'];
		if(count($remind_result) > 0 ){

			foreach($remind_result as $rem){

				$whr = array('id'=>'1');

				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');

				

				$info_arr=array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->master_model->decryptIt($rem['crt_email']),
					'subject'=>'CrisisFlo update request',
					'view'=>'contact-information-mail-to-crt'
				);

				

				 $other_info = array(
				 	'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
					'onetime_key'=>$rem['onetime_key'],
					'cc_name'=>$cc_id
				);

				//print_r($other_info); 

				if($this->email_sending->sendmail($info_arr,$other_info)){

					$update_array = array('contact_verified'=>'2');

					$this->master_model->updateRecord('cf_login_master',$update_array,array('onetime_key'=>'"'.$rem['onetime_key'].'"'));

				}

			}
			
			$data['message'] = $action_message['success'];


		}

		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	

	public function remindstk(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '1';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '1';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '2';

		$whr_arr = array(
			'login.user_status'=>'1',
			'stk.cc_id'=>$cc_id,
			'stk.org_id'=>$org_id
		);

		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '88';
		$action_message = $this->common_model->get_message($action_message_id);

		$this->db->join('cf_login_master as login','login.login_id=stk.login_id');

		$remind_result=$this->master_model->getRecords('cf_stakeholder as stk',$whr_arr);


		$data['message'] = $action_message['error'];
		
		if(count($remind_result) > 0 ) {

			foreach($remind_result as $rem){  

				$whr = array('id'=>'1');

				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');

				

				$info_arr = array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->master_model->decryptIt($rem['stk_email_address']),
					'subject'=>'CrisisFlo update request',
					'view'=>'contact-information-mail-to-stk'
				);

				

				 $other_info = array( 
				 	'name'=>$this->master_model->decryptIt($rem['stk_first_name']).' '.$this->master_model->decryptIt($rem['stk_last_name']),
					'onetime_key'=>$rem['onetime_key'],
					'cc_name'=>$this->session->userdata('logged_cc_login_id')
				);

				//print_r($info_arr); 

				if($this->email_sending->sendmail($info_arr,$other_info)){

					$update_array = array('contact_verified'=>'2');

					$this->master_model->updateRecord('cf_login_master',$update_array,array('onetime_key'=>'"'.$rem['onetime_key'].'"'));

				}

			}


			$data['message'] = $action_message['success'];
		}
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}

		
		
	
	
	public function unreadforum(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '1';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '1';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '2';


		$topics = $this->common_model->forumtopics($cc_id, $org_id, $login_id);		
		$data['topics'] = $topics;
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	}
	
	public function singletopic(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '1';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '1';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '2';
		$topic_id = (isset($_POST['topic_id'])) ? $_POST['topic_id'] : '32';

		$whr = array(
//			'cc_id'=>$cc_id,
//			'org_id'=>$org_id,
			'fp_id'=>$topic_id
		);
		
		$forum_post = $this->master_model->getRecords('forum_post_master', $whr);
		$post = (count($forum_post) > 0) ? $forum_post[0] : array();
		$topics = $this->common_model->topic_wreply_format($post);		
		$data['topics'] = $topics;
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	}
	
	
	public function topics(){
		//$this->check_api_key();

		$recall_id = (isset($_POST['recall_id'])) ? $_POST['recall_id'] : '34';
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		
		
		//if add new reply
		if(isset($_POST['post_title'])){
			$post_title = $_POST['post_title'];
			$post_content = $_POST['post_content'];
			$post_cat = $_POST['post_cat'];
			
			$data['post_title'] = $post_title;
			$data['post_content'] = $post_content;
			$data['post_cat'] = $post_cat;
			
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
			$nowtime = $this->common_model->usertime($user_info[0]['timezone']);
			
			$insrt_arr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'fp_title'=>$post_title,
				'fp_content'=>$post_content,
				'fp_author_id'=>$login_id,
				'fp_category_id'=>$post_cat,
				'post_level'=>'0',
				'post_parent'=>'0',
				'reply_parent'=>'0',
				'scenario_id'=>'r'.$recall_id,
				'date_created'=>$nowtime,
			);
			$data['action'] = 'add_post';
			$data['status'] = 'success';
			$this->master_model->insertRecord('forum_post_master',$insrt_arr);
			
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
			return false;
		}
		
		
		//get forum category
		
		$forumcategories = $this->master_model->getRecords('forum_category_master', array('parent_category_id'=>'0'));
		
		$forumcat = array();
		
		foreach($forumcategories as $r=>$value){
			$forumcat[] = array(
				'id'=>$value['fc_id'],
				'name'=>$value['fc_name']
			);
		}
		
		$data['categories'] = $forumcat;
		
		
		
		//get topics
		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'id'=>$recall_id
		);
		
		$incident = $this->master_model->getRecords('cf_recall', $whr_inci);
		
		
		if($incident[0]['initiation_type'] == '1'){
			$inci_class = 'text-primary';
		}
		else{
			$inci_class = 'text-muted';
		}
		
		$data['incident'] = array(
			'name'=>$incident[0]['incident_no'].': '.$incident[0]['incident_name'],
			'class'=>$inci_class
		);

		
		//get count forum topics
		$whr_topic = array(
			'post_level'=>'0'
		);
		
		
		//if single topic is set
		
		if(isset($_POST['topic_id'])){
			$topic_id = $_POST['topic_id'];
			$whr_topic['fp_id'] = $topic_id; 
			
				
		}//end of single topics
		
		$topics = $this->master_model->getRecords('forum_post_master', $whr_topic, '*', array('fp_id'=>'DESC'));
		
		
		

		$topic_count = 0;
		
		$thetopics = array();
		
		if(count($topics) > 0){
			
			foreach($topics as $trow=>$tval){
				if(substr($tval['scenario_id'], 1) == $recall_id){
					$reply_count = $this->master_model->getRecordCount('forum_post_master', array('post_level'=>'1', 'post_parent'=>$tval['fp_id']));
					$thetopics[] = array(
						'id'=>$tval['fp_id'],
						'recall_id'=>$recall_id,
						'title'=>$tval['fp_title'],
						'content'=>strip_tags($tval['fp_content']),
						'author_id'=>$tval['fp_author_id'],
						'author_name'=>$this->common_model->getname($tval['fp_author_id']),
						'category_id'=>$tval['fp_category_id'],
						'category_name'=>$this->common_model->forumcategory($tval['fp_category_id']),
						'reply_count'=>$reply_count,
						'date_created'=>date_format(date_create($tval['date_created']), 'M jS Y, g:ia')
						
					);
					$topic_count++;
				}
			}
		}

		$data['topic_count'] = $topic_count;
		$data['topics'] = $thetopics;
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	
	

	public function forumreply(){
		//$this->check_api_key();

		$recall_id = $_POST['recall_id'];
		$topic_id = $_POST['topic_id'];
		$user_level = $_POST['user_level'];
		$login_id = $_POST['login_id'];
		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
		
		$the_topic = $this->master_model->getRecords('forum_post_master', array('fp_id'=>$topic_id));
		$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
		$nowtime = $this->common_model->usertime($user_info[0]['timezone']);

		//if add new reply
		if(isset($_POST['reply_text'])){
			$reply_text = $_POST['reply_text'];
			$data['reply_text'] = $reply_text;
			
			
			
			$insrt_arr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'fp_content'=>$reply_text,
				'fp_author_id'=>$login_id,
				'fp_category_id'=>$the_topic[0]['fp_category_id'],
				'post_level'=>'1',
				'post_parent'=>$topic_id,
				'reply_parent'=>'0',
				'scenario_id'=>'r'.$recall_id,
				'date_created'=>$nowtime,
			);
			$data['action'] = 'add_reply';
			$data['status'] = 'success';
			$this->master_model->insertRecord('forum_post_master',$insrt_arr);
			
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
			return false;
		}
		

		//get count forum replies
		$whr_topic = array(
			'post_level'=>'1',
			'post_parent'=>$topic_id
		);
		
		
		$topics = $this->master_model->getRecords('forum_post_master', $whr_topic, '*', array('fp_id'=>'DESC'));
		
		$reply_count = 0;
		
		$thereplies = array();
		
		if(count($topics) > 0){
			
			foreach($topics as $trow=>$tval){
				if(substr($tval['scenario_id'], 1) == $recall_id){

					$thereplies[] = array(
						'id'=>$tval['fp_id'],
						'recall_id'=>$recall_id,
						'title'=>$tval['fp_title'],
						'content'=>$tval['fp_content'], //strip_tags($tval['fp_content']),
						'author_id'=>$tval['fp_author_id'],
						'author_name'=>$this->common_model->getname($tval['fp_author_id']),
						'category_id'=>$tval['fp_category_id'],
						'category_name'=>$this->common_model->forumcategory($tval['fp_category_id']),
						'date_created'=>date_format(date_create($tval['date_created']), 'jS M Y, g:ia')
						
					);
					$reply_count++;
				}
			}
		}
		$data['topic_id'] = $topic_id;
		$data['reply_count'] = $reply_count;
		$data['replies'] = $thereplies;

		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	
	}	
	
	
	
	
	public function upload(){
		//$this->check_api_key();

		$task_id = $_POST['task_id'];
		$recall_id = $_POST['recall_id'];
		$user_level = $_POST['user_level'];
		$login_id = $_POST['login_id'];
		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
		
		
		$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
		$nowtime = $this->common_model->usertime($user_info[0]['timezone']);

		$output_dir = "uploads/recallsteps-uploads/";
		
		$thefilename = uniqid().str_replace(' ', '_', $nowtime.'.jpg');
		$thefilename = str_replace(':', '_', $thefilename);
		
		if(isset($_FILES["file"])){

			//Filter the file types , if you want.
			if ($_FILES["file"]["error"] > 0)
			{
			   $data['result'] = 'error';
			}
			else
			{
				
				
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir.$thefilename);
				
				
				
				$arr = array(
					'message_id'=>$task_id,
					'file_upload_name'=>$thefilename,
					'cc_id'=>$cc_id,
					'org_id'=>$org_id
				);
				
			    $data['result'] = 'success';
			    $data['file_url'] = base_url().'uploads/recallsteps-uploads/'.$thefilename; //http://192.168.254.102/nguyen/crisisflo.com/secure/
		
				$this->master_model->insertRecord('cf_recall_steps_file', $arr);
				
			}
		}
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		
		echo base_url().'uploads/recallsteps-uploads/'.$thefilename; //json_encode($data, JSON_PRETTY_PRINT); http://192.168.254.102/nguyen/crisisflo.com/secure/
		
	}
	
	public function tokbox(){
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		 $data = '{"rid":"cordova","sid":"2_MX40NDQ0MzEyMn5-MTQyNDgxMTI1MTg1MX54TG44MXBwNGVXT2o1aFVDQzNXSXEwSWd-fg","sessionId":"2_MX40NDQ0MzEyMn5-MTQyNDgxMTI1MTg1MX54TG44MXBwNGVXT2o1aFVDQzNXSXEwSWd-fg","apiKey":"44443122","token":"T1==cGFydG5lcl9pZD00NDQ0MzEyMiZzaWc9YjZmMWQzZWRlZmQ1ZmQ3Y2RhODQwNmNmYzcwZDgyMDJlNzY5MTMzNzpzZXNzaW9uX2lkPTJfTVg0ME5EUTBNekV5TW41LU1UUXlORGd4TVRJMU1UZzFNWDU0VEc0NE1YQndOR1ZYVDJvMWFGVkRRek5YU1hFd1NXZC1mZyZjcmVhdGVfdGltZT0xNDI2MDY0MDA4Jm5vbmNlPTAuOTU5OTUzNTU3MjM0MjU3NSZyb2xlPW1vZGVyYXRvciZleHBpcmVfdGltZT0xNDI2MTUwNDA4"}';
		 
		echo $data ;
	}
	
		
	public function blockers() {
		//$this->check_api_key();
		$data = array();
	
		$recall_id = (isset($_POST['recall_id'])) ? $_POST['recall_id'] : '38';
		if ($recall_id != ''){
			$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : '0';
			$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
			$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
			$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		
			
			//get blockers 
			$bb = array(
				'cc_id'=>$cc_id,
				'recall_id'=>$recall_id
			);
			
			$bb_unresolved = array(
				'cc_id'=>$cc_id,
				'recall_id'=>$recall_id,
				'status'=>'0'
			);

			
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
			$nowtime = $this->common_model->usertime($user_info[0]['timezone']);
			
			
			//if add note
			if(isset($_POST['blocker_id']) && isset($_POST['note'])){
				$data['action'] = 'add_note';
				$data['status'] = 'success';

				$this->master_model->updateRecord('cf_recall_blockers', array('note'=> $_POST['note']), array('id'=>$_POST['blocker_id']));
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
				
			}
			
			//if add new blocker
			if(isset($_POST['issue']) && isset($_POST['impact'])){
				$data['action'] = 'add_blocker';
				$data['status'] = 'success';
				
				$arr = array(
					'cc_id'=>$cc_id,
					'crt_id'=>$login_id,
					'recall_id'=>$recall_id,
					'blocker'=>$_POST['blocker'],
					'issue'=>$_POST['issue'],
					'impact'=>$_POST['impact'],
					'step_no'=>$_POST['step_no'],
					'task_id'=>$_POST['task'],
					'assigned'=>$_POST['assigned'],
					'date_created'=>$nowtime
				);
				
				$this->master_model->insertRecord('cf_recall_blockers', $arr);
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
				
			}
			
			
			//if update status
			if(isset($_POST['blockers_id']) && isset($_POST['status'])){
				$data['action'] = 'update_status';
				$data['status'] = 'success';
				
				if($_POST['status'] == '0'){
					 $this->master_model->updateRecord('cf_recall_blockers', array('status'=> '1','date_resolved'=>$nowtime), array('id'=>$_POST['blockers_id']));
				}
				else{
					 $this->master_model->updateRecord('cf_recall_blockers', array('status'=> '0'), array('id'=>$_POST['blockers_id']));
				}
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
				
			}
			
			//if each blocker id is set
			if(isset($_POST['blocker_id'])){
				$bb['id'] = $_POST['blocker_id'];
			}
			
			$blockers = $this->master_model->getRecords('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));
			$unresolved_blockers = $this->master_model->getRecords('cf_recall_blockers',$bb_unresolved,'*',array('id'=>'DESC'));
			
			$the_blockers = array();
			
			if(count($blockers) > 0){
				foreach($blockers as $r=>$value){
					$status_name = 'Unresolved';
					$status_class = 'badge-negative';
					$btn_class = 'btn-positive';
					$btn_text = 'Mark as Resolved';
					$btn_fa = 'Opener';
					$date_resolved = '';
					if($value['status'] == '1'){
						$status_name = 'Resolved';
						$status_class = 'badge-positive';
						$btn_class = 'btn-negative';
						$btn_text = 'Mark as Unresolved';
						$btn_fa = 'Closer';
						$date_resolved = date_format(date_create($value['date_resolved']), 'jS F Y, g:ia');
					}
					$the_blockers[] = array(
						"id"=> $value['id'],
						"recall_id"=> $value['recall_id'],
						"continuity_id"=> $value['continuity_id'],
						"cc_id"=> $value['cc_id'],
						"crt_id"=> $value['crt_id'],
						"raised_by"=> $this->common_model->getname($value['crt_id']),
						"assigned"=> $this->common_model->getname($value['assigned']),
						"blocker"=> $value['blocker'],
						"issue"=> $value['issue'],
						"impact"=> $value['impact'],
						"note"=>($value['note'] != '') ? $value['note'] : 'No notes added',
						"step_no"=> $value['step_no'],
						"status"=> $value['status'],
						"status_name"=> $status_name,
						"status_class"=> $status_class,
						"task"=>$this->common_model->taskname($value['task_id'], 'cf_recall_steps'),
						"btn_class"=> $btn_class,
						"btn_text"=> $btn_text,
						"btn_fa"=> $btn_fa,
						"date_created"=> date_format(date_create($value['date_created']), 'jS F Y, g:ia'),
						"date_resolved"=> $date_resolved
					);
				}
			}
			
			
			$whr['id']= $recall_id;
			$recall_date = $this->master_model->getRecords('cf_recall',$whr,'*', array('id'=>'DESC'));
			
			$whr_step = array(
				'recall_pack_id'=>'18',
				'date <'=>$recall_date[0]['initiation_date']
			);
			
			$recallsteps = $this->master_model->getRecords('cf_recall_packs_steps', $whr_step, '*', array('order'=>'ASC'));
			
			$data['recallsteps'] = $recallsteps;
			
			
			
			$data['steptasks'] = $this->api_model->steptasks($recall_id);
			$data['crts'] = $this->common_model->get_org_crts($cc_id, $org_id, '5', 'add_cc'); 
			

			$data['blockers'] = $the_blockers;
			
			$data['blockers_count'] = count($blockers);
			$data['unresolved_blockers_count'] = count($unresolved_blockers);
			
			
			$data['result'] = 'success';
		}
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	
	public function notes() {
		
		//$this->check_api_key();
		
		if (isset($_POST['recall_id']) && isset($_POST['task_id'])){
			$task_id = $_POST['task_id'];
			$recall_id = $_POST['recall_id'];
			$user_level = $_POST['user_level'];
			$login_id = $_POST['login_id'];
			$cc_id = $_POST['cc_id'];
			$org_id = $_POST['org_id'];
			
			$whr_task = array(
				//'recall_id'=>$recall_id,
				'id'=>$task_id
			);
			$data['result'] = 'success'; 

			$tasks = $this->master_model->getRecords('cf_recall_steps', $whr_task, '*', array('category_id'=>'ASC', 'id'=>'ASC'));

			//if attached task note
			if(isset($_POST['theimg'])){
				$theimg = $_POST['theimg'];
				
				
				$arr = array(
					'message_id'=>$task_id,
					'base64_img'=>$theimg,
					'cc_id'=>$cc_id,
					'org_id'=>$org_id
				);
				
				$this->master_model->insertRecord('cf_recall_steps_file', $arr);

							
				$data['theimg'] = $theimg;
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
				
			}
			
			
			//if update task note
			if(isset($_POST['thenote'])){
				$thenote = $_POST['thenote'];
				
				$arr['answer'] = $thenote;
				if($tasks[0]['assigned'] == ''){
					
					$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
					$nowtime = $this->common_model->usertime($user_info[0]['timezone']);
					
					$arr['assigned'] = $login_id;
					$arr['date_assigned'] = $nowtime;
					
				}
				
				$this->master_model->updateRecord('cf_recall_steps', $arr, array('id'=>$task_id));
				
				$data['thenote'] = $thenote;
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}


			//get the task
			foreach($tasks as $r=>$task){

				if($task['status'] == 0){// not completed
					$date_class="hidden";
					$status_name="";
					
					if($task['answer'] == ""){// not completed
						$answer="";
					}
					else{
						$answer = $task['answer'];
					}
					
				}
				else{
					$date_class="";
					$status_name="Completed";
					
					
					if($task['answer'] == ""){// not completed
						$answer="No notes added";
					}
					else{
						$answer = $task['answer'];
					}
				}
	
				if($task['assigned'] == ""){
					$assigned_class="hidden";
				}
				else{
					$assigned_class="";
				}
					
					
				
				$data['task'] = array(
					'id'=>$task['id'],
					'question'=>$task['question'],
					'answer'=>$answer,
					'recall_id'=>$recall_id,
					'assigned'=>$task['assigned'],
					'assigned_name'=>$this->common_model->getname($task['assigned']),
					'status'=>$task['status'],
					'status_name'=>$status_name,
					'task_guide'=>$task['task_guide'],
					'category_id'=>$task['category_id'],
					'date_assigned'=>date_format(date_create($task['date_assigned']), 'g:ia, jS F Y'),
					'date_completed'=>date_format(date_create($task['date_completed']), 'g:ia, jS F Y'),
					'time_lapse'=>$task['time_lapse'],
					'assigned_class'=>$assigned_class,
					'date_class'=>$date_class
				);
				
				$whr_file = array(
					'message_id'=>$task_id,
					'cc_id'=>$cc_id,
					'org_id'=>$org_id
				);
				$thefiles = $this->master_model->getRecords('cf_recall_steps_file', $whr_file, '*', array('file_upload_id'=>'DESC'));
				
				$filesattached = array();
				$filecount = 0;
				if(count($thefiles) > 0){
					foreach($thefiles as $r=>$value){
						if(substr($value['file_upload_name'], -3) == 'jpg'){
							$filesattached[] = array(
								'filename'=>substr($value['file_upload_name'], 13),
								'url'=> base_url().'uploads/recallsteps-uploads/'.$value['file_upload_name']			
							);
							$filecount++;
							
						}
					}
				}
				
				$data['file_count'] = $filecount;
				$data['files'] = $filesattached;
				
			}


		}//end valid url
		
		else{
			$data['result'] = 'no_data';
		}
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	
	
	
	
	public function kpi() {
		//$this->check_api_key();

		$recall_id = (isset($_POST['recall_id'])) ? $_POST['recall_id'] : '38';

		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';

		$data = $this->api_model->kpi($recall_id, $cc_id, $org_id, $login_id);
		
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	
	public function costmonitor() {
		//$this->check_api_key();
		$data['result'] = 'error';
		$recall_id = (isset($_POST['recall_id'])) ? $_POST['recall_id'] : '2';
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '5';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '5';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '3';
		
		if ($recall_id != ''){


			$active_mod  = 5;
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
			$nowtime = $this->common_model->usertime($user_info[0]['timezone']);
			
			
			//set data
			$wher_cost_cat = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'recall_id'=>$recall_id
			);
			
			$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));

			$default_currency = $this->master_model->getRecords('cf_recall',array('id'=>$recall_id));
			
			$currency_code = $this->master_model->getRecords('countries_currency',array('currencyCode !='=>''), '*', array('countryName'=>'ASC'));
			
			$curr_arr = array();
			
			foreach($currency_code as $curr=>$currval){
				$currval['name_currency'] = $currval['countryName'].' ('.$currval['currencyCode'].')';
				$curr_arr[] = $currval;
			}
			
			$data['currency_code'] = $curr_arr;
			$data['default_currency'] = $default_currency[0]['default_currency_id'];
			
			
			//if add cost category
			if(isset($_POST['category_name'])){
				
						
				$name_cat = $_POST['category_name'];
				$cat_id = $_POST['category_id'];

				//active module is recall
				if($active_mod == '5'){
					$themod = 'recall_id';
				}
				else{
					$themod = 'continuity_id';
				}
				$arr = array(
					'name'=>$name_cat,
					'org_id'=>$org_id,
					'cc_id'=>$cc_id,
					$themod=>$recall_id,
					'date_created'=>$nowtime
					
				);
				if($cat_id == ''){
					$data['result'] = 'error';
					$data['action'] = 'add_category';

					if($id = $this->master_model->insertRecord('cf_cost_category',$arr,true)){
						$data['result'] = 'success';
					}

				}else{
					$data['result'] = 'error';
					$data['action'] = 'update_category';
					$arr2 = array(
						'name'=>$name_cat,
						'org_id'=>$org_id,
						'cc_id'=>$cc_id
					);
					
					if($this->master_model->updateRecord('cf_cost_category',$arr2,array('cost_id'=>$cat_id))){
						$data['result'] = 'success';
					
					}
				}				
			
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
				
					
			}//end add/update category name
			
			
			//if add new cost item
			if(isset($_POST['invoice_date'])){
			
				$item_name = $_POST['item_name'];
				$item_cost = str_replace( ',', '', $_POST['item_cost'] );
				$item_invoice = $_POST['invoice_date'];
				$currency_code = $_POST['item_currency'];
				
				$cost_item_id = $_POST['item_id'];
				$cat_id = $_POST['category_id'];

				if($active_mod == '5'){
					$themod = 'recall_id';
				}
				else{
					$themod = 'continuity_id';
				}
				
				$arr = array(
					'item_name'=>$item_name,
					'item_cost'=>$item_cost,
					'item_date_invoice'=>$item_invoice,
					'currency'=>$currency_code,
					'org_id'=>$org_id,
					'cost_id'=>$cat_id,
					$themod=>$recall_id,
					'date_created'=>$nowtime
					
				);
				if($cost_item_id == ''){
					if($id = $this->master_model->insertRecord('cf_cost_category_item',$arr,true)){
						
						$data['result'] = 'success';
					
					}
					else{
						$data['result'] = 'error';
					}
					$data['action'] = 'add_item';
					
				}else{
					
					$arr2 = array(
						'item_name'=>$item_name,
						'item_cost'=>$item_cost,
						'item_date_invoice'=>$item_invoice,
						'currency'=>$currency_code,
					);
					if($this->master_model->updateRecord('cf_cost_category_item',$arr2,array('item_id'=>$cost_item_id))){
						
						$data['result'] = 'success';

					
					}
					else{
						$data['result'] = 'error';
					}
					
					$data['action'] = 'update_item';
				}

				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;

			}//end add/update items
			
			//if get items
			if(isset($_POST['getitems'])){
				$cost_id = $_POST['cost_id'];
				$items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$cost_id, 'recall_id'=>$recall_id));
				

				$the_items = array();
				if(count($items) > 0){
					foreach($items as $r=>$value){
						$the_items[] = array(
							'id'=>$value['item_id'],
							'cost_id'=>$value['cost_id'],
							'currency_id'=>$value['currency'],
							'currency'=>$this->common_model->currencycode($value['currency']),
							'item_name'=>$value['item_name'],
							'item_date_invoice'=>$value['item_date_invoice'],
							'item_cost'=>$value['item_cost'],
							'item_cost_formatted'=>number_format($value['item_cost'],2),
							'recall_id'=>$value['recall_id'],
							'date_created'=>$value['date_created'],
						);
					}
				}
				
				
				
				$cost_category = $this->master_model->getRecords('cf_cost_category',array('cost_id'=>$cost_id));
				
				$the_subtotal = '<div class="list list-nm">';

				
				$the_subtotal .= '<div class="item item-divider">';
				
				$the_subtotal .= $cost_category[0]['name'];
				  
				$the_subtotal .= '</div>';
				
				if(count($items) > 0){
			
					$mycurr = $items[0]['currency'];
					$totalcurr = 0;
					$m = 0;
					
				
					
					foreach($items as $it=>$ems){
						if($mycurr != $ems['currency']){
								
							$the_subtotal .= '<div class="item balanced">';
							
							$the_subtotal .= 'Total Summary '.number_format($totalcurr,2).' for '.$this->common_model->currencycode($mycurr).'';
		
							$the_subtotal .= '</div>';

							$mycurr = $ems['currency'];
							$totalcurr = $ems['item_cost'];
							
		
						}else{
							
							$totalcurr += $ems['item_cost'];
						}
		
						$the_subtotal .= '<div class="item">';
						
						$the_subtotal .= $ems['item_name'];
						$the_subtotal .= '<p>'.$this->common_model->currencycode($ems['currency']).' '.number_format($ems['item_cost'],2).'</p>';
						  
						$the_subtotal .= '</div>';
						
						if((count($items) - 1) == $m){
							$the_subtotal .= '<div class="item balanced">';

							$the_subtotal .= 'Total Summary '.number_format($totalcurr,2).' for '.$this->common_model->currencycode($mycurr);
		
							$the_subtotal .= '</div>';
						}
						
						$m++;
					}
					
				}else{
					$the_subtotal .= '<div class="item">No Cost Items</div>';
				}

				$the_subtotal .= '</div> ';	
				
				$data['result'] = 'success';
				$data['the_subtotal'] = $the_subtotal;				

				$data['items'] = $the_items;
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}//end get items




			//if get subtotal
			if(isset($_POST['getsubtotal'])){
				
				$cost_id = $_POST['cost_id'];

				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}//end get subtotal


			//if get running cost
			if(isset($_POST['getrunningcost'])){
				$data['result'] = 'success';
				$data['running_cost'] = $this->common_model->accumulate($recall_id, '5');
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}//end get running cost
			
			
			
			//if delete cost cat
			if(isset($_POST['deletecostcat'])){
				$cat_id = $_POST['id'];
		
				if($this->master_model->deleteRecord('cf_cost_category','cost_id',$cat_id)){	
				
					if($this->master_model->deleteRecord('cf_cost_category_item','cost_id',$cat_id)){
						$data['result'] = 'success';
					}
				}

				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
				
			}//end  delete cost cat
			

			$data['result'] = 'success';
			$data['cost_category'] = $cost_category;
			$data['running_cost'] = $this->common_model->accumulate($recall_id, '5');
		}

		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	public function approvers() {
		
		//$this->check_api_key();
		$data['result'] = 'error';
		if (isset($_POST['recall_id']) && isset($_POST['task_id'])){
			$task_id = $_POST['task_id'];
			$recall_id = $_POST['recall_id'];
			$user_level = $_POST['user_level'];
			$login_id = $_POST['login_id'];
			$cc_id = $_POST['cc_id'];
			$org_id = $_POST['org_id'];


			$whr_task = array(
				'recall_id'=>$recall_id,
				'id'=>$task_id
			);
			$tasks = $this->master_model->getRecords('cf_recall_steps', $whr_task, '*', array('category_id'=>'ASC', 'id'=>'DESC'));
			
			
			//check if assigned or cc
			$data['assigned'] = 'no';
			if($user_level == '0' || $tasks[0]['assigned'] == $login_id){
				$data['assigned'] = 'yes';
			}

			
			
			if(isset($_POST['mark_approve'])){
				$active_module = '5';
				$the_approver = $this->common_model->markapproved($task_id, $login_id,  $active_module );

				$data['result'] = 'success';
				$data['approver_data'] = $the_approver;
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}
			



			//if assign approver
			if(isset($_POST['selected_approver'])){
				
				$selected_approver = $_POST['selected_approver'];
				$data['selected_approver'] = $selected_approver;
				$data['action'] = 'update_approver';
				$data['result'] = 'success';
				$data['task_id'] = $task_id;
						
				$arr = array(
					'approvers'=>serialize(array($selected_approver)),
					'approvers_status'=>''
				);
		
				$active_mod  = '5';
		
				//active module is recall
				if($active_mod == '5'){
					$this->master_model->updateRecord('cf_recall_steps', $arr, array('id'=>$task_id));
				}
				else{
					$this->master_model->updateRecord('cf_continuity_steps', $arr, array('id'=>$task_id));
				}
				$data['arr'] = $arr;
				
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}//end update approvers

			$data['result'] = 'success';
			$approvers = $this->common_model->get_task_approvers($task_id, $user_level, '5');
			
			
			$data['total_approvers'] = 0;
			$data['unapproved_count'] = 0;
			$data['theapprovers'] = array();
			if(count($approvers) > 0){
				$data['total_approvers'] = $approvers['total_approvers'];
				$data['unapproved_count'] = $approvers['unapproved_count'];
				$data['theapprovers'] = $approvers['theapprovers'];
			}
			
			
			
		}
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	
	public function task() {
		
		//$this->check_api_key();
		
		if (isset($_POST['recall_id']) && isset($_POST['task_id'])){
			$task_id = $_POST['task_id'];
			$recall_id = $_POST['recall_id'];
			$user_level = $_POST['user_level'];
			$login_id = $_POST['login_id'];
			$cc_id = $_POST['cc_id'];
			$org_id = $_POST['org_id'];
			
			
			$data['task_id'] = $task_id;
			$data['recall_id'] = $recall_id;
			$data['user_level'] = $user_level;
			$data['login_id'] = $login_id;
			$data['cc_id'] = $cc_id;
			$data['org_id'] = $org_id;
			
			$whr_task = array(
				'recall_id'=>$recall_id,
				'id'=>$task_id
			);
			$tasks = $this->master_model->getRecords('cf_recall_steps', $whr_task, '*', array('category_id'=>'ASC', 'id'=>'DESC'));

			$data['result'] = 'success'; 
			
			


			//get the task
			foreach($tasks as $r=>$task){
				
				
				if($task['status'] == 0){// not completed
					$date_class="hidden";
					$status_name="In Progress";
					$status_class="badge-negative";
					
					if($task['answer'] == ""){// not completed
						$answer="Task not yet started";
					}
					else{
						$answer = $task['answer'];
					}
					
				}
				else{
					$date_class="";
					$status_name="Completed";
					$status_class="badge-positive";
					
					if($task['answer'] == ""){// not completed
						$answer="No notes added";
					}
					else{
						$answer = $task['answer'];
					}
				}
				
				$approvers = $this->common_model->get_task_approvers($task['id'], $user_level, '5');
				if(count($approvers) > 0){
					if($approvers['unapproved_count'] > 0){
						$status_name="Awaiting Approval";
						$status_class="";
					}
				}
				
				
	
				if($task['assigned'] == ""){
					$assigned_class="hidden";
				}
				else{
					$assigned_class="";
				}
					
				if($task['question'] == 'Hazard Analysis'){
					$data['hazard'] = 'yes';
				}
				else{
					$data['hazard'] = '';
				}
				
				$data['task'] = array(
					'id'=>$task['id'],
					'question'=>$task['question'],
					'answer'=>$answer,
					'recall_id'=>$recall_id,
					'assigned'=>$task['assigned'],
					'assigned_name'=>$this->common_model->getname($task['assigned']),
					'status'=>$task['status'],
					'status_name'=>$status_name,
					'status_class'=>$status_class,
					'task_guide'=>$task['task_guide'],
					'category_id'=>$task['category_id'],
					'date_assigned'=>date_format(date_create($task['date_assigned']), 'g:ia, jS F Y'),
					'date_completed'=>date_format(date_create($task['date_completed']), 'g:ia, jS F Y'),
					'time_lapse'=>$task['time_lapse'],
					'assigned_class'=>$assigned_class,
					'date_class'=>$date_class
				);
					
				$actions = array();
				
				if($task['step_no'] != '2'){
				
					if($user_level == 0){// if cc
						
						if($task['status'] == 0){
							$actions[] = array(
								'action'=>'Mark Completed',
								'name'=>'complete',
								'class'=>'btn btn-positive',
								'icon'=>'fa fa-check',
								'task_id'=>$task['id']
							);
							
							$actions[] = array(
								'action'=>'Assign',
								'name'=>'assign',
								'class'=>'btn',
								'icon'=>'fa fa-user',
								'task_id'=>$task['id']
							);
						}
						else{
							$actions[] = array(
								'action'=>'Mark Incomplete',
								'name'=>'incomplete',
								'class'=>'btn btn-negative',
								'icon'=>'fa fa-times',
								'task_id'=>$task['id']
							);
						
						}
						
						
						if(($task['assigned'] == "" || $task['assigned'] == $login_id) && ($task['status'] == 0)){
						
							if($task['question'] != 'Hazard Analysis'){
								$actions[] = array(
									'action'=>'Task Notes',
									'name'=>'notes',
									'class'=>'btn',
									'icon'=>'fa fa-edit',
									'task_id'=>$task['id']
								);
							
							}
							
							
						}
						
						$actions[] = array(
							'action'=>'Approver(s)',
							'name'=>'approver',
							'class'=>'btn',
							'icon'=>'fa fa-users',
							'task_id'=>$task['id']
						);
						
						
						if(count($approvers) > 0){
							if($approvers['unapproved_count'] > 0){
								if($approvers['theapprovers'] == $login_id){
								
									
									$actions[] = array(
										'action'=>'Approve',
										'name'=>'approve',
										'class'=>'btn',
										'icon'=>'fa fa-thumbs-o-up',
										'task_id'=>$task['id']
									);
								
								
								}
							}
						}//end of mark approve btn
					}
					else{// if crt
						
						
						if($task['assigned'] == $login_id){
							if($task['status'] == 0){
								$actions[] = array(
									'action'=>'Mark Completed',
									'name'=>'complete',
									'class'=>'btn btn-positive',
									'icon'=>'fa fa-check',
									'task_id'=>$task['id']
								);
								
								if($task['question'] != 'Hazard Analysis'){//check if not hazard table
									
									$actions[] = array(
										'action'=>'Task Notes',
										'name'=>'notes',
										'class'=>'btn',
										'icon'=>'fa fa-edit',
										'task_id'=>$task['id']
									);
								}

							}
							else{
								$actions[] = array(
									'action'=>'Mark Incomplete',
									'name'=>'incomplete',
									'class'=>'btn btn-negative',
									'icon'=>'fa fa-times',
									'task_id'=>$task['id']
								);
							
							}
							
							
						}
						else{
							
							if($task['assigned'] == '' && $task['status'] == 0){
								$actions[] = array(
									'action'=>'Pick-up Task',
									'name'=>'pickup',
									'class'=>'btn',
									'icon'=>'fa fa-user',
									'task_id'=>$task['id']
								);
							}
							
						}
						
							
						$actions[] = array(
							'action'=>'Approver(s)',
							'name'=>'approver',
							'class'=>'btn',
							'icon'=>'fa fa-users',
							'task_id'=>$task['id']
						);
					
						if(count($approvers) > 0){
							if($approvers['unapproved_count'] > 0){
								
								
								if($approvers['theapprovers'][0]['id'] == $login_id){
								
									$actions[] = array(
										'action'=>'Approve',
										'name'=>'approve',
										'class'=>'btn',
										'icon'=>'fa fa-thumbs-o-up',
										'task_id'=>$task['id']
									);
								
								}
							}
						}//end of mark approve btn
						
					
					}
						
					$data['actions'] = $actions;
				
				
				
				}
									
			}//end foreach
			

			$the_org_crts = $this->common_model->get_org_crts($cc_id, $org_id, '5');


			$data['crts'] = $the_org_crts; // $crts;

			
		}

		/*if (isset($_POST['recall_id']) && isset($_POST['task_id'])){
			$task_id = $_POST['task_id'];
			$recall_id = $_POST['recall_id'];
			$user_level = $_POST['user_level'];
			$login_id = $_POST['login_id'];
			$cc_id = $_POST['cc_id'];
			$org_id = $_POST['org_id'];
			
			$whr_task = array(
				'recall_id'=>$recall_id,
				'id'=>$task_id
			);
			$tasks = $this->master_model->getRecords('cf_recall_steps', $whr_task, '*', array('category_id'=>'ASC', 'id'=>'DESC'));

			$data['result'] = 'success'; 
			

					
			
		}
		*/
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	
	public function save_hazard()	{ 

		$cc_id = $_POST['cc_id'];
		$org_id = $_POST['org_id'];
		$login_id = $_POST['login_id'];
		$user_level = $_POST['user_level'];
		
		$severity = $_POST['severity'];
		$presence = $_POST['presence'];
		$like_dist = $_POST['injury'];
		$distribution = $_POST['distribution'];
		$identification = $_POST['identification'];
		$hazard = $_POST['hazard'];
		$step_3_table_id = $_POST['task_id'];
		
		$step3table_answer = $severity. ' ' .$presence. ' ' .$like_dist. ' ' .$distribution. ' ' .$identification. ' ' .$hazard;


		$nowtime = $this->common_model->userdatetime($login_id);
		
		
		$the_auth = $login_id;

		
		$active_mod  = '5';

		$arr = array(
			'answer'=>$step3table_answer,
			'assigned'=>$the_auth,
			'date_updated'=>$nowtime,
			'date_assigned'=>$nowtime
		);
		
		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps', $arr, array('id'=>$step_3_table_id));
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps', $arr, array('id'=>$step_3_table_id));
		}
		
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($arr, JSON_PRETTY_PRINT);
		
	}	
		
	
	
	public function recall() {
		
		//$this->check_api_key();
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		//$login_id = $this->uri->segment(3);
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$whr = array(
			'closed'=>(isset($_POST['incident_type'])) ? '1' : '0'
		);
		
		if (isset($_POST['cc_id']) && isset($_POST['org_id'])){
			
			$whr['cc_id'] = $cc_id;
			$whr['org_id'] = $org_id;
		}
		
		if(isset($_POST['recall_id'])){
			$recall_id = (isset($_POST['recall_id'])) ? $_POST['recall_id'] : '34';
			$data['recall_id'] = $recall_id;

			if($recall_id != ''){
				$whr['id']= $recall_id;
			}
			
			$incident_type = '5';
			
			if($incident_type == '5'){
				$inci_type = 'recall';
			}
			else{ //continuity = 8
				$inci_type = 'continuity';
			}
			
			
			//get notices
			$whr_noti = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'type'=>$inci_type,
				'incident_id'=>$recall_id
			);
			
			$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
			
			$email_sent_count = 0;
			$email_confirmed_count = 0;
			$sms_sent_count = 0;
			if(count($notices_sent) > 0){                
				foreach($notices_sent as $r=>$ns){
					//email sent count
					$whr_email_sent = array(
						'bulk_id'=>$ns['id'],
						'email_sent'=>'1'
					);
					$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

					//email confirmed count
					$whr_email_confirmed = array(
						'bulk_id'=>$ns['id'],
						'email_confirmed'=>'1'
					);
					$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


					//sms sent count
					$whr_sms_sent = array(
						'bulk_id'=>$ns['id'],
						'sms_sent'=>'1'
					);
					$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
					
					$email_sent_count += $bulk_data_email_sent;
					$email_confirmed_count += $bulk_data_email_confirmed;
					$sms_sent_count += $bulk_data_sms_sent;
				
				}
				
			}			
			
			$data['confirmed_notices'] = $email_confirmed_count;
			
			
			//get blockers 
			$bb = array(
				'cc_id'=>$cc_id,
				'recall_id'=>$recall_id
			);
			
			$bb_unresolved = array(
				'cc_id'=>$cc_id,
				'recall_id'=>$recall_id,
				'status'=>'0'
			);
			
			$blockers = $this->master_model->getRecordCount('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));
			$unresolved_blockers = $this->master_model->getRecordCount('cf_recall_blockers',$bb_unresolved,'*',array('id'=>'DESC'));
			
			$data['blockers'] = $blockers;
			$data['unresolved_blockers'] = $unresolved_blockers;
			
			//get crts
			$the_org_crts = $this->common_model->get_org_crts($cc_id, $org_id, '5');
			$data['crts'] = $the_org_crts;
			
			
			//get cost monitor
			$wher_cost_cat = array(
				'org_id'=>$org_id,
				'cc_id'=>$cc_id,
				'recall_id'=>$recall_id,
			);
			
			$cost_category = $this->master_model->getRecordCount('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));
			
			$items = $this->master_model->getRecordCount('cf_cost_category_item',array('recall_id'=>$recall_id));

			$data['cost_category'] = $cost_category;
			$data['cost_items'] = $items;
			
			//get all steps 
			
			$steps = array();
		
			$recall_date = $this->master_model->getRecords('cf_recall',$whr,'*', array('id'=>'DESC'));
			$recall_pack_id = 18;
			
			$whr_step = array(
				'recall_pack_id'=>$recall_pack_id,
				'date <'=>$recall_date[0]['initiation_date']
			);
			
			$recallsteps = $this->master_model->getRecords('cf_recall_packs_steps', $whr_step, '*', array('order'=>'ASC'));
			
			$data['recallsteps'] = $recallsteps;
			
			
			$count_step = 1;
			foreach($recallsteps as $r=>$value){//get all steps

				$whr_category = array(
					'recall_pack_id'=>$recall_pack_id,
					'step_no'=>$value['step_no']
				);

				$steps_category = $this->master_model->getRecords('cf_recall_steps_category', $whr_category, '*', array('id'=>'ASC'));
				
				//set empty category for tasks with category
				$categories = array();
				$category_count = 1;

				
				//set task class
				if($count_step == 1){ 
					$task_class = '';
					$able_class = 'disabled opacity-full';
				}
				else{
					$able_class = '';
					$task_class = 'navigate-right task-btn-link';
				}
				
				//check if no category on step
				if(count($steps_category) == 0){
					$uncat_tasks = array();
					
					
					
					
					//include date and affected location incident
					if($count_step == 1){ 
						$uncat_tasks[] = array(
							'id'=>'',
							'question'=>'Date of incident',
							'answer'=>$recall_date[0]['incident_date'],
							'object_answer'=>'',
							'task_class'=>$task_class,
							'able_class'=>$able_class,
							'recall_id'=>$recall_id,
							'assigned'=>'',
							'assigned_name'=>'',
							'status'=>'1',
							'task_guide'=>'',
							'category_id'=>'',
							'date_assigned'=>'',
							'date_completed'=>'',
							'time_lapse'=>''
						);
							
							
						//array of affected location
						//get affected locations
						$locs = json_decode($recall_date[0]['affected_location']);
						$loc_c = 1;
						$loc_index = 0;
						$aff_locations = '';
						$affected_locaxions = array();
						if(count($locs) > 0){
							 foreach($locs[0] as $loc){
								 
								 if($loc !=''){
									 $aff_locations .= $loc_c.'. '.$loc. ' ';
		
									 $loc_c++;
		
		
									 $affected_locaxions[] = array(
										'location_name'=>$loc,
										'latitude'=>$locs[1][$loc_index],
										'longitude'=>$locs[2][$loc_index]
									 );
									 
									 
								 }
								 $loc_index++;
								 
							 }
						}
						else{
							$aff_locations .= 'No affected location.';
						}
						
							
									
						$uncat_tasks[] = array(
							'id'=>'',
							'question'=>'Affected sites/locations',
							'answer'=>'',
							'object_answer'=>$affected_locaxions,
							'task_class'=>$task_class,
							'able_class'=>$able_class,
							'recall_id'=>$recall_id,
							'assigned'=>'',
							'assigned_name'=>'',
							'status'=>'1',
							'task_guide'=>'',
							'category_id'=>'',
							'date_assigned'=>'',
							'date_completed'=>'',
							'time_lapse'=>''
						);
									
					}//end of date and affected location
			
			
	
					$whr_task = array(
						'recall_id'=>$recall_id,
						'step_no'=>$value['step_no']
					);
	
					$step_tasks = $this->master_model->getRecords('cf_recall_steps', $whr_task, '*', array('category_id'=>'ASC', 'id'=>'DESC'));
					
				
					if(count($step_tasks) > 0){
						
						foreach($step_tasks as $r=>$task){
						
							if($task['category_id'] == '0'){// check if under the category
								
							
								$uncat_tasks[] = array(
									'id'=>$task['id'],
									'question'=>$task['question'],
									'answer'=>$task['answer'],
									'object_answer'=>'',
									'task_class'=>$task_class,
									'able_class'=>$able_class,
									'recall_id'=>$recall_id,
									'assigned'=>$task['assigned'],
									'assigned_name'=>$this->common_model->getname($task['assigned']),
									'status'=>$task['status'],
									'task_guide'=>$task['task_guide'],
									'category_id'=>$task['category_id'],
									'date_assigned'=>$task['date_assigned'],
									'date_completed'=>$task['date_completed'],
									'time_lapse'=>$task['time_lapse']
								);
								
									
							}
						}
	
						if(count($uncat_tasks) > 0){
	
							$categories[] = array(
								'id'=>'0',
								'category_name'=>'Uncategorized',
								'class'=>'hidden',
								'step_no'=>$value['step_no'],
								'tasks'=>$uncat_tasks
							);
							
						}//add uncategorized
						
						
					}
							
						
					//get count of completed/incomplete tasks
					$whr_completed = $whr_task;
					$whr_completed['status'] = '1'; 
					$completed = $this->master_model->getRecordCount('cf_recall_steps', $whr_completed, '*', array('category_id'=>'ASC', 'id'=>'DESC'));

					$whr_incompleted = $whr_task;
					$whr_incompleted['status'] = '0'; 
					$incompleted = $this->master_model->getRecordCount('cf_recall_steps', $whr_incompleted, '*', array('category_id'=>'ASC', 'id'=>'DESC'));
				
					
					$steps[] = array(
						'step_no'=>$value['step_no'] - 1,
						'step_name'=>$value['name'],
						'categories'=>$categories,
						'incompleted'=>$incompleted,
						'incompleted'=>$incompleted,
						'completed'=>$completed,
						'task_count'=>count($step_tasks)
					);
					
				
				
				}//end empty category
				else{
					
						
									
					foreach($steps_category as $r=>$category){//get all categories
	
		
						$whr_task = array(
							'recall_id'=>$recall_id,
							'step_no'=>$value['step_no']
						);
		
						$step_tasks = $this->master_model->getRecords('cf_recall_steps', $whr_task, '*', array('category_id'=>'DESC'));
						
						$tasks = array();
						$uncat_tasks = array();
							
						
						//include date and affected location incident
						if($count_step == 1){ 
							$uncat_tasks[] = array(
								'id'=>'',
								'question'=>'Date of incident',
								'answer'=>$recall_date[0]['incident_date'],
								'object_answer'=>'',
								'task_class'=>$task_class,
								'able_class'=>$able_class,
								'recall_id'=>$recall_id,
								'assigned'=>'',
								'assigned_name'=>'',
								'status'=>'1',
								'task_guide'=>'',
								'category_id'=>'',
								'date_assigned'=>'',
								'date_completed'=>'',
								'time_lapse'=>''
							);
								
								
							//array of affected location
							//get affected locations
							$locs = json_decode($recall_date[0]['affected_location']);
							$loc_c = 1;
							$loc_index = 0;
							$aff_locations = '';
							$affected_locaxions = array();
							if(count($locs) > 0){
								 foreach($locs[0] as $loc){
									 
									 if($loc !=''){
										 $aff_locations .= $loc_c.'. '.$loc. ' ';
			
										 $loc_c++;
			
			
										 $affected_locaxions[] = array(
											'location_name'=>$loc,
											'latitude'=>$locs[1][$loc_index],
											'longitude'=>$locs[2][$loc_index]
										 );
										 
										 
									 }
									 $loc_index++;
									 
								 }
							}
							else{
								$aff_locations .= 'No affected location.';
							}
							
								
										
							$uncat_tasks[] = array(
								'id'=>'',
								'question'=>'Affected sites/locations',
								'answer'=>'',
								'object_answer'=>$affected_locaxions,
								'task_class'=>$task_class,
								'able_class'=>$able_class,
								'recall_id'=>$recall_id,
								'assigned'=>'',
								'assigned_name'=>'',
								'status'=>'1',
								'task_guide'=>'',
								'category_id'=>'',
								'date_assigned'=>'',
								'date_completed'=>'',
								'time_lapse'=>''
							);
										
						}//end of date and affected location
						
						
						if(count($step_tasks) > 0){
							
							foreach($step_tasks as $r=>$task){
								//check if hazard anaylisis
								if($task['question'] == 'Hazard Analysis'){
									$theanswer = '';
								}
								else{
									$theanswer = $task['answer'];
								}
							
								if($task['category_id'] == '0'){// check if under the category
									
									
									$uncat_tasks[] = array(
										'id'=>$task['id'],
										'question'=>$task['question'],
										'answer'=>$theanswer,
										'object_answer'=>'',
										'task_class'=>$task_class,
										'able_class'=>$able_class,
										'recall_id'=>$recall_id,
										'assigned'=>$task['assigned'],
										'assigned_name'=>$this->common_model->getname($task['assigned']),
										'status'=>$task['status'],
										'task_guide'=>$task['task_guide'],
										'category_id'=>$task['category_id'],
										'date_assigned'=>$task['date_assigned'],
										'date_completed'=>$task['date_completed'],
										'time_lapse'=>$task['time_lapse']
									);
										
								}
								
								
								else if($task['category_id'] == $category['id']){
									
									$tasks[] = array(
										'id'=>$task['id'],
										'question'=>$task['question'],
										'answer'=>$theanswer,
										'object_answer'=>'',
										'task_class'=>$task_class,
										'able_class'=>$able_class,
										'recall_id'=>$recall_id,
										'assigned'=>$task['assigned'],
										'assigned_name'=>$this->common_model->getname($task['assigned']),
										'status'=>$task['status'],
										'task_guide'=>$task['task_guide'],
										'category_id'=>$task['category_id'],
										'date_assigned'=>$task['date_assigned'],
										'date_completed'=>$task['date_completed'],
										'time_lapse'=>$task['time_lapse']
									);
								
								
								}//end of no category
							
								
							}//end foreach
		
							
						
						}//end check empty task
							
	
	
	
						if(count($uncat_tasks) > 0 && $category_count == 1){
	
							$categories[] = array(
								'id'=>'0',
								'category_name'=>'Uncategorized',
								'class'=>'hidden',
								'step_no'=>$value['step_no'],
								'tasks'=>$uncat_tasks
							);
							
						}//add uncategorized
						
						
						
						if(count($tasks) > 0){
							
							$categories[] = array(
								'id'=>$category['id'],
								'category_name'=>$category['category_name'],
								'class'=>'',
								'step_no'=>$category['step_no'],
								'tasks'=>$tasks
							);
							
						}//add categorized
						
	

					//get count of completed/incomplete tasks
					$whr_completed = $whr_task;
					$whr_completed['status'] = '1'; 
					$completed = $this->master_model->getRecordCount('cf_recall_steps', $whr_completed, '*', array('category_id'=>'ASC', 'id'=>'DESC'));
	
					$whr_incompleted = $whr_task;
					$whr_incompleted['status'] = '0'; 
					$incompleted = $this->master_model->getRecordCount('cf_recall_steps', $whr_incompleted, '*', array('category_id'=>'ASC', 'id'=>'DESC'));
		
					//store uncategory once
					$category_count++;				
					}//end foreach category
					
					$steps[] = array(
						'step_no'=>$value['step_no'] - 1,
						'step_name'=>$value['name'],
						'categories'=>$categories,
						'incompleted'=>$incompleted,
						'incompleted'=>$incompleted,
						'completed'=>$completed,
						'task_count'=>count($step_tasks)
					);
					
					
					
					
					
				}//end notempty category
			$count_step++;	
			}//end foreach steps

			$data['steps'] = $steps;

		}//end if single recall
		
		
		$recall = $this->master_model->getRecords('cf_recall',$whr,'*', array('id'=>'DESC'));
		$recall_count = count($recall);
		
		$data['org_id'] = $org_id;
		$data['cc_id'] = $cc_id;
		$data['recall_count'] = $recall_count;
		$the_recalls = array();
		
		if($recall_count > 0){
			foreach($recall as $r=>$value){
				
				//get count forum topics
				$topics = $this->master_model->getRecords('forum_post_master', array('post_level'=>'0'));
				$topic_count = 0;
				if(count($topics) > 0){
					foreach($topics as $trow=>$tval){
						if(substr($tval['scenario_id'], 1) == $value['id']){
							$topic_count++;
						}
					}
				}
				
				//get initiation type name
				if($value['initiation_type'] == 0){
					$init_typename = 'Mock';
					$type_class = 'badge';
					$object_class = 'text-muted';
				}
				else{
					$init_typename = 'Live';
					$type_class = 'badge badge-primary';
					$object_class = 'text-primary';
				}
				
				//get affected locations
				$locs = json_decode($value['affected_location']);
				$loc_c = 1;
				$loc_index = 0;
				$aff_locations = '';
				$affected_locaxions = array();
				if(count($locs) > 0){
					 foreach($locs[0] as $loc){
						 
						 if($loc !=''){
							 $aff_locations .= $loc_c.'. '.$loc. ' ';

							 $loc_c++;


							 $affected_locaxions[] = array(
							 	'location_name'=>$loc,
							 	'latitude'=>$locs[1][$loc_index],
							 	'longitude'=>$locs[2][$loc_index]
							 );
							 
							 
						 }
						 $loc_index++;
						 
					 }
				}
				else{
					$aff_locations .= 'No affected location.';
				}
				
				$history = $this->master_model->getRecords('reporting', array('recall_id'=>$value['id']),'*',array('id'=>'DESC'));

				
				
				$the_recalls[] = array(
					'id'=>$value['id'],
					'history'=>$history,
					'incident_no'=>$value['incident_no'],
					'incident_date'=>$value['incident_date'],
					'incident_name'=>$value['incident_name'],
					'affected_location'=>$affected_locaxions, //$value['affected_location'],
					'aff_locations'=>$aff_locations,
					'initiation_type'=>$value['initiation_type'],
					'init_typename'=>$init_typename,
					'type_class'=>$type_class,
					'initiation_date'=>$value['initiation_date'],
					'closed_date'=>$value['closed_date'],
					'incident_total_time'=>$value['incident_total_time'],
					'tasks_started'=>$value['tasks_started'],
					'tasks_ended'=>$value['tasks_ended'],
					'total_time_tasks'=>$value['total_time_tasks'],
					'input_pack_id'=>$value['input_pack_id'],
					'default_currency_id'=>$value['default_currency_id'],
					'object_class'=>$object_class,
					'topic_count'=>$topic_count
				);
				
			}
			$data['recalls'] = $the_recalls;
		
		}
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	
	}
	
	
	

	//fetch user messages	
	public function messages() {
		
		//$this->check_api_key();
	

		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		
		
		if(isset($_POST['crts'])){ //send compose message
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
				
			$themess = $_POST['message'];
			$subject = $_POST['subject'];
			$recepient = $_POST['crts'];
			$nowtime = $this->common_model->usertime($user_info[0]['timezone']);


			$arr=array(
				'sender_id'=>$login_id,
				'is_read'=>'0',
				'receiver_id'=>$recepient,
				'subject'=>$subject,
				'message'=>$themess,
				'send_date'=>$nowtime
			);
			
			$data['result'] = 'error'; 
			
			if($mess_id = $this->master_model->insertRecord('private_messages',$arr,true))
			{
				$data['result'] = 'success'; 
				$data['mess_id'] = $mess_id; 

			}

			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);

			return false;
		
		}
		

		
		if(isset($_POST['messid'])){ //get single message
	
			$whr_inb = array(
				'message_id'=>$_POST['messid']
			);
			
			$inbox = $this->master_model->getRecords('private_messages',$whr_inb,'*', array('send_date'=>'DESC'));
					
	
	
			//declare the_messages variable
			$the_messages = array(
				'login_id'=>$login_id
			);
			
			$the_messages['inbox'] = array();
			if (count($inbox) > 0) { //get user inbox
			
				foreach($inbox as $r=>$inb){
					
					if($inb['is_read'] == '0'){ //unread
						$inbstat = 'unread';
					}
					else{
						$inbstat = 'read';
					}
					
					array_push($the_messages['inbox'],
						array(
							'id'=>$inb['message_id'],
							'read'=>$inb['is_read'],
							'readstat'=>$inbstat,
							'sender_id'=>$inb['sender_id'],
							'sender'=>$this->common_model->getname($inb['sender_id']),
							'receiver_id'=>$inb['receiver_id'],
							'receiver'=>$this->common_model->getname($inb['receiver_id']),
							'subject'=>$inb['subject'],
							'message'=>$inb['message'],
							'send_date'=>date_format(date_create($inb['send_date']), 'j F Y') //.' ('.$this->common_model->ago($inb['send_date']).')'
						)
					);
				
				}//end inbox foreach
	
			}//end of check inbox					
			
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($the_messages, JSON_PRETTY_PRINT);
				
			$this->master_model->updateRecord('private_messages',array('is_read'=>'1'),array('message_id'=>$_POST['messid']));
					
			return false;
		} //end get single message
		
		
		$whr_inb = array(
			'receiver_id'=>$login_id,
			'is_receiver_del'=>'0'
		);
		$inbox = $this->master_model->getRecords('private_messages',$whr_inb,'*', array('send_date'=>'DESC'));
		

		$whr_outb = array(
			'sender_id'=>$login_id,
			'is_sender_del'=>'0'
		);
		$outbox = $this->master_model->getRecords('private_messages',$whr_outb,'*', array('send_date'=>'DESC'));
		
		
		$whr_str = "(msg.sender_id=".$login_id." AND msg.is_sender_del='1' AND msg.is_sender_del_trash='0') OR (msg.receiver_id=".$login_id." AND msg.is_receiver_del='1' AND msg.is_receiver_del_trash='0')";
		$this->db->where($whr_str,NULL,FALSE);
		$trash = $this->master_model->getRecords('private_messages as msg');
		
		
		//declare the_messages variable
		$the_messages = array(
			'login_id'=>$login_id
		);
		
		//get crts
		$the_org_crts = $this->common_model->get_org_crts($cc_id, $org_id, '5', 'add_cc');

		$the_messages['crts'] = $the_org_crts; // $crts;
		
		
		$the_messages['inbox'] = array();
		if (count($inbox) > 0) { //get user inbox
		
			foreach($inbox as $r=>$inb){
				
				if($inb['is_read'] == '0'){ //unread
					$inbstat = 'unread';
					$font_awe = 'fa fa-envelope fa-fw';
				}
				else{
					$inbstat = 'read';
					$font_awe = 'fa fa-envelope-o fa-fw';
				}
				
				array_push($the_messages['inbox'],
					array(
						'id'=>$inb['message_id'],
						'read'=>$inb['is_read'],
						'font_awe'=>$font_awe,
						'readstat'=>$inbstat,
						'sender_id'=>$inb['sender_id'],
						'sender'=>$this->common_model->getname($inb['sender_id']),
						'receiver_id'=>$inb['receiver_id'],
						'receiver'=>$this->common_model->getname($inb['receiver_id']),
						'subject'=>$inb['subject'],
						'message'=>$inb['message'],
						'send_date'=>date_format(date_create($inb['send_date']), 'j F Y') //.' ('.$this->common_model->ago($inb['send_date']).')'
					)
				);
			
			}//end inbox foreach

		}//end of check inbox
		
		$the_messages['outbox'] = array();
		if (count($outbox) > 0) { //get user outbox
		
			foreach($outbox as $r=>$inb){

				array_push($the_messages['outbox'],
					array(
						'id'=>$inb['message_id'],
						'read'=>$inb['is_read'],
						'sender_id'=>$inb['sender_id'],
						'sender'=>$this->common_model->getname($inb['sender_id']),
						'receiver_id'=>$inb['receiver_id'],
						'receiver'=>$this->common_model->getname($inb['receiver_id']),
						'subject'=>$inb['subject'],
						'message'=>$inb['message'],
						'send_date'=>date_format(date_create($inb['send_date']), 'j F Y') //.' ('.$this->common_model->ago($inb['send_date']).')'
					)
				);
			
			}//end outbox foreach

		}//end of check outbox
		
		$the_messages['trash'] = array();
		if (count($trash) > 0) { //get user outbox
		
			foreach($trash as $r=>$inb){

				array_push($the_messages['trash'],
					array(
						'id'=>$inb['message_id'],
						'read'=>$inb['is_read'],
						'sender_id'=>$inb['sender_id'],
						'sender'=>$this->common_model->getname($inb['sender_id']),
						'receiver_id'=>$inb['receiver_id'],
						'receiver'=>$this->common_model->getname($inb['receiver_id']),
						'subject'=>$inb['subject'],
						'message'=>$inb['message'],
						'send_date'=>date_format(date_create($inb['send_date']), 'j F Y') //.' ('.$this->common_model->ago($inb['send_date']).')'
					)
				);
			
			}//end trash foreach

		}//end of check trash
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($the_messages, JSON_PRETTY_PRINT);
		
	}
	
	
	public function incidentid(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';

		$incident_no = $this->common_model->gen_incidentno($cc_id, $org_id);
	
	
		$whr_question = array(
			'recall_pack_id'=>'18',
			'step_no'=>'2',
			'disabled'=>'0'
		);
		
	  	$questions = $this->master_model->getRecords('cf_recall_guidance', $whr_question,'',array('category'=>'ASC','arrangement'=>'DESC'));
		
		$data = array(
			'number'=>$incident_no,
			'questions'=>$questions
		);
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
		

	public function postincident(){ 

		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$user_full_name = (isset($_POST['user_full_name'])) ? $_POST['user_full_name'] : '';
		$user_email = (isset($_POST['user_email'])) ? $_POST['user_email'] : '';

		
		// load library
		$this->load->library('nexmo');
		// set response format: xml or json, default json
		$this->nexmo->set_format('json');
	 

		//recall pack
		$continuity_id = 18;

	//	$pack_id = $continuity_id;
		$r_pack_id = $continuity_id;

		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
		
		
		if(count($mypack)> 0){
			$pack_id = $mypack[0]['input_pack_id'];
			
			//if org has input_pack
			if($pack_id != 0){
				$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('input_pack_id'=>$mypack[0]['input_pack_id']));
				
				if (count($myrecallpack) > 0){
					$r_pack_id = $myrecallpack[0]['id'];
				}

			}
			else{
				//check if has reseller assigned
				if($mypack[0]['reseller_id'] != '0'){
					$reseller_pack = $this->master_model->getRecords('cf_recall_packs',array('consultant_id'=>$mypack[0]['reseller_id']));
					
					if(count($reseller_pack) > 0){
						$r_pack_id = $reseller_pack[0]['id'];
					}
				}

			}
			
		}


		
	  	$questions = $this->master_model->getRecords('cf_recall_guidance',array('recall_pack_id'=>$r_pack_id,'step_no'=>'2','disabled'=>'0'),'',array('category'=>'ASC','arrangement'=>'DESC'));
	  	$the_other_questions = $this->master_model->getRecords('cf_recall_guidance',array('recall_pack_id'=>$r_pack_id,'step_no !='=>'2','disabled'=>'0'),'',array('category'=>'ASC','arrangement'=>'DESC'));
		
	 	$eurtousdval = '234'; //$this->common_model->get_nexmo_balance('exchange');
		
		
		//select org module
		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$the_active_module = array('0'=>array('active_module'=>$theorg[0]['active_module']));
		
//actions_taken_whom: "aaaa a"
//aware_of_incident: "a a a"
//date: Fri Jan 08 2016 00:00:00 GMT+0800 (Malay Peninsula Standard Time)
//incident_date: "2016-01-08"
//incident_name: "sd ds"
//incident_no: "R007"
//inform: "crtonly"
//initiation_type: "0"
//investigate_started_whom: "aaaa"
//lat: 14.5493202
//lng: 121.04475290000005
//location: "Avant at The Fort, Taguig, Metro Manila, Philippines"
//overview: "s sdf f"


			
		$radiovalit = $_POST['initiation_type'];
		$inci_no = $_POST['incident_no'];
		$inci_base_no = substr($inci_no, -1);
		$inci_date = date_format(date_create($_POST['incident_date']), 'd-M-Y');
		$inci_name = $_POST['incident_name'];
		
		$step_one_answers = array(
			'', //add empty , question index start at 1
			$_POST['investigate_started_whom'],
			$_POST['actions_taken_whom'],
			$_POST['aware_of_incident'],
			$_POST['overview']
		);
		
		//set value variables
		if(count($questions) > 0){
			$the_q = 1;
			foreach($questions as $q=>$question){
				//validation
				${"step1q" . $the_q} = (isset($step_one_answers[$the_q])) ? $step_one_answers[$the_q] : '';
				$the_q++;	
			}
		}

		$aff_loc = array($_POST['location']); //$this->input->post('p_scnt',true);
		$aff_loc_lat = array($_POST['lat']); //$this->input->post('lat',true);
		$aff_loc_lng = array($_POST['lng']); //$this->input->post('lng',true);
		
		//add to one field the affected location
		$affected_loc = array();
		array_push($affected_loc, $aff_loc);
		array_push($affected_loc, $aff_loc_lat);
		array_push($affected_loc, $aff_loc_lng);

		$inform = $_POST['inform']; 


		//timezone datetime
		$nowtime = $this->common_model->userdatetime($cc_id);
		
		//insert scenario log 
		$recall_arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'incident_no'=>$inci_no,
			'cc_incident_id'=>$inci_base_no,
			'initiation_type'=>$radiovalit,
			'incident_status'=>'0',
			'initiation_date'=>$nowtime,
			'input_pack_id'=>$r_pack_id,
			'incident_date'=>$inci_date,
			'incident_name'=>$inci_name,
			'affected_location'=>json_encode($affected_loc),
		);

	


		$incident_id = $this->master_model->insertRecord('cf_recall',$recall_arr,true);
		//.insert recall step1 info
		
		$log_incident_id = $incident_id.'r';





		$count_var = 1;
		foreach($questions as $rc=>$rpack){
			
			$approvers = $this->master_model->getRecords('cf_recall_approvers', array('standbytask_id'=>$rpack['id']));
			$the_approvers = '';
			if(count($approvers) > 0){
				$the_approvers = $approvers[0]['approvers'];
			}


			$the_questions = array(
				'recall_id'=>$incident_id,
				'recall_pack_id'=>$continuity_id,
				'question'=>$rpack['question'],
				'answer'=>${'step1q'.$count_var},
				'task_guide'=>$rpack['guidance'],
				'approvers'=>$the_approvers,
				'category_id'=>$rpack['category'],
				'step_no'=>$rpack['step_no'],
				'status'=>'1'
			);
			$this->master_model->insertRecord('cf_recall_steps',$the_questions);
			$count_var++;
		}//insert step1 questions
		
		if(count($the_other_questions) > 0){

			foreach($the_other_questions as $toq=>$oq){
				
				$approvers = $this->master_model->getRecords('cf_recall_approvers', array('standbytask_id'=>$oq['id']));
				$the_approvers = '';
				if(count($approvers) > 0){
					$the_approvers = $approvers[0]['approvers'];
				}
				
				
				
				$the_questions = array(
					'recall_id'=>$incident_id,
					'recall_pack_id'=>$continuity_id,
					'question'=>$oq['question'],
					'task_guide'=>$oq['guidance'],
					'approvers'=>$the_approvers,
					'category_id'=>$oq['category'],
					'step_no'=>$oq['step_no'],
					'status'=>'0'
				);
				$this->master_model->insertRecord('cf_recall_steps',$the_questions);
			}
		}//insert other questions





		// Admin email
		$whr = array(
			'id'=>'1'
		);
		
		$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');
		
		// Sending mail to CC only
		$info_arr_cc = array(
			'from'=>$adminemail[0]['contact_email'],
			'to'=>$user_email,
			'subject'=>'Recall Incident has been initiated',
			'view'=>'incident-occurence-mail-to-cc'
		);
		
		$other_info_cc = array(
			'name'=>$user_full_name
		);
		
		$this->email_sending->sendmail($info_arr_cc,$other_info_cc);
		// Mail sending  to CC completed  
		
		
		
		//get sms message
		$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'notify-incident'));
		
		$from = 'CrisisFlo';				
		$message = array(
			'text' => $sms_message[0]['text']//'A notify incident has been submitted. Please log into your user panel.'
		);
		
		
		
			
		//Recall module activated
		if (strpos($the_active_module[0]['active_module'], '7') !== false){ 
			//sms sending
			//if($informsms=='crtonly'){
				$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
				$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
				array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$cc_id,'cf_crisis_response_team.org_id'=>$org_id));
	
				if(count($remind_result) > 0 ){
					foreach($remind_result as $rr=>$rem){
						
						$to = $this->master_model->decryptIt($rem['crt_mobile']);
						$response = $this->nexmo->send_message($from, $to, $message);
	
						foreach ($response->messages as $messageinfo) {
							$recipient = $messageinfo->{'to'};
							$status = $messageinfo->{'status'};
						}
	
						
						$det_contact = $this->master_model->decryptIt($rem['crt_first_name']);
						$det_contact .= ' ';
						$det_contact .= $this->master_model->decryptIt($rem['crt_last_name']);
						$det_contact .= ' - ';
						$det_contact .= $recipient;
	
						if ($status == '0'){
	
							$date_sent = date("Y-m-d H:i:s");
	
							foreach ($response->messages as $messageinfo) {
								$message_id = $messageinfo->{'message-id'};
								$messprice = $messageinfo->{'message-price'};
								$message_price = $eurtousdval * $messprice;
								$network = $messageinfo->{'network'};
							}
							
							//store sms records
							$sms_arr = array(
								'cc_id'=>$cc_id,
								'org_id'=>$org_id,
								'message_id'=>$message_id,
								'network'=>$network,
								'recipient'=>$det_contact,
								'price'=>$message_price,
								'status'=>$status,
								'date_sent'=>$date_sent,
								'log_scenario_id'=>$log_incident_id
							);
							
							$this->master_model->insertRecord('sms',$sms_arr);
							
							
						}
						else{
							
							foreach ($response->messages as $messageinfo) {
								$err_txt = $messageinfo->{'error-text'};
							}
	
							$err_stat = $status;
							$err_stat .= ' - ';
							$err_stat .= $err_txt;
							
							$sms_err_arr = array(
								'cc_id'=>$cc_id,
								'org_id'=>$org_id,
								'recipient'=>$det_contact,
								'price'=>'0',
								'status'=>$err_stat,
								'log_scenario_id'=>$log_incident_id
							);
							
							$this->master_model->insertRecord('sms', $sms_err_arr);
						}
							
					}
				}//end of sms notification
				
			}//end of strpos 7 = sms noti
				
				
				
			if($inform == 'crtonly'){
				
				$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
				$whr_remind_res = array(
					'cf_login_master.user_status'=>'1',
					'cf_crisis_response_team.cc_id'=>$cc_id,
					'cf_crisis_response_team.org_id'=>$org_id
				);
				
				$remind_result = $this->master_model->getRecords('cf_crisis_response_team', $whr_remind_res);
				
				$whr_cpack = array(
					'c_pack'=>'yes',
					'cc_id'=>$cc_id,
					'org_id'=>$org_id
				);
				
				$c_pack = $this->master_model->getRecords('cf_file_upload', $whr_cpack);
				
	
				
				if(count($remind_result) > 0 ){
					
					foreach($remind_result as $rr=>$rem){
						
						$the_cpack = array();
						if(count($c_pack) > 0){
							foreach($c_pack as $r=>$value){
								if($value['group_id'] =='0' || $value['group_id'] == $rem['group_id']){
									$the_cpack[] = 'uploads/crisis-document/'.$value['file_upload_name'];
								}
							}
						}
						
						$info_arr = array(
							'from'=>$adminemail[0]['contact_email'],
							'to'=>$this->master_model->decryptIt($rem['crt_email']),
							'subject'=>'Recall Incident has been initiated',
							'view'=>'incident-occurence-mail-to-crt'
						);
						
						$other_info = array(
							'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
							'cc_name'=>$user_full_name,
							'incident_name'=>$inci_name,
							'group_id'=>$rem['group_id'],
							'c_pack'=>$the_cpack,
							'attachments'=>$the_cpack
						);
						  
						$this->email_sending->sendmail($info_arr,$other_info);
					}
				}
				
				$data = array(
					'result'=>'OK'
				);
				header('Content-Type: application/json');
				header('Access-Control-Allow-Origin: *');
				echo json_encode($data, JSON_PRETTY_PRINT);
				return false;
			}
			
		else if($inform=='crtstk') {
			//--------------------------------------for crt
			$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
			
			$whr_remind_res = array(
				'cf_login_master.user_status'=>'1',
				'cf_crisis_response_team.cc_id'=>$cc_id,
				'cf_crisis_response_team.org_id'=>$org_id
			);
			
			$remind_result = $this->master_model->getRecords('cf_crisis_response_team', $whr_remind_res);

			$whr_cpack = array(
				'c_pack'=>'yes',
				'cc_id'=>$cc_id,
				'org_id'=>$org_id
			);
			
			$c_pack = $this->master_model->getRecords('cf_file_upload', $whr_cpack);

			if(count($remind_result) > 0 )
			{
				foreach($remind_result as $rr=>$rem)
				{

					$info_arr = array(
						'from'=>$adminemail[0]['contact_email'],
						'to'=>$this->master_model->decryptIt($rem['crt_email']),
						'subject'=>'Recall Incident has been initiated',
						'view'=>'incident-occurence-mail-to-crt'
					);
					
					$other_info = array(
						'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
						'cc_name'=>$user_full_name,
						'incident_name'=>$inci_name,
						'group_id'=>$rem['group_id'],
						'c_pack'=>$c_pack
					);
					$this->email_sending->sendmail($info_arr,$other_info);
				}
			}
			
			
			//--------------------------------------for stakeholder

			$whr = array('organization_id'=>$org_id);
	
			$org_name = $this->master_model->getRecords('organization_master',$whr,'*');
			$stks = unserialize($org_name[0]['stakeholders']);
			foreach($stks as $value) {
				$thestk = $this->master_model->getRecords('cf_stakeholder', array('login_id'=>$value));
				
				if(count($thestk) > 0){

					if($this->common_model->stkstatus($value) == 1){
						
						
						$info_arr = array(
							'from'=>$adminemail[0]['contact_email'],
							'to'=>$this->master_model->decryptIt($thestk[0]['stk_email_address']),
							'subject'=>'Recall Incident has been initiated',
							'view'=>'incident-occurence-mail-to-stk'
						);
						
						$other_info=array( 
							'name'=>$this->master_model->decryptIt($thestk[0]['stk_first_name']).' '.$this->master_model->decryptIt($thestk[0]['stk_last_name']),
							'cc_name'=>$user_full_name,
							'incident_name'=>$inci_name,
							'overview'=>$overview,
							'org_name'=>$org_name[0]['organization_name']
						);
						
						$this->email_sending->sendmail($info_arr,$other_info);
						
						
					}//end of check if stk completed

				}//end check if stk exist
			}//end loop for stks
			
			
			$data = array(
				'result'=>'OK'
			);
			header('Content-Type: application/json');
			header('Access-Control-Allow-Origin: *');
			echo json_encode($data, JSON_PRETTY_PRINT);
			return false;
		}
		
	}
	
	public function dashboard(){
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
		$recall_id = (isset($_POST['recall_id'])) ? $_POST['recall_id'] : '38';
		
		$data = $this->api_model->dashboard($recall_id, $cc_id, $org_id);
	
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
	
	public function auditlog(){
		
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';

		$data = $this->api_model->auditlog($cc_id, $org_id);
	
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
			
	}
	
	public function payments(){

		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
	
		$user_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$login_id));
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$org_members = $this->common_model->get_org_crts($login_id,$org_id);

		$billing = $this->common_model->single_payment($login_id, 'preview');
	

		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; // (base_url() != 'https://www.crisisflo.com/') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';
		Stripe::setApiKey($apikey);
		
		$customer = Stripe_Customer::retrieve($user_info[0]['stripe_customer_id']);
		$customer = $customer->__toArray(true);

		
		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'settings-payments',
			'success'=>'',
			'billing'=>$billing,
			'org_members'=>count($org_members),
			'user_info'=>$user_info,
			'customer'=>$customer
		);
		
		
		if($user_info[0]['single_user'] == 'y' && $user_info[0]['stripe_customer_id'] != '0') {
			
			$bill_info = $billing['orgs'][0]; 
			$prv_txt = ($bill_info['org_info'][0]['last_payment'] == '0000-00-00 00:00:00') ? 'Trial end Date: ' : 'Last Payment Date:';
			$prv_txt .= date_format(date_create($bill_info['last_payment']), 'd M Y');
			$nxt_date = date('Y-m-1 H:i:s', strtotime("+1 month"));
			$nxt_date = date_format(date_create($nxt_date), 'd M Y');
			
			
			$total_u = count($bill_info['org_crts']) + 1; //org_members + 1;
			$total = $total_u * $user_info[0]['single_price'];
			
			$data['bill_info'] = $bill_info;
			$data['prv_txt'] = $prv_txt;
			$data['nxt_date'] = $nxt_date;
			$data['total_u'] = $total_u;
			$data['total'] = $total;
			
		} 
	
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);
			
	}	
	
	public function stripe_card_token(){
	
		$user_level = (isset($_POST['user_level'])) ? $_POST['user_level'] : 0;
		$login_id = (isset($_POST['login_id'])) ? $_POST['login_id'] : '68';
		$cc_id = (isset($_POST['cc_id'])) ? $_POST['cc_id'] : '68';
		$org_id = (isset($_POST['org_id'])) ? $_POST['org_id'] : '30';
	
		$user_id = $login_id;
		$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$username = $this->common_model->getname($user_id);
		$email = $this->master_model->decryptIt($cc_info[0]['email_id']);
		$orgname = $this->common_model->getorgname($cc_info[0]['default_org']);
		$token = $_POST['token'];
		
		$data = array(
			'token'=>$token,
			'username'=>$username,
			'email'=>$email,
			'orgname'=>$orgname
		);



		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; // (base_url() != 'https://www.crisisflo.com/') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';
		
		Stripe::setApiKey($apikey);
	
	
		try {
			// Create a Customer
			$customer = Stripe_Customer::create(array(
			  "description" => $orgname,
			  "email" => $email,
			  //"plan" => $subs_plan_id, //subscrib user to plan 
			  "card" => $token // obtained with Stripe.js
			));
			
			if($customer->id){
				$login_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
			
				$cc_arr = array(
					'stripe_customer_id'=>$customer->id,
					'user_type'=>'live'
				);
				
				if($login_info[0]['trial_end'] == '0000-00-00 00:00:00'){
					$nowtime = date("Y-m-d h:i:s");
					$cc_arr['trial_end'] = $nowtime;
				}
				
				$this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$user_id));
				$data['result'] = 'success';
			}
			
			
			
		} catch(Stripe_CardError $e) {
		  // The card has been declined
			//$this->session->set_flashdata('error','The card has been declined.');
			$data['result'] = 'declined';
		}		
		

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data, JSON_PRETTY_PRINT);

	}
		
	
}

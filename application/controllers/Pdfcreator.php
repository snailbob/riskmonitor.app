<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdfcreator extends CI_Controller {


	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

	}


	public function pdf() {
		$this->load->helper('pdf_helper');
		$recall_id = $this->uri->segment('3');
		
		if(!empty($recall_id)){
			$recallid = $recall_id;
			$tobegen = array('steps'); //$_POST['tobegen'];
			
			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			if($active_mod == '5'){
				$recall = $this->master_model->getRecords('cf_recall', array('id'=>$recallid));
			}
			else{
				$recall = $this->master_model->getRecords('cf_continuity', array('id'=>$recallid));
			}
			$nowtime = $this->common_model->userdatetime();
			$cc_id = $this->session->userdata('logged_cc_login_id');
			$org_id = $this->session->userdata('cc_selected_orgnaization');
			
			
			if ($recall[0]['initiation_type'] == 0){
				$tasktype =  'Mock';
			}
			else{
				$tasktype = 'Live';
			}
			
			$data = array(
				'recallid'=>$recallid,
				'tobegen'=>$tobegen,
				'tasktype'=>$tasktype,
				'recall'=>$recall,
				'nowtime'=>$nowtime,
				'org_id'=>$org_id,
				'cc_id'=>$cc_id
			);
			$this->load->view('cc/pdfreport', $data);
			
			$includes = '';
			$gencount = 1;
			foreach($tobegen as $r){
				if($r == 'kpis'){
					$includes .= 'Recall KPIs';
				}
				if($r == 'cost'){
					$includes .= 'Cost Monitor';
				}
				if($r == 'blockers'){
					$includes .= 'Blockers';
				}
				if($r == 'steps'){
					$includes .= 'Steps';
				}
				
				if($gencount < count($tobegen)){
					$includes .= ', ';
				}
				$gencount++;
			}
			
			$arr = array(
				'recall_id'=>$recallid,
				'date_created'=>$nowtime,
				'org_id'=>$org_id,
				'includes'=>$includes,
				'cc_id'=>$cc_id
			);
			$this->master_model->insertRecord('reporting', $arr);

			
		}//if user submit
		
		
		
		else{
			echo 'no';
		}
	}
	
}

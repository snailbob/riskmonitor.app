<?php

class Signin extends CI_Controller{

	public function index(){

		$error = "";
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '37';
		$action_message = $this->common_model->get_message($action_message_id);


		if($this->session->userdata('logged_cc_login_id') != ""){

			redirect(base_url().'cc/dashboard');

		}

		if($this->session->userdata('logged_crt_login_id')!=""){

			redirect(base_url().'crt/dashboard');

		}

		else{

			if(isset($_POST['btn_cc_login'])){

				$this->form_validation->set_rules('user_name','Username','required|xss_clean');
				$this->form_validation->set_rules('pass_word','Password','required|xss_clean');

				$chk_arr = array(
					'email_id'=>$this->master_model->encryptIt($_POST['user_name']),
					'pass_word'=>md5($_POST['pass_word'])
				);

				if($this->form_validation->run()) {
					$data['result'] = 'ok';
					$data['rdr'] = 'signin';
					
					//check if google + or linked in
					if(isset($_POST['id'])){
						$id = $_POST['id'];
						$email = $_POST['email'];
						$type = $_POST['type'];

						$data['id'] = $id;
						$data['email'] = $email;
						$data['type'] = $type;
						$data['success'] = $action_message['success'];

						if($type == 'linkedin'){
							$whr_soc = array(
								'linkedin_id'=>$id
							);

							$chk_arr = array(
								'login.linkedin_id'=>$id
							);
					
						}
						else if($type == 'gplus'){
							$whr_soc = array(
								'gplus_id'=>$id
							);
							
							$chk_arr = array(
								'login.gplus_id'=>$id
							);
						}
						
						//check if login via email
						if($type != 'email'){
							$whr_soc['date_deleted'] = '0000-00-00 00:00:00';
							$users = $this->master_model->getRecords('cf_login_master', $whr_soc);
							
							if(count($users) == 0){
								$data['result'] = 'not_exist';
								echo json_encode($data);
								return false;
							}
						}
						
					
					}

					

					$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');
					
					$chk_arr['login.date_deleted'] = '0000-00-00 00:00:00';					
					$row = $this->master_model->getRecords('cf_login_master as login',$chk_arr);



					if(count($row) > 0){

						//cookie for remember me
						$year = time() + 31536000;
						if(isset($_POST['remember'])) {
							setcookie('email', $_POST['user_name'], $year);
							setcookie('password', $_POST['pass_word'], $year);
							setcookie('remember', $_POST['remember'], $year);
						}
						else {
							if(isset($_COOKIE['email'])) {
								$past = time() - 100;
								setcookie('email', '', $past);
								setcookie('password', '', $past);
								setcookie('remember', '', $past);
							}
						}

						$name_of_org = $this->master_model->getRecords('organization_master',array('organization_id'=>$row[0]['default_org']),'*');
						
							
						//check cc logged in already
						$log_sessions = $this->common_model->log_sessions($row[0]['login_id'], $name_of_org[0]['cc_id']);
						
						if($log_sessions['match'] > 0){
							$error = 'A Crisis coordinator already logged in.';
					
							$name_of_org[0]['log_sessions'] = $log_sessions;
							$name_of_org[0]['error'] = $error;
							$name_of_org[0]['result'] = 'no_user';
							echo json_encode($name_of_org[0]);
							return false;
						}
							
							
						//check if org disabled
						if($name_of_org[0]['enabled'] == 'N'){
							
							$error = 'Organization disabled.';
					
							$name_of_org[0]['error'] = $error;
							$name_of_org[0]['result'] = 'no_user';
							echo json_encode($name_of_org[0]);
							return false;
					
						}
						
						else{
							$user_login_for_session = $this->common_model->user_login_for_session($row[0]['login_id']);
							

							// $user_data = array(
							// 	'logged_cc_login_id'=>$name_of_org[0]['cc_id'], //row[0]['login_id'],
							// 	'logged_cc_email_id'=>$this->master_model->decryptIt($row[0]['crt_email']),
							// 	'logged_parent_cc'=>$name_of_org[0]['cc_id'], //row[0]['cc_id'],
							// 	'team_cc'=>$name_of_org[0]['cc_id'], //row[0]['cc_id'],
							// 	'user_id'=>$row[0]['login_id'], //row[0]['cc_id'],
							// 	'timezone'=>$row[0]['timezone'],
							// 	'country_id'=>$row[0]['country_id'],
							// 	'forum_last_visit'=>$row[0]['forum_last_visit'],
							// 	'location'=>$row[0]['location'],
							// 	'location_lat'=>$row[0]['location_lat'],
							// 	'location_lng'=>$row[0]['location_lng'],
							// 	'country_id'=>$row[0]['country_id'],
							// 	'countrycode'=>$this->master_model->decryptIt($row[0]['countrycode']),
							// 	'crt_digits'=>$this->master_model->decryptIt($row[0]['crt_digits']),
							// 	'my_id'=>$row[0]['login_id'],
							// 	'logged_user_type'=>$row[0]['user_type'],
							// 	'avatar'=>$row[0]['avatar'],
							// 	'logged_display_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
							// 	'first_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']),
							// 	'last_name'=>$this->master_model->decryptIt($row[0]['crt_last_name']),
							// 	'logged_user_level'=>$row[0]['user_level'],
							// 	'cc_selected_orgnaization'=>$row[0]['default_org'],
							// 	'task_review_date'=>$row[0]['tasks_review_date'],
							// 	'cc_selected_orgnaization_name'=>$name_of_org[0]['organization_name']
							// 	);

							// $user_data['user_orgs'] = $this->common_model->getcrtorgs($user_data['logged_cc_email_id']);
								

							$this->session->set_userdata($user_login_for_session);
							$data['user_info'] = $user_login_for_session;
							
							//redirect(base_url().'cc/dashboard/selectorganization');
							//set values to store
							$nowtime = $this->common_model->userdatetime();
							
							$arr = array(
								'org_id'=>$row[0]['default_org'],
								'cc_id'=>$name_of_org[0]['cc_id'], //name_of_org[0]['cc_id'],
								'crt_id'=>$row[0]['login_id'],
								'module_id'=>'5', //recall
								'type'=>'0', //cc_login
								'date_added'=>$nowtime
							);

							//insert arr to audit_log table
							$this->master_model->insertRecord('audit_log', $arr);
							
							if(!isset($_POST['id'])){
								redirect(base_url().'cc/dashboard/selectmodule');
							}
							else{
								echo json_encode($data);
								return false;
							}
							
						}//end if org not enabled
							
					}

					else{
						$error = $action_message['error'];
						
						if(isset($_POST['id'])){
							$data['error'] = $error;
							$data['result'] = 'no_user';
							echo json_encode($data);
							return false;
						}
						
					}
					

				}

				else {
					$error = $this->form_validation->error_string();
				}

			}

			$data = array(
				'page_title'=>'User Login',
				'error'=>$error,
				'success'=>''
			);

			$this->load->view('cc/index',$data);	

		}

	}

	public function org_select_form(){
		$org_id = $_POST['org_id'];
		$email = $this->session->userdata('logged_cc_email_id');
		$crt_email_encrypted = $this->master_model->encryptIt($email);
		
		$whr = array(
			'email_id'=>$crt_email_encrypted,
			'default_org'=>$org_id
		);
		$user = $this->master_model->getRecords('cf_login_master', $whr);

		if(!empty($user)){
			$user_login_for_session = $this->common_model->user_login_for_session($user[0]['login_id']);
		}
		
		$arr = $user_login_for_session;
		
		// $arr['cc_selected_orgnaization'] = $org_id;
		// $arr['cc_selected_orgnaization_name'] = $this->common_model->getorgname($org_id);

		$this->session->set_userdata($arr);
		echo json_encode($arr);
	}

	public function mysession(){
		$sess = $this->session->all_userdata();
		
		echo json_encode($sess);
	}
}

?>
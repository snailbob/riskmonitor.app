<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Communication extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

		

	}


	//------------------------------------index
	public function index(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);
	
		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
	
	
		if($active_module == '5'){
			$inci_name = 'recall';
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
		}
		else{
			$inci_name = 'continuity';
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
		}
	
	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type'=>$inci_name
		);
		
		$logs = $this->master_model->getRecords('bulk_notification', $whr, '*', array('date_added'=>'DESC'));
	
	
		$data=array(
			'page_title'=>'Communication File',
			'middle_content'=>'bulk_view',
			'error'=>'',
			'success'=>'',
			'logs'=>$logs,
			'incidents'=>$incidents
		);

	

		$this->load->view('cc/cc-view',$data);
	}


	//------------------------------------index
	public function addbulk(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$bulks = $this->master_model->getRecords('bulk_notification',array('cc_id'=>$cc_id,'org_id'=>$org_id),'*',array('file_count'=>'DESC'));
		if(count($bulks) > 0){
			$newcount = $bulks[0]['file_count'] + 1;
		}
		else{
			$newcount = 1;
		}
		
		$file_name = 'Bulk Notification '.$newcount;
		
		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'file_count'=>$newcount,
			'name'=>$file_name,
			'date_added'=>$nowtime
		);

		$bulk_id = $this->master_model->insertRecord('bulk_notification', $arr, true);
		echo $bulk_id;
	}
	

	//------------------------------------addupdatebulk
	public function addupdatebulk(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$name = $_POST['bulk_name'];
		$assoc_incident = $_POST['assoc_incident'];
		$id = $_POST['bulk_id'];


		//get validation message from admin
		$action_message_id = '69';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		if($active_mod == '5') { //recall
			$inci_type = 'recall';
		}
		else{
			$inci_type = 'continuity';
		}
		
		
		if($id != ''){
			$this->master_model->updateRecord(
				'bulk_notification',
				array('name'=>$name, 'type'=>$inci_type, 'incident_id'=>$assoc_incident),
				array('id'=>$id)
			);
			
			$this->session->set_flashdata('success', $action_message['success']);
			$data['type'] = 'update'; 
			$data['id'] = $id; 
		}
		else{
			
			$bulks = $this->master_model->getRecords('bulk_notification',array('cc_id'=>$cc_id,'org_id'=>$org_id),'*',array('file_count'=>'DESC'));
			if(count($bulks) > 0){
				$newcount = $bulks[0]['file_count'] + 1;
			}
			else{
				$newcount = 1;
			}
			
			$nowtime = $this->common_model->userdatetime();

			$arr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'file_count'=>$newcount,
				'name'=>$name,
				'incident_id'=>$assoc_incident,
				'type'=>$inci_type,
				'date_added'=>$nowtime
			);
			
			$bulk_id = $this->master_model->insertRecord('bulk_notification', $arr, true);

			//$this->session->set_flashdata('success','Bulk Notification successfully added.');
		
			$data['type'] = 'add'; 
			$data['id'] = $bulk_id; 
		}
		$data['name'] = $name; 
		echo json_encode($data);
	
	}
	
	//------------------------------------index
	public function newcontact(){
		$name = $_POST['bulk_name'];
		$contact_email = $_POST['contact_email'];
		$contact_mobile = $_POST['contact_mobile'];
		$letter_code = substr($_POST['letter_code'], 0, 2);
		$id = $_POST['bulk_id'];
		
		//get validation message from admin
		$action_message_id = '70';
		$action_message = $this->common_model->get_message($action_message_id);
			
		//validate country code
		if($letter_code !=''){
			
			$countries = $this->master_model->getRecords('country_t', array('iso2'=>strtoupper($letter_code)));
			if(count($countries) > 0){
				if($contact_mobile != ''){
					$contact_mobile = $countries[0]['calling_code'].$contact_mobile;
				}
			}
			else{
				
				$this->session->set_flashdata('error','Country Code is invalid.');
				$data['result'] = 'code_invalid'; 
				
				echo json_encode($data);
				return false;
			}
		}
		
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$data['bulk_id'] = $id; 
		$data['cc_id'] = $cc_id; 
		$data['org_id'] = $org_id; 
		$data['name'] = $name; 
		$data['emails'] = $contact_email; 
		$data['mobile'] = $contact_mobile; 
		$data['countrycode'] = strtoupper($letter_code); 
		
		$this->master_model->insertRecord('bulk_data', $data);
						$this->session->set_flashdata('success',$action_message['success']);
		$data['id'] = $id; 
		
		echo json_encode($data);
	
	}
	
	
	
	//------------------------------------index
	public function manage(){
		$bulk_id = $this->uri->segment(4);
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);
		
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$countries = $this->master_model->getRecords('country_t','','*', array('short_name'=>'ASC'));

		//check if a stripe customer
		$org_stripe = 0;
		$stripe_customer_id = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
		if($stripe_customer_id[0]['stripe_customer_id'] != '0'){
			$org_stripe = 1;
		}
		$nexmo_balance = $this->common_model->get_nexmo_balance();

	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'id'=>$bulk_id,
			//'module_id'=>$active_module
		);
		
		$bulk = $this->master_model->getRecords('bulk_notification', $whr, '*', array('date_added'=>'DESC'));
	
		if(count($bulk) == 0){
			$this->session->set_flashdata('error','Communication File selected not found.');
			redirect('cc/communication');
			return false;
		}
		
		$data_whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'bulk_id'=>$bulk[0]['id'],
		);
		$bulk_data = $this->master_model->getRecords('bulk_data', $data_whr, '*', array('id'=>'DESC'));
	
		$data=array(
			'page_title'=>'Manage Communication File',
			'middle_content'=>'bulk_view_manage',
			'error'=>'',
			'success'=>'',
			'bulk'=>$bulk,
			'bulk_data'=>$bulk_data,
			'countries'=>$countries,
			'org_info'=>$org_info,
			'org_stripe'=>$org_stripe,
			'nexmo_balance'=>$nexmo_balance
		);

		$this->load->view('cc/cc-view',$data);
	}



	public function countrycode(){
		
		$countries = $this->master_model->getRecords('country_t');
	
		$data=array(
			'page_title'=>'Country Code Reference',
			'middle_content'=>'bulk_view_country_codes',
			'error'=>'',
			'success'=>'',
			'countries'=>$countries
		);

		$this->load->view('cc/cc-view',$data);
	
	}
	

	public function upload_attach(){

		$output_dir = "uploads/users-bulk/attachments/";
		$bulk_id = $_POST['bulk_id'];

		$file_input = $_FILES["bulka_input"];
		if(isset($file_input))
		{

			//Filter the file types , if you want.
			if ($file_input["error"] > 0)
			{
			  echo 'error_upload';//$_FILES["file"]["error"] . "<br>";
			}
			else
			{
				$the_unique = uniqid();
				$thefilename = $the_unique.str_replace(' ', '_', $file_input["name"]);
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($file_input["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);


				$att_id = $this->master_model->insertRecord('bulk_attachments', array('file_name'=>$thefilename, 'bulk_id'=>$bulk_id));

				echo ' <a href="'.base_url().$output_dir.$thefilename.'" target="_blank" id = "'.$the_unique.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$file_input["name"].'</a> <span class="delbulk_attach" data-filename="'.$thefilename.'" data-attid="'.$att_id.'" data-unique="'.$the_unique.'">&times;</span>';

			}

		
		}		

	}	
	
	
	public function delete_bulkattached(){
		$filename = $_POST['filename'];
		$attid = $_POST['attid'];
		
		$data = array();

		if($this->master_model->deleteRecord('bulk_attachments','id',$attid)) {

			@unlink('uploads/message-uploads/'.$file_name);

			//$this->session->set_flashdata('success',' Document successfully deleted.');
			$data['result'] = 'success';

		}

		echo json_encode($data);
	
	}


	
	public function upload(){

		$output_dir = "uploads/users-bulk/";
		
		$bulk_id = $_POST['bulk_id'];
		

		//get validation message from admin
		$action_message_id = '70';
		$action_message = $this->common_model->get_message($action_message_id);

		if(isset($_FILES["mycsv_input"])) {
			
			if(substr($_FILES["mycsv_input"]["name"], -3) == 'csv'){

				//Filter the file types , if you want.
				if ($_FILES["mycsv_input"]["error"] > 0) {
					
				  echo 'error_upload';//$_FILES["file"]["error"] . "<br>";
				  
				}
				else
				{
					$thefilename = uniqid().str_replace(' ', '_', $_FILES["mycsv_input"]["name"]);
					
					//move the uploaded file to uploads folder;
					move_uploaded_file($_FILES["mycsv_input"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);
									
					$the_emails = $this->csv($thefilename, $bulk_id);
					
					if($the_emails != ''){
						$this->session->set_flashdata('success', $action_message['success']);
						
						echo $the_emails;
					}
					else{
						echo 'not_readable';
					}
					
	
				}

			}//end of check file ext
			else{
				echo 'not_valid_file';
			}
		
		}		

	}
	
	public function empty_arr(){
	
		$arr = array();
		
		echo serialize($arr);
	}
	


	public function csv($csv_file, $bulk_id){

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		
		$bulk_noti = $this->master_model->getRecords('bulk_notification', array('cc_id'=>$cc_id,'org_id'=>$org_id), '*', array('file_count'=>'DESC'));

		if(count($bulk_noti) > 0){
			$bulk_count = $bulk_noti[0]['file_count'] + 1;
		}
		else{
			$bulk_count = 1;
		}

		//store uploaded file to delete all
		$the_bulk = $this->master_model->getRecords('bulk_notification', array('id'=>$bulk_id));
		
		
		if($the_bulk[0]['file_name'] !=''){
			
			$files = unserialize($the_bulk[0]['file_name']);
			$files[] = $csv_file;

			$arr = array(
				'file_name'=>serialize($files),
			);
			
			$this->master_model->updateRecord('bulk_notification',$arr, array('id'=>$bulk_id));

		}else{
			$files = array();
			$files[] = $csv_file;

			$arr = array(
				'file_name'=>serialize($files),
			);
			
			$this->master_model->updateRecord('bulk_notification',$arr, array('id'=>$bulk_id));

		}
		



		$f = fopen(base_url()."uploads/users-bulk/".$csv_file, "r");

		$name = array();
		$emails = array();
		$countrycode = array();
		$mobile = array();
		while (($data = fgetcsv($f, 1000, ",")) !== FALSE) {
			
			if(array_key_exists('0', $data)){
				$name[] = $data[0];
			}
			if(array_key_exists('1', $data)){
				$emails[] = $data[1];
			}
			if(array_key_exists('2', $data)){
				$countrycode[] = $data[2];
			}
			if(array_key_exists('3', $data)){
				$mobile[] = $data[3];
			}
		}		
		
		
		$output = array(); 
		$count_inv_mobile = 0;
		$count_inv_email = 0;
		$email_rows_error = array();
		$sms_rows_error = array();
		for($i = 1; $i < count($name); $i++){
			
			if($mobile[$i] !=''){
				if(!ctype_digit($mobile[$i])){
					$count_inv_mobile += 1;
					$sms_rows_error[] = $i;
				}
			}
			
			if (!filter_var($emails[$i], FILTER_VALIDATE_EMAIL)) {
				$count_inv_email += 1;
				$email_rows_error[] = $i;
			}
		}
		
		if($count_inv_email == 0 && $count_inv_mobile == 0 ){
	
			for($i = 1; $i < count($name); $i++){
				$iso2code = substr($countrycode[$i], 0, 2);
				//get mobile country code
				$m_counrycode = $this->master_model->getRecords('country_t', array('iso2'=>$iso2code));
				
				if(count($m_counrycode) > 0){
					$mobile_no = $m_counrycode[0]['calling_code'].$mobile[$i];
				}
				else{
					$mobile_no = $mobile[$i];
				}
				
				
				//identify if valid
				if(ctype_digit($mobile[$i])){
					$valid_mobile = '1';
				}
				else{
					$valid_mobile = '0';
				}
				
				if (filter_var($emails[$i], FILTER_VALIDATE_EMAIL)) {
					$valid_email = '1';
				}
				else{
					$valid_email = '0';
				}
	
				$output['name'] = $name[$i];
				$output['emails'] = $emails[$i];
				$output['countrycode'] = $iso2code;
				$output['mobile'] = $mobile_no;
				$output['bulk_id'] = $bulk_id;
				$output['email_valid'] = $valid_email;
				$output['mobile_valid'] = $valid_mobile;
				$output['cc_id'] = $cc_id;
				$output['org_id'] = $org_id;
				
				$this->master_model->insertRecord('bulk_data', $output);
				
			}
			
			return $bulk_id;
		
		
		}//end of no invalid
		
		else{
			$result = $count_inv_email .' invalid emails and '.$count_inv_mobile .' invalid mobile numbers was	 found.';
			
			
			
			if(count($email_rows_error) > 0){
				
				$result .= '<table class="table table-striped table-condensed table-hover text-left text-muted">';
					
			
					$result .= 
						'<thead><tr>
						  <th>Invalid Emails</th>
						</tr></thead>';
					
					 $result .=  '<tbody>';
	  

				$i = 1;
				foreach($email_rows_error as $err_email){
			
					$result .= 
						'<tr>
						  <td><i class="fa fa-times-circle text-danger"></i> <i>'.$emails[$err_email].'</i> on line '.$err_email.'</td>
						</tr>';
			
					$i++;
				}
		
				$result .= '
					  </tbody>
					</table>';		
				
			}//end display invalid email
		
	
	  
			
			if(count($sms_rows_error) > 0){
				
				$result .= '<table class="table table-striped table-condensed table-hover text-left text-muted">';
					
			
					$result .= 
						'<thead><tr>
						  <th>Invalid Mobile Numbers</th>
						</tr></thead>';
					
					 $result .=  '<tbody>';
	  

				$i = 1;
				foreach($sms_rows_error as $err_sms){
			
					$result .= 
						'<tr>
						  <td><i class="fa fa-times-circle text-danger"></i> <i>'.$mobile[$err_sms].'</i> on line '.$err_sms.'</td>
						</tr>';
			
					$i++;
				}
		
				$result .= '
					  </tbody>
					</table>';		
				
			}//end display invalid mobile
			
			
			
			$result .= '<table class="table table-striped table-condensed table-hover text-left text-muted">';
			
	
			$result .= 
				'<thead><tr>
				  <th colspan="4">Valid Data</th>
				</tr></thead>';
			
			 $result .=  '<tbody>';
		  
			
			$count_valid = 0;
			for($i = 1; $i < count($name); $i++){
				
				if(!in_array($i, $sms_rows_error) && !in_array($i, $email_rows_error)){
	

				
					$result .= 
						'<tr>
						  <td><i class="fa fa-check-circle text-success"></i> '.$name[$i].'</td>
						  <td>'.$emails[$i].'</td>
						  <td>'.$countrycode[$i].'</td>
						  <td>'.$mobile[$i].'</td>
						</tr>';
				
					$count_valid++;

				}
			
			}
	
			if($count_valid == 0){
				
				$result .= 
					'<tr>
					  <td colspan="4">No valid row found.</td>
					</tr>';
				
			}
	
			$result .= '
				  </tbody>
				</table>';		
	
	
			
			
			
			
			return $result;	
			
		}
		
//		return $output;
	}
	
	
	public function get_sms_price(){
		$bulk_data_id = $_POST['bulk_data_id'];
		
		$bulk_data = $this->master_model->getRecords('bulk_data', array('id'=>$bulk_data_id));
        
		if($bulk_data[0]['price'] == ''){
			// load library
			$this->load->library('nexmo');
			// set response format: xml or json, default json
			$response = $this->nexmo->get_pricing($bulk_data[0]['countrycode']);
			$this->nexmo->set_format('json');
			
			$eurtousdval = $this->common_model->get_nexmo_balance('exchange');
			
			
/*			//new json api source for currency
			$url = "http://currency-api.appspot.com/api/EUR/USD.json?key=0d58e6fb444400a40e03e3f04bc80cb7647b0912";
			
			$result = file_get_contents($url);
			$exrate = json_decode($result);
			
			
			if (is_object($exrate)){//check if it return json
				if ($exrate->success){
					$eurtousdval = $exrate->rate;
				}
				else{
					$eurtousdval = 1.3628; //static value for euro to usd
				}
			}
			else{
				$eurtousdval = 1.3628; //static value for euro to usd
			}
		
*/			
			
			$eur_price = $response->mt;
			$the_price = $eur_price*$eurtousdval;

			$this->master_model->updateRecord('bulk_data', array('price'=>$the_price), array('id'=>$bulk_data_id));

		}
		else{
			$the_price = $bulk_data[0]['price'];
		}

		$data = array(
			'result'=>'success',
			'type'=>'each',
			'bulk_data_id'=>$bulk_data_id,
			'price'=>$the_price
		);
		
		echo json_encode($data);
		
	}
		
		
	public function get_total_price(){
		$bulk_data_id = $_POST['bulk_data_id'];
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$organization = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id,'cc_id'=>$cc_id));
		
		$org_sms_budget = $organization[0]['sms_credit'];
		
		$the_total = 0;
		foreach($bulk_data_id as $val){
		$bulk_data = $this->master_model->getRecords('bulk_data', array('id'=>$val));
			$the_total += $bulk_data[0]['price'];
		}
		
		
		//check user sms balance
		$remaining_balance = $org_sms_budget - $the_total;
		if($remaining_balance > 0){
			$status = 'ok';
		}
		else{
			$status = 'not_ok';
		}
		
		$data = array(
			'result'=>'success',
			'type'=>'total',
			'bulk_data_id'=>$bulk_data_id,
			'the_total'=>$the_total,
			'status'=>$status
		);
		
		echo json_encode($data);
	}
			
		
	
	public function send_bulk_email(){
		$bulk_data_id = $_POST['bulk_data_id'];
		$bulk_type = $_POST['bulk_type'];
		$smsmessage = $_POST['smsmessage'];
		$emailmessage = $_POST['emailmessage'];
		
		$bulk_data = $this->master_model->getRecords('bulk_data', array('id'=>$bulk_data_id));

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
	
	
		//select org module
		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$the_active_module = array('0'=>array('active_module'=>$theorg[0]['active_module']));
	
		//get validation message from admin
		$action_message_id = '71';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '72';
		$action_message2 = $this->common_model->get_message($action_message_id2);

		//save new sms and email messages
		$new_messages = array(
			'sms_message'=>$smsmessage,
			'email_message'=>$emailmessage
		);
		
		$this->master_model->updateRecord('bulk_notification', $new_messages, array('id'=>$bulk_data[0]['bulk_id']));
		

		if($bulk_type == 'emailsms'){
            if (strpos($the_active_module[0]['active_module'], '7') !== false){
				$flashdata = $action_message['success'];
				$this->session->set_flashdata('success',$flashdata);
			}
			else{
				$flashdata = $action_message['error'];
				$this->session->set_flashdata('success',$flashdata);
			}
		}
		else{
			$flashdata = $action_message2['success'];
			$this->session->set_flashdata('success',$flashdata);
		}

		
		

		// Admin email
		$whr=array('id'=>'1');
		$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');
		
		// set email data
		$info_arr_cc = array(
			'from'=>$adminemail[0]['contact_email'],
			'to'=>$bulk_data[0]['emails'],
			'subject'=>'Recall Incident has been initiated',
			'view'=>'communication-bulk-email'
		);
		
		$other_info_cc = array(
			'name'=>$bulk_data[0]['name'],
			'emailmessage'=>$emailmessage,
			'bulkid'=>$bulk_data[0]['id'],
			'emailmd5'=>md5($bulk_data[0]['emails']),
			'bulkidmd5'=>md5($bulk_data[0]['id'])
		);
		
		$this->email_sending->sendmail($info_arr_cc,$other_info_cc);
		// Mail sending completed  
		
		//mark sent email
		$this->master_model->updateRecord('bulk_data',array('email_sent'=>'1'),array('id'=>$bulk_data[0]['id']));
		
		$data = array(
			'result'=>'success',
			'type'=>'sendbulk',
			'bulk_data_id'=>$bulk_data_id,
			'bulk_type'=>$bulk_type
		);
		
		if($bulk_type == 'emailsms'){
			
            if (strpos($the_active_module[0]['active_module'], '7') !== false){ 
			
				
				
				// load library
				$this->load->library('nexmo');
				// set response format: xml or json, default json
				$this->nexmo->set_format('json');
	
				$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'bulk-sms'));
				
				$from = 'CrisisFlo';				
				$message = array(
					'text' => $smsmessage // $sms_message[0]['text']
				);
	
				
				$to = $bulk_data[0]['mobile'];
				$response = $this->nexmo->send_message($from, $to, $message); //sms sending
				
				//get recipient
				$recipient = $response->messages[0]->{'to'};
				$det_contact = $bulk_data[0]['name']. ' - ';
				$det_contact .= $recipient;
				
				
				//if success update org sms budget, set sms status to 1
				if($response->messages[0]->status == '0'){
					$sms_status = '1';
					
					$organization = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id,'cc_id'=>$cc_id));
					
					$org_sms_budget = $organization[0]['sms_credit'];
							
					//new json api source for currency
					$eurtousdval = $this->common_model->get_nexmo_balance('exchange');
					
		
					
					$date_sent = date("Y-m-d H:i:s");
	
					foreach ($response->messages as $messageinfo) {
						$message_id = $messageinfo->{'message-id'};
						$messprice = $messageinfo->{'message-price'};
						$message_price = $eurtousdval * $messprice;
						$network = $messageinfo->{'network'};
					}
					
					//store sms records
					$status_nexmo = '0';
					$sms_arr = array(
						'cc_id'=>$cc_id,
						'org_id'=>$org_id,
						'message_id'=>$message_id,
						'network'=>$network,
						'recipient'=>$det_contact,
						'price'=>$message_price, //message_price,
						'status'=>$status_nexmo,
						'date_sent'=>$date_sent,
						'log_scenario_id'=>'notifications_'.$bulk_data[0]['bulk_id']
					);
					
					$this->master_model->insertRecord('sms',$sms_arr);
					
					
					//deduct sms value
					//$smsval = $response->messages[0]->{'message-price'};
					
					$new_org_balance = $org_sms_budget - $message_price; // $smsval;
					
					$sms_org_array = array(
						'sms_credit'=>$new_org_balance
					);
					
					$whr_sms_org = array(
						'organization_id'=>$org_id
					);
					
					$this->master_model->updateRecord('organization_master',$sms_org_array, $whr_sms_org);
					
				}
				else{//if not success
					$sms_status = '0';
					
					foreach ($response->messages as $messageinfo) {
						$err_txt = $messageinfo->{'error-text'};
						$status_nexmo = $messageinfo->{'status'};
					}
	
					$err_stat = $status_nexmo;
					$err_stat .= ' - ';
					$err_stat .= $err_txt;
					
					$sms_err_arr = array(
						'cc_id'=>$cc_id,
						'org_id'=>$org_id,
						'recipient'=>$det_contact,
						'price'=>'0',
						'status'=>$err_stat,
						'log_scenario_id'=>'notifications_'.$bulk_data[0]['bulk_id']
					);
					
					$this->master_model->insertRecord('sms', $sms_err_arr);
	
				}
			
				$arr = array(
					'sms_sent'=>$sms_status,
					//'sms_data'=>serialize($response)
				);
				
				$this->master_model->updateRecord('bulk_data', $arr, array('id'=>$bulk_data_id));
				
				$data['response'] = $response;
				$data['to'] = $to;
				$data['sms_message'] = $sms_message[0]['text'];
				$data['sms_status'] = 'sent';			
			
			
			}//end check if sms module activated
		}//end emailsms
		
		echo json_encode($data);
		
	}
	
	
	public function delete_contact(){
		$contact_id = $this->uri->segment(4);
		$bulk_id = $this->uri->segment(5);
		
		//get validation message from admin - generic delete = 63
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if($this->master_model->deleteRecord('bulk_data','id',$contact_id)) {

			$this->session->set_flashdata('success', $action_message['success']);

			redirect('cc/communication/manage/'.$bulk_id);

		}
	
	}
	
	public function delete_bulk(){
		$bulk_id = $this->uri->segment(4);

		$bulk_notification = $this->master_model->getRecords('bulk_notification', array('id'=>$bulk_id));
		
		$doc_name = unserialize($bulk_notification[0]['file_name']);
		
		//get validation message from admin - generic delete id = 63
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);

		if(count($doc_name) > 0){
			
			foreach($doc_name as $doc){
				//delete file
				unlink('uploads/users-bulk/'.$doc);
			}
		}
		
		if($this->master_model->deleteRecord('bulk_notification','id',$bulk_id)) {

			$this->master_model->deleteRecord('bulk_data','bulk_id',$bulk_id);
			$this->session->set_flashdata('success',$action_message['success']); //' File successfully deleted.');

			redirect('cc/communication');

		}

	}
	
	public function updatestatus(){
	
		$status = $_POST['title'];
		$db = $_POST['db'];
		$id = $_POST['id'];
		
		if($status == 'enabled'){
			$status = '1';
			$data['title'] = 'disabled';
			$data['class'] = 'btn-danger';
			$data['text'] = 'fa-times';
		}
		else{
			$status = '0';
			$data['title'] = 'enabled';
			$data['class'] = 'btn-success';
			$data['text'] = 'fa-check';
		}
		
		$this->master_model->updateRecord($db, array('enabled'=>$status), array('id'=>$id));
		
		echo json_encode($data);
	}
	
	public function top_up(){
		$amount = $_POST['amount'];
		$nexmo_balance = $_POST['nexmo_balance'];
		
		$user_id = $this->session->userdata('team_cc');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		
		$sms_credit = $org_info[0]['sms_credit'] + $amount;
		$waiting_credit = $org_info[0]['waiting_sms_credit'] + $amount;
		
		$stripe_customer_id = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		
		$stripe_cust = $stripe_customer_id[0]['stripe_customer_id'];
		$data['result'] = 'error';

		//get validation message from admin - generic delete id = 63
		$action_message_id = '68';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '73';
		$action_message2 = $this->common_model->get_message($action_message_id2);


		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0 ");
		
		if($stripe_cust != '0'){
			try{
				Stripe_Charge::create(array(
				  "amount" => $amount*100,
				  "currency" => "usd",
				  "customer" => $stripe_cust, // obtained with Stripe.js
				  "description" => "purchase sms credit"
				));
				
					
				//store if nexmo balance larger than purchased
				if($nexmo_balance > $amount){
					
					$this->session->set_flashdata('success', $action_message['success']);
					
					$this->master_model->updateRecord('organization_master', array('sms_credit'=>$sms_credit), array('organization_id'=>$org_id));
					
				}
				else{
					
					$this->session->set_flashdata('success', $action_message2['success']);
					$this->master_model->updateRecord('organization_master', array('waiting_sms_credit'=>$waiting_credit), array('organization_id'=>$org_id));
				
				}
				
				$data['result'] = 'success';
				
			}
			
			catch(Stripe_CardError $e){
				$data['result'] = 'declined';
			}
		}//end customer exist
		
		else{
			$data['result'] = 'notcustomer';
		}//end not a stripe customer

		echo json_encode($data);
	}
	
	
	
	public function addcust_purchase(){
		

		$amount = $_POST['amount'];
		$nexmo_balance = $_POST['nexmo_balance'];
		$stripeToken = $_POST['stripeToken'];
		
		$user_id = $this->session->userdata('team_cc');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		
		$sms_credit = $org_info[0]['sms_credit'] + $amount;
		$waiting_credit = $org_info[0]['waiting_sms_credit'] + $amount;

		$data['amount'] = $amount;
		$data['stripeToken'] = $stripeToken;
		
		
		//get validation message from admin - generic delete id = 63
		$action_message_id = '68';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '73';
		$action_message2 = $this->common_model->get_message($action_message_id2);
		
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0 ");
		
		// Create the charge on Stripe's servers - this will charge the user's card
		try {
		
			// Create a Customer
			$customer = Stripe_Customer::create(array(
			  "description" => "crisisflo_user",
			  "email" => $this->session->userdata('logged_cc_email_id'),
			  "card" => $stripeToken // obtained with Stripe.js
			));
			
			
			$charge = Stripe_Charge::create(array(
			  "amount" => $amount*100, // amount in cents, again
			  "currency" => "usd",
			  //"card" => $stripeToken,
			  "customer" => $customer->id,
			  "description" => 'purchase sms credit')
			);
			
			
			$this->master_model->updateRecord('cf_login_master', array('stripe_customer_id'=>$customer->id), array('login_id'=>$user_id));
			
			//store if nexmo balance larger than purchased
			if($nexmo_balance > $amount){
				$this->session->set_flashdata('success', $action_message['success']);
				$this->master_model->updateRecord('organization_master', array('sms_credit'=>$sms_credit), array('organization_id'=>$org_id));
			}
			else{
				$this->session->set_flashdata('success', $action_message2['success']);
				$this->master_model->updateRecord('organization_master', array('waiting_sms_credit'=>$waiting_credit), array('organization_id'=>$org_id));
			}

			$data['result'] = 'success';
			
			
			
		} catch(Stripe_CardError $e) {
		  // The card has been declined
			$this->session->set_flashdata('error', $action_message['error']);
			$data['result'] = 'declined';
		}		

		echo json_encode($data);
		
	
	}
			
}

?>
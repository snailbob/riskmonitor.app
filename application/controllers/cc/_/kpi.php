<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Kpi extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();
		
		
		$org_modules = $this->session->userdata('org_module');
		if (strpos($org_modules, '0') === false){
			$this->session->set_flashdata('error', ' Recall KPI Module not activated for this organization.');
			redirect('cc/dashboard');
		}
	
		

	}


	//------------------------------------index
	public function index(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);
	
	
	
		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
	
	
		if($active_module == '5'){
			$incident_type = '5';
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

		}
		else{ //continuity = 8
			$incident_type = '8';
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));

		}
		
		//check incident count
		if(count($incidents) > 0){
			redirect('cc/kpi/manage/'.$incidents[0]['id']);
			return false;
		}
//		else{
//			$this->session->set_flashdata('error','No open incident.');
//			redirect('cc/dashboard');
//		}
	
	
	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'incident_type'=>$incident_type
		);
		
		$kpis = $this->master_model->getRecords('bulk_kpi', $whr, '*', array('date_added'=>'DESC'));
	
		$data=array(
			'page_title'=>'Communication File',
			'middle_content'=>'bulk_kpi_view',
			'error'=>'',
			'success'=>'',
			'kpis'=>$kpis,
			'incidents'=>$incidents
		);

	

		$this->load->view('cc/cc-view',$data);
	}


	//------------------------------------manage
	public function manage(){

		$incident_id = $this->uri->segment(4);
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);


		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
	
	
		if($active_module == '5'){
			$incident_type = '5';
			$inci_type = 'recall';
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

		}
		else{ //continuity = 8
			$incident_type = '8';
			$inci_type = 'continuity';
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
		}
	
		//check if incident empty
		if(count($the_incident) == 0){
			redirect('cc/kpi');
		}

		//get kpi owners
		$whr_dist_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'dist',
			'deleted_dist'=>'0'
		);
		$whr_market_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'mkt',
			'deleted_mkt'=>'0'
		);
		$whr_all_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type
		);
		$whr_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all'
		);
		$whr_notdeldist_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all',
			'deleted_dist'=>'0'
		);
		$whr_notdelmkt_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all',
			'deleted_mkt'=>'0'
		);
		$dist_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_dist_owner);
		$market_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_market_owner);
		$all_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_all_owner);
		$both_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_both_owner);
		$both_notdeldist_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_notdeldist_both_owner);
		$both_notdelmkt_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_notdelmkt_both_owner);
		
		
		//get noti count kpi
		$whr_noti = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type'=>$inci_type,
			'incident_id'=>$incident_id
		);
		
		$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
		

		$email_sent_count = 0;
		$email_confirmed_count = 0;
		$sms_sent_count = 0;
		if(count($notices_sent) > 0){                
			foreach($notices_sent as $r=>$ns){
				//email sent count
				$whr_email_sent = array(
					'bulk_id'=>$ns['id'],
					'email_sent'=>'1'
				);
				$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

				//email confirmed count
				$whr_email_confirmed = array(
					'bulk_id'=>$ns['id'],
					'email_confirmed'=>'1'
				);
				$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


				//sms sent count
				$whr_sms_sent = array(
					'bulk_id'=>$ns['id'],
					'sms_sent'=>'1'
				);
				$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
				
				$email_sent_count += $bulk_data_email_sent;
				$email_confirmed_count += $bulk_data_email_confirmed;
				$sms_sent_count += $bulk_data_sms_sent;
			
			}
			
		}

		//get notification kpi
		$noti_kpi = 0;
		if($email_sent_count != 0){
			$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
		}
		


	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'incident_type'=>$incident_type,
			'incident_id'=>$incident_id
		);
		
		$kpis = $this->master_model->getRecords('bulk_kpi', $whr, '*', array('date_added'=>'DESC'));
	
	
		//set kpi variables
		$all_kpi = array();
		
		$all_kpi[] = array(
			'type'=>'comp_kpi',
			'dbcell'=>'comp_data',
			'kpistatus'=>'comp_stat',
			'name'=>'Units under Company control'
		);
	
		$all_kpi[] = array(
			'type'=>'dist_kpi',
			'dbcell'=>'dist_data',
			'kpistatus'=>'dist_stat',
			'name'=>'Units in distribution'
		);
	
		$all_kpi[] = array(
			'type'=>'market_kpi',
			'dbcell'=>'market_data',
			'kpistatus'=>'market_stat',
			'name'=>'Units in consumer market'
		);

	
	
		$data=array(
			'page_title'=>'Communication File',
			'middle_content'=>'bulk_kpi_manage',
			'error'=>'',
			'success'=>'',
			'kpis'=>$kpis,
			'incidents'=>$incidents,
			'the_incident'=>$the_incident,
			'email_sent_count'=>$email_sent_count,
			'email_confirmed_count'=>$email_confirmed_count,
			'noti_kpi'=>$noti_kpi,
			'market_owners'=>$market_owners,
			'dist_owners'=>$dist_owners,
			'both_owners'=>$both_owners,
			'all_owners'=>$all_owners,
			'both_notdeldist_owners'=>$both_notdeldist_owners,
			'both_notdelmkt_owners'=>$both_notdelmkt_owners,
			'all_kpi'=>$all_kpi

		);



		$this->load->view('cc/cc-view',$data);
	}
	

	//------------------------------------owners
	public function owners(){
		$incident_id = $this->uri->segment(4);
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);


		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
	
	
		if($active_module == '5'){
			$incident_type = '5';
			$inci_type = 'recall';
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

		}
		else{ //continuity = 8
			$incident_type = '8';
			$inci_type = 'continuity';
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
		}
	
		//check if incident empty
		if(count($the_incident) == 0){
			redirect('cc/kpi');
		}
		

		//get kpi owners
		$whr_dist_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'dist',
			'deleted'=>'0'
		);
		$whr_market_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'mkt',
			'deleted'=>'0'
		);
		$whr_all_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'deleted'=>'0'
		);
		$whr_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all',
			'deleted'=>'0'
		);
		$dist_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_dist_owner);
		$market_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_market_owner);
		$all_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_all_owner);
		$both_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_both_owner);
		
		
	
		$data=array(
			'page_title'=>'KPI Owners',
			'middle_content'=>'bulk_kpi_owners',
			'error'=>'',
			'success'=>'',
			'incidents'=>$incidents,
			'the_incident'=>$the_incident,
			'market_owners'=>$market_owners,
			'dist_owners'=>$dist_owners,
			'both_owners'=>$both_owners,
			'all_owners'=>$all_owners,

		);

		$this->load->view('cc/cc-view',$data);
	
	}
	
	
	
	public function mdd(){
		echo md5('12');
	}
	
	public function sendowner(){
	
		$id = $_POST['id'];
		$nowtime = $this->common_model->userdatetime();

		$owner = $this->master_model->getRecords('bulk_kpi_owners', array('id'=>$id));
		
		//get validation message from admin gen update = 96, delete = 63
		$action_message_id = '72';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if(count($owner) > 0){
			// Admin email
			$whr=array('id'=>'1');
			$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');
			
			// set email data
			$info_arr_cc = array(
				'from'=>$adminemail[0]['contact_email'],
				'to'=>$owner[0]['email'],
				'subject'=>'Recall Incident has been initiated',
				'view'=>'kpi-owner-email'
			);
			
			$other_info_cc = array(
				'name'=>$owner[0]['name'],
				'bulkid'=>$owner[0]['id'],
				'emailmd5'=>md5($owner[0]['email']),
				'bulkidmd5'=>md5($owner[0]['id'])
			);
			
			$this->email_sending->sendmail($info_arr_cc,$other_info_cc);
			// Mail sending completed  
			
			$this->master_model->updateRecord('bulk_kpi_owners', array('date_sent'=>$nowtime), array('id'=>$id));
			$this->session->set_flashdata('success', $action_message['success']);
			$data['result'] = 'success';
		}
		$data['id'] = $id;
		$data['last_sent'] = date_format(date_create($nowtime), 'M d, Y, g:i A');
		echo json_encode($data);
	}
	
	public function addup_owner(){
	
		$email = $_POST['owner_email'];
		$name = $_POST['owner_name'];
		$company = $_POST['owner_company'];
		$incident_id = $_POST['incident_id'];
		$id = $_POST['owner_id'];
		$owner_type = $_POST['owner_type'];
		$incident_type = substr($this->session->userdata('org_module'), 0, 1);
		
		$nowtime = $this->common_model->userdatetime();
		//get validation message from admin gen update = 96, delete = 63
		$action_message_id = '79';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '80';
		$action_message2 = $this->common_model->get_message($action_message_id2);
		
		if($id !=''){
			$data = array(
				'name'=>$name,
				'kpi_type'=>$owner_type,
				'company'=>$company,
				'email'=>$email
			);
			$this->session->set_flashdata('success', $action_message2['success']);
			$this->master_model->updateRecord('bulk_kpi_owners', $data, array('id'=>$id));
			$data['message'] = $action_message2['success'];	
			}
		
		else{
			$data = array(
				'kpi_type'=>$owner_type,
				'name'=>$name,
				'company'=>$company,
				'email'=>$email,
				'incident_type'=>$incident_type,
				'incident_id'=>$incident_id,
				'date_added'=>$nowtime
			);
			
			$this->session->set_flashdata('success', $action_message['success']);
			$this->master_model->insertRecord('bulk_kpi_owners', $data);
			$data['message'] = $action_message['success'];	
			
		}
		echo json_encode($data);
	
	}
	
	
	
	//kpi inputs/threshold
	public function savefields(){
		$val = json_decode($_POST['val']);
		$field = json_decode($_POST['field']);
	
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '69';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '81';
		$action_message2 = $this->common_model->get_message($action_message_id2);
		
		$i = 0;
		foreach($field as $f){
			
			if($f != 'id' && $val[$i] == ''){
				$theval = '0';
			}
			else{
				$theval = str_replace(",", "",$val[$i]);
			}
			
			$data[$f] = $theval;
			$i++;
		}
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$nowtime = $this->common_model->userdatetime();

		//check what type of kpi
		if (array_key_exists('kpi_cell', $data)) {

			
			$u_corrections = $data['u_disposed'] + $data['u_transformed'];
			
			//check total > correction+return
			$corr_plus_return = $data['total_unit'] - ($u_corrections + $data['unit_return']);
			
			if($corr_plus_return < 0){
				
				$data['type'] = 'greater';
				$data['message'] = 'Units Returned + Corrective Actions cannot be more than Total Units.';
				echo json_encode($data);
				return false;
			}
			
			
			$u_corrections = $data['u_disposed'] + $data['u_transformed'];
			$data['u_corrections'] = $u_corrections;
			
			
			if($data['total_unit'] == 0){
				
				$the_kpi = 0;
				
			}
			else{
				
				$the_kpi = (($data['unit_return'] + $u_corrections) / $data['total_unit']) * 100;
			}
			
			$data['the_kpi'] = $the_kpi;

			$data['date_updated'] = $nowtime;
			$kpi_cell = $data['kpi_cell'];
			$rowdata[$kpi_cell] = serialize($data);
	

		}
		else{// if a pie data
			$i = 0;
			foreach($field as $f){
				$rowdata[$f] = str_replace(",", "",$val[$i]);
				$i++;
			}
		}


		
		
//		print_r($data);
//		return false;
		
		if($data['id'] != ''){
			$rowdata['date_updated'] = $nowtime;
			
			$this->master_model->updateRecord( 'bulk_kpi', $rowdata, array('id'=>$data['id']));
			
			$this->session->set_flashdata('success', $action_message['success']);
			$data['type'] = 'update'; 

		}else{
		
			$rowdata['cc_id'] = $cc_id;
			$rowdata['org_id'] = $org_id;
			$rowdata['incident_id'] = $data['incident_id'];
			$rowdata['incident_type'] = $active_mod;
			$rowdata['date_added'] = $nowtime;


			$kpi_id = $this->master_model->insertRecord('bulk_kpi', $rowdata, true);

			$this->session->set_flashdata('success', $action_message2['success']);
		
			$data['type'] = 'add'; 
			$data['id'] = $kpi_id; 

		}

		echo json_encode($data);
	
	}
	
	public function updatestatus(){
	
		$status = $_POST['title'];
		$kpitype = $_POST['kpitype'];
		$kpiid = $_POST['kpiid'];
		
		if($status == 'enabled'){
			$status = '1';
			$data['title'] = 'disabled';
			$data['class'] = 'btn-danger';
			$data['text'] = 'fa-times';
		}
		else{
			$status = '0';
			$data['title'] = 'enabled';
			$data['class'] = 'btn-success';
			$data['text'] = 'fa-check';
		}
		
		$this->master_model->updateRecord('bulk_kpi', array($kpitype=>$status), array('id'=>$kpiid));
		
		echo json_encode($data);
	}


	//------------------------------------addupdatepie
	public function addupdatepie(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$contact_cntr = str_replace(",", "",$_POST['contact_cntr']);
		$kpi_email = str_replace(",", "",$_POST['kpi_email']);
		$kpi_website = str_replace(",", "",$_POST['kpi_website']);
		$kpi_trading = str_replace(",", "",$_POST['kpi_trading']);
		
		$assoc_incident = $_POST['assoc_incident'];
		$id = $_POST['kpi_id'];
		
//		$data['id'] = $kpi_trading;
//		echo json_encode($data);
//		
//		return false;
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		if($active_mod == '5') { //recall
			$inci_type = '5';
		}
		else{ // continuity = 8
			$inci_type = '8';
		}

		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '69';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '81';
		$action_message2 = $this->common_model->get_message($action_message_id2);

		
		$comp_correction = $compcon_disposed + $compcon_transformed;
		$dist_correction = $dist_disposed + $dist_transformed;
		$market_correction = $market_disposed + $market_transformed;
		
		$comp_kpi = ($comp_correction / $company_control) * 100;
		$dist_kpi = ($dist_correction / $distribution) * 100;
		$market_kpi = ($market_correction / $consumer_market) * 100;
		
		
		$nowtime = $this->common_model->userdatetime();
		
		if($id != ''){
			$arr = array(
				'contact_cntr'=>$contact_cntr,
				'kpi_email'=>$kpi_email,
				'kpi_website'=>$kpi_website,
				'kpi_trading'=>$kpi_trading,
				'date_updated'=>$nowtime
			);
			
			$this->master_model->updateRecord( 'bulk_kpi', $arr, array('id'=>$id));
			
			$this->session->set_flashdata('success', $action_message['success']); //' Inbound customer contacts successfully updated.');
			$data['type'] = 'update'; 
			$data['id'] = $id; 
				
			
		}else{
		
			$arr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'incident_id'=>$assoc_incident,
				'incident_type'=>$inci_type,

				'contact_cntr'=>$contact_cntr,
				'kpi_email'=>$kpi_email,
				'kpi_website'=>$kpi_website,
				'kpi_trading'=>$kpi_trading,

				'date_added'=>$nowtime
			);
			
			$kpi_id = $this->master_model->insertRecord('bulk_kpi', $arr, true);

			$this->session->set_flashdata('success', $action_message2['success']); //' Inbound customer contacts successfully added.');
		
			$data['type'] = 'add'; 
			$data['id'] = $kpi_id; 

		}

		echo json_encode($data);
		
	}

	//------------------------------------addupdatebulk
	public function addupdate(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$company_control = str_replace(",", "",$_POST['company_control']);
		$distribution = str_replace(",", "",$_POST['distribution']);
		$consumer_market = str_replace(",", "",$_POST['consumer_market']);
		$assoc_incident = $_POST['assoc_incident'];
		$id = $_POST['kpi_id'];
		
		
		$compcon_disposed = str_replace(",", "",$_POST['compcon_disposed']);
		$compcon_transformed = str_replace(",", "",$_POST['compcon_transformed']);
		$dist_disposed = str_replace(",", "",$_POST['dist_disposed']);
		$dist_transformed = str_replace(",", "",$_POST['dist_transformed']);
		$market_disposed = str_replace(",", "",$_POST['market_disposed']);
		$market_transformed = str_replace(",", "",$_POST['market_transformed']);

		
		$comp_correction = $compcon_disposed + $compcon_transformed;
		$dist_correction = $dist_disposed + $dist_transformed;
		$market_correction = $market_disposed + $market_transformed;
		
		$comp_kpi = ($comp_correction / $company_control) * 100;
		$dist_kpi = ($dist_correction / $distribution) * 100;
		$market_kpi = ($market_correction / $consumer_market) * 100;
		
		
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		if($active_mod == '5') { //recall
			$inci_type = '5';
		}
		else{ // continuity = 8
			$inci_type = '8';
		}
		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '69';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '81';
		$action_message2 = $this->common_model->get_message($action_message_id2);
		
		$nowtime = $this->common_model->userdatetime();
		if($id != ''){
			$arr = array(
				'incident_id'=>$assoc_incident,
				'incident_type'=>$inci_type,
				'company_control'=>$company_control,
				'distribution'=>$distribution,
				'consumer_market'=>$consumer_market,
				
				'compcon_disposed'=>$compcon_disposed,
				'compcon_transformed'=>$compcon_transformed,
				'dist_disposed'=>$dist_disposed,
				'dist_transformed'=>$dist_transformed,
				'market_disposed'=>$market_disposed,
				'market_transformed'=>$market_transformed,
		
				'comp_correction'=>$comp_correction,
				'dist_correction'=>$dist_correction,
				'market_correction'=>$market_correction,
				
				'comp_kpi'=>$comp_kpi,
				'dist_kpi'=>$dist_kpi,
				'market_kpi'=>$market_kpi,
						
				'date_updated'=>$nowtime
			);
			
			$this->master_model->updateRecord( 'bulk_kpi', $arr, array('id'=>$id));
			
			$this->session->set_flashdata('success', $action_message['success']); //' Recall KPIs successfully updated.');
			$data['type'] = 'update'; 
			$data['id'] = $id; 
		}
		else{

			$arr = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'incident_id'=>$assoc_incident,
				'incident_type'=>$inci_type,
				'company_control'=>$company_control,
				'distribution'=>$distribution,
				'consumer_market'=>$consumer_market,

				'compcon_disposed'=>$compcon_disposed,
				'compcon_transformed'=>$compcon_transformed,
				'dist_disposed'=>$dist_disposed,
				'dist_transformed'=>$dist_transformed,
				'market_disposed'=>$market_disposed,
				'market_transformed'=>$market_transformed,
		
				'comp_correction'=>$comp_correction,
				'dist_correction'=>$dist_correction,
				'market_correction'=>$market_correction,
				
				'comp_kpi'=>$comp_kpi,
				'dist_kpi'=>$dist_kpi,
				'market_kpi'=>$market_kpi,
						
				'date_added'=>$nowtime
			);
			
			$kpi_id = $this->master_model->insertRecord('bulk_kpi', $arr, true);

			$this->session->set_flashdata('success', $action_message2['success']); //' Recall KPIs successfully added.');
		
			$data['type'] = 'add'; 
			$data['id'] = $kpi_id; 
		}
		$data['name'] = $company_control; 
		echo json_encode($data);
	
	}
	
	public function delete_data(){
		$id = $_POST['id'];
		$db = $_POST['db'];

		$data['id']= $id;
		$data['db']= $db;
		
		if($this->master_model->updateRecord($db, array('deleted'=>'1'), array('id'=>$id))) {
			$data['result'] = 'success';
		}
		else{
			$data['result'] = 'notdeleted';
		}
		
		echo json_encode($data);
	
	}
	
	
	public function delete_kpidata(){
		$id = $_POST['id'];
		$type = $_POST['type'];
		$deltype = $_POST['deltype'];
		
		$nowtime = $this->common_model->userdatetime();

		$owner_info = $this->master_model->getRecords('bulk_kpi_owners', array('id'=>$id));
		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);

		//update db
		if($type == 'mkt'){
			$del_arr['deleted_mkt'] = '1';
			
			if($deltype == 'datacontact'){
				$del_arr['deleted'] = '1';
			}
		}
		else if($type == 'dist'){
			$del_arr['deleted_dist'] = '1';
			
			if($deltype == 'datacontact'){
				$del_arr['deleted'] = '1';
			}
		}
		$this->master_model->updateRecord('bulk_kpi_owners', $del_arr, array('id'=>$id));
		
		
		$whr_kpi = array(
			'incident_id'=>$owner_info[0]['incident_id'],
			'incident_type'=>$owner_info[0]['incident_type'],
			'id !='=>$id
		);
		
		
		$kpi = $this->master_model->getRecords('bulk_kpi_owners', $whr_kpi);
	
		$main_total_unit = '0';
		$main_unit_return = '0';
		$main_u_disposed = '0';
		$main_u_transformed = '0';
		$main_u_corrections = '0';
		$main_the_kpi = '0';

		
		if($type == 'dist'){
			$kpi_type = 'dist_data';
			$kpi_type_del = 'deleted_dist';
		}
		else{
			$kpi_type = 'market_data';
			$kpi_type_del = 'deleted_mkt';
		}
		
		//check count of kpi
		if(count($kpi) > 0) {
			foreach($kpi as $r=>$value){
				
				if(($value['kpi_type'] == 'all' && $value[$kpi_type_del] == 0) || ($value['kpi_type'] == $type && $value[$kpi_type_del] == 0)){
					$dbcell = unserialize($value[$kpi_type]);
					if($dbcell != ''){ //get each kpi db cell data
						$main_total_unit += $dbcell['total_unit'];
						$main_unit_return += $dbcell['unit_return'];
						$main_u_disposed += $dbcell['u_disposed'];
						$main_u_transformed += $dbcell['u_transformed'];
						$main_u_corrections += $dbcell['u_corrections'];
						$main_the_kpi += $dbcell['the_kpi'];

					}
				
				
				}
				
				
			}
			
			
		}
		
		
		//check if single user
		if($main_total_unit == 0){
			$main_data['total_unit'] = '0';
			$main_data['unit_return'] = '0';
			$main_data['u_disposed'] = '0';
			$main_data['u_transformed'] = '0';
			$main_data['u_corrections'] = '0';
			$main_data['the_kpi'] = '0';
			$main_data['date_updated'] = $nowtime;
			
			$rowdata[$kpi_type] = serialize($main_data);
			
		}
		
		else{
			
			$main_data['total_unit']= $main_total_unit;
			$main_data['unit_return'] = $main_unit_return;
			$main_data['u_disposed'] = $main_u_disposed;
			$main_data['u_transformed'] = $main_u_transformed;
	
			$main_data['u_corrections'] = $main_data['u_disposed'] + $main_data['u_transformed'];
			$main_data['the_kpi'] = (($main_data['unit_return'] + $main_data['u_corrections']) / $main_data['total_unit']) * 100;
			$main_data['date_updated'] = $nowtime;
			
			
			$rowdata[$kpi_type] = serialize($main_data);
			
		}
		
		

		//update if main kpi existing
		if($owner_info[0]['kpi_id'] != '0'){
			$this->master_model->updateRecord('bulk_kpi', $rowdata, array('id'=>$owner_info[0]['kpi_id']));
		
		}
		
		$this->session->set_flashdata('success', $action_message['success']); // ' KPI Owner data successfully removed.');
		
		echo json_encode($main_data);
	}
	
	
	
	
	public function delete(){
		$id = $this->uri->segment(4);
		$page = $this->uri->segment(5);

		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);

		if($this->master_model->deleteRecord('bulk_kpi','id',$id)) {
			$this->session->set_flashdata('success', $action_message['success']); //' KPI successfully deleted.');
		
		
			if($page == 'dash'){
				redirect('cc/dashboard');
			}
			else{
				redirect('cc/kpi');
			}

		}

	}	
	
		
}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Reminder extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

		

	}

	

	public function index(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$selectedremider = $this->master_model->getRecords('cf_cc_reminders',array('cc_id'=>$cc_id,'org_id'=>$org_id));

		

		if(isset($_POST['btn_set_reminder'])){

			//get validation message from admin gen update = 69, delete = 63, add = 81
			$action_message_id = '86';
			$action_message = $this->common_model->get_message($action_message_id);

			$this->form_validation->set_rules('reminder','Remider option','required|callback_isexist');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run()){

				$reminder = $this->input->post('reminder');
				$review_date = $this->input->post('review_date');

				$input_array = array(
					'reminder_month_id'=>$reminder,
					'reminder_date'=>$review_date,
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization')
				);

				

				if(count($selectedremider) == 0){

					//$this->db->set('reminder_date','DATE_ADD(NOW(),INTERVAL '.$reminder.' Month)',FALSE);

					if($this->master_model->insertRecord('cf_cc_reminders',$input_array)){

						$this->session->set_flashdata('success', $action_message['success']); //' Review date successfully saved.');

						redirect(base_url().'cc/setup');

					}

				}

				if(count($selectedremider) >  0) {

					//$this->db->set('reminder_date','DATE_ADD(NOW(),INTERVAL '.$reminder.' Month)',FALSE);

					if($this->master_model->updateRecord('cf_cc_reminders',$input_array,

					array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'))))

					{

						$this->session->set_flashdata('success', $action_message['success']);

						redirect(base_url().'cc/setup');

					}

				}

				else

				{

					$this->session->set_flashdata('error', $action_message['error']);

					redirect(base_url().'cc/reminder/');

				}

			}

		}

		

		

		$remind_result = $this->master_model->getRecords('reminder_master');

		$data = array(
			'page_title'=>'Add Crisis Responce Team Member',
			'error'=>'',
			'middle_content'=>'reminder-view',
			'success'=>'',
			'remind_result'=>$remind_result,
			'selectedremider'=>$selectedremider
		);

		$this->load->view('cc/cc-view',$data);

	}

	


	public function team(){

		$data = array(
			'page_title'=>'Add Crisis Responce Team Member',
			'error'=>'',
			'middle_content'=>'reminder-request-view',
			'success'=>''
		);

		$this->load->view('cc/cc-view',$data);

	}

	public function crt(){

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$whr_arr = array('login.user_status'=>'1','crt.cc_id'=>$cc_id,'crt.org_id'=>$org_id,'crt.login_id !='=>$cc_id);


		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$remind_result = $this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr);



		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '87';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if(count($remind_result) > 0 ){


			foreach($remind_result as $rem){

				$whr = array('id'=>'1');

				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');

				

				$info_arr=array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->master_model->decryptIt($rem['crt_email']),
					'subject'=>'CrisisFlo update request',
					'view'=>'contact-information-mail-to-crt'
				);

				

				 $other_info = array(
				 	'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
					'onetime_key'=>$rem['onetime_key'],
					'cc_name'=>$cc_id
				);

				//print_r($other_info); 

				if($this->email_sending->sendmail($info_arr,$other_info)){

					$update_array = array('contact_verified'=>'2');

					$this->master_model->updateRecord('cf_login_master',$update_array,array('onetime_key'=>'"'.$rem['onetime_key'].'"'));

				}

			}

			$this->session->set_flashdata('success', $action_message['success']); //'Request successfully sent.');

			redirect(base_url().'cc/reminder/team');		

		}
		else{

			$this->session->set_flashdata('error', $action_message['error']); //'There are no verified team member on file.');

			redirect(base_url().'cc/reminder/team');		
		}

	}

	

	public function stakeholder(){

			$whr_arr=array('login.user_status'=>'1','stk.cc_id'=>$this->session->userdata('logged_cc_login_id'),'stk.org_id'=>$this->session->userdata('cc_selected_orgnaization'));

		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '88';
		$action_message = $this->common_model->get_message($action_message_id);

		$this->db->join('cf_login_master as login','login.login_id=stk.login_id');

		$remind_result=$this->master_model->getRecords('cf_stakeholder as stk',$whr_arr);


		if(count($remind_result) > 0 ) {

			foreach($remind_result as $rem){  

				$whr=array('id'=>'1');

				$adminemail=$this->master_model->getRecords('email_id_master',$whr,'*');

				

				$info_arr = array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->master_model->decryptIt($rem['stk_email_address']),
					'subject'=>'CrisisFlo update request',
					'view'=>'contact-information-mail-to-stk'
				);

				

				 $other_info = array( 
				 	'name'=>$this->master_model->decryptIt($rem['stk_first_name']).' '.$this->master_model->decryptIt($rem['stk_last_name']),
					'onetime_key'=>$rem['onetime_key'],
					'cc_name'=>$this->session->userdata('logged_cc_login_id')
				);

				//print_r($info_arr); 

				if($this->email_sending->sendmail($info_arr,$other_info)){

					$update_array = array('contact_verified'=>'2');

					$this->master_model->updateRecord('cf_login_master',$update_array,array('onetime_key'=>'"'.$rem['onetime_key'].'"'));

				}

			}

			$this->session->set_flashdata('success', $action_message['success']);

			redirect(base_url().'cc/reminder/team');	

		}
		else{

			$this->session->set_flashdata('error', $action_message['error']);

			redirect(base_url().'cc/reminder/team');	
		}

	}

	

	public function set_cron(){

		$this->db->select('cf_cc_reminders.*,crt.crt_first_name,crt.crt_last_name,crt.crt_email,organization_master.organization_name');

		$this->db->join('organization_master','organization_master.organization_id=cf_cc_reminders.org_id');

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=cf_cc_reminders.cc_id');

		$info = $this->master_model->getRecords('cf_cc_reminders',array('reminder_date'=>date('Y-m-d')));		

		

		if(count($info) > 0){

			foreach($info as $inf){

				$whr = array(
					'id'=>'1'
				);

				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');

				

				$info_arr = array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>'priyankad@webwingtechnologies.com',//$inf['crt_email']
					'subject'=>'Reminder to Crisis-coordinator',
					'view'=>'reminder-mail-to-cc'
				);

				

				 $other_info=array(
				 	'name'=>$inf['crt_first_name'].' '.$inf['crt_last_name'],
					'org'=>$inf['organization_name'],
				);

				$this->email_sending->sendmail($info_arr,$other_info);

			}

		}

	}

}

?>
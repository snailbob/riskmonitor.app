<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Auditlog extends CI_Controller {

	public function __construct(){

		parent::__construct();
//		$this->load->library('cc_check_session');
//		$this->cc_check_session->checksessionvalue();

	}

	public function empty_loc(){
		$this->session->set_userdata('location', '');
	}
	//------------------------------------post incident

	public function index(){
		$my_class = $this;
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);
	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type !='=>'4' //not cc_login
		);
		
		$logs = $this->master_model->getRecords('audit_log', $whr, '*', array('date_added'=>'DESC'));
	
		$the_logs = array();
		if(count($logs) > 0){
			foreach($logs as $r=>$value){
				if($value['type'] != '5'){
					$the_logs[] = $value;
				}//filter cc_logout
			}
		}
		
//		print_r($the_logs);
		
//		return false;
		
		$data = array(
			'page_title'=>'Activity Log',
			'middle_content'=>'audit_log_view',
			'error'=>'',
			'success'=>'',
			'my_class'=>$my_class,
			'logs'=>$the_logs
		);

	

		$this->load->view('cc/cc-view',$data);
	}



	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		return $crtname=$this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);
		
	}
	

	//recall cost monitor
	public function currencycode($id) {
		
		$currency = $this->master_model->getRecords('countries_currency',array('idCountry'=>$id));
		if(count($currency) > 0){
			return $currency[0]['currencyCode'];
		}else{
			return 'Currency Unknown';
		}
	}
	
	
	public function accumulate($recall){
	
	
		if(count($recall) == 0){
			$myaccumulate = 'No Cost Items';
			return $myaccumulate;
		}
		else{

			$recall_items = $this->master_model->getRecords('cf_cost_category_item',array('recall_id'=>$recall[0]['id']),'*',array('currency'=>'ASC'));
	
			$myaccumulate = '';
			if(count($recall_items) > 0){
				
				$mycurr = $recall_items[0]['currency'];
				$totalcurr = 0;
				$m = 0;
				
				foreach($recall_items as $it=>$ems){
					if($mycurr != $ems['currency']){
							
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2).' + ';
						
						$mycurr = $ems['currency'];
						$totalcurr = $ems['item_cost'];
						
	
					}else{
						
						$totalcurr += $ems['item_cost'];
					}
				
					if((count($recall_items) - 1) == $m){
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2);
					}
					
					$m++;
				}
				
						
			}else{
				$myaccumulate .= 'No Cost Items';
			}
			return $myaccumulate;
			
		}
		
		
	}
	
	public function admin_help_content(){
		$uri_2 = $_POST['uri_2'];
		$uri_3 = $_POST['uri_3'];
		$uri_4 = $_POST['uri_4'];
		$uri_5 = $_POST['uri_5'];
		$uri_6 = $_POST['uri_6'];
		$kpi_menu_active = $_POST['kpi_menu_active'];
		$costmon_menu_active = $_POST['costmon_menu_active'];
		$blockers_menu_active = $_POST['blockers_menu_active'];
		
		$content = array();

		
		//check pages
		if($uri_2 == 'dashboard'){
			if($kpi_menu_active != '0'){
				$content = $this->master_model->getRecords('admin_help_function', array('name'=>'kpi_dashboard'));
				
			}
			if($costmon_menu_active != '0'){
				$content = $this->master_model->getRecords('admin_help_function', array('name'=>'cost_monitor'));
				
			}
			if($blockers_menu_active != '0'){
				$content = $this->master_model->getRecords('admin_help_function', array('name'=>'issues_board'));
				
			}
		}
	
		if($uri_2 == 'message'){
			$content = $this->master_model->getRecords('admin_help_function', array('name'=>'messages'));
		}
	

		//check the help content
		$help_content = '';				
		if(count($content) > 0){
			if($content[0]['help_content'] != '' && $content[0]['help_content'] != '<br>'){
				$help_content = $content[0]['help_content'];
			}
		
		}
		
		echo $help_content;

	}
	
	public function recall_pages(){
		$pages = array(
			'kpi_dashboard',
			'cost_monitor',
			'issues_board',
			'messages',
			'raise_incident',
			'pre_incident_menu',
			'user_group',
			'respones_team',
			'stakeholders',
			'recall_documents',
			'standby_tasks',
			'forums',
			'notifications',
			'manage_dashboard',
			'video_conference',
			'update_request',
			'post_incident_review',
			'reporting',
			'audit_log',
			'settings_menu',
			'change_password',
			'update_details',
			'set_timezone',
			'change_module'
		);
		
		return $pages;

	}
	
	public function admin_help(){
		$db = 'admin_help_function';
		
		$entry = array();
		
		$pages = array(
			'kpi_dashboard',
			'cost_monitor',
			'issues_board',
			'messages',
			'raise_incident',
			'pre_incident_menu',
			'user_group',
			'respones_team',
			'stakeholders',
			'recall_documents',
			'standby_tasks',
			'forums',
			'notifications',
			'manage_dashboard',
			'video_conference',
			'update_request',
			'post_incident_review',
			'reporting',
			'audit_log',
			'settings_menu',
			'change_password',
			'update_details',
			'set_timezone',
			'change_module'
		);

		foreach($pages as $r){
			$arr = array(
				'module'=>'recall',
				'name'=>$r
			);
			echo json_encode($arr);
			$this->master_model->insertRecord('admin_help_function', $arr);
		}


	}
	
	
		
}

?>
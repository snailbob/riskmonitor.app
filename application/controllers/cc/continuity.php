<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Continuity extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
	}
	public function charmus(){

	}
	
	

	
	public function manage()
	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
	 
		//questions
		$continuity_id = 17;
	  	$questions = $this->master_model->getRecords('cf_recall_guidance',array('recall_pack_id'=>$continuity_id,'step_no'=>'2','disabled'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
	  	$the_other_questions = $this->master_model->getRecords('cf_recall_guidance',array('recall_pack_id'=>$continuity_id,'step_no !='=>'2','disabled'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		
	 
        // load library
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');


		//new json api source for currency
		$url = "http://currency-api.appspot.com/api/EUR/USD.json?key=0d58e6fb444400a40e03e3f04bc80cb7647b0912";
		
		$result = file_get_contents($url);
		$exrate = json_decode($result);
		
		
		if (is_object($exrate)){//check if it return json
			if ($exrate->success){
				$eurtousdval = $exrate->rate;
			}
			else{
				$eurtousdval = 1.3628; //static value for euro to usd
			}
		}
		else{
			$eurtousdval = 1.3628; //static value for euro to usd
		}
		
		//select org module
		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
		$the_active_module = array('0'=>array('active_module'=>$theorg[0]['active_module']));



		if(isset($_POST['btn_notify_incident']))
		{
			$this->form_validation->set_rules('radiovalit', 'Respone Type', 'required');
			$this->form_validation->set_rules('inci_no', 'Incident number', 'required');
			$this->form_validation->set_rules('inci_date', 'Date of incident', 'required');
			$this->form_validation->set_rules('inci_name', 'Incident name', 'required');
			
			
	  		if(count($questions) > 0){
				$the_q = 1;
				foreach($questions as $q=>$question){
					//validation
					$this->form_validation->set_rules('step1q'.$the_q, $question['question'], 'required');
					
					$the_q++;	
				}
			}

			$this->form_validation->set_rules('inform', 'Notify via Email', 'required');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run())
			{

				$radiovalit=$this->input->post('radiovalit');
				$inci_no=$this->input->post('inci_no');
				$inci_base_no=$this->input->post('inci_base_no');
				$inci_date=$this->input->post('inci_date'); //.' 00:00:00';
				$inci_name=$this->input->post('inci_name');
				
				//set value variables
				if(count($questions) > 0){
					$the_q = 1;
					foreach($questions as $q=>$question){
						//validation
						${"step1q" . $the_q} = $this->input->post('step1q'.$the_q);
						$the_q++;	
					}
				}

				$aff_loc = $this->input->post('p_scnt',true);
				$aff_loc_lat = $this->input->post('lat',true);
				$aff_loc_lng = $this->input->post('lng',true);
				
				//add to one field the affected location
				$affected_loc = array();
				array_push($affected_loc, $aff_loc);
				array_push($affected_loc, $aff_loc_lat);
				array_push($affected_loc, $aff_loc_lng);

				$inform = $this->input->post('inform'); 
				//$informsms=$this->input->post('informsms'); 


				//timezone datetime
				$nowinitiatetime = time();
				$timezone = $this->session->userdata('timezone');
				$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
				
				$nowtime = gmdate("Y-m-d H:i:s", $theinitiatetime);


				//insert scenario log 
				$recall_arr= array(
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'incident_no'=>$inci_no,
					'cc_incident_id'=>$inci_base_no,
					'initiation_type'=>$radiovalit,
					'incident_status'=>'0',
					'initiation_date'=>$nowtime,
					'input_pack_id'=>$continuity_id,
					'incident_date'=>$inci_date,
					'incident_name'=>$inci_name,
					'affected_location'=>json_encode($affected_loc),
				);

				
//					'incident_date'=>$inci_date,
//					'incident_name'=>$inci_name,
//					'overview'=>$overview,
//					'product_affected'=>$products,
//					'injuries'=>$injuries,
//					'aware_of_incident'=>$aware,
//					'actions_taken_whom'=>$actions_taken,
//					'investigate_started_whom'=>$investigation,
//					'affected_location'=>json_encode($affected_loc),
				
				
				

				$incident_id = $this->master_model->insertRecord('cf_continuity',$recall_arr,true);
				//.insert recall step1 info
				
				$log_incident_id = $incident_id.'r';

				
				
				
				
				$count_var = 1;
				foreach($questions as $rc=>$rpack){
		
					$the_questions = array(
						'recall_id'=>$incident_id,
						'recall_pack_id'=>$continuity_id,
						'question'=>$rpack['question'],
						'answer'=>${'step1q'.$count_var},
						'task_guide'=>$rpack['guidance'],
						'category_id'=>$rpack['category'],
						'step_no'=>$rpack['step_no'],
						'status'=>'1'
					);
					$this->master_model->insertRecord('cf_continuity_steps',$the_questions);
					$count_var++;
				}//insert step1 questions
				
				if(count($the_other_questions) > 0){

					foreach($the_other_questions as $toq=>$oq){
						$the_questions = array(
							'recall_id'=>$incident_id,
							'recall_pack_id'=>$continuity_id,
							'question'=>$oq['question'],
							'task_guide'=>$oq['guidance'],
							'category_id'=>$oq['category'],
							'step_no'=>$oq['step_no'],
							'status'=>'0'
						);
						$this->master_model->insertRecord('cf_continuity_steps',$the_questions);
					}
				}//insert other questions
				
				



				// Admin email
				$whr=array('id'=>'1');
				$adminemail=$this->master_model->getRecords('email_id_master',$whr,'*');
				
				// Sending mail to CC only
				$info_arr_cc=array('from'=>$adminemail[0]['contact_email'],
										'to'=>$this->session->userdata('logged_cc_email_id'),
										'subject'=>'Recall Incident has been initiated',
										'view'=>'incident-occurence-mail-to-cc');
				$other_info_cc=array(
							  'name'=>$this->session->userdata('logged_display_name'));
				$this->email_sending->sendmail($info_arr_cc,$other_info_cc);
				// Mail sending  to CC completed  
				
				
				$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'notify-incident'));
				
				$from = 'CrisisFlo';				
				$message = array(
					'text' => $sms_message[0]['text']//'A notify incident has been submitted. Please log into your user panel.'
				);
				
				
				
				
         	//Recall module activated
            if (strpos($the_active_module[0]['active_module'], '7') !== false){ 
				//sms sending
				//if($informsms=='crtonly'){
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));

					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{
							
							$to = $this->master_model->decryptIt($rem['crt_mobile']);
							$response = $this->nexmo->send_message($from, $to, $message);

							foreach ($response->messages as $messageinfo) {
								$recipient = $messageinfo->{'to'};
								$status = $messageinfo->{'status'};
							}

								
								$det_contact = $this->master_model->decryptIt($rem['crt_first_name']);
								$det_contact .= ' ';
								$det_contact .= $this->master_model->decryptIt($rem['crt_last_name']);
								$det_contact .= ' - ';
								$det_contact .= $recipient;

							if ($status == '0'){

							$date_sent = date("Y-m-d H:i:s");

								foreach ($response->messages as $messageinfo) {
									$message_id = $messageinfo->{'message-id'};
									$messprice = $messageinfo->{'message-price'};
									$message_price = $eurtousdval * $messprice;
									$network = $messageinfo->{'network'};
								}

								$this->master_model->insertRecord('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'message_id'=>$message_id,'network'=>$network,'recipient'=>$det_contact,'price'=>$message_price,'status'=>$status,'date_sent'=>$date_sent, 'log_scenario_id'=>$log_incident_id));
								
								
								$sms_creditt=$this->master_model->getRecords('organization_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'organization_id'=>$this->session->userdata('cc_selected_orgnaization')),'sms_credit');
								
								$ded_creditt = $sms_creditt[0]['sms_credit'] - $message_price;
								
								$this->master_model->updateRecord('organization_master',array('sms_credit'=>$ded_creditt),array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
								
								
							}
							else{
								
								foreach ($response->messages as $messageinfo) {
									$err_txt = $messageinfo->{'error-text'};
								}

								$err_stat = $status;
								$err_stat .= ' - ';
								$err_stat .= $err_txt;
								
								$this->master_model->insertRecord('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'), 'recipient'=>$det_contact, 'price'=>'0', 'status'=>$err_stat, 'log_scenario_id'=>$log_incident_id));
							}
								
						}
					}//end of sms notification
					
				}//end of strpos 7 = sms noti
					
					
					
				if($inform=='crtonly')
				{
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));

					
					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{
							$info_arr=array('from'=>$adminemail[0]['contact_email'],
										'to'=>$this->master_model->decryptIt($rem['crt_email']),
										'subject'=>'Recall Incident has been initiated',
										'view'=>'incident-occurence-mail-to-crt');
							
							 $other_info=array(
										  'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
										  'cc_name'=>$this->session->userdata('logged_cc_login_id'),
										  'incident_name'=>$inci_name,
										  'group_id'=>$rem['group_id']
										  );
							if($this->email_sending->sendmail($info_arr,$other_info))
							{
								
							}
						}
					}
					$this->session->set_flashdata('success',' Notify incident successfully submitted.'); // <a href="#"  data-toggle="modal" data-target="#mySmsReportLog">View SMS Report</a>');
					
					redirect('cc/dashboard/index/'.$log_incident_id.'/r/');
						
				}
				else if($inform=='crtstk') {
					//--------------------------------------for crt
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));
	
					$c_pack=$this->master_model->getRecords('cf_file_upload', array('c_pack'=>'yes','cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')));

					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{
							/*$whr=array('id'=>'1');
							$adminemail=$this->master_model->getRecords('email_id_master',$whr,'*');*/
							
							$info_arr=array('from'=>$adminemail[0]['contact_email'],
										'to'=>$this->master_model->decryptIt($rem['crt_email']),
										'subject'=>'Recall Incident has been initiated',
										'view'=>'incident-occurence-mail-to-crt');
							
							 $other_info=array(
										  'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
								//		  
										  'cc_name'=>$this->session->userdata('logged_cc_login_id'),
										  'incident_name'=>$inci_name,
										  'group_id'=>$rem['group_id'],
								//		  
								//		  'scenario_name'=>$scenario
								
								);
							$this->email_sending->sendmail($info_arr,$other_info);
						}
					}
					//--------------------------------------for stakeholder
//					$this->db->join('cf_login_master','cf_login_master.login_id=cf_stakeholder.login_id');
//					$remind_result1=$this->master_model->getRecords('cf_stakeholder',array('cf_login_master.user_status'=>'1','cf_stakeholder.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_stakeholder.org_id'=>$this->session->userdata('cc_selected_orgnaization')));
				
					
					$org_id = $this->session->userdata('cc_selected_orgnaization');
					$whr = array('organization_id !='=>$org_id);
			
					$org_name = $this->master_model->getRecords('organization_master',$whr,'*');
					$stks = unserialize($org_name[0]['stakeholders']);
					foreach($stks as $value) {
						$thestk = $this->master_model->getRecords('cf_stakeholder', array('login_id'=>$value));
						
						if(count($thestk) > 0){

							if($this->common_model->stkstatus($value) == 1){
								
								
								$info_arr = array(
									'from'=>$adminemail[0]['contact_email'],
									'to'=>$this->master_model->decryptIt($rem['stk_email_address']),
									'subject'=>'Recall Incident has been initiated',
									'view'=>'incident-occurence-mail-to-stk'
								);
								
								$other_info = array( 
									'name'=>$this->master_model->decryptIt($rem['stk_first_name']).' '.$this->master_model->decryptIt($rem['stk_last_name']),
									'cc_name'=>$this->session->userdata('logged_cc_login_id'),
									'incident_name'=>$inci_name,
									'overview'=>$overview,
									'org_name'=>$this->session->userdata('cc_selected_orgnaization_name')
								);
								
								$this->email_sending->sendmail($info_arr,$other_info);						
						
								
							}//end of check if stk completed

						}//end check if stk exist
					}//end loop for stks
				


					$this->session->set_flashdata('success',' Notify incident successfully submitted. <a href="#"  data-toggle="modal" data-target="#mySmsReportLog">View SMS Report</a>');
					
					redirect('cc/dashboard/index/'.$log_incident_id.'/r/');	
					
				}
				
			}
		}

		$excase_id=$this->master_model->getRecords('cf_continuity',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')),'cc_incident_id');
		if(count($excase_id) > 0){

			$numItems = count($excase_id);
			$i = 0;
			foreach($excase_id as $key=>$value) {
			  if(++$i === $numItems) {
				  
				$cc_incident_id = $value['cc_incident_id'] + 1;

			  }
			}  

				
		}
		
		else{
			$cc_incident_id = 1;
		}		
		
		
		//get org sms balance
		$response=$this->master_model->getRecords('organization_master',array(
		'organization_id'=>$this->session->userdata('cc_selected_orgnaization'),
		'cc_id'=>$this->session->userdata('logged_cc_login_id')));
		
		
		$data=array(
			'page_title'=>'Initiate Response',
			'error'=>'',
			'middle_content'=>'recontinuity-view',
			'success'=>'',
			'response'=>$response,
			'cc_incident_id'=>$cc_incident_id,
			'questions'=>$questions
		);
		
		$this->load->view('cc/cc-view',$data);
	}
	
		
	

}
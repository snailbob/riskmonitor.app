<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Crisisteam extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

	}

	/*------------------------------------
		add crt page
	------------------------------------*/
	public function add(){
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
		$active_module = $this->session->userdata('org_module');


		//get all crts	
		$all_crts = $this->master_model->getRecords('cf_crisis_response_team',array('org_id'=>$org_id));
		//get org info
		$myorg = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
		
		
		//filter crts
		$module_memb = 0;
		$org_members = $this->common_model->get_org_crts($cc_id,$org_id);

		$module_memb = count($org_members);


		if(substr($active_module, 0, 1) == '5'){ //get active module 5 = recall
			$addmore = $myorg[0]['recall_max'] - $module_memb;
			$mod_max = $myorg[0]['recall_max'];
			$module_name = 'Recall Management Module';
		}
		else if(substr($active_module, 0, 1) == '1'){ //get active module 1 = standard
			$addmore = $myorg[0]['standard_max'] - $module_memb;
			$mod_max = $myorg[0]['standard_max'];
			$module_name = 'Crisis Management Module';
		}
		else if(substr($active_module, 0, 1) == '2'){ //get active module 2 = case
			$addmore = $myorg[0]['case_max'] - $module_memb;
			$mod_max = $myorg[0]['case_max'];
			$module_name = 'Case Management Module';
		}
		else if(substr($active_module, 0, 1) == '4'){ //get active module 4 = member
			$addmore = $myorg[0]['member_max'] - $module_memb;
			$mod_max = $myorg[0]['member_max'];
			$module_name = 'Member Safety Module';
		}//end filter crts
		else if(substr($active_module, 0, 1) == '8'){ //get active module 8 = member
			$addmore = $myorg[0]['continuity_max'] - $module_memb;
			$mod_max = $myorg[0]['continuity_max'];
			$module_name = 'Continuity Manager';
		}//end filter crts
		
		$addmore = ($mod_max == '1000') ? '1000' : $addmore - 1;


		
		$countriescode = $this->master_model->getRecords('country_t');
		
		$all_group=$this->master_model->getRecords('user_group',array('org_id'=>$this->session->userdata('cc_selected_orgnaization')));

		$data = array(
			'page_title'=>'Add Crisis Response Team Member',
			'error'=>'',
			'middle_content'=>'add-crt',
			'success'=>'',
			'countriescode'=>$countriescode,
			'all_group'=>$all_group,
			'all_crts'=>$all_crts,
			'myorg'=>$myorg,
			'active_module'=>$active_module,
			'module_memb'=>$module_memb,
			'addmore'=>$addmore,
			'mod_max'=>$mod_max,
			'cc_info'=>$cc_info,
			'module_name'=>$module_name
		);

		$this->load->view('cc/cc-view',$data);

	}
	
	
	/*-------------------------------
		create_teammates_submit
	-------------------------------*/	
	public function create_teammates_submit(){
		$emails = $_POST['emails'];
		$utype = $_POST['utype'];
		$message = $_POST['message'];

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = $this->session->userdata('org_module');
		$current_module = substr($active_module, 0, 1);
		$timezone = $this->session->userdata('timezone');
		
		$cct_firstname = '';
		$cct_lastname = '';

		//team count
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);
		$accountActivated = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
		$all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);
		$total_crt_count = ($accountActivated[0]['stripe_customer_id'] == '0') ? count($all_crt) : 0;
		
		$whr = array(
			'organization_id'=>$org_id
		);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');

		$the_data = array();
		$count = 0;

		// echo $total_crt_count;
		// return false;

		foreach($emails as $r=>$value){
			$crt_email = $value;

			$onetime_key = md5(microtime());

			//check existing crt on a module and org
			$whr_same_exist = array(
				'date_deleted'=>'0000-00-00 00:00:00',
				'default_org'=>$org_id,
				'email_id'=>$this->master_model->encryptIt($crt_email)
			);
			$the_same_crt = $this->master_model->getRecordCount('cf_login_master', $whr_same_exist);	
			
			$crt_duplicate = (!empty($the_same_crt)) ? true : false;

			if($total_crt_count < 4){
				if(!$crt_duplicate && $crt_email){

					if($accountActivated[0]['stripe_customer_id'] == '0'){
						$total_crt_count++;
					}
	
					//check existing email
					$users = $this->master_model->getRecords('cf_login_master');	
					$user_count = 0;
	
					foreach($users as $ss=>$usr){
						if($usr['email_id'] == $this->master_model->encryptIt($crt_email)){
							$user_count++;
						}
					}
	
					$nowtime = date("Y-m-d h:i:s");
	
					$admin_array = array(
						'email_id'=>$this->master_model->encryptIt($crt_email),
						'user_level'=>$utype[$count],
						'default_org'=>$org_id,
						'onetime_key'=>$onetime_key,
						'date_added'=>$nowtime
					);
	
					$login_id = $this->master_model->insertRecord('cf_login_master', $admin_array, true);
	
	
					//response_team inputs
					$input_array = array(
						'login_id'=>$login_id,
						// 'group_id'=>$group,
						'crt_first_name'=>$this->master_model->encryptIt($cct_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cct_lastname),
						'crt_organisation'=>$org_name[0]['organization_name'],
						// 'crt_position'=>$crt_position,
						'crt_email'=>$this->master_model->encryptIt($crt_email),
						'module'=>$active_module,
						'timezone'=>$timezone,
						'cc_id'=>$cc_id,
						'org_id'=>$org_id
					);
	
					$new_crt_id = $this->master_model->insertRecord('cf_crisis_response_team', $input_array, true);
	
	
	
	
					//insert im room
					$wherrr = array(
						'org_id'=>$org_id,
						'login_id !='=>$login_id
					);
					
					$crt_memb = $this->master_model->getRecords('cf_crisis_response_team',$wherrr,'*');
					
					$chat_id = 'im_'.$login_id.'-'.$cc_id;
					$im_cc = array(
						'sender_id'=>$login_id,
						'receiver_id'=>$cc_id,
						'chat_id'=>'im_'.$login_id.'-'.$cc_id,
						
					);
					
					$this->master_model->insertRecord('private_instant_messages',$im_cc);
					
					$im_cc2 = array(
						'sender_id'=>$cc_id,
						'receiver_id'=>$login_id,
						'chat_id'=>'im_'.$login_id.'-'.$cc_id,
						
					);
	
					$this->master_model->insertRecord('private_instant_messages',$im_cc2);
					
					//create file room for crt and cc
					$room_name = 'chat_room/im/';
					$room_name .= $chat_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	
					
					if(count($crt_memb) > 0){
						foreach($crt_memb as $ccc => $memb){
							$chat_id = 'im_'.$login_id.'-'.$memb['login_id'];
							$im = array(
								'sender_id'=>$login_id,
								'receiver_id'=>$memb['login_id'],
								'chat_id'=>$chat_id,
								
							);
							
							$this->master_model->insertRecord('private_instant_messages',$im);
							
							$im2 = array(
								'sender_id'=>$memb['login_id'],
								'receiver_id'=>$login_id,
								'chat_id'=>$chat_id,
								
							);
							
							$this->master_model->insertRecord('private_instant_messages',$im2);
							
							
							//create file room for crt and crts
							$room_name = 'chat_room/im/';
							$room_name .= $chat_id;
							$room_name .= '.txt';
							
							$myfile = fopen($room_name, "w");	
							
						}
						
					} //.insert im room
	
	
					$whr = array('id'=>'1');
					$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');
	
	
					$info_arr = array(
						'from'=>$adminemail[0]['recovery_email'],
						'to'=>$crt_email,
						'subject'=>'Welcome to CrisisFlo',
						'view'=>'registration-mail-to-cc' //crt_registration_on_crisesflo'
					);
	
	
					if($user_count > 0){
						$info_arr['view'] = 'registration-mail-to-cc'; //crt_registration_on_crisesflo_2';
					}
	
					$other_info = array(
						'first_name'=>$cct_firstname,
						'last_name'=>$cct_lastname,
						'login_id'=>$onetime_key,
						'onetime_key'=>$onetime_key,
						'email'=>$crt_email
					);
	
					$this->email_sending->sendmail($info_arr,$other_info);
					$the_data[] = 'added';
	
					$action_message_id = '55';
					$action_message = $this->common_model->get_message($action_message_id);
					
					$this->session->set_flashdata('success', $action_message['success']);
	
					// $this->session->set_flashdata('success', 'Team member successfully added.');
	
				}
	
				if($crt_duplicate){
					$this->session->set_flashdata('error', 'User already registered.');
				}
			}
			else{
				$this->session->set_flashdata('error', 'Please upgrade your account in order to add more than 5 team members.');
			}


			$count++;

		}
		
		echo json_encode(array('emails'=>$emails, 'the_data'=>$the_data));

	}


	/*-------------------------------
		crt_submit
	-------------------------------*/	
	public function crt_submit(){
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');
		
	 	$eurtousdval = $this->common_model->get_nexmo_balance('exchange');
		//cct_firstname=dava&cct_lastname=adfsad&crt_position=asdf&crt_location=Davao+City%2C+Davao+Region%2C+Philippines&country_short=PH&lat=7.190708&lng=125.45534099999998&crt_email=asdf%40asdf.com&countrycode=63&countrycodedd=174&crt_no=465465&access%5B%5D=kpi&group=2&crt_func=asdf&alter_member=asdf&btn_add_crt_member=
		
		$countrycodedd = $_POST['countrycodedd'];
		$cct_firstname = $_POST['cct_firstname']; //$this->input->post('cct_firstname');
		$cct_lastname = $_POST['cct_lastname'];// $this->input->post('cct_lastname');
		$crt_position = $_POST['crt_position'];// $this->input->post('crt_position');
		$crt_email = $_POST['crt_email']; // $this->input->post('crt_email');
		$location = $_POST['crt_location'];//$this->input->post('crt_location');
		$location_lat = $_POST['lat']; // $this->input->post('lat');
		$location_lng = $_POST['lng'];// $this->input->post('lng');
		$countrycode = $_POST['countrycode'];// $this->input->post('countrycode');
		$crt_digits = ltrim($_POST['crt_no'], '0');// ltrim($this->input->post('crt_no'), '0');
		$crt_no = $countrycode.$crt_digits;
		$group = $_POST['group'];// $this->input->post('group');
		$crt_func = $_POST['crt_func'];// $this->input->post('crt_func');
		$alt_member = $_POST['alter_member'];// $this->input->post('alter_member');
	
//		$city = $_POST['cct_firstname'];// $this->input->post('city');
//		$city_lat = $_POST['cct_firstname'];// $this->input->post('city_lat');
//		$city_lng = $_POST['cct_firstname'];// $this->input->post('city_lng');

		$access = array();
		if(isset($_POST['access'])){
			$access = $_POST['access'];
		}
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = $this->session->userdata('org_module');
		$current_module = substr($active_module, 0, 1);
		$timezone = $this->session->userdata('timezone');
		

		
		
		$whr = array(
			'organization_id'=>$org_id
		);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');

		//get validation message from admin
		$action_message_id = '54';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$addmore = $_POST['addmore'];
		if($addmore <= 0){
			$this->session->set_flashdata('error', $action_message['error']);
			redirect('cc/crisisteam/add');
		}
		
		
//					$state = $this->input->post('state');
//					$availability = $this->input->post('availability');
//					$experience = $this->input->post('experience');
		

		//get validation message from admin
		$action_message_id = '74';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$onetime_key = md5(microtime());


		//check existing crt on a module and org
		$whr_same_exist = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'crt_email'=>$this->master_model->encryptIt($crt_email)
		);
		$the_same_crt = $this->master_model->getRecords('cf_crisis_response_team', $whr_same_exist);	
		
		if(count($the_same_crt) > 0){
			foreach($the_same_crt as $r=>$val){
				$crt_active_org = substr($val['module'], 0, 1);
				if($crt_active_org == $current_module){
					$this->session->set_flashdata('error', $action_message['error']);
					$data['result'] = 'exist';
					echo json_encode($data);
					return false;
				}
			}
		}
		
		
		
		//check existing email
		$users = $this->master_model->getRecords('cf_login_master');	
		$user_count = 0;

		foreach($users as $ss=>$usr){
			if($usr['email_id'] == $this->master_model->encryptIt($crt_email)){
				$user_count++;
//				$this->session->set_flashdata('error', ' Email already exist in the database.');
//				redirect('cc/crisisteam/add');
//				return false;
			}
		}

		$nowtime = date("Y-m-d h:i:s");

		$admin_array = array(
			'email_id'=>$this->master_model->encryptIt($crt_email),
			'user_level'=>'1',
			'default_org'=>$org_id,
			'onetime_key'=>$onetime_key,
			'date_added'=>$nowtime
		);

		$login_id = $this->master_model->insertRecord('cf_login_master', $admin_array, true);


		//response_team inputs
		$input_array = array(
			'login_id'=>$login_id,
			'group_id'=>$group,
			'crt_first_name'=>$this->master_model->encryptIt($cct_firstname),
			'crt_last_name'=>$this->master_model->encryptIt($cct_lastname),
			'crt_organisation'=>$org_name[0]['organization_name'],
			'crt_position'=>$crt_position,
			'crt_email'=>$this->master_model->encryptIt($crt_email),
			'location'=>$location,
			'location_lat'=>$location_lat,
			'location_lng'=>$location_lng,
			'country_id'=>$countrycodedd, //$this->session->userdata('country_id'),
			'countrycode'=>$this->master_model->encryptIt($countrycode),
			'crt_digits'=>$this->master_model->encryptIt($crt_digits),
			'crt_mobile'=>$this->master_model->encryptIt($crt_no),
			'crisis_function'=>$crt_func,
			'alt_member'=>$alt_member,
			'access'=>serialize($access),
			
//			'crt_city'=>$city,
//			'crt_city_lng'=>$city_lng,
//			'crt_city_lat'=>$city_lat,
			
//			'crt_state'=>$state,
//			'crt_availability'=>$availability,
//			'crt_experience'=>$experience,
			
			'module'=>$active_module,
			'timezone'=>$timezone,
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);

		$new_crt_id = $this->master_model->insertRecord('cf_crisis_response_team', $input_array, true);




		//insert im room
		$wherrr = array(
			'org_id'=>$org_id,
			'login_id !='=>$login_id
		);
		
		$crt_memb = $this->master_model->getRecords('cf_crisis_response_team',$wherrr,'*');
		
		$chat_id = 'im_'.$login_id.'-'.$cc_id;
		$im_cc = array(
			'sender_id'=>$login_id,
			'receiver_id'=>$cc_id,
			'chat_id'=>'im_'.$login_id.'-'.$cc_id,
			
		);
		
		$this->master_model->insertRecord('private_instant_messages',$im_cc);
		
		$im_cc2 = array(
			'sender_id'=>$cc_id,
			'receiver_id'=>$login_id,
			'chat_id'=>'im_'.$login_id.'-'.$cc_id,
			
		);

		$this->master_model->insertRecord('private_instant_messages',$im_cc2);
		
		//create file room for crt and cc
		$room_name = 'chat_room/im/';
		$room_name .= $chat_id;
		$room_name .= '.txt';
		$myfile = fopen($room_name, "w");	
		
		if(count($crt_memb) > 0){
			foreach($crt_memb as $ccc => $memb){
				$chat_id = 'im_'.$login_id.'-'.$memb['login_id'];
				$im = array(
					'sender_id'=>$login_id,
					'receiver_id'=>$memb['login_id'],
					'chat_id'=>$chat_id,
					
				);
				
				$this->master_model->insertRecord('private_instant_messages',$im);
				
				$im2 = array(
					'sender_id'=>$memb['login_id'],
					'receiver_id'=>$login_id,
					'chat_id'=>$chat_id,
					
				);
				
				$this->master_model->insertRecord('private_instant_messages',$im2);
				
				
				//create file room for crt and crts
				$room_name = 'chat_room/im/';
				$room_name .= $chat_id;
				$room_name .= '.txt';
				
				$myfile = fopen($room_name, "w");	
				
			}
			
		} //.insert im room

		//send sms
		$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'crt-registration'));

		$from = 'CrisisFlo';
		$message = array(
			'text' => $sms_message[0]['text']//'You are added as a Crisis Response Team Member by '.$this->session->userdata('logged_display_name').'. Please follow the link in the email we sent you to complete your registration.'
		);
		
		
		$to = $crt_no;
		$response = $this->nexmo->send_message($from, $to, $message);

		foreach ($response->messages as $messageinfo) {
			$recipient = $messageinfo->{'to'};
			$status = $messageinfo->{'status'};
		}

			$det_contact = $cct_firstname;
			$det_contact .= ' ';
			$det_contact .= $cct_lastname;
			$det_contact .= ' - ';
			$det_contact .= $recipient;

		if ($status == '0'){

		$date_sent = $this->common_model->userdatetime();

			foreach ($response->messages as $messageinfo) {
				$message_id = $messageinfo->{'message-id'};
				$messprice = $messageinfo->{'message-price'};
				$message_price = $eurtousdval * $messprice;
				$network = $messageinfo->{'network'};
			}
			
			
			$sms_data = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'message_id'=>$message_id,
				'network'=>$network,
				'recipient'=>$det_contact,
				'price'=>$message_price,
				'status'=>$status,
				'date_sent'=>$date_sent
			);
			
			$my_sms = $this->master_model->insertRecord('sms', $sms_data, true);
			
			
		}
		else{
			
			foreach ($response->messages as $messageinfo) {
				$err_txt = $messageinfo->{'error-text'};
			}

			$err_stat = $status;
			$err_stat .= ' - ';
			$err_stat .= $err_txt;
			
			$sms_data = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'recipient'=>$det_contact,
				'price'=>'0',
				'status'=>$err_stat
			);
			
			$my_sms = $this->master_model->insertRecord('sms', $sms_data, true);
		}

		//.send sms

		$whr = array('id'=>'1');
		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');


		$info_arr = array(
			'from'=>$adminemail[0]['recovery_email'],
			'to'=>$crt_email,
			'subject'=>'Welcome to CrisisFlo',
			'view'=>'registration-mail-to-cc'
		);



		if($user_count > 0){
			$info_arr['view'] = 'registration-mail-to-cc'; //crt_registration_on_crisesflo_2';
		}


		$other_info = array(
			'first_name'=>$cct_firstname,
			'last_name'=>$cct_lastname,
			'login_id'=>$onetime_key,
			'onetime_key'=>$onetime_key,
			'email'=>$crt_email
		);


//		$input_array['user_count'] = $user_count;
//		echo json_encode($info_arr);
//		return false;


		$this->email_sending->sendmail($info_arr,$other_info);

		//get validation message from admin
		$action_message_id = '55';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$this->session->set_flashdata('success', $action_message['success']);

		$input_array['result'] = 'ok';
		$input_array['aa_crt_number'] = $crt_no;
		echo json_encode($input_array);


	}//end of crt_submit
	
	
	
	
	/*-------------------------------
		checkduplicate
	-------------------------------*/	
	public function checkduplicate(){ //not used
		$email = $_POST['email'];
		$users = $this->master_model->getRecords('cf_login_master');	

		foreach($users as $ss=>$usr){
			if($usr['email_id'] == $this->master_model->encryptIt($email)){
				echo 'duplicate';
				return false;
			}
		}
					

	
	}

	//-----------------------------------------manage crt
	public function managecrt()

	{
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$consultants = $this->master_model->getRecords('reseller_master');
		$plan_max_user = $this->common_model->plan_max_user();
		$whr_not = array(
			'cc_id !='=>$cc_id,
			'org_id !='=>$org_id
		);


		// $this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
		// $this->db->order_by('cf_crisis_response_team.login_id','DESC');

		// $all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);


		$whr = array(
			'login.default_org'=>$org_id,
			// 'login.date_deleted'=>'0000-00-00 00:00:00'
		);

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');
		$all_crt = $this->master_model->getRecords('cf_login_master as login', $whr);

		$the_all = array();
		$total_crt_count = 0;

		if(!empty($all_crt)){
			foreach($all_crt as $r=>$value){
				$crt = $this->common_model->format_crt($value['login_id']);
				$the_all[] = $crt;
				if($crt['crt_email'] != ''){
					$total_crt_count++;
				}
			}
		}

		// echo json_encode($the_all); return false;


		$all_notmy_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr_not);
		//filter crts
		$active_module = $this->session->userdata('org_module');
		
				

		//redirect to add teamwork via email
		if($total_crt_count == 0){
			redirect('cc/crisisteam/createteam');
		}

		$user_id = $this->session->userdata('user_id');

		$my_info = $this->master_model->getRecords('cf_login_master',array('login_id'=>$user_id));

		$percentage_user = (count($the_all) / $plan_max_user) * 100;
		$percentage_user = round($percentage_user, 0);
		
		$remaining_user = $plan_max_user - count($the_all);
		$remaining_user = ($remaining_user > 0) ? $remaining_user : 0;
		
		$data = array(
			'page_title'=>"Manage Crisis Team Members",
			'middle_content'=>'manage-crt',
			'success'=>'',
			'error'=>'',
			'my_info'=>$my_info[0],
			'all_crt'=>$the_all,
			'consultants'=>$consultants,
			'all_notmy_crt'=> array(), //$all_notmy_crt,
			'active_module'=>$active_module,
			'org_id'=>$org_id,
			'plan_max_user'=>$plan_max_user,
			'percentage_user'=>$percentage_user,
			'remaining_user'=>$remaining_user,
			'total_crt_count'=>$total_crt_count
		);

		$this->load->view('cc/cc-view',$data);

	}

	//-----------------------------------------manage crt
	public function createteam(){

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$consultants = $this->master_model->getRecords('reseller_master');


		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);

		$whr_not = array(
			'cc_id !='=>$cc_id,
			'org_id !='=>$org_id
		);

		

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');

		$this->db->order_by('cf_crisis_response_team.login_id','DESC');

		$all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);
		$all_notmy_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr_not);
		//filter crts
		$active_module = $this->session->userdata('org_module');
		
		
		$total_crt_count = count($all_crt); // + $the_other_crts; 

		$data=array(
			'page_title'=>"Create Crisis Team Members",
			'middle_content'=>'manage-create-crt',
			'success'=>'',
			'error'=>'',
			'all_crt'=>$all_crt,
			'consultants'=>$consultants,
			'all_notmy_crt'=> array(), //$all_notmy_crt,
			'active_module'=>$active_module,
			'org_id'=>$org_id,
			'total_crt_count'=>$total_crt_count
		);

		$this->load->view('cc/cc-view',$data);

	}

	public function delete_user(){
		$id = $_POST['id'];

		//get validation message from admin
		$action_message_id = '52';
		$action_message = $this->common_model->get_message($action_message_id);

		$this->session->set_flashdata('success', $action_message['success']);

		$this->master_model->deleteRecord('cf_login_master', 'login_id', $id);
		$this->master_model->deleteRecord('cf_crisis_response_team', 'login_id', $id);
		echo json_encode($_POST);
	}

	//-----------------------------------------Delete crt

	public function delete(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$nowtime = date("Y-m-d h:i:s");
		
		//check count of admin
		$all_admin = $this->master_model->getRecordCount('cf_login_master', array('default_org'=>$org_id, 'user_level'=>'0', 'date_deleted'=>'0000-00-00 00:00:00'));
		
		
		$data['success'] = $data['error'] = "";
		$login_id = $this->uri->segment(4);
		$rdr = $this->uri->segment(5);



		//get validation message from admin
		$action_message_id = '52';
		$action_message = $this->common_model->get_message($action_message_id);


		//check if user is admin
		$is_admin = $this->master_model->getRecordCount('cf_login_master', array('login_id'=>$login_id, 'user_level'=>'0'));

		$del_data = array(
			'date_deleted'=>$nowtime
		);

		// var_dump($all_admin); return false;
		
		if(!empty($is_admin)){
			if($all_admin > 1){
				$this->session->set_flashdata('success', 'Administrator successfully deleted.');
				$this->master_model->updateRecord('cf_login_master', $del_data, array('login_id'=>$login_id));
			}
			else{
				$this->session->set_flashdata('error', $action_message['error']);
			}
		}
		else{
			$this->session->set_flashdata('success', $action_message['success']);
			$this->master_model->updateRecord('cf_login_master', $del_data, array('login_id'=>$login_id));
		}
		
		redirect(base_url()."cc/crisisteam/managecrt/");

	}
	
	//------------------------------------resend crt
	public function resend_request(){
		
		$id = $_POST['login_id'];


		/* fetch  crt members*/
		$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$id));
		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));

		//check if has any other email account exist
		$the_users = $this->master_model->getRecords('cf_crisis_response_team',array('crt_email'=>$crt_record[0]['crt_email']));
		
		//get validation message from admin
		$action_message_id = '51';
		$action_message = $this->common_model->get_message($action_message_id);
		
		//send email
		$whr=array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');
		
		if(count($the_users) > 1){

			$info_arr = array(
				'from'=>$adminemail[0]['recovery_email'],
				'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
				'subject'=>'Welcome to CrisisFlo',
				'view'=>'registration-mail-to-cc' //crt_registration_on_crisesflo_2'
			);
			
	
		}
		else{
			$info_arr = array(
				'from'=>$adminemail[0]['recovery_email'],
				'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
				'subject'=>'Welcome to CrisisFlo',
				'view'=>'registration-mail-to-cc', //crt_registration_on_crisesflo'
			);
		
		}


		$other_info = array(
			'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),
			'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),
			'login_id'=>$crt_onetimekey[0]['onetime_key'],
			'onetime_key'=>$crt_onetimekey[0]['onetime_key'],
			'email'=>$this->master_model->decryptIt($crt_record[0]['crt_email'])

		);

	
	
		if($this->email_sending->sendmail($info_arr,$other_info)){
			$this->session->set_flashdata('success', $action_message['success']);
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}

		
	}
	
	//------------------------------------import_crt
	public function import_crt(){
		$active_module = $_POST['active_module'];
		$addmore = $_POST['addmore'];


		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$consultants = $this->master_model->getRecords('reseller_master');
		$crts = $this->master_model->getRecords('cf_crisis_response_team',array('org_id'=>$org_id,'cc_id'=>$cc_id));
		$not_my_crts = $this->master_model->getRecords('cf_crisis_response_team',array('org_id !='=>$org_id,'cc_id !='=>$cc_id));
		
		if(count($crts) > 0){
			echo '<div class="alert alert-danger fade in" role="alert" style="display: none;">
					  Opps! Selected CRT exceeded.
					</div>
					<input type="hidden" value="'.$active_module.'" id="active_module"/>';
			$i = 0;
			$crt_emails = array();
			
			foreach($crts as $c=>$rt){
				if(strpos($rt['module'], $active_module) === false){

					echo '<div class="checkboxx">
					  <label for="checkimport'.$rt['login_id'].'">
						<input type="checkbox" name="checkimport[]" id="checkimport'.$rt['login_id'].'" value="'.$rt['login_id'].'" onclick="countToImport('.$addmore.');"> ';
						echo $this->master_model->decryptIt($rt['crt_first_name']).' '.$this->master_model->decryptIt($rt['crt_last_name']).
					  '</label>
					</div>';
					
					$crt_emails[] = $rt['crt_email'];
					$i++;

				}//end check of user in the module
			}//end foreach
			
			
			//display not crt of org
			if(count($not_my_crts) > 0){
				
				foreach($not_my_crts as $c=>$rt){
					$usertype = $this->master_model->getRecords('cf_login_master', array('login_id'=>$rt['login_id']));
					
					$crt_checkbox = '<div class="checkboxx">
						  <label for="checkimport'.$rt['login_id'].'">
							<input type="checkbox" name="checkimport[]" id="checkimport'.$rt['login_id'].'" value="'.$rt['login_id'].'" onclick="countToImport('.$addmore.');"> ';
							
							$crt_checkbox .= $this->master_model->decryptIt($rt['crt_first_name']).' '.$this->master_model->decryptIt($rt['crt_last_name']) .' (from '.$rt['crt_organisation'].')'.
						  '</label>
						</div>';
					
					
					
					if($usertype[0]['user_level'] == '1'){ //check if a crt
						$crt_orgs = unserialize($rt['org_list']);
						//print_r($crt_orgs);
						//echo $org_id;
						
						if(count($crt_orgs) > 0){

							$count_module_exist = 0;
							foreach($crt_orgs as $crt_row=>$crt_val){
								//echo $crt_val['active_module'];
								if($active_module == $crt_val['active_module'] && $org_id == $crt_val['org_id']){
									$count_module_exist++;
								}
							}//foreach crt_orgs
							
							if($count_module_exist == 0){
								//display not added crt
					
								$crt_emails[] = $rt['crt_email'];
								$i++;
								echo $crt_checkbox;
							}

						}//check crt_orgs
						else{
							
							$crt_emails[] = $rt['crt_email'];
							$i++;
							//display not added crt
							echo $crt_checkbox;
						}

						
	
					}//end check of user in the module
				}//end foreach
				
			}//check if not crt not empty
							
			if($i == 0){
				echo '<p class="text-center text-muted">There\'s nothing to import.</p>';
			}
		}else{
			
			$i = 0;
			
			//display not crt of org
			if(count($not_my_crts) > 0){
				
				foreach($not_my_crts as $c=>$rt){
					$usertype = $this->master_model->getRecords('cf_login_master', array('login_id'=>$rt['login_id']));
					
					$crt_checkbox = '<div class="checkboxx">
						  <label for="checkimport'.$rt['login_id'].'">
							<input type="checkbox" name="checkimport[]" id="checkimport'.$rt['login_id'].'" value="'.$rt['login_id'].'" onclick="countToImport('.$addmore.');"> ';
							
							$crt_checkbox .= $this->master_model->decryptIt($rt['crt_first_name']).' '.$this->master_model->decryptIt($rt['crt_last_name']) .' (from '.$rt['crt_organisation'].')'.
						  '</label>
						</div>';
					
					
					
					if($usertype[0]['user_level'] == '1'){ //check if a crt
						$crt_orgs = unserialize($rt['org_list']);
						//print_r($crt_orgs);
						//echo $org_id;
						
						if(count($crt_orgs) > 0){

							$count_module_exist = 0;
							foreach($crt_orgs as $crt_row=>$crt_val){
								//echo $crt_val['active_module'];
								if($active_module == $crt_val['active_module'] && $org_id == $crt_val['org_id']){
									$count_module_exist++;
								}
							}//foreach crt_orgs
							
							if($count_module_exist == 0){
								//display not added crt
					
								$crt_emails[] = $rt['crt_email'];
								$i++;
								echo $crt_checkbox;
							}

						}//check crt_orgs
						else{
							
							$crt_emails[] = $rt['crt_email'];
							$i++;
							//display not added crt
							echo $crt_checkbox;
						}

						
	
					}//end check of user in the module
				}//end foreach
				
			}//check if not crt not empty
			
			
			
			if($i == 0){
				echo '<p class="text-center text-muted">There\'s nothing to import.</p>';
			}
		}
	}
	
	
	
	//------------------------------------save_import_crt
	public function save_import_crt(){
		$crts = $_POST['checked']; //array
		$active_module = substr($this->session->userdata('org_module'), 0, 1);//$_POST['active_module'];
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		foreach($crts as $crt){
			$thecrt = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$crt));
			
			if($thecrt[0]['org_id'] == $org_id){// if a member of org
				$newmod = $thecrt[0]['module'].$active_module;
				$this->master_model->updateRecord('cf_crisis_response_team',array('module'=>$newmod),array('login_id'=>$crt));
			}
			else{
				$crt_orgs = unserialize($thecrt[0]['org_list']); //array of other org
				$new_crt_org = array(
					'org_id'=>$org_id,
					'active_module'=>$active_module,
				);
				$crt_orgs[] = $new_crt_org;

				$inss = array(
					'org_list'=>serialize($crt_orgs)
				);
				$this->master_model->updateRecord('cf_crisis_response_team', $inss, array('login_id'=>$crt));
			}
						
		}
		$this->session->set_flashdata('success',' Crisis Response Team imported successfully.');
		echo base_url().'cc/crisisteam/managecrt';
	}
	
	//------------------------------------Update crt

	public function update(){

		$login_id = $this->uri->segment(4);

		//get validation message from admin
		$action_message_id = '53';
		$action_message = $this->common_model->get_message($action_message_id);


		if(isset($_POST['btn_add_crt_member'])){

			$this->form_validation->set_rules('cct_firstname','First Name','required');

			$this->form_validation->set_rules('cct_lastname','Last Name','required');

			$this->form_validation->set_rules('crt_position','Postion','required');

			$this->form_validation->set_rules('crt_email','Email','required');

			$this->form_validation->set_rules('crt_no','Contact Number','required');

			// $this->form_validation->set_rules('group','User Group','required');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			
				if($this->form_validation->run()){

					$cct_firstname = $this->input->post('cct_firstname');

					$cct_lastname = $this->input->post('cct_lastname');
					$crt_email = $this->input->post('crt_email');
					
					$crt_position = $this->input->post('crt_position');

					$location = $this->input->post('crt_location');

					$location_lat = $this->input->post('lat');

					$location_lng = $this->input->post('lng');

					$countrycode = $this->input->post('countrycode');

					$crt_digits = ltrim($this->input->post('crt_no'), '0');

					$crt_no= $countrycode.$crt_digits;

					

					// $group = $this->input->post('group');

					$crisis_function = $this->input->post('crisis_function');

					$alt_member = $this->input->post('alter_member');

					$city = $this->input->post('city');
					$city_lng = $this->input->post('city_lng');
					$city_lat = $this->input->post('city_lat');

					$access = array();

					$input_array = array(
						// 'group_id'=>$group,
						'crt_first_name'=>$this->master_model->encryptIt($cct_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cct_lastname),
						'crt_email'=>$this->master_model->encryptIt($crt_email),
						'crt_position'=>$crt_position,
						'location'=>$location,
						'location_lat'=>$location_lat,
						'location_lng'=>$location_lng,
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($crt_no),
						'crisis_function'=>$crisis_function,
						'alt_member'=>$alt_member,
						'access'=>serialize($access),

						'crt_city'=>$city,
						'crt_city_lng'=>$city_lng,
						'crt_city_lat'=>$city_lat,
//						'crt_state'=>$state,
//						'crt_availability'=>$availability,
//						'crt_experience'=>$experience,
						
						'cc_id'=>$this->session->userdata('logged_cc_login_id')
					);

					if($this->master_model->updateRecord('cf_crisis_response_team',$input_array,array('login_id'=>$login_id))){
						$arr = array(
							'email_id'=>$this->master_model->encryptIt($crt_email),
							'date_deleted'=>'0000-00-00 00:00:00'
						);
						$this->master_model->updateRecord('cf_login_master',$arr,array('login_id'=>$login_id));

						$this->session->set_flashdata('success', $action_message['success']);
						redirect(base_url().'cc/crisisteam/managecrt/');

					}

					else

					{

						$this->session->set_flashdata('error', $action_message['error']);

						redirect(base_url().'cc/crisisteam/managecrt/');

					}

				}

		}
		$countriescode=$this->master_model->getRecords('country_t');

		// $crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		$crt_record = $this->common_model->format_crt($login_id);
		
		$all_group=$this->master_model->getRecords('user_group',array('org_id'=>$this->session->userdata('cc_selected_orgnaization')));

		$data=array('page_title'=>'Update Crisis Team Member','error'=>'','middle_content'=>'update-crt','success'=>'','crt_record'=>$crt_record,'countriescode'=>$countriescode,'all_group'=>$all_group);

		$this->load->view('cc/cc-view',$data);

	}
	

	public function details(){

		$login_id = $this->uri->segment(4);

		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data = array('page_title'=>'Crisis Team Member details','error'=>'','middle_content'=>'details-crt',

		'success'=>'','crt_record'=>$crt_record);

		

		$this->load->view('cc/cc-view',$data);

	}

	public function show_country_code() {
		$counrty_code = $_POST['country_short'];
		
		$mobile_code = $this->master_model->getRecords('country_t',array('iso2'=>$counrty_code));

		$data['calling_code'] = '';
		$data['country_id'] = '';
		if(count($mobile_code) > 0){
			//echo $mobile_code[0]['calling_code'];
			$data['calling_code'] = $mobile_code[0]['calling_code'];
			$data['country_id'] = $mobile_code[0]['country_id'];
		}

		echo json_encode($data);
	}


}

?>
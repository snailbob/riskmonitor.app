<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

//		$this->cc_check_session->checksessionvalue();

	}

	public function mysession(){
		$ss = $this->session->all_userdata();

		echo json_encode($ss);
	}



	public function index(){
		$setup_status_completed = $this->common_model->setup_status_completed();
		
		if(!$setup_status_completed){
			redirect('cc/setup');
		}

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');


		//paginate recall config
		$where_recall = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,'closed'=>'0'
		);

		$open_recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'));


		$data = array(
			'page_title'=>'Dashboard',
			'middle_content'=>'dashboard_new',
			'error'=>'',
			'success'=>'',
			'open_recall'=>$open_recall
		);


		$this->load->view('cc/cc-view',$data);
	}






	#--------------------------------------------->>dashboard view loading<<-------------------------------------

	public function indexx(){
//		echo current_url();
//		return false;

		$my_class=$this;

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword

		if($this->session->userdata('logged_cc_login_id')!="" && $this->session->userdata('cc_selected_orgnaization')!=""){


			$waiting_crtmem = $waiting_stkmem = $cc_document = $cc_response_plan = array();

			$cc_id = $this->session->userdata('logged_cc_login_id');

			$org_id = $this->session->userdata('cc_selected_orgnaization');

			$active_mod = substr($this->session->userdata('org_module'),0,1);

			//active module is recall
			if($active_mod == '5'){

					//paginate recall config
					$where_recall=array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0');

					$open_recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'));

					$this->load->library('pagination');

					$config['base_url'] = base_url().'cc/dashboard/index/r/';
					$config['total_rows'] = count($open_recall);
					$config['per_page'] = 1;
					$config['uri_segment'] = 5;

					$config['full_tag_open'] = ' <div class="btn-group">';
					$config['full_tag_close'] = '</div>';

					$config['display_pages'] = TRUE;
					$config['first_link'] = FALSE;
					$config['last_link'] = FALSE;

					$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
					$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
					$config['next_tag_close'] = '</div>';

					$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
					$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
					$config['prev_tag_close'] = '</div>';

					$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
					$config['num_tag_close'] = '</div>';

					$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
					$config['cur_tag_close'] = '</b></div>';



					$this->pagination->initialize($config);



					$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

					$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'),$page,$config["per_page"]);

					//get recall time
					if(count($recall) > 0){
						$recall_initiated = new DateTime($recall[0]['initiation_date']);
						//timezone datetime
						$nowtime = $this->common_model->userdatetime();

						$recall_closed = new DateTime($nowtime);
						$difference = $recall_initiated->diff($recall_closed);

						$sec_y = $difference->y === 0 ? '0' : $difference->y*12*30*24*60*60;
						$sec_m = $difference->m === 0 ? '0' : $difference->m*30*24*60*60;
						$sec_d = $difference->d === 0 ? '0' : $difference->d*24*60*60;
						$sec_h = $difference->h === 0 ? '0' : $difference->h*60*60;
						$sec_i = $difference->i === 0 ? '0' : $difference->i*60;

						$the_sec = $sec_y + $sec_m + $sec_d + $sec_h + $sec_i + $difference->s;

					}
					else{
						$the_sec = 0;
					}


					//.paginate recall config

					$uniquetime= time().mt_rand();
					$currency_code = $this->master_model->getRecords('countries_currency',array('currencyCode !='=>''));
					$recall_accumulate = $this->accumulate($recall);

					if(count($recall) > 0){
						$wher_cost_cat = array(
							'cc_id'=>$this->session->userdata('logged_cc_login_id'),
							'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
							'recall_id'=>$recall[0]['id']
						);

						$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));

					}else{
						$cost_category = array();
					}


					//redirect if open recall not empty but page is empty
					if(count($open_recall) > 0 && count($recall) == 0){
						redirect('cc/dashboard');
					}



			}else{ //active is not recall but continuity

					//paginate recall config
					$where_recall=array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0');

					$open_recall = $this->master_model->getRecords('cf_continuity',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'));

					$this->load->library('pagination');

					$config['base_url'] = base_url().'cc/dashboard/index/r/';
					$config['total_rows'] = count($open_recall);
					$config['per_page'] = 1;
					$config['uri_segment'] = 5;

					$config['full_tag_open'] = ' <div class="btn-group">';
					$config['full_tag_close'] = '</div>';

					$config['display_pages'] = TRUE;
					$config['first_link'] = FALSE;
					$config['last_link'] = FALSE;

					$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
					$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
					$config['next_tag_close'] = '</div>';

					$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
					$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
					$config['prev_tag_close'] = '</div>';

					$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
					$config['num_tag_close'] = '</div>';

					$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
					$config['cur_tag_close'] = '</b></div>';



					$this->pagination->initialize($config);



					$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

					$recall = $this->master_model->getRecords('cf_continuity',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'),$page,$config["per_page"]);

					//get recall time
					if(count($recall) > 0){
						$recall_initiated = new DateTime($recall[0]['initiation_date']);
						//timezone datetime
						$nowtime = $this->common_model->userdatetime();

						$recall_closed = new DateTime($nowtime);
						$difference = $recall_initiated->diff($recall_closed);

						$sec_y = $difference->y === 0 ? '0' : $difference->y*12*30*24*60*60;
						$sec_m = $difference->m === 0 ? '0' : $difference->m*30*24*60*60;
						$sec_d = $difference->d === 0 ? '0' : $difference->d*24*60*60;
						$sec_h = $difference->h === 0 ? '0' : $difference->h*60*60;
						$sec_i = $difference->i === 0 ? '0' : $difference->i*60;

						$the_sec = $sec_y + $sec_m + $sec_d + $sec_h + $sec_i + $difference->s;

					}
					else{
						$the_sec = 0;
					}


					//.paginate recall config

					$uniquetime= time().mt_rand();
					$currency_code = $this->master_model->getRecords('countries_currency',array('currencyCode !='=>''));
					$recall_accumulate = $this->accumulate($recall);

					if(count($recall) > 0){
						$wher_cost_cat = array(
							'cc_id'=>$this->session->userdata('logged_cc_login_id'),
							'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
							'continuity_id'=>$recall[0]['id']
						);

						$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));

					}else{
						$cost_category = array();
					}


					//redirect if open recall not empty but page is empty
					if(count($open_recall) > 0 && count($recall) == 0){
						redirect('cc/dashboard');
					}

			}



			$all_crt = $this->common_model->get_org_crts($cc_id,$org_id); //$this->master_model->getRecords('cf_crisis_response_team',$whr);



			$whr_case=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'cc_decision'=>'3','status'=>'0');
			$casereport = $this->master_model->getRecords('case_report_master',$whr_case);


			$main_status=$this->common_model->get_main_status($cc_id,$org_id);



			if($main_status=="Pre-Incident Phase")

			{

				/* to display remaining task */

				$num_group=$this->master_model->getRecordCount('user_group',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_crt=$this->master_model->getRecordCount('cf_crisis_response_team',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_stk=$this->master_model->getRecordCount('cf_stakeholder',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_plan=$this->master_model->getRecordCount('cf_scenario',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				/* to display remaining task : End  */

				$waiting_crtmem = $this->common_model->get_waiting_crt($cc_id,$org_id);

				$waiting_stkmem = $this->common_model->get_waiting_stk($cc_id,$org_id);

				$waiting_contact_crtmem = $this->common_model->get_waiting_contact_crt($cc_id,$org_id);

				$waiting_contact_stkmem = $this->common_model->get_waiting_contact_stk($cc_id,$org_id);

				$cc_document = $this->common_model->get_documents($cc_id,$org_id);

				$cc_response_plan=$this->common_model->check_response_plan_generated($cc_id,$org_id);

				$review_date=$this->master_model->getRecords('cf_cc_reminders',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_standby_message=$this->master_model->getRecordCount('cf_standby_messages',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_reminder=$this->master_model->getRecordCount('cf_cc_reminders',array('cc_id'=>$cc_id,'org_id'=>$org_id));



				/* fetch task to check un assigned tasks */
				$whr_tsk=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'));
				$the_scene = $this->master_model->getRecords('cf_task',$whr_tsk,'*');




				$data=array(
					'page_title'=>'Dashboard',
					'middle_content'=>'dashboard',
					'error'=>'','success'=>'',
					'waiting_crtmem'=>$waiting_crtmem,
					'waiting_stkmem'=>$waiting_stkmem,
					'cc_document'=>$cc_document,
					'cc_response_plan'=>$cc_response_plan,
					'num_crt'=>$num_crt,
					'num_stk'=>$num_stk,
					'num_plan'=>$num_plan,
					'waiting_contact_crtmem'=>$waiting_contact_crtmem,
					'waiting_contact_stkmem'=>$waiting_contact_stkmem,
					'review_date'=>$review_date,
					'num_standby_message'=>$num_standby_message,
					'num_reminder'=>$num_reminder,
					'num_group'=>$num_group,
					'the_scene'=>$the_scene,
					'recall'=>$recall,
					'open_recall'=>$open_recall,
					'all_crt'=>$all_crt,
					'my_class'=>$my_class,
					'casereport'=>$casereport,

					'uniquetime'=>$uniquetime,
					'recall_accumulate'=>$recall_accumulate,
					'currency_code'=>$currency_code,
					'cost_category'=>$cost_category,

					'the_sec'=>$the_sec
					);

			}

			else

			{


				//copied in pre-incident

				/* to display remaining task */

				$num_group=$this->master_model->getRecordCount('user_group',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_crt=$this->master_model->getRecordCount('cf_crisis_response_team',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_stk=$this->master_model->getRecordCount('cf_stakeholder',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_plan=$this->master_model->getRecordCount('cf_scenario',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				/* to display remaining task : End  */

				$waiting_crtmem = $this->common_model->get_waiting_crt($cc_id,$org_id);

				$waiting_stkmem = $this->common_model->get_waiting_stk($cc_id,$org_id);

				$waiting_contact_crtmem = $this->common_model->get_waiting_contact_crt($cc_id,$org_id);

				$waiting_contact_stkmem = $this->common_model->get_waiting_contact_stk($cc_id,$org_id);

				$cc_document = $this->common_model->get_documents($cc_id,$org_id);

				$cc_response_plan=$this->common_model->check_response_plan_generated($cc_id,$org_id);

				$review_date=$this->master_model->getRecords('cf_cc_reminders',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_standby_message=$this->master_model->getRecordCount('cf_standby_messages',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				$num_reminder=$this->master_model->getRecordCount('cf_cc_reminders',array('cc_id'=>$cc_id,'org_id'=>$org_id));

				//---------------end of copied from pre incident-------------------//


				/* fetch initiated scenario for displying common messages */

				$whr_arr1=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario_status'=>'1');

				$scenarios=$this->master_model->getRecords('cf_scenario',$whr_arr1,'*',array('scenario_id'=>'ASC'));

				/* fetch initiated scenarion for displying task and team assign for it */

				$initiated_scn=$this->common_model->get_initiated_scenario($cc_id,$org_id);



				/* fetch all task assign to CC himself */

				$whr_arr2=array('task.crt_id'=>$this->session->userdata('logged_cc_login_id'),'scn.scenario_status'=>'1','scn.org_id'=>$this->session->userdata('cc_selected_orgnaization'),'task.task_status'=>'1');

				$order=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');

				$this->db->join('cf_task as task','task.scenario_id=scn.scenario_id');

				$this->db->join('cf_crisis_response_team as team','team.login_id=task.crt_id');

				$my_task=$this->master_model->getRecords('cf_scenario as scn',$whr_arr2,'',$order);

				$standby_messages=$this->master_model->getRecords('cf_standby_messages',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')));

				$cf_stakeholder=$this->master_model->getRecords('cf_stakeholder',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')));

				$cf_file_upload=$this->master_model->getRecords('cf_file_upload',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')));

				$data=array(
					'page_title'=>'Dashboard',
					'middle_content'=>'dashboard',
					'error'=>'',
					'success'=>'',
					'initiated_scn'=>$initiated_scn,


					'waiting_crtmem'=>$waiting_crtmem,
					'waiting_stkmem'=>$waiting_stkmem,
					'cc_document'=>$cc_document,
					'cc_response_plan'=>$cc_response_plan,
					'num_crt'=>$num_crt,
					'num_stk'=>$num_stk,
					'num_plan'=>$num_plan,
					'waiting_contact_crtmem'=>$waiting_contact_crtmem,
					'waiting_contact_stkmem'=>$waiting_contact_stkmem,
					'review_date'=>$review_date,
					'num_standby_message'=>$num_standby_message,
					'num_reminder'=>$num_reminder,
					'num_group'=>$num_group,


					'scenarios'=>$scenarios,
					'my_task'=>$my_task,
					'all_crt'=>$all_crt,
					'cf_file_upload'=>$cf_file_upload,
					'cf_stakeholder'=>$cf_stakeholder,
					'standby_messages'=>$standby_messages,
					'recall'=>$recall,
					'open_recall'=>$open_recall,
					'my_class'=>$my_class,
					'casereport'=>$casereport,

					'uniquetime'=>$uniquetime,
					'recall_accumulate'=>$recall_accumulate,
					'currency_code'=>$currency_code,
					'cost_category'=>$cost_category,

					'the_sec'=>$the_sec
				);

			}

			if(isset($_POST['add_message'])){

				$this->form_validation->set_rules('common_message','Message','required|xss_clean');

				$this->form_validation->set_message('required', 'Mandatory field');

				if($this->form_validation->run()){

					$common_message=$this->input->post('common_message',true);

					$scenario_id=$this->input->post('scenario_id',true);

					$sender_id=$this->session->userdata('logged_cc_login_id');

					$this->db->set('send_date','now()',FALSE);

					$ins_array=array('scenario_id'=>$scenario_id,'sender_id'=>$sender_id,'common_message'=>$common_message);

					if($this->master_model->insertRecord('common_messages',$ins_array)){

						$this->session->set_flashdata('success','Message send successfully.');

						redirect(base_url().'cc/dashboard');

					}

				}

			}

			//change view if pre incident
			//active modules
			$org_id = $this->session->userdata('cc_selected_orgnaization');
			$main_active_module = substr($this->session->userdata('org_module'), 0,1);
			$active_feature_modules = $this->common_model->active_module($org_id);

			$active_module =  $main_active_module.$active_feature_modules;

			$the_active_module = array('0'=>array('active_module'=>$active_module));

			$mymodyul = substr($this->session->userdata('org_module'),0,1);
			$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));

			$setup_status_completed = $this->common_model->setup_status_completed();

			if(empty($setup_status_completed)){
				redirect(base_url().'cc/preincident?status=preincident');
			}

			$this->load->view('cc/cc-view',$data);

		}

	}



	#---------------------------------------------->>Admin login page<<-----------------------------------------

	public function login() {

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword

		$error="";

		if($this->session->userdata('logged_cc_login_id')!="")

		{

			redirect(base_url().'cc/dashboard');

		}

		else {

			if(isset($_POST['btn_cc_login']))

			{


				$this->form_validation->set_rules('user_name','Username','required|xss_clean');

				$this->form_validation->set_rules('pass_word','Password','required|xss_clean');


				$this->form_validation->set_message('required', 'Mandatory field');
				if($this->form_validation->run())

				{
					//$this->session->cookie_monster($this->input->post('remember_me') ? FALSE : TRUE); //end session when browser closes
					$chk_arr=array('email_id'=>$this->input->post('user_name',true),

									'pass_word'=>md5($this->input->post('pass_word',true)));



					$this->db->join('cf_coordinator_master','cf_login_master.login_id=cf_coordinator_master.login_id');

					$row=$this->master_model->getRecords('cf_login_master',$chk_arr);



					if(count($row) > 0) {

						$user_data = array(
							'logged_cc_login_id'=>$row[0]['login_id'],
							'logged_cc_email_id'=>$row[0]['email_id'],
							'logged_parent_cc'=>$row[0]['login_id'],
							'logged_user_type'=>$row[0]['user_type'],
							'logged_display_name'=>$row[0]['cc_first_name'].' '.$row[0]['cc_last_name'],
							'logged_user_level'=>$row[0]['user_level']
						);



						$this->session->set_userdata($user_data);

						redirect(base_url().'cc/dashboard/selectorganization');

					}

					else

					{$error="Invalid Credentials";}

				}

				else

				{$error=$this->form_validation->error_string();}

			}

			$data = array(
				'page_title'=>'CC Login',
				'error'=>$error,
				'success'=>''
			);

			$this->load->view('cc/index',$data);

		}

	}



	public function selectmodule() {

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword

		$whr = array('organization_id'=>$this->session->userdata('cc_selected_orgnaization'));
		$org_modules = $this->master_model->getRecords('organization_master',$whr);

		//get validation message from admin
		$action_message_id = '75';
		$action_message = $this->common_model->get_message($action_message_id);


		// if(isset($_POST['btn_select_org'])){
		//
		// 	$this->form_validation->set_rules('org_module','Module','required|xss_clean');
		//
		// 	$this->form_validation->set_message('required', 'Mandatory field');
		// 	if($this->form_validation->run())
		//
		// 	{

				$org_module = '5-6-7-3-9-0-b'; //$this->input->post('org_module');

				//delete unused csv files
				$cc_id = $this->session->userdata('logged_cc_login_id');
				$org_id = $this->session->userdata('cc_selected_orgnaization');
				$recalls = $this->master_model->getRecords('cf_recall', array('cc_id'=>$cc_id, 'org_id'=>$org_id,'closed'=>'1'));

				$active_module = substr($org_module, 0, 1);
				$date_added = $this->common_model->userdatetime();

				//set values to store
				$arr = array(
					'org_id'=>$org_id,
					'cc_id'=>$cc_id,
					'crt_id'=>$cc_id,
					'module_id'=>$active_module,
					'type'=>'4', //cc_login
					'date_added'=>$date_added
				);

				//insert arr to audit_log table
				$this->master_model->insertRecord('audit_log', $arr);

				$this->session->set_userdata('org_module',$org_module);

				if(count($recalls) > 0){
					foreach($recalls as $r=>$value){
						if($value['csv_file_name'] != ''){
							@unlink('uploads/recall-csv/'.$value['csv_file_name']);
						}
					}
				}


				redirect(base_url().'cc/dashboard/');

		// 	}
		// 	else{
		// 		$this->session->set_flashdata('error', $action_message['error']);
		// 		redirect(base_url().'cc/dashboard/selectmodule');
		// 	}
		// }


		// $data = array(
		// 	'page_title'=>"Select Module",
		// 	'middle_content'=>'select-module',
		// 	'success'=>'',
		// 	'error'=>'',
		// 	'org_modules'=>$org_modules
		// );
		//
		// $this->load->view('cc/select-module',$data);



	}


	public function selectorganization()

	{
		redirect('cc/dashboard');
		return false;

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword

		$whr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'));

		$order=array('organization_name'=>'ASC');

		$organizationinfo=$this->master_model->getRecords('organization_master',$whr,'*',$order);

		//get validation message from admin
		$action_message_id = '76';
		$action_message = $this->common_model->get_message($action_message_id);

		if(isset($_POST['btn_select_org'])) {

			$this->form_validation->set_rules('org_name','Organization Name','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run()) {

				$org_name=$this->input->post('org_name');

				$this->session->set_userdata('cc_selected_orgnaization',$org_name);



				$name_of_org=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_name),'organization_name');

				$this->session->set_userdata('cc_selected_orgnaization_name',$name_of_org[0]['organization_name']);

				redirect(base_url().'cc/dashboard/');

			}
			else{
				$this->session->set_flashdata('error', $action_message['error']);
				redirect(base_url().'cc/dashboard/selectorganization');
			}
		}

		//echo $this->session->userdata('logged_cc_login_id');

		//print_r($organizationinfo); exit;

		$data = array(
			'page_title'=>"Select Organization",
			'middle_content'=>'select-organisation',
			'success'=>'',
			'error'=>'',
			'organizationinfo'=>$organizationinfo
		);

		$this->load->view('cc/select-organisation',$data);

	}




	#---------------------------------------------->>Admin Forgot password<<-----------------------------------------

	public function forgot() {

		$success="";

		$error="";

		$whr=array('id'=>'1');

		$email_id = $this->master_model->getRecords('admin_login',$whr,'*');

		$data = array(
			'page_title'=>"Forget Password",
			'middle_content'=>'forget-password',
			'success'=>'','error'=>''
		);


		//get validation message from admin
		$action_message_id = '33';
		$action_message = $this->common_model->get_message($action_message_id);


		if(isset($_POST['btn_forget'])){

			$this->form_validation->set_rules('user_name','User name','trim|required|xss_clean|valid_email');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()){

				$email=$this->input->post('user_name',true);

				if($email==$email_id[0]['recovery_email']){

					$info_arr = array(
						'from'=>$email_id[0]['recovery_email'],
						'to'=>$email_id[0]['recovery_email'],
						'subject'=>'Password Recovery',
						'view'=>'forget-password-mail-to-admin'
					);

					$other_info = array(
						'password'=>$email_id[0]['pass_word'],
						'email'=>$email
					);

					$this->email_sending->sendmail($info_arr,$other_info);

					$data['success']= $action_message['success'];

				}

				else{

					$data['error'] = $action_message['error'];

				}

			}

			else {

				$data['error']=$this->form_validation->error_string();

    		}

		}

//				$whr=array('email_id'=>'snailbob01@gmail.com');
//
//				$this->db->join('cf_coordinator_master','cf_coordinator_master.login_id=cf_login_master.login_id');
//
//				$email_id=$this->master_model->getRecords('cf_login_master',$whr,'*');
//
//		$data['emailss'] = $email_id;

		$this->load->view('cc/forget-password',$data);

	}




	#---------------------------------------------->>CC Forgot password<<-----------------------------------------


	public function forgotpassword(){

		$success = "";
		$error = "";

		$data = array(
			'page_title'=>"Forget Password",
			'middle_content'=>'forget-password',
			'success'=>'',
			'error'=>''
		);

		//get validation message from admin
		$action_message_id = '33';
		$action_message = $this->common_model->get_message($action_message_id);

		if(isset($_POST['btn_forget'])){

			$this->form_validation->set_rules('user_name','Email address','trim|required|xss_clean|valid_email');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()){

				$email=$this->master_model->encryptIt($this->input->post('user_name',true));

				$whr=array('email_id'=>$email);

//				$this->db->join('cf_coordinator_master','cf_coordinator_master.login_id=cf_login_master.login_id');
				$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_login_master.login_id');

				$email_id = $this->master_model->getRecords('cf_login_master',$whr,'*');


//				$whr2=array('login_id'=>$email_id[0]['login_id']);
//				$cc_name=$this->master_model->getRecords('cf_crisis_response_team',$whr2,'*');
//


				$whr = array('id'=>'1');

				$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

				    //generate random password

					$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

					$pass = array(); //remember to declare $pass as an array

					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

					for ($i = 0; $i < 8; $i++)  {

						$n = rand(0, $alphaLength);

						$pass[] = $alphabet[$n];

					}

					$random_pass= implode($pass);



//				if(count($email_id) > 0)
				if($email_id != null) {

					$this->master_model->updateRecord('cf_login_master',array('pass_word'=>md5($random_pass)),

					array('login_id'=>$email_id[0]['login_id']));



					$info_arr = array(
						'from'=>$adminemail[0]['recovery_email'],
						'to'=>$this->master_model->decryptIt($email),
						'subject'=>'Password Recovery',
						'view'=>'forget-password-mail-to-cc'
					);



					$other_info = array(
						'password'=>$random_pass,
						'email'=>$this->master_model->decryptIt($email),
						'first_name'=>'',//$cc_name[0]['cc_first_name'],
						'last_name'=>''//$email_id[0]['cc_last_name']
					);



					if($this->email_sending->sendmail($info_arr,$other_info)){

						$this->master_model->updateRecord('cf_login_master',array('pass_word'=>md5($random_pass)),

						array('login_id'=>$email_id[0]['login_id']));

//						$this->session->set_flashdata('success','Please check your email.');


						$data['success'] = $action_message['success'];
//						redirect(base_url().'signin');
					}

				}

				else {
//					$this->session->set_flashdata('error','Email Address is invalid.');
					$data['error'] = $action_message['error'];
				}

			}

			else

			{
				$data['error']=$this->form_validation->error_string();
//				$this->session->set_flashdata('error',$this->form_validation->error_string());
			}

		}

		$this->load->view('cc/forget-password',$data);

	}



	#----------------------------------------------->>cc Logout<<------------------------------------------------

	public function logout() {

		$this->cc_check_session->checksessionvalue();  //added to individual function but not to forgotpassword
		//$this->session->sess_destroy();

		$org_module = $this->session->userdata('org_module');
		$active_module = substr($org_module, 0, 1);
		$cc_id = $this->session->userdata('team_cc');
		$user_id = $this->session->userdata('user_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$date_added = $this->common_model->userdatetime();

		//set values to store
		$arr = array(
			'org_id'=>$org_id,
			'cc_id'=>$cc_id,
			'crt_id'=>$user_id,
			'module_id'=>$active_module,
			'type'=>'1', //cc_logout
			'date_added'=>$date_added
		);

		//insert arr to audit_log table
		$this->master_model->insertRecord('audit_log', $arr);


		$user_data = $this->session->all_userdata();
		
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		


		// $this->session->unset_userdata('logged_cc_login_id');
		// $this->session->unset_userdata('cc_selected_orgnaization');
		// $this->session->unset_userdata('cc_selected_orgnaization_name');
		// $this->session->unset_userdata('forum_last_visit');

		// $this->session->unset_userdata('country_id');

		// $this->session->unset_userdata('timezone');

		// $this->session->unset_userdata('team_cc');


		// $this->session->unset_userdata('logged_cc_email_id');

		// $this->session->unset_userdata('logged_parent_cc');

		// $this->session->unset_userdata('logged_user_type');

		// $this->session->unset_userdata('logged_display_name');

		// $this->session->unset_userdata('logged_user_level');

		// $this->session->unset_userdata('org_module');



		redirect(base_url().'signin/');

	}



	public function closescenario(){

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword
		$scenario_id=$this->uri->segment(4);


		//get validation message from admin
		$action_message_id = '34';
		$action_message = $this->common_model->get_message($action_message_id);

		$scenario=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenario_id));

		$scene_initiated = new DateTime($scenario[0]['initiation_date']);


		//timezone datetime
		$nowtime = $this->common_model->userdatetime();

		$scene_closed = new DateTime($nowtime);
		$difference = $scene_initiated->diff($scene_closed);

		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';


		//adding complete date and lapse time to closed scenario to log table
		$data = array(
			'completed_date' => $nowtime,
			'lapse_time' => $interval
		);

		$this->db->where('initiation_date', $scenario[0]['initiation_date']);
		$this->db->update('log_scene_master', $data);



		/* first complete scenario and also complete task */

		$this->master_model->updateRecord('cf_scenario',array('scenario_status'=>'0'),array('scenario_id'=>$scenario_id));


		$this->master_model->updateRecord('cf_task',array('task_status'=>'0'),array('scenario_id'=>$scenario_id));

		$crt=$this->master_model->getRecords('cf_crisis_response_team',array('cc_id'=>$this->session->userdata('logged_cc_login_id')));

		$stakeholder=$this->master_model->getRecords('cf_stakeholder',array('cc_id'=>$this->session->userdata('logged_cc_login_id')));

		$adminemail=$this->master_model->getRecords('email_id_master');

		$cc_name=$this->session->userdata('logged_display_name');

		$cc_email=$this->session->userdata('logged_cc_email_id');


//			//send close email to cc
//			$info_arr=array('from'=>$adminemail[0]['contact_email'],
//
//						'to'=>$cc_email,
//
//						'subject'=>'Crisis incident closed',
//
//						'view'=>'incident_close_mail'); //task_completion_mail_to_cc
//
//
//
//			 $other_info=array(
//
//						  'first_name'=>$crt[$i]['crt_first_name'],
//
//						  'last_name'=>$crt[$i]['crt_last_name'],
//
//						  'scenario_name'=>$scenario[0]['scenario'],
//
//						  'cc_name'=>$cc_name);
//
//			$this->email_sending->sendmail($info_arr,$other_info);
//			//.send close email to cc


		if(count($crt)>0){

			for($i=0;$i<count($crt);$i++){

				$info_arr=array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->master_model->decryptIt($crt[$i]['crt_email']),
					'subject'=>'Crisis incident closed',
					'view'=>'incident_close_mail'
				); //task_completion_mail_to_cc



				 $other_info = array(
				 	'first_name'=>$this->master_model->decryptIt($crt[$i]['crt_first_name']),
					'last_name'=>$this->master_model->decryptIt($crt[$i]['crt_last_name']),
					'scenario_name'=>$scenario[0]['scenario'],
					'cc_name'=>$cc_name
				);

				$this->email_sending->sendmail($info_arr,$other_info);

			}

		}

		if(count($stakeholder)>0){

			for($i=0;$i<count($stakeholder);$i++){

				$info_arr1 = array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->master_model->decryptIt($stakeholder[$i]['stk_email_address']),
					'subject'=>'Crisis incident closed',
					'view'=>'incident_close_mail'
				); //task_completion_mail_to_cc



				 $other_info1 = array(
				 	'first_name'=>$this->master_model->decryptIt($stakeholder[$i]['stk_first_name']),
					'last_name'=>$this->master_model->decryptIt($stakeholder[$i]['stk_last_name']),
					'scenario_name'=>$scenario[0]['scenario'],
					'cc_name'=>$cc_name
				);

				$this->email_sending->sendmail($info_arr1,$other_info1);

			}

		}

		// Delete all message related to this scenario

		$this->master_model->deleteRecord('common_messages','scenario_id',$scenario_id);



		//$this->session->set_flashdata('success',' You have successfully closed incident '.ucfirst($scenario[0]['scenario']));
		$this->session->set_flashdata('success', $action_message['success']);

		redirect(base_url().'cc/dashboard');

	}



	public function delete_task(){

		$this->cc_check_session->checksessionvalue();
		$task_id=$this->uri->segment(4);

		//get validation message from admin
		$action_message_id = '35';
		$action_message = $this->common_model->get_message($action_message_id);

		$this->master_model->deleteRecord('cf_task','task_id',$task_id);

		$this->session->set_flashdata('success', $action_message['success']);

		redirect(base_url().'cc/dashboard');

	}

	/* Complete task assign to CC */

	public function changestatus() {

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword
		$task_id=$this->uri->segment(4);

		$adminemail = $this->master_model->getRecords('email_id_master');

		//get validation message from admin
		$action_message_id = '36';
		$action_message = $this->common_model->get_message($action_message_id);

		/* get cc info */

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.cc_id');

		$cc_info=$this->master_model->getRecords('cf_task as task',array('task.task_id'=>$task_id));

		/* get crt info */

		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_task.crt_id');

		$this->db->join('cf_scenario','cf_scenario.scenario_id=cf_task.scenario_id');

		$task_info=$this->master_model->getRecords('cf_task',array('task_id'=>$task_id));

		//timezone datetime
		$nowtime = $this->common_model->userdatetime();

		if($this->master_model->updateRecord('cf_task',array('task_status'=>'2'),array('task_id'=>$task_id))) {

			//insert task log
			$taskslog = array(
				'scenario_id'=>$task_info[0]['scenario_id'],
				'task_id'=>$task_info[0]['task_id'],
				'task'=>$task_info[0]['task'],
				'crt_id'=>$task_info[0]['crt_id'],
				'cc_id'=>$task_info[0]['cc_id'],
				'org_id'=>$task_info[0]['org_id'],
				'current_response'=>$task_info[0]['current_response'],
				'completed_date'=>$nowtime
				);

			$login_id=$this->master_model->insertRecord('log_task_master',$taskslog,true);

			/*$info_arr=array('from'=>$adminemail[0]['contact_email'],

						'to'=>$cc_info[0]['crt_email'],

						'subject'=>'Task completion Mail',

						'view'=>'task_completion_mail_to_cc');



			 $other_info=array(

						  'first_name'=>$cc_info[0]['crt_first_name'],

						  'last_name'=>$cc_info[0]['crt_last_name'],

						  'scenario_name'=>$task_info[0]['scenario'],

						  'task_name'=>$task_info[0]['task'],

						  'crt_name'=>$task_info[0]['crt_first_name'].' '.$task_info[0]['crt_last_name']);



			$this->email_sending->sendmail($info_arr,$other_info);*/


			$this->session->set_flashdata('success', $action_message['success']);

			redirect(base_url().'cc/dashboard');

		}

	}



	/*message details view */

	public function read(){

		$this->cc_check_session->checksessionvalue(); //added to individual function but not to forgotpassword
		$message_id=$this->uri->segment(4);

		$my_class = $this;

		$this->db->join('cf_scenario','cf_scenario.scenario_id=common_messages.scenario_id');

		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=common_messages.sender_id');

		$msg_details=$this->master_model->getRecords('common_messages',array('com_message_id'=>$message_id));



		$data = array(
			'page_title'=>'Message Details',
			'middle_content'=>'common-messages-details',
			'msg_details'=>$msg_details,
			'my_class'=>$my_class
		);

		$this->load->view('cc/cc-view',$data);

	}


	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);

			return $crtname;
		}
		else{
			return 'Unexisting User';
		}


	}



	//recall cost monitor
	public function currencycode($id) {

		$currency = $this->master_model->getRecords('countries_currency',array('idCountry'=>$id));
		if(count($currency) > 0){
			return $currency[0]['currencyCode'];
		}else{
			return 'Currency Unknown';
		}
	}


	public function accumulate($recall){


		if(count($recall) == 0){
			$myaccumulate = 'No Cost Items';
			return $myaccumulate;
		}
		else{

			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			if($active_mod == '5'){
				$themod = 'recall_id';
			}
			else{
				$themod = 'continuity_id';
			}


			$recall_items = $this->master_model->getRecords('cf_cost_category_item',array($themod=>$recall[0]['id']),'*',array('currency'=>'ASC'));

			$myaccumulate = '';
			if(count($recall_items) > 0){

				$mycurr = $recall_items[0]['currency'];
				$totalcurr = 0;
				$m = 0;

				foreach($recall_items as $it=>$ems){
					if($mycurr != $ems['currency']){

						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2).' + ';

						$mycurr = $ems['currency'];
						$totalcurr = $ems['item_cost'];


					}else{

						$totalcurr += $ems['item_cost'];
					}

					if((count($recall_items) - 1) == $m){
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2);
					}

					$m++;
				}


			}else{
				$myaccumulate .= 'No Cost Items';
			}
			return $myaccumulate;

		}


	}


	public function endtourforever(){
		$value = $_POST['value'];
		$user_id = $this->session->userdata('user_id');

		$arr = array(
			'endtourforever'=>$value
		);
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		echo json_encode(array('value'=>$value));

	}


}

?>

<?php

class Document extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('upload');

		$this->load->helper('download');

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

	}

	

	public function upload() {

		if(isset($_POST['upload_doc'])) {
			
			//get validation message from admin
			$action_message_id = '77';
			$action_message = $this->common_model->get_message($action_message_id);
				
			//$this->form_validation->set_rules('file_upload_name','File Input','required');

			$this->form_validation->set_rules('c_pack','Crisis Pack','required');

			$this->form_validation->set_rules('group','User Group','required');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()) {
				
				$c_pack = $this->input->post('c_pack');

				$group = $this->input->post('group');

				$selectcrt = $this->input->post('selectcrt');

				$selectgroup = $this->input->post('selectgroup');
				
				$category = $this->input->post('category');
				

				if($_FILES['file_upload_name']['name']!="" && $_FILES['file_upload_name']['error']==0) {
	
					$doc_config = array(
						'file_name'=>uniqid().$_FILES['file_upload_name']['name'],
						'allowed_types'=>'doc|docx|pdf|txt',
						'upload_path'=>'uploads/crisis-document/',
						'max_size'=>'0'
					);
	
					$this->upload->initialize($doc_config);
	
					//print_r($_FILES['file_upload_name']);exit;
	
					if($this->upload->do_upload('file_upload_name')) {
	
						$upload_data=$this->upload->data();
	
						$file_name=$upload_data['file_name'];
	
						$insert_array=array(
							'file_upload_name'=>$file_name,
							'cc_id'=>$this->session->userdata['logged_cc_login_id'],
							'c_pack'=>$c_pack,
							'group_id'=>$group,
							'rights_to_indi'=>$selectcrt,
							'category_id'=>$category,
							'org_id'=>$this->session->userdata['cc_selected_orgnaization']
						);
	
						if($thedoc = $this->master_model->insertRecord('cf_file_upload',$insert_array,true)){
							
							$this->session->set_flashdata('success', $action_message['success']);
							redirect(base_url().'cc/document/scan/'.$thedoc);	
	
						}
	
					}
					else { 
	
						$this->session->set_flashdata('error',$this->upload->display_errors());
						redirect(base_url().'cc/document/manage');	
	
					}
				}

				else {

					$this->session->set_flashdata('error', $action_message['error']);

					redirect(base_url().'cc/document/upload');

				}


			}

			

			

		}
		
		//crts
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_cc_login_id'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization')
		);
		
		$crts = $this->master_model->getRecords('cf_crisis_response_team',$whr_arr);
		
		$arr = array('org_id'=>$this->session->userdata['cc_selected_orgnaization']);
		$all_group = $this->master_model->getRecords('user_group', $arr);
		$categories = $this->master_model->getRecords('document_category', $arr);

		$data = array(
			'page_title'=>'Upload Document',
			'middle_content'=>'upload-document',
			'all_group'=>$all_group,
			'crts'=>$crts,
			'categories'=>$categories
		);

		$this->load->view('cc/cc-view',$data);

	}



	
	public function move_doc(){
		$categories = $_POST['categories'];
		$id = $_POST['doc_id'];

		//get validation message from admin
		$action_message_id = '78';
		$action_message = $this->common_model->get_message($action_message_id);


		$arr = array(
			'category_id'=>$categories
		);
		
		$this->master_model->updateRecord('cf_file_upload', $arr, array('file_upload_id'=>$id));
		$this->session->set_flashdata('success', $action_message['successs']);
		echo 'ok';
		
	}
	
	
	//update doc group
	public function update() {
		$doc_id=$this->uri->segment(4);

		if(isset($_POST['upload_doc'])) {
			//get validation message from admin gen update = 69, delete = 63
			$action_message_id = '69';
			$action_message = $this->common_model->get_message($action_message_id);
			
			
			//$this->form_validation->set_rules('file_upload_name','File Input','required');

			$this->form_validation->set_rules('c_pack','Crisis Pack','required');

			$this->form_validation->set_rules('group','User Group','required');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run()) {
				
				$c_pack = $this->input->post('c_pack');

				$group = $this->input->post('group');

				$selectcrt = $this->input->post('selectcrt');

				$selectgroup = $this->input->post('selectgroup');
				$category = $this->input->post('category');


				$insert_array = array(
					'cc_id'=>$this->session->userdata['logged_cc_login_id'],
					'c_pack'=>$c_pack,
					'group_id'=>$group,
					'rights_to_indi'=>$selectcrt,
					'category_id'=>$category,
					'org_id'=>$this->session->userdata['cc_selected_orgnaization']
				);

				if($this->master_model->updateRecord('cf_file_upload',$insert_array,array('file_upload_id'=>$doc_id))){

					$this->session->set_flashdata('success', $action_message['success']);

					redirect(base_url().'cc/document/manage');	

				}

				else {
	
					$this->session->set_flashdata('error', $action_message['error']);
	
					redirect(base_url().'cc/document/manage');
	
				}

			}


			

		}
		//crts
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_cc_login_id'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization')
		);
		
		$crts=$this->master_model->getRecords('cf_crisis_response_team',$whr_arr);
		
		$arr = array('org_id'=>$this->session->userdata['cc_selected_orgnaization']);
		$all_group = $this->master_model->getRecords('user_group', $arr);
		$categories = $this->master_model->getRecords('document_category', $arr);


		$the_doc=$this->master_model->getRecords('cf_file_upload',array('file_upload_id'=>$doc_id));

		$data=array(
			'page_title'=>'Upload Document',
			'middle_content'=>'update-document',
			'all_group'=>$all_group,
			'the_doc'=>$the_doc,
			'crts'=>$crts,
			'categories'=>$categories
		);

		$this->load->view('cc/cc-view',$data);

	}


	// Manage documents

	public function manage()

	{
		$org_id = $this->session->userdata['cc_selected_orgnaization'];
		
		$arr = array(
			'org_id'=>$org_id
		);
		
		$arr2 = array(
			'org_id'=>$org_id,
			'category_id'=>0
		);
		$all_doc = $this->master_model->getRecords('cf_file_upload', $arr2);
		
		$user_group = $this->master_model->getRecords('user_group', $arr);
		
		$categories = $this->master_model->getRecords('document_category', $arr);
		
		$data = array(
			'page_title'=>"Manage Crises Documents",
			'middle_content'=>'manage-document',
			'success'=>'',
			'error'=>'',
			'all_doc'=>$all_doc,
			'user_group'=>$user_group,
			'categories'=>$categories
		);

		$this->load->view('cc/cc-view',$data);

	}

	// Scan documents
	public function scan() {
		//get validation message from admin gen update = 96, delete = 63
		$action_message_id = '77';
		$action_message = $this->common_model->get_message($action_message_id);

		$this->session->set_flashdata('success', $action_message['success']);
		redirect('cc/document/manage');
		return false;
		
		$thedoc = $this->uri->segment(4);

		$up_doc=$this->master_model->getRecords('cf_file_upload',array('file_upload_id'=>$thedoc, 'org_id'=>$this->session->userdata['cc_selected_orgnaization']));

		// Config to scan.
		$api    = 'https://scan.metascan-online.com/v2/file';
		$apikey = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		$file   = base_url();
		$file.='uploads/crisis-document/';
		$file.=$up_doc[0]['file_upload_name'];
		
		// Build headers array.
		$headers = array(
			'apikey: '.$apikey,
			'filename: '.basename($file)
		);
		
		// Build options array.
		$options = array(
			CURLOPT_URL     => $api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_POST        => true,
			CURLOPT_POSTFIELDS  => file_get_contents($file),
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		// Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$response = json_decode(curl_exec($ch), true);
		
		
		$insert_array = array(
			'data_id'=>$response['data_id'],
			'rest_ip'=>$response['rest_ip']
		);
		$this->master_model->updateRecord('cf_file_upload',$insert_array,array('file_upload_id'=>$thedoc));

		redirect(base_url().'cc/document/manage/'.$thedoc);
//		$data=array('page_title'=>"Manage Crises Documents",'middle_content'=>'scan-document','success'=>'',

//		'error'=>'','response'=>$response);

//		$this->load->view('cc/cc-view',$data);

	}
	
	// scan response document
	public function scan_response()

	{
		$thedoc = $_POST['file_id'];

		$up_doc=$this->master_model->getRecords('cf_file_upload',array('file_upload_id'=>$thedoc, 'org_id'=>$this->session->userdata['cc_selected_orgnaization']));
		
		

		//Config for req.
		$req_api = 'https://';
		$req_api .= $up_doc[0]['rest_ip']; //'scan05.metascan-online.com:443/v2'; //$rest_ip;
		$req_api .= '/file/';
		$req_api .=  $up_doc[0]['data_id']; //'48271edc06b34edeb86cf27d3fc54377';//$data_id;
		
		$apikey     = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		
		//Build headers array.
		$headers = array(
			'apikey: '.$apikey
		);
		
		//Build options array.
		$options = array(
			CURLOPT_URL     => $req_api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		$response = "";
		//Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		do {
			$response = json_decode(curl_exec($ch), true);
		}
		while ($response["scan_results"]["progress_percentage"] != 100);
		
		//print_r($response);
		
		$this->master_model->updateRecord('cf_file_upload',array('scan_result'=>$response['scan_results']['scan_all_result_a']),array('file_upload_id'=>$thedoc));
		
		
		echo '<p class="lead" style="margin-bottom: 5px;">';
		//doc icon
		$fileext = explode ('.',$up_doc[0]['file_upload_name']);
		if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
		
		echo '<i class="fa fa-file-pdf-o text-red"></i> ';
		
		}
		
		else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
		
		echo '<i class="fa fa-file-word-o text-blue"></i> ';
		
		}
		
		else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
		
		echo '<i class="fa fa-file-text-o text-muted"></i> ';
		
		}
		
		
		echo substr($up_doc[0]['file_upload_name'],13,50);		
		echo '</p>';
		echo '<p class="lead"  style="margin-bottom: 5px;">';
		echo 'Over all Scan Result: '.$response['scan_results']['scan_all_result_a'];		
		
		echo '</p>';
		
		
		echo ' <div class="table-responsive"> <table class="table table-hover table-bordered table-green" id="response-table"> <thead> <tr>  <th width="25%">Antivirus</th>  <th width="25%">Result</th>  <th width="25%">Scan Time</th> <th width="25%">Update</th> </tr> </thead> <tbody>';
								
								
		foreach ($response['scan_results'] as $key => $scan_details){
			if(is_array($scan_details)){
				foreach($scan_details as $subkey => $subvalue){
					echo '<tr> <td>'.$subkey.'</td>';
					if(is_array($subvalue)){
						//foreach($subvalue as $key => $subsubvalue){
							echo '<td class="text-center">';
							if($subvalue['threat_found']==""){
								echo '<i class="fa fa-check-circle text-success" title="No Threat Found"></i>';
							}
							else{
								echo '<i class="fa fa-exclamation-circle text-danger" title="Threat Found"></i><br>'.$subvalue['threat_found'];
							}
							echo '</td>';
							//echo '<td>'.$subvalue['threat_found'].'</td>'; $subvalue['scan_result_i']
							echo '<td>'.$subvalue['scan_time'].' ms</td>';
							echo '<td>'.substr($subvalue['def_time'], 0, 10).'</td>';
						//}
						
					echo '</tr>';
					} else {
						echo $subkey.' '.$subvalue."<br />";
					}
				}
			} else {
			//	echo $key.': '.$scan_details."<br />";
			}
		}	
		
		echo ' </tbody> </table> </div><p class="small hidden">Virus scan results from metascan-online.com</p>';
			
	}


	// Download document
	public function download()

	{

		$doc_name=$this->uri->segment(4);

		$data=file_get_contents("uploads/crisis-document/".$doc_name);

		$name=substr($doc_name,13,50);

		force_download($name,$data);

	}

	// Delete document

	public function delete() {

		$doc_id=$this->uri->segment(4);

		$doc_name=$this->uri->segment(5);	

		//get validation message from admin gen update = 96, delete = 63
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);

		if($this->master_model->deleteRecord('cf_file_upload','file_upload_id',$doc_id)) {

			@unlink('uploads/crisis-document/'.$doc_name);

			$this->session->set_flashdata('success', $action_message['success']);

			redirect(base_url().'cc/document/manage');

		}

	}
	
	
	public function scandoc() {
		
		// Config.
		$api    = 'https://scan.metascan-online.com/v2/file';
		$apikey = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		$file   = 'http://localhost/nga_nguyen/crisisflo.com/client/uploads/crisis-document/53a7ac2a20220ZenPen.txt';
		
		// Build headers array.
		$headers = array(
			'apikey: '.$apikey,
			'filename: '.basename($file)
		);
		
		// Build options array.
		$options = array(
			CURLOPT_URL     => $api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_POST        => true,
			CURLOPT_POSTFIELDS  => file_get_contents($file),
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		// Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$response = json_decode(curl_exec($ch), true);
		
		print_r($response);
		
		
	}
	
	
	
	public function addcategory(){
		$name = $_POST['name'];
		$id = $_POST['id'];
		$cc_id = $this->session->userdata('team_cc');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$group = array();
		if(isset($group)){
			$group = $_POST['group'];
		}
		
		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'name'=>$name,
			'groups'=>serialize($group)
		);
		if($id != ''){
			$this->master_model->insertRecord('document_category', $arr, array('id'=>$id));
		}
		else{
			$this->master_model->insertRecord('document_category', $arr);
		}
		echo json_encode($arr);
	}
	
	
}

?>
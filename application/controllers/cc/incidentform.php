<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Incidentform extends CI_Controller 

{

	public function __construct(){

		parent::__construct();

		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

	}

	
	public function session(){
		$sess = $this->session->all_userdata();
		echo json_encode($sess);
	}

	public function index(){

		//$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
	
		$review_date = $this->master_model->getRecords('cf_cc_reminders',array('cc_id'=>$cc_id,'org_id'=>$org_id));
		$myzone = $this->session->userdata('timezone');
		$timezone = ($myzone) ? $this->master_model->getRecords('time_zone', array('ci_timezone'=>$myzone)) : array();
		

		$whr_arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);
		$user_group_updated = $this->master_model->getRecords('user_group', $whr_arr, 'date_updated', array('date_updated'=>'DESC'));
		
		$crt_updated = $this->master_model->getRecords('cf_login_master', array('default_org'=>$org_id, 'user_level'=>'1'), 'date_updated', array('date_updated'=>'DESC'));
		
		$stk_updated = $this->master_model->getRecords('cf_login_master', array('default_org'=>$org_id, 'user_level'=>'2'), 'date_updated', array('date_updated'=>'DESC'));
		
		$docs_updated = $this->master_model->getRecords('cf_file_upload', $whr_arr, 'date_updated', array('date_updated'=>'DESC'));
		
		$cc_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$cc_id), '*');
		
		$reminder_updated = $this->master_model->getRecords('cf_cc_reminders', $whr_arr, 'date_updated', array('date_updated'=>'DESC'));

		$action_message_id = '102';
		$action_message = $this->common_model->get_message($action_message_id);



		$data = array(
			'page_title'=>'CC Setup',
			'error'=>'',
			'middle_content'=>'pre-incident-form',
			'success'=>'',
			'user_group_updated'=>$user_group_updated,
			'crt_updated'=>$crt_updated,
			'stk_updated'=>$stk_updated,
			'docs_updated'=>$docs_updated,
			'cc_info'=>$cc_info,
			'review_date'=>$review_date,
			'timezone'=>$timezone,
			'action_message'=>(isset($_GET['status'])) ? $action_message : array(),
			'reminder_updated'=>$reminder_updated
		);

		$this->load->view('cc/cc-view',$data);

	}

	public function link(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
	
		$data = array(
			'page_title'=>'CC Setup',
			'error'=>'',
			'middle_content'=>'pre-incident-form-link',
			'success'=>'',
			'action_message'=>(isset($_GET['status'])) ? $action_message : array(),
		);

		$this->load->view('cc/cc-view',$data);

	}

	public function get_incident_custom_form(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);

		$form = $this->master_model->getRecords('cf_recall_form', $arr);
		$arr['questionnaire'] = array();

		if(!empty($form)){
			$arr['questionnaire'] = unserialize($form[0]['questionnaire']);
		}
		echo json_encode($arr);
	}

	public function incident_custom_form(){
		$questionnaire = $_POST['data'];
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
	
		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);

		$form = $this->master_model->getRecords('cf_recall_form', $arr);
		$arr['questionnaire'] = serialize($questionnaire);

		if(!empty($form)){
			$this->master_model->updateRecord('cf_recall_form', $arr, array('id'=>$form[0]['id']));
		}
		else{
			$this->master_model->insertRecord('cf_recall_form', $arr);
		}
		echo json_encode($arr);
	}

	public function save_incident_live_form(){
		$data = $_POST['data'];
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$nowtime = date("Y-m-d h:i:s");

		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'response'=>serialize($data),
			'date_added'=>$nowtime
		);

		$this->master_model->insertRecord('cf_recall_form_responds', $arr);
		echo json_encode($arr);

	}
}

?>
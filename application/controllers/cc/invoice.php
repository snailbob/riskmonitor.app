<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Invoice extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

	}

	

	//------------------------------------post incident

	public function index(){


		$data = array(
			'page_title'=>'Invoice',
			'nav_title'=>'Invoice',
			'middle_content'=>'invoice_view',
			'error'=>'',
			'success'=>'',
		);

		$this->load->view('cc/headless/cc_headless_view', $data);
	}


}

?>
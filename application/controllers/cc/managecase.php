<?php

class Managecase extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

	}

	

	public function index()

	{

		
		$case_list=$this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0'));

		$reported_cases=$this->master_model->getRecords('case_report_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'1','cc_decision'=>'3'));

		

		$data=array('page_title'=>'Manage Case','middle_content'=>'manage-case','case_list'=>$case_list,'reported_cases'=>$reported_cases);

		$this->load->view('cc/cc-view',$data);

	}
	
	
	
	//cc decision
	public function decide()

	{
		$case_id=$this->uri->segment(4);
		
		if(isset($_POST['cc_decision']))

		{

			$this->form_validation->set_rules('decision_note','Decision Note','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())

			{
				if($this->input->post('selectdecision',true) == '0'){
				
					$up_case=$this->master_model->getRecords('case_master',array('id'=>$case_id));
					
					$case_initiated = new DateTime($up_case[0]['date_initiated']);
				
					$nowtime = date("Y-m-d H:i:s");
					$case_closed = new DateTime(date("Y-m-d H:i:s"));
					$difference = $case_initiated->diff($case_closed);
					
					$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';
			
			
					//adding complete date and lapse time to closed scenario to log table
					$data = array(
								   'date_closed' => $nowtime,
								   'status' => '1',
								   'time_lapsed' => $interval
								);
					
					$this->db->where('id', $case_id);
					$this->db->update('case_master', $data); 

				}
				else{
					$up_case=$this->master_model->getRecords('case_master',array('id'=>$case_id));
					
					$case_initiated = new DateTime($up_case[0]['date_initiated']);
				
					$nowtime = date("Y-m-d H:i:s");
					$case_closed = new DateTime(date("Y-m-d H:i:s"));
					$difference = $case_initiated->diff($case_closed);
					
					$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';
			
			
					//adding complete date and lapse time to closed scenario to log table
					$data = array(
								   'date_closed' => $nowtime,
								   'status' => '2',
								   'time_lapsed' => $interval
								);
					
					$this->db->where('id', $case_id);
					$this->db->update('case_master', $data); 
				
				}

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
				
					$this->master_model->updateRecord('case_report_master',array('cc_decision'=>$this->input->post('selectdecision',true),'cc_note'=>$this->input->post('decision_note',true)),$whr_arr);
					$this->session->set_flashdata('success','Case successfully decided .');
					redirect('cc/managecase');
				
				}
				

				else

				{

					$this->session->set_flashdata('error','Error while storing data');

					redirect('cc/managecase');

				}
				
			}
		}
		
		
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_cc_login_id'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);
		
		$data=array('middle_content'=>'add-case-decision','success'=>'','error'=>'','casedocs'=>$casedocs);

		$this->load->view('cc/cc-view',$data);
		
	}
	

	//cc share case to crt
	public function share()

	{
		$case_id=$this->uri->segment(4);
		
		
		//validate cc share
		if(isset($_POST['cc_share']))

		{

			$this->form_validation->set_rules('selectcrt','Response Team','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())

			{
				

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
					if ($casedocs[0]['share_to'] !=''){
						
						$checkcrts = explode(' ',$casedocs[0]['share_to']);
						if (in_array($this->input->post('selectcrt',true), $checkcrts)) {
							$this->session->set_flashdata('error','You already shared this case to the selected Reponse Team member.');
							redirect('cc/managecase/share/'.$case_id);
						}						
						$crts = $casedocs[0]['share_to'].' '.$this->input->post('selectcrt',true);
					}
					else{
						$crts = $this->input->post('selectcrt',true);
					}
					$this->master_model->updateRecord('case_report_master',array('share_to'=>$crts),$whr_arr);
					$this->session->set_flashdata('success','Case successfully shared.');
					redirect('cc/managecase/share/'.$case_id);
				
				}
				

				else

				{

					$this->session->set_flashdata('error','Error while storing data');

					redirect('cc/managecase/share/'.$case_id);

				}
				
			}
		}
		
		//validate group share
		if(isset($_POST['group_share']))

		{

			$this->form_validation->set_rules('selectgroup','Response Team','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())

			{
				

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs = $this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
					if ($casedocs[0]['share_to_group'] !=''){
						
						$checkgroups = explode(' ',$casedocs[0]['share_to_group']);
						if (in_array($this->input->post('selectgroup',true), $checkgroups)) {
							$this->session->set_flashdata('error','You already shared this case to the selected user group.');
							redirect('cc/managecase/share/'.$case_id);
						}						
						$groups = $casedocs[0]['share_to_group'].' '.$this->input->post('selectgroup',true);
					}
					else{
						$groups = $this->input->post('selectgroup',true);
					}
					$this->master_model->updateRecord('case_report_master',array('share_to_group'=>$groups),$whr_arr);
					$this->session->set_flashdata('success','Case successfully shared to selected user group.');
					redirect('cc/managecase/share/'.$case_id);
				
				}
				

				else

				{

					$this->session->set_flashdata('error','Error while storing data');

					redirect('cc/managecase/share/'.$case_id);

				}
				
			}
		}
				
		
		
		//crts
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_cc_login_id'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization')
		);
		
		$crts=$this->master_model->getRecords('cf_crisis_response_team',$whr_arr);
		
		//usergroup
		$usergroup=$this->master_model->getRecords('user_group',$whr_arr);
		
		
		//case info
		$whr_arr2=array(
			'cc_id'=>$this->session->userdata('logged_cc_login_id'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$caseinfo=$this->master_model->getRecords('case_report_master',$whr_arr2);
		
		$data=array(
			'middle_content'=>'add-case-socialize',
			'success'=>'',
			'error'=>'',
			'crts'=>$crts,
			'caseinfo'=>$caseinfo,
			'usergroup'=>$usergroup
		);

		$this->load->view('cc/cc-view',$data);
		
	}
	
	//add case
	public function add()

	{

		if(isset($_POST['add_case']))

		{

			$this->form_validation->set_rules('case_title','Case Title','required|xss_clean');
			$this->form_validation->set_rules('case_det','Case Details','required|xss_clean');
			$this->form_validation->set_rules('crt_mem','Response Team Member','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())

			{

				$crt_id=$this->input->post('crt_mem',true);
				$nowtime = date("Y-m-d H:i:s");

				$insrt_arr=array(
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'gen_id'=>$this->input->post('gen_id',true),
					'cc_case_id'=>$this->input->post('cc_case_id',true),
					'case_title'=>$this->input->post('case_title',true),
					'case_details'=>$this->input->post('case_det',true),
					'crt_id'=>$this->input->post('crt_mem',true),
					'date_initiated'=>$nowtime
				);

				if($case_id=$this->master_model->insertRecord('case_master',$insrt_arr,TRUE))

				{
					$insrt_forreport=array(
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
						'case_id'=>$case_id,
						'crt_id'=>$this->input->post('crt_mem',true)
					);
					
					//insert data for crt case report
					$this->master_model->insertRecord('case_report_master',$insrt_forreport);
					
					
					//get admin email
					$whr=array('id'=>'1');
					$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

					//get crt name email
					$whrr=array('login_id'=>$crt_id);
					$crtinfo=$this->master_model->getRecords('cf_crisis_response_team',$whrr,'*');


					$info_arr=array('from'=>$adminemail[0]['recovery_email'],
	
						'to'=>$this->master_model->decryptIt($crtinfo[0]['crt_email']),

						'subject'=>'New Case',

						'view'=>'crt_new_case'
					);

				

					 $other_info=array(

						'first_name'=>$this->master_model->decryptIt($crtinfo[0]['crt_first_name']),
						
						'last_name'=>$this->master_model->decryptIt($crtinfo[0]['crt_last_name'])
					);

				

					$this->email_sending->sendmail($info_arr,$other_info);

					
					
					
					
					
					
					$this->session->set_flashdata('success','Case successfully added.');
					redirect(base_url().'cc/managecase');

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding scenario');

					redirect('cc/scenario/add');

				}

			}

		}

		
		$excase_id=$this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')),'cc_case_id');
		if(count($excase_id) > 0){

			$numItems = count($excase_id);
			$i = 0;
			foreach($excase_id as $key=>$value) {
			  if(++$i === $numItems) {
				  
				$cc_case_id = $value['cc_case_id'] + 1;

			  }
			}  

				
		}
		
		else{
			$cc_case_id = 1;
		}
		
		/* fetch CR list to assing case, CRT will be of current selected organization */

		$whr_arr=array('crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1');

		$whr_str="(crt.org_id=".$this->session->userdata('cc_selected_orgnaization')." OR crt.org_id=0) AND (login.user_level='1')";

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		

//		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');
//
//		$this->db->where($whr_str,NULL,FALSE);
//
//		$crt_list = $this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);


		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$crt_list = $this->common_model->get_org_crts($cc_id,$org_id);


		$data=array('middle_content'=>'add-case','cc_case_id'=>$cc_case_id,'crt_list'=>$crt_list);

		$this->load->view('cc/cc-view',$data);

	}


	
	//update case
	public function update()

	{
		$case_id=$this->uri->segment(4);

		if(isset($_POST['add_case']))

		{

			$this->form_validation->set_rules('case_title','Case Title','required|xss_clean');
			$this->form_validation->set_rules('case_det','Case Details','required|xss_clean');
			$this->form_validation->set_rules('crt_mem','Response Team Member','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())

			{

				$crt_id=$this->input->post('crt_mem',true);

				$insrt_arr=array(
					'case_title'=>$this->input->post('case_title',true),
					'case_details'=>$this->input->post('case_det',true),
					'crt_id'=>$this->input->post('crt_mem',true)
				);

				if($this->master_model->updateRecord('case_master',$insrt_arr,array('id'=>$case_id)))

				{
					$updt_crt=array(
						'crt_id'=>$crt_id
					);

					//update crt id in case report
					$this->master_model->updateRecord('case_report_master',$updt_crt,array('case_id'=>$case_id));
					
					
					$this->session->set_flashdata('success','Case successfully updated.');
					redirect(base_url().'cc/managecase/update/'.$case_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding scenario');

					redirect('cc/managecase');

				}

			}

		}



		/* fetch CR list to assing case, CRT will be of current selected organization */

		$whr_arr=array('crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1');

		$whr_str="(crt.org_id=".$this->session->userdata('cc_selected_orgnaization')." OR crt.org_id=0) AND (login.user_level='1')";

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$this->db->where($whr_str,NULL,FALSE);

		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);


		$caseinfo = $this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'id'=>$case_id));


		$data=array('middle_content'=>'update-case','crt_list'=>$crt_list,'caseinfo'=>$caseinfo);

		$this->load->view('cc/cc-view',$data);

	}


	//delete case
	public function delete()

	{
		$case_id=$this->uri->segment(4);
		

		$data['success']=$data['error']="";


		if($this->master_model->deleteRecord('case_master','id',$case_id))

		{	
			if($this->master_model->deleteRecord('case_report_master','case_id',$case_id)){
				
				$this->master_model->deleteRecord('case_report_master','case_id',$case_id);
			}
			
			if ($this->master_model->deleteRecord('case_document_report','case_id',$case_id) ){
			
				$this->master_model->deleteRecord('case_document_report','case_id',$case_id);
			}

			$this->master_model->deleteRecord('case_master','id',$case_id);

			$this->session->set_flashdata('success',' Case successfully deleted.');

			redirect(base_url()."cc/managecase");
		}

		else

		{

			$this->session->set_flashdata('error',' Error while deleting task.');

			redirect(base_url()."cc/scenario/managetask/");

		}
		
		
	}
	
	//search_crt case
	public function search_crt()

	{
		$location = $_POST['location'];
		$city_lat = $_POST['city_lat'];
		$city_lng = $_POST['city_lng'];
		

		
		if ($location == '' && $city_lat =='' && $city_lng ==''){
			$data['text'] = '<p class="text-center text-muted" style="margin-top: 50px">No search key added.</p>';
			echo json_encode($data);
		}
		
		else if ($location == '' || $city_lat =='' || $city_lng ==''){
			$data['text'] = '<p class="text-center text-muted" style="margin-top: 50px">Please select location from dropdown.</p>';
			echo json_encode($data);
		}
		else{

			
			$cc_id = $this->session->userdata('logged_cc_login_id');
			$org_id = $this->session->userdata('cc_selected_orgnaization');
			
			$crt_list = $this->common_model->get_org_crts($cc_id,$org_id);
			
			//var_dump($crt_list);
			$data['text'] = '';
			$i = 0;
			

			
			if(count($crt_list) > 0){
				
					
	
				$search['location_lat'] = $city_lat;
				$search['location_lng'] = $city_lng;
				
				$the_closest = $this->common_model->get_closest($search, $crt_list);
				
				//select org
				$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );
				
	//			$data['text'] = $the_closest;
	//			echo json_encode($data);
	//			return false;
	
				foreach($the_closest as $closest){
					
					foreach($crt_list as $r=>$value){
						$availability_status = $this->common_model->availability($value['login_id']);
						
						if($availability_status == 'Available'){
							$availability_class = 'label-success';
						}
						else{
							$availability_class = 'label-danger';
						}


						if($value['login_id'] == $closest){ //get verified user and matching location
	
							$data['text'] .= '<p style="border-bottom: 1px solid #ccc; margin-bottom: 0px; padding-bottom: 5px;"><label class="radio-inline" for="filter_crt_mem'.$i.'"> <input type="radio" name="crt_mem" id="filter_crt_mem'.$i.'" value="'.$value['login_id'].'"> <b>'.$value['full_name'];
							
							if($i == 0){
								//$data['text'] .= ' <span class="label label-success">closest</span>';
							}
							//.'<br>Latitude: '.$value['location_lat'].'<br>Longitude: '.$value['location_lng']
							
							$data['text'] .= '</b><span class="text-muted small"><br>Location: '.$value['location'];
							
							
								
							if (strpos($the_active_module[0]['active_module'], 'b') !== false){
					
								$data['text'] .= '<br>Status: <span class="label '.$availability_class.'">'.$availability_status.'</span></span></label>';
								
								$data['text'] .= '<br><a href="#" class="btn btn-default btn-xs view_calendar_btn" data-id="'.$value['login_id'].'" data-name="'.$value['full_name'].'" style="margin-top: 5px; margin-left: 20px;"><i class="fa fa-calendar"></i> View Calendar</a>';
								
							}
							
							$data['text'] .= '</p>';
						
							$i++;
						}
					}
			
			
				}
				
				
				
			
/*				foreach($crt_list as $r=>$value){
					
					if($value['user_status'] == '1'){ //get verified user and matching location

						$data['text'] .= '<p style="border-bottom: 1px solid #ccc;"><label class="radio-inline" for="filter_crt_mem'.$i.'"> <input type="radio" name="crt_mem" id="filter_crt_mem'.$i.'" value="'.$value['login_id'].'"> <b>'.$value['full_name'].'</b><span class="text-muted small"><br>Location: '.$value['location'].'<br>Latitude: '.$value['location_lat'].'<br>Longitude: '.$value['location_lng'].'</span></label> </p>';
					
						$i++;
					}
					
					//if ($value['crt_city'] == $location || $value['crt_state'] == $location || $value['crt_availability'] == $availability || $value['crt_experience'] == $experience ){
						
						//$data['text'] .= '<p style="border-bottom: 1px solid #ccc;"><label class="radio-inline" for="filter_crt_mem'.$i.'"> <input type="radio" name="crt_mem" id="filter_crt_mem'.$i.'" value="'.$value['login_id'].'"> <b>'.$this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']).'</b><span class="text-muted small"><br>City: '.$value['location'].'<br>Latitude: '.$value['location_lat'].'<br>Longitude: '.$value['location_lng'].'</span></label> </p>';

					//}
		
				}*/
			
			}
			

			
			if ($i == 0){
				$data['text'] .= '<p class="text-center text-muted" style="margin-top: 50px">No case officer found.</p>';
			}
			
			echo json_encode($data);
		}
		
	}
	

	public function get_events(){
		$login_id = $this->uri->segment(4);
		$scheds = $this->master_model->getRecords('schedules', array('login_id'=>$login_id));

		$available = array();
		
		if(count($scheds) > 0){
			foreach($scheds as $r=>$value){
				$sched = array(
					'id'=>$value['id'],
					'the_id'=>$value['id'],
					'title'=>$value['title'],
					'start'=>$value['start']
				);
				
				if($value['end'] != ''){
					$sched['end'] = $value['end'];
				}
				
				$available[] = $sched;
			}
		}

		//$output_arrays = json_decode($arr, true);
		
		
		echo json_encode($available);
	}	
	
}

?>
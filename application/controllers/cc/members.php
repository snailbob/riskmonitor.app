<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Members extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

		

	}

	

	//------------------------------------add crt

	public function add()

	{

		if(isset($_POST['btn_add_member']))

		{

			$this->form_validation->set_rules('cct_firstname','First Name','required');

			$this->form_validation->set_rules('cct_lastname','Last Name','required');

			$this->form_validation->set_rules('crt_email','Email','required|valid_email|is_unique[employee_master.email_id]');


			$this->form_validation->set_message('required', 'Mandatory field');
				if($this->form_validation->run())

				{
					

					$cct_firstname=$this->input->post('cct_firstname');

					$cct_lastname=$this->input->post('cct_lastname');

					$crt_email=$this->input->post('crt_email');
					
					$dups_mem = $this->master_model->getRecords('employee_master');
					foreach ($dups_mem as $dd=>$dm){
						if ($this->master_model->encryptIt($crt_email) == $dm['email_id']){
							
							$this->session->set_flashdata('error',' Member\'s email already exist in the database.');
	
							redirect(base_url().'cc/members/add');
						}
						
					}

					$onetime_key=md5(microtime());



					$input_array=array(
						'first_name'=>$this->master_model->encryptIt($cct_firstname),
						'last_name'=>$this->master_model->encryptIt($cct_lastname),
						'email_id'=>$this->master_model->encryptIt($crt_email),
						'onetime_key'=>$onetime_key,
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);

					if($this->master_model->insertRecord('employee_master',$input_array))

					{

						$whr=array('id'=>'1');

						$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

						

						$info_arr=array('from'=>$adminemail[0]['recovery_email'],

									'to'=>$crt_email,

									'subject'=>'Welcome to CrisisFlo',

									'view'=>'cmember_registration_on_crisesflo');

					

					     $other_info=array(

									  'first_name'=>$cct_firstname,

									  'last_name'=>$cct_lastname,

									  'login_id'=>$onetime_key);

					

					    $this->email_sending->sendmail($info_arr,$other_info);

						$this->session->set_flashdata('success',' Member successfully added.');

						redirect(base_url().'cc/members/manage/');

					}

				}

		}
		


		$data=array('page_title'=>'Add Crisis Response Team Member','error'=>'','middle_content'=>'add-members','success'=>'');

		$this->load->view('cc/cc-view',$data);

	}

	

	

	//-----------------------------------------manage crt

	public function manage()

	{

		$whr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'));

		$all_mem=$this->master_model->getRecords('employee_master',$whr);



		$data=array('page_title'=>"Manage Members",'middle_content'=>'manage-members','success'=>'',

		'error'=>'','all_mem'=>$all_mem);

		$this->load->view('cc/cc-view',$data);

	}

	//-----------------------------------------Delete crt

	public function delete()

	{

		$data['success']=$data['error']="";

		$login_id=$this->uri->segment(4);

		

		if($this->master_model->deleteRecord('employee_master','login_id',$login_id))

		{	


			$this->session->set_flashdata('success',' Record successfully deleted.');

			redirect(base_url()."cc/members/manage/");

		}

		else

		{

			$this->session->set_flashdata('error',' Error while deleting records.');

			redirect(base_url()."cc/crisisteam/managecrt/");

		}

		

	}

	

	//------------------------------------Update crt

	public function update()

	{

		$login_id=$this->uri->segment(4);

		if(isset($_POST['btn_add_crt_member']))

		{

			$this->form_validation->set_rules('cct_firstname','First Name','required');

			$this->form_validation->set_rules('cct_lastname','Last Name','required');

			$this->form_validation->set_rules('crt_email','Email','required');

			$this->form_validation->set_rules('crt_no','Contact Number','required');

			$this->form_validation->set_message('required', 'Mandatory field');

				if($this->form_validation->run())

				{

					$cct_firstname=$this->input->post('cct_firstname');

					$cct_lastname=$this->input->post('cct_lastname');

					$countrycode = $this->input->post('countrycode');

					$crt_digits = $this->input->post('crt_no');

					$crt_no= $countrycode.$crt_digits;


					$input_array=array(

						'first_name'=>$this->master_model->encryptIt($cct_firstname),
						'last_name'=>$this->master_model->encryptIt($cct_lastname),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'digits'=>$this->master_model->encryptIt($crt_digits),
						'mobile'=>$this->master_model->encryptIt($crt_no),
						'cc_id'=>$this->session->userdata('logged_cc_login_id')
					);

					if($this->master_model->updateRecord('employee_master',$input_array,array('login_id'=>$login_id)))

					{

						$this->session->set_flashdata('success',' Member successfully updated');

						redirect(base_url().'cc/members/manage/');

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating Member.');

						redirect(base_url().'cc/crisisteam/managecrt/');

					}

				}

		}
		$countriescode=$this->master_model->getRecords('country_t');
		$crt_record=$this->master_model->getRecords('employee_master',array('login_id'=>$login_id));
		$all_group=$this->master_model->getRecords('user_group',array('org_id'=>$this->session->userdata('cc_selected_orgnaization')));

		$data=array('page_title'=>'Update Crisis Team Member','error'=>'','middle_content'=>'update-members','success'=>'','crt_record'=>$crt_record,'countriescode'=>$countriescode,'all_group'=>$all_group);

		$this->load->view('cc/cc-view',$data);

	}

	

	public function details()

	{

		$login_id=$this->uri->segment(4);

		

		$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data=array('page_title'=>'Crisis Team Member details','error'=>'','middle_content'=>'details-crt',

		'success'=>'','crt_record'=>$crt_record);

		

		$this->load->view('cc/cc-view',$data);

	}

}

?>
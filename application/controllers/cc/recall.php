<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recall extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('download');
	}
	
	
	
	public function close_all_recall(){
		//$this->master_model->updateRecord('cf_recall', array('closed'=>'1'), array('cc_id'=>'1'));
		//$this->master_model->updateRecord('cf_crisis_response_team', array('tasks_review_date'=>'0000-00-00 00:00:00'), array('login_id'=>'1'));
		
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$crts = $this->common_model->get_org_crts($cc_id, $org_id);
		
		print_r($crts);
		
	}

	public function manage(){ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
	 
	 
		// load library
		$this->load->library('nexmo');
		// set response format: xml or json, default json
		$this->nexmo->set_format('json');
	 
	 
	 	$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		//recall pack
		$continuity_id = 18;

	//	$pack_id = $continuity_id;
		$r_pack_id = $continuity_id;

		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
		
		
		if(count($mypack)> 0){
			$pack_id = $mypack[0]['input_pack_id'];
			
			//if org has input_pack
			if($pack_id != 0){
				$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('input_pack_id'=>$mypack[0]['input_pack_id']));
				
				if (count($myrecallpack) > 0){
					$r_pack_id = $myrecallpack[0]['id'];
				}

			}
			else{
				//check if has reseller assigned
				if($mypack[0]['reseller_id'] != '0'){
					$reseller_pack = $this->master_model->getRecords('cf_recall_packs',array('consultant_id'=>$mypack[0]['reseller_id']));
					
					if(count($reseller_pack) > 0){
						$r_pack_id = $reseller_pack[0]['id'];
					}
				}

			}
			
		}


		
	  	$questions = $this->master_model->getRecords('cf_recall_guidance',array('recall_pack_id'=>$r_pack_id,'step_no'=>'2','disabled'=>'0'),'',array('category'=>'ASC','arrangement'=>'DESC'));
	  	$the_other_questions = $this->master_model->getRecords('cf_recall_guidance',array('recall_pack_id'=>$r_pack_id,'step_no !='=>'2','disabled'=>'0'),'',array('category'=>'ASC','arrangement'=>'DESC'));
		
	 	$eurtousdval = $this->common_model->get_nexmo_balance('exchange');
		
		
/*		echo $get_nexmo_balance;
		return false;
        // load library
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');


		//new json api source for currency
		$url = "http://currency-api.appspot.com/api/EUR/USD.json?key=0d58e6fb444400a40e03e3f04bc80cb7647b0912";
		
		$result = file_get_contents($url);
		$exrate = json_decode($result);
		
		
		if (is_object($exrate)){//check if it return json
			if ($exrate->success){
				$eurtousdval = $exrate->rate;
			}
			else{
				$eurtousdval = 1.3628; //static value for euro to usd
			}
		}
		else{
			$eurtousdval = 1.3628; //static value for euro to usd
		}
*/	

	
		//select org module
		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
		$the_active_module = array('0'=>array('active_module'=>$theorg[0]['active_module']));



		if(isset($_POST['btn_notify_incident'])){
			
			$this->form_validation->set_rules('radiovalit', 'Respone Type', 'required');
			$this->form_validation->set_rules('inci_no', 'Incident number', 'required');
			$this->form_validation->set_rules('inci_date', 'Date of incident', 'required');
			$this->form_validation->set_rules('inci_name', 'Incident name', 'required');
			
			
	  		if(count($questions) > 0){
				$the_q = 1;
				foreach($questions as $q=>$question){
					//validation
					//$this->form_validation->set_rules('step1q'.$the_q, $question['question'], 'required');
					
					$the_q++;	
				}
			}

			$this->form_validation->set_rules('inform', 'Notify via Email', 'required');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run()){

				$radiovalit = $this->input->post('radiovalit');
				$inci_no = $this->input->post('inci_no');
				$inci_base_no = $this->input->post('inci_base_no');
				$description = $this->input->post('description');
				$inci_date = $this->input->post('inci_date'); //.' 00:00:00';
				$inci_name = $this->input->post('inci_name');
			//	$simulation_modules = $this->input->post('simulation_modules');
				
				//set value variables
				if(count($questions) > 0){
					$the_q = 1;
					foreach($questions as $q=>$question){
						//validation
						${"step1q" . $the_q} = $this->input->post('step1q'.$the_q);
						$the_q++;	
					}
				}

				$aff_loc = $this->input->post('p_scnt',true);
				$aff_loc_lat = $this->input->post('lat',true);
				$aff_loc_lng = $this->input->post('lng',true);
				
				//add to one field the affected location
				$affected_loc = array();
				array_push($affected_loc, $aff_loc);
				array_push($affected_loc, $aff_loc_lat);
				array_push($affected_loc, $aff_loc_lng);

				$inform = $this->input->post('inform'); 
				//$informsms=$this->input->post('informsms'); 


				//timezone datetime
				$nowtime = $this->common_model->userdatetime();


				//insert scenario log 
				$recall_arr = array(
					'cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'incident_no'=>$inci_no,
					'cc_incident_id'=>$inci_base_no,
					'description'=>$description,
					'initiation_type'=>$radiovalit,
					'incident_status'=>'0',
					'initiation_date'=>$nowtime,
					'input_pack_id'=>$r_pack_id,
					'incident_date'=>$inci_date,
					'incident_name'=>$inci_name,
				//	'simulation_id'=>$simulation_modules,
					'affected_location'=>json_encode($affected_loc),
				);


				$incident_id = $this->master_model->insertRecord('cf_recall',$recall_arr,true);
				//.insert recall step1 info
				
				$log_incident_id = $incident_id.'r';





				$count_var = 1;
				foreach($questions as $rc=>$rpack){
					
					$approvers = $this->master_model->getRecords('cf_recall_approvers', array('standbytask_id'=>$rpack['id']));
					$the_approvers = '';
					if(count($approvers) > 0){
						$the_approvers = $approvers[0]['approvers'];
					}

		
					$the_questions = array(
						'recall_id'=>$incident_id,
						'recall_pack_id'=>$continuity_id,
						'question'=>$rpack['question'],
						'answer'=>${'step1q'.$count_var},
						'task_guide'=>$rpack['guidance'],
						'approvers'=>$the_approvers,
						'category_id'=>$rpack['category'],
						'step_no'=>$rpack['step_no'],
						'status'=>'1'
					);
					$this->master_model->insertRecord('cf_recall_steps',$the_questions);
					$count_var++;
				}//insert step1 questions
				
				if(count($the_other_questions) > 0){

					foreach($the_other_questions as $toq=>$oq){
						
						$approvers = $this->master_model->getRecords('cf_recall_approvers', array('standbytask_id'=>$oq['id']));
						$the_approvers = '';
						if(count($approvers) > 0){
							$the_approvers = $approvers[0]['approvers'];
						}
						
						
						
						$the_questions = array(
							'recall_id'=>$incident_id,
							'recall_pack_id'=>$continuity_id,
							'question'=>$oq['question'],
							'task_guide'=>$oq['guidance'],
							'approvers'=>$the_approvers,
							'category_id'=>$oq['category'],
							'step_no'=>$oq['step_no'],
							'status'=>'0'
						);
						$this->master_model->insertRecord('cf_recall_steps',$the_questions);
					}
				}//insert other questions

		
		


				// Admin email
				$whr = array(
					'id'=>'1'
				);
				
				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');
				
				// Sending mail to CC only
				$info_arr_cc=array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->session->userdata('logged_cc_email_id'),
					'subject'=>'Recall Incident has been initiated',
					'view'=>'incident-occurence-mail-to-cc'
				);
				
				$other_info_cc = array(
					'name'=>$this->session->userdata('logged_display_name')
				);
				
				$this->email_sending->sendmail($info_arr_cc,$other_info_cc);
				// Mail sending  to CC completed  
				
				
				
				//get sms message
				$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'notify-incident'));
				
				$from = 'CrisisFlo';				
				$message = array(
					'text' => $sms_message[0]['text']//'A notify incident has been submitted. Please log into your user panel.'
				);
				
				
				
				
         	//Recall module activated
            if (strpos($the_active_module[0]['active_module'], '7') !== false){ 
				//sms sending
				//if($informsms=='crtonly'){
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));

					if(count($remind_result) > 0 ){
						foreach($remind_result as $rr=>$rem){
							
							$to = $this->master_model->decryptIt($rem['crt_mobile']);
							$response = $this->nexmo->send_message($from, $to, $message);

							foreach ($response->messages as $messageinfo) {
								$recipient = $messageinfo->{'to'};
								$status = $messageinfo->{'status'};
							}

							
							$det_contact = $this->master_model->decryptIt($rem['crt_first_name']);
							$det_contact .= ' ';
							$det_contact .= $this->master_model->decryptIt($rem['crt_last_name']);
							$det_contact .= ' - ';
							$det_contact .= $recipient;

							if ($status == '0'){

								$date_sent = date("Y-m-d H:i:s");

								foreach ($response->messages as $messageinfo) {
									$message_id = $messageinfo->{'message-id'};
									$messprice = $messageinfo->{'message-price'};
									$message_price = $eurtousdval * $messprice;
									$network = $messageinfo->{'network'};
								}
								
								//store sms records
								$sms_arr = array(
									'cc_id'=>$this->session->userdata('logged_cc_login_id'),
									'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
									'message_id'=>$message_id,
									'network'=>$network,
									'recipient'=>$det_contact,
									'price'=>$message_price,
									'status'=>$status,
									'date_sent'=>$date_sent,
									'log_scenario_id'=>$log_incident_id
								);
								
								$this->master_model->insertRecord('sms',$sms_arr);
								
								//update sms credit
//								$sms_creditt = $this->master_model->getRecords('organization_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'organization_id'=>$this->session->userdata('cc_selected_orgnaization')),'sms_credit');
//								
//								$ded_creditt = $sms_creditt[0]['sms_credit'] - $message_price;
//								
//								$sms_org_array = array(
//									'sms_credit'=>$ded_creditt
//								);
//								
//								$whr_sms_org = array(
//									'cc_id'=>$this->session->userdata('logged_cc_login_id'),
//									'organization_id'=>$this->session->userdata('cc_selected_orgnaization')
//								);
//								
//								$this->master_model->updateRecord('organization_master',$sms_org_array, $whr_sms_org);

							}
							else{
								
								foreach ($response->messages as $messageinfo) {
									$err_txt = $messageinfo->{'error-text'};
								}

								$err_stat = $status;
								$err_stat .= ' - ';
								$err_stat .= $err_txt;
								
								$sms_err_arr = array(
									'cc_id'=>$this->session->userdata('logged_cc_login_id'),
									'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
									'recipient'=>$det_contact,
									'price'=>'0',
									'status'=>$err_stat,
									'log_scenario_id'=>$log_incident_id
								);
								
								$this->master_model->insertRecord('sms', $sms_err_arr);
							}
								
						}
					}//end of sms notification
					
				}//end of strpos 7 = sms noti
					
					
					
				if($inform == 'crtonly'){
					
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$whr_remind_res = array(
						'cf_login_master.user_status'=>'1',
						'cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
					$remind_result = $this->master_model->getRecords('cf_crisis_response_team', $whr_remind_res);
					
					$whr_cpack = array(
						'c_pack'=>'yes',
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
					$c_pack = $this->master_model->getRecords('cf_file_upload', $whr_cpack);
					

					
					if(count($remind_result) > 0 ){
						
						foreach($remind_result as $rr=>$rem){
							
							$the_cpack = array();
							if(count($c_pack) > 0){
								foreach($c_pack as $r=>$value){
									if($value['group_id'] =='0' || $value['group_id'] == $rem['group_id']){
										$the_cpack[] = 'uploads/crisis-document/'.$value['file_upload_name'];
									}
								}
							}
							
							$info_arr = array(
								'from'=>$adminemail[0]['contact_email'],
								'to'=>$this->master_model->decryptIt($rem['crt_email']),
								'subject'=>'Recall Incident has been initiated',
								'view'=>'incident-occurence-mail-to-crt'
							);
							
							$other_info = array(
								'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
								'cc_name'=>$this->session->userdata('logged_cc_login_id'),
								'incident_name'=>$inci_name,
								'group_id'=>$rem['group_id'],
								'c_pack'=>$the_cpack,
								'attachments'=>$the_cpack
							);
							  
							$this->email_sending->sendmail($info_arr,$other_info);
						}
					}
					
					//$this->session->set_flashdata('success',' New incident successfully submitted.'); // <a href="#"  data-toggle="modal" data-target="#mySmsReportLog">View SMS Report</a>
					
					//redirect('cc/dashboard/index/'.$log_incident_id.'/r/');
					echo 'yeah';
					return false;
				}
				
				else if($inform=='crtstk') {
					//--------------------------------------for crt
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					
					$whr_remind_res = array(
						'cf_login_master.user_status'=>'1',
						'cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
					$remind_result = $this->master_model->getRecords('cf_crisis_response_team', $whr_remind_res);

					$whr_cpack = array(
						'c_pack'=>'yes',
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
					$c_pack = $this->master_model->getRecords('cf_file_upload', $whr_cpack);

					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{

							$info_arr = array(
								'from'=>$adminemail[0]['contact_email'],
								'to'=>$this->master_model->decryptIt($rem['crt_email']),
								'subject'=>'Recall Incident has been initiated',
								'view'=>'incident-occurence-mail-to-crt'
							);
							
							$other_info = array(
							 	'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
								'cc_name'=>$this->session->userdata('logged_cc_login_id'),
								'incident_name'=>$inci_name,
								'group_id'=>$rem['group_id'],
								'c_pack'=>$c_pack
							);
							$this->email_sending->sendmail($info_arr,$other_info);
						}
					}
					
					
					//--------------------------------------for stakeholder
					$org_id = $this->session->userdata('cc_selected_orgnaization');
					$whr = array('organization_id'=>$org_id);
			
					$org_name = $this->master_model->getRecords('organization_master',$whr,'*');
					$stks = unserialize($org_name[0]['stakeholders']);
					foreach($stks as $value) {
						$thestk = $this->master_model->getRecords('cf_stakeholder', array('login_id'=>$value));
						
						if(count($thestk) > 0){

							if($this->common_model->stkstatus($value) == 1){
								
								
								$info_arr = array(
									'from'=>$adminemail[0]['contact_email'],
									'to'=>$this->master_model->decryptIt($thestk[0]['stk_email_address']),
									'subject'=>'Recall Incident has been initiated',
									'view'=>'incident-occurence-mail-to-stk'
								);
								
								$other_info=array( 
									'name'=>$this->master_model->decryptIt($thestk[0]['stk_first_name']).' '.$this->master_model->decryptIt($thestk[0]['stk_last_name']),
									'cc_name'=>$this->session->userdata('logged_cc_login_id'),
									'incident_name'=>$inci_name,
									'overview'=>$overview,
									'org_name'=>$this->session->userdata('cc_selected_orgnaization_name')
								);
								
								$this->email_sending->sendmail($info_arr,$other_info);
								
								
							}//end of check if stk completed

						}//end check if stk exist
					}//end loop for stks
					
					

					//$this->session->set_flashdata('success',' New incident successfully submitted.');
					
					//redirect('cc/dashboard/index/'.$log_incident_id.'/r/');	
					echo 'yeah';
					return false;
				}
				
			}
		}//end if form submitted


		$whr_xcase = array(
			'cc_id'=>$this->session->userdata('logged_cc_login_id'),
			'org_id'=>$this->session->userdata('cc_selected_orgnaization')
		);
		
		$excase_id = $this->master_model->getRecords('cf_recall', $whr_xcase, 'cc_incident_id');
		if(count($excase_id) > 0){

			$numItems = count($excase_id);
			$i = 0;
			foreach($excase_id as $key=>$value) {
			  if(++$i === $numItems) {
				  
				$cc_incident_id = $value['cc_incident_id'] + 1;

			  }
			}  

				
		}
		
		else{
			$cc_incident_id = 1;
		}		
		
		
		//get org sms balance
		$response=$this->master_model->getRecords('organization_master',array(
		'organization_id'=>$this->session->userdata('cc_selected_orgnaization'),
		'cc_id'=>$this->session->userdata('logged_cc_login_id')));
		
		
		$data=array(
			'page_title'=>'Initiate Response',
			'error'=>'',
			'middle_content'=>'recall-view-v2',
			'success'=>'',
			'response'=>$response,
			'cc_incident_id'=>$cc_incident_id,
			'questions'=>$questions
		);
		
		$this->load->view('cc/cc-view',$data);
	}
		
	
	
	
/***************************************************put_all_task***********************************/
	public function put_all_task($incident_id)	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		//insert step default tasks
		$gen_step = $this->master_model->getRecords('cf_recall_guidance',array('disabled'=>'0','recall_pack_id'=>'0'),'*',array('step_no'=>'ASC','category'=>'ASC','arrangement'=>'ASC'));
		
		foreach($gen_step as $s2=>$step_tsk){
			$the_questions = array(
				'recall_id'=>$incident_id,
				'recall_pack_id'=>'0',
				'question'=>$step_tsk['question'],
				'task_guide'=>$step_tsk['guidance'],
				'category_id'=>$step_tsk['category'],
				'step_no'=>$step_tsk['step_no'],
				'status'=>'0'
			);
			$this->master_model->insertRecord('cf_recall_steps',$the_questions);
		}

		return true;

	}
	
	
	
	public function fetch_stand_by_message()
	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$scenarioid=$this->uri->segment(4);
		$stby_msg=$this->master_model->getRecords('cf_standby_messages',array('scenario_id'=>$scenarioid));
		$stn_messsages=array();
		
		foreach($stby_msg as $msg)
		{
			$stn_messsages[$msg['stn_msg_id']]=$msg['standby_message'];
		}
		echo json_encode($stn_messsages);
	}
	
	
	public function mark_task_completed()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$quest_id = $_POST['question_id'];
		
		$the_new_status = '1';
		
		//get validation message from admin
		$action_message_id = '41';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps',array('id'=>$quest_id));
			
			$rc = $this->master_model->getRecords('cf_recall',array('id'=>$task[0]['recall_id']));
		}
		else{
			$task = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$quest_id));
			
			$rc = $this->master_model->getRecords('cf_continuity',array('id'=>$task[0]['recall_id']));
		}
			
		//check if the task has been marked completed
		if($task[0]['status'] == '1'){
			$data['result'] = 'was_update';
			echo json_encode($data);
			return false;
		}
		
		
		
		//get the date completed
		$initiation_date = new DateTime($rc[0]['initiation_date']);


		$nowtime = $this->common_model->userdatetime();
		
		
		$task_completed = new DateTime($nowtime);
		$difference = $initiation_date->diff($task_completed);

		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i; //.' '.$difference->s.' ';
		
		
		$task_tot = explode(' ',$interval);
		$unit_time = array(
			'Y : ',
			'Mo : ',
			'D : ',
			'H : ',
			'M' // : ',
		//	'S'
		);
		$u = 0; 
		$time_lapse = '';
		foreach($task_tot as $tot){
			if ($tot == 0 ){
			}
			else if ($tot == 1 ){
				$time_lapse .= $tot.' '.$unit_time[$u].' ';
			}
			else{
				$time_lapse .= $tot.' '.$unit_time[$u].' ';
			}
			$u++;
		}
					

		
		
		$arr = array(
			'status'=>$the_new_status,
			'date_completed'=>$nowtime,
			'time_lapse'=>$time_lapse
			
		);
		
		
		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',$arr,array('id'=>$quest_id));
			
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps',$arr,array('id'=>$quest_id));
		}
		
		$data['time_completed'] = 'Time to Complete:<br><span class="text-muted">'.$time_lapse.'</span>';
		$data['result'] = 'success';
		

		
		//check if there's approvers
		$the_approvers = unserialize($task[0]['approvers']);
		$the_approvers_status = unserialize($task[0]['approvers_status']);
		$approvers_count = 0;
		$approvers_status = 0;
		
		$new_task_owner = '';
		
		if(is_array($the_approvers)){
			$approvers_count = count($the_approvers);
		}
		else{
			$the_approvers = array();
		}
		
		if(is_array($the_approvers_status)){
			$approvers_status = count($the_approvers_status);
		}
		else{
			$the_approvers_status = array();
		}
			
			
		$unapproved_count = count($the_approvers);
		
		if(count($the_approvers) > 0){
			foreach($the_approvers as $tapp){
				if(in_array($tapp, $the_approvers_status)){
					$unapproved_count--;
				}
			}
		}


		if($unapproved_count > 0) { //$approvers_count != $approvers_status){
			$status_text = 'Awaiting Approval';
			$status_class = 'text-warning';
			
			//array_diff — Computes the difference of arrays
			$result = array_diff($the_approvers, $the_approvers_status);
			
			if(count($result) > 0){
				$new_arr = array();
				foreach($result as $rs){
					$new_arr[] = $rs;
				}
				$new_task_owner = $this->common_model->getname($new_arr[0]);
			}
		}
		else if ($the_new_status == '0'){
			$status_text = 'In Progress';
			$status_class = 'text-danger';
		}
		
		else{
			$status_text = 'Completed';
			$status_class = 'text-success';
		}

		$data['new_task_owner'] = $new_task_owner;
		$data['status_text'] = $status_text;
		$data['status_class'] = $status_class;

				
		echo json_encode($data);
		//echo 'Time to Complete:<br><span class="text-muted">'.$time_lapse.'</span>';
		//echo 'success';
	
	}
	
	
	
	public function mark_task_incomplete()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
		$active_mod  = substr($this->session->userdata('org_module'),0,1);


		$quest_id = $_POST['question_id'];
		$the_new_status = '0';

		if($active_mod == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps',array('id'=>$quest_id));
		}
		else{
			$task = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$quest_id));
		}
		

		//check if the task has been marked completed
		if($task[0]['status'] == '0'){
			$data['result'] = 'was_update';
			echo json_encode($data);
			return false;
		}
		


		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',array('status'=>$the_new_status),array('id'=>$quest_id));
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps',array('status'=>$the_new_status),array('id'=>$quest_id));
		}
		
		
		$data['result'] = 'success';
		

		//check if there's approvers
		$the_approvers = unserialize($task[0]['approvers']);
		$the_approvers_status = unserialize($task[0]['approvers_status']);
		$approvers_count = 0;
		$approvers_status = 0;

		if(is_array($the_approvers)){
			$approvers_count = count($the_approvers);
		}
		else{
			$the_approvers = array();
		}
		
		if(is_array($the_approvers_status)){
			$approvers_status = count($the_approvers_status);
		}
		else{
			$the_approvers_status = array();
		}
		
		
		//count not yet approved	
		$unapproved_count = count($the_approvers);
		
		if(count($the_approvers) > 0){
			foreach($the_approvers as $tapp){
				if(in_array($tapp, $the_approvers_status)){
					$unapproved_count--;
				}
			}
		}
		
		
		
		if($unapproved_count > 0) { //$approvers_count != $approvers_status){
			$status_text = 'Awaiting Approval';
			$status_class = 'text-warning';

		}

		else if ($the_new_status == '0'){
			$status_text = 'In Progress';
			$status_class = 'text-danger';
		}
		
		
		else{
			$status_text = 'Completed';
			$status_class = 'text-success';
		}


		

		$data['new_task_owner'] = ( $task[0]['assigned'] != '' ? $this->common_model->getname($task[0]['assigned']) : '');


		$data['status_text'] = $status_text;
		$data['status_class'] = $status_class;

		echo json_encode($data);
		//$this->session->set_flashdata('success',' Task has been marked as incomplete');
		//echo 'success';
	
	}
	
	public function get_respond_to_edit()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$task_id = $_POST['task_id'];

		//active module is recall
		if($active_mod == '5'){
			$kyu = $this->master_model->getRecords('cf_recall_steps',array('id'=>$task_id));
		}
		else{
			$kyu = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$task_id));
		}
		echo $kyu[0]['answer'];
	
	}
	
	public function get_task_response()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$task_id = $_POST['task_id'];
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		//active module is recall
		if($active_mod == '5'){
			$kyu=$this->master_model->getRecords('cf_recall_steps',array('id'=>$task_id));
		}
		else{
			$kyu = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$task_id));
		}
			$recall_file = $this->master_model->getRecords('cf_recall_steps_file',array('message_id'=>$kyu[0]['id']));
		
        echo '<p class="lead">Task Response</p>';
		echo '<div class="well" style="color: #333; border-radius: 0;">'.nl2br($kyu[0]['answer']).'</div> ';	
		echo '<div class="well" style="color: #333; border-radius: 0;">'.$this->getcrtname($kyu[0]['assigned']).' - '.date_format(date_create($kyu[0]['date_updated']), 'jS F Y, g:ia').'</div>';
		
		if(count($recall_file) > 0){
			echo '<p>File Attached: ';
		}
		
		foreach($recall_file as $ff=>$rff){
			echo ' <a href="'.base_url().'uploads/recallsteps-uploads/'.$rff['file_upload_name'].'" target="_blank" id = "file_del'.$rff['file_upload_id'].'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.substr($rff['file_upload_name'], 13).'</a>';
			
			if($kyu[0]['status'] != '1'){
			
			echo ' <span onclick="$(\'#file_del'.$rff['file_upload_id'].'\').hide();$(this).hide();delete_the_recall_file(event.target.id);" style="cursor: default" id="delete_file'.$rff['file_upload_id'].'">&times;</span>';
			}

		}//foreach
		
		echo '</p>';
		
	}
	
	
	public function assign_the_crt()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$quest_id = $_POST['question_id'];
		$curr_assigned = $_POST['curr_assigned'];
		$message_receiver = $_POST['message_receiver'];
		
		$crt_name = $this->getcrtname($message_receiver);
		
		
		$nowtime = $this->common_model->userdatetime();

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		//check if assigned has changed
		if($active_mod == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps', array('id'=>$quest_id));
			
			if($task[0]['assigned'] != $curr_assigned){
				echo 'was_update';
				return false;
			}
		}
		else{
			$task = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$quest_id));
			
			if($task[0]['assigned'] != $curr_assigned){
				echo 'was_update';
				return false;
			}
		}
		
		//active module is recall
		if($active_mod == '5'){
			if($this->master_model->updateRecord('cf_recall_steps',array('assigned'=>$message_receiver,'date_assigned'=>$nowtime),array('id'=>$quest_id))){
				echo $crt_name;
			} else{
				echo 'error';
			}
		}
		else{
			if($this->master_model->updateRecord('cf_continuity_steps',array('assigned'=>$message_receiver,'date_assigned'=>$nowtime),array('id'=>$quest_id))){
				echo $crt_name;
			} else{
				echo 'error';
			}
		}
	
	}
	
	
	
	public function add_message_respond()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$quest_id = $_POST['question_id'];
		$txt_message = $_POST['txt_message'];


		$nowtime = $this->common_model->userdatetime();
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		if ($this->session->userdata('logged_cc_login_id') !=''){
			$the_auth = $this->session->userdata('logged_cc_login_id');
		
			//active module is recall
			if($active_mod == '5'){
				$this->master_model->updateRecord('cf_recall_steps',array('answer'=>$txt_message,'assigned'=>$the_auth,'date_updated'=>$nowtime,'date_assigned'=>$nowtime),array('id'=>$quest_id));
			}
			else{
				$this->master_model->updateRecord('cf_continuity_steps',array('answer'=>$txt_message,'assigned'=>$the_auth,'date_updated'=>$nowtime,'date_assigned'=>$nowtime),array('id'=>$quest_id));
			}
			
		}
		else{
			$the_auth = $this->session->userdata('logged_crt_login_id');
		
			//active module is recall
			if($active_mod == '5'){
				$this->master_model->updateRecord('cf_recall_steps',array('answer'=>$txt_message,'assigned'=>$the_auth,'date_updated'=>$nowtime),array('id'=>$quest_id));
			}
			else{
				$this->master_model->updateRecord('cf_continuity_steps',array('answer'=>$txt_message,'assigned'=>$the_auth,'date_updated'=>$nowtime),array('id'=>$quest_id));
			}
		}

		
		//$this->session->set_flashdata('success',' Task has been Completed');
		echo 'success';
	}
	
	
	//close incident
	public function close_recall()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		$recall_id = $this->uri->segment(4);
		
		//active module is recall
		if($active_mod == '5'){
			$recall = $this->master_model->getRecords('cf_recall',array('id'=>$recall_id));
		}
		else{
			$recall = $this->master_model->getRecords('cf_continuity',array('id'=>$recall_id));
		}

		$recall_initiated = new DateTime($recall[0]['initiation_date']);


		$nowtime = $this->common_model->userdatetime();
		
		
		$recall_closed = new DateTime($nowtime);
		$difference = $recall_initiated->diff($recall_closed);
		
		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';


		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '34';
		$action_message = $this->common_model->get_message($action_message_id);


		//active module is recall
		if($active_mod == '5'){
			//task time stamp
			$r_tasks = $this->master_model->getRecords('cf_recall_steps',array('recall_id'=>$recall[0]['id']),'date_completed',array('date_completed'=>'ASC'));
		}
		else{
			//task time stamp
			$r_tasks = $this->master_model->getRecords('cf_continuity_steps',array('recall_id'=>$recall[0]['id']),'date_completed',array('date_completed'=>'ASC'));
		
		}
		
		$last_task = count($r_tasks) - 1;
		$task_started = new DateTime($r_tasks[0]['date_completed']);
		$task_ended = new DateTime($r_tasks[$last_task]['date_completed']);

		$difference_task = $task_started->diff($task_ended);
		
		$interval_task = $difference_task->y.' '.$difference_task->m.' '.$difference_task->d.' '.$difference_task->h.' '.$difference_task->i.' '.$difference_task->s.' ';

		$arr = array(
			'closed'=>'1',
			'closed_date'=>$nowtime,
			'incident_total_time'=>$interval,
			'tasks_started'=>$r_tasks[0]['date_completed'],
			'tasks_ended'=>$r_tasks[$last_task]['date_completed'],
			'total_time_tasks'=>$interval_task
		);

		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall',$arr,array('id'=>$recall_id));
			$this->session->set_flashdata('success', $action_message['success']);
		}
		else{
			$this->master_model->updateRecord('cf_continuity',$arr,array('id'=>$recall_id));
			$this->session->set_flashdata('success', $action_message['success']);
		}
		
		redirect('cc/dashboard');
	}
	
	public function check_to_close()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		$id = $_POST['id'];
		
		//active module is recall
		if($active_mod == '5'){
			$tasks = $this->master_model->getRecords('cf_recall_steps',array('recall_id'=>$id));
		}
		else{
			$tasks = $this->master_model->getRecords('cf_continuity_steps',array('recall_id'=>$id));
		}
		
		foreach($tasks as $tk=>$tsk){
			if($tsk['status'] =='0'){
				echo 'error';
				return false;
			}
		}
		echo 'success';
	}
	
	
	
	public function save_new_rc_tsk()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		$tsk_name = $_POST['tsk_name'];
		$category_id = $_POST['category_id'];
		$tsk_guide = $_POST['tsk_guide'];
		$step_no = $_POST['step_no'];
		$recall_id = $_POST['recall_id'];
		
		$aarr = array(
			'recall_id'=>$recall_id,
			'step_no'=>$step_no,
			'question'=>$tsk_name,
			'category_id'=>$category_id,
			'task_guide'=>$tsk_guide
		);
		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '81';
		$action_message = $this->common_model->get_message($action_message_id);
		
		//active module is recall
		if($active_mod == '5'){
			if($this->master_model->insertRecord('cf_recall_steps',$aarr)){
				$this->session->set_flashdata('success', $action_message['success']); //' New Task successfully added.');
				echo 'success';
			}
			else{
				$this->session->set_flashdata('error', $action_message['error']);
				echo 'error';
			}
		}
		else{
			if($this->master_model->insertRecord('cf_continuity_steps',$aarr)){
				$this->session->set_flashdata('success', $action_message['success']);
				echo 'success';
			}
			else{
				$this->session->set_flashdata('success', $action_message['error']);
				echo 'error';
			}
		}

	
	}

	public function save_stp3_table()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$severity = $_POST['severity'];
		$presence = $_POST['presence'];
		$like_dist = $_POST['like_dist'];
		$distribution = $_POST['distribution'];
		$identification = $_POST['identification'];
		$hazard = $_POST['hazard'];
		$step_3_table_id = $_POST['step_3_table_id'];
		
		$step3table_answer = $severity. ' ' .$presence. ' ' .$like_dist. ' ' .$distribution. ' ' .$identification. ' ' .$hazard;


		$nowtime = $this->common_model->userdatetime();
		
		if ($this->session->userdata('logged_cc_login_id') !=''){
			$the_auth = $this->session->userdata('logged_cc_login_id');

		} else{
			$the_auth = $this->session->userdata('logged_crt_login_id');
		}
		
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',array('answer'=>$step3table_answer,'assigned'=>$the_auth,'date_updated'=>$nowtime,'date_assigned'=>$nowtime),array('id'=>$step_3_table_id));
		}else{
			$this->master_model->updateRecord('cf_continuity_steps',array('answer'=>$step3table_answer,'assigned'=>$the_auth,'date_updated'=>$nowtime,'date_assigned'=>$nowtime),array('id'=>$step_3_table_id));
		}
		

		
		echo 'success';
		
	}	
	

	public function download()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$this->load->helper('download');

		$doc_name=$this->uri->segment(4);

		$data = file_get_contents("uploads/recallsteps-uploads/".$doc_name);

		$name= $doc_name;

		force_download($name,$data);

	}
	
	
	public function upload()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

	
	
		$output_dir = "uploads/recallsteps-uploads/";
		
		
		if(isset($_FILES["myfile"]))
		{
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0)
			{
			  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
			}
			else
			{
				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
				$task_id = $_POST['the_question_id'];
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);
								
					
					$insrt_arr = array (
						'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
						'temp_id'=>$_POST['uniquetime'],
						'message_id'=>$task_id //$cc_case_id
					);					
					if ($file_id = $this->master_model->insertRecord('cf_recall_steps_file',$insrt_arr,true)){
						
						echo ' <a href="'.base_url().'uploads/recallsteps-uploads/'.$thefilename.'" target="_blank" id = "file_del'.$file_id.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$_FILES["myfile"]["name"].'</a> <span onclick="$(\'#file_del'.$file_id.'\').hide();$(this).hide();delete_the_recall_file(event.target.id);" style="cursor: default" id="delete_file'.$file_id.'">&times;</span>';
					
					}
					else{
					  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
					}
			}
		
		}		

	}
	
	public function delete_message_file()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$thenum = $_POST['thenum'];

	    $file = $this->master_model->getRecords('cf_recall_steps_file',array('file_upload_id'=>$thenum));
		
		$file_name = $file[0]['file_upload_name'];
		
		if($this->master_model->deleteRecord('cf_recall_steps_file','file_upload_id',$thenum))

		{

			@unlink('uploads/recallsteps-uploads/'.$file_name);

			//$this->session->set_flashdata('success',' Document successfully deleted.');
			echo 'success';

		}

	
	}	
	
	
	//fetch name for crt on trash view
	public function getcrtname($login_id)	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);
			
			return $crtname;
		}
		else{
			return 'Non-existing User';
		}
		
		
	}
	

	//blocker functions
	public function load_blocker_note(){
		$id = $this->uri->segment(4);
		$blocker = $this->master_model->getRecords('cf_recall_blockers',array('id'=>$id));
		
		if(count($blocker) > 0){
			echo nl2br($blocker[0]['note']);	
		}
		else{
			echo '';	
		}
		
	}
		
	public function send_incident_status()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$recall_id = $_POST['recall_id'];
		$module = $this->session->userdata('org_module');
		$org = $this->session->userdata('cc_selected_orgnaization');

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$recall = $this->master_model->getRecords('cf_recall',array('id'=>$recall_id));
		}
		else{
		$recall = $this->master_model->getRecords('cf_continuity',array('id'=>$recall_id));
		}

		//get validation message from admin
		$action_message_id = '38';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$crts = $this->master_model->getRecords('cf_crisis_response_team',array('org_id'=>$org));
		
		foreach($crts as $c=>$rts){
			$mymodyul = substr($module,0,1);
			if(strpos($rts['module'],$mymodyul) !== false){
				
		
				$whr=array('id'=>'1');
				$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

				$info_arr = array(
					'from'=>$adminemail[0]['recovery_email'],
					'to'=> $this->master_model->decryptIt($rts['crt_email']),
					'subject'=>'Welcome to CrisisFlo',
					'view'=>'send-incident-status-report'
				);

				$other_info = array(
					'first_name'=> $this->master_model->decryptIt($rts['crt_first_name']),
					'last_name'=> $this->master_model->decryptIt($rts['crt_last_name']),
					'recall_id'=>$recall_id,
					'incident_name'=>$recall[0]['incident_name']
				);

				//if($this->email_sending->sendmail($info_arr,$other_info)){
					$this->session->set_flashdata('success', $action_message['success']);
					echo 'success';
				//}

				//echo $this->master_model->decryptIt($rts['crt_email']).',';
			}
			
		}
	}


	public function select_step()	{ 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$step_no = $_POST['step'];
		$rc_id = $_POST['rc_id'];
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		//active module is recall
		if($active_mod == '5'){
			$recall = $this->master_model->getRecords('cf_recall',array('id'=>$rc_id));
			
			$mysteps = 0;
			
			if($recall[0]['input_pack_id'] == 18){ //recall module pack
				$mysteps = 18;
			}
			else{
				$recall_pack = $this->master_model->getRecords('cf_recall_packs',array('id'=>$recall[0]['input_pack_id']));

				if(count($recall_pack) > 0){
					$mysteps = $recall_pack[0]['id'];
				}
				
			}
		}
		else{
			$recall = $this->master_model->getRecords('cf_continuity',array('id'=>$rc_id));

			$continuity_id = 17;
			
			$mysteps = $continuity_id;
				

		}
		

		
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$step_no,'deleted'=>'0','recall_pack_id'=>$mysteps));


		echo '<label class="control-label" for="select_subcat">Sub-Category</label>
		<select id="select_subcat" name="select_subcat" class="form-control">
		  <option value="0">Select</option>';
		  
		foreach($category as $r=>$cat){ 
			
			echo '<option value="';
			echo $cat['id'];
			echo '">';
			echo $cat['category_name'].'</option>';
		}
		echo '</select>';
	}

	//blocker functions
	public function addnote_blocker()	{ 
//		$this->load->library('cc_check_session');
//		$this->cc_check_session->checksessionvalue();

		$note = $_POST['blocker_note'];
		$the_blocker_id = $_POST['the_blocker_id'];

		$arr = array(
			'note'=>$note
		);
		
		$whr = array(
			'id'=>$the_blocker_id
		);
		if($this->master_model->updateRecord('cf_recall_blockers',$arr,$whr)){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}

	public function mark_check_blocker() { 
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

		$status = $_POST['status'];
		$the_blocker_id = $_POST['id'];
	
	
		$nowtime = $this->common_model->userdatetime();
		

		$arr = array(
			'status'=>$status,
			'date_resolved'=>$nowtime
		);
		
		$whr = array(
			'id'=>$the_blocker_id
		);
		
		if($this->master_model->updateRecord('cf_recall_blockers',$arr,$whr)){
			if($status == '1'){
				echo 'Date Resolved: <span class="text-muted">'.date_format(date_create($nowtime),"m.d.Y, g:ia").'</span>';
			}else{
				echo '<span style="visibility: hidden">open</span>';
			}
		}
		else{
			echo 'error';
		}
	}
	
	public function places(){
		$cc_id = $this->session->userdata('team_cc');
		$recall_id = $_POST['id'];
		$active_mod  = substr($this->session->userdata('org_module'),0,1);


		//active module is recall
		if($active_mod == '5'){
			$recalls = $this->master_model->getRecords('cf_recall',array('id'=>$recall_id));
			$themod = 'recall_id';
		}
		else{
			$recalls = $this->master_model->getRecords('cf_continuity',array('id'=>$recall_id));
			$themod = 'continuity_id';
		}
		
		$new_markers = $this->master_model->getRecords('cf_recall_markers',array($themod=>$recall_id));
		$team = $this->master_model->getRecords('cf_crisis_response_team',array('cc_id'=>$cc_id));
		
		$places = array();
		

		if(count($recalls) > 0){
			foreach($recalls as $r=>$recall){
				
				$recall_loc = json_decode($recall['affected_location']);

				for($i = 0; $i < 6; $i++){
					if($recall_loc[0][$i] != ''){
						 array_push($places,
							array(
							  "name"=>$recall['incident_name'],
							  "geo_name"=>$recall_loc[0][$i],
							  "type"=>'aff',
							  "id"=>$recall['id'],
							  "geo"=>array(
								$recall_loc[1][$i],
								$recall_loc[2][$i]
							  )
							)
						 );


					}
				}
			}
		}	
		
			

		if(count($new_markers) > 0){
			foreach($new_markers as $nm=>$nmarker){
				 array_push($places,
					array(
					  "name"=>'Details',
					  "geo_name"=>$nmarker['comment'].'<br>'.$nmarker['location'],
					  "type"=>'nm',
					  "id"=>$nmarker['id'],
					  "geo"=>array(
						$nmarker['location_lat'],
						$nmarker['location_lng']
					  )
					)
				 );

			}
		}		
		
		
		
		
		if(count($team) > 0){
			foreach($team as $t=>$crt){
				if(strpos($crt['module'], $active_mod) !== false){
					 array_push($places,
						array(
						  "name"=>$this->master_model->decryptIt($crt['crt_first_name']).' '.$this->master_model->decryptIt($crt['crt_last_name']),
						  "geo_name"=>$crt['location'],
						  "type"=>'crts',
						  "id"=>$crt['login_id'],
						  "geo"=>array(
							$crt['location_lat'],
							$crt['location_lng']
						  )
						)
					 );
				}
			}
		}
		
		
		$data = array(
			'status'=>'OK',
			'places'=>$places
		); 
		
		
		echo json_encode($data);
		
		
	}
	
	
	public function update_my_loc(){
	
		$user_id = $this->session->userdata('my_id');
		$loc = $_POST['loc'];
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];

		$arr = array(
			'location'=>$loc,
			'location_lat'=>$lat,
			'location_lng'=>$lng
		);
		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '85';
		$action_message = $this->common_model->get_message($action_message_id);
		
		
		if($this->master_model->updateRecord('cf_crisis_response_team',$arr,array('login_id'=>$user_id))){
			$this->session->set_userdata('location',$loc);
			$this->session->set_flashdata('success', $action_message['success']);
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
		}
		
		
	}
	
	public function add_marker(){
		$user_id = $this->session->userdata('my_id');
		$loc = $_POST['loc'];
		$comment = $_POST['comment'];
		$recall_id = $_POST['recall_id'];
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		$nm_id = $_POST['nm_id'];
		
		$nowtime = $this->common_model->userdatetime();
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$themod = 'recall_id';
		}
		else{
			$themod = 'continuity_id';
		}
		
		
		$arr = array(
			'location'=>$loc,
			'location_lat'=>$lat,
			'location_lng'=>$lng,
			'cc_id'=>$user_id,
			$themod=>$recall_id,
			'comment'=>$comment,
		);
		
		if($nm_id != ''){
			$this->master_model->updateRecord('cf_recall_markers',$arr,array('id'=>$nm_id));
			echo 'success';
		}
		else{
	
			if($id = $this->master_model->insertRecord('cf_recall_markers',$arr,true)){
				echo 'success';
			}else{
				echo 'error';
			}
		
		}
	
	}
	
	public function get_new_marker(){
		
		$user_id = $this->session->userdata('my_id');
		$id = $_POST['id'];

		$arr = array(
			'id'=>$id
		);

		$marker = $this->master_model->getRecords('cf_recall_markers',$arr);
		
		if(count($marker) > 0){
			
			
			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			if($active_mod == '5'){
				$themod = $marker[0]['recall_id'];
			}
			else{
				$themod = $marker[0]['continuity_id'];
			}
			
			$data = array(
				'status'=>'OK',
				'location'=>$marker[0]['location'],
				'lat'=>$marker[0]['location_lat'],
				'lng'=>$marker[0]['location_lng'],
				'cc_id'=>$marker[0]['cc_id'],
				'id'=>$marker[0]['id'],
				'recall_id'=>$themod,
				'comment'=>$marker[0]['comment'],
				
			); 
			echo json_encode($data);

		}else{
			$data = array(
				'status'=>'error'
			); 
			echo json_encode($data);
		}

	}
	
	
	public function delete_marker(){
		$user_id = $this->session->userdata('my_id');
		$id = $_POST['id'];
		
		if($this->master_model->deleteRecord('cf_recall_markers','id',$id)) {	
			$data = array(
				'status'=>'success'
			); 
			echo json_encode($data);
		}
		else{
			$data = array(
				'status'=>'error'
			); 
			echo json_encode($data);
		}


	}
	
	
	public function get_crts_localtime(){
		
		$cc_id = $this->session->userdata('team_cc');

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$all_crt = $this->common_model->get_org_crts($cc_id,$org_id);
		
		//$all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);
		
		if(count($all_crt)>0){
			
			$user_data = array();
			
			foreach($all_crt as $r => $value){
				
//				$mymodyul = substr($this->session->userdata('org_module'),0,1);
//				if(strpos($value['module'],$mymodyul) !== false){
						
					$ts_date = date_create();
					$ts_tz = date_timestamp_get($ts_date);							
					$lat = $value['location_lat'];
					$lng = $value['location_lng'];
					$g_api_tz = 'https://maps.googleapis.com/maps/api/timezone/json?location='.$lat.','.$lng.'&timestamp='.$ts_tz;
					
					$json = file_get_contents($g_api_tz); // this WILL do an http request for you
					$tz_data = json_decode($json);
					//echo $data->{'token'};	
					
					array_push($user_data,
						array(
							"loc_time"=>$tz_data->timeZoneId,
							"user_id"=>$value['login_id']
						)
					);

				//}
				
			}//end foreach
		}//end of check count all_crt
		
		
		$date_zone = $this->common_model->userdatetime();
		
		$data = array(
			'status'=>'OK',
			'the_time'=>$date_zone,
			'user_data'=>$user_data
		);
		
		echo json_encode($data);
		
	}
	
	

	public function getapprovers(){
		$stand_id = $_POST['stand_id'];
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//check if approval exist
		$arr = array(
			'id'=>$stand_id
		);

		//active module is recall
		if($active_mod == '5'){
			$approvers = $this->master_model->getRecords('cf_recall_steps',$arr);
		}else{
			$approvers = $this->master_model->getRecords('cf_continuity_steps',$arr);
		}
		

		
		$theapprovers['approvers'] = array();
		$theapprovers['stand_id'] = $stand_id;
		$theapprovers['approvers_id'] = '';
		
		if(count($approvers) > 0){
			$theapprvrs = unserialize($approvers[0]['approvers']);
			$theapprvrs_stat = unserialize($approvers[0]['approvers_status']);
			
			if(!is_array($theapprvrs)){
				$theapprvrs = array();
			}
			
			if(!is_array($theapprvrs_stat)){
				$theapprvrs_stat = array();
			}
			
			$approvers_name = array();
			
			if(count($theapprvrs)){
				foreach($theapprvrs as $tapp){
					$user_approved = '';
					if(in_array($tapp, $theapprvrs_stat)){
						$user_approved = ' - Approved';
					}
					
					$approvers_name[] = array(
						'name'=>$this->common_model->getname($tapp),
						'status'=>$user_approved
					); 
				}
			}
			
			$theapprovers['approvers'] = $approvers_name;
			$theapprovers['approvers_id'] = $approvers[0]['id'];
		}
		
		
		echo json_encode($theapprovers);
	}	
	
	

	public function assignapprover(){
		$standbytask_id = $_POST['standbytask_id'];
		$approvers = $_POST['approvers'];
		$approvers_id = $_POST['approvers_id'];
		
		foreach($approvers as $key => $val) 
		{ 
			if($val == '') 
			{ 
				unset($approvers[$key]); 
			} 
		} 
		
		
		$approvers = array_unique($approvers);		

		$arr = array(
			'approvers'=>serialize($approvers),
			'approvers_status'=>''
		);

		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps', $arr, array('id'=>$standbytask_id));
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps', $arr, array('id'=>$standbytask_id));
		}


		$this->session->set_flashdata('success',' Approver successfully updated.');

		
		$data['approvers'] = $approvers;
		echo json_encode($data);
	}		
	

	public function markapproved(){
		$task_id = $_POST['task_id'];
		$user_id = $this->session->userdata('my_id');
		$active_mod = substr($this->session->userdata('org_module'),0,1);

		$my_approvers_info = $this->common_model->get_task_approvers($task_id);
		
		
		if($my_approvers_info['unapproved_count'] == 1){
			
				
				
			$the_new_status = '1';

			//active module is recall
			if($active_mod == '5'){
				$task = $this->master_model->getRecords('cf_recall_steps',array('id'=>$task_id));
				
				$rc = $this->master_model->getRecords('cf_recall',array('id'=>$task[0]['recall_id']));
			}
			else{
				$task = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$task_id));
				
				$rc = $this->master_model->getRecords('cf_continuity',array('id'=>$task[0]['recall_id']));
			}
				

			//get the date completed
			$initiation_date = new DateTime($rc[0]['initiation_date']);
	
	
			$nowtime = $this->common_model->userdatetime();
			
			
			$task_completed = new DateTime($nowtime);
			$difference = $initiation_date->diff($task_completed);
	
			$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i; //.' '.$difference->s.' ';
			
			
			$task_tot = explode(' ',$interval);
			$unit_time = array(
				'Y : ',
				'Mo : ',
				'D : ',
				'H : ',
				'M' // : ',
			//	'S'
			);
			$u = 0; 
			$time_lapse = '';
			foreach($task_tot as $tot){
				if ($tot == 0 ){
				}
				else if ($tot == 1 ){
					$time_lapse .= $tot.' '.$unit_time[$u].' ';
				}
				else{
					$time_lapse .= $tot.' '.$unit_time[$u].' ';
				}
				$u++;
			}
						
			
			$arr = array(
				'status'=>$the_new_status,
				'date_completed'=>$nowtime,
				'time_lapse'=>$time_lapse
				
			);
			
			
			//active module is recall
			if($active_mod == '5'){
				$this->master_model->updateRecord('cf_recall_steps',$arr,array('id'=>$task_id));
				
			}
			else{
				$this->master_model->updateRecord('cf_continuity_steps',$arr,array('id'=>$task_id));
			}
			
			
		}//end of mark completed if one approver is remaining
		
		

		//active module is recall
		if($active_mod == '5'){
			$db_name = 'cf_recall_steps';
			$task = $this->master_model->getRecords('cf_recall_steps', array('id'=>$task_id));
			
		}
		else{
			$db_name = 'cf_continuity_steps';
			$task = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$task_id));
		}


		//update approvers status
		$the_approvers = unserialize($task[0]['approvers']);
		$the_approvers_status = unserialize($task[0]['approvers_status']);
		$approvers_count = 0;
		$approvers_status = 0;
		
		$new_task_owner = '';
		
		


		if(is_array($the_approvers)){
			$approvers_count = count($the_approvers);
		}
		else{
			$the_approvers = array();
		}
		
		if(is_array($the_approvers_status)){
			$approvers_status = count($the_approvers_status);
		}
		else{
			$the_approvers_status = array();
		}


		array_push($the_approvers_status, $user_id);
		
		//$the_approvers_status[] = $user_id;

		$arr = array(
			'approvers_status'=>serialize($the_approvers_status)
		);

		$this->master_model->updateRecord($db_name, $arr, array('id'=>$task_id));

		
		//get new task owner
		$status_text = 'Awaiting Approval';
		$status_class = 'text-warning';
		
		//array_diff — Computes the difference of arrays
		$result = array_diff($the_approvers, $the_approvers_status);
		$data['countapprr'] = count($result);
		if(count($result) > 0){
			$new_arr = array();
			foreach($result as $rs){
				$new_arr[] = $rs;
			}
			$new_task_owner = $this->common_model->getname($new_arr[0]);
		}
			

		else{
			$status_text = 'Completed';
			$status_class = 'text-success';
			
			$new_task_owner = $this->common_model->getname($task[0]['assigned']);
			
		}

		$data['new_task_owner'] = $new_task_owner;
		$data['status_text'] = $status_text;
		$data['status_class'] = $status_class;
		$this->session->set_flashdata('success', ' Task approved successfully.');
		echo json_encode($data);
	}

	
	public function mark_complete_all(){
		$recall_id = $this->uri->segment(4);
		$steps_db = $this->uri->segment(5);
		
		if($recall_id !='' && $steps_db !=''){
			if($this->master_model->updateRecord($steps_db,array('status'=>'1'),array('recall_id'=>$recall_id))){
				echo 'updated<br>';
			}
		}
		echo 'uri-4 = recall_id<br>';
		echo 'uri-5 = steps_db (cf_recall_steps, cf_continuity_steps)<br>';
	}
	

	public function generate_csv(){
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$crts = $this->common_model->get_org_crts($cc_id, $org_id);

		$recallid = $this->uri->segment(4); //$_POST['recall_id'];
		$the_action = $this->uri->segment(5);
		
		$the_active_module = $this->session->userdata('org_module');
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		//active module is recall
		if($active_mod == '5'){
			$incident_type = '5';
			$inci_type = 'recall';
			$recall = $this->master_model->getRecords('cf_recall', array('id'=>$recallid));
		}
		else{
			$incident_type = '8';
			$inci_type = 'continuity';
			$recall = $this->master_model->getRecords('cf_continuity', array('id'=>$recallid));
		}

	

		$nowtime = $this->common_model->userdatetime();
		$nowtime = str_replace(array(' ', ':', '-'), '_', $nowtime);
		$recall_name = str_replace(array(' ', ':', '-'), '_', $recall[0]['incident_name']);
		$file_name = $recall_name.'_'.$recall[0]['incident_no'].'_'.$nowtime.'.csv';
		
		$fp = fopen('uploads/recall-csv/'.$file_name, 'w');
		
		$arr = array();
		
		$arr['date'] = $nowtime;
		
		$csv_margin = array(
			array(),
			array()
		);
		
		
		if(count($recall) > 0){
			$loop_count = 1;
			
			$donot_inc = array(
				'overview',
				'product_affected',
				'injuries',
				'aware_of_incident',
				'actions_taken_whom',
				'investigate_started_whom',
				'incident_status',
				'affected_location',
				'csv_file_name',
//				'cc_id',
//				'org_id'
				
			);
			fputcsv($fp, array('The Recall Incident'));
			foreach($recall as $r=>$rc){//start loop each recall
			
				$rc['default_currency_name'] = $this->common_model->currencycode($rc['default_currency_id']);
				
				$rc['initiation_type_name'] = 'Mock';
				if($rc['initiation_type'] == '1'){
					$rc['initiation_type_name'] = 'Live';
				}
				
			
				$locs = json_decode($rc['affected_location']);
				$loc_c = 1;
				$rc['affected_loc'] = '';
				
				if(count($locs) > 0){
					 foreach($locs[0] as $loc){
						 
						 if($loc !=''){
							 $rc['affected_loc'] .= $loc_c.'. '.$loc.' ';
							 $loc_c++;
						 }
					 }
				}else{
					$rc['affected_loc'] .= 'No affected location.';
				}
			
				foreach($donot_inc as $dni){
					unset($rc[$dni]);
				}
				


				
				$arr[] = array_keys($rc); //get the array keys
				
				
				
				if($loop_count == 1){
					fputcsv($fp, array_keys($rc));
				}
				
				fputcsv($fp, $rc);
	
				$loop_count++;
				
				
			}//end foreach
			
			//margin content
			foreach($csv_margin as $csv_marg){
				fputcsv($fp, $csv_marg);
			}
			
			
			
			
			//get kpi data
            if (strpos($the_active_module, '0') !== false){
				
				
				$kpi_data = $this->common_model->loop_kpi($cc_id, $org_id, $recallid, $inci_type, $incident_type);	
				
				$all_kpi = $kpi_data['all_kpi'];
				$outbound_notification = $kpi_data['outbound_notification'];
				$inbound_notification = $kpi_data['inbound_notification'];
		
		
				fputcsv($fp, array('KPI Dashboard'));
				foreach($all_kpi as $r=>$value){
					fputcsv($fp, array($value['name']));
					
					unset($value['name']);
					unset($value['kpi_status']);
					fputcsv($fp, array_keys($value));
					fputcsv($fp, $value);
					fputcsv($fp, array());
				
				}
				
				
				//outbound_notification
				fputcsv($fp, array($outbound_notification['name']));
				unset($outbound_notification['name']);
				fputcsv($fp, array_keys($outbound_notification));
				fputcsv($fp, $outbound_notification);
				fputcsv($fp, array());
				
				
				//inbound_notification
				fputcsv($fp, array($inbound_notification['name']));
				unset($inbound_notification['name']);
				fputcsv($fp, array_keys($inbound_notification));
				fputcsv($fp, $inbound_notification);

				
				//margin content
				foreach($csv_margin as $csv_marg){
					fputcsv($fp, $csv_marg);
				}
				
				
			}
			
			
			//get cost monitor data
            if (strpos($the_active_module, '6') !== false){
			
				$cost_monitor = $this->common_model->cost_monitor($cc_id, $org_id, $recallid);
			
				fputcsv($fp, array('Cost Monitor'));
				if(count($cost_monitor) > 0){
					$unset_costcat = array(
						'items',
						'cc_id',
						'org_id'
					);
					foreach($cost_monitor as $r=>$category){
						$category_data = $category;
						
						foreach($unset_costcat as $val){
							unset($category_data[$val]);
						}
						fputcsv($fp, array('Cost Category'));
						fputcsv($fp, array_keys($category_data));
						fputcsv($fp, $category_data);
						
						if(count($category['items']) > 0){
							fputcsv($fp, array());
							fputcsv($fp, array('Cost Items'));
							
							$count_items = 1;
							foreach($category['items'] as $r=>$value){
								if($count_items == 1){
									fputcsv($fp, array_keys($value));
								}
								fputcsv($fp, $value);
								$count_items++;
							}
						}
						else{
							fputcsv($fp, array());
							fputcsv($fp, array('No Cost Items'));
						}
						
						fputcsv($fp, array());
						fputcsv($fp, array());
						
					}
					
				}
				else{
					fputcsv($fp, array());
					fputcsv($fp, array('No Cost Category'));
				}


				//margin content
				foreach($csv_margin as $csv_marg){
					fputcsv($fp, $csv_marg);
				}
				
				
			}
			
			
			//get blockers
			$blockers = $this->common_model->blockers($cc_id, $recallid);
			fputcsv($fp, array('Blockers'));
			if(count($blockers) > 0){
				$count_items = 1;
				foreach($blockers as $r=>$value){
					if($count_items == 1){
						fputcsv($fp, array_keys($value));
					}
					fputcsv($fp, $value);
					$count_items++;
				}
			}
			else{
				fputcsv($fp, array('No Blockers'));
			}
			

			//margin content
			foreach($csv_margin as $csv_marg){
				fputcsv($fp, $csv_marg);
			}
			
			
			//get the task	
			$the_tasks = $this->master_model->getRecords('cf_recall_steps', array('recall_id'=>$recall[0]['id']), '*', array('step_no'=>'ASC'));
			
			
			if(count($the_tasks) > 0){
				$loop_count = 1;
				fputcsv($fp, array('The Tasks'));
				foreach($the_tasks as $r=>$value){//start loop each recall
				
					$arr[] = array_keys($value); //get the array keys
					
					//add assigned name
					$value['assigned_name'] = '';
					if($value['assigned'] != ''){
						$value['assigned_name'] = $this->common_model->getname($value['assigned']);
					}
					$value['step_no'] = $value['step_no']; // - 1;


					//add status name
					if($value['status'] == '0'){
						$value['status_name'] = 'Task Not Started';
					}
					else{
						$value['status_name'] = 'Completed';
					}
					
					$value['step_no'] = $value['step_no'] - 1;
					
					
					
					//get category name
					$tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$value['category_id']));
					
					$value['category_name'] = '';
					if(count($tsk_cat) > 0){
						$value['category_name'] = $tsk_cat[0]['category_name'];
					}
					
					$the_approvers = unserialize($value['approvers']);
					$the_approvers_status = unserialize($value['approvers_status']);

					if(!is_array($the_approvers)){
						$the_approvers = array();
					}

					if(!is_array($the_approvers_status)){
						$the_approvers_status = array();
					}
					
					

					$user_approvers = '';
					if(count($the_approvers) > 0){
						foreach($the_approvers as $tapp){
							$user_approvers .= $this->common_model->getname($tapp);
							if(in_array($tapp, $the_approvers_status)){
								$user_approvers .= ' - Approved, ';
							}
							else{
								$user_approvers .= ' - Not Approved, ';
							}
							
							
						}
					}
					else{
						$user_approvers = '';
					}
					
					$value['approvers_list'] = $user_approvers;
					
					
					if($loop_count == 1){
						fputcsv($fp, array_keys($value));
					}
					
					fputcsv($fp, $value);
					
					
					$loop_count++;
				}
			}
			
				
						
		}//end if recall > 0
		
		
		
		fclose($fp);
		
		//get validation message from admin gen update = 69, delete = 63, add = 81
		$action_message_id = '34';
		$action_message = $this->common_model->get_message($action_message_id);

		$this->session->set_flashdata('success', $action_message['success']); //' Incident successfully closed.');
		if($the_action == 'download'){
			
			//send the file to email
			$whr = array('id'=>'1');

			$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

			

			$info_arr = array(
				'from'=>$adminemail[0]['recovery_email'],
				'to'=>$this->session->userdata('logged_cc_email_id'),
				'subject'=>'Welcome to CrisisFlo',
				'view'=>'cc_send_recall_incident_csv'
			);

			$other_info = array(
				'full_name'=>$this->session->userdata('logged_display_name'),
				'attachments'=>array('uploads/recall-csv/'.$file_name)
			);

			$this->email_sending->sendmail($info_arr,$other_info);
			
		
			//delete recall incident data
			$this->db->delete('bulk_kpi', array('incident_id' => $recallid, 'incident_type'=>$active_mod));		
			
			//active module is recall, update db for file name and delete related data
			if($active_mod == '5'){
				$this->master_model->updateRecord('cf_recall', array('csv_file_name'=>$file_name), array('id'=>$recallid));
				$this->master_model->deleteRecord('cf_recall_steps', 'recall_id', $recallid);
				$this->master_model->deleteRecord('cf_recall_blockers', 'recall_id', $recallid);
				$this->master_model->deleteRecord('cf_cost_category', 'recall_id', $recallid);
				$this->master_model->deleteRecord('cf_cost_category_item', 'recall_id', $recallid);
	
			}
			else{
				$this->master_model->updateRecord('cf_continuity', array('csv_file_name'=>$file_name), array('id'=>$recallid));
				$this->master_model->deleteRecord('cf_continuity_steps', 'recall_id', $recallid);
				$this->master_model->deleteRecord('cf_recall_blockers', 'continuity_id', $recallid);
				$this->master_model->deleteRecord('cf_cost_category', 'continuity_id', $recallid);
				$this->master_model->deleteRecord('cf_cost_category_item', 'continuity_id', $recallid);
	
			}
			
			$the_csv = file_get_contents("uploads/recall-csv/".$file_name);
			force_download($file_name,$the_csv);
		}
		else{
			redirect('cc/dashboard');
		}

		
		echo 'success';
		
	}	
	
	
	public function issueboard(){
		
		$issue_id = $_POST['issue_id'];
		$task_id = $_POST['task_id'];
		$blocker = $_POST['blocker'];
		$issue = $_POST['issue'];
		$impact = $_POST['impact'];
		$issue_assign = $_POST['issue_assign'];
		$cc_id = $this->session->userdata('team_cc');
		$crt_id = $this->session->userdata('team_cc');
		if($this->session->userdata('logged_crt_login_id') != ''){
			$crt_id = $this->session->userdata('logged_crt_login_id');
		}
		$org_module = $this->session->userdata('org_module');
		$org_module = substr($org_module, 0, 1);
		
		$nowtime = $this->common_model->userdatetime();
		$arr = array(
			'cc_id'=>$cc_id,
			'crt_id'=>$crt_id,
			'blocker'=>$blocker,
			'issue'=>$issue,
			'impact'=>$impact,
			'task_id'=>$task_id,
			'assigned'=>$issue_assign
		);
		
		if($org_module == '5'){
			$recall = $this->master_model->getRecords('cf_recall_steps', array('id'=>$_POST['task_id']));
			$arr['recall_id'] = $recall[0]['recall_id'];
			$recall_field_name = 'recall_id';
		}
		else{
			$recall = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$_POST['task_id']));
			$arr['continuity_id'] = $recall[0]['recall_id'];
			$recall_field_name = 'continuity_id';
		}
		
		
		if($issue_id == ''){
			$arr['date_created'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_blockers', $arr);
			$arr['id'] = $id;
			
		}
		else{
			$this->master_model->updateRecord('cf_recall_blockers', $arr, array('id'=>$issue_id));
			$arr['id'] = $issue_id;
		}
		
		$issues = $this->master_model->getRecordCount('cf_recall_blockers', array($recall_field_name=>$recall[0]['recall_id'], 'task_id'=>$task_id));
		
		
		$issue_plural = 'Issue';
		if($issues > 1){
			$issue_plural = 'Issues';
		}
		
		$arr['issue_count'] = 'View '.$issues.' '.$issue_plural;
		
		$arr['task_status'] = $recall[0]['status'];
		
		echo json_encode($arr);
		
	}
	
	
	public function get_issues(){
		$task_id = $_GET['task_id'];
		$cc_id = $this->session->userdata('team_cc');
		$org_module = $this->session->userdata('org_module');
		$org_module = substr($org_module, 0, 1);
		
		
		if($org_module == '5'){
			$recall = $this->master_model->getRecords('cf_recall_steps', array('id'=>$task_id));
			$recall_field_name = 'recall_id';
		}
		else{
			$recall = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$task_id));
			$recall_field_name = 'continuity_id';
		}
			
		$issues = $this->master_model->getRecords('cf_recall_blockers', array($recall_field_name=>$recall[0]['recall_id'], 'task_id'=>$task_id));

		$the_issues = array();
		if(count($issues) > 0){
			foreach($issues as $r=>$issue){
				$issue['crt_name'] = $this->common_model->getname($issue['crt_id']);
				$issue['date_created_format'] = date_format(date_create($issue['date_created']), 'M d Y, g:ia');
				$status = '<span class="bg-danger">Unresolved</span>';
				$assigned = 'Not assigned';
				if($issue['note'] == ''){
					$issue['note'] = 'No notes added.';
				}
				if($issue['status'] == 1){
					$status = '<span class="bg-success">Closed</span>';
				}
				if($issue['assigned'] != '0'){
					$assigned = $this->common_model->getname($issue['assigned']);
				}
				$issue['assigned_name'] = $assigned;
				$issue['status_name'] = $status;
				$the_issues[] = $issue;
			}
		}
		
		echo json_encode($the_issues);
		
	}
	
	
	public function getsteptasks(){
		$step_no = $_GET['step_no'];
		$recall_id = $_GET['recall_id'];
		$incident_type = $_GET['incident_type'];
		
		if($incident_type == '5'){
			$tasks = $this->master_model->getRecords('cf_recall_steps', array('step_no'=>$step_no, 'recall_id'=>$recall_id), 'question, id', array('id'=>'DESC'));
		}
		else{
			$tasks = $this->master_model->getRecords('cf_continuity_steps', array('step_no'=>$step_no, 'recall_id'=>$recall_id), 'question, id', array('id'=>'DESC'));
		}
		
		echo json_encode($tasks);
		
	}
	
	
	public function action_messages(){
		$action_messages = $this->common_model->get_message();
		
		echo json_encode($action_messages);
	}
	
}
?>
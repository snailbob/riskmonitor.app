<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporting extends CI_Controller{

	public function __construct()

	{
		parent::__construct();

		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
	}


	//------------------------------------post incident

	public function index(){

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$active_module = substr($this->session->userdata('org_module'), 0, 1);
	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$open_recall = $this->master_model->getRecords('cf_recall',$whr,'*',array('id'=>'DESC'));
		}
		else{
			$open_recall = $this->master_model->getRecords('cf_continuity',$whr,'*',array('id'=>'DESC'));
		}
		

		$data=array(
			'page_title'=>'Activity Log',
			'middle_content'=>'reporting_view',
			'error'=>'',
			'success'=>'',
			'open_recall'=>$open_recall
		);

	

		$this->load->view('cc/cc-view',$data);
	}


	public function gethistory(){
		$recallid = $_POST['id'];

		$history = $this->master_model->getRecords('reporting', array('recall_id'=>$recallid),'*',array('id'=>'DESC'));
		
		
		if(count($history) > 0){
			echo '<table class="table table-hover">';
			echo '<thead><tr><th>Generated Report</th><th>Date Generated</th></tr></thead>';
			foreach($history as $r=>$value){
				echo '<tr>
					<td>'.$value['includes'].'</td>
					<td>'.date_format(date_create($value['date_created']), "F j, Y, g:i a").'</td>
					</tr>';
			}
			echo '</table>';

		}else{
			echo '<p class="lead text-center">No Report History.</p>';
		}
	}
	
		
}

?>
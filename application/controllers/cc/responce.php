<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Responce extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
		
	}
	
	public function manage()
	{  
        // load library
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');


		//new json api source for currency
		$url = "http://currency-api.appspot.com/api/EUR/USD.json?key=0d58e6fb444400a40e03e3f04bc80cb7647b0912";
		
		$result = file_get_contents($url);
		$exrate = json_decode($result);
		
		
		if (is_object($exrate)){//check if it return json
			if ($exrate->success){
				$eurtousdval = $exrate->rate;
			}
			else{
				$eurtousdval = 1.3628; //static value for euro to usd
			}
		}
		else{
			$eurtousdval = 1.3628; //static value for euro to usd
		}
		
		//select org module
		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
		$the_active_module = array('0'=>array('active_module'=>$theorg[0]['active_module']));
		

		if(isset($_POST['btn_initiate_responce']))
		{
			$this->form_validation->set_rules('scenario', 'Scenario', 'required');
			$this->form_validation->set_rules('inform', 'Inform to', 'required');
			$msg="";$scenario="";$standbymsg="";
			

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())
			{
				$scenarioid=$this->input->post('scenario');
				$radiovalit=$this->input->post('radiovalit'); 
				$inform=$this->input->post('inform'); 
				//$informsms=$this->input->post('informsms'); 
				$standbymsgid=$this->input->post('stnd_by_id');
				$thestandbymessage=$this->input->post('standbymsg');
				$newmsg=$this->input->post('newmsg');
				
				//timezone datetime
				$nowinitiatetime = time();
				$timezone = $this->session->userdata('timezone');
				$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
				
				$nowtime = gmdate("Y-m-d H:i:s", $theinitiatetime);

				//$nowtime = date("Y-m-d H:i:s");
				/* start : change status of scenario & task */
				$this->master_model->updateRecord('cf_scenario',array('scenario_status'=>'1','initiation_type'=>$radiovalit,'initiation_date'=>$nowtime),
								array('scenario_id'=>$scenarioid));
								
				$this->master_model->updateRecord('cf_task',array('task_status'=>'1'),
								array('scenario_id'=>$scenarioid));
			
				$chk_whr_arr=array('scenario_id'=>$scenarioid);
				$chk_scn=$this->master_model->getRecordCount('cf_standby_messages',$chk_whr_arr);

				if($thestandbymessage !='' && $chk_scn != 0){
				$update_stmess=array('standby_message'=>addslashes($thestandbymessage));
				$this->master_model->updateRecord('cf_standby_messages',$update_stmess,
								array('stn_msg_id'=>$standbymsgid));
				}
				/* end : change status of scenario & task */
				
				$scenario_info=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenarioid),'scenario');
				$scenario=$scenario_info[0]['scenario'];
				$sc_info=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenarioid),'*');

				
				//insert scenario log 
				$scenariolog= array(
					'scenario_id'=>$sc_info[0]['scenario_id'],
					'scenario'=>$sc_info[0]['scenario'],
					'cc_id'=>$sc_info[0]['cc_id'],
					'org_id'=>$sc_info[0]['org_id'],
					'initiation_type'=>$radiovalit,
					'initiation_date'=>$nowtime
				);

				$log_scenario_id=$this->master_model->insertRecord('log_scene_master',$scenariolog,true);
				//.insert scenario log
				
				//add date to tasks
				$this->master_model->updateRecord('cf_task',array('current_response'=>$nowtime),array('scenario_id'=>$sc_info[0]['scenario_id']));
				
				
				if(count($sc_info) > 0)
				{
					$scenario=$sc_info['0']['scenario'];
				}
				$stbymsg=$this->master_model->getRecords('cf_standby_messages',array('stn_msg_id'=>$standbymsgid),'*');
				if(count($stbymsg) > 0)
				{
					$standbymsg=$stbymsg['0']['standby_message'];
				}
				if($standbymsg!="")
				{
					$msg=$standbymsg;
				}
				else
				{
					$msg=$newmsg;
				}
				// Admin email
				$whr = array('id'=>'1');
				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');
				
				// Sending mail to CC only
				$info_arr_cc = array(
					'from'=>$adminemail[0]['contact_email'],
					'to'=>$this->session->userdata('logged_cc_email_id'),
					'subject'=>'Crisis response has been initiated',
					'view'=>'scenario-occurence-mail-to-cc'
				);
				$other_info_cc = array(
				  'name'=>$this->session->userdata('logged_display_name'),
				  'scenario_name'=>$scenario
			    );
				
				$this->email_sending->sendmail($info_arr_cc,$other_info_cc);
				// Mail sending  to CC completed  
				
				$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'initiate-response'));
				
				$from = 'CrisisFlo';
				$message = array(
					'text' => $sms_message[0]['text']//'A crisis scenario has been initiated. Please log into your user panel.'
				);
				
					
				//Recall module activated
				if (strpos($the_active_module[0]['active_module'], '7') !== false){ 
	
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));
	
					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{
							
							$to = $this->master_model->decryptIt($rem['crt_mobile']);
							$response = $this->nexmo->send_message($from, $to, $message);
	
							foreach ($response->messages as $messageinfo) {
								$recipient = $messageinfo->{'to'};
								$status = $messageinfo->{'status'};
							}
	
								
								$det_contact = $this->master_model->decryptIt($rem['crt_first_name']);
								$det_contact .= ' ';
								$det_contact .= $this->master_model->decryptIt($rem['crt_last_name']);
								$det_contact .= ' - ';
								$det_contact .= $recipient;
	
							if ($status == '0'){
	
							$nowsenttime = time();
							$timezone = $this->session->userdata('timezone');
							$thesenttime = gmt_to_local($nowsenttime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
							
							$date_sent = gmdate("Y-m-d H:i:s", $thesenttime); //g:i:sa d/m/Y
							//$date_sent = date("Y-m-d H:i:s");
	
								foreach ($response->messages as $messageinfo) {
									$message_id = $messageinfo->{'message-id'};
									$messprice = $messageinfo->{'message-price'};
									$message_price = $eurtousdval * $messprice;
									$network = $messageinfo->{'network'};
								}
	
								$this->master_model->insertRecord('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'message_id'=>$message_id,'network'=>$network,'recipient'=>$det_contact,'price'=>$message_price,'status'=>$status,'date_sent'=>$date_sent, 'log_scenario_id'=>$log_scenario_id));
								
								
								$sms_creditt=$this->master_model->getRecords('organization_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'organization_id'=>$this->session->userdata('cc_selected_orgnaization')),'sms_credit');
								
								$ded_creditt = $sms_creditt[0]['sms_credit'] - $message_price;
								
								$this->master_model->updateRecord('organization_master',array('sms_credit'=>$ded_creditt),array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
								
								
							}
							else{
								
								foreach ($response->messages as $messageinfo) {
									$err_txt = $messageinfo->{'error-text'};
								}
	
								$err_stat = $status;
								$err_stat .= ' - ';
								$err_stat .= $err_txt;
								
								$this->master_model->insertRecord('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'), 'recipient'=>$det_contact, 'price'=>'0', 'status'=>$err_stat, 'log_scenario_id'=>$log_scenario_id));
							}
								
						}
					}//end of sms notification
					
				}//end of strpos 7 = sms noti
					
				
				if($inform=='crtonly')
				{
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					$remind_result=$this->master_model->getRecords('cf_crisis_response_team',
					array('cf_login_master.user_status'=>'1','cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')));
	
					$c_pack=$this->master_model->getRecords('cf_file_upload', array('c_pack'=>'yes','cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')));
					
					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{
							$info_arr=array(
								'from'=>$adminemail[0]['contact_email'],
								'to'=>$this->master_model->decryptIt($rem['crt_email']),
								'subject'=>'Crisis response has been initiated',
								'view'=>'scenario-occurence-mail-to-crt'
							);
							
							$other_info=array(
								'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
								'msg'=>$msg,
								'cc_name'=>$this->session->userdata('logged_cc_login_id'),
								'group_id'=>$rem['group_id'],
								'c_pack'=>$c_pack,
								'scenario_name'=>$scenario
							);
							$this->email_sending->sendmail($info_arr,$other_info);
						}
					}
					$this->session->set_flashdata('success',' Crisis response successfully initiated.'); // <a href="#"  data-toggle="modal" data-target="#mySmsReportLog">View SMS Report</a>
					
					redirect(base_url().'cc/dashboard/index/'.$log_scenario_id);
					return false;
						
				}
				else if($inform=='crtstk')
				{
					//--------------------------------------for crt
					$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
					
					$whr_remind_res = array(
						'cf_login_master.user_status'=>'1',
						'cf_crisis_response_team.cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'cf_crisis_response_team.org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
					$remind_result = $this->master_model->getRecords('cf_crisis_response_team', $whr_remind_res);
					
					$whr_cpack = array(
						'c_pack'=>'yes',
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
					$c_pack = $this->master_model->getRecords('cf_file_upload', $whr_cpack);

					if(count($remind_result) > 0 )
					{
						foreach($remind_result as $rr=>$rem)
						{
							/*$whr=array('id'=>'1');
							$adminemail=$this->master_model->getRecords('email_id_master',$whr,'*');*/
							
							$info_arr = array(
								'from'=>$adminemail[0]['contact_email'],
								'to'=>$this->master_model->decryptIt($rem['crt_email']),
								'subject'=>'Crisis response has been initiated',
								'view'=>'scenario-occurence-mail-to-crt'
							);
							
							 $other_info = array(
							 	'name'=>$this->master_model->decryptIt($rem['crt_first_name']).' '.$this->master_model->decryptIt($rem['crt_last_name']),
								'msg'=>$msg,
								'cc_name'=>$this->session->userdata('logged_cc_login_id'),
								'group_id'=>$rem['group_id'],
								'c_pack'=>$c_pack,
								'scenario_name'=>$scenario
							);
							
							$this->email_sending->sendmail($info_arr,$other_info);
						}
					}
					//--------------------------------------for stakeholder
					
					$org_id = $this->session->userdata('cc_selected_orgnaization');
					
					$whr_org = array('organization_id'=>$org_id);
			
					$org_name = $this->master_model->getRecords('organization_master',$whr_org,'*');
					$stks = unserialize($org_name[0]['stakeholders']);
					foreach($stks as $value) {
						$thestk = $this->master_model->getRecords('cf_stakeholder', array('login_id'=>$value));
						
						if(count($thestk) > 0){

							if($this->common_model->stkstatus($value) == 1){
							
							
								$info_arr=array(
									'from'=>$adminemail[0]['contact_email'],
									'to'=>$this->master_model->decryptIt($thestk[0]['stk_email_address']),
									'subject'=>'Crisis response has been initiated',
									'view'=>'scenario-occurence-mail-to-stk'
								);
								
								 $other_info=array( 
									  'name'=>$this->master_model->decryptIt($thestk[0]['stk_first_name']).' '.$this->master_model->decryptIt($thestk[0]['stk_last_name']),
									  'msg'=>$msg,
									  'cc_name'=>$this->session->userdata('logged_cc_login_id'),
									  'org_name'=>$this->session->userdata('cc_selected_orgnaization_name'),
									  'scenario_name'=>$scenario
								  );
								
								$this->email_sending->sendmail($info_arr,$other_info);
							
							}//end of check if stk completed

						}//end check if stk exist
					}//end loop for stks
					
//					$this->db->join('cf_login_master','cf_login_master.login_id=cf_stakeholder.login_id');
//					$remind_result1=$this->master_model->getRecords('cf_stakeholder',array('cf_login_master.user_status'=>'1','cf_stakeholder.cc_id'=>$this->session->userdata('logged_cc_login_id'),'cf_stakeholder.org_id'=>$this->session->userdata('cc_selected_orgnaization')));



					$this->session->set_flashdata('success',' Crisis response successfully initiated.'); // <a href="#"  data-toggle="modal" data-target="#mySmsReportLog">View SMS Report</a>
					
					redirect(base_url().'cc/dashboard/index/'.$log_scenario_id);	
					return false;
				}
				
			}
		}
		//get org sms balance
		$response=$this->master_model->getRecords('organization_master',array(
		'organization_id'=>$this->session->userdata('cc_selected_orgnaization'),
		'cc_id'=>$this->session->userdata('logged_cc_login_id')));
		

		$result_scene=$this->master_model->getRecords('cf_scenario',array('scenario_status'=>'0',
		'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
		'cc_id'=>$this->session->userdata('logged_cc_login_id')),'*');
		
		$stby_msg=$this->master_model->getRecords('cf_standby_messages',array(
		'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
		'cc_id'=>$this->session->userdata('logged_cc_login_id')));
		//print_r($result_scene);exit;
		$data=array('page_title'=>'Initiate Response','error'=>'','middle_content'=>'initiate-respoce','success'=>'','result_scene'=>$result_scene,'stby_msg'=>$stby_msg,'response'=>$response);
		$this->load->view('cc/cc-view',$data);
	}
	
	
	public function fetch_stand_by_message()
	{
		$scenarioid=$this->uri->segment(4);
		$stby_msg=$this->master_model->getRecords('cf_standby_messages',array('scenario_id'=>$scenarioid));
		$stn_messsages=array();
		
		foreach($stby_msg as $msg)
		{
			$stn_messsages[$msg['stn_msg_id']]=$msg['standby_message'];
		}
		echo json_encode($stn_messsages);
	}
	
}
?>
<?php

class Scenario extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

	}

	

	public function managescene()

	{
		
		
		/* fetch initiated scenario for displying common messages */

		$whr_arr1=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')); 

		$scenarios=$this->master_model->getRecords('cf_scenario',$whr_arr1,'*',array('scenario_status'=>'ASC'));

		/* fetch initiated scenarion for displying task and team assign for it */


		$data=array('page_title'=>'Manage action plan','middle_content'=>'manage-scenario', 'scenarios'=>$scenarios);

		$this->load->view('cc/cc-view',$data);
		
	}


	public function responseflow()

	{
		$scene_id = $this->uri->segment(4); 
		
		$whr_arr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario_id'=>$scene_id); 

		$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr);
		
		$scenario_info = $this->master_model->getRecords('cf_scenario',$whr_arr);


		$data=array('page_title'=>'Manage action plan','middle_content'=>'manage-responseflow', 'task_list'=>$task_list,'scenario_info'=>$scenario_info);

		$this->load->view('cc/cc-view',$data);
	}
	
	
	
	public function add()

	{

		if(isset($_POST['add_scen']))

		{

			$this->form_validation->set_rules('scenario_desc','Scenario','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())

			{

				$scenario_desc=$this->input->post('scenario_desc',true);

				$insrt_arr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),

					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario'=>$scenario_desc,'scenario_status'=>'0');

				if($scenario_id=$this->master_model->insertRecord('cf_scenario',$insrt_arr,TRUE))

				{

					redirect(base_url().'cc/scenario/addtask/'.$scenario_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding scenario');

					redirect('cc/scenario/add');

				}

			}

		}

		

		$data=array('page_title'=>'Add Scenario','middle_content'=>'add-scenario');

		$this->load->view('cc/cc-view',$data);

	}

	

	public function addtask()

	{

		$scenario_id=$this->uri->segment(4);

		

		if(isset($_POST['add_task']))

		{

			$this->form_validation->set_rules('task_desc','Task Description','required|xss_clean');

			//$this->form_validation->set_rules('task_guide','Task Guidance','required|xss_clean');

			$this->form_validation->set_rules('assign_to','Select CRT','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			

			if($this->form_validation->run())

			{

				$task_desc=$this->input->post('task_desc',true);

				$task_guide=$this->input->post('task_guide',true);

				$assign_to=$this->input->post('assign_to',true);	

				if($this->uri->segment(5) !="" && $this->uri->segment(5)=='res')

				{$task_status='1';}

				else

				{$task_status='0';}
				//add current response date to include into task log
				$scene_resp=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenario_id),'initiation_date');

				$insrt_arr=array('scenario_id'=>$scenario_id,'task'=>$task_desc,'task_guide'=>$task_guide,'crt_id'=>$assign_to,

							'cc_id'=>$this->session->userdata('logged_cc_login_id'),
							
							'current_response' =>$scene_resp[0]['initiation_date'], //add current response date

							'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'task_status'=>$task_status);

							

				if($this->master_model->insertRecord('cf_task',$insrt_arr))

				{

					$this->session->set_flashdata('success',' Task successfully added. Please add another task for current scenario if required. Alternatively, you can create another scenario.');

					if($this->uri->segment(5) !="" && $this->uri->segment(5)=='res')

					{redirect(base_url().'cc/dashboard');}

					else

					{redirect(base_url().'cc/scenario/addtask/'.$scenario_id);}

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding task');

					if($this->uri->segment(5) !="" && $this->uri->segment(5)=='res')

					{redirect(base_url().'cc/dashboard');}

					else

					{redirect(base_url().'cc/scenario/addtask/'.$scenario_id);}

				}

			}

		}

		

		/* get last added scenario */

		$scn_info=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenario_id));

		

		/* fetch CR list to assing task, CRT will be of current selected organization */

		$whr_arr=array('crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1');

		$whr_str="(crt.org_id=".$this->session->userdata('cc_selected_orgnaization')." OR crt.org_id=0) AND (login.user_level='0' OR login.user_level='1')";

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$this->db->where($whr_str,NULL,FALSE);

		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);

		

		/* To display task list for a scenarion */

		$whr_arr1=array('scn.cc_id'=>$this->session->userdata('logged_cc_login_id'),

					'scn.org_id'=>$this->session->userdata('cc_selected_orgnaization'),'task.scenario_id'=>$scenario_id);

		$order1=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');

		$select1="scn.*,task.*,crt.crt_first_name,crt.crt_last_name";

		

		$this->db->join('cf_scenario as scn','scn.scenario_id=task.scenario_id','left');

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.crt_id','left');

		$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr1,$select1,$order1);

		/* Task list display  END */

		

		

		$data=array('page_title'=>'Add Task','middle_content'=>'add-task','scn_info'=>$scn_info,'crt_list'=>$crt_list,'task_list'=>$task_list);

		$this->load->view('cc/cc-view',$data);

	}

	

	public function managetask()

	{

		

		$whr_arr=array('scn.cc_id'=>$this->session->userdata('logged_cc_login_id'),

					'scn.org_id'=>$this->session->userdata('cc_selected_orgnaization'));

		$order=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');

		$select="scn.*,task.*,crt.crt_first_name,crt.crt_last_name";

		

		$this->db->join('cf_scenario as scn','scn.scenario_id=task.scenario_id','left');

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.crt_id','left');

		$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr,$select,$order);

		

		

		$data=array('page_title'=>'Manage action plan','middle_content'=>'manage-task','task_list'=>$task_list);

		$this->load->view('cc/cc-view',$data);

	}

	

	/* update task */

	public function update()

	{

		$task_id=$this->uri->segment(4);

		

		if(isset($_POST['add_task']))

		{

			$this->form_validation->set_rules('task_desc','Task Description','required|xss_clean');

			//$this->form_validation->set_rules('task_guide','Task Guidance','required|xss_clean');

			$this->form_validation->set_rules('assign_to','Select CRT','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			

			if($this->form_validation->run())

			{

				$task_desc=$this->input->post('task_desc',true);

				$task_guide=$this->input->post('task_guide',true);

				$assign_to=$this->input->post('assign_to',true);	

				

				$updt_arr=array('task'=>$task_desc,'crt_id'=>$assign_to,'task_guide'=>$task_guide);

							

				if($this->master_model->updateRecord('cf_task',$updt_arr,array('task_id'=>$task_id)))

				{

					$this->session->set_flashdata('success','Task updated successfully');

					redirect(base_url().'cc/scenario/managetask/');

				}

				else

				{

					$this->session->set_flashdata('error','Error while updating task');

					redirect(base_url().'cc/scenario/managetask/');

				}

			}

		}

		

		

		/* get task added scenario */

		$whr_arr=array('task.task_id'=>$task_id);

		$select="scn.*,task.*";

		$this->db->join('cf_scenario as scn','scn.scenario_id=task.scenario_id','left');

		$task_info=$this->master_model->getRecords('cf_task as task',$whr_arr,$select);

		
		
		/* fetch CR list to assing task, CRT will be of current selected organization ..old*/ 

		/*$whr_arr=array('crt.org_id'=>$this->session->userdata('cc_selected_orgnaization'),

				'crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1','login.user_level'=>'1');

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);*/

		
		
		/* fetch CR list to assing task, CRT will be of current selected organization ..display cc also*/

		$whr_arr=array('crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1');

		$whr_str="(crt.org_id=".$this->session->userdata('cc_selected_orgnaization')." OR crt.org_id=0) AND (login.user_level='0' OR login.user_level='1')";

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$this->db->where($whr_str,NULL,FALSE);

		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);
		
		
		

		$data=array('page_title'=>'Update Task','middle_content'=>'update-task','task_info'=>$task_info,

			'crt_list'=>$crt_list);

		$this->load->view('cc/cc-view',$data);

	}

	

	public function details()

	{

		$task_id=$this->uri->segment(4);

		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_task.crt_id');

		$this->db->join('cf_scenario','cf_task.scenario_id=cf_scenario.scenario_id');

		$task_info=$this->master_model->getRecords('cf_task',array('cf_task.task_id'=>$task_id));

		//print_r($task_info); exit;

		$data=array('page_title'=>'Task Details','middle_content'=>'details-task','task_info'=>$task_info,

			);

		$this->load->view('cc/cc-view',$data);

	}

	

	public function delete()

	{
		
		

		$data['success']=$data['error']="";

		$task_id=$this->uri->segment(4);

		

		if($this->master_model->deleteRecord('cf_task','task_id',$task_id))

		{	

			$this->master_model->deleteRecord('cf_task','task_id',$task_id);

			$this->session->set_flashdata('success',' Task successfully deleted.');

			redirect(base_url()."cc/scenario/managetask/");

		}

		else

		{

			$this->session->set_flashdata('error',' Error while deleting task.');

			redirect(base_url()."cc/scenario/managetask/");

		}
		
		
	//avoid error session, model
	/*	$task_id=$this->uri->segment(4);

		if($this->master_model->deleteRecord('cf_task',array('cf_task.task_id'=>$task_id)))

		{

			$this->session->set_flashdata('success','Task deleted successfully');

			redirect(base_url().'cc/scenario/managetask/');	

		}

		else

		{

			$this->session->set_flashdata('error','Error while deleting task. ');

			redirect(base_url().'cc/scenario/managetask/');

		}*/

	}

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Settings extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

		

	}


	public function index(){

		$data = array(
			'page_title'=>'Account',
			'error'=>'',
			'middle_content'=>'settings-account',
			'success'=>''
		);

		$this->load->view('cc/cc-view',$data);

	}

	public function indexx(){

		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'settings-menu',
			'success'=>''
		);

		$this->load->view('cc/cc-view',$data);

	}







	#--------------------------------------------->>updating admin password<<-------------------------------------

	

	public function changePassword() {

		if(isset($_POST['change_cc_pw'])) {
			
			//get validation message from admin gen update = 69, delete = 63, add = 81
			$action_message_id = '89';
			$action_message = $this->common_model->get_message($action_message_id);

			$this->form_validation->set_rules('new_pass','Password','required|xss_clean');

			$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean');	


			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()) {

				$new_pass=$this->input->post('new_pass');

				$confirm_pass=$this->input->post('confirm_pass');

				if($new_pass==$confirm_pass) {

					$data_array=array('pass_word'=>md5($new_pass));

					

					if($this->master_model->updateRecord('cf_login_master',$data_array,array('login_id'=>$this->session->userdata('user_id')))) {

						$this->session->set_flashdata('success', $action_message['success']);

						redirect(base_url().'cc/settings/changepassword/');

					}

					else {

						//$this->session->set_flashdata('error','Error while updating information');

						redirect(base_url().'cc/settings/changepassword/');

					}

				}

				else {

					$this->session->set_flashdata('error','Wrong Password Confirmation.');

					redirect(base_url().'cc/settings/changepassword/');

				}

			}

		}

		$details = $this->master_model->getRecords('cf_login_master',array('login_id'=>$this->session->userdata('user_id')));

		$data = array(
			'middle_content'=>'change-password',
			'page_title'=>'Change Password',
			'details'=>$details
		);	

		$this->load->view('cc/cc-view',$data);

	}

	

	

	#-------------------------------------->>Function for assignconsultant<<-----------------------------------
	public function assignconsultant(){
		
		if(isset($_POST['assign_cons'])) {

			$this->form_validation->set_rules('consultant','Consultant','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())

			{
				$consultant_id=$this->input->post('consultant','',true);
		 		
				if($this->master_model->updateRecord('organization_master',array('reseller_id'=>$consultant_id),array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')))){
					
					$this->session->set_flashdata('success',' Changes successfully saved.');
					redirect('cc/settings/assignconsultant');
				
				}
			}
			
		}
		
		$consultants = $this->master_model->getRecords('reseller_master');
		$orgg = $this->master_model->getRecords('organization_master',array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));

		$data=array('middle_content'=>'update-assignconsultant','page_title'=>'Change Password','consultants'=>$consultants,'orgg'=>$orgg);	

		$this->load->view('cc/cc-view',$data);
	}

	#-------------------------------------->>Function for updating timezone<<-----------------------------------
	public function timezone(){

		if(isset($_POST['update_timezone'])) {
			
			//get validation message from admin gen update = 69, delete = 63, add = 81
			$action_message_id = '90';
			$action_message = $this->common_model->get_message($action_message_id);
			
			$cc_id = $this->session->userdata('logged_cc_login_id');
			
			$this->form_validation->set_rules('sel_timezone','Timezone','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run()){

				$sel_timezone=$this->input->post('sel_timezone','',true);
				
				$arrr = array(
					'timezone'=>$sel_timezone,
					'timezone_updated'=>date('Y-m-d H:i:s')
					
				);
				
				if($this->master_model->updateRecord('cf_crisis_response_team',$arrr,array('cc_id'=>$cc_id))) {

					$this->session->set_userdata($arrr);
					
					$this->master_model->updateRecord('organization_master',array('timezone_set'=>'1'),array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
					
					$this->session->set_flashdata('success', $action_message['success']); //'Timezone successfully updated.');

					redirect(base_url().'cc/setup');

				}

				else {

					$this->session->set_flashdata('error', $action_message['error']); //'Error while updating Timezone');
					redirect(base_url().'cc/settings/timezone');
				}


			}
			
						
		}
		
		$personal_info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_cc_login_id')));

		$data = array(
			'middle_content'=>'update-timezone',
			'page_title'=>'Update Personal infomation',
			'personal_info'=>$personal_info
		);	
	
		$this->load->view('cc/cc-view',$data);


	}
	
	
	
	#-------------------------------------->>Function for updating emails<<-----------------------------------

	public function updateinfo() {


		if(isset($_POST['update_personal_info'])) {

			//get validation message from admin gen update = 69, delete = 63, add = 81
			$action_message_id = '91';
			$action_message = $this->common_model->get_message($action_message_id);
	
			$this->form_validation->set_rules('f_name','First Name','required|xss_clean');

			$this->form_validation->set_rules('l_name','Last Name','required|xss_clean');

			$this->form_validation->set_rules('ph_no','Support Email','required|xss_clean');	

			$this->form_validation->set_rules('address','Support Email','required|xss_clean');	

			$this->form_validation->set_rules('city','Support Email','required|xss_clean');	


			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()) {

				$f_name=$this->input->post('f_name','',true);

				$l_name=$this->input->post('l_name','',true);

				$address=$this->input->post('address','',true);
				
				$location = $this->input->post('crt_location');
				
				$location_lat = $this->input->post('lat');
				
				$location_lng = $this->input->post('lng');
					
				$countrycode = $this->input->post('countrycode','',true);
				
 				$countrycode = explode(' ', $countrycode);
				$country_id = $countrycode[0];
				$country_calling = $countrycode[1];
				
				
				// $c_id_code = explode(" ", $countrycode);

				$crt_digits = ltrim($this->input->post('ph_no','',true), '0');

				$ph_no= $country_calling.$crt_digits;

				

				$city=$this->input->post('city','',true);

				$state=$this->input->post('state','',true);

				$zip=$this->input->post('zip','',true);

				

				$data_array = array(
					'crt_first_name'=>$this->master_model->encryptIt($f_name),
					'crt_last_name'=>$this->master_model->encryptIt($l_name),
					'location'=>$location,
					'location_lat'=>$location_lat,
					'location_lng'=>$location_lng,
					'country_id'=>$country_id,
					'countrycode'=>$this->master_model->encryptIt($country_calling), //$c_id_code[1]),
					'crt_digits'=>$this->master_model->encryptIt($crt_digits),
					'crt_mobile'=>$this->master_model->encryptIt($ph_no),
					'crt_address'=>$this->master_model->encryptIt($address),
					'crt_city'=>$this->master_model->encryptIt($city),
					'crt_state'=>$this->master_model->encryptIt($state),
					'crt_zip_code'=>$this->master_model->encryptIt($zip)
				);

				

				if($this->master_model->updateRecord('cf_crisis_response_team',$data_array,array('login_id'=>$this->session->userdata('user_id')))) {

					$this->session->set_userdata('location',$location);

					$this->session->set_flashdata('success', $action_message['success']);

					redirect(base_url().'cc/settings/updateinfo');

				}

				else {

					$this->session->set_flashdata('error', $action_message['error']);
					redirect(base_url().'cc/settings/updateinfo');
				}

			}

		}	

		$personal_info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('user_id')));
	
		$countriescode=$this->master_model->getRecords('country_t');
			
	
		$data = array(
			'middle_content'=>'update-cc-info',
			'page_title'=>'Update Personal infomation',
			'personal_info'=>$personal_info,
			'countriescode'=>$countriescode
		);	
	
		$this->load->view('cc/cc-view',$data);

	}


	public function subscription(){

		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'settings-subscription',
			'success'=>''
		);

		$this->load->view('cc/cc-view',$data);
	}


	public function verifications(){
		$user_id = $this->session->userdata('user_id');
		$user_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'settings-verifications',
			'success'=>'',
			'user_info'=>$user_info
		);

		$this->load->view('cc/cc-view',$data);
	}
	
	
	public function payments(){
		$user_id = $this->session->userdata('logged_cc_login_id');
		$user_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$org_members = $this->common_model->get_org_crts($user_id,$org_id);

		$billing = $this->common_model->single_payment($user_id, 'preview');
		
//		echo json_encode($billing);
//		return false;
		
		
		$customer = ($this->session->userdata('customer') != '') ? $this->session->userdata('customer') : array();

		if(!isset($customer['description']) && $user_info[0]['stripe_customer_id'] != '0'){
			$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; // (base_url() != 'https://www.crisisflo.com/') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';
			Stripe::setApiKey($apikey);
			
			$customer = Stripe_Customer::retrieve($user_info[0]['stripe_customer_id']);
			$customer = $customer->__toArray(true);
			$this->session->set_userdata('customer', $customer);
		}
		
//		echo json_encode($customer);
//		return false;
		
		
		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'settings-payments',
			'success'=>'',
			'billing'=>$billing,
			'org_members'=>count($org_members),
			'user_info'=>$user_info,
			'customer'=>$customer
		);

		$this->load->view('cc/cc-view',$data);
	}
	
	public function stripe_card_token(){
		$user_id = $this->session->userdata('logged_cc_login_id');
		$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$username = $this->common_model->getname($user_id);
		$email = $this->master_model->decryptIt($cc_info[0]['email_id']);
		$orgname = $this->common_model->getorgname($cc_info[0]['default_org']);
		$token = $_POST['token'];
		
		$data = array(
			'token'=>$token,
			'username'=>$username,
			'email'=>$email,
			'orgname'=>$orgname
		);



		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; // (base_url() != 'https://www.crisisflo.com/') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';
		
		Stripe::setApiKey($apikey);
	
	
		try {
			// Create a Customer
			$customer = Stripe_Customer::create(array(
			  "description" => $orgname,
			  "email" => $email,
			  //"plan" => $subs_plan_id, //subscrib user to plan 
			  "card" => $token // obtained with Stripe.js
			));
			
			if($customer->id){
				$login_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
			
				$cc_arr = array(
					'stripe_customer_id'=>$customer->id,
					'user_type'=>'live'
				);
				
				if($login_info[0]['trial_end'] == '0000-00-00 00:00:00'){
					$nowtime = date("Y-m-d h:i:s");
					$cc_arr['trial_end'] = $nowtime;
				}
				
				$this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$user_id));
				$data['result'] = 'success';
			}
			
			
			
			
		} catch(Stripe_CardError $e) {
		  // The card has been declined
			//$this->session->set_flashdata('error','The card has been declined.');
			$data['result'] = 'declined';
		}		
		

		echo json_encode($data);

	}
	
	
	public function coordinators(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
	
		$whr = array(
		//	'login_id !='=>$cc_id,
			'user_level'=>'0',
			'default_org'=>$org_id
		);
		
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		
		
		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_login_master.login_id');

		$this->db->order_by('cf_login_master.login_id','DESC');

		$all_cc = $this->master_model->getRecords('cf_login_master',$whr);
		
//		echo json_encode($all_cc);
//		return false;
		$data = array(
			'page_title'=>'Manage Coordinators',
			'error'=>'',
			'middle_content'=>'settings-coordinators',
			'success'=>'',
			'all_cc'=>$all_cc,
			'org_info'=>$org_info
		);

		$this->load->view('cc/cc-view',$data);
	
	}
	
	
	public function coordinator(){
		
		$cc_id = ($this->uri->segment(4) != '') ? $this->uri->segment(4) : $this->session->userdata('logged_cc_login_id');
		$master_id = $this->session->userdata('logged_cc_login_id');
		$personal_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$cc_id));
		
		$master_cc = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$master_id));
		
		$user_info = array();
		if($this->uri->segment(4) == ''){
			foreach($personal_info as $r=>$value){
				$keys = array_keys($value);
				
				foreach($keys as $k){
					$value[$k] = '';
				}
				$user_info[] = $value;
			}
			
			$personal_info = $user_info;
			
		}
		
//		echo json_encode($user_info);
//		return false;


		if(isset($_POST['add_coordinator'])) {

			//get validation message from admin gen update = 69, delete = 63, add = 81
			$action_message_id = '91';
			$action_message = $this->common_model->get_message($action_message_id);
	
			$this->form_validation->set_rules('f_name','First Name','required|xss_clean');

			$this->form_validation->set_rules('l_name','Last Name','required|xss_clean');

			$this->form_validation->set_rules('ph_no','Support Email','required|xss_clean');	

			$this->form_validation->set_rules('address','Support Email','required|xss_clean');	

			$this->form_validation->set_rules('city','Support Email','required|xss_clean');	

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');	


			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()) {

				$f_name=$this->input->post('f_name','',true);

				$l_name=$this->input->post('l_name','',true);

				$emailid=$this->input->post('emailid','',true);
				
				$address=$this->input->post('address','',true);
				
				$location = $this->input->post('crt_location');
				
				$location_lat = $this->input->post('lat');
				
				$location_lng = $this->input->post('lng');
					
				$countrycode = $this->input->post('countrycode','',true);
				
 				$countrycode = explode(' ', $countrycode);
				$country_id = $countrycode[0];
				$country_calling = $countrycode[1];
				
				
			//	$c_id_code = explode(" ", $countrycode);

				$crt_digits = ltrim($this->input->post('ph_no','',true), '0');

				$ph_no= $country_calling.$crt_digits;

				

				$city = $this->input->post('city','',true);

				$state = $this->input->post('state','',true);

				$zip = $this->input->post('zip','',true);


				$data_array = array(
					'crt_first_name'=>$this->master_model->encryptIt($f_name),
					'crt_last_name'=>$this->master_model->encryptIt($l_name),
					'crt_email'=>$this->master_model->encryptIt($emailid),
					'location'=>$master_cc[0]['location'],//$location,
					'location_lat'=>$master_cc[0]['location_lat'],//$location_lat,
					'location_lng'=>$master_cc[0]['location_lng'],//$location_lng,
					'country_id'=>$country_id,
					'countrycode'=>$this->master_model->encryptIt($country_calling), //$c_id_code[1]),
					'crt_digits'=>$this->master_model->encryptIt($crt_digits),
					'crt_mobile'=>$this->master_model->encryptIt($ph_no),
					'crt_address'=>$this->master_model->encryptIt($address),
					'crt_city'=>$this->master_model->encryptIt($city),
					'crt_state'=>$this->master_model->encryptIt($state),
					'crt_zip_code'=>$this->master_model->encryptIt($zip)
				);

				//get validation message from admin
				$action_message_id = '52';
				$action_message = $this->common_model->get_message($action_message_id);


				//add new coordinator
				if($this->uri->segment(4) == ''){
					
					$duplicate_count = $this->common_model->checkduplicate($emailid);
					
					if($duplicate_count > 0){
						$this->session->set_flashdata('error', 'Email already exist in database.');
						redirect(base_url().'cc/settings/coordinators');
						return false;
					}
					
					$onetime_key = md5(microtime());
					$master_login = $this->master_model->getRecords('cf_login_master', array('login_id'=>$master_id));
					
					$super_cc_info = $master_login[0];
					unset($super_cc_info['login_id']);
					$nowtime = date("Y-m-d h:i:s");
					$super_cc_info['pass_word'] = '';
					$super_cc_info['date_added'] = $nowtime;
					$super_cc_info['user_status'] = '0';
					$super_cc_info['onetime_key'] = $onetime_key;
					$super_cc_info['email_id'] = $this->master_model->encryptIt($emailid);	
					
					//inherit from super user
					$cc_id = $this->master_model->insertRecord('cf_login_master', $super_cc_info, true);
					
					$data_array['login_id'] = $cc_id;	
									
					$this->master_model->insertRecord('cf_crisis_response_team', $data_array);
			
			
			
					$admin_email = $this->master_model->getRecords('email_id_master');

					$info_arr = array('to'=>$emailid,'from'=>$admin_email[0]['info_email'],

					'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

					

					$other_info=array('first_name'=>$f_name,'last_name'=>$l_name,'onetime_key'=>$onetime_key,

					'email'=>$emailid);
					

					$this->email_sending->sendmail($info_arr,$other_info);

					$data_array['super_cc_info'] = $super_cc_info;
					$data_array['info_arr'] = $info_arr;
					$data_array['other_info'] = $other_info;
					
					$this->session->set_flashdata('success', $action_message['success']);
				}
				
				//update
				else{
					
					if($this->master_model->updateRecord('cf_crisis_response_team',$data_array,array('login_id'=>$cc_id))) {
	
//						$this->session->set_userdata('location',$location);
//	
						$this->session->set_flashdata('success', $action_message['success']);
	
					}
	
					else {
	
						$this->session->set_flashdata('error', $action_message['error']);
					}
			
			
				
				}
				
				redirect(base_url().'cc/settings/coordinators');
				//echo json_encode($data_array);
				return false;
				


			}

		}	//end form submit

		
	
		$countriescode = $this->master_model->getRecords('country_t');
			
	
		$data = array(
			'middle_content'=>'settings-update-cc-info',
			'page_title'=>'Update Personal infomation',
			'personal_info'=>$personal_info,
			'countriescode'=>$countriescode
		);	
	
		$this->load->view('cc/cc-view',$data);
	
	}
	
	//------------------------------------resend crt
	public function resend_request(){
		
		$id = $_POST['login_id'];


		/* fetch  crt members*/
		$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$id));
		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));


		$whr=array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

		$info_arr=array(
			'from'=>$adminemail[0]['recovery_email'],
		
			'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
		
			'subject'=>'Welcome to CrisisFlo',
		
			'view'=>'registration-mail-to-cc'
		);

	

		$other_info = array(
			
			'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),
			
			'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),
			
			'onetime_key'=>$crt_onetimekey[0]['onetime_key'],
			
			'email'=>$this->master_model->decryptIt($crt_record[0]['crt_email'])

		);


		if($this->email_sending->sendmail($info_arr,$other_info)){
//			$this->session->set_flashdata('success',' Email request successfully re-sent.');
			echo 'success';
		}
		else{
//			$this->session->set_flashdata('error',' Email request failed to re-sent.');
			echo 'error';
		}

		
	}
		
	
	public function add_connect(){
		$id = $_POST['id'];
		$email = $_POST['email'];
		$type = $_POST['type'];
		$user_id = $this->session->userdata('logged_cc_login_id');
		
		if($type == 'linkedin'){
			$arr = array(
				'linkedin_id'=>$id
			);
		}
		
		else if($type == 'gplus'){
			$arr = array(
				'gplus_id'=>$id
			);
		}
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		
		echo json_encode($arr);
	
	}
	
	public function remove_connect(){
		$type = $_POST['type'];
		$user_id = $this->session->userdata('logged_cc_login_id');

		if($type == 'linkedin'){
			$arr = array(
				'linkedin_id'=>''
			);
		}
		
		else if($type == 'gplus'){
			$arr = array(
				'gplus_id'=>''
			);
		}
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		echo json_encode($arr);
	}


	public function save_avatar(){

		$avatar = $_POST['avatar'];
		$id = $this->session->userdata('user_id');

		$arr = array(
			'avatar'=>$avatar
		);
		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$id));

		$this->session->set_userdata($arr);

		echo json_encode($arr);
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Stakeholder extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

	}

	//------------------------------------add crt


	public function test(){

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$whr = array('organization_id !='=>$org_id);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');

		//add stake to org table
//					$org_stks = json_decode($org_name[0]['stakeholders']); 
//					$new_stks = array_push($org_stks, $login_id);
		$org_stks = array(); 
		$new_stks = serialize($org_stks);
		$this->master_model->updateRecord('organization_master', array('stakeholders'=>$new_stks), $whr);



	}
	
	public function import_stk(){
		$stakeholders = $_POST['stkcheck'];
		

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$whr = array('organization_id'=>$org_id);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');
		
		$stks = $org_name[0]['stakeholders'];
		foreach($stakeholders as $stake){
			$stks = $this->addItem($stks, $stake);
			$this->master_model->updateRecord('organization_master', array('stakeholders'=>$stks), $whr);
		}
		$this->session->set_flashdata('success',' Import successful.');
		redirect('cc/stakeholder/managestk');

	}
	
	
	public function addItem($serializedArray, $item){
		
	   $a = unserialize($serializedArray);
	   $a[] = $item;
	   return serialize($a);
	}

	
	public function add(){

		if(isset($_POST['btn_add_stakeholder'])){

			$this->form_validation->set_rules('stk_firstname','First Name','required');

			$this->form_validation->set_rules('stk_lastname','Last Name','required');

			$this->form_validation->set_rules('stk_position','Postion','required');

			$this->form_validation->set_rules('stk_email','Email','required|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('stk_no','Contact Number','required');

			$this->form_validation->set_message('required', 'Mandatory field');


				if($this->form_validation->run()){

					$stk_firstname=$this->input->post('stk_firstname');

					$stk_lastname=$this->input->post('stk_lastname');

					$stk_position=$this->input->post('stk_position');

					$stk_email=$this->input->post('stk_email');

					$countrycode = $this->input->post('countrycode');

					$stk_digits = ltrim($this->input->post('stk_no'), '0');

					$stk_no = $countrycode.$stk_digits;

					$onetime_key = md5(microtime());

					//check duplicate email
					$users = $this->master_model->getRecords('cf_login_master');	
			
//					foreach($users as $ss=>$usr){
//						if($usr['email_id'] == $this->master_model->encryptIt($stk_email)){
//							$this->session->set_flashdata('error', ' Email already exists in the database.');
//							redirect('cc/stakeholder/add');
//							return false;
//						}
//					}


					$org_id = $this->session->userdata('cc_selected_orgnaization');
					$whr = array('organization_id'=>$org_id);

					$org_name = $this->master_model->getRecords('organization_master',$whr,'*');

					$input_arr=array(
						'email_id'=>$this->master_model->encryptIt($stk_email),
						'user_level'=>'2',
						'default_org'=>$org_id,
						'onetime_key'=>$onetime_key
					);

					$login_id = $this->master_model->insertRecord('cf_login_master',$input_arr,true);

					//add stake to serialized org table
//					$org_stks = unserialize($org_name[0]['stakeholders']); 
//					$org_stks[] = $login_id;
					$stks = $this->addItem($org_name[0]['stakeholders'], $login_id);
					$this->master_model->updateRecord('organization_master', array('stakeholders'=>$stks), $whr);
					
					
					$input_array=array(
						'login_id'=>$login_id,
						'stk_first_name'=>$this->master_model->encryptIt($stk_firstname),
						'stk_last_name'=>$this->master_model->encryptIt($stk_lastname),
						'stk_organisation'=>$org_name[0]['organization_name'],
						'stk_position'=>$stk_position,
						'stk_email_address'=>$this->master_model->encryptIt($stk_email),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'stk_digits'=>$this->master_model->encryptIt($stk_digits),
						'stk_mobile_num'=>$this->master_model->encryptIt($stk_no),
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);

					

					if($this->master_model->insertRecord('cf_stakeholder',$input_array))

					{

						$whr=array('id'=>'1');

						$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

						$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

						$info_arr = array(
							'from'=>$adminemail[0]['recovery_email'],
							'to'=>$stk_email,
							'subject'=>'Stakeholder Contact Details on CrisisFlo',
							'view'=>'stk_registration_on_crisesflo'
						);

					
					
					     $other_info = array(
						 	'first_name'=>$stk_firstname,
							'selected_orgnaization_name'=>$this->session->userdata('cc_selected_orgnaization_name'),
							'last_name'=>$stk_lastname,
							'login_id'=>$onetime_key
						);


					    $this->email_sending->sendmail($info_arr,$other_info);


						//get validation message from admin gen update = 69, delete = 63, add = 81
						$action_message_id = '92';
						$action_message = $this->common_model->get_message($action_message_id);

						$this->session->set_flashdata('success', $action_message['success']);

						redirect(base_url().'cc/stakeholder/managestk/');

					}

				}

		}
		$countriescode = $this->master_model->getRecords('country_t');

		$data = array(
			'page_title'=>'Add Stakeholder',
			'error'=>'',
			'middle_content'=>'add-stakeholder',
			'success'=>'',
			'countriescode'=>$countriescode
		);

		$this->load->view('cc/cc-view',$data);

	}

	

	

	//-----------------------------------------manage crt

	public function managestk()

	{
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		
		$this->db->join('cf_login_master','cf_login_master.login_id=cf_stakeholder.login_id');

		$this->db->order_by('cf_stakeholder.login_id','DESC');

			
		$all_stk = $this->master_model->getRecords('cf_stakeholder',array('cf_stakeholder.cc_id'=>$cc_id,'cf_stakeholder.org_id'=>$org_id));
	
	
		$whr = array(
			'cc_id'=>$cc_id,
			'organization_id'=>$org_id,
			
		);
	
		$the_org = $this->master_model->getRecords('organization_master',$whr);
		$stks = unserialize($the_org[0]['stakeholders']);
		$allstk = $this->master_model->getRecords('cf_login_master', array('user_level'=>'2'));

		$data=array(
			'page_title'=>"Manage Stakeholder",
			'middle_content'=>'manage-stakeholder',
			'success'=>'',
			'error'=>'',
			'the_org'=>$the_org,
			'stks'=>$stks,
			'allstk'=>$allstk,
			'all_stk'=>$all_stk
		);

		$this->load->view('cc/cc-view',$data);

	}

	//-----------------------------------------remove stk

	public function remove() {
		$stkid = $this->uri->segment(4);
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$whr = array('organization_id'=>$org_id);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');
		
		$stks = unserialize($org_name[0]['stakeholders']);
		
		if(($key = array_search($stkid, $stks)) !== false) {
			unset($stks[$key]);
		}
		
		$this->master_model->updateRecord('organization_master', array('stakeholders'=>serialize($stks)), $whr);


		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94
		$action_message_id = '94';
		$action_message = $this->common_model->get_message($action_message_id);

		if($this->master_model->deleteRecord('cf_login_master','login_id',$login_id)) {	
			$this->master_model->deleteRecord('cf_stakeholder','login_id',$login_id);
			$this->session->set_flashdata('success', $action_message['success']);
		}

		else {
			$this->session->set_flashdata('error', $action_message['error']);
		}


		redirect(base_url().'cc/stakeholder/managestk/');
		
	}


	//-----------------------------------------Delete crt

	public function delete() {

		$data['success'] = $data['error']="";

		$login_id = $this->uri->segment(4);

		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94
		$action_message_id = '94';
		$action_message = $this->common_model->get_message($action_message_id);

		if($this->master_model->deleteRecord('cf_login_master','login_id',$login_id)) {	

			$this->master_model->deleteRecord('cf_stakeholder','login_id',$login_id);


			$this->session->set_flashdata('success', $action_message['success']);

			redirect(base_url().'cc/stakeholder/managestk/');

		}

		else {

			$this->session->set_flashdata('error', $action_message['error']);

			redirect(base_url().'cc/stakeholder/managestk/');

		}

		

	}

	

	//------------------------------------Update crt

	public function update() {

		$login_id=$this->uri->segment(4);

		if(isset($_POST['btn_add_stakeholder'])) {

			//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94
			$action_message_id = '93';
			$action_message = $this->common_model->get_message($action_message_id);


			$this->form_validation->set_rules('stk_firstname','First Name','required');

			$this->form_validation->set_rules('stk_lastname','Last Name','required');

			$this->form_validation->set_rules('stk_position','Postion','required');

			$this->form_validation->set_rules('stk_no','Contact Number','required');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run()) {

				$stk_firstname = $this->input->post('stk_firstname');

				$stk_lastname = $this->input->post('stk_lastname');

				$stk_position = $this->input->post('stk_position');


				$countrycode = $this->input->post('countrycode');

				$stk_digits = ltrim($this->input->post('stk_no'),'0');

				$stk_no = $countrycode.$stk_digits;

				

				$input_array=array(
					'stk_first_name'=>$this->master_model->encryptIt($stk_firstname),
					'stk_last_name'=>$this->master_model->encryptIt($stk_lastname),
					'stk_position'=>$stk_position,
					'countrycode'=>$this->master_model->encryptIt($countrycode),
					'stk_digits'=>$this->master_model->encryptIt($stk_digits),
					'stk_mobile_num'=>$this->master_model->encryptIt($stk_no),
					'cc_id'=>$this->session->userdata('logged_cc_login_id')
				);

				

				if($this->master_model->updateRecord('cf_stakeholder',$input_array,array('login_id'=>$login_id))){

					$this->session->set_flashdata('success', $action_message['success']);
					redirect(base_url().'cc/stakeholder/managestk/');

				}

			}

		}
		
		$countriescode = $this->master_model->getRecords('country_t');

		$stk_record = $this->master_model->getRecords('cf_stakeholder',array('login_id'=>$login_id));

		$data = array(
			'page_title'=>'Update Stakeholder',
			'error'=>'',
			'middle_content'=>
			'update-stk',
			'success'=>'',
			'stk_record'=>$stk_record,
			'countriescode'=>$countriescode
		);

		$this->load->view('cc/cc-view',$data);

	}

	

	//----------------------------------view more about stakeholder

	

	public function details() {

		$login_id=$this->uri->segment(4);

		$stk_record=$this->master_model->getRecords('cf_stakeholder',array('login_id'=>$login_id));

		$data=array('page_title'=>'Stakeholder Details','error'=>'','middle_content'=>'details-stk','success'=>'',

		'stk_record'=>$stk_record);

		$this->load->view('cc/cc-view',$data);

	}

}

?>
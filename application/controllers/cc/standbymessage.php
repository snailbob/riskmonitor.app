<?php
class Standbymessage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();
	}
	
	public function add()
	{
		if(isset($_POST['add_stand_by']))
		{
			$this->form_validation->set_rules('sel_scenario','Select Scenario','required|xss_clean');
			$this->form_validation->set_rules('stand_by_msg','Message','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())
			{
				$sel_scenario=$this->input->post('sel_scenario',true);
				$stand_by_msg=$this->input->post('stand_by_msg',true);
				
				/*check wether message is alredy exists for same scenario or not*/				
				$chk_whr_arr=array('scenario_id'=>$sel_scenario);
				$chk_scn=$this->master_model->getRecordCount('cf_standby_messages',$chk_whr_arr);
				if($chk_scn == 0)
				{
					$insrt_arr=array('scenario_id'=>$sel_scenario,'cc_id'=>$this->session->userdata('logged_cc_login_id'),
							'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'standby_message'=>$stand_by_msg);
					if($this->master_model->insertRecord('cf_standby_messages',$insrt_arr))
					{
						//indicates scenario has message
						$thesceneid=array('scenario_id'=>$sel_scenario);
						$standmessrec=$this->master_model->getRecords('cf_standby_messages',$thesceneid);
						$this->master_model->updateRecord('cf_scenario',array('has_standmess'=>$standmessrec[0]['stn_msg_id']),array('scenario_id'=>$sel_scenario));
						
						$this->session->set_flashdata('success',' Standby message successfully added.');
						redirect(base_url().'cc/standbymessage/manage');
					}
					else
					{
						$this->session->set_flashdata('error','Error while adding message.');
						redirect(base_url().'cc/standbymessage/add');
					}
				}
				else
				{
					$this->session->set_flashdata('error','Message already exists for selected scenario.');
					redirect(base_url().'cc/standbymessage/add');
				}
			}
		}
		
		/* fetch scenarion added by current CC and for selected organization */
		$whr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),
					'has_standmess'=>'0');
		$scn_list=$this->master_model->getRecords('cf_scenario',$whr);
		
		$data=array('page_title'=>'Add Stand by Message','middle_content'=>'add-standbymsg','scn_list'=>$scn_list);
		$this->load->view('cc/cc-view',$data);
	}
	
	public function manage()
	{
		$whr=array('scn.cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'scn.org_id'=>$this->session->userdata('cc_selected_orgnaization'));
		$order=array('scn.scenario_id'=>'ASC');
		
		$this->db->join('cf_scenario as scn','scn.scenario_id=stand_msg.scenario_id');
		$msg_list=$this->master_model->getRecords('cf_standby_messages as stand_msg',$whr,'',$order);
		
		$data=array('page_title'=>'Manage stand by message','middle_content'=>'manange-standbymsg','msg_list'=>$msg_list);
		$this->load->view('cc/cc-view',$data);
	}
	
	public function update()
	{
		$message_id=$this->uri->segment(4);
		
		if(isset($_POST['add_stand_by']))
		{
			
			$this->form_validation->set_rules('sel_scenario','Select Scenario','required|xss_clean');
			$this->form_validation->set_rules('stand_by_msg','Message','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');
			if($this->form_validation->run())
			{
				$sel_scenario=$this->input->post('sel_scenario',true);
				$stand_by_msg=$this->input->post('stand_by_msg',true);
				
				$update_arr=array('scenario_id'=>$sel_scenario,'standby_message'=>addslashes($stand_by_msg));
				if($this->master_model->updateRecord('cf_standby_messages',$update_arr,array('stn_msg_id'=>$message_id)))
				{
					$this->session->set_flashdata('success',' Message successfully updated.');
					redirect(base_url().'cc/standbymessage/manage');
				}
				else
				{
					$this->session->set_flashdata('error',' Error while updating message.');
					redirect(base_url().'cc/standbymessage/update/'.$message_id);
				}
			}
		}
		
		/* select message details */
		$msg_info=$this->master_model->getRecords('cf_standby_messages',array('stn_msg_id'=>$message_id));
		
		/* fetch scenarion added by current CC and for selected organization */
		$whr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'));
		$scn_list=$this->master_model->getRecords('cf_scenario',$whr);
		
		$data=array('page_title'=>'Update Stand by Message','middle_content'=>'update-standbymsg','scn_list'=>$scn_list,
				'msg_info'=>$msg_info);
		$this->load->view('cc/cc-view',$data);
	}
	
	public function details()
	{
		$message_id=$this->uri->segment(4); 
		/* fetch scenarion added by current CC and for selected organization */
		$whr=array('cf_standby_messages.stn_msg_id'=>$message_id);
		$this->db->join('cf_standby_messages','cf_standby_messages.scenario_id=cf_scenario.scenario_id');
		$scn_list=$this->master_model->getRecords('cf_scenario',$whr);
		
		$data=array('page_title'=>'Stand by Message Details','middle_content'=>'details-standbymsg','scn_list'=>$scn_list);
		$this->load->view('cc/cc-view',$data);
		
	}
	
	public function delete()
	{
		$stn_msg_id=$this->uri->segment(4);
		if($this->master_model->deleteRecord('cf_standby_messages','stn_msg_id',$stn_msg_id))
		{
			//added to reset message
			$this->master_model->updateRecord('cf_scenario',array('has_standmess'=>'0'),array('has_standmess'=>$stn_msg_id));
			
			$this->session->set_flashdata('success',' Record successfully deleted.');
			redirect(base_url().'cc/standbymessage/manage');
		}
		else
		{
			$this->session->set_flashdata('error',' Error while deleting record');
			redirect(base_url().'cc/standbymessage/manage');
		}
	}
}
?>
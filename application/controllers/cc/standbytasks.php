<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Standbytasks extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');

		// $this->cc_check_session->checksessionvalue();

	}


	//------------------------------------post incident

	public function index() {
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$the_crts = $this->common_model->get_org_crts($cc_id, $org_id);


		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$pack_id = $this->common_model->pack_id();
		

		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		
		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'manage-recall-v3',
			'packname'=>$packname,
			'packsteps'=>$packsteps,
			'category'=>$category,
			'pack_id'=>$pack_id,
			'the_crts'=>$the_crts
		);
		
		$this->load->view('cc/cc-view',$data);
		
	}

	public function incident_form() {
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		
		$data = array(
			'page_title'=>'Incident Form Builder',
			'middle_content'=>'incident_form_builder',
		);
		
		$this->load->view('cc/cc-view',$data);
		
	}

	public function activetask(){

		$data = array(
			'page_title'=>'Task',
			'middle_content'=>'activetask_view',
			'error'=>'',
			'success'=>'',
		);


		$this->load->view('cc/cc-view',$data);
	}
		

	public function preview() {

		$data = array(
			'nav_title'=>'[CRISISFLO TASK PREVIEW MODE]',
			'page_title'=>'Preview Task',
			'middle_content'=>'headless/preview_task_view',
		);
		
		$this->load->view('cc/headless/cc_headless_view',$data);
	}

		

	public function preview_incident() {

		$data = array(
			'nav_title'=>'[CRISISFLO INCIDENT TASKS PREVIEW MODE]',
			'page_title'=>'Preview Incident Tasks',
			'middle_content'=>'activetask_view',
		);
		
		$this->load->view('cc/headless/cc_headless_view',$data);
	}
	
	public function old() {
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$the_crts = $this->common_model->get_org_crts($cc_id,$org_id);


		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$pack_id = 18; //recall pack id
				
			if(count($mypack)> 0){
				$input_pack_id = $mypack[0]['input_pack_id'];
				
				//if org has input_pack
				if($input_pack_id != 0){
					$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('input_pack_id'=>$input_pack_id));
					
					if (count($myrecallpack) > 0){
						$pack_id = $myrecallpack[0]['id'];
					}
	
				}
				else{
					//check if has reseller assigned
					if($mypack[0]['reseller_id'] != '0'){
						$reseller_pack = $this->master_model->getRecords('cf_recall_packs',array('consultant_id'=>$mypack[0]['reseller_id']));
						
						if(count($reseller_pack) > 0){
							$pack_id = $reseller_pack[0]['id'];
						}
					}
	
				}
				
			}	//count($mypack)> 0		
			
		}
		else{
			$pack_id = 17; //continuity pack id
		}
		

		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		
		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'manage-recall-v2',
			'packname'=>$packname,
			'packsteps'=>$packsteps,
			'category'=>$category,
			'pack_id'=>$pack_id,
			'the_crts'=>$the_crts
		);
		
		$this->load->view('cc/cc-view',$data);
		
	}

	public function assignapprover(){
		$standbytask_id = $_POST['standbytask_id'];
		$approvers = $_POST['approvers'];
		$approvers_id = $_POST['approvers_id'];
		
		foreach($approvers as $key => $val) 
		{ 
			if($val == '') 
			{ 
				unset($approvers[$key]); 
			} 
		} 
		
		
		$approvers = array_unique($approvers);		
		

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'standbytask_id'=>$standbytask_id,
			'approvers'=>serialize($approvers)
		);
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94
		$action_message_id = '69';
		$action_message = $this->common_model->get_message($action_message_id);
		$action_message_id2 = '81';
		$action_message2 = $this->common_model->get_message($action_message_id2);
		
		if($approvers_id != ''){
			$this->master_model->updateRecord('cf_recall_approvers', $arr, array('id'=>$approvers_id));
			$this->session->set_flashdata('success', $action_message['success']);
		}
		else{
			$arr['date_added'] = $nowtime;
			$approvers_id = $this->master_model->insertRecord('cf_recall_approvers', $arr, true);
			$this->session->set_flashdata('success', $action_message2['success']);
		}
		
		$data['approvers'] = $approvers;
		$data['approvers_id'] = $approvers_id;
		echo json_encode($data);
	}


	public function getapprovers(){
		$standbytask_id = $_POST['stand_id'];

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		//check if approval exist
		$arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'standbytask_id'=>$standbytask_id
		);
		
		$approvers = $this->master_model->getRecords('cf_recall_approvers', $arr);
		
		$theapprovers['approvers'] = array();
		$theapprovers['standbytask_id'] = $standbytask_id;
		$theapprovers['approvers_id'] = '';
		
		if(count($approvers) > 0){
			$theapprovers['approvers'] = unserialize($approvers[0]['approvers']);
			$theapprovers['approvers_id'] = $approvers[0]['id'];
		}
		
		
		echo json_encode($theapprovers);
	}
	
	
	public function getguidance(){
		$standbytask_id = $_POST['stand_id'];

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		//check if approval exist
		$arr = array(
			'id'=>$standbytask_id
		);
		
		$standbytask = $this->master_model->getRecords('cf_recall_guidance', $arr);
		
		echo json_encode($standbytask);

	}
	
	public function updateguidance(){
		$standbytask_id = $_POST['standbytask_id'];
		$task_guide = $_POST['task_guide'];

		$arr = array(
			'guidance'=>$task_guide
		);

		$this->master_model->updateRecord('cf_recall_guidance', $arr, array('id'=>$standbytask_id));
		
		echo json_encode($arr);
	
	}


/*	public function indexxx()

	{
		$step2 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'2','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step3 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'3','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step4 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'4','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step5 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'5','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step6 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'6','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step7 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'7','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step8 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'8','recall_pack_id'=>'0'),'',array('category'=>'ASC','arrangement'=>'ASC'));

		$category=$this->master_model->getRecords('cf_recall_steps_category',array('deleted'=>'0','recall_pack_id'=>'0'),'',array('step_no'=>'ASC','id'=>'ASC'));
		$recall_packs=$this->master_model->getRecords('cf_recall_packs',array('continuity'=>'0'));

		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'manage-recall-tasks',
			'step2'=>$step2,
			'step3'=>$step3,
			'step4'=>$step4,
			'step5'=>$step5,
			'step6'=>$step6,
			'step7'=>$step7,
			'step8'=>$step8,
			'category'=>$category,
			'recall_packs'=>$recall_packs
		);

		$this->load->view('cc/cc-view',$data);


	}*/


	public function recall_pack() {
		
		$pack_id = $this->uri->segment(4);
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'',array('step_no'=>'ASC','id'=>'ASC'));
		
		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'manage-recall-pack',
			'packname'=>$packname,
			'packsteps'=>$packsteps,
			'category'=>$category
		);
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function save_recall_pack_category(){
		$name = $_POST['name'];
		$pack_id = $_POST['pack_id'];

		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '96';
		$action_message = $this->common_model->get_message($action_message_id);


		if($this->master_model->insertRecord('cf_recall_packs_steps',array('name'=>$name,'recall_pack_id'=>$pack_id),true)){
			$this->session->set_flashdata('success', $action_message['success']);
			echo 'success';
		}
	}
	
	public function update_recall_pack_category(){
		$name = $_POST['name'];
		$id = $_POST['id'];
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '78';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if($this->master_model->updateRecord('cf_recall_packs_steps',array('name'=>$name),array('id'=>$id))){
			$this->session->set_flashdata('success', $action_message['successs']);
			echo 'success';
		}

	}
	
	public function save_pack_subcat(){
		$step = $_POST['step'];
		$subname = $_POST['catt'];
		$pack_id = $_POST['pack_id'];
		

		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '96';
		$action_message = $this->common_model->get_message($action_message_id);

		$arr = array(
			'category_name'=>$subname,
			'step_no'=>$step,
			'recall_pack_id'=>$pack_id
		);
		if($this->master_model->insertRecord('cf_recall_steps_category',$arr,true)){
			$this->session->set_flashdata('success','<span class="new_cat">'.$action_message['success'].'</span>');
			echo 'success';
		
		}
	}

	
	
	public function save_new_pack_task(){
		$task = $_POST['task'];
		$task_guidance = $_POST['task_guidance'];
		$step = $_POST['category'];
		$sub_cat = $_POST['sub_cat'];
		$pack_id = $_POST['pack_id'];
		
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '81';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$updt_arr = array(
			'step_no'=>$step,
			'guidance'=>$task_guidance,
			'question'=>$task,
			'category'=>$sub_cat,
			'recall_pack_id'=>$pack_id,
		);
		if($this->master_model->insertRecord('cf_recall_guidance',$updt_arr)){
			$this->session->set_flashdata('success', $action_message['success']);
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}
	}
	
	
	

	public function select_pack_step(){
		
		$step_no = $_POST['step'];
		$pack_id = $_POST['pack_id'];
		
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$step_no,'deleted'=>'0','recall_pack_id'=>$pack_id));


		echo '<label class="control-label" for="select_subcat">Sub-Category</label>
		<select id="select_subcat" name="select_subcat" class="form-control">
		  <option value="0">Select</option>';
		  
		foreach($category as $r=>$cat){ 
			
			echo '<option value="';
			echo $cat['id'];
			echo '">';
			echo $cat['category_name'].'</option>';
		}
		echo '</select>';
	}


	public function guidance()

	{
		$step2 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'2'),'',array('category'=>'ASC'));
		$step3 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'3'),'',array('category'=>'ASC'));
		$step4 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'4'),'',array('category'=>'ASC'));
		$step5 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'5'),'',array('category'=>'ASC'));
		$step6 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'6'),'',array('category'=>'ASC'));
		$step7 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'7'),'',array('category'=>'ASC'));
		$step8 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'8'),'',array('category'=>'ASC'));

		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'add-task-guidance',
			'step2'=>$step2,
			'step3'=>$step3,
			'step4'=>$step4,
			'step5'=>$step5,
			'step6'=>$step6,
			'step7'=>$step7,
			'step8'=>$step8
		);


		$this->load->view('admin/admin-view',$data);

	}

	
	
	
	public function update_task(){
	
		$task = $_POST['task'];
		$task_id = $_POST['task_id'];
		
		$updt_arr = array(
			'question'=>$task
		);
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}
		
	
	public function subcategory(){
		
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('deleted'=>'0'));

		$data=array('page_title'=>'Add Organization','middle_content'=>'manage-task-category','category'=>$category);

		$this->load->view('admin/admin-view',$data);
	}


		
	
	public function update_category(){
	
		$category = $_POST['category'];
		$category_id = $_POST['category_id'];
		
		$updt_arr = array(
			'category_name'=>$category
		);
		if($this->master_model->updateRecord('cf_recall_steps_category',$updt_arr,array('id'=>$category_id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}
		
	
	public function select_new_category(){
	
		$category = $_POST['category'];
		$task_id = $_POST['task_id'];
		
		$updt_arr = array(
			'category'=>$category
		);
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			
			$sub_category = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$category));
			
			if (count($sub_category) > 0 ){
				echo $sub_category[0]['category_name'];
			} else{ 
				echo 'No Sub-Category';
			}
			
		}
		else{
			echo 'error';
		}
	}



	public function save_new_category(){
		$catt = $_POST['catt'];
		$step = $_POST['step'];
		$updt_arr = array(
			'category_name'=>$catt,
			'step_no'=>$step
		);
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '96';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if($this->master_model->insertRecord('cf_recall_steps_category',$updt_arr)){
			$this->session->set_flashdata('success','<span class="new_cat">'.$action_message['success'].'</span>');
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}
	}



	public function select_step(){
		
		$step_no = $_POST['step'];
		
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$step_no,'deleted'=>'0'));


		echo '<label class="control-label" for="select_subcat">Sub-Category</label>
		<select id="select_subcat" name="select_subcat" class="form-control">
		  <option value="0">Select</option>';
		  
		foreach($category as $r=>$cat){ 
			
			echo '<option value="';
			echo $cat['id'];
			echo '">';
			echo $cat['category_name'].'</option>';
		}
		echo '</select>';
	}


	public function toggle_task_enable(){
		$task_id = $_POST['id'];
		$disabled = $_POST['disabled'];
		
		if ($disabled == '0'){
			$disable = '1';
		}
		else{
			$disable = '0';
		}
		$updt_arr = array(
			'disabled'=>$disable
		);
		
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			echo 'success';
		}
		
	}
	public function save_sort(){
		$the_id = $_POST['ID'];
		$counter = 1;
		echo count($the_id);
		foreach($the_id as $id){
			$updt_arr = array(
				'arrangement'=>$counter
			);
			if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$id))){
				echo 'success';
			}
		$counter++;	
		}
		
	}

	public function save_new_task(){
		$task = $_POST['task'];
		$task_guidance = $_POST['task_guidance'];
		$step = $_POST['category'];
		$sub_cat = $_POST['sub_cat'];
		
		$updt_arr = array(
			'step_no'=>$step,
			'guidance'=>$task_guidance,
			'question'=>$task,
			'category'=>$sub_cat,
		);
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '81';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if($this->master_model->insertRecord('cf_recall_guidance',$updt_arr)){
			$this->session->set_flashdata('success', $action_message['success']);
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}
	}
	
	
	public function save_recall_pack(){
	
		$name = $_POST['name'];
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '81';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if($this->master_model->insertRecord('cf_recall_packs',array('name'=>$name),true)){
			$this->session->set_flashdata('success', $action_message['success']);
			echo 'success';
		}
		
	}
	
	public function delete_recall_pack(){
		$id = $_POST['id'];
		if($this->master_model->deleteRecord('cf_recall_packs','id',$id)){
			echo 'success';
		}
	
	}
	
	public function delete_recall_pack_cat(){
		$id = $_POST['id'];
		if($this->master_model->deleteRecord('cf_recall_packs_steps','id',$id)){
			echo 'success';
		}
	
	}
	
	
	public function add_guide(){
	
		$guidance = $_POST['guidance'];
		$task_id = $_POST['task_id'];
		
		$updt_arr = array(
			'guidance'=>$guidance
		);
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}








	public function check_dulication($org_name,$org_id)

	{

		$whr=array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num=$this->master_model->getRecordCount('organization_master',$whr);

		if($num==0)

		{return true;}

		else

		{return false;}

	}

	

	public function ajax_delete(){
		$id = $_POST['id'];
		$table = $_POST['table'];
		
		if ($table == 'cf_recall_steps_category'){
			if($this->master_model->updateRecord($table,array('deleted'=>'1'),array('id'=>$id))){
				
				echo 'success';
			} else{
				echo 'error';
			}
		}
		
		else if ($table == 'cf_recall_guidance'){
			if($this->master_model->deleteRecord($table,'id',$id)){
				echo 'success';
			} else{
				echo 'error';
			}
		}
		
		
	}

	/* delete organization */

	public function delete()

	{

		$org_id=$this->uri->segment(4);

		/* check documents present for organization if documents are present then unlink document first*/

		$documents=$this->master_model->getRecords('cf_file_upload',array('org_id'=>$org_id));

		if(count($documents)>0)

		{

			foreach($documents as $doc)

			{

				@unlink('uploads/crisis-document/'.$doc['file_upload_name']);

			}

		}

		

		/*select cc id assign for organization */

		$cc_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		

		$del_org="DELETE login,crt FROM organization_master as org 

			LEFT JOIN cf_crisis_response_team as crt ON crt.org_id=org.organization_id 

			LEFT JOIN cf_login_master as login ON login.login_id=crt.login_id 

			WHERE org.organization_id=".$org_id;

		

		if($this->db->query($del_org))

		{

			/* delete stakeholders */

			

			$del_stk="DELETE org,login,stakeholder FROM organization_master as org 

			LEFT JOIN cf_stakeholder as stakeholder ON stakeholder.org_id=org.organization_id 

			LEFT JOIN cf_login_master as login ON login.login_id=stakeholder.login_id 

			WHERE org.organization_id=".$org_id;

			$this->db->query($del_stk);

			

		}

		
		$this->session->set_flashdata('success','Organization successfully deleted.');
		redirect(base_url().'admin/organization/manage');

	}
	
	public function tasks_reviewed(){
		$nowtime = $this->common_model->userdatetime();
		$userid = $this->session->userdata('logged_cc_login_id');
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '60';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if($this->master_model->updateRecord('cf_crisis_response_team',array('tasks_review_date'=>$nowtime),array('login_id'=>$userid))){
			$this->session->set_flashdata('success', $action_message['success']);
			$this->session->set_userdata('task_review_date', $nowtime);
			echo 'success';
		}
		
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}
	}
	
	public function get_crts(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$the_crts = $this->common_model->get_org_crts($cc_id,$org_id, null, 'assign');

		// $arr = array();

		// if(!empty($the_crts)){
		// 	foreach($the_crts as $r=>$value){

		// 		$value['first_name'] = $this->master_model->decryptIt($value['crt_first_name']);
		// 		$value['last_name'] = $this->master_model->decryptIt($value['crt_last_name']);
		// 		$arr[] = $value;

		// 	}
		// }

		echo json_encode($the_crts);

	}
	public function get_templates(){
		$user_type = (isset($_GET['user_type'])) ? $_GET['user_type'] : 'cc';
		$org_id = (isset($_GET['org_id'])) ? $_GET['org_id'] : $this->session->userdata('cc_selected_orgnaization');
		
		$all = $this->common_model->format_templates($org_id, $user_type);

		echo json_encode($all);
	}
		
	
	public function header_tasks(){
		$incident_id = (isset($_GET['incident_id'])) ? $_GET['incident_id']: '';
		$template_id = (isset($_GET['template_id'])) ? $_GET['template_id']: '18';
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		//incident form
		if($incident_id == 'edit' && $template_id == 'form'){
			$contents = $this->master_model->getRecords('checklist_contents',array('cc_id'=>$cc_id),'*',array('sort'=>'ASC'));
			$arr = array();

			$arr_content = array();

			if(!empty($contents)){
				foreach($contents as $cr=>$cvalue){
					$whr = array(
						'content_id'=>$cvalue['id']
					);

					$email = $this->master_model->getRecords('content_emails', $whr);

					$cvalue['email'] = (!empty($email)) ? $email[0] : null;

					$sublist = $this->master_model->getRecords('content_fields', $whr);;
					$cvalue['field'] = $this->common_model->format_sublist($sublist);

					$cvalue['options'] = $this->master_model->getRecords('content_options', $whr);

					$arr_content[] = $cvalue;
				}
			}

			$arr[] = array(
				'step_no'=>'2',
				'tasks'=>array(
					array(
						'contents'=>$arr_content,
					)
				),
			);
			echo json_encode($arr); return;
		}

		$the_crts = $this->common_model->get_org_crts($cc_id,$org_id);
	
		//fetch incident date
		$incident = $this->master_model->getRecords('cf_recall', array('id'=>$incident_id));
				

		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$pack_id = (!empty($template_id)) ? $template_id : $this->common_model->pack_id();
		

		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id, 'deleted'=>'0'),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		$arr = array();

		if(count($packsteps)>0) {
			$i = 0;
			foreach($packsteps as $r => $value) {
				$step_no = $value['step_no'];

				$countqq = 0;
				$curr_sub_c = '';
				
				//get tasks
				$tasks = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id, 'date_deleted'=>'0000-00-00 00:00:00'),''); //,array('category'=>'ASC','arrangement'=>'ASC'));
				
				$value['tasks'] = array();
				$value['show'] = true;
			

				$todo = array();
				$in_progress = array();
				$donetask = array();
				
				if(!empty($tasks)){
					foreach($tasks as $tr=>$tvalue){
						//get answer of task
						$whr_ans = array(
							'incident_id'=>$incident_id,
							'task_id'=>$tvalue['id']
						);

						if(!empty($incident)){
							$tvalue['incident_date'] = $incident[0]['initiation_date'];
						}

						$task_answer = $this->common_model->task_answer_bool($whr_ans);
						$tvalue['checked'] = $task_answer;
						
						$task_answer_progress = $this->common_model->task_answer_progress($whr_ans);
						$tvalue['in_progress'] = $task_answer_progress['answer'];
						$tvalue['task_answer'] = $task_answer_progress['task_answer'];
						$tvalue['blocked'] = $task_answer_progress['blocked'];
						
						$tvalue['format_task_id'] = $this->common_model->format_task_id($tvalue['id']);
						
						$contents = $this->master_model->getRecords('checklist_contents',array('checklist_id'=>$tvalue['id'], 'module'=>'text'),'*',array('sort'=>'ASC'), '0', '1');

						$arr_content = array();

						//get assigned
						$tvalue['assigned'] = $this->common_model->format_crt($tvalue['assigned_id']);
						$tvalue['assigned_info'] = $this->common_model->format_crt($tvalue['assigned_id']);


						// $nowtime = date("m/d/y");
						$tvalue['due_date_format'] = $tvalue['due_date']; 
						$tvalue['due_date'] = ($tvalue['due_date']) ? date_format(date_create($tvalue['due_date']), 'm/d/y') : '';

						$tvalue['due_time'] = ($tvalue['due_time']) ? $tvalue['due_time'] : '7:00pm';
						

						if(!empty($contents)){
							foreach($contents as $cr=>$cvalue){
								$whr = array(
									'content_id'=>$cvalue['id']
								);

								$email = $this->master_model->getRecords('content_emails', $whr);

								$cvalue['email'] = (!empty($email)) ? $email[0] : null;

								$sublist = $this->master_model->getRecords('content_fields', $whr);;
								$cvalue['field'] = $this->common_model->format_sublist($sublist);

								$cvalue['options'] = $this->master_model->getRecords('content_options', $whr);

								$arr_content[] = $cvalue;
							}
						}
						else{
							$arr_content = array(
								array(
									'checklist_id'=>$tvalue['id'],
									'module'=>'text',
									'sort'=>0,
									'content'=>'',
									'id'=>'',
								)
							);
						}

						//comments
						$comments = $this->master_model->getRecords('recall_task_comments', array('recall_id'=>$incident_id, 'task_id'=>$tvalue['id']));

						$carr = array();
						if(!empty($comments)){
							foreach($comments as $cr=>$cvalue){
								$cvalue['created_at'] = date_format(date_create($cvalue['created_at']), 'M d, Y');
								$cvalue['author'] = $this->common_model->format_crt($cvalue['user_id']);
								$carr[] = $cvalue;
		
							}
						}
		
						$tvalue['comments'] = $carr;
						$tvalue['incident_id'] = $incident_id;
						
						$tvalue['contents'] = $arr_content;
						$value['tasks'][] = $tvalue;
						$value['incident_id'] = $incident_id;

					}
				}

				$arr[] = $value;

				$i++;
			}
		}

		echo json_encode($arr);
	}

	public function header_kanban(){
		$incident_id = (isset($_GET['incident_id'])) ? $_GET['incident_id'] : '';
		$template_id = (isset($_GET['template_id'])) ? $_GET['template_id'] : '18';
		
		$arr = $this->common_model->kanban_details($incident_id, $template_id);
		echo json_encode($arr);
	}


	//save by known name
	public function save_task_headers(){ //cf_recall_packs_steps
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$recall_pack_id = $_POST['recall_pack_id'];
		$step_no = $_POST['step_no'];
		$order = $_POST['order'];
		$name = $_POST['name'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'order'=>$order,
			'name'=>$name
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_packs_steps', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['recall_pack_id'] = $recall_pack_id;
			$arr['step_no'] = $step_no;
			$arr['date'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_packs_steps', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function add_incident_form(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$user_type = (isset($_POST['user_type'])) ? $_POST['user_type'] : 'cc';
		
		$name = $_POST['name'];
		$org_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('cc_selected_orgnaization');
		$user_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('user_id');

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'name'=>$name,
			'org_id'=>$org_id,
			'user_id'=>$user_id
		);

		if(!isset($_POST['id'])){
			$inci = $this->master_model->getRecords('incident_form', array('user_id'=>$user_id));
			if(count($inci)){
				$this->master_model->updateRecord('incident_form', $arr, array('id'=>$inci[0]['id']));
			}
			else{
				$arr['date_created'] = $nowtime;
				$id = $this->master_model->insertRecord('incident_form', $arr, true);
			}
			$arr['id'] = $id;
		}
		else{
			if(!empty($id)){
				$this->master_model->updateRecord('incident_form', $arr, array('id'=>$id));
			}
			else{
				$arr['date_created'] = $nowtime;
				$id = $this->master_model->insertRecord('incident_form', $arr, true);
			}
			$arr['id'] = $id;
		}


		echo json_encode($arr);
	}

	public function get_incident_title() {
		$user_type = (isset($_POST['user_type'])) ? $_POST['user_type'] : 'cc';
		
		$org_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('cc_selected_orgnaization');
		$user_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('user_id');
		$inci = $this->master_model->getRecords('incident_form', array('user_id'=>$user_id));
		echo json_encode($inci);
	}

	public function get_template_name() {
		$id = (isset($_GET['id'])) ? $_GET['id'] : '18';
		$data = $this->master_model->getRecords('cf_recall_packs', array('id'=>$id));
		echo json_encode($data);
	}

	public function delete_template() {
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$this->master_model->deleteRecord('cf_recall_packs','id',$id);
		echo json_encode($_POST);
	}

	public function save_template(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$user_type = (isset($_POST['user_type'])) ? $_POST['user_type'] : 'cc';
		
		$name = $_POST['name'];
		$org_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('cc_selected_orgnaization');
		$user_id = ($user_type == 'webmanager') ? '0' : $this->session->userdata('user_id');

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'name'=>$name,
			'org_id'=>$org_id,
			'user_id'=>$user_id
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_packs', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['date_created'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_packs', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);
	}

	public function delete_header(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			'deleted'=>'1',
			'date_deleted'=>$nowtime
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_packs_steps', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		echo json_encode($arr);

	}

	public function save_tasks(){ //cf_recall_guidance
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$wiki_id = (isset($_POST['wiki_id'])) ? $_POST['wiki_id'] : '0';
		$recall_pack_id = $_POST['recall_pack_id'];
		$arrangement = $_POST['arrangement'];
		$question = $_POST['question'];
		$step_no = $_POST['step_no'];
		$assigned_id = $_POST['assigned_id'];
		$months = ($_POST['months'] != 'undefined') ? $_POST['months'] : '';
		$days = ($_POST['days'] != 'undefined') ? $_POST['days'] : '';
		$hours = ($_POST['hours'] != 'undefined') ? $_POST['hours'] : '';
		$mins = ($_POST['mins'] != 'undefined') ? $_POST['mins'] : '';
		// $due_date = $_POST['due_date'];
		// $due_time = $_POST['due_time'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'arrangement'=>$arrangement,
			'assigned_id'=>$assigned_id,
			'months'=>$months,
			'days'=>$days,
			'hours'=>$hours,
			'mins'=>$mins,
			'wiki_id'=>$wiki_id,
			// 'due_date'=>$due_date,
			// 'due_time'=>$due_time,
			'question'=>$question,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_guidance', $arr, array('id'=>$id));
			$arr['id'] = $id;			
		}
		else{
			$arr['recall_pack_id'] = $recall_pack_id;
			$arr['step_no'] = $step_no;
			$arr['date_created'] = $nowtime;
			$id = $this->master_model->insertRecord('cf_recall_guidance', $arr, true);
			$arr['id'] = $id;
			$arr['format_task_id'] = $this->common_model->format_task_id($id);
		}

		$user_id = $this->session->userdata('logged_cc_login_id');

		if(!empty($user_id)){
			$this->master_model->updateRecord('cf_crisis_response_team',array('tasks_review_date'=>$nowtime),array('login_id'=>$user_id));
		}

		echo json_encode($arr);

	}

	public function delete_task(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			// 'deleted'=>'1',
			'date_deleted'=>$nowtime
		);

		if(!empty($id)){
			$this->master_model->updateRecord('cf_recall_guidance', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		echo json_encode($arr);

	}
	public function save_contents(){ //checklist_contents
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$recall_pack_id = '18';
		$checklist_id = $_POST['checklist_id'];
		$sort = $_POST['sort'];
		$content = $_POST['content'];
		$module = $_POST['module'];
		$user_id = $this->session->userdata('user_id');

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'cc_id'=>$user_id,
			'checklist_id'=>$checklist_id,
			'module'=>$module,
			'sort'=>$sort,
			'content'=>$content,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('checklist_contents', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('checklist_contents', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_fields(){ //save_fields
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$answer = (isset($_POST['answer'])) ? $_POST['answer'] : '';
		$text = $_POST['text'];
		$content_id = $_POST['content_id'];
		$type = $_POST['type'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'answer'=>$answer,
			'text'=>$text,
			'content_id'=>$content_id,
			'type'=>$type,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('content_fields', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('content_fields', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_options(){ //save_options
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$text = $_POST['text'];
		$content_id = $_POST['content_id'];
		$type = $_POST['type'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'text'=>$text,
			'content_id'=>$content_id,
			'type'=>$type,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('content_options', $arr, array('id'=>$id));
			$arr['id'] = $id;

		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('content_options', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function save_emails(){ //save_emails
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$content_id = $_POST['content_id'];
		$email = $_POST['email'];
		$cc = $_POST['cc'];
		$bcc = $_POST['bcc'];
		$subject = $_POST['subject'];
		$body = $_POST['body'];

		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'content_id'=>$content_id,
			'email'=>$email,
			'cc'=>$cc,
			'bcc'=>$bcc,
			'subject'=>$subject,
			'body'=>$body,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('content_emails', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('content_emails', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

	}

	public function delete_file(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id,
		);

		$this->master_model->deleteRecord('wiki_files','id',$id);
		
		echo json_encode($arr);
	}
	
	public function delete_content(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id,
		);

		$this->master_model->deleteRecord('checklist_contents','id',$id);
		
		echo json_encode($arr);
	}

	
	public function delete_page(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id,
		);

		$this->master_model->deleteRecord('wiki_pages','id',$id);
		
		echo json_encode($arr);
	}

	public function get_messages(){
		$format_messages = $this->common_model->format_messages();
		echo json_encode($format_messages);

	}

	public function group_messages_istyping(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');

		$nowtime = $this->common_model->userdatetime();

		$gm = $this->master_model->getRecords('group_messages_istyping', array('org_id'=>$org_id));
		
		$arr = array(
			'org_id'=>$org_id,
			'cc_id'=>$user_id,
		);

		if(!empty($gm)){
			$arr['date_updated'] = $nowtime;
			$this->master_model->updateRecord('group_messages_istyping', $arr, array('id'=>$gm[0]['id']));
		}
		else{
			$arr['date_added'] = $nowtime;
			$id = $this->master_model->insertRecord('group_messages_istyping', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);
	}

	public function get_istyping(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');

		$nowtime = $this->common_model->userdatetime();
		$unread_message_count = $this->common_model->unread_message_count();
		
		$gm = $this->master_model->getRecords('group_messages_istyping', array('org_id'=>$org_id));
		
		$arr = array();
		$arr['unread_message_count'] = $unread_message_count;
		
		if(!empty($gm)){
			if($gm[0]['cc_id'] != $user_id){
				$recall_initiated = new DateTime($gm[0]['date_updated']);
				
				$nowtime = $this->common_model->userdatetime();
				
				$recall_closed = new DateTime($nowtime);
				$difference = $recall_initiated->diff($recall_closed);
				
				$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';
	
				if($difference->i == 0){
					if($difference->s < 6){
						$arr['user'] = $this->common_model->format_crt($gm[0]['cc_id']);
						$arr['unread_message_count'] = $unread_message_count;


					}
				}
			}


		}
		
		if($unread_message_count > 0){
			$this->common_model->visited_messages($user_id);
		}

		echo json_encode($arr);
		

	}

	public function save_gmessage(){
		$message = $_POST['message'];
		$files = (isset($_POST['files'])) ? $_POST['files'] : array();
		
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');

		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			'message'=>$message,
			'org_id'=>$org_id,
			'user_id'=>$user_id,
			'created_at'=>$nowtime
		);

		$id = $this->master_model->insertRecord('group_messages', $arr, true);
		$arr['id'] = $id;
		$arr['date_format'] = date_format(date_create($arr['created_at']), 'm/d/Y');
		$arr['time_sent'] = date_format(date_create($arr['created_at']), 'h:i A');
		$arr['owner'] = 'me';
		$arr['author'] = $this->common_model->format_crt($arr['user_id']);
		$arr['files'] = $files;
		
		echo json_encode($arr);
		
	}

	public function get_pages(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
		
		$whr = array(
			'org_id'=>$org_id,
			'subparent_id'=>'0',
			'parent_id'=>'0'
		);
		$wiki_pages = $this->common_model->format_pages($whr);
		
		$arr = array();

		if(!empty($wiki_pages)){
			foreach($wiki_pages as $r=>$value){
				//include all history
				$whr2 = array(
					'parent_id'=>$value['id'],
				);
				$sort = array(
					'id'=>'DESC'
				);
				$wiki_pages2 = $this->common_model->format_pages($whr2, $sort);

				$value['histories'] = $wiki_pages2;


				//include subpages
				$whr3 = array(
					'subparent_id'=>$value['id'],
				);

				$wiki_pages3 = $this->common_model->format_pages($whr3);
				$w3 = array();

				if(!empty($wiki_pages3)){
					foreach($wiki_pages3 as $r3=>$value3){
						//include all history
						$whr2 = array(
							'parent_id'=>$value3['id'],
						);
						$sort = array(
							'id'=>'DESC'
						);
						$wiki_pages2 = $this->common_model->format_pages($whr2, $sort);

						$value3['histories'] = $wiki_pages2;
						$w3[] = $value3;
					}
				}

				$value['subpages'] = $w3;


				$arr[] = $value;
			}
		}

		echo json_encode($arr);

	}

	public function get_files(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$whr = array(
			'org_id'=>$org_id,
		);
		$wiki_files = $this->common_model->format_files($whr);

		echo json_encode($wiki_files);
	}

	public function logged_info(){
		$cc_id = $this->session->userdata('user_id');
		$cc_id = (!empty($cc_id)) ? $cc_id : $this->session->userdata('my_id');
		$format_crt = $this->common_model->format_crt($cc_id);
		echo json_encode($format_crt);

	}

	public function cancel_subscription(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$nowtime = $this->common_model->userdatetime();
		
		$arr['subscription_cancelled_date'] = $nowtime;
		$arr['subscription_cancelled'] = '1';
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$cc_id));
		echo json_encode($arr);
		
	}


	public function reactivate_subscription(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$nowtime = $this->common_model->userdatetime();
		
		// $arr['subscription_cancelled_date'] = $nowtime;
		$arr['subscription_cancelled'] = '0';
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$cc_id));
		echo json_encode($arr);
		
	}

	public function restore_history(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$parent_id = $_POST['parent_id'];
		$content = $_POST['content'];
		$title = $_POST['title'];

		$arr = array(
			'content'=>$content,
			'title'=>$title,
		);
		$this->master_model->updateRecord('wiki_pages', $arr, array('id'=>$parent_id));

		echo json_encode($arr);
	}

	public function save_page(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$comment = (isset($_POST['comment'])) ? $_POST['comment'] : '';
		$subparent_id = $_POST['subparent_id'];
		$content = $_POST['content'];
		$title = $_POST['title'];

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');


		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'subparent_id'=>$subparent_id,
			'content'=>$content,
			'created_at'=>$nowtime,
			'title'=>$title,
		);

		if(!empty($id)){
			$prev = $this->master_model->getRecords('wiki_pages', array('id'=>$id));
			//save old to new entry
			$old_arr = array(
				'content'=>$prev[0]['content'],
				'title'=>$prev[0]['title'],
				'created_at'=>$prev[0]['created_at'],
			);

			$old_arr['org_id'] = $org_id;
			$old_arr['parent_id'] = $id;
			$old_arr['user_id'] = $user_id;
			// $old_arr['created_at'] = $nowtime;
			$old_id = $this->master_model->insertRecord('wiki_pages', $old_arr, true);
			$old_arr['id'] = $old_id;

			if($comment){
				$comment_data = array(
					'message'=>$comment,
					'page_id'=>$old_id
				);
				$this->save_comment_api($comment_data);
			}

			$this->master_model->updateRecord('wiki_pages', $arr, array('id'=>$id));

			//get old
			$whr = array(
				'id'=>$old_id
			);
			$history = $this->common_model->format_pages($whr);
			$arr['history'] = (!empty($history)) ? $history[0] : null;
			$arr['created_at_time'] = date_format(date_create($arr['created_at']), 'M d, Y h:i');
			$arr['created_at'] = date_format(date_create($arr['created_at']), 'M d, Y');

		}
		else{
			$arr['org_id'] = $org_id;
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('wiki_pages', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

		// echo json_encode($_POST);
	}

	

	public function save_task_comment(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$message = $_POST['message'];
		$recall_id = $_POST['recall_id'];
		$task_id = $_POST['task_id'];
		
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');


		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'message'=>$message,
			'task_id'=>$task_id,
			'recall_id'=>$recall_id,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('recall_task_comments', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('recall_task_comments', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

		// echo json_encode($_POST);
	}
	public function save_comment(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$message = $_POST['message'];
		$page_id = $_POST['page_id'];

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');


		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'message'=>$message,
			'page_id'=>$page_id,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('wiki_comments', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('wiki_comments', $arr, true);
			$arr['id'] = $id;
		}

		echo json_encode($arr);

		// echo json_encode($_POST);
	}
	public function save_comment_api($data){
		$id = (isset($data['id'])) ? $data['id'] : '';
		$message = $data['message'];
		$page_id = $data['page_id'];

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$user_id = $this->session->userdata('user_id');


		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'message'=>$message,
			'page_id'=>$page_id,
		);

		if(!empty($id)){
			$this->master_model->updateRecord('wiki_comments', $arr, array('id'=>$id));
			$arr['id'] = $id;
		}
		else{
			$arr['user_id'] = $user_id;
			$arr['created_at'] = $nowtime;
			$id = $this->master_model->insertRecord('wiki_comments', $arr, true);
			$arr['id'] = $id;
		}
		return $arr;
	}

	public function get_timezone(){
		$time_zone = $this->master_model->getRecords('time_zone');
		echo json_encode($time_zone);

	}

	public function get_countries(){
		$countryformat = $this->common_model->countryformat();
		echo json_encode($countryformat);
	}

	public function save_timezone(){
		$data = $_POST;

		$arr = array(
			'timezone'=>$data['timezone']
		);

		$this->session->set_userdata($arr);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		
		echo json_encode($arr);

	}

	public function save_stripe_card(){
		$data = $_POST;
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$logged_id = $this->session->userdata('user_id');
		$nowtime = $this->common_model->userdatetime();


		$user_id = $this->session->userdata('logged_cc_login_id');
		$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$username = $this->common_model->getname($user_id);
		$email = $this->master_model->decryptIt($cc_info[0]['email_id']);
		$orgname = $this->common_model->getorgname($cc_info[0]['default_org']);
		$token = $data['token'];
		
		$data2 = array(
			'token'=>$token,
			'username'=>$username,
			'email'=>$email,
			'orgname'=>$orgname
		);

		//save card details to customer stripe table
		$arr = array(
			'card_info'=>serialize($data),
			'user_id'=>$cc_id,
			'logged_id'=>$logged_id,
			'org_id'=>$org_id
		);

		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; // (base_url() != 'https://www.crisisflo.com/') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';
		
		Stripe::setApiKey($apikey);
	
	
		try {
			// Create a Customer
			$customer = Stripe_Customer::create(array(
			  "description" => $orgname,
			  "email" => $email,
			  //"plan" => $subs_plan_id, //subscrib user to plan 
			  "card" => $token // obtained with Stripe.js
			));
			
			if($customer->id){
				$login_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
			
				$cc_arr = array(
					'stripe_customer_id'=>$customer->id,
					'user_type'=>'live'
				);
				
				if($login_info[0]['trial_end'] == '0000-00-00 00:00:00'){
					$nowtime = date("Y-m-d h:i:s");
					$cc_arr['trial_end'] = $nowtime;
				}
				
				$this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$user_id));


				//save card details to customer stripe table
				$arr['stripe_id'] = $customer->id;
				$id = $this->master_model->insertRecord('stripe_customers', $arr, true);
				$arr['id'] = $id;

				$data2['result'] = 'success';
			}
			
		} catch(Stripe_CardError $e) {
		  // The card has been declined
			//$this->session->set_flashdata('error','The card has been declined.');
			$data2['result'] = 'declined';
		}		
		

		echo json_encode($arr);
	}

	public function save_form_payment(){
		$data = $_POST;
		$data['number'] = (isset($data['number'])) ? $this->master_model->encryptIt($data['number']) : '';

		$arr = array(
			'form_info'=>serialize($data),
		);
		$this->master_model->updateRecord('stripe_customers', $arr, array('id'=>$data['id']));
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$format_billing = $this->common_model->format_billing($cc_id);

		$arr['format_billing'] = $format_billing;
		echo json_encode($arr);
	}


	public function get_submitted_incident(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$incident_form_fields = $this->master_model->getRecords('incident_form_fields', array('cc_id'=>$cc_id, 'answer'=>'Open'));

		$formatted = $this->common_model->format_incident_form($incident_form_fields);

		echo json_encode($formatted);
	}

	public function get_submitted_incident_history(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$incident_form_fields = $this->master_model->getRecords('incident_form_fields', array('cc_id'=>$cc_id, 'answer !='=>'Open'));

		$formatted = $this->common_model->format_incident_form($incident_form_fields);

		echo json_encode($formatted);
	}

	public function archive_incident(){
		$incident_id = $_POST['incident_id'];
		$this->master_model->updateRecord('incident_form_fields', array('answer'=>'Archived'), array('id'=>$incident_id));
		echo json_encode($_POST);

	}

	public function comment_incident(){
		$incident_id = $_POST['incident_id'];
		$comment = $_POST['module'];
		$this->master_model->updateRecord('incident_form_fields', array('module'=>$comment), array('id'=>$incident_id));
		echo json_encode($_POST);
	}

	public function get_incidents(){
		$incident_id = (isset($_GET['incident_id'])) ? $_GET['incident_id']: '';
		$incident_type = (isset($_GET['incident_type'])) ? $_GET['incident_type']: 'dashboard';
		$incident_status = ($incident_type == 'log') ? '1' : '0';
		
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		//paginate recall config
		$where_recall = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'date_deleted'=>'0000-00-00 00:00:00',
			'incident_status'=>$incident_status
		);

		if(!empty($incident_id)){
			$where_recall = array(
				'id'=>$incident_id
			);
		}
		
		$settings = array();
		

		$arr = $this->common_model->format_incident($where_recall);
		$arr = (!empty($incident_id) && $incident_id != 'edit') ? $arr[0] : $arr;
		
		echo json_encode($arr);
	}

	public function save_incident(){

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$description = $_POST['description'];
		$template_id = $_POST['template_id'];
		$generate_incident_id = $this->common_model->generate_incident_id();
		$format_inci_id = $this->common_model->format_inci_id($generate_incident_id);
		
		//timezone datetime
		$nowtime = $this->common_model->userdatetime();
		$r_pack_id = '18';

		if($template_id == '0'){
			$template_arr = array(
				'user_id'=>$cc_id,
				'org_id'=>$org_id,
				'name'=>$description.' template',
				'date_created'=>$nowtime
			);
			$template_id = $this->master_model->insertRecord('cf_recall_packs',$template_arr,true);
		}

		//insert scenario log 
		$recall_arr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'template_id'=>$template_id,
			'incident_no'=>$format_inci_id,
			'cc_incident_id'=>$generate_incident_id,
			'description'=>$description,
			'incident_status'=>'0',
			'initiation_date'=>$nowtime,
			'input_pack_id'=>$r_pack_id,
			// 'incident_date'=>$inci_date,
			// 'incident_name'=>$inci_name,
			// 'simulation_id'=>$simulation_modules,
			// 'affected_location'=>json_encode($affected_loc),
		);

		$incident_id = $this->master_model->insertRecord('cf_recall',$recall_arr,true);
		$incident = $this->common_model->format_incident(array('id'=>$incident_id));
		
		echo json_encode($incident[0]);
		
	}		

	public function save_task_answer(){
		$data = $_POST;
		$nowtime = $this->common_model->userdatetime();
		
		$data['created_at'] = $nowtime;
		$this->master_model->insertRecord('recall_task_answers', $data);

		echo json_encode($data);
		
	}		

	public function save_task_block(){
		$data = $_POST;
		$nowtime = $this->common_model->userdatetime();
		
		$data['created_at'] = $nowtime;
		$this->master_model->insertRecord('recall_task_answers', $data);

		echo json_encode($data);
		
	}

	public function update_incident_status(){
		$status = $_POST['status'];
		$incident_id = $_POST['incident_id'];
		$nowtime = $this->common_model->userdatetime();
		
		$arr = array(
			'incident_status'=>$status,
			'closed_date'=>$nowtime
		);

		$this->master_model->updateRecord('cf_recall', $arr, array('id'=>$incident_id));
		echo json_encode($arr);

	}

	public function get_billing_info(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$format_billing = $this->common_model->format_billing($cc_id);
		echo json_encode($format_billing);
	}

	public function get_billing_contact(){
		$cc_id = $this->session->userdata('logged_cc_login_id');
		$billing_contact = $this->common_model->get_billing_contact($cc_id);
		echo json_encode($billing_contact);
	}

	public function get_transactions(){
		$user_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');

		$whr = array(
			'org_id'=>$org_id
		);
		
		$format_transaction_history = $this->common_model->format_transaction_history($whr);

		$billing = $this->common_model->single_payment($user_id, 'preview');


		
		$whr = array(
			'default_org'=>$org_id,
		);
		$all_crt = $this->master_model->getRecords('cf_login_master', $whr);

		$users_count = array(
			'users'=>count($all_crt),
			'percent'=>round(count($all_crt)/5 * 100, 0)
		);

		$trial_days = $this->common_model->trial_days($user_id);

		$arr = array(
			'billing'=>$billing,
			'users_count'=>$users_count,
			'trial_days'=>$trial_days,
			'history'=>$format_transaction_history
		);
		echo json_encode($arr);

	}

	public function save_billing_contact(){
		$data = $_POST;

		$arr = array(
			'billing_contact'=>serialize($data)
		);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		echo json_encode($arr);
	}

	public function run_billing(){
		$user_id = $this->session->userdata('logged_cc_login_id');
		$billing = $this->common_model->single_payment($user_id, 'preview');

		$cc_info = null;
		$total_bill = 0;
		$billing_date = $billing['next_month'];
		$billing_details = $billing;

		$orgs = $billing['orgs'];
		if(!empty($orgs)){
			foreach($orgs as $r=>$value){
				$cc_info = $value['cc_info'];
				$total_bill = $value['total_bill'];
			}
		}
		
		$stripe = $this->common_model->paynow_single($cc_info, $total_bill, $billing_date, $billing_details);

		echo json_encode($stripe);
		
	}

	public function save_name(){
		$data = $_POST;

		$arr = array(
			'crt_first_name'=>$this->master_model->encryptIt($data['first_name']),
			'crt_last_name'=>$this->master_model->encryptIt($data['last_name'])
		);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		echo json_encode($arr);

	}

	public function save_password(){
		$data = $_POST;

		$u = $this->master_model->getRecords('cf_login_master', array('login_id'=>$data['login_id'], 'pass_word'=>md5($data['current'])));

		$arr = array(
			'pass_word'=>md5($data['password'])
		);
		
		if(!empty($u)){
			$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$data['login_id']));
		}

		$arr['count'] = count($u);

		echo json_encode($arr);
	}

	public function save_uinfo(){
		$data = $_POST;

		if($data['type'] == 'Postion'){
			$arr = array(
				'crt_position'=>$data['crt_position']
			);
		}
		else if($data['type'] == 'Location'){
			$arr = array(
				'location'=>$data['location'],
				'location_lat'=>$data['location_lat'],
				'location_lng'=>$data['location_lng'],
			);
		}
		else if($data['type'] == 'Mobile Number'){
			$mob = $data['countrycode'].$data['crt_digits'];
			$arr = array(
				'countrycode'=>$this->master_model->encryptIt($data['countrycode']),
				'crt_digits'=>$this->master_model->encryptIt($data['crt_digits']),
				'crt_mobile'=>$this->master_model->encryptIt($mob),
			);
		}
		else{
			$arr = array(
				'crisis_function'=>$data['crisis_function']
			);
		}
		
		
		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));

		echo json_encode($arr);
	}

	public function save_email(){
		$data = $_POST;

		$u = $this->master_model->getRecords('cf_login_master', array('login_id'=>$data['login_id'], 'pass_word'=>md5($data['current'])));

		$arr = array(
			'email_id'=>$this->master_model->encryptIt($data['crt_email']),
		);
		
		if(!empty($u)){
			$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$data['login_id']));

			$arr = array(
				'crt_email'=>$this->master_model->encryptIt($data['crt_email']),
			);
			$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$data['login_id']));
		}

		$arr['count'] = count($u);

		echo json_encode($arr);
	}


	public function save_gm_files(){
		$data = $_POST;

		$arr = array(
			'message_id'=>$data['message_id']
		);

		$this->master_model->updateRecord('group_messages_files', $arr, array('id'=>$data['id']));
		echo json_encode($data);
	}

	public function upload_gmessage_file(){

		if ( !empty( $_FILES ) ) {
			$uploads_dir = 'uploads/gmessage-files/';
			$file_name = $_FILES[ 'file' ][ 'name' ];
			$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
			$uploadPath = 'uploads/wiki-files/';//dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];

			move_uploaded_file( $tempPath, $uploads_dir.$file_name );


			$org_id = $this->session->userdata('cc_selected_orgnaization');
			$cc_id = $this->session->userdata('user_id');
			$nowtime = $this->common_model->userdatetime();

			$arr = array(
				'file_name'=>$file_name,
				'org_id'=>$org_id,
				'cc_id'=>$cc_id,
				'date_added'=>$nowtime,
			);

			$id = $this->master_model->insertRecord('group_messages_files', $arr, true);
			// $arr['id'] = $id;

			$the_file = $this->common_model->format_gm_files(array('id'=>$id));
			$the_file = (!empty($the_file)) ? $the_file[0] : null;

			$answer = array( 'answer' => 'File transfer completed', 'file'=>$the_file );
			$json = json_encode( $answer );

			echo $json;

		} else {

			echo 'No files';

		}
	}
	

	public function upload_file(){

		if ( !empty( $_FILES ) ) {
			$uploads_dir = 'uploads/wiki-files/';
			$file_name = $_FILES[ 'file' ][ 'name' ];
			$file_size = $_FILES[ 'file' ][ 'size' ];
			$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
			$uploadPath = 'uploads/wiki-files/';//dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];

			move_uploaded_file( $tempPath, $uploads_dir.$file_name );


			$org_id = $this->session->userdata('cc_selected_orgnaization');
			$cc_id = $this->session->userdata('user_id');
			$nowtime = $this->common_model->userdatetime();

			$arr = array(
				'file_name'=>$file_name,
				'file_size'=>$file_size,
				'org_id'=>$org_id,
				'cc_id'=>$cc_id,
				'date_added'=>$nowtime,
			);

			$id = $this->master_model->insertRecord('wiki_files', $arr, true);
			// $arr['id'] = $id;

			$the_file = $this->common_model->format_files(array('id'=>$id));
			$the_file = (!empty($the_file)) ? $the_file[0] : null;

			$answer = array( 'answer' => 'File transfer completed', 'file'=>$the_file );
			$json = json_encode( $answer );

			echo $json;

		} else {

			echo 'No files';

		}
	}

	public function show_country_code() {
		$counrty_code = $_POST['country_short'];
		
		$mobile_code = $this->master_model->getRecords('country_t',array('iso2'=>$counrty_code));

		$data['calling_code'] = '';
		$data['country_id'] = '';
		if(count($mobile_code) > 0){
			//echo $mobile_code[0]['calling_code'];
			$data['calling_code'] = $mobile_code[0]['calling_code'];
			$data['country_id'] = $mobile_code[0]['country_id'];
		}

		echo json_encode($data);
	}

	public function get_server_time(){
		$t = $this->common_model->userdatetime();
		$data['time'] = $t;
		
		echo json_encode($data);
	}

	public function send_content_email(){
		$email = $_POST['email'];
		$subject = $_POST['subject'];
		$body = $_POST['body'];
		
		$admin_email_sender = $this->common_model->admin_email_sender();

		$info_arr = array(
			'from'=>$admin_email_sender,
			'to'=>$email,
			'subject'=>$subject,
			'view'=>'task_email_sending'
		);

		$other_info = array(
			'body'=>$body
		);

		$arr['info_arr'] = $info_arr;
		$arr['other_info'] = $other_info;
		
		// $this->load->view('email/task_email_sending', $other_info);
		$this->email_sending->sendmail($info_arr,$other_info);

		echo json_encode($arr);
	}

	public function delete_incident(){
		$post = $_POST;
		$nowtime = $this->common_model->userdatetime();

		$arr = array(
			'date_deleted'=>$nowtime
		);

		$this->master_model->updateRecord('cf_recall', $arr, array('id'=>$post['id']));
		echo json_encode($arr);
	}
}

?>
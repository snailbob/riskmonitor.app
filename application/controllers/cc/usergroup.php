<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Usergroup extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->library('cc_check_session');

		$this->cc_check_session->checksessionvalue();

		

	}

	

	//------------------------------------add crt

	public function add()

	{

		if(isset($_POST['btn_add_group']))

		{

			$this->form_validation->set_rules('group_name','User Group Name','required');

			$this->form_validation->set_message('required', 'Mandatory field');

				if($this->form_validation->run()) {

					//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
					$action_message_id = '81';
					$action_message = $this->common_model->get_message($action_message_id);

					$group_name = $this->input->post('group_name');
					$access = ($this->input->post('access')) ? $this->input->post('access') : array();

					$input_array = array(
						'group_name'=>$group_name,
						'access'=>serialize($access),
						'cc_id'=>$this->session->userdata('logged_cc_login_id'),
						'org_id'=>$this->session->userdata('cc_selected_orgnaization')
					);
					
//					echo json_encode($input_array);
//					return false;

					if($this->master_model->insertRecord('user_group',$input_array)) {


						$this->session->set_flashdata('success', $action_message['success']);

						redirect(base_url().'cc/usergroup/managegroup/');

					}

				}

		}
		
		$countriescode = $this->master_model->getRecords('country_t');
		

		$data = array(
			'page_title'=>'Add Crisis Response Team Member',
			'error'=>'',
			'middle_content'=>'add-group',
			'success'=>'',
			'countriescode'=>$countriescode
		);

		$this->load->view('cc/cc-view',$data);

	}

	

	

	//-----------------------------------------manage user group

	public function managegroup(){

		$whr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'));


		$all_group=$this->master_model->getRecords('user_group',$whr);



		$data=array('page_title'=>"Manage User Group",'middle_content'=>'manage-group','success'=>'',

		'error'=>'','all_group'=>$all_group);

		$this->load->view('cc/cc-view',$data);

	}

	//-----------------------------------------Delete crt

	public function delete(){

		$data['success']=$data['error']="";

		$group_id = $this->uri->segment(4);

		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);
		

		if($this->master_model->deleteRecord('user_group','id',$group_id)){	

			$this->session->set_flashdata('success', $action_message['success']);

			redirect(base_url()."cc/usergroup/managegroup/");

		}

		else

		{

			$this->session->set_flashdata('error', $action_message['error']);

			redirect(base_url()."cc/crisisteam/managecrt/");

		}

		

	}

	

	//------------------------------------Update crt

	public function update(){

		$group_id=$this->uri->segment(4);

		if(isset($_POST['btn_add_group'])){


			//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
			$action_message_id = '69';
			$action_message = $this->common_model->get_message($action_message_id);

			$this->form_validation->set_rules('group_name','User Group Name','required');

			$this->form_validation->set_message('required', 'Mandatory field');
			
			if($this->form_validation->run()){

				$group_name = $this->input->post('group_name');
				$access = ($this->input->post('access')) ? $this->input->post('access') : array();
			
				$input_array = array(
					'group_name'=>$group_name,
					'access'=>serialize($access),
					'cc_id'=>$this->session->userdata('logged_cc_login_id')
				);

				if($this->master_model->updateRecord('user_group',$input_array,array('id'=>$group_id))){

					$this->session->set_flashdata('success', $action_message['success']);

					redirect(base_url().'cc/usergroup/managegroup/');

				}

				else{

					$this->session->set_flashdata('error', $action_message['error']);

					redirect(base_url().'cc/usergroup/managegroup/');

				}

			}

		}
		$group_record=$this->master_model->getRecords('user_group',array('id'=>$group_id));

		$data=array('page_title'=>'Update Group','error'=>'','middle_content'=>'update-group','success'=>'','group_record'=>$group_record);

		$this->load->view('cc/cc-view',$data);

	}

	
	public function details() {

		$login_id = $this->uri->segment(4);

		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'details-crt',
			'success'=>'',
			'crt_record'=>$crt_record
		);
		
		$this->load->view('cc/cc-view',$data);

	}

}

?>
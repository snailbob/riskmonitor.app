<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Wiki extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('cc_check_session');
		$this->cc_check_session->checksessionvalue();

	}

	

	public function index(){

		$data=array(
			'page_title'=>'Wiki Space',
			'middle_content'=>'wiki_view',
			'error'=>'',
			'success'=>''
		);
	
		$this->load->view('cc/cc-view',$data);
	}

	public function sharing(){
		$data=array(
			'page_title'=>'Document Sharing',
			'middle_content'=>'wiki_sharing_view',
			'error'=>'',
			'success'=>''
		);
		$this->load->view('cc/cc-view',$data);
	}


}

?>
<?php

class Customer extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	
	}


	public function testCrypt()

	{
		
		
		/*$str = 'apple';
		
		
		//echo md5('Nga Nguyen');
		*/
		$input = "ZjHV1D8sDGcwWi35UGtk6ceKkc7TSY19te79lqcPZtc=";
		
		$encrypted = $this->master_model->encryptIt($input);
		$decrypted = $this->master_model->decryptIt($encrypted);
		
		echo '<h1> Encrypted: '. $encrypted .'<br>';
		echo 'Decrypted: '. $decrypted .'<br></h1>';
		
		
		
	}
	
	public function request()

	{
		
		$reseller_info = $this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));
		$tabs = $reseller_info[0]['deactivated_tab'];
		
		$single_tab = explode(' ',$tabs);

		
		if (!in_array("request", $single_tab)){
			redirect('consultant');
		}
		

		if(isset($_POST['add_orgzz']))

		{

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_rules('sel_cc','Coordinator','required|xss_clean');

			if($this->form_validation->run())

			{

				$organization_name=$this->input->post('cc_org',true);	

				$sel_cc=$this->input->post('sel_cc',true);
				
				$modules=$this->input->post('modules',true);
				
				$cust_type=$this->input->post('cust_type',true);

				$packs = $this->input->post('input_packs',true);

				$themods = "";
				foreach($modules as $check) {
					$themods .= $check; 
				}		

				$id_email = explode(' ', $sel_cc);

				$insrt_arr=array(
					'organization_name'=>$organization_name,
					'cc_id'=>$id_email[0],
					'input_pack_id'=>$packs,
					'cust_type'=>$cust_type,
					'reseller_id'=>$_SESSION['reseller_id'],
					'active_module'=>$themods
				);
				
				if($org_id = $this->master_model->insertRecord('organization_master',$insrt_arr, true))

				{
					
					//update org id in input_packs table
					$this->master_model->updateRecord('input_packs',array('org_id'=>$org_id),array('id'=>$packs));
					
					
					//update org_id and cc_id for each inputs
					if ($packs != '0'){
						
						$updt_arr = array(
							'org_id'=>$org_id,
							'cc_id'=>$id_email[0]
						);
						
						$this->master_model->updateRecord('cf_file_upload',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_task',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_scenario',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_standby_messages',$updt_arr,array('input_pack_id'=>$packs));
					}



						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$id_email[1],'from'=>$admin_email[0]['info_email'],
		
						'subject'=>'CrisisFlo Access',
		
						'view'=>'registration-mail-to-cc-org');
						
						$other_info=array(
						  'first_name'=>$id_email[2],
	
						  'last_name'=>$id_email[3]);
						
						$this->email_sending->sendmail($info_arr,$other_info);
	
						//create text file for organization chat room
						$room_name = 'chat_room/org';
						$room_name .= $org_id;
						$room_name .= '.txt';
						
						$myfile = fopen($room_name, "w");	
						

					$this->session->set_flashdata('success','Organization added successfully');

					redirect(base_url().'consultant/customer/pending'); 

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding request');

					redirect(base_url().'consultant/customer/pending');

				}

			}

		}

		
		
		
		
		
		
		if(isset($_POST['add_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('cc_email','Email','required|xss_clean|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			

			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$cc_email=$this->input->post('cc_email',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				$cc_org=$this->input->post('cc_org',true);

				$modules=$this->input->post('modules',true);

				$cust_type=$this->input->post('cust_type',true);

				$onetime_key=md5(microtime());



				$themods = "";
				foreach($modules as $check) {
					$themods .= $check; 
				}		

				$login_arr=array(
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'user_type'=>$cc_type,
					'user_level'=>'0',
					'onetime_key'=>$onetime_key,
					'user_status'=>'0'
				);



				//filter duplicate email
				$resellers=$this->master_model->getRecords('cf_login_master');
				
				foreach ($resellers as $r=>$value){
					if ($this->master_model->encryptIt($cc_email) == $value['email_id']){
						
						$this->session->set_flashdata('error','Coordinator\'s email already exist in the database.');

						redirect(base_url().'consultant/customer/request');
					}
				}



				if($login_id=$this->master_model->insertRecord('cf_login_master',$login_arr,TRUE))

				{
					//insert data to organization_master
					$org_arr=array(
						'organization_name'=>$cc_org,
						'active_module'=>$themods,
						'cust_type'=>$cust_type,
						'reseller_id'=>$_SESSION['reseller_id'],
						'cc_id'=>$login_id
					);
					
					$org_id=$this->master_model->insertRecord('organization_master',$org_arr,TRUE);
					
					//add default org to cc
					$this->master_model->updateRecord('cf_login_master',array('default_org'=>$org_id),array('login_id'=>$login_id));



					$cc_arr=array(
						'login_id'=>$login_id,
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_organisation'=>$cc_org,
						'crt_email'=>$this->master_model->encryptIt($cc_email),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code),
						'cc_id'=>$login_id,
						'activated'=>'no',
						'reseller_id'=>$_SESSION['reseller_id'],
						'org_id'=>0
					);

					//create text file for organization chat room
					$room_name = 'chat_room/org';
					$room_name .= $org_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	


					if($this->master_model->insertRecord('cf_crisis_response_team',$cc_arr))

					{

						//$this->session->set_flashdata('success','CC added successfully');

						/*$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],

						'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

						

						$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,

						'email'=>$cc_email);

						

						$this->email_sending->sendmail($info_arr,$other_info);*/

						

						$this->session->set_flashdata('success','CC added successfully.');

						redirect(base_url().'consultant/customer/pending');

						

					}

					else

					{

						$this->session->set_flashdata('error','Error while adding CC');

						redirect(base_url().'consultant/customer/pending');

					}

				}

			}

		}


		$whr_arr=array('login.user_level'=>'0','reseller_id'=>$_SESSION['reseller_id']);//'login.user_status'=>'1',

		$order_arr=array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str="cc.login_id,crt_first_name,crt_last_name,crt_email";

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list=$this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		$modules=$this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
		
		$countriescode=$this->master_model->getRecords('country_t');

		$input_packs=$this->master_model->getRecords('input_packs',array('org_id'=>'0'));

		$data=array('page_title'=>'Setup CC','middle_content'=>'add-cc','countriescode'=>$countriescode,'input_packs'=>$input_packs,'cc_list'=>$cc_list,'modules'=>$modules);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function manage()

	{

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$all_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0','cc.reseller_id'=>$_SESSION['reseller_id']));

		$data=array('page_title'=>'Manage CC','middle_content'=>'manage-cc','all_cc'=>$all_cc);

		$this->load->view('consultant/admin-view',$data);

	}

	
	

	public function activated()

	{
		$reseller_info = $this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));
		$tabs = $reseller_info[0]['deactivated_tab'];
		
		$single_tab = explode(' ',$tabs);

		
		if (!in_array("activated", $single_tab)){
			redirect('consultant');
		}
		
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$all_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0','cc.reseller_id'=>$_SESSION['reseller_id'],'cc.activated'=>'yes'));

		$data=array('page_title'=>'Manage CC','middle_content'=>'manage-cc-activated','all_cc'=>$all_cc);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function pending()

	{
		$reseller_info = $this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));
		$tabs = $reseller_info[0]['deactivated_tab'];
		
		$single_tab = explode(' ',$tabs);

		
		if (!in_array("request", $single_tab)){
			redirect('consultant');
		}
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$all_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0','cc.reseller_id'=>$_SESSION['reseller_id'],'cc.activated'=>'no'));

		$data=array('page_title'=>'Manage CC','middle_content'=>'manage-cc-pending','all_cc'=>$all_cc);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function update()

	{

		$cc_id=$this->uri->segment(4);

		if(isset($_POST['add_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			

			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				

				

				$login_arr=array('user_type'=>$cc_type);

				if($login_id=$this->master_model->updateRecord('cf_login_master',$login_arr,array('login_id'=>$cc_id)))

				{

					$cc_arr=array(
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code)
					);

							

					if($this->master_model->updateRecord('cf_crisis_response_team',$cc_arr,array('login_id'=>$cc_id)))

					{

						$this->session->set_flashdata('success','CC updated successfully');

						redirect(base_url().'consultant/customer/update/'.$cc_id);

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating CC');

						redirect(base_url().'consultant/customer/update/'.$cc_id);

					}

				}

			}

		}

		

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');

		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Update CC','middle_content'=>'update-cc','cc_info'=>$cc_info,'countriescode'=>$countriescode);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function details()

	{

		$cc_id=$this->uri->segment(4);

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

//		$cc_info=$this->master_model->getRecords('cf_coordinator_master as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		

		$data=array('page_title'=>'Details CC','middle_content'=>'details-cc','cc_info'=>$cc_info);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function delete()

	{

		$cc_id=$this->uri->segment(4);

		if($this->master_model->deleteRecord('cf_login_master','login_id',$cc_id))

		{

			if($this->master_model->deleteRecord('cf_crisis_response_team','cc_id',$cc_id))

			{

				$this->session->set_flashdata('success','CC successfully deleted.');

				redirect(base_url().'consultant/customer/manage');

			}

		}

		else

		{

			$this->session->set_flashdata('success','Error Deleting CC.');

			redirect(base_url().'consultant/customer/manage');

		}

	}

}

?>
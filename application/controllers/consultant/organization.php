<?php 

class Organization extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}

	

	public function add()

	{

		if(isset($_POST['add_orgz']))

		{

			$this->form_validation->set_rules('organization_name','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			if($this->form_validation->run())

			{

				$organization_name=$this->input->post('organization_name',true);	

				$sel_cc=$this->input->post('sel_cc',true);
				
				$modules=$this->input->post('modules',true);
				
				$cust_type=$this->input->post('cust_type',true);

				$packs = $this->input->post('input_packs',true);

				$themods = "";
				foreach($modules as $check) {
					$themods .= $check; 
				}		

				$id_email = explode(' ', $sel_cc);

				$insrt_arr=array(
					'organization_name'=>$organization_name,
					'cc_id'=>$id_email[0],
					'input_pack_id'=>$packs,
					'cust_type'=>$cust_type,
					'reseller_id'=>$_SESSION['reseller_id'],
					'active_module'=>$themods
				);
				
				if($org_id = $this->master_model->insertRecord('organization_master',$insrt_arr, true))

				{
					
					//update org id in input_packs table
					$this->master_model->updateRecord('input_packs',array('org_id'=>$org_id),array('id'=>$packs));
					
					
					//update org_id and cc_id for each inputs
					if ($packs != '0'){
						
						$updt_arr = array(
							'org_id'=>$org_id,
							'cc_id'=>$id_email[0]
						);
						
						$this->master_model->updateRecord('cf_file_upload',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_task',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_scenario',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_standby_messages',$updt_arr,array('input_pack_id'=>$packs));
					}



						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$id_email[1],'from'=>$admin_email[0]['info_email'],
		
						'subject'=>'CrisisFlo Access',
		
						'view'=>'registration-mail-to-cc-org');
						
						$other_info=array(
						  'first_name'=>$id_email[2],
	
						  'last_name'=>$id_email[3]);
						
						$this->email_sending->sendmail($info_arr,$other_info);
	
						//create text file for organization chat room
						$room_name = 'chat_room/org';
						$room_name .= $org_id;
						$room_name .= '.txt';
						
						$myfile = fopen($room_name, "w");	
						
//					/**script to send email after confirmation*/
//					$whr=array('id'=>'1');
//
//					$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');
//
//						
//					$info_arr=array('from'=>$adminemail[0]['recovery_email'],
//	
//					'to'=>$id_email[1],
//	
//					'subject'=>'CrisisFlo Access',
//	
//					'view'=>'registration-mail-to-cc-org');
//					
//					$other_info=array(
//					  'first_name'=>$id_email[2],
//
//					  'last_name'=>$id_email[3]);
//					
//					$this->email_sending->sendmail($info_arr,$other_info);
//					/**.script to send email after confirmation*/
					
					

					$this->session->set_flashdata('success','Orgnization added successfully');

					redirect(base_url().'consultant/organization/manage'); 

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding Orgnization');

					redirect(base_url().'consultant/organization/manage');

				}

			}

		}

		

		$whr_arr=array('login.user_level'=>'0','reseller_id'=>$_SESSION['reseller_id']);//'login.user_status'=>'1',

		$order_arr=array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str="cc.login_id,crt_first_name,crt_last_name,crt_email";

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list=$this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		$input_packs=$this->master_model->getRecords('input_packs',array('org_id'=>'0'));

		$modules=$this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
		

		$data=array('page_title'=>'Add Organization','middle_content'=>'add-organization','cc_list'=>$cc_list,'input_packs'=>$input_packs,'modules'=>$modules);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function manage()

	{

		$order_arr=array('org.organization_name'=>'ASC');

		$sel_str="cc.crt_first_name,cc.crt_last_name,org.*";

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=org.cc_id');

		$org_list=$this->master_model->getRecords('organization_master as org',array('org.reseller_id'=>$_SESSION['reseller_id']),$sel_str,$order_arr);

		

		$data=array('page_title'=>'Manage Organization','middle_content'=>'manage-organization','org_list'=>$org_list);

		$this->load->view('consultant/admin-view',$data);

	}

	

	public function check_dulication($org_name,$org_id)

	{

		$whr=array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num=$this->master_model->getRecordCount('organization_master',$whr);

		if($num==0)

		{return true;}

		else

		{return false;}

	}

	

	public function update()

	{

		$org_id=$this->uri->segment(4);

		

		if(isset($_POST['add_orgz']))

		{

			$this->form_validation->set_rules('organization_name','Organization','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');
			
			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			//$this->form_validation->set_rules('input_packs','Input Pack','required|xss_clean');

			

			if($this->form_validation->run())

			{

				$organization_name=$this->input->post('organization_name',true);	

				$sel_cc=$this->input->post('sel_cc',true);
				
				$modules=$this->input->post('modules',true);
				
				$cust_type=$this->input->post('cust_type',true);
				
				$packs = $this->input->post('input_packs',true);

				$old_packs = $this->input->post('old_packs',true);

				$themods = "";
				foreach($modules as $check) {
					$themods .= $check; 
				}		

				
				

				if($this->check_dulication($organization_name,$org_id))

				{

					$updt_arr=array(
						'organization_name'=>$organization_name,
						'cc_id'=>$sel_cc,
						'input_pack_id'=>$packs,
						'cust_type'=>$cust_type,
						'active_module'=>$themods
					);
					
					
					//if ($org_id !='1'){
//						//remove assignment to other org
//						$this->master_model->updateRecord('organization_master',array('input_pack_id'=>'0'),array('input_pack_id'=>$old_packs,'organization_id !='=>$org_id));
//					}
					
					
					if($this->master_model->updateRecord('organization_master',$updt_arr,array('organization_id'=>$org_id)))

					{
						
						
						$updt_old_pack = array(
							'org_id'=>'1',
							'cc_id'=>'0'
						);

						//update org_id and cc_id to each inputs
						if ($packs != '0'){
							
							$updt_pack = array(
								'org_id'=>$org_id,
								'cc_id'=>$sel_cc
							);
							
							
							if ($old_packs != '0'){
								//reset old selected pack
								$this->master_model->updateRecord('cf_file_upload',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_task',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_scenario',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_standby_messages',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
							}
						
							
							//assign new pack
							$this->master_model->updateRecord('cf_file_upload',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_task',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_pack,array('input_pack_id'=>$packs));
						}
						
						//update packs with no assignment
						if ($old_packs != '0' && $packs == '0'){
							//reset old selected pack
							$this->master_model->updateRecord('cf_file_upload',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_task',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
						}
					
						
						/*if ($packs == '0'){
							
							$updt_pack = array(
								'org_id'=>'1',
								'cc_id'=>'0'
							);
							
							$this->master_model->updateRecord('cf_file_upload',$updt_pack,array('input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_task',$updt_pack,array('input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_pack,array('input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_pack,array('input_pack_id'=>$old_packs));
							
						}*/
						
						//update org id in input_packs table
						$this->master_model->updateRecord('input_packs',array('org_id'=>'0'),array('org_id'=>$org_id));
						$this->master_model->updateRecord('input_packs',array('org_id'=>$org_id),array('id'=>$packs));
						$this->master_model->updateRecord('organization_master',array('input_pack_id'=>'0'),array('input_pack_id'=>$old_packs,'organization_id !='=>$org_id));
	
							
							

						$this->session->set_flashdata('success','Organization updated successfully');

						redirect(base_url().'consultant/organization/update/'.$org_id);

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating Organization');

						redirect(base_url().'consultant/organization/update/'.$org_id);

					}

				}

				else

				{

					$this->session->set_flashdata('error','Organization already exists');

					redirect(base_url().'consultant/organization/update/'.$org_id);

				}

			}

		}

		

		$org_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		

		$whr_arr=array(/*'login.user_status'=>'1',*/'login.user_level'=>'0');

		$order_arr=array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str="cc.login_id,crt_first_name,crt_last_name";

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list=$this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		$input_packs=$this->master_model->getRecords('input_packs');


		$data=array(
			'page_title'=>'Add Organization',
			'middle_content'=>'update-organization',
			'cc_list'=>$cc_list,
			'org_info'=>$org_info,
			'input_packs'=>$input_packs
		);

		$this->load->view('consultant/admin-view',$data);

	}

	

	/* delete organization */

	public function delete()

	{

		$org_id=$this->uri->segment(4);

		/* check documents present for organization if documents are present then unlink document first*/

		$documents=$this->master_model->getRecords('cf_file_upload',array('org_id'=>$org_id));

		if(count($documents)>0)

		{

			foreach($documents as $doc)

			{

				@unlink('uploads/crisis-document/'.$doc['file_upload_name']);

			}

		}

		

		/*select cc id assign for organization */

		$cc_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		

		$del_org="DELETE login,crt FROM organization_master as org 

			LEFT JOIN cf_crisis_response_team as crt ON crt.org_id=org.organization_id 

			LEFT JOIN cf_login_master as login ON login.login_id=crt.login_id 

			WHERE org.organization_id=".$org_id;

		

		if($this->db->query($del_org))

		{

			/* delete stakeholders */

			

			$del_stk="DELETE org,login,stakeholder FROM organization_master as org 

			LEFT JOIN cf_stakeholder as stakeholder ON stakeholder.org_id=org.organization_id 

			LEFT JOIN cf_login_master as login ON login.login_id=stakeholder.login_id 

			WHERE org.organization_id=".$org_id;

			$this->db->query($del_stk);

			

		}

		
		$this->session->set_flashdata('success','Organization successfully deleted.');
		redirect(base_url().'consultant/organization/manage');

	}

	

}

?>
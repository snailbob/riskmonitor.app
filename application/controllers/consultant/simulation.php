<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Simulation extends CI_Controller 

{

	public function __construct(){

		parent::__construct();

		$this->load->library('upload');

	}



//-------------- Function for updating timezone ----------------------
	public function index(){
		$simulation_modules = $this->master_model->getRecords('simulation_modules');
		$consultant_id = $_SESSION['reseller_id'];

		if(isset($_POST['name'])){
			$name = $_POST['name'];
			$id = $_POST['id'];
			
			$arr = array(
				'name'=>$name,
				'consultant_id'=>$consultant_id
			);
			
			
			if($id == ''){
				$this->session->set_flashdata('success', 'Simulation module successfully added.');
				$arr['date_added'] = $this->common_model->usertime('UP10');
				$id = $this->master_model->insertRecord('simulation_modules', $arr, true);
				$data['action'] = 'add';
				$data['id'] = $id;
			}
			else{
				$this->session->set_flashdata('success', 'Simulation module successfully updated.');
				$this->master_model->updateRecord('simulation_modules', $arr, array('id'=>$id));
				$data['action'] = 'update';
				$data['id'] = $id;
			}

			echo json_encode($data);
			return false;
		}
		
		$data = array(
			'middle_content'=>'manage-simulation_modules',
			'page_title'=>'Simulation',
			'simulation_modules'=>$simulation_modules
		);	
	
		$this->load->view('consultant/admin-view',$data);
	}
	
	
//-------------- Function for updating timezone ----------------------
	public function module(){
		$simu_id = $this->uri->segment(4);
		$simulation_mod = $this->master_model->getRecords('simulation_modules', array('id'=>$simu_id));
		$simulations = $this->master_model->getRecords('simulation', array('module_id'=>$simu_id));
		
		if(count($simulation_mod) == 0){
			$this->session->set_flashdata('error', 'No simulation module found.');
			redirect('consultant/simulation');
		}
		
		$data = array(
			'middle_content'=>'manage-simulation',
			'page_title'=>'Simulation',
			'simulations'=>$simulations
		);	
	
		$this->load->view('consultant/admin-view',$data);
	}


//-------------- Function for updating timezone ----------------------
	public function manage(){
		$simu_mod_id = $this->uri->segment(4);
		$simu_id = $this->uri->segment(5);
		$simu_text = 'Add';
		$consultant_id = $_SESSION['reseller_id'];
		$uniquetime = time().mt_rand();

		$steps = $this->master_model->getRecords('cf_recall_packs_steps', array('recall_pack_id'=>'18'), '*', array('order'=>'ASC'));
		$simulation_count = $this->master_model->getRecordCount('simulation');
		$name = 'New Input'; // '.($simulation_count + 1);
		$the_content = '';
		$the_trigger = '';
		$the_step = '';
		$the_thedate = '';
		$attached_media = array();
		
		
		if($simu_id != ''){
			$the_simu = $this->master_model->getRecords('simulation', array('id'=>$simu_id));
			if(count($the_simu) > 0){
				$simu_text = 'Update';
				$name = $the_simu[0]['name'];
				$the_content = $the_simu[0]['content'];
				$the_trigger = $the_simu[0]['trigger'];
				$the_step = $the_simu[0]['step_id'];
				$the_thedate = $the_simu[0]['trigger_date'];
				
				$attached_media = $this->master_model->getRecords('private_message_file',array('simulation_id'=>$simu_id));
			}
			else{
				$this->session->set_flashdata('error', 'No simulation found.');
				redirect('consultant/simulation');
			}
			
		}
		
		
		if(isset($_POST['content'])){
			$content =  $_POST['content'];
			$simulation_name = $_POST['simulation_name'];
			$simulation_id = $_POST['simulation_id'];
			$module_id = $_POST['module_id'];
			$utime = $_POST['utime'];
			$trigger = $_POST['trigger'];
			$steps = $_POST['steps'];
			$mediadate = $_POST['mediadate'];
	
			$arr = array(
				'content'=>$content,
				'name'=>$simulation_name,
				'module_id'=>$module_id,
				'consultant_id'=>$consultant_id,
				'trigger'=>$trigger,
				'step_id'=>$steps,
				'trigger_date'=>$mediadate
			);
			
			if($simulation_id == ''){
				$arr['date_added'] = $this->common_model->usertime('UP10');
				$simulation_id = $this->master_model->insertRecord('simulation', $arr, true);
				$data['action'] = 'add';
				$data['id'] = $simulation_id;
			}
			else{
				$this->master_model->updateRecord('simulation', $arr, array('id'=>$simulation_id));
				$data['action'] = 'update';
				$data['id'] = $simulation_id;
			}
			//update attached file id
			$this->master_model->updateRecord('private_message_file',array('simulation_id'=>$simulation_id), array('temp_id'=>$utime));
		
			
			echo json_encode($data);
			return false;
		}
		
		$data = array(
			'middle_content'=>'add-simulation',
			'page_title'=>'Simulation',
			'simu_text'=>$simu_text,
			'name'=>$name,
			'simu_id'=>$simu_id,
			'simu_mod_id'=>$simu_mod_id,
			'content'=>$the_content,
			'steps'=>$steps,
			'uniquetime'=>$uniquetime,
			'attached_media'=>$attached_media,
			'the_trigger'=>$the_trigger,
			'the_step'=>$the_step,
			'the_thedate'=>$the_thedate
		);	
	
		$this->load->view('consultant/admin-view',$data);
	}
	
	

	public function upload(){
	
		$output_dir = "uploads/simulations/";

		if(isset($_FILES["myfile"]))
		{


			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0){
				
			  echo 'Error Uploading File!';
			  
			}
			
			else{
				
				$file_ext = substr($_FILES["myfile"]["name"], -3, 3);
				$filetype = $_POST['filetype'];
				
				$data['filetype'] = $filetype;
				$data['filext'] = $file_ext;
				
				$data['result'] = 'success';
				
				if($filetype == 'video'){
					if($file_ext != 'mp4' && $file_ext != 'mov' && $file_ext != 'wmv' && $file_ext != 'avi'){
						$data['result'] = 'video_invalid';
						echo json_encode($data);
						return false;
					}
					
				}
				else{
					if($file_ext != 'mp3' && $file_ext != 'wav'){
						$data['result'] = 'audio_invalid';
						echo json_encode($data);
						return false;
					}
					
				}
				
				
				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);
								
				
				$insrt_arr = array (
					'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
					'cc_id'=>'0',
					'org_id'=>'0',
					'temp_id'=>$_POST['uniquetime'],
					'message_id'=>'0',
					'cost_item_id'=>'0',
					'simulation_id'=>'0' //$cc_case_id
				);					
				if ($file_id = $this->master_model->insertRecord('private_message_file',$insrt_arr,true)){
					
					$data['file_url'] = base_url().$output_dir.$thefilename;
					$data['filename'] = $_FILES["myfile"]["name"];
					$data['file_id'] = $file_id;
				
				}
				
				
				echo json_encode($data);
					
			}
		
		}		
	}		
	
	
	
	
	public function deletemedia(){
		
		$thenum = $_POST['id'];

	    $file = $this->master_model->getRecords('private_message_file',array('file_upload_id'=>$thenum));
		$file_name = $file[0]['file_upload_name'];
		
		if($this->master_model->deleteRecord('private_message_file','file_upload_id',$thenum))

		{

			@unlink('uploads/simulations/'.$file_name);
			echo 'success';

		}

	
	}
	
	
	function delete(){
		$id = $_POST['id'];
		$this->master_model->deleteRecord('simulation','id',$id);
		$this->master_model->deleteRecord('private_message_file','simulation_id',$id);
		echo 'success';
	}
	
	
	function delete_mod(){
		$id = $_POST['id'];
		$this->master_model->deleteRecord('simulation_modules','id',$id);
		echo 'success';
	}
	
	
	
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Availability extends CI_Controller 

{

	public function __construct(){

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

	}

	


	public function index(){
		
		$login_id = $this->session->userdata('logged_crt_login_id'); 

		$crt = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
		$available = $crt[0]['crt_availability'];
		
		$sched_count = $this->master_model->getRecords('schedules', '', '*', array('id'=>'DESC'));
		
		$last_id = 1;
		if(count($sched_count) > 0){
			$last_id = $sched_count[0]['id'] + 1;
		}
		

		
		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'availability-view',
			'success'=>'',
			'available'=>$available,
			'last_id'=>$last_id
		);

		$this->load->view('crt/crt-view',$data);

	}
	
	
	public function update_availability(){
		$availability = $_POST['the_val'];

		$login_id = $this->session->userdata('logged_crt_login_id'); 

		$this->master_model->updateRecord('cf_crisis_response_team', array('crt_availability'=> $availability), array('login_id'=>$login_id));
		
		echo 'yeah';
		
	}
	
	
	public function update_schedule(){
		
		//sched_id=10&newstart_date=2015-04-09T17%3A33%3A00&newend_date=2015-04-09T18%3A03%3A00
		$sched_id = $_POST['sched_id'];
		$newstart_date = $_POST['newstart_date'];
		$newend_date = $_POST['newend_date'];
		
		$login_id = $this->session->userdata('logged_crt_login_id');
		
		
		$the_schedules = array();
		
		$arr = array(
			'start'=>$newstart_date,
			'end'=>$newend_date,
			'login_id'=>$login_id
		);
		
		$the_schedules[] = $arr;
		
		$this->master_model->updateRecord('schedules', $arr, array('id'=>$sched_id));
		
		$test['the_schedules'] = $the_schedules;

		echo json_encode($test);
		
		
	}
	
	public function add_time_schedule(){
		//daystart_time=5%3A30+PM&today_day=2015-03-31&dayend_time=6%3A30+PM&start_time_format=17%3A30&end_time_format=18%3A30
		
		if(isset($_POST['start_time'])){
			$today_day = $_POST['start_time'];
			$today_endday = $_POST['end_time'];
			
		}
		else{
		
			$today_day = $_POST['today_day'];
			$today_endday = $_POST['today_day'];
			$start_time_format = $_POST['start_time_format'];
			$end_time_format = $_POST['end_time_format'];
			
			
			if($end_time_format != ''){
				$today_endday = $today_day.'T'.$end_time_format.':00';
			}
	
			if($start_time_format != ''){
				$today_day = $today_day.'T'.$start_time_format.':00';
			}
			
		
		}
										
		
		$login_id = $this->session->userdata('logged_crt_login_id');
		$scheds = $this->master_model->getRecords('schedules', array('login_id'=>$login_id), '*', array('recur_id'=>'DESC'));
		
		$nowtime = $this->common_model->userdatetime();
		
		//get last id
		$recur_id =  count($scheds) > 0 ? $scheds[0]['recur_id'] + 1 : 1;
		
		$the_schedules = array();
		
		$arr = array(
			'start'=>$today_day,
			'end'=>$today_endday,
			'login_id'=>$login_id,
			'recur_id'=>$recur_id,
			'date_added'=>$nowtime
		);
		
		$the_schedules[] = $arr;
		
		$id = $this->master_model->insertRecord('schedules', $arr, true);
		
		$test['id'] = $id;
		
		$test['the_schedules'] = $the_schedules;
		
		$this->session->set_flashdata('success', 'Available schedule successfully added.');
		
		echo json_encode($test);
		
	}
	
	
	
	public function add_schedule(){
	

//start_time=&end_time=&duration=30&recur_radios=monthly&daily_set_radios=1&daily_every_day=1&monthly_set_radios=1&monthly_every_day=&monthly_day_months=&monthly_week_no=1&monthly_days_settings=everyday&monthly_the_months=&yearly_every_day=&yearly_on_month=1&yearly_day=&yearly_week_no=1&yearly_days_settings=everyday&yearly_onthe_month=1&weekly_recure=1&days_in_week%5B%5D=5&start_date=Fri+4%2F10%2F2015&end_radios=1&number_of_occurence=1&end_date=Fri+4%2F10%2F2015&start_time_format=&end_time_format=&start_date_format=2015-04-10&end_date_format=2015-04-10

		$start_time = $_POST['start_time'];
		$end_time = $_POST['end_time'];
		$duration = $_POST['duration'];
		$recur_radios = $_POST['recur_radios'];
		$daily_set_radios = $_POST['daily_set_radios'];
		$monthly_set_radios = $_POST['monthly_set_radios'];
		$yearly_set_radios = $_POST['yearly_set_radios'];
		$daily_every_day = $_POST['daily_every_day'];
		$monthly_every_day = $_POST['monthly_every_day'];
		$yearly_every_day = $_POST['yearly_every_day'];
		$monthly_day_months = $_POST['monthly_day_months'];
		$monthly_the_months = $_POST['monthly_the_months'];
		$monthly_week_no = $_POST['monthly_week_no'];
		$monthly_days_settings = $_POST['monthly_days_settings'];
		$yearly_on_month = $_POST['yearly_on_month'];
		$yearly_day = $_POST['yearly_day'];
		$yearly_week_no = $_POST['yearly_week_no'];
		$yearly_days_settings = $_POST['yearly_days_settings'];
		$yearly_onthe_month = $_POST['yearly_onthe_month'];
		$weekly_recure = $_POST['weekly_recure'];
		$yearly_days_settings = $_POST['yearly_days_settings'];
		$days_in_week = $_POST['days_in_week'];
		$start_date = $_POST['start_date'];
		$end_radios = $_POST['end_radios'];
		$number_of_occurence = $_POST['number_of_occurence'];
		$end_date = $_POST['end_date'];


		$start_time_format = $_POST['start_time_format'];
		$end_time_format = $_POST['end_time_format'];
		
		$start_date_format = $_POST['start_date_format'];
		$end_date_format = $start_date_format;
		
		$the_end_date_format = $_POST['end_date_format'];
		
		
		$concat_startdate_time = $start_date_format.' 00:00';
		$concat_enddate_time = $end_date_format.' 00:00';
		
		
		if($start_time_format != ''){
			$concat_startdate_time = $start_date_format.' '.$start_time_format;
			$start_date_format = $start_date_format.'T'.$start_time_format.':00';
		}
		
		if($end_time_format != ''){
			$concat_enddate_time = $end_date_format.' '.$end_time_format;
			$end_date_format = $end_date_format.'T'.$end_time_format.':00';
		}
		
		
		$login_id = $this->session->userdata('logged_crt_login_id');
		$scheds = $this->master_model->getRecords('schedules', array('login_id'=>$login_id), '*', array('recur_id'=>'DESC'));
		
		$nowtime = $this->common_model->userdatetime();
		
		//get last id
		$recur_id =  count($scheds) > 0 ? $scheds[0]['recur_id'] + 1 : 1;

		

		//$days_in_year = 365;
		
		//get remaining days from the start day
		$current_year = date('Y');
		$endyr_date = new DateTime($current_year.'-12-31 23:59');
		$strt_date = new DateTime($start_date_format);
		$eend_date = new DateTime($the_end_date_format);
		$diff = $strt_date->diff($endyr_date);
		$diff2 = $strt_date->diff($eend_date);
		
		$remaining_days = $diff->y * 365 + $diff->m * 30 + $diff->d + $diff->h/24; // + $diff->i / 60;
		$remaining_endby_days = $diff2->y * 365 + $diff2->m * 30 + $diff2->d + $diff2->h/24; // + ($diff2->i / 60)/24;
		
		$test['recur_radios'] = $recur_radios;
		$test['remaining_days'] = $remaining_days;
		$test['end_radios'] = $end_radios;
		$test['concat_startdate_time'] = $concat_startdate_time;
		$test['start_date_format'] = $start_date_format;
		
		
		
		
		
		$the_schedules = array();
		if($recur_radios == 'daily'){ //if daily
		
			$until = ($remaining_days/$daily_every_day);
			$test['until'] = $until;
		
			if($daily_set_radios == '1'){ //if x days
			
				
				$test['recurrence_type'] = 'if x days';
				
				if ($end_radios == '1'){ //if until the end of year
					$test['range_type'] = 'loop until remaining days of the year';
					for($x = 0; $x < $until; $x++){ //loop until remaining days of the year
						
						$arr = array(
							'start'=>$start_date_format,
							'end'=>$end_date_format,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							'date_added'=>$nowtime
						);
						$the_schedules[] = $arr;
						
						$this->master_model->insertRecord('schedules', $arr);
						
						
						//change date variables						
						$start_added_date_time = strtotime($concat_startdate_time.' + '.$daily_every_day.' DAYS');
						$end_added_date_time = strtotime($concat_enddate_time.' + '.$daily_every_day.' DAYS');
		
						$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
						$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);

						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);

						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
						
						
					} //loop until remaining days of the year				
				
				} //if until the end of year
				
				else if ($end_radios == '2'){ //if End after x occurence
				
					$test['range_type'] = 'End after x occurence';
					for($x = 0; $x < $number_of_occurence; $x++){ //loop until remaining days of the year
						
						$arr = array(
							'start'=>$start_date_format,
							'end'=>$end_date_format,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							'date_added'=>$nowtime
						);
						$the_schedules[] = $arr;
						
						$this->master_model->insertRecord('schedules', $arr);
						
						
						//change date variables						
						$start_added_date_time = strtotime($concat_startdate_time.' + '.$daily_every_day.' DAYS');
						$end_added_date_time = strtotime($concat_enddate_time.' + '.$daily_every_day.' DAYS');
		
						$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
						$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);

						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);

						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
						
						
					} //loop until remaining days of the year				
								
				
				}//if End after x occurence
				
				else if ($end_radios == '3'){ //End by date

					$test['remaining_endby_days'] = $remaining_endby_days;
					$test['range_type'] = 'End by date';
					for($x = 0; $x < $remaining_endby_days; $x++){ //loop until remaining days of the year
						
						$arr = array(
							'start'=>$start_date_format,
							'end'=>$end_date_format,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							//'date_added'=>$nowtime
						);
						$the_schedules[] = $arr;
						
						$this->master_model->insertRecord('schedules', $arr);
						
						
						//change date variables						
						$start_added_date_time = strtotime($concat_startdate_time.' + '.$daily_every_day.' DAYS');
						$end_added_date_time = strtotime($concat_enddate_time.' + '.$daily_every_day.' DAYS');
		
						$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
						$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);

						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);

						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
						
						
					} //loop until remaining days of the year				
								
								
				
				
				} //End by date
				
				
			}//x of days
			
		
			else{ //if weekdays
				$test['recurrence_type'] = 'if weekdays';
				
				if ($end_radios == '1'){ //if until the end of year
					$test['range_type'] = 'loop until remaining days of the year';
				
					for($x = 0; $x < $until; $x++){ //loop until remaining days of the year
						$test['range_type'] = 'loop until remaining days of the year';
						$weekday_id = date('w', strtotime($concat_startdate_time));
						
						if($weekday_id != 0 && $weekday_id != 6){ //if not sunday and saturday 
							
							$arr = array(
								'start'=>$start_date_format,
								'end'=>$end_date_format,
								'login_id'=>$login_id,
								'recur_id'=>$recur_id,
								'date_added'=>$nowtime
							);
							$the_schedules[] = $arr;
							
							$this->master_model->insertRecord('schedules', $arr);
						
						}
	
						//change date variables						
						$start_added_date_time = strtotime($concat_startdate_time.' + '.$daily_every_day.' DAYS');
						$end_added_date_time = strtotime($concat_enddate_time.' + '.$daily_every_day.' DAYS');
		
						$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
						$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);
	
						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);
	
						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
	
					}					
				
				
				} //if until the end of year
				
				else if ($end_radios == '2'){ //if End after x occurence

					$test['range_type'] = 'if End after x occurence';
				
					for($x = 0; $x < $number_of_occurence; $x++){ //loop until remaining days of the year
						$test['range_type'] = 'loop until remaining days of the year';
						$weekday_id = date('w', strtotime($concat_startdate_time));
						
						if($weekday_id != 0 && $weekday_id != 6){ //if not sunday and saturday 
							
							$arr = array(
								'start'=>$start_date_format,
								'end'=>$end_date_format,
								'login_id'=>$login_id,
								'recur_id'=>$recur_id,
								'date_added'=>$nowtime
							);
							$the_schedules[] = $arr;
							
							$this->master_model->insertRecord('schedules', $arr);
						
						}
	
						//change date variables						
						$start_added_date_time = strtotime($concat_startdate_time.' + '.$daily_every_day.' DAYS');
						$end_added_date_time = strtotime($concat_enddate_time.' + '.$daily_every_day.' DAYS');
		
						$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
						$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);
	
						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);
	
						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
	
					}					
				
				}//end after x occurence
				else if ($end_radios == '3'){ //End by date

					$test['range_type'] = 'End by date';
				
					for($x = 0; $x < $remaining_endby_days; $x++){ //loop until remaining days of the year
						$test['range_type'] = 'loop until remaining days of the year';
						$weekday_id = date('w', strtotime($concat_startdate_time));
						
						if($weekday_id != 0 && $weekday_id != 6){ //if not sunday and saturday 
							
							$arr = array(
								'start'=>$start_date_format,
								'end'=>$end_date_format,
								'login_id'=>$login_id,
								'recur_id'=>$recur_id,
								'date_added'=>$nowtime
							);
							$the_schedules[] = $arr;
							
							$this->master_model->insertRecord('schedules', $arr);
						
						}
	
						//change date variables						
						$start_added_date_time = strtotime($concat_startdate_time.' + '.$daily_every_day.' DAYS');
						$end_added_date_time = strtotime($concat_enddate_time.' + '.$daily_every_day.' DAYS');
		
						$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
						$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);
	
						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);
	
						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
	
					}		
					
				} //End by date
				
				
			}//if weekdays
			
					
		}//if daily
		
		else if($recur_radios == 'weekly'){ //if weekly
			//$weekly_recure = $weekly_recure * 7;
			$until = ($remaining_days/$weekly_recure);
			$test['weekly_recure'] = $weekly_recure;
			$test['until'] = $until;
			$test['days_in_week'] = $days_in_week; //weekday
			$test['recurrence_type'] = 'if weekly';
					
			if ($end_radios == '1'){ //if until the end of year
				$test['range_type'] = 'loop until remaining days of the year';
				$weekly_looper = $remaining_days;
			} //if until the end of year

			else if ($end_radios == '2'){ //if End after x occurence
				$test['range_type'] = 'End after x occurence';
				$weekly_looper = $number_of_occurence;
			} //if End after x occurence
			
			else if ($end_radios == '3'){ //End by date
				$test['range_type'] = 'End by date';
				$weekly_looper = $remaining_endby_days;
			} //End by date
			
			$test['weekly_looper'] = $weekly_looper;

			//apply recurrence
			$day_after_day = 1;
			$current_week_number = date('W', strtotime($concat_startdate_time));
			$curr_week_number = 0;
			$test['week_number_count'] = array(); 
			
			$total_stored_sched = 0;
			for($x = 0; $x < $remaining_days; $x++){ //loop until remaining days of the year
				$weekday_id = date('w', strtotime($concat_startdate_time));
				$new_week_number = date('W', strtotime($concat_startdate_time));
				
				$test['week_number_count'][] = $new_week_number;
				
				
				if(($curr_week_number % $weekly_recure) == 0){ //modulus for weekly recur
					if(in_array($weekday_id, $days_in_week)){
					
						$arr = array(
							'start'=>$start_date_format,
							'end'=>$end_date_format,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							//'date_added'=>$nowtime
						);
						$the_schedules[] = $arr;
						$this->master_model->insertRecord('schedules', $arr);
					}
				}


				//update number of week
				if($current_week_number != $new_week_number){
					$current_week_number = $new_week_number;
					$curr_week_number++;
				}
				
				
				//change date variables						
				$start_added_date_time = strtotime($concat_startdate_time.' + '.$day_after_day.' DAYS');
				$end_added_date_time = strtotime($concat_enddate_time.' + '.$day_after_day.' DAYS');

				$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
				$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);

				$start_time_format = substr($concat_startdate_time, -5);
				$end_time_format = substr($concat_enddate_time, -5);
				$start_date_format = substr($concat_startdate_time, 0, 10);
				$end_date_format = substr($concat_enddate_time, 0, 10);

				$start_date_format = $start_date_format.'T'.$start_time_format.':00';
				$end_date_format = $end_date_format.'T'.$end_time_format.':00';
			
				//end if limit recurence
				if ($end_radios == '2'){ //if End after x occurence
					if($total_stored_sched == $number_of_occurence){
						
						$test['the_schedules'] = $the_schedules;
						$this->session->set_flashdata('success','Available schedule successfully added.');
						echo json_encode($test);
						return false;
					}
				} //if End after x occurence
				
				else if ($end_radios == '3'){ //End by date
				
					$strt_dateweek2 = new DateTime($start_date_format);
					$eend_dateweek2 = new DateTime($the_end_date_format);
					$diffweek = $strt_dateweek2->diff($eend_dateweek2);
					
					$remaining_days_of_week = $diffweek->y * 365 + $diffweek->m * 30 + $diffweek->d + $diffweek->h/24; // + $diff->i / 60;
						
					if($remaining_days_of_week < $total_stored_sched){
					//if($start_date_format == $the_end_date_format){
						
						$test['the_schedules'] = $the_schedules;
						$this->session->set_flashdata('success','Available schedule successfully added.');
						echo json_encode($test);
						return false;
					}

				} //End by date
			
			
				//increment stored scheds
				if(($curr_week_number % $weekly_recure) == 0){ //modulus for weekly recur
					if(in_array($weekday_id, $days_in_week)){
						$total_stored_sched++;
					}
				}
			
			
			
			}//end for loop

		
		
		}//if weekdays
		
				
		
		else if($recur_radios == 'monthly'){ //if Monthly


			if ($end_radios == '1'){ //if until the end of year
				$test['range_type'] = 'loop until remaining days of the year';
				$monthly_looper = $remaining_days;
			} //if until the end of year

			else if ($end_radios == '2'){ //if End after x occurence
				$test['range_type'] = 'End after x occurence';
				$monthly_looper = $number_of_occurence;
			} //if End after x occurence
			
			else if ($end_radios == '3'){ //End by date
				$test['range_type'] = 'End by date';
				$monthly_looper = $remaining_endby_days;
			} //End by date
			
			$test['monthly_looper'] = $monthly_looper;

			$total_stored_sched = 0;
			//start loop
			if($monthly_set_radios == '1'){
				$test['monthly_set_radios'] = $monthly_set_radios;
				$test['monthly_every_day'] = $monthly_every_day;
				
				$month_number = idate('n', strtotime($concat_startdate_time)); //Numeric representation of a month, without leading zeros
				$test['month_number'] = $month_number;
				
				$the_remaining_months = array();
				$count_of_rem_month = 0;
				for($month_number; $month_number <= 12; $month_number++){

					if(($count_of_rem_month % $monthly_day_months) == 0){ //apply monthly recur

						//change date variables		
						$formatted_month_num = sprintf("%02d", $month_number);				

						$concat_year = substr($concat_startdate_time, 0, 5);
						$concat_new_month = $formatted_month_num;
						$concat_starttime = substr($concat_startdate_time, 7);
						
						$concat_endtime = substr($concat_enddate_time, 7);
						
						
						$concat_startdate_time = $concat_year.$concat_new_month.$concat_starttime;
						
						$concat_enddate_time = $concat_year.$concat_new_month.$concat_endtime;

						$start_time_format = substr($concat_startdate_time, -5);
						$end_time_format = substr($concat_enddate_time, -5);
						$start_date_format = substr($concat_startdate_time, 0, 10);
						$end_date_format = substr($concat_enddate_time, 0, 10);

						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
						$end_date_format = $end_date_format.'T'.$end_time_format.':00';
						
						
						$arr = array(
							'start'=>$start_date_format,
							'end'=>$end_date_format,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							'date_added'=>$nowtime
						);
						$the_schedules[] = $arr;
						$total_stored_sched++;
						
						$this->master_model->insertRecord('schedules', $arr);
						
						$the_remaining_months[] = $formatted_month_num;
							
	
						//end if limit recurence
						if ($end_radios == '2'){ //if End after x occurence
							if($total_stored_sched == $number_of_occurence){
								
								$test['the_schedules'] = $the_schedules;
								$this->session->set_flashdata('success','Available schedule successfully added.');
								echo json_encode($test);
								return false;
							}
						} //if End after x occurence
						
						else if ($end_radios == '3'){ //End by date
						
							$strt_dateweek2 = new DateTime($start_date_format);
							$eend_dateweek2 = new DateTime($the_end_date_format);
							$diffweek = $strt_dateweek2->diff($eend_dateweek2);
							
							$remaining_days_of_week = $diffweek->y * 365 + $diffweek->m * 30 + $diffweek->d + $diffweek->h/24; // + $diff->i / 60;
								
							//if($remaining_days_of_week == $total_stored_sched){
							if($start_date_format < $the_end_date_format){
								
								$test['the_schedules'] = $the_schedules;
								$this->session->set_flashdata('success','Available schedule successfully added.');
								echo json_encode($test);
								return false;
							}
		
						} //End by date
								
						
					}
					
					$count_of_rem_month++;
					

	
				}//end for loop
				
				$test['the_remaining_months'] = $the_remaining_months;
				
				
			
			}
			else{ //by number of week
			
			
				//$monthly_day_months = $monthly_day_months * 30;
				//$until = ($remaining_days/$monthly_day_months);
				$test['monthly_day_months'] = $monthly_day_months;
				//$test['until'] = $until;
			
			
				$test['monthly_set_radios'] = $monthly_set_radios;
				$test['monthly_week_no'] = $monthly_week_no;
				$test['monthly_days_settings'] = $monthly_days_settings;
				$test['monthly_the_months'] = $monthly_the_months;
				
				
				$month_number = idate('n', strtotime($concat_startdate_time)); //Numeric representation of a month, without leading zeros
				$test['month_number'] = $month_number;
				
				$count_of_rem_month = 0;
				
				$the_week_no = array();
				for($i = 0; $i < $remaining_days; $i++){ //loop the remaining days
				
					//get the week no of current date
					$concat_start_timestamp = strtotime($concat_startdate_time);
					$week_no = date('W', $concat_start_timestamp) - date('W', strtotime(date('Y-m-01', $concat_start_timestamp))) + 1;	
					
					$the_week_no[] = $week_no;

					if(($count_of_rem_month % $monthly_the_months) == 0){ //apply monthly recur

						if($week_no == $monthly_week_no){// store if matching week no - first, second etc

							$weekday_id = date('w', strtotime($concat_startdate_time));

							$arr = array(
								'start'=>$start_date_format,
								'end'=>$end_date_format,
								'login_id'=>$login_id,
								'recur_id'=>$recur_id,
								'date_added'=>$nowtime
							);

							if($monthly_days_settings == 'weekday'){
								
								if($weekday_id != 0 && $weekday_id != 6){ //if not sunday and saturday 
								
									$the_schedules[] = $arr;
									$total_stored_sched++;
									$this->master_model->insertRecord('schedules', $arr);
								}
							}//if weekday
							
							else if($monthly_days_settings == 'weekend'){
								
								if($weekday_id == 0 || $weekday_id == 6){ //if not sunday and saturday 
								
									$the_schedules[] = $arr;
									$total_stored_sched++;
									$this->master_model->insertRecord('schedules', $arr);
								}
							}
							else if($monthly_days_settings == 'everyday'){
								
									$the_schedules[] = $arr;
									$total_stored_sched++;
									$this->master_model->insertRecord('schedules', $arr);
							}
							
							else{ //particular day
								
								if($weekday_id == $monthly_days_settings){ //if not sunday and saturday

									$the_schedules[] = $arr;
									$total_stored_sched++;
									$this->master_model->insertRecord('schedules', $arr);
								}
							}
												
						} //if weekly match

					} //apply monthly recur
					
					
				
				
					//change date variables						
					$start_added_date_time = strtotime($concat_startdate_time.' + 1 DAYS');
					$end_added_date_time = strtotime($concat_enddate_time.' + 1 DAYS');
	
					$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
					$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);

					$start_time_format = substr($concat_startdate_time, -5);
					$end_time_format = substr($concat_enddate_time, -5);
					$start_date_format = substr($concat_startdate_time, 0, 10);
					$end_date_format = substr($concat_enddate_time, 0, 10);

					$start_date_format = $start_date_format.'T'.$start_time_format.':00';
					$end_date_format = $end_date_format.'T'.$end_time_format.':00';

				
					$new_month_number = idate('n', strtotime($concat_startdate_time)); //Numeric representation of a month, without leading zeros

				
					//check what month to skip
					if($month_number != $new_month_number){
						$month_number = $new_month_number;
						$count_of_rem_month++;
					}

	
						

					//end if limit recurence
					if ($end_radios == '2'){ //if End after x occurence
						if($total_stored_sched == $number_of_occurence){
							
							$test['the_schedules'] = $the_schedules;
							$this->session->set_flashdata('success','Available schedule successfully added.');
							echo json_encode($test);
							return false;
						}
					} //if End after x occurence
					
					else if ($end_radios == '3'){ //End by date
					
						$strt_dateweek2 = new DateTime($start_date_format);
						$eend_dateweek2 = new DateTime($the_end_date_format);
						$diffweek = $strt_dateweek2->diff($eend_dateweek2);
						
						$remaining_days_of_week = $diffweek->y * 365 + $diffweek->m * 30 + $diffweek->d + $diffweek->h/24; // + $diff->i / 60;
							
						//if($remaining_days_of_week == $total_stored_sched){
						if($start_date_format < $the_end_date_format){
							
							$test['the_schedules'] = $the_schedules;
							$this->session->set_flashdata('success','Available schedule successfully added.');
							echo json_encode($test);
							return false;
						}
	
					} //End by date
							



				}//end loop remaining days
				$test['the_week_no'] = $the_week_no;
								
			}
		
		} //if Monthly
		
		else if($recur_radios == 'yearly'){ //if Yearly
		
			$test['yearly_every_day'] = $yearly_every_day;
			$test['yearly_set_radios'] = $yearly_set_radios;



			if ($end_radios == '1'){ //if until the end of year
				$test['range_type'] = 'loop until remaining days of the year';
				$monthly_looper = '1'; // $remaining_days;
			} //if until the end of year

			else if ($end_radios == '2'){ //if End after x occurence
				$test['range_type'] = 'End after x occurence';
				$monthly_looper = $number_of_occurence;
				
				if($monthly_looper > 100){ //limit loop to one hunder
					
					$test['the_schedules'] = $the_schedules;
					$test['max_year'] = 'exceed';
					echo json_encode($test);
					return false;
				}
	
				
			} //if End after x occurence
			
			else if ($end_radios == '3'){ //End by date
				$test['range_type'] = 'End by date';
				$monthly_looper = '1'; // $remaining_endby_days;
				
			} //End by date
			
			$test['monthly_looper'] = $monthly_looper;



			//loop the year
			if($yearly_set_radios == '1'){
				
				$test['yearly_on_month'] = $yearly_on_month;
				$test['yearly_day'] = $yearly_day;
				$test['yearly_yeaar'] = array();
				

				for($x = 0; $x < $monthly_looper; $x++){
					
					$formatted_month_num = sprintf("%02d", $yearly_on_month);				
					$formatted_day_num = sprintf("%02d", $yearly_day);	
					
					//create new date format			
					$start_date_format = $current_year.'-'.$formatted_month_num.'-'.$formatted_day_num;
					$end_date_format = '';
					
					if($end_time_format != ''){
						$end_date_format = $start_date_format.'T'.$end_time_format.':00';
					}
					
					if($start_time_format != ''){
						$start_date_format = $start_date_format.'T'.$start_time_format.':00';
					}
					
					$test['yearly_the_date'] = $start_date_format;
					
					
					$arr = array(
						'start'=>$start_date_format,
						'end'=>$end_date_format,
						'login_id'=>$login_id,
						'recur_id'=>$recur_id,
						'date_added'=>$nowtime
					);
				
					$the_schedules[] = $arr;
					$this->master_model->insertRecord('schedules', $arr);
					$test['yearly_yeaar'][] = $current_year;
					$current_year += $yearly_every_day;
					$current_year = strval($current_year);
				}


			}
			
			
			else{
				
				$test['yearly_week_no'] = $yearly_week_no;
				$test['yearly_days_settings'] = $yearly_days_settings;
				$test['yearly_onthe_month'] = $yearly_onthe_month;
				
				
				
				for($x = 0; $x < $monthly_looper; $x++){
						
	
					$formatted_month_num = sprintf("%02d", $yearly_onthe_month);				
					$full_month_name = date('F', strtotime($current_year.'-'.$formatted_month_num));
				
					$test['full_month_name'] = $full_month_name;
				
					
					$week_arr = array(
						'Sunday',
						'Monday',
						'Tuesday',
						'Wednesday',
						'Thursday',
						'Friday',
						'Saturday'
					);
					$test['yearly_date'] = array();
					
					
					//set to last if forth is selected when yearly_days_settings != everyday
					if($yearly_days_settings != 'everyday'){
						if($yearly_week_no == 'forth'){
							$yearly_week_no = 'last';
						}
					}
	
					if($yearly_days_settings == 'everyday'){//get first day of the month
					
						if($yearly_week_no == 'first'){
							$format_startdate = $current_year.'-'.$formatted_month_num.'-01';
						}
						else if($yearly_week_no == 'second'){
							$format_startdate = $current_year.'-'.$formatted_month_num.'-02';
						}
						else if($yearly_week_no == 'third'){
							$format_startdate = $current_year.'-'.$formatted_month_num.'-03';
						}
						else if($yearly_week_no == 'forth'){
							$format_startdate = $current_year.'-'.$formatted_month_num.'-04';
						}
						else if($yearly_week_no == 'last'){
							
							$last_day_ofmonth = date("t", strtotime($current_year.'-'.$formatted_month_num));
							
							$format_startdate = $current_year.'-'.$formatted_month_num.'-'.$last_day_ofmonth;
						}
	
						$test['format_startdate'] = $format_startdate;
						$format_enddate = $format_startdate;
						
						if($end_time_format != ''){
							$format_enddate = $format_enddate.'T'.$end_time_format.':00';
						}
						
						if($start_time_format != ''){
							$format_startdate = $format_startdate.'T'.$start_time_format.':00';
						}
	
						$arr = array(
							'start'=>$format_startdate,
							'end'=>$format_enddate,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							//'date_added'=>$nowtime
						);
						
						$the_schedules[] = $arr;
						$this->master_model->insertRecord('schedules', $arr);
					
					}//end everyday
					
					else if($yearly_days_settings == 'weekday'){
						foreach($week_arr as $r=>$weekday){
							if($weekday != 'Sunday' && $weekday != 'Saturday'){
								
								$the_yearly_date = new DateTime($yearly_week_no.' '.$weekday.' of '.$full_month_name);
								$the_yearly_date = $the_yearly_date->format('Y-m-d');
								$test['yearly_date'][] = $the_yearly_date;
								
								
								$format_enddate = $the_yearly_date;
								
								if($end_time_format != ''){
									$format_enddate = $format_enddate.'T'.$end_time_format.':00';
								}
								
								if($start_time_format != ''){
									$the_yearly_date = $the_yearly_date.'T'.$start_time_format.':00';
								}
								
								
								$arr = array(
									'start'=>$the_yearly_date,
									'end'=>$format_enddate,
									'login_id'=>$login_id,
									'recur_id'=>$recur_id,
									//'date_added'=>$nowtime
								);
								
								$the_schedules[] = $arr;
								$this->master_model->insertRecord('schedules', $arr);
							}
						}
					
					}//end weekday
					
					else if($yearly_days_settings == 'weekend'){
						foreach($week_arr as $r=>$weekday){
							if($weekday == 'Sunday' || $weekday == 'Saturday'){
								
								$the_yearly_date = new DateTime($yearly_week_no.' '.$weekday.' of '.$full_month_name);
								$the_yearly_date = $the_yearly_date->format('Y-m-d');
								$test['yearly_date'][] = $the_yearly_date;
								
								
								$format_enddate = $the_yearly_date;
								
								if($end_time_format != ''){
									$format_enddate = $format_enddate.'T'.$end_time_format.':00';
								}
								
								if($start_time_format != ''){
									$the_yearly_date = $the_yearly_date.'T'.$start_time_format.':00';
								}
								
								
								$arr = array(
									'start'=>$the_yearly_date,
									'end'=>$format_enddate,
									'login_id'=>$login_id,
									'recur_id'=>$recur_id,
									//'date_added'=>$nowtime
								);
								
								$the_schedules[] = $arr;
								$this->master_model->insertRecord('schedules', $arr);
							}
						}
					
					}//end weekend
		
										
					else{
	
						$the_yearly_date = new DateTime($yearly_week_no.' '.$week_arr[$yearly_days_settings].' of '.$full_month_name);
						$the_yearly_date = $the_yearly_date->format('Y-m-d');
						$test['yearly_date'][] = $the_yearly_date;
						
						
						$format_enddate = $the_yearly_date;
						
						if($end_time_format != ''){
							$format_enddate = $format_enddate.'T'.$end_time_format.':00';
						}
						
						if($start_time_format != ''){
							$the_yearly_date = $the_yearly_date.'T'.$start_time_format.':00';
						}
						
						
						$arr = array(
							'start'=>$the_yearly_date,
							'end'=>$format_enddate,
							'login_id'=>$login_id,
							'recur_id'=>$recur_id,
							//'date_added'=>$nowtime
						);
						
						$the_schedules[] = $arr;
						$this->master_model->insertRecord('schedules', $arr);
	
					
					}//end if particular weekday
		
					
					$current_year += $yearly_every_day;
					
				}//end for loop

				
			}//end the nth day of month
			

		} //if Yearly
		
		
		
		$test['the_schedules'] = $the_schedules;
		
		$this->session->set_flashdata('success', 'Available schedule successfully added.');
		$this->master_model->insertRecord('schedules', $arr);
		
		echo json_encode($test);
	}
	
	public function testtodate(){
		
		$e = strtotime('2015-04-09 12:00 + 7 DAYS');
		$test = new DateTime("@$e");
		$test;
		$fff = date('w', strtotime('2015-04-05 12:00'));
		$newformat = date_format($test, 'Y m d');
		$start = date("Y-m-d H:i", $e);
		//echo date('Y-m-01', time());
		
//		$dt = new DateTime('last Sunday of March');
//		echo $dt->format('l m d');

		$a_date = "2015-05-23";
		echo date("Y-m-t", strtotime($a_date));
	}
	
	
	public function get_events(){
		$login_id = $this->session->userdata('logged_crt_login_id');
		$scheds = $this->master_model->getRecords('schedules', array('login_id'=>$login_id));

		$available = array();
		
		if(count($scheds) > 0){
			foreach($scheds as $r=>$value){
				$sched = array(
					'id'=>$value['id'],
					'the_id'=>$value['id'],
					'title'=>$value['title'],
					'start'=>$value['start']
				);
				
				if($value['end'] != ''){
					$sched['end'] = $value['end'];
				}
				
				$available[] = $sched;
			}
		}

		//$output_arrays = json_decode($arr, true);
		
		
		echo json_encode($available);
	}
	
	public function delete(){
		$id = $_POST['id'];
		$this->master_model->deleteRecord('schedules', 'id', $id);
		$this->session->set_flashdata('success', 'Schedule successfully deleted.');
		echo $id;
	}

}

?>
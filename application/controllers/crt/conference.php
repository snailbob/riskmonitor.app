<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Conference extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		

	}

	

	public function index()

	{
		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();
		

		$data=array('page_title'=>'Add Crisis Responce Team Member','error'=>'','middle_content'=>'conference-view',

		'success'=>'');

		$this->load->view('crt/crt-view',$data);

	}

	



	//process chat 
	public function process_chat()
	{

		$function = $_POST['function'];
		$org = $_POST['org'];


		$log = array();
		
		$org_room = 'chat_room/im/';
		$org_room .= $org;
		$org_room .= '.txt';
		
		switch($function) {
		
			 case('getState'):
				 if(file_exists($org_room)){
				   $lines = file($org_room);
				 }
				 $log['state'] = count($lines); 
				 break;	
			
			 case('update'):
			 
		 
				$state = $_POST['state'];
				if(file_exists($org_room)){
				   $lines = file($org_room);
				 }
				 
				 $count =  count($lines);
				 if($state == $count){
					 $log['state'] = $state;
					 $log['text'] = false;
					 
				 }
				 else{
					 $text= array();
					 $log['state'] = $state + count($lines) - $state;
					 foreach ($lines as $line_num => $line)
					   {
						   if($line_num >= $state){
						 $text[] =  $line = str_replace("\n", "", $line);
						   }
		 
						}
					 $log['text'] = $text; 
				 }
				  
				/*//mark as read
				$wwhh_s = array(
					'sender_id'=>$sender,
					'receiver_id'=>$receiver
				);

				
				$this->master_model->updateRecord('private_instant_messages',array('is_read'=>'1','count_unread'=>'0'),$wwhh_s);
		*/
			 
				  
				  
				  
				 break;
			 
			 case('send'):
					$senderr = $_POST['senderr'];
					$receiverr = $_POST['receiverr'];
				 			 
					$nickname = htmlentities(strip_tags($_POST['nickname']));
					  
					$nowsenttime = time();
					$timezone = $this->session->userdata('timezone');
					$thesenttime = gmt_to_local($nowsenttime, $timezone, TRUE); //(timestamp, timezone, daylight_saving)
					
					$time_sent = gmdate("g:i:a", $thesenttime); //g:i:sa d/m/Y
					$time_sent_title = gmdate("g:i:sa d/m/Y", $thesenttime); //g:i:sa d/m/Y

						 $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
						  $message = htmlentities(strip_tags($_POST['message']));
					 if(($message) != "\n"){
						
						 if(preg_match($reg_exUrl, $message, $url)) {
							$message = preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $message);
							} 
						 
						
						 fwrite(fopen($org_room, 'a'), "<span>". $nickname . "</span>" . $message = str_replace("\n", " ", $message) . "<br><span class='pull-right small chat_date' title='".$time_sent_title."'>" . $time_sent . "</span>" . "\n"); 
					 }
					 
					 
					// update receiver to unread
					$wwhh_r = array(
						'sender_id'=>$receiverr,
						'receiver_id'=>$senderr
					);
					
					
					$txt_file = $this->master_model->getRecords('private_instant_messages',$wwhh_r);
				
					
					$count_unread = $txt_file[0]['count_unread'] + 1;
					
					$this->master_model->updateRecord('private_instant_messages',array('is_read'=>'0','count_unread'=>$count_unread),$wwhh_r);
		
					 
					// update sender to read
					$wwhh_s = array(
						'sender_id'=>$senderr,
						'receiver_id'=>$receiverr
					);
				
					
					$this->master_model->updateRecord('private_instant_messages',array('is_read'=>'1','count_unread'=>'0'),$wwhh_s);
					 
				 break;
			
		}
		
		echo json_encode($log);	
		
	}
	

	public function onlines()
	{
		if ($this->session->userdata('crt_selected_orgnaization') !=''){ 

							
			$this->db->order_by("last_activity", "desc"); 
			$this->db->distinct();
			$this->db->group_by('user_data');
			$onlines = $this->db->get('ci_sessions');

			//$this->master_model->getRecords('ci_sessions');
			
			if ($onlines->num_rows() > 0){
				$ccid = $this->session->userdata('team_cc');
				$count = 0;
				$count_unread_total = 0;
				$ol_cc = array();
				$check_duplicate = array();

				foreach ($onlines->result() as $value){
				
					$userdata = unserialize($value->user_data);
					
					$idle = time() - $value->last_activity;
				  
					$dele = time() - 7200;
					$this->db->where('last_activity <', $dele);
					$this->db->delete('ci_sessions'); 

					
					$initiator_id = $this->session->userdata('logged_crt_login_id');
					
					if (isset($userdata['logged_cc_login_id'])){
						$ol_cc[$count] = $userdata['logged_cc_login_id'];
						$receiver_id = $userdata['logged_cc_login_id'];
						
					}
					else if (isset($userdata['logged_crt_login_id'])){
						$ol_cc[$count] = $userdata['logged_crt_login_id'];
						$receiver_id = $userdata['logged_crt_login_id'];
					}
					
				  if (isset($userdata['logged_display_name']) && isset($userdata['team_cc']) && $userdata['team_cc'] == $ccid){
						if(in_array($userdata['my_id'], $check_duplicate) == false && $userdata['my_id'] != $this->session->userdata('my_id')){
	
							$unread_count = $this->master_model->getRecords('private_instant_messages',array('receiver_id'=>$receiver_id,'sender_id'=>$initiator_id));
						  
						if( isset($userdata['cc_selected_orgnaization']) && $userdata['cc_selected_orgnaization'] == $this->session->userdata('cc_selected_orgnaization') && $initiator_id != $receiver_id){
	
							  echo '<li class="ol" onclick="createChatroom(\''.$userdata['logged_display_name'].'\',\''.$initiator_id.'\',\''.$receiver_id.'\');" style="cursor: pointer;"><i class="fa fa-circle text-green"></i> '; // style="color: #093"
	
							  echo $userdata['logged_display_name'];
	
							  if (count($unread_count) > 0 && $unread_count[0]['count_unread'] > 0){
								  echo '<span class="badge red pull-right">'.$unread_count[0]['count_unread'].'</span>';
								  $count_unread_total += $unread_count[0]['count_unread']; 
							  }
							  echo '</li><hr>';

							array_push($check_duplicate, $userdata['my_id']);
						}
						
					 }
				  }
				  
				$count++;

			  }//.foreach
			  
				echo '<span id="total_unread" class="hidden">'.$count_unread_total.'</span>';
			  
				/* fetch  crt members*/
				$whr=array('cc_id'=>$this->session->userdata('team_cc'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'));

				$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
		
				$this->db->order_by('cf_crisis_response_team.login_id','DESC');
		
				$all_crt=$this->master_model->getRecords('cf_crisis_response_team',$whr);
				

				foreach($all_crt as $aa=>$crt){
					if (!in_array($crt['login_id'], $ol_cc)){
						echo '<li class=""><i class="fa fa-circle text-muted"></i> '.$this->master_model->decryptIt($crt['crt_first_name']).' '.$this->master_model->decryptIt($crt['crt_last_name']).'</li><hr>';
					}
				}
			
			}
			
			
		}
		else{
			echo '<li><span class="text-muted"><i class="fa fa-warning"></i> Your session expired. Please login again to continue.</span></li>';
		}

	}
	


	public function have_read(){
	
	$initiator = $_POST['initiator'];
	$receiver = $_POST['receiver'];
	echo 'init'.$initiator;
	echo ' receiver'.$receiver;
	//mark as read
	$wwhh_s = array(
		'sender_id'=>$initiator,
		'receiver_id'=>$receiver
	);

	
	$this->master_model->updateRecord('private_instant_messages',array('is_read'=>'1','count_unread'=>'0'),$wwhh_s);

	
	}




}

?>
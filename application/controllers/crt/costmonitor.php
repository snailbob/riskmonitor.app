<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Costmonitor extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();


		$this->load->library('upload');

		$this->load->helper('download');

	}

	
	public function index(){
		$recall_id = $this->uri->segment(4);
		$my_class = $this;
		$whr = array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'recall_id'=>$recall_id
		);
		$uniquetime= time().mt_rand();

		$cost_category = $this->master_model->getRecords('cf_cost_category',$whr,'*',array('date_created'=>'DESC'));
		$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'closed'=>'0'),'*',array('id'=>'DESC'));
		$my_recall = $this->master_model->getRecords('cf_recall',array('id'=>$recall_id));

		$recall_accumulate = $this->accumulate($my_recall);
						
		$data=array(
			'page_title'=>"Manage",
			'middle_content'=>'manage-cost',
			'success'=>'',
			'error'=>'',
			'recall'=>$recall,
			'my_recall'=>$my_recall,
			'cost_category'=>$cost_category,
			'my_class'=>$my_class,
			'uniquetime'=>$uniquetime,
			'recall_accumulate'=>$recall_accumulate
		);

		$this->load->view('crt/crt-view',$data);
		
	}
	

	//-----------------------------------------manage cost monitor category
	
	
	public function accumulate($recall){
	
	
		if(count($recall) == 0){
			$myaccumulate = 'No Cost Items';
			return $myaccumulate;
		}
		else{

			$recall_items = $this->master_model->getRecords('cf_cost_category_item',array('recall_id'=>$recall[0]['id']),'*',array('currency'=>'ASC'));
	
			$myaccumulate = '';
			if(count($recall_items) > 0){
				
				$mycurr = $recall_items[0]['currency'];
				$totalcurr = 0;
				$m = 0;
				
				foreach($recall_items as $it=>$ems){
					if($mycurr != $ems['currency']){
							
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2).' + ';
						
						$mycurr = $ems['currency'];
						$totalcurr = $ems['item_cost'];
						
	
					}else{
						
						$totalcurr += $ems['item_cost'];
					}
				
					if((count($recall_items) - 1) == $m){
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2);
					}
					
					$m++;
				}
				
						
			}else{
				$myaccumulate .= 'No Cost Items';
			}
			return $myaccumulate;
			
		}
		
		
	}
	
	
	
	public function manage()

	{
		$my_class = $this;
		$my_recall = array();
		$uniquetime= time().mt_rand();

		$whr = array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization')
		);

		$cost_category = $this->master_model->getRecords('cf_cost_category',$whr,'*',array('date_created'=>'DESC'));
		$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'closed'=>'0'),'*',array('id'=>'DESC'));


		/*if(count($recall) > 0){
			redirect('cc/costmonitor/index/'.$recall[0]['id']);
		}else{
		
		}*/

		
		$data=array(
			'page_title'=>"Manage",
			'middle_content'=>'manage-cost',
			'success'=>'',
			'error'=>'',
			'recall'=>$recall,
			'cost_category'=>$cost_category,
			'my_class'=>$my_class,
			'my_recall'=>$my_recall,
			'uniquetime'=>$uniquetime
		);

		$this->load->view('crt/crt-view',$data);

	}	
	

	//-----------------------------------------manage cost monitor category
	public function managex()

	{
		$my_class = $this;
		$my_recall = array();
		$whr = array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization')
		);

		$cost_category = $this->master_model->getRecords('cf_cost_category',$whr);
		$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),));

		$data=array(
			'page_title'=>"Manage",
			'middle_content'=>'manage-cost',
			'success'=>'',
			'error'=>'',
			'recall'=>$recall,
			'cost_category'=>$cost_category,
			'my_class'=>$my_class,
			'my_recall'=>$my_recall
		);

		$this->load->view('crt/crt-view',$data);

	}	
	
	public function add_category(){
		$name_cat = $_POST['name_cat'];
		$recall_id = $_POST['recall_id'];
		$cat_id = $_POST['cat_id'];
		
		$nowtime = date("Y-m-d H:i:s");

		
		$arr = array(
			'name'=>$name_cat,
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'recall_id'=>$recall_id,
			'date_created'=>$nowtime
			
		);
		if($cat_id == ''){
			if($id = $this->master_model->insertRecord('cf_cost_category',$arr,true)){
				$this->session->set_flashdata('success',' Category added successfully.');
				echo 'success';
			
			}
			else{
				$this->session->set_flashdata('error',' Something went wrong.');
				echo 'error';
			}
			
		}else{
			
			$arr2 = array(
				'name'=>$name_cat,
				'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
				'cc_id'=>$this->session->userdata('logged_parent_crt')
			);
			if($this->master_model->updateRecord('cf_cost_category',$arr2,array('cost_id'=>$cat_id))){
				$this->session->set_flashdata('success',' Category updated successfully.');
				echo 'success';
			
			}
			else{
				$this->session->set_flashdata('error',' Something went wrong.');
				echo 'error';
			}
			
		}
		
	}
	
	public function add_cost_item() {
	
		$item_name = $_POST['item_name'];
		$item_cost = $_POST['item_cost'];
		$recall_id = $_POST['recall_id'];
		$currency_code = $_POST['currency_code'];
		$uniquetime = $_POST['uniquetime'];
		
		$cost_item_id = $_POST['cost_item_id'];
		$cat_id = $_POST['cat_id'];
		
		$nowtime = date("Y-m-d H:i:s");

		
		$arr = array(
			'item_name'=>$item_name,
			'item_cost'=>$item_cost,
			'currency'=>$currency_code,
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'cost_id'=>$cat_id,
			'recall_id'=>$recall_id,
			'date_created'=>$nowtime
			
		);
		if($cost_item_id == ''){
			if($id = $this->master_model->insertRecord('cf_cost_category_item',$arr,true)){
				
				//update attached file id
				$this->master_model->updateRecord('private_message_file',array('cost_item_id'=>$id), array('temp_id'=>$uniquetime));
				
				$this->session->set_flashdata('success',' Cost Item added successfully.');
				echo 'success';
			
			}
			else{
				$this->session->set_flashdata('error',' Something went wrong.');
				echo 'error';
			}
			
		}else{
			
			$arr2 = array(
				'item_name'=>$item_name,
				'item_cost'=>$item_cost,
				'currency'=>$currency_code,
			);
			if($this->master_model->updateRecord('cf_cost_category_item',$arr2,array('item_id'=>$cost_item_id))){
				
				//update attached file id
				$this->master_model->updateRecord('private_message_file',array('cost_item_id'=>$cost_item_id), array('temp_id'=>$uniquetime));
				
				$this->session->set_flashdata('success',' Cost Item updated successfully.');
				echo 'success';
			
			}
			else{
				$this->session->set_flashdata('error',' Something went wrong.');
				echo 'error';
			}
			
		}
	
	
	
	}
	

	public function get_item_details(){
	
		$item_id = $_POST['id'];

		$item =$this->master_model->getRecords('cf_cost_category_item',array('item_id'=>$item_id));
		$item_file = $this->master_model->getRecords('private_message_file',array('cost_item_id'=>$item_id));
		
		echo '<div class="well" style="color: #333; border-radius: 0;">';
		
		echo '<p><b>Name:</b><br> '.$item[0]['item_name'].'</p>';
		
		echo '<p><b>Cost:</b><br> '.number_format($item[0]['item_cost'],2).'</p>';
		
		echo '<p><b>Currency:</b><br> '.$this->currencycode($item[0]['currency']).'</p>';

		echo '</div> ';	

		
		if(count($item_file) > 0){
			echo '<p>File Attached: ';
		}
		
		foreach($item_file as $ff=>$rff){
			echo ' <a href="'.base_url().'uploads/costitems-uploads/'.$rff['file_upload_name'].'" target="_blank" id = "file_del'.$rff['file_upload_id'].'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.substr($rff['file_upload_name'], 13).'</a>';
			
			
			echo ' <span onclick="$(\'#file_del'.$rff['file_upload_id'].'\').hide();$(this).hide();delete_the_cost_file(event.target.id);" style="cursor: default" id="delete_file'.$rff['file_upload_id'].'">&times;</span>';

		}//foreach
		
		echo '</p>';
		
	}
	
	
	public function get_summary_details(){
		$cost_id = $this->uri->segment(4);
		
		$cost_category=$this->master_model->getRecords('cf_cost_category',array('cost_id'=>$cost_id));
		
		$items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$cost_id),'*',array('currency'=>'ASC'));
	
		echo '<div class="well" style="color: #333; border-radius: 0;">';
		
		echo '<h4><b>Category Name:</b> '.$cost_category[0]['name'].'</h4>';
		
		
		if(count($items) > 0){
			
			echo '<table class="table table-hover">';
			
			echo '<tr>';
			
			echo '<th>Item Name</th>';
			echo '<th>Item Cost</th>';
			  
			echo '</tr>';
	
			$mycurr = $items[0]['currency'];
			$totalcurr = 0;
			$m = 0;
			foreach($items as $it=>$ems){
				if($mycurr != $ems['currency']){
						
					echo '<tr class="success">';
					
					echo '<td></td>';
					echo '<td>Total Summary '.number_format($totalcurr,2).' for '.$this->currencycode($mycurr).' </td>';

					echo '</tr>';
					
					echo '<tr>';
					
					echo '<th>Item Name</th>';
					echo '<th>Item Cost</th>';
					  
					echo '</tr>';
					
					$mycurr = $ems['currency'];
					$totalcurr = $ems['item_cost'];
					

				}else{
					
					$totalcurr += $ems['item_cost'];
				}

				echo '<tr>';
				
				echo '<td>'.$ems['item_name'].'</td>';
				echo '<td>'.$ems['currency'].' '.number_format($ems['item_cost'],2).'</td>';
				  
				echo '</tr>';
				
				if((count($items) - 1) == $m){
					echo '<tr class="success">';
					
					echo '<td></td>';
					echo '<td>Total Summary '.number_format($totalcurr,2).' for '.$this->currencycode($mycurr).' </td>';

					echo '</tr>';
				}
				
				$m++;
			}
			
			echo '</table>';		
		}else{
			echo '<span class="label label-default">No Cost Items</span>';
		}
		
		echo '</div> ';	
	
	}
		
	
	public function manageitems() {
		$cat_id =  $this->uri->segment(4);
		
		$cost_category=$this->master_model->getRecords('cf_cost_category',array('cost_id'=>$cat_id));
		
		$items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$cat_id));
		
		$uniquetime= time().mt_rand();


		$data=array(
			'page_title'=>"Manage",
			'middle_content'=>'manage-costitems',
			'success'=>'',
			'error'=>'',
			'items'=>$items,
			'cost_category'=>$cost_category,
			'uniquetime'=>$uniquetime
		);

		$this->load->view('crt/crt-view',$data);
	}
	
	

	public function upload(){
	
		$output_dir = "uploads/costitems-uploads/";
		
		
		if(isset($_FILES["myfile"]))
		{
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0)
			{
			  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
			}
			else
			{
				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);
								
					
					$insrt_arr = array (
						'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
						'cc_id'=>$this->session->userdata('logged_parent_crt'),
						'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
						'temp_id'=>$_POST['uniquetime'],
						'message_id'=>'0',
						'cost_item_id'=>'0' //$cc_case_id
					);					
					if ($file_id = $this->master_model->insertRecord('private_message_file',$insrt_arr,true)){
						
						echo ' <a href="'.base_url().'uploads/costitems-uploads/'.$thefilename.'" target="_blank" id = "file_del'.$file_id.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$_FILES["myfile"]["name"].'</a> <span onclick="$(\'#file_del'.$file_id.'\').hide();$(this).hide();delete_the_cost_file(event.target.id);" style="cursor: default" id="delete_file'.$file_id.'">&times;</span>';
					
					}
					else{
					  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
					}
			}
		
		}		
	}	
	

	public function delete_message_file(){
		
		$thenum = $_POST['thenum'];

	    $file = $this->master_model->getRecords('private_message_file',array('file_upload_id'=>$thenum));
		
		$file_name = $file[0]['file_upload_name'];
		
		if($this->master_model->deleteRecord('private_message_file','file_upload_id',$thenum))

		{

			@unlink('uploads/costitems-uploads/'.$file_name);

			//$this->session->set_flashdata('success',' Document successfully deleted.');
			echo 'success';

		}

	
	}
		//-----------------------------------------Delete crt

	public function delete()

	{

		$data['success']=$data['error']="";
		$cat_id=$this->uri->segment(4);
		$recall_id = $this->uri->segment(5);

		if($this->master_model->deleteRecord('cf_cost_category','cost_id',$cat_id))
		{	
			$this->session->set_flashdata('success',' Record successfully deleted.');
			redirect("cc/costmonitor/index/".$recall_id);
		}
		else{
			$this->session->set_flashdata('error',' Error while deleting record.');
			redirect("cc/costmonitor/index/".$recall_id);

		}

	}
	
	public function deleteitem()

	{

		$data['success']=$data['error']="";
		$item_id=$this->uri->segment(4);
		$cat_id=$this->uri->segment(5);
		$recall_id=$this->uri->segment(6);

		if($this->master_model->deleteRecord('cf_cost_category_item','item_id',$item_id))
		{	
			$this->session->set_flashdata('success',' Record successfully deleted.');
			redirect("cc/costmonitor/index/".$recall_id);
		}
		else{
			$this->session->set_flashdata('error',' Error while deleting record.');
			redirect("cc/costmonitor/index".$recall_id);

		}

	}
	
	//------------------------------------resend crt
	public function resend_request(){
		
		$id = $_POST['login_id'];


		/* fetch  crt members*/
		$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$id));
		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));


		$whr=array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

		$info_arr=array(
			'from'=>$adminemail[0]['recovery_email'],
		
			'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
		
			'subject'=>'Welcome to CrisisFlo',
		
			'view'=>'crt_registration_on_crisesflo'
		);

	

		$other_info=array(
			
			'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),
			
			'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),
			
			'login_id'=>$crt_onetimekey[0]['onetime_key']
		);


		if($this->email_sending->sendmail($info_arr,$other_info)){
			$this->session->set_flashdata('success',' Email request successfully re-sent.');
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error',' Email request failed to re-sent.');
			echo 'error';
		}

		
	}
	
	//------------------------------------import_crt
	public function import_crt(){
		$active_module = $_POST['active_module'];
		$addmore = $_POST['addmore'];

		$crts = $this->master_model->getRecords('cf_crisis_response_team',array('org_id'=>$this->session->userdata('crt_selected_orgnaization')));
		
		if(count($crts)){
			echo '<div class="alert alert-danger fade in" role="alert" style="display: none;">
					  Opps! Selected CRT exceeded.
					</div><input type="hidden" value="'.$active_module.'" id="active_module"/>';
			$i = 0;
			foreach($crts as $c=>$rt){
				if(strpos($rt['module'], $active_module) === false){
					echo '<div class="checkboxes">
							<label for="checkimport'.$rt['login_id'].'">
							  <input type="checkbox" name="checkimport[]" id="checkimport'.$rt['login_id'].'" value="'.$rt['login_id'].'" onclick="countToImport('.$addmore.');">'; 
							  
							  echo $this->master_model->decryptIt($rt['crt_first_name']).' '.$this->master_model->decryptIt($rt['crt_last_name']);
					
					echo '</label></div>';
					$i++;
				}
				
			}
			if($i == 0){
				echo '<p class="text-center text-muted">There\'s nothing to import.</p>';
			}
		}else{
			echo '<p class="text-center text-muted">Nothing found.</p>';
		}
	}
	
	
	
	//------------------------------------save_import_crt
	public function save_import_crt(){
		$crts = $_POST['checked']; //array
		$active_module = $_POST['active_module'];
		
		foreach($crts as $crt){
			$thecrt = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$crt));

			$newmod = $thecrt[0]['module'].$active_module;
			$this->master_model->updateRecord('cf_crisis_response_team',array('module'=>$newmod),array('login_id'=>$crt));
		}
		$this->session->set_flashdata('success',' Crisis Response Team imported successfully.');
		echo base_url().'cc/crisisteam/managecrt';
	}
	
	//------------------------------------Update crt

	public function update()

	{

		$login_id=$this->uri->segment(4);

		if(isset($_POST['btn_add_crt_member']))

		{

			$this->form_validation->set_rules('cct_firstname','First Name','required');

			$this->form_validation->set_rules('cct_lastname','Last Name','required');

			$this->form_validation->set_rules('crt_position','Postion','required');

			$this->form_validation->set_rules('crt_email','Email','required');

			$this->form_validation->set_rules('crt_no','Contact Number','required');

			$this->form_validation->set_rules('group','User Group','required');

			$this->form_validation->set_message('required', 'Mandatory field');
				if($this->form_validation->run())

				{

					$cct_firstname=$this->input->post('cct_firstname');

					$cct_lastname=$this->input->post('cct_lastname');

					$crt_position=$this->input->post('crt_position');

					$countrycode = $this->input->post('countrycode');

					$crt_digits = $this->input->post('crt_no');

					$crt_no= $countrycode.$crt_digits;

					$group = $this->input->post('group');

					$crt_func=$this->input->post('crt_func');

					$alt_member=$this->input->post('alter_member');

					$city = $this->input->post('city');
					
					$state = $this->input->post('state');
					
					$availability = $this->input->post('availability');
					
					$experience = $this->input->post('experience');
					

					$input_array=array(
						'group_id'=>$group,
						'crt_first_name'=>$this->master_model->encryptIt($cct_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cct_lastname),
						'crt_position'=>$crt_position,
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($crt_no),
						'crisis_function'=>$crt_func,
						'alt_member'=>$alt_member,
						
						'crt_city'=>$city,
						'crt_state'=>$state,
						'crt_availability'=>$availability,
						'crt_experience'=>$experience,
						
						'cc_id'=>$this->session->userdata('logged_parent_crt')
					);

					if($this->master_model->updateRecord('cf_crisis_response_team',$input_array,array('login_id'=>$login_id)))

					{

						$this->session->set_flashdata('success',' Crisis Response Team Member Updated successfully');

						redirect(base_url().'cc/crisisteam/managecrt/');

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating Crisis Response Team Member.');

						redirect(base_url().'cc/crisisteam/managecrt/');

					}

				}

		}
		$countriescode=$this->master_model->getRecords('country_t');
		$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		$all_group=$this->master_model->getRecords('user_group',array('org_id'=>$this->session->userdata('crt_selected_orgnaization')));

		$data=array('page_title'=>'Update Crisis Team Member','error'=>'','middle_content'=>'update-crt','success'=>'','crt_record'=>$crt_record,'countriescode'=>$countriescode,'all_group'=>$all_group);

		$this->load->view('crt/crt-view',$data);

	}

	

	public function details()

	{

		$login_id=$this->uri->segment(4);

		

		$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data=array('page_title'=>'Crisis Team Member details','error'=>'','middle_content'=>'details-crt',

		'success'=>'','crt_record'=>$crt_record);

		

		$this->load->view('crt/crt-view',$data);

	}
	
	
	public function recallname($id)

	{
		
		$recall = $this->master_model->getRecords('cf_recall',array('id'=>$id));
		if(count($recall) > 0){
			return $recall[0]['incident_no'].': '.$recall[0]['incident_name'];
		}else{
			return 'no recall found';
		}
	}


	public function currencycode($id) {
		
		$currency = $this->master_model->getRecords('countries_currency',array('idCountry'=>$id));
		if(count($currency) > 0){
			return $currency[0]['currencyCode'];
		}else{
			return 'Currency Unknown';
		}
	}
	

	// Download document
	public function download()

	{

		$doc_name=$this->uri->segment(4);

		$data=file_get_contents("uploads/costitems-uploads/".$doc_name);

		$name=substr($doc_name,13,50);

		force_download($name,$data);

	}	
}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Crisisteam extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

		

	}

	

	//------------------------------------add crt

	public function add()

	{

		if(isset($_POST['btn_add_crt_member']))

		{

			$this->form_validation->set_rules('cct_firstname','First Name','required');

			$this->form_validation->set_rules('cct_lastname','Last Name','required');

			$this->form_validation->set_rules('crt_position','Postion','required');

			$this->form_validation->set_rules('crt_email','Email','required|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('crt_no','Contact Number','required');

				if($this->form_validation->run())

				{

					$cct_firstname=$this->input->post('cct_firstname');

					$cct_lastname=$this->input->post('cct_lastname');

					$crt_position=$this->input->post('crt_position');

					$crt_email=$this->input->post('crt_email');

					$crt_no=$this->input->post('crt_no');

					$onetime_key=md5(microtime());

					

					$admin_array=array('email_id'=>$crt_email,'user_level'=>'1','onetime_key'=>$onetime_key);

					$login_id=$this->master_model->insertRecord('cf_login_master',$admin_array,true);

					

					$whr=array('organization_id'=>$this->session->userdata('cc_selected_orgnaization'));

					$org_name=$this->master_model->getRecords('organization_master',$whr,'*');

					

					$input_array=array('login_id'=>$login_id,'crt_first_name'=>$cct_firstname,'crt_last_name'=>$cct_lastname,'crt_organisation'=>$org_name[0]['organization_name'],'crt_position'=>$crt_position,'crt_email'=>$crt_email,'crt_mobile'=>$crt_no,'cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'));

					if($this->master_model->insertRecord('cf_crisis_response_team',$input_array))

					{

						$whr=array('id'=>'1');

						$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

						

						$info_arr=array('from'=>$adminemail[0]['recovery_email'],

									'to'=>$crt_email,

									'subject'=>'Registration on Crisisflo',

									'view'=>'crt_registration_on_crisesflo');

					

					     $other_info=array(

									  'first_name'=>$cct_firstname,

									  'last_name'=>$cct_lastname,

									  'login_id'=>$onetime_key);

					

					    $this->email_sending->sendmail($info_arr,$other_info);

						$this->session->set_flashdata('success','Crisis Response Team Member added successfully');

						redirect(base_url().'cc/crisisteam/add/');

					}

				}

		}

		$data=array('page_title'=>'Add Crisis Responce Team Member','error'=>'','middle_content'=>'add-crt','success'=>'');

		$this->load->view('cc/cc-view',$data);

	}

	

	

	//-----------------------------------------manage crt

	public function managecrt()

	{  	//print_r($this->session->userdata); exit;

	//echo "**********".$this->session->userdata('logged_crt_login_id'); exit;

		$whr=array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'cf_crisis_response_team.login_id !='=>$this->session->userdata('logged_crt_login_id')); 

		

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');

		$all_crt=$this->master_model->getRecords('cf_crisis_response_team',$whr);

		

		$data=array('page_title'=>"Manage Crisis Team Members",'middle_content'=>'manage-crt','success'=>'',

		'error'=>'','all_crt'=>$all_crt);

		$this->load->view('crt/crt-view',$data);

	}

	//-----------------------------------------Delete crt

	public function delete()

	{

		$data['success']=$data['error']="";

		$login_id=$this->uri->segment(4);

		if($this->master_model->deleteRecord('cf_login_master','login_id',$login_id))

		{	

			$this->master_model->deleteRecord('cf_crisis_response_team','login_id',$login_id);

			$this->session->set_flashdata('success','Recored Delete Successfully.');

			redirect(base_url()."cc/crisisteam/managecrt/");

		}

		else

		{

			$this->session->set_flashdata('error','Error while deleting records.');

			redirect(base_url()."cc/crisisteam/managecrt/");

		}

		

	}

	

	//------------------------------------Update crt

	public function update()

	{

		$login_id=$this->uri->segment(4);

		if(isset($_POST['btn_add_crt_member']))

		{

			$this->form_validation->set_rules('cct_firstname','First Name','required');

			$this->form_validation->set_rules('cct_lastname','Last Name','required');

			$this->form_validation->set_rules('crt_position','Postion','required');

			$this->form_validation->set_rules('crt_email','Email','required');

			$this->form_validation->set_rules('crt_no','Contact Number','required');

				if($this->form_validation->run())

				{

					$cct_firstname=$this->input->post('cct_firstname');

					$cct_lastname=$this->input->post('cct_lastname');

					$crt_position=$this->input->post('crt_position');

					$crt_no=$this->input->post('crt_no');

					

					

					$input_array=array('crt_first_name'=>$cct_firstname,'crt_last_name'=>$cct_lastname,'crt_position'=>$crt_position,'crt_mobile'=>$crt_no,'cc_id'=>$this->session->userdata('logged_cc_login_id'));

					if($this->master_model->updateRecord('cf_crisis_response_team',$input_array,array('login_id'=>$login_id)))

					{

						$this->session->set_flashdata('success','Crisis Responce Team Member Updated successufully');

						redirect(base_url().'cc/crisisteam/managecrt/');

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating Crisis Responce Team Member.');

						redirect(base_url().'cc/crisisteam/managecrt/');

					}

				}

		}

		$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data=array('page_title'=>'Update Crisis Team Member','error'=>'','middle_content'=>'update-crt','success'=>'','crt_record'=>$crt_record);

		$this->load->view('cc/cc-view',$data);

	}

	

	public function details()

	{

		$login_id=$this->uri->segment(4);

		

		$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data=array('page_title'=>'Crisis Team Member details','error'=>'','middle_content'=>'details-crt',

		'success'=>'','crt_record'=>$crt_record);

		

		$this->load->view('crt/crt-view',$data);

	}

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

	}





	#--------------------------------------------->>dashboard view loading<<-------------------------------------

	public function index()

	{
		$my_class=$this;

		$waiting_crtmem=$waiting_stkmem=$cc_document=$cc_response_plan=array();

		$crt_id=$this->session->userdata('logged_crt_login_id');

		$cc_id=$this->session->userdata('team_cc');

		$org_id=$this->session->userdata('crt_selected_orgnaization');

		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){


			//paginate recall config
			$where_recall=array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0');

			$open_recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'));

			$this->load->library('pagination');

			$config['base_url'] = base_url().'crt/dashboard/index/r/';
			$config['total_rows'] = count($open_recall);
			$config['per_page'] = 1;
			$config['uri_segment'] = 5;

			$config['full_tag_open'] = ' <div class="btn-group">';
			$config['full_tag_close'] = '</div>';

			$config['display_pages'] = TRUE;
			$config['first_link'] = FALSE;
			$config['last_link'] = FALSE;

			$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
			$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
			$config['next_tag_close'] = '</div>';

			$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
			$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
			$config['prev_tag_close'] = '</div>';

			$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
			$config['num_tag_close'] = '</div>';

			$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
			$config['cur_tag_close'] = '</b></div>';


			$this->pagination->initialize($config);


			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

			$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'),$page,$config["per_page"]);



		}
		else{ //active is not recall but continuity

			//paginate recall config
			$where_recall=array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0');

			$open_recall = $this->master_model->getRecords('cf_continuity',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'));

			$this->load->library('pagination');

			$config['base_url'] = base_url().'crt/dashboard/index/r/';
			$config['total_rows'] = count($open_recall);
			$config['per_page'] = 1;
			$config['uri_segment'] = 5;

			$config['full_tag_open'] = ' <div class="btn-group">';
			$config['full_tag_close'] = '</div>';

			$config['display_pages'] = TRUE;
			$config['first_link'] = FALSE;
			$config['last_link'] = FALSE;

			$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
			$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
			$config['next_tag_close'] = '</div>';

			$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
			$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
			$config['prev_tag_close'] = '</div>';

			$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
			$config['num_tag_close'] = '</div>';

			$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
			$config['cur_tag_close'] = '</b></div>';


			$this->pagination->initialize($config);


			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

			$recall = $this->master_model->getRecords('cf_continuity',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'),$page,$config["per_page"]);

		}



		//get recall time
		if(count($recall) > 0){
			$recall_initiated = new DateTime($recall[0]['initiation_date']);

			$nowtime = $this->common_model->userdatetime();

			$recall_closed = new DateTime($nowtime);
			$difference = $recall_initiated->diff($recall_closed);

			$sec_y = $difference->y === 0 ? '0' : $difference->y*12*30*24*60*60;
			$sec_m = $difference->m === 0 ? '0' : $difference->m*30*24*60*60;
			$sec_d = $difference->d === 0 ? '0' : $difference->d*24*60*60;
			$sec_h = $difference->h === 0 ? '0' : $difference->h*60*60;
			$sec_i = $difference->i === 0 ? '0' : $difference->i*60;

			$the_sec = $sec_y + $sec_m + $sec_d + $sec_h + $sec_i + $difference->s;

		}
		else{
			$the_sec = 0;
		}



		//.paginate recall config

		$uniquetime= time().mt_rand();
		$currency_code = $this->master_model->getRecords('countries_currency',array('currencyCode !='=>''));
		$recall_accumulate = $this->accumulate($recall);

		if(count($recall) > 0){
			$wher_cost_cat = array(
				'cc_id'=>$this->session->userdata('logged_parent_crt'),
				'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
				'recall_id'=>$recall[0]['id']
			);

			$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));

		}else{
			$cost_category = array();
		}


		/* fetch  crt members*/
		$parent_crt = $this->session->userdata('logged_parent_crt');


//		$whr=array('cc_id'=>$parent_crt,'org_id'=>$org_id);
//
//		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
//
//		$this->db->order_by('cf_crisis_response_team.login_id','DESC');

		$all_crt = $this->common_model->get_org_crts($cc_id,$org_id);
		//$all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);


		$ccinfo=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$parent_crt));


		$main_status=$this->common_model->get_main_status($this->session->userdata('logged_parent_crt'),$org_id);



		if($main_status=="Pre-Incident Phase")

		{

			$data=array(
				'page_title'=>'Dashboard',
				'middle_content'=>'dashboard',
				'error'=>'',
				'success'=>'',
				'ccinfo'=>$ccinfo,
				'recall'=>$recall,
				'open_recall'=>$open_recall,
				'my_class'=>$my_class,
				'all_crt'=>$all_crt,

				'uniquetime'=>$uniquetime,
				'recall_accumulate'=>$recall_accumulate,
				'currency_code'=>$currency_code,
				'cost_category'=>$cost_category,

				'the_sec'=>$the_sec
			);

		}

		else

		{

			/* fetch all task assign to logged CRT  */

			$whr_arr=array('task.crt_id'=>$crt_id,'scn.scenario_status'=>'1','scn.org_id'=>$org_id,'task.task_status'=>'1');

			$order=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');



			$this->db->join('cf_task as task','task.scenario_id=scn.scenario_id');

			$this->db->join('cf_crisis_response_team as team','team.login_id=task.crt_id');

			$my_task=$this->master_model->getRecords('cf_scenario as scn',$whr_arr,'',$order);

			/* fetch all team task */

			$whr_arr2=array('task.crt_id != '=>$crt_id,'scn.scenario_status'=>'1','scn.org_id'=>$org_id);

			$this->db->join('cf_task as task','task.scenario_id=scn.scenario_id');

			$this->db->join('cf_crisis_response_team as team','team.login_id=task.crt_id');

			$team_task=$this->master_model->getRecords('cf_scenario as scn',$whr_arr2,'',$order);


			/* fetch initiated scenarion for displying task and team assign for it */

			$crtinfo=$this->master_model->getRecords('cf_crisis_response_team',array('crt_id'=>$crt_id));

			$initiated_scn=$this->common_model->get_initiated_scenario($parent_crt,$org_id);



			/* fetch scenario */

			$whr_arr1=array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'scenario_status'=>'1');

			$scenario=$this->master_model->getRecords('cf_scenario',$whr_arr1,'*',array('scenario_id'=>'ASC'));



			$data=array(
				'page_title'=>'Dashboard',
				'middle_content'=>'dashboard',
				'error'=>'',
				'success'=>'',
				'my_task'=>$my_task,
				'team_task'=>$team_task,
				'scenario'=>$scenario,
				'ccinfo'=>$ccinfo,
				'initiated_scn'=>$initiated_scn,
				'recall'=>$recall,
				'open_recall'=>$open_recall,
				'my_class'=>$my_class,
				'all_crt'=>$all_crt,

				'uniquetime'=>$uniquetime,
				'recall_accumulate'=>$recall_accumulate,
				'currency_code'=>$currency_code,
				'cost_category'=>$cost_category,

				'the_sec'=>$the_sec
			);

		}

		if(isset($_POST['add_message']))

		{

			$this->form_validation->set_rules('common_message','Message','required|xss_clean');

			if($this->form_validation->run())

			{

				$common_message=$this->input->post('common_message',true);

				$scenario_id=$this->input->post('scenario_id',true);

				$sender_id=$this->session->userdata('logged_crt_login_id');

				$this->db->set('send_date','now()',FALSE);

				$ins_array=array('scenario_id'=>$scenario_id,'sender_id'=>$sender_id,'common_message'=>$common_message);

				if($this->master_model->insertRecord('common_messages',$ins_array))

				{

					$this->session->set_flashdata('success','Message send successfully.');

					redirect(base_url().'crt/dashboard');

				}

			}

		}

		$this->load->view('crt/crt-view',$data);

	}

	#---------------------------------------------->>Admin login page<<-----------------------------------------

	public function login()

	{

		$error="";

		if($this->session->userdata('logged_crt_login_id')!="")

		{

			redirect(base_url().'crt/dashboard');

		}

		else

		{

			if(isset($_POST['btn_cc_login']))

			{

				$this->form_validation->set_rules('user_name','Username','required|xss_clean');

				$this->form_validation->set_rules('pass_word','Password','required|xss_clean');



				if($this->form_validation->run())

				{

					$chk_arr=array('email_id'=>$this->input->post('user_name',true),

									'pass_word'=>md5($this->input->post('pass_word',true)));





					$this->db->join('cf_crisis_response_team','cf_login_master.login_id=cf_crisis_response_team.login_id');

					$row=$this->master_model->getRecords('cf_login_master',$chk_arr);



					if(count($row) > 0)

					{

						$name_of_org=$this->master_model->getRecords('organization_master',array('organization_id'=>$row[0]['org_id']),'organization_name');

						$user_data=array('logged_crt_login_id'=>$row[0]['login_id'],

										 'logged_crt_email_id'=>$row[0]['crt_email'],

										 'logged_parent_crt'=>$row[0]['cc_id'],

										 'logged_user_type'=>$row[0]['user_type'],

										 'logged_display_name'=>$row[0]['crt_first_name'].' '.$row[0]['crt_last_name'],

										 'logged_user_level'=>$row[0]['user_level'],

										 'crt_selected_orgnaization'=>$row[0]['org_id'],

										 'crt_selected_organization_name'=>$name_of_org[0]['organization_name']);



						$this->session->set_userdata($user_data);







						redirect(base_url().'crt/dashboard/index');

					}

					else

					{$error="Invalid Credentials";}

				}

				else

				{$error=$this->form_validation->error_string();}

			}

			$data=array('page_title'=>'CRT Login','error'=>$error,'success'=>'','middle_content'=>'index');

			$this->load->view('crt/index',$data);

		}

	}

	public function selectorganization(){

		$crt_consultant_id = $this->session->userdata('crt_consultant_id');
		$org_id = $this->session->userdata('crt_selected_orgnaization');
		$login_id = $this->session->userdata('logged_crt_login_id');
		$crt_email = $this->session->userdata('logged_crt_email_id');
		$crt_orgs = $this->common_model->getcrtorgs($crt_email);


		if($crt_consultant_id == ''){
			redirect(base_url().'crt/dashboard/selectmodule');
		}



		if(isset($_POST['btn_select_org'])){

			$this->form_validation->set_rules('org_name','Organization Name','required|xss_clean');

			if($this->form_validation->run()){

				$org_name = $this->input->post('org_name');
				$org_id = $org_name;

				$this->session->set_userdata('crt_selected_orgnaization',$org_name);



				$org_info = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id),'*');



				//check if status is demo
				$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$org_info[0]['cc_id']));
				$user_status = $cc_info[0]['user_type'];
				$remaining = 0;

				if($user_status != 'live'){

					if(count($cc_info) > 0){
						foreach($cc_info as $r=>$value){
							if($value['trial_end'] != '0000-00-00 00:00:00'){

								$date1 = new DateTime(date('Y-m-d H:i:s'));
								$date2 = new DateTime($value['trial_end']);
								$remaining = $date1->diff($date2)->format("%d");

							}

						}
					}

					if($remaining <= 0){
						$this->session->set_flashdata('error','Organization subscription expired. Contact your coordinator.');
						redirect(base_url().'crt/dashboard/selectorganization');
						return false;
					}//subscription expires

				}



				$this->session->set_userdata('crt_selected_organization_name',$org_info[0]['organization_name']);
				$this->session->set_userdata('cc_selected_orgnaization',$org_name);
				$this->session->set_userdata('crt_selected_orgnaization',$org_name);
				$this->session->set_userdata('logged_parent_crt',$org_info[0]['cc_id']);
				$this->session->set_userdata('team_cc',$org_info[0]['cc_id']);

				$crt_email = $this->session->userdata('logged_crt_email_id');
				$crt_arr = array(
					'crt_email'=>$this->master_model->encryptIt($crt_email),
					'cc_id'=>$org_info[0]['cc_id'],
					'org_id'=>$org_id,
				);

				$row = $this->master_model->getRecords('cf_crisis_response_team', $crt_arr);

				$update_crt_arr = array(
					'logged_crt_login_id'=>$row[0]['login_id'],
					'location'=>$row[0]['location'],
					'my_id'=>$row[0]['login_id'],
					'timezone'=>$row[0]['timezone'],
					'country_id'=>$row[0]['country_id'],
					'forum_last_visit'=>$row[0]['forum_last_visit'],
					'group_id'=>$row[0]['group_id'],
					'logged_user_type'=>$row[0]['user_type'],
					'logged_display_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
					'first_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']),
					'last_name'=>$this->master_model->decryptIt($row[0]['crt_last_name']),
				);

				$this->session->set_userdata($update_crt_arr);


				$timezone = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$org_info[0]['cc_id']),'*');

				$this->session->set_userdata('timezone',$timezone[0]['timezone']);

				redirect(base_url().'crt/dashboard/selectmodule');

			}
			else{
				$this->session->set_flashdata('error','Please select organization.');
				redirect(base_url().'crt/dashboard/selectorganization');
			}
		}


		$data = array(
			'page_title'=>"Select Organization",
			'middle_content'=>'select-organisation',
			'success'=>'',
			'error'=>'',
			'crt_orgs'=>$crt_orgs
		);

		$this->load->view('crt/select-organisation',$data);

	}

	public function selectmodule() {

		$this->crt_check_session->checksessionvalue(); //added to individual function but not to forgotpassword

		$org_id = $this->session->userdata('crt_selected_orgnaization');
		$crt_id = $this->session->userdata('logged_crt_login_id');
		$crt_email = $this->session->userdata('logged_crt_email_id');

		if($org_id == ''){
			redirect('crt/dashboard/selectorganization');
		}


		$main_module = $this->common_model->getcrtorg_modules($crt_email, $org_id);
		$org_modules = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));

//		print_r($main_module);
//
//		return false;



		// if(isset($_POST['btn_select_org'])){
		//
		// 	$this->form_validation->set_rules('org_module','Module','required|xss_clean');
		//
		// 	if($this->form_validation->run()){

				$org_module = '5b0763'; //$this->input->post('org_module');
				$active_module = substr($org_module, 0, 1);
				$cc_id = $this->session->userdata('team_cc');
				$crt_id = $this->session->userdata('logged_crt_login_id');
				$org_id = $this->session->userdata('crt_selected_orgnaization');

				$date_added = $this->common_model->userdatetime($cc_id);

				//set values to store
				$arr = array(
					'org_id'=>$org_id,
					'cc_id'=>$cc_id,
					'crt_id'=>$crt_id,
					'module_id'=>$active_module,
					'type'=>'0', //login
					'date_added'=>$date_added
				);

				//insert arr to audit_log table
				$this->master_model->insertRecord('audit_log', $arr);

				$this->session->set_userdata('org_module',$org_module);

				redirect(base_url().'crt/dashboard/');


		// 	}
		// 	else{
		// 		$this->session->set_flashdata('error','Please select module.');
		// 		redirect(base_url().'crt/dashboard/selectmodule');
		// 	}
		// }


		// $data = array(
		// 	'page_title'=>"Select Module",
		// 	'middle_content'=>'select-module',
		// 	'success'=>'',
		// 	'error'=>'',
		// 	'main_module'=>$main_module,
		// 	'org_modules'=>$org_modules
		// );
		//
		// $this->load->view('crt/select-module',$data);



	}



	#---------------------------------------------->>CC Forgot password<<-----------------------------------------
	public function forgotpassword()

	{

		$success="";

		$error="";

		$data=array('page_title'=>"Forget Password",'middle_content'=>'forget-password','success'=>'','error'=>'');

		if(isset($_POST['btn_forget']))

		{

			$this->form_validation->set_rules('user_name','Email address','trim|required|xss_clean|valid_email');

			if($this->form_validation->run())

			{

				$email=$this->input->post('user_name',true);

				$whr=array('email_id'=>$email);

				$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_login_master.login_id');

				$email_id=$this->master_model->getRecords('cf_login_master',$whr,'*');



				$whr=array('id'=>'1');

				$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

				    //generate random password

					$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

					$pass = array(); //remember to declare $pass as an array

					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

					for ($i = 0; $i < 8; $i++)

					{

						$n = rand(0, $alphaLength);

						$pass[] = $alphabet[$n];

					}

					$random_pass= implode($pass);



				if(count($email_id) > 0)

				{	//print_r($email_id); exit;

					$this->master_model->updateRecord('cf_login_master',array('pass_word'=>md5($random_pass)),

					array('login_id'=>$email_id[0]['login_id']));



					$info_arr=array('from'=>$adminemail[0]['recovery_email'],

									'to'=>$email,

									'subject'=>'Password Recovery',

									'view'=>'forget-password-mail-to-cc');



					$other_info=array('password'=>$random_pass,

									  'email'=>$email,

									  'first_name'=>$email_id[0]['crt_first_name'],

									  'last_name'=>$email_id[0]['crt_last_name']);



					if($this->email_sending->sendmail($info_arr,$other_info))

					{

						$this->master_model->updateRecord('cf_login_master',array('pass_word'=>md5($random_pass)),

						array('login_id'=>$email_id[0]['login_id']));

						$this->session->set_flashdata('success','Password Recovery mail send successfully.');

						redirect(base_url().'crt/dashboard/login');

					}

				}

				else

				{

					$this->session->set_flashdata('error','Email Address is invalid.');

					redirect(base_url().'crt/dashboard/forgotpassword');

				}

			}

			else

			{

				$this->session->set_flashdata('error',$this->form_validation->error_string());

			}

		}

		$this->load->view('crt/forget-password',$data);

	}

	#----------------------------------------------->>Change Task status<<------------------------------------------------

	public function changestatus()

	{

		$task_id=$this->uri->segment(4);

		$adminemail=$this->master_model->getRecords('email_id_master');



		/* get cc info */

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.cc_id');

		$cc_info=$this->master_model->getRecords('cf_task as task',array('task.task_id'=>$task_id));

		/* get crt info */

		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_task.crt_id');

		$this->db->join('cf_scenario','cf_scenario.scenario_id=cf_task.scenario_id');

		$task_info=$this->master_model->getRecords('cf_task',array('task_id'=>$task_id));

		$nowtime = $this->common_model->userdatetime();

		if($this->master_model->updateRecord('cf_task',array('task_status'=>'2'),array('task_id'=>$task_id)))

		{
			//insert task log
			$taskslog= array(
				'scenario_id'=>$task_info[0]['scenario_id'],
				'task_id'=>$task_info[0]['task_id'],
				'task'=>$task_info[0]['task'],
				'crt_id'=>$task_info[0]['crt_id'],
				'cc_id'=>$task_info[0]['cc_id'],
				'org_id'=>$task_info[0]['org_id'],
				'current_response'=>$task_info[0]['current_response'],
				'completed_date'=>$nowtime
				);

			$login_id=$this->master_model->insertRecord('log_task_master',$taskslog,true);

			/*$info_arr=array('from'=>$adminemail[0]['contact_email'],

						'to'=>$cc_info[0]['crt_email'],

						'subject'=>'Task completion Mail',

						'view'=>'task_completion_mail_to_cc');



			 $other_info=array(

						  'first_name'=>$cc_info[0]['crt_first_name'],

						  'last_name'=>$cc_info[0]['crt_last_name'],

						  'scenario_name'=>$task_info[0]['scenario'],

						  'task_name'=>$task_info[0]['task'],

						  'crt_name'=>$task_info[0]['crt_first_name'].' '.$task_info[0]['crt_last_name']);



			$this->email_sending->sendmail($info_arr,$other_info);*/



			$this->session->set_flashdata('success',' Task successfully marked as completed.');

			redirect(base_url().'crt/dashboard/index');

		}

	}

	#----------------------------------------------->>cc Logout<<------------------------------------------------

	public function logout()

	{

		//$this->session->sess_destroy();

		//redirect(base_url().'crt');

		$org_module = $this->session->userdata('org_module');
		$active_module = substr($org_module, 0, 1);
		$cc_id = $this->session->userdata('team_cc');
		$crt_id = $this->session->userdata('logged_crt_login_id');
		$org_id = $this->session->userdata('crt_selected_orgnaization');

		$date_added = $this->common_model->userdatetime();

		//set values to store
		$arr = array(
			'org_id'=>$org_id,
			'cc_id'=>$cc_id,
			'crt_id'=>$crt_id,
			'module_id'=>$active_module,
			'type'=>'1', //logout
			'date_added'=>$date_added
		);

		//insert arr to audit_log table
		$this->master_model->insertRecord('audit_log', $arr);



		$this->session->unset_userdata('logged_crt_login_id');

		$this->session->unset_userdata('logged_crt_email_id');

		$this->session->unset_userdata('logged_parent_crt');

		$this->session->unset_userdata('logged_user_type');

		$this->session->unset_userdata('logged_display_name');

		$this->session->unset_userdata('logged_user_level');

		$this->session->unset_userdata('crt_selected_orgnaization');

		$this->session->unset_userdata('cc_selected_orgnaization');

		$this->session->unset_userdata('crt_selected_organization_name');

		$this->session->unset_userdata('forum_last_visit');

		$this->session->unset_userdata('country_id');

		$this->session->unset_userdata('timezone');

		$this->session->unset_userdata('team_cc');

		$this->session->unset_userdata('group_id');

		$this->session->unset_userdata('crt_consultant_id');

		$this->session->unset_userdata('org_module');


		redirect(base_url().'signin/');

	}



	public function completescn()

	{

		$scenario_id=$this->uri->segment(4);

		/* first complete scenario and also complete task */

		$this->master_model->updateRecord('cf_scenario',array('scenario_status'=>'2'),array('scenario_id'=>$scenario_id));



		$this->master_model->updateRecord('cf_task',array('task_status'=>'2'),array('scenario_id'=>$scenario_id));



		redirect(base_url().'crt/dashboard');

	}



	/*message details view */

	public function read()

	{

		$message_id=$this->uri->segment(4);

		$my_class=$this;

		$this->db->join('cf_scenario','cf_scenario.scenario_id=common_messages.scenario_id');

		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=common_messages.sender_id');

		$msg_details=$this->master_model->getRecords('common_messages',array('com_message_id'=>$message_id));



		$data=array('page_title'=>'Message Details','middle_content'=>'common-messages-details','msg_details'=>$msg_details,

				'my_class'=>$my_class);

		$this->load->view('crt/crt-view',$data);

	}


	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);

			return $crtname;
		}
		else{
			return 'Unexisting User';
		}


	}




	//recall cost monitor
	public function currencycode($id) {

		$currency = $this->master_model->getRecords('countries_currency',array('idCountry'=>$id));
		if(count($currency) > 0){
			return $currency[0]['currencyCode'];
		}else{
			return 'Currency Unknown';
		}
	}


	public function accumulate($recall){


		if(count($recall) == 0){
			$myaccumulate = 'No Cost Items';
			return $myaccumulate;
		}
		else{

			$recall_items = $this->master_model->getRecords('cf_cost_category_item',array('recall_id'=>$recall[0]['id']),'*',array('currency'=>'ASC'));

			$myaccumulate = '';
			if(count($recall_items) > 0){

				$mycurr = $recall_items[0]['currency'];
				$totalcurr = 0;
				$m = 0;

				foreach($recall_items as $it=>$ems){
					if($mycurr != $ems['currency']){

						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2).' + ';

						$mycurr = $ems['currency'];
						$totalcurr = $ems['item_cost'];


					}else{

						$totalcurr += $ems['item_cost'];
					}

					if((count($recall_items) - 1) == $m){
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2);
					}

					$m++;
				}


			}else{
				$myaccumulate .= 'No Cost Items';
			}
			return $myaccumulate;

		}


	}




}

?>

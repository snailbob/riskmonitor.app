<?php

class Document extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('upload');

		$this->load->helper('download');

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

	}

	

	public function update()

	{
		
		
		$file_id = $this->uri->segment(4);

		$the_file = $this->master_model->getRecords('cf_file_upload',array('file_upload_id'=>$file_id));

		if(isset($_POST['upload_docc']))

		{
			
			$this->form_validation->set_rules('comment','Comment','required');

	
	
			if($this->form_validation->run())

			{

				$comment = $this->input->post('comment');

				if($_FILES['file_upload_name']['name']!="" && $_FILES['file_upload_name']['error']==0)
	
				{
						
					$doc_config=array('file_name'=>uniqid().$_FILES['file_upload_name']['name'],
	
									'allowed_types'=>'doc|docx|pdf|txt',
	
									'upload_path'=>'uploads/crisis-document/',
	
									'max_size'=>'0');
	
					$this->upload->initialize($doc_config);
	
					//print_r($_FILES['file_upload_name']);exit;
	
					if($this->upload->do_upload('file_upload_name'))
	
					{
	
						$upload_data=$this->upload->data();
	
						$file_name=$upload_data['file_name'];
	
						$insert_array=array(
							'current_file_name'=>$file_name
							
						);
	
						if($this->master_model->updateRecord('cf_file_upload',$insert_array,array('file_upload_id'=>$file_id)))
	
						{	
							$insert_array2=array(
								'cf_file_name'=>$file_name,
								'editor_id'=>$this->session->userdata('logged_crt_login_id'),
								'cf_file_id'=>$file_id,
								'comment'=>$comment,
								'date_updated'=>date('Y-m-d H:i:s',time())
							);
							
							if ($file_his_id = $this->master_model->insertRecord('cf_file_history',$insert_array2,true))
							
							
							$this->session->set_flashdata('success',' Document successfully uploaded.');
	
						//	redirect(base_url().'crt/document/scan/'.$file_his_id);	
							redirect(base_url().'crt/document/manage/');	
	
						}
	
					}
	
					else
	
					{ 
	
						$this->session->set_flashdata('error',$this->upload->display_errors());
	
						redirect(base_url().'crt/document/manage');	
	
						
	
					}
				}

				else

				{

					$this->session->set_flashdata('error','The uploaded file exceeds the maximum allowed size.');

					redirect(base_url().'crt/document/upload');

				}


			}

			

			

		}
		
		$data = array(
			'page_title'=>'Upload Document',
			'middle_content'=>'upload-document',
			'the_file'=>$the_file

		);

		$this->load->view('crt/crt-view',$data);

	}

	

	// Scan documents
	public function scan()

	{
		$thedoc = $this->uri->segment(4);

		$up_doc=$this->master_model->getRecords('cf_file_history',array('id'=>$thedoc));

		// Config to scan.
		$api    = 'https://scan.metascan-online.com/v2/file';
		$apikey = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		$file   = base_url();
		$file.='uploads/crisis-document/';
		$file.=$up_doc[0]['cf_file_name'];
		
		// Build headers array.
		$headers = array(
			'apikey: '.$apikey,
			'filename: '.basename($file)
		);
		
		// Build options array.
		$options = array(
			CURLOPT_URL     => $api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_POST        => true,
			CURLOPT_POSTFIELDS  => file_get_contents($file),
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		// Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$response = json_decode(curl_exec($ch), true);


		$insert_array = array(
			'data_id'=>$response['data_id'],
			'rest_ip'=>$response['rest_ip']
		);
		$this->master_model->updateRecord('cf_file_history',$insert_array,array('id'=>$thedoc));

		redirect(base_url().'crt/document/manage/'.$thedoc);

	}

	
	// scan response document
	public function scan_response()

	{
		$thedoc = $_POST['file_id'];

		$up_doc=$this->master_model->getRecords('cf_file_history',array('id'=>$thedoc));
		
		

		//Config for req.
		$req_api = 'https://';
		$req_api .= $up_doc[0]['rest_ip']; //'scan05.metascan-online.com:443/v2'; //$rest_ip;
		$req_api .= '/file/';
		$req_api .=  $up_doc[0]['data_id']; //'48271edc06b34edeb86cf27d3fc54377';//$data_id;
		
		$apikey     = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		
		//Build headers array.
		$headers = array(
			'apikey: '.$apikey
		);
		
		//Build options array.
		$options = array(
			CURLOPT_URL     => $req_api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		$response = "";
		//Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		do {
			$response = json_decode(curl_exec($ch), true);
		}
		while ($response["scan_results"]["progress_percentage"] != 100);
		
		//print_r($response);
		
		$this->master_model->updateRecord('cf_file_history',array('scan_result'=>$response['scan_results']['scan_all_result_a']),array('id'=>$thedoc));
		
		
		echo '<p class="lead" style="margin-bottom: 5px;">';
		//doc icon
		$fileext = explode ('.',$up_doc[0]['cf_file_name']);
		if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
		
		echo '<i class="fa fa-file-pdf-o text-red"></i> ';
		
		}
		
		else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
		
		echo '<i class="fa fa-file-word-o text-blue"></i> ';
		
		}
		
		else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
		
		echo '<i class="fa fa-file-text-o text-muted"></i> ';
		
		}
		
		
		echo substr($up_doc[0]['cf_file_name'],13,50);		
		echo '</p>';
		echo '<p class="lead"  style="margin-bottom: 5px;">';
		echo 'Over all Scan Result: '.$response['scan_results']['scan_all_result_a'];		
		
		echo '</p>';
		
		
		echo ' <div class="table-responsive"> <table class="table table-hover table-bordered table-green" id="response-table"> <thead> <tr>  <th width="25%">Antivirus</th>  <th width="25%">Result</th>  <th width="25%">Scan Time</th> <th width="25%">Update</th> </tr> </thead> <tbody>';
								
								
		foreach ($response['scan_results'] as $key => $scan_details){
			if(is_array($scan_details)){
				foreach($scan_details as $subkey => $subvalue){
					echo '<tr> <td>'.$subkey.'</td>';

					if(is_array($subvalue)){
						//foreach($subvalue as $key => $subsubvalue){
							echo '<td class="text-center">';
							if($subvalue['threat_found']==""){
								echo '<i class="fa fa-check-circle text-success" title="No Threat Found"></i>';
							}
							else{
								echo '<i class="fa fa-exclamation-circle text-danger" title="Threat Found"></i><br>'.$subvalue['threat_found'];
							}
							echo '</td>';
							//echo '<td>'.$subvalue['threat_found'].'</td>'; $subvalue['scan_result_i']
							echo '<td>'.$subvalue['scan_time'].' ms</td>';
							echo '<td>'.substr($subvalue['def_time'], 0, 10).'</td>';
						//}
						
					echo '</tr>';
					} else {
						echo $subkey.' '.$subvalue."<br />";
					}
				}
			} else {
			//	echo $key.': '.$scan_details."<br />";
			}
		}	
		
		echo ' </tbody> </table> </div><p class="small hidden">Virus scan results from metascan-online.com</p>';
			
	}	
	
	// Manage documents
	public function manage()

	{  

		$all_doc=$this->master_model->getRecords('cf_file_upload',array('cc_id'=>$this->session->userdata['logged_parent_crt'],'org_id'=>$this->session->userdata['crt_selected_orgnaization']));

		$data=array('page_title'=>"Manage Crises Documents",'middle_content'=>'manage-document','success'=>'',

		'error'=>'','all_doc'=>$all_doc);

		$this->load->view('crt/crt-view',$data);

	}


	// Download document
	public function download()

	{

		$doc_name=$this->uri->segment(4);

		$data=file_get_contents("uploads/crisis-document/".$doc_name);

		$name=substr($doc_name,13,50);

		force_download($name,$data);

	}

	

}

?>
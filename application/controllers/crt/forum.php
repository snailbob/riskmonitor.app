<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Forum extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

		

	}

	

	//------------------------------------post incident

	public function index(){

		if($this->session->userdata('logged_crt_login_id')!="" && $this->session->userdata('crt_selected_orgnaization')!="" )

		{

		//unread forum post count
		$forum_post=$this->master_model->getRecords('forum_post_master',array('cc_id'=>$this->session->userdata('team_cc'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'date_created >'=>$this->session->userdata('forum_last_visit'),'post_parent'=>'0'),'*',array('date_created'=>'DESC'));



			$data=array(
				'page_title'=>'Activity Log',
				'middle_content'=>'forum_view',
				'error'=>'',
				'success'=>'',
				'forum_post'=>$forum_post
			);

		}
	

		$this->load->view('crt/crt-view',$data);
	}


}

?>
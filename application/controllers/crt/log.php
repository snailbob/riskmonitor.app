<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Log extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

		

	}

	

	//------------------------------------add crt

	public function index(){
		$crt_id = $this->session->userdata('logged_crt_login_id');
		$crtinfo=$this->master_model->getRecords('cf_crisis_response_team',array('crt_id'=>$crt_id ));
	
//		$ccinfo=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_parent_crt'));

		if($crt_id !="" && $this->session->userdata('crt_selected_orgnaization')!="" )

		{
			
			$my_class=$this;
			
			$cc_id= $this->session->userdata('logged_parent_crt');

			$org_id=$this->session->userdata('crt_selected_orgnaization');

			/* fetch initiated scenario for displying common messages */

			$whr_arr1=array('cc_id'=>$cc_id,'org_id'=>$org_id,'completed_date !='=>'0000-00-00 00:00:00'); 

			//$scenarios=$this->master_model->getRecords('cf_scenario',$whr_arr1,'*',array('scenario_id'=>'ASC'));
			$scenarios=$this->master_model->getRecords('log_scene_master',$whr_arr1,'*',array('completed_date'=>'DESC'));

			/* fetch initiated scenarion for displying task and team assign for it */

			$initiated_scn=$this->common_model->get_initiated_scenario($cc_id,$org_id);

			

			/* fetch all task assign to CC himself */

			$whr_arr2=array('task.task_status'=>'2');

			$order=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');

			$this->db->join('cf_task as task','task.scenario_id=scn.scenario_id');

			$this->db->join('cf_crisis_response_team as team','team.login_id=task.crt_id');

			$completed_task=$this->master_model->getRecords('cf_scenario as scn',$whr_arr2,'',$order);

			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			
			//display closed and remidiated cases
			$case_list=$this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status !='=>'0'),'',array('date_closed'=>'DESC'));




				//paginate recall config
				$where_recall=array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0');
			
			
				//active module is recall
				if($active_mod == '5'){
					$close_recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'1'),'*',array('id'=>'DESC'));
					
				}
				else{
					$close_recall = $this->master_model->getRecords('cf_continuity',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'1'),'*',array('id'=>'DESC'));
				}
				
			
				
				$this->load->library('pagination');
	
				$config['base_url'] = base_url().'cc/log/index/r/';
				$config['total_rows'] = count($close_recall);
				$config['per_page'] = 1; 
				$config['uri_segment'] = 5;
				
				$config['full_tag_open'] = ' <div class="btn-group">';
				$config['full_tag_close'] = '</div>';
				
				$config['display_pages'] = TRUE;
				$config['first_link'] = FALSE;
				$config['last_link'] = FALSE;
		
				$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
				$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
				$config['next_tag_close'] = '</div>';
				
				$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
				$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
				$config['prev_tag_close'] = '</div>';
				
				$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
				$config['num_tag_close'] = '</div>';
				
				$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
				$config['cur_tag_close'] = '</b></div>';
				
				
				
				$this->pagination->initialize($config); 
				
				
				
				$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
				
				//$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'),$page,$config["per_page"]);
				
				
				//active module is recall
				if($active_mod == '5'){
					$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'1'),'*',array('id'=>'DESC'),$page,$config["per_page"]);
					
				}
				else{
					$recall = $this->master_model->getRecords('cf_continuity',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'1'),'*',array('id'=>'DESC'),$page,$config["per_page"]);
				}
				
				


		
				$uniquetime= time().mt_rand();
				$currency_code = $this->master_model->getRecords('countries_currency',array('currencyCode !='=>''));
				$recall_accumulate = $this->accumulate($recall);
				
				if(count($recall) > 0){
					$wher_cost_cat = array(
						'cc_id'=>$this->session->userdata('logged_parent_crt'),
						'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
						'recall_id'=>$recall[0]['id']
					);
					
					$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));
		
				}else{
					$cost_category = array();
				}
							


			/*//get closed recall incident
			$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'closed'=>'1'),'*',array('closed_date'=>'DESC'));
			
			//paginate recall config
			$where_recall=array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'1');
			$inboxx = $this->master_model->getRecordCount('cf_recall',$where_recall);*/

			$data=array(
				'page_title'=>'Activity Log',
				'middle_content'=>'log_view',
				'error'=>'','success'=>'',
				'initiated_scn'=>$initiated_scn,
				'scenarios'=>$scenarios,
				'completed_task'=>$completed_task,
				'recall'=>$recall,
				'open_recall'=>$close_recall,
				//'inboxx'=>$inboxx,
				'my_class'=>$my_class,
				'case_list'=>$case_list,
					
				'uniquetime'=>$uniquetime,
				'recall_accumulate'=>$recall_accumulate,
				'currency_code'=>$currency_code,
				'cost_category'=>$cost_category
			);

		}
	

		$this->load->view('crt/crt-view',$data);
	}



	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		return $crtname=$this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);
		
	}
	

	//recall cost monitor
	public function currencycode($id) {
		
		$currency = $this->master_model->getRecords('countries_currency',array('idCountry'=>$id));
		if(count($currency) > 0){
			return $currency[0]['currencyCode'];
		}else{
			return 'Currency Unknown';
		}
	}
	
	
	public function accumulate($recall){
	
	
		if(count($recall) == 0){
			$myaccumulate = 'No Cost Items';
			return $myaccumulate;
		}
		else{

			$recall_items = $this->master_model->getRecords('cf_cost_category_item',array('recall_id'=>$recall[0]['id']),'*',array('currency'=>'ASC'));
	
			$myaccumulate = '';
			if(count($recall_items) > 0){
				
				$mycurr = $recall_items[0]['currency'];
				$totalcurr = 0;
				$m = 0;
				
				foreach($recall_items as $it=>$ems){
					if($mycurr != $ems['currency']){
							
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2).' + ';
						
						$mycurr = $ems['currency'];
						$totalcurr = $ems['item_cost'];
						
	
					}else{
						
						$totalcurr += $ems['item_cost'];
					}
				
					if((count($recall_items) - 1) == $m){
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2);
					}
					
					$m++;
				}
				
						
			}else{
				$myaccumulate .= 'No Cost Items';
			}
			return $myaccumulate;
			
		}
		
		
	}
	
	
	

}

?>
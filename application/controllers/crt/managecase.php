<?php

class Managecase extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('crt_check_session');

		$this->crt_check_session->checksessionvalue();

	}

	

	public function index()

	{

		
		$case_list=$this->master_model->getRecords('case_master',array('crt_id'=>$this->session->userdata('logged_crt_login_id'),'cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'0'));

		$reported_cases=$this->master_model->getRecords('case_report_master',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'1'));



		$data=array('page_title'=>'Manage Case','middle_content'=>'manage-case','case_list'=>$case_list,'reported_cases'=>$reported_cases);

		$this->load->view('crt/crt-view',$data);

	}

	
	
	

	public function add()

	{

		if(isset($_POST['add_case']))

		{

			$this->form_validation->set_rules('case_title','Case Title','required|xss_clean');
			$this->form_validation->set_rules('case_det','Case Details','required|xss_clean');
			$this->form_validation->set_rules('crt_mem','Response Team Member','required|xss_clean');

			if($this->form_validation->run())

			{

				$crt_id=$this->input->post('crt_mem',true);

				$insrt_arr=array(
					'cc_id'=>$this->session->userdata('logged_parent_crt'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'gen_id'=>$this->input->post('gen_id',true),
					'cc_case_id'=>$this->input->post('cc_case_id',true),
					'case_title'=>$this->input->post('case_title',true),
					'case_details'=>$this->input->post('case_det',true),
					'crt_id'=>$this->input->post('crt_mem',true)
				);

				if($case_id=$this->master_model->insertRecord('case_master',$insrt_arr,TRUE))

				{
					
					//get admin email
					$whr=array('id'=>'1');
					$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

					//get crt name email
					$whrr=array('login_id'=>$crt_id);
					$crtinfo=$this->master_model->getRecords('cf_crisis_response_team',$whrr,'*');


					$info_arr=array('from'=>$adminemail[0]['recovery_email'],
	
						'to'=>$crtinfo[0]['crt_email'],

						'subject'=>'New Case',

						'view'=>'crt_new_case'
					);

				

					 $other_info=array(

						'first_name'=>$crtinfo[0]['crt_first_name'],
						
						'last_name'=>$crtinfo[0]['crt_last_name']
					);

				

					$this->email_sending->sendmail($info_arr,$other_info);

					
					
					
					
					
					
					$this->session->set_flashdata('success','Case successfully added.');
					redirect(base_url().'cc/managecase');

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding scenario');

					redirect('cc/scenario/add');

				}

			}

		}

		
		$excase_id=$this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization')),'cc_case_id');
		if(count($excase_id) > 0){

			$numItems = count($excase_id);
			$i = 0;
			foreach($excase_id as $key=>$value) {
			  if(++$i === $numItems) {
				  
				$cc_case_id = $value['cc_case_id'] + 1;

			  }
			}  

				
		}
		
		else{
			$cc_case_id = 1;
		}
		
//		$crt_list=$this->master_model->getRecords('cf_crisis_response_team',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization')),'*');

		/* fetch CR list to assing task, CRT will be of current selected organization */

		$whr_arr=array('crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1');

		$whr_str="(crt.org_id=".$this->session->userdata('logged_parent_crt')." OR crt.org_id=0) AND (login.user_level='1')";

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$this->db->where($whr_str,NULL,FALSE);

		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);



		$data=array('page_title'=>'Add Scenario','middle_content'=>'add-case','cc_case_id'=>$cc_case_id,'crt_list'=>$crt_list);

		$this->load->view('cc/cc-view',$data);

	}

	

	public function reportdoc()

	{
		
		$this->load->library('upload');
		
		$case_id=$this->uri->segment(4);
		
		if(isset($_POST['add_casedoc'])){

			$config['upload_path'] = './uploads/crisis-document';
			$config['allowed_types'] = 'pdf|doc|docx|txt';
			$config['max_size']	= '2000';
			$config['file_name']= uniqid().$_FILES['file_upload_name']['name'];
	
			$this->upload->initialize($config);
	
			if ( ! $this->upload->do_upload('file_upload_name'))
			{
				
			$whr_arr=array(
				'cc_id'=>$this->session->userdata('logged_parent_crt'),
				'crt_id'=>$this->session->userdata('logged_crt_login_id'),
				'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
				'case_id'=>$case_id
			);
			
			$casedocs=$this->master_model->getRecords('case_document_report',$whr_arr);
				
				
			$data=array('page_title'=>'Campaigns','middle_content'=>'add-case-reportdoc','error'=>$this->upload->display_errors(),'casedocs'=>$casedocs);
	
			$this->load->view('crt/crt-view',$data);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$upload_data=$this->upload->data();
				$file_name=$upload_data['file_name'];
			   //Success in validation of all fields.
			   
			   $docdetails = $this->input->post('c_duration',true);
			   
				$data = array(
				   'case_id' => $case_id,
				   'crt_id' => $this->session->userdata('logged_crt_login_id'),
				   'cc_id' => $this->session->userdata('logged_parent_crt') ,
				   'org_id' => $this->session->userdata('crt_selected_orgnaization'),
				   'file_desc' => $this->input->post('file_desc',true),
				   'document' =>  $file_name
				);		
				//$this->db->insert('case_document_report', $data); 
				$thedoc = $this->master_model->insertRecord('case_document_report',$data,true);

				
				//$this->session->set_flashdata('success','Document successfully added.');
				redirect(base_url().'crt/managecase/scan/'.$thedoc.'/'.$case_id);
			   
			}

		}
		else{
			
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			//'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_document_report',$whr_arr);
			
		$data=array('middle_content'=>'add-case-reportdoc','success'=>'','error'=>'','casedocs'=>$casedocs);

		$this->load->view('crt/crt-view',$data);
		}

	}
	
	

	// Scan documents

	public function scan()

	{
		$thedoc = $this->uri->segment(4);

		$thecase = $this->uri->segment(5);

		$up_doc=$this->master_model->getRecords('case_document_report',array('id'=>$thedoc, 'org_id'=>$this->session->userdata['crt_selected_orgnaization']));

		// Config to scan.
		$api    = 'https://scan.metascan-online.com/v2/file';
		$apikey = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		$file   = base_url();
		$file.='uploads/crisis-document/';
		$file.=$up_doc[0]['document'];
		
		// Build headers array.
		$headers = array(
			'apikey: '.$apikey,
			'filename: '.basename($file)
		);
		
		// Build options array.
		$options = array(
			CURLOPT_URL     => $api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_POST        => true,
			CURLOPT_POSTFIELDS  => file_get_contents($file),
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		// Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$response = json_decode(curl_exec($ch), true);
		
		
		$insert_array = array(
			'data_id'=>$response['data_id'],
			'rest_ip'=>$response['rest_ip']
		);
		$this->master_model->updateRecord('case_document_report',$insert_array,array('id'=>$thedoc));

		redirect(base_url().'crt/managecase/reportdoc/'.$thecase.'/'.$thedoc);
//		$data=array('page_title'=>"Manage Crises Documents",'middle_content'=>'scan-document','success'=>'',

//		'error'=>'','response'=>$response);

//		$this->load->view('cc/cc-view',$data);

	}
	
	// scan response document
	public function scan_response()

	{
		$thedoc = $_POST['file_id'];

		$up_doc=$this->master_model->getRecords('case_document_report',array('id'=>$thedoc, 'org_id'=>$this->session->userdata['crt_selected_orgnaization']));
		
		

		//Config for req.
		$req_api = 'https://';
		$req_api .= $up_doc[0]['rest_ip']; //'scan05.metascan-online.com:443/v2'; //$rest_ip;
		$req_api .= '/file/';
		$req_api .=  $up_doc[0]['data_id']; //'48271edc06b34edeb86cf27d3fc54377';//$data_id;
		
		$apikey     = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		
		//Build headers array.
		$headers = array(
			'apikey: '.$apikey
		);
		
		//Build options array.
		$options = array(
			CURLOPT_URL     => $req_api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		$response = "";
		//Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		do {
			$response = json_decode(curl_exec($ch), true);
		}
		while ($response["scan_results"]["progress_percentage"] != 100);
		
		//print_r($response);
		
		$this->master_model->updateRecord('case_document_report',array('scan_result'=>$response['scan_results']['scan_all_result_a']),array('id'=>$thedoc));
		
		
		echo '<p class="lead" style="margin-bottom: 5px;">';
		//doc icon
		$fileext = explode ('.',$up_doc[0]['document']);
		if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
		
		echo '<i class="fa fa-file-pdf-o text-red"></i> ';
		
		}
		
		else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
		
		echo '<i class="fa fa-file-word-o text-blue"></i> ';
		
		}
		
		else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
		
		echo '<i class="fa fa-file-text-o text-muted"></i> ';
		
		}
		
		
		echo substr($up_doc[0]['document'],13,50);		
		echo '</p>';
		echo '<p class="lead"  style="margin-bottom: 5px;">';
		echo 'Over all Scan Result: '.$response['scan_results']['scan_all_result_a'];		
		
		echo '</p>';
		
		
		echo ' <div class="table-responsive"> <table class="table table-hover table-bordered table-green" id="response-table"> <thead> <tr>  <th width="25%">Antivirus</th>  <th width="25%">Result</th>  <th width="25%">Scan Time</th> <th width="25%">Update</th> </tr> </thead> <tbody>';
								
								
		foreach ($response['scan_results'] as $key => $scan_details){
			if(is_array($scan_details)){
				foreach($scan_details as $subkey => $subvalue){
					echo '<tr> <td>'.$subkey.'</td>';
					if(is_array($subvalue)){
						//foreach($subvalue as $key => $subsubvalue){
							echo '<td class="text-center">';
							if($subvalue['threat_found']==""){
								echo '<i class="fa fa-check-circle text-success" title="No Threat Found"></i>';
							}
							else{
								echo '<i class="fa fa-exclamation-circle text-danger" title="Threat Found"></i><br>'.$subvalue['threat_found'];
							}
							echo '</td>';
							//echo '<td>'.$subvalue['threat_found'].'</td>'; $subvalue['scan_result_i']
							echo '<td>'.$subvalue['scan_time'].' ms</td>';
							echo '<td>'.substr($subvalue['def_time'], 0, 10).'</td>';
						//}
						
					echo '</tr>';
					} else {
						echo $subkey.' '.$subvalue."<br />";
					}
				}
			} else {
			//	echo $key.': '.$scan_details."<br />";
			}
		}	
		
		echo ' </tbody> </table> </div><p class="small hidden">Virus scan results from metascan-online.com</p>';
			
	}
	
	
	
	
	
	//report hazard analysis
	public function reporthazard()

	{
		$case_id=$this->uri->segment(4);

		if(isset($_POST['add_hazard']))

		{

			$this->form_validation->set_rules('hazard','Hazard Analysis','required|xss_clean');

			if($this->form_validation->run())

			{
				
				$insrt_arr=array(
				   'case_id' => $case_id,
				   'crt_id' => $this->session->userdata('logged_crt_login_id'),
				   'cc_id' => $this->session->userdata('logged_parent_crt') ,
				   'org_id' => $this->session->userdata('crt_selected_orgnaization'),
				   'hazard'=>$this->input->post('hazard',true)
				);

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_parent_crt'),
					'crt_id'=>$this->session->userdata('logged_crt_login_id'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
				
					$this->master_model->updateRecord('case_report_master',array('hazard'=>$this->input->post('hazard',true)),$whr_arr);
					$this->session->set_flashdata('success','Hazard Analysis successfully updated.');
					redirect('crt/managecase/reporthazard/'.$case_id);
				
				}
				
				else if($this->master_model->insertRecord('case_report_master',$insrt_arr,TRUE))

				{
					
					$this->session->set_flashdata('success','Hazard Analysis successfully added.');
					redirect('crt/managecase/reporthazard/'.$case_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding scenario');

					redirect('crt/managecase/reporthazard/'.$case_id);

				}
				
			}
		}
		//get exisiting  report hazard
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);
		
		$data=array('middle_content'=>'add-case-reporthazard','success'=>'','error'=>'','casedocs'=>$casedocs);

		$this->load->view('crt/crt-view',$data);
	}
	
	
	
	
	//report root cause
	public function reportcause()

	{
		$case_id=$this->uri->segment(4);

		if(isset($_POST['add_reportcause']))

		{

			$this->form_validation->set_rules('root_cause','Root-cause Analysis','required|xss_clean');

			if($this->form_validation->run())

			{
				
				$insrt_arr=array(
				   'case_id' => $case_id,
				   'crt_id' => $this->session->userdata('logged_crt_login_id'),
				   'cc_id' => $this->session->userdata('logged_parent_crt') ,
				   'org_id' => $this->session->userdata('crt_selected_orgnaization'),
				   'root_cause'=>$this->input->post('root_cause',true)
				);

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_parent_crt'),
					'crt_id'=>$this->session->userdata('logged_crt_login_id'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
				
					$this->master_model->updateRecord('case_report_master',array('root_cause'=>$this->input->post('root_cause',true)),$whr_arr);
					$this->session->set_flashdata('success','Root-Cause Analysis successfully updated.');
					redirect('crt/managecase/reportcause/'.$case_id);
				
				}
				
				else if($this->master_model->insertRecord('case_report_master',$insrt_arr,TRUE))

				{
					
					$this->session->set_flashdata('success','Root-Cause Analysis successfully added.');
					redirect('crt/managecase/reportcause/'.$case_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding Root-Cause Analysis');

					redirect('crt/managecase/reportcause/'.$case_id);

				}
				
			}
		}
		
		
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);
		
		$data=array('middle_content'=>'add-case-report-root-cause','success'=>'','error'=>'','casedocs'=>$casedocs);

		$this->load->view('crt/crt-view',$data);

	}
	
	
	//report impact analysis
	public function reportimpact()

	{
		$case_id=$this->uri->segment(4);

		if(isset($_POST['add_reportcause']))

		{

			$this->form_validation->set_rules('impact_to_bsn','Impact to business operations','required|xss_clean');
			$this->form_validation->set_rules('impact_to_stk','Impact to stakeholders','required|xss_clean');

			if($this->form_validation->run())

			{
				
				$insrt_arr=array(
				   'case_id' => $case_id,
				   'crt_id' => $this->session->userdata('logged_crt_login_id'),
				   'cc_id' => $this->session->userdata('logged_parent_crt') ,
				   'org_id' => $this->session->userdata('crt_selected_orgnaization'),
				   'impact_to_stk' => $this->input->post('impact_to_stk',true),
				   'impact_to_bsn'=> $this->input->post('impact_to_bsn',true)
				);

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_parent_crt'),
					'crt_id'=>$this->session->userdata('logged_crt_login_id'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
				
					$this->master_model->updateRecord('case_report_master',array('impact_to_bsn'=>$this->input->post('impact_to_bsn',true),'impact_to_stk'=>$this->input->post('impact_to_stk',true)),$whr_arr);
					$this->session->set_flashdata('success','Impact Analysis successfully updated.');
					redirect('crt/managecase/reportimpact/'.$case_id);
				
				}
				
				else if($this->master_model->insertRecord('case_report_master',$insrt_arr,TRUE))

				{
					
					$this->session->set_flashdata('success','Impact Analysis successfully added.');
					redirect('crt/managecase/reportimpact/'.$case_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding Impact Analysis');

					redirect('crt/managecase/reportimpact/'.$case_id);

				}
				
			}
		}
		
		
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);
		
		$data=array('middle_content'=>'add-case-report-impact','success'=>'','error'=>'','casedocs'=>$casedocs);

		$this->load->view('crt/crt-view',$data);

	}
	
	
	//report control analysis
	public function reportcontrol()

	{
		$case_id=$this->uri->segment(4);

		if(isset($_POST['add_reportcontrol']))

		{

			$this->form_validation->set_rules('r_control','Control Analysis','required|xss_clean');

			if($this->form_validation->run())

			{
				
				$insrt_arr=array(
				   'case_id' => $case_id,
				   'crt_id' => $this->session->userdata('logged_crt_login_id'),
				   'cc_id' => $this->session->userdata('logged_parent_crt') ,
				   'org_id' => $this->session->userdata('crt_selected_orgnaization'),
				   'r_control'=>$this->input->post('r_control',true)
				);

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_parent_crt'),
					'crt_id'=>$this->session->userdata('logged_crt_login_id'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
				
					$this->master_model->updateRecord('case_report_master',array('control'=>$this->input->post('r_control',true)),$whr_arr);
					$this->session->set_flashdata('success','Control Analysis successfully updated.');
					redirect('crt/managecase/reportcontrol/'.$case_id);
				
				}
				
				else if($this->master_model->insertRecord('case_report_master',$insrt_arr,TRUE))

				{
					
					$this->session->set_flashdata('success','Control Analysis successfully added.');
					redirect('crt/managecase/reportcontrol/'.$case_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding Control Analysis');

					redirect('crt/managecase/reportcontrol/'.$case_id);

				}
				
			}
		}
		
		
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);
		
		$data=array('middle_content'=>'add-case-report-control','success'=>'','error'=>'','casedocs'=>$casedocs);

		$this->load->view('crt/crt-view',$data);

	}
	
	
	//report recommendation
	public function reportrecommend()

	{
		$case_id=$this->uri->segment(4);

		if(isset($_POST['add_reportrecommend']))

		{

			$this->form_validation->set_rules('r_recommend','Recommendation','required|xss_clean');

			if($this->form_validation->run())

			{
				
				$insrt_arr=array(
				   'case_id' => $case_id,
				   'crt_id' => $this->session->userdata('logged_crt_login_id'),
				   'cc_id' => $this->session->userdata('logged_parent_crt') ,
				   'org_id' => $this->session->userdata('crt_selected_orgnaization'),
				   'recommendation'=>$this->input->post('recommendation',true)
				);

				$whr_arr=array(
					'cc_id'=>$this->session->userdata('logged_parent_crt'),
					'crt_id'=>$this->session->userdata('logged_crt_login_id'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'case_id'=>$case_id
				);
				
				$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);

				if(count($casedocs) > 0){ 
				
					$this->master_model->updateRecord('case_report_master',array('recommendation'=>$this->input->post('r_recommend',true)),$whr_arr);
					$this->session->set_flashdata('success','Recommendation successfully updated.');
					redirect('crt/managecase/reportrecommend/'.$case_id);
				
				}
				
				else if($this->master_model->insertRecord('case_report_master',$insrt_arr,TRUE))

				{
					
					$this->session->set_flashdata('success','Recommendation successfully added.');
					redirect('crt/managecase/reportrecommend/'.$case_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding recommendation');

					redirect('crt/managecase/reportrecommend/'.$case_id);

				}
				
			}
		}
		
		
		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);
		
		$casedocs=$this->master_model->getRecords('case_report_master',$whr_arr);
		$casedocu=$this->master_model->getRecords('case_document_report',$whr_arr);
		
		$data=array('middle_content'=>'add-case-report-recommend','success'=>'','error'=>'','casedocs'=>$casedocs,'casedocu'=>$casedocu);

		$this->load->view('crt/crt-view',$data);

	}
	
	
	
	// submit report
	public function submitreport()

	{
		$case_id=$this->uri->segment(4);


		$case_report=$this->master_model->getRecords('case_master',array('id'=>$case_id),'*');
		
		
		$cc_details=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_parent_crt')),'*');


		$whr=array('id'=>'1');
		$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

		

		$info_arr=array(
			'from'=>$adminemail[0]['recovery_email'],

			'to'=>$this->master_model->decryptIt($cc_details[0]['crt_email']),
	
			'subject'=>'Case Report Submitted',
	
			'view'=>'crt_report_case'
		);

	

		 $other_info=array(

		  'first_name'=>$this->master_model->decryptIt($cc_details[0]['crt_first_name']),

		  'last_name'=>$this->master_model->decryptIt($cc_details[0]['crt_last_name']),

		  'case_gen'=>$case_report[0]['gen_id']
		);

	

		$this->email_sending->sendmail($info_arr,$other_info);



		$whr_arr=array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
			'case_id'=>$case_id
		);

		//change status to submitted
		$this->master_model->updateRecord('case_report_master',array('status'=>'1'),$whr_arr);



		$this->session->set_flashdata('success',' Report successfully submitted.');

		redirect('crt/managecase/reportrecommend/'.$case_id);



	}

	

	// Delete document

	public function delete()

	{

		$doc_id=$this->uri->segment(4);

		$doc_name=$this->uri->segment(5);	

		$case_id=$this->uri->segment(6);

		if($this->master_model->deleteRecord('case_document_report','id',$doc_id))

		{

			@unlink('uploads/crisis-document/'.$doc_name);

			$this->session->set_flashdata('success',' Document successfully deleted.');

			redirect(base_url().'crt/managecase/reportdoc/'.$case_id);

		}

	}


}

?>
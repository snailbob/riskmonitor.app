<?php
class Message extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('crt_check_session');
		$this->crt_check_session->checksessionvalue();
	}
	
	
	public function filename_extension($filename) {
		
		$pos = strrpos($filename, '.');
		if($pos===false) {
			return false;
		} else {
			return substr($filename, $pos+1);
		}
	}

	

	public function download()

	{
		$this->load->helper('download');

		$doc_name=$this->uri->segment(4);

		$data=file_get_contents("uploads/message-uploads/".$doc_name);

		$name=$doc_name;

		force_download($name,$data);

	}
		
	
	
	public function upload(){
	
		/*if (isset($_FILES["photoimg"])) {
			// Your Custom PHP Code Here
			$return = json_encode($_FILES["photoimg"]);
			echo $return;
		}*/
		
		$output_dir = "uploads/message-uploads/";
		
		
		if(isset($_FILES["myfile"]))
		{
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0)
			{
			  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
			}
			else
			{

				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);
	
				$insrt_arr = array (
						'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
					'cc_id'=>$this->session->userdata('logged_crt_login_id'),
					'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
					'temp_id'=>$_POST['uniquetime'],
					'message_id'=>'0' //$cc_case_id
				);					
				if ($file_id = $this->master_model->insertRecord('private_message_file',$insrt_arr,true)){
					
						echo ' <a href="'.base_url().'uploads/message-uploads/'.$thefilename.'" target="_blank" id = "file_del'.$file_id.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$_FILES["myfile"]["name"].'</a> <span onclick="$(\'#file_del'.$file_id.'\').hide();$(this).hide();delete_the_file(event.target.id);" style="cursor: default" id="delete_file'.$file_id.'">&times;</span>';
				
				}
				else{
					
				  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
				  
				}
			}
		
		}		
		/*else{
			
			$data=array('page_title'=>"Inbox",'middle_content'=>'test-upload');
			$this->load->view('cc/cc-view',$data);
		}*/
	}
		
	
	

	public function delete_message_file(){
		
		$thenum = $_POST['thenum'];

	    $file = $this->master_model->getRecords('private_message_file',array('file_upload_id'=>$thenum));
		
		$file_name = $file[0]['file_upload_name'];
		
		if($this->master_model->deleteRecord('private_message_file','file_upload_id',$thenum))

		{

			@unlink('uploads/message-uploads/'.$file_name);

			//$this->session->set_flashdata('success',' Document successfully deleted.');
			echo 'success';

		}

	
	}
		
	
	
	public function inbox()
	{
		
		$my_class=$this;
		$whr_arr1=array('receiver_id'=>$this->session->userdata('logged_crt_login_id'),'is_receiver_del'=>'0');
		$inboxx=$this->master_model->getRecordCount('private_messages',$whr_arr1);
		
		$this->load->library('pagination');

		$config['base_url'] = base_url().'crt/message/inbox';
		$config['total_rows'] = $inboxx;
		$config['per_page'] = 10; 
		$config['uri_segment'] = 4;
		
		$config['full_tag_open'] = ' <div class="btn-group btn-group-sm">';
		$config['full_tag_close'] = '</div>';
		
		$config['display_pages'] = TRUE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['next_tag_close'] = '</div>';
		
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
		$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['prev_tag_close'] = '</div>';
		
		$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['num_tag_close'] = '</div>';
		
		$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
		$config['cur_tag_close'] = '</b></div>';
		
		
		
		$this->pagination->initialize($config); 
		
		
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
     
		$whr_arr=array('msg.receiver_id'=>$this->session->userdata('logged_crt_login_id'),'msg.is_receiver_del'=>'0');
		$select="msg.*,crt.crt_first_name,crt.crt_last_name,crt.login_id";
		$order=array('msg.send_date'=>'DESC');
		$this->db->join('cf_crisis_response_team as crt','crt.login_id=msg.sender_id');
	    $inbox = $this->master_model->getRecords('private_messages as msg',$whr_arr,$select,$order,$page,$config["per_page"]);
       // $data["links"] = $this->pagination->create_links();
		$data=array('page_title'=>"Inbox",'middle_content'=>'inbox','my_class'=>$my_class,'inbox'=>$inbox,'count_inbox'=>$inboxx);
		$this->load->view('crt/crt-view',$data);
		
		/*
		$my_class=$this;
		$whr_arr=array('msg.receiver_id'=>$this->session->userdata('logged_crt_login_id'),'msg.is_receiver_del'=>'0');
		$select="msg.*,crt.crt_first_name,crt.crt_last_name,crt.login_id";
		$order=array('msg.send_date'=>'DESC');
		$this->db->join('cf_crisis_response_team as crt','crt.login_id=msg.sender_id');
		$inbox=$this->master_model->getRecords('private_messages as msg',$whr_arr,$select,$order);
		
		$data=array('page_title'=>"Inbox",'middle_content'=>'inbox','my_class'=>$my_class,'inbox'=>$inbox);
		$this->load->view('crt/crt-view',$data);
		*/
		
	}
	

		
	public function sentmail()
	{
		$my_class=$this;
		$whr_arr1=array('sender_id'=>$this->session->userdata('logged_crt_login_id'),'is_sender_del'=>'0');
		$inboxx=$this->master_model->getRecordCount('private_messages',$whr_arr1);
		
		$this->load->library('pagination');

		$config['base_url'] = base_url().'crt/message/sentmail';
		$config['total_rows'] = $inboxx;
		$config['per_page'] = 10; 
		$config['uri_segment'] = 4;
		
		$config['full_tag_open'] = ' <div class="btn-group btn-group-sm">';
		$config['full_tag_close'] = '</div>';
		
		$config['display_pages'] = TRUE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['next_tag_close'] = '</div>';
		
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
		$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['prev_tag_close'] = '</div>';
		
		$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['num_tag_close'] = '</div>';
		
		$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
		$config['cur_tag_close'] = '</b></div>';
		
		
		
		$this->pagination->initialize($config); 
		
		
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
     
		$whr_arr=array('msg.sender_id'=>$this->session->userdata('logged_crt_login_id'),'msg.is_sender_del'=>'0');
		$select="msg.*,crt.crt_first_name,crt.crt_last_name,crt.login_id";
		$order=array('msg.send_date'=>'DESC');
		
		$this->db->join('cf_crisis_response_team as crt','crt.login_id=msg.receiver_id');
	    $outbox = $this->master_model->getRecords('private_messages as msg',$whr_arr,$select,$order,$page,$config["per_page"]);
       // $data["links"] = $this->pagination->create_links();
		
		$data=array('page_title'=>"Inbox",'middle_content'=>'outbox','my_class'=>$my_class,'outbox'=>$outbox,'count_inbox'=>$inboxx);
		$this->load->view('crt/crt-view',$data);
	}
	
	
	
		
	
	
	public function outbox()
	{
		redirect('crt/message/sentmail');
		/*
		$my_class=$this;
		
		$whr_arr=array('msg.sender_id'=>$this->session->userdata('logged_crt_login_id'),'msg.is_sender_del'=>'0');
		$select="msg.*,crt.crt_first_name,crt.crt_last_name,crt.login_id";
		$order=array('msg.send_date'=>'DESC');
		
		$this->db->join('cf_crisis_response_team as crt','crt.login_id=msg.receiver_id');
		$outbox=$this->master_model->getRecords('private_messages as msg',$whr_arr,$select,$order);
		//echo $this->db->last_query(); exit;
		$data=array('page_title'=>"Outbox",'middle_content'=>'outbox','my_class'=>$my_class,'outbox'=>$outbox);
		$this->load->view('crt/crt-view',$data);
		*/
		
	}
	
	public function trash()
	{	
		$my_class=$this;
		
		$whr_str="(msg.sender_id=".$this->session->userdata('logged_crt_login_id')." AND msg.is_sender_del='1' AND msg.is_sender_del_trash='0') OR (msg.receiver_id=".$this->session->userdata('logged_crt_login_id')." AND msg.is_receiver_del='1' AND msg.is_receiver_del_trash='0')";
		$this->db->where($whr_str,NULL,FALSE);
		$trash=$this->master_model->getRecords('private_messages as msg');
		
		$data=array('page_title'=>"Trash",'middle_content'=>'trash','trash'=>$trash,'my_class'=>$my_class);
		$this->load->view('crt/crt-view',$data);
	}
	 
	// Permanantly delete message from trash
	public function permnantdelete()
	{ 
		$cnt_select=count($_POST['selectedId']);
		
		for($i=0;$i<$cnt_select;$i++)
		{
			$message=$this->master_model->getRecords('private_messages',array('message_id'=>$_POST['selectedId'][$i]));
			
			if(count($message) > 0)
			{
				if($message[0]['sender_id']==$this->session->userdata('logged_crt_login_id'))
				{
					$update_array=array('is_sender_del_trash'=>'1');
					$this->master_model->updateRecord('private_messages',$update_array,
					array('message_id'=>$_POST['selectedId'][$i]));
						if($message[0]['is_receiver_del_trash']=='1')
						{	
							$this->master_model->deleteRecord('private_messages','message_id',$_POST['selectedId'][$i]);	
						}
				}
				else if($message[0]['receiver_id']==$this->session->userdata('logged_crt_login_id'))
				{
					$update_array=array('is_receiver_del_trash'=>'1');
					$this->master_model->updateRecord('private_messages',$update_array,
					array('message_id'=>$_POST['selectedId'][$i]));
						if($message[0]['is_sender_del_trash']=='1')
						{
							$this->master_model->deleteRecord('private_messages','message_id',$_POST['selectedId'][$i]);	
						}
				}
			}
		}
		$this->session->set_flashdata('success',' Message(s) has been deleted successfully.');
		redirect('crt/message/trash');
	}
	
	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		return $crtname=$this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);
		
	}
		
	public function compose()
	{
		$my_class=$this;
		
		if(isset($_POST['send_msg']))
		{
			$this->form_validation->set_rules('message_receiver','Select To','required|xss_clean');
			$this->form_validation->set_rules('subject','Subject','required|xss_clean');
			$this->form_validation->set_rules('txt_message','Message','required');
			if($this->form_validation->run())
			{
				$message_receiver = $this->input->post('message_receiver',true);
				
				$receiver_id_email = explode(' ',$message_receiver);

				$subject=$this->input->post('subject',true);
				$txt_message=$this->input->post('txt_message');
				$insrt_arr=array('sender_id'=>$this->session->userdata('logged_crt_login_id'),
					'receiver_id'=>$receiver_id_email[0],'subject'=>$subject,'message'=>$txt_message);
				$this->db->set('send_date','now()',FALSE);
				if($mess_id = $this->master_model->insertRecord('private_messages',$insrt_arr,true))
				{
						//update attached file id
						$this->master_model->updateRecord('private_message_file',array('message_id'=>$mess_id), array('temp_id'=>$_POST['uniquetime']));
					
					
						//email noti send to receiver
						$whr=array('id'=>'1');

						$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

						

						$info_arr=array('from'=>$adminemail[0]['recovery_email'],

									'to'=>$receiver_id_email[1],

									'subject'=>'You have a new message',

									'view'=>'new_message_for_crt');

					

					     $other_info=array(

									  'first_name'=>$receiver_id_email[2],

									  'last_name'=>$receiver_id_email[3]);

					

					    $this->email_sending->sendmail($info_arr,$other_info);
						//.email noti send to receiver
					
					
					$this->session->set_flashdata('success',' Message successfully sent.');
					redirect(base_url().'crt/message/inbox');
				}
				else
				{
					$this->session->set_flashdata('error','Error while sending message');
					redirect(base_url().'crt/message/compose');
				}
				
			}
		}
		$uniquetime= time().mt_rand();
		$data=array('page_title'=>"Compose",'middle_content'=>'compose-message','my_class'=>$my_class,'uniquetime'=>$uniquetime);
		$this->load->view('crt/crt-view',$data);
	}
	
	public function fetch_team_members()
	{
		$whr_arr=array('cc_id'=>$this->session->userdata('logged_parent_crt'));
		$select="login_id,crt_first_name,crt_last_name,crt_email";
		$order=array('crt_first_name'=>'ASC','crt_last_name'=>'ASC');
		$str_whr="(org_id=0 OR org_id=".$this->session->userdata('crt_selected_orgnaization').")";
		
		$this->db->where($str_whr,NULL,FALSE);
		$this->db->where('login_id != ',$this->session->userdata('logged_crt_login_id'));
		$my_team=$this->master_model->getRecords('cf_crisis_response_team',$whr_arr,$select,$order);
		return $my_team;
	}
	
	public function multdelete()
	{
		$cnt_select=count($_POST['selectedId']);
		
		for($i=0;$i<$cnt_select;$i++)
		{
			$message=$this->master_model->getRecords('private_messages',array('message_id'=>$_POST['selectedId'][$i]));
			
			if(count($message) > 0)
			{
				if($message[0]['sender_id']==$this->session->userdata('logged_crt_login_id'))
				{
					$update_array=array('is_sender_del'=>'1');
					$this->master_model->updateRecord('private_messages',$update_array,
					array('message_id'=>$_POST['selectedId'][$i]));
					$redirect_to="outbox";
				}
				else if($message[0]['receiver_id']==$this->session->userdata('logged_crt_login_id'))
				{
					$update_array=array('is_receiver_del'=>'1');
					$this->master_model->updateRecord('private_messages',$update_array,
					array('message_id'=>$_POST['selectedId'][$i]));
					$redirect_to="inbox";	
				}
			}
		}
		$this->session->set_flashdata('success',' Message(s) has been moved to the Trash.');
		redirect('crt/message/'.$redirect_to);
	}
	
	/* mark message as read */
	public function mark_as_read()
	{
		$message_id=$this->input->post('msgid');
		$this->master_model->updateRecord('private_messages',array('is_read'=>'1'),array('message_id'=>$message_id));
		echo 'done';
	}
	
	/*message details view */
	public function read()
	{
		$message_id=$this->uri->segment(4);
		$my_class=$this;
		$msg_details=$this->master_model->getRecords('private_messages',array('message_id'=>$message_id));
		
		$data=array('page_title'=>'Message Details','middle_content'=>'message_details','msg_details'=>$msg_details,
				'my_class'=>$my_class);
		$this->load->view('crt/crt-view',$data);
	}
	
	
}
?>
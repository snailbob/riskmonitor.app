<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recall extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('crt_check_session');
		$this->crt_check_session->checksessionvalue();
		
	}
	public function charmus(){

	}
	
	
	public function fetch_stand_by_message()
	{
		$scenarioid=$this->uri->segment(4);
		$stby_msg=$this->master_model->getRecords('cf_standby_messages',array('scenario_id'=>$scenarioid));
		$stn_messsages=array();

		foreach($stby_msg as $msg)
		{
			$stn_messsages[$msg['stn_msg_id']]=$msg['standby_message'];
		}
		echo json_encode($stn_messsages);
	}
	
	
	
	
	public function mark_task_completed(){
		
		$quest_id = $_POST['question_id'];
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		$the_new_status = '1';
		//active module is recall
		if($active_mod == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps',array('id'=>$quest_id));
			
			$rc = $this->master_model->getRecords('cf_recall',array('id'=>$task[0]['recall_id']));
		}else{
			$task = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$quest_id));
			
			$rc = $this->master_model->getRecords('cf_continuity',array('id'=>$task[0]['recall_id']));
		}
		
			
		//check if the task has been marked completed
		if($task[0]['status'] == '1'){
			$data['result'] = 'was_update';
			echo json_encode($data);
			return false;
		}
		
		$initiation_date = new DateTime($rc[0]['initiation_date']);


		$nowtime = $this->common_model->userdatetime();
		
		
		$task_completed = new DateTime($nowtime);
		$difference = $initiation_date->diff($task_completed);

		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i; //.' '.$difference->s.' ';
		
		
		$task_tot = explode(' ',$interval);
		$unit_time = array(
			'Y : ',
			'Mo : ',
			'D : ',
			'H : ',
			'M'// : ',
			//'S'
		);
		$u = 0; 
		$time_lapse = '';
		foreach($task_tot as $tot){
			if ($tot == 0 ){
			}
			else if ($tot == 1 ){
				$time_lapse .= $tot.' '.$unit_time[$u].' ';
			}
			else{
				$time_lapse .= $tot.' '.$unit_time[$u].' ';
			}
			$u++;
		}
					

		$arr = array(
			'status'=>$the_new_status,
			'date_completed'=>$nowtime,
			'time_lapse'=>$time_lapse
			
		);

		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',$arr,array('id'=>$quest_id));
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps',$arr,array('id'=>$quest_id));
		}
		

		$org_module = $this->session->userdata('org_module');
		$active_module = substr($org_module, 0, 1);
		$cc_id = $this->session->userdata('team_cc');
		$crt_id = $this->session->userdata('logged_crt_login_id');
		$org_id = $this->session->userdata('crt_selected_orgnaization');

		$date_added = $this->common_model->userdatetime();
		
		//set values to store
		$arr = array(
			'org_id'=>$org_id,
			'cc_id'=>$cc_id,
			'crt_id'=>$crt_id,
			'module_id'=>$active_module,
			'type'=>'3', //completed,
			'inci_task_id'=>$quest_id,
			'date_added'=>$date_added
		);
		
		//insert arr to audit_log table
		$this->master_model->insertRecord('audit_log', $arr);
				
		
		

		$data['time_completed'] = 'Time to Complete:<br><span class="text-muted">'.$time_lapse.'</span>';
		$data['result'] = 'success';
		

		
		//check if there's approvers
		$the_approvers = unserialize($task[0]['approvers']);
		$the_approvers_status = unserialize($task[0]['approvers_status']);
		$approvers_count = 0;
		$approvers_status = 0;
		
		$new_task_owner = '';
		
		if(is_array($the_approvers)){
			$approvers_count = count($the_approvers);
		}
		else{
			$the_approvers = array();
		}
		
		if(is_array($the_approvers_status)){
			$approvers_status = count($the_approvers_status);
		}
		else{
			$the_approvers_status = array();
		}
		
		
		//count not yet approved	
		$unapproved_count = count($the_approvers);
		
		if(count($the_approvers) > 0){
			foreach($the_approvers as $tapp){
				if(in_array($tapp, $the_approvers_status)){
					$unapproved_count--;
				}
			}
		}
		
		
		if($unapproved_count > 0) { //$approvers_count != $approvers_status){
			$status_text = 'Awaiting Approval';
			$status_class = 'text-warning';
			
			//array_diff — Computes the difference of arrays
			$result = array_diff($the_approvers, $the_approvers_status);
			
			if(count($result) > 0){
				$new_arr = array();
				foreach($result as $rs){
					$new_arr[] = $rs;
				}
				$new_task_owner = $this->common_model->getname($new_arr[0]);
			}
		}

		else if ($the_new_status == '0'){
			$status_text = 'In Progress';
			$status_class = 'text-danger';
		}
		
		else{
			$status_text = 'Completed';
			$status_class = 'text-success';
		}
		
		$data['action'] = '';
		if($approvers_count == 0){
			$data['action'] = 'show_approvers_modal';
		}

		$data['new_task_owner'] = $new_task_owner;
		$data['status_text'] = $status_text;
		$data['status_class'] = $status_class;

				
		echo json_encode($data);		

	}
	
	public function mark_task_incomplete(){
		
		$quest_id = $_POST['question_id'];
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$the_new_status = '0';

		if($active_mod == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps',array('id'=>$quest_id));
		}
		else{
			$task = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$quest_id));
		}
		
		//check if the task has been marked completed
		if($task[0]['status'] == '0'){
			$data['result'] = 'was_update';
			echo json_encode($data);
			return false;
		}
		
		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',array('status'=>$the_new_status),array('id'=>$quest_id));
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps',array('status'=>$the_new_status),array('id'=>$quest_id));
		}
		
		
		$data['result'] = 'success';
		

		//check if there's approvers
		$the_approvers = unserialize($task[0]['approvers']);
		$the_approvers_status = unserialize($task[0]['approvers_status']);
		$approvers_count = 0;
		$approvers_status = 0;

		
		if(is_array($the_approvers)){
			$approvers_count = count($the_approvers);
		}
		else{
			$the_approvers = array();
		}
		
		if(is_array($the_approvers_status)){
			$approvers_status = count($the_approvers_status);
		}
		else{
			$the_approvers_status = array();
		}
		
		
		//count not yet approved	
		$unapproved_count = count($the_approvers);
		
		if(count($the_approvers) > 0){
			foreach($the_approvers as $tapp){
				if(in_array($tapp, $the_approvers_status)){
					$unapproved_count--;
				}
			}
		}
		
		
		
		if($unapproved_count > 0) { //$approvers_count != $approvers_status){
			$status_text = 'Awaiting Approval';
			$status_class = 'text-warning';

		}

		else if ($the_new_status == '0'){
			$status_text = 'In Progress';
			$status_class = 'text-danger';
		}
		
		else{
			$status_text = 'Completed';
			$status_class = 'text-success';
		}


		$data['new_task_owner'] = ( $task[0]['assigned'] != '' ? $this->common_model->getname($task[0]['assigned']) : '');


		$data['status_text'] = $status_text;
		$data['status_class'] = $status_class;

		echo json_encode($data);
		
	
	}
	
	
	
	public function assign_the_crt(){
		
		$quest_id = $_POST['question_id'];
		$curr_assigned = $_POST['curr_assigned'];
		$message_receiver = $_POST['message_receiver'];


		$nowtime = $this->common_model->userdatetime();

		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		
		//check if assigned has changed
		if($active_mod == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps', array('id'=>$quest_id));
			
			if($task[0]['assigned'] != $curr_assigned){
				echo 'was_update';
				return false;
			}
		}
		else{
			$task = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$quest_id));
			
			if($task[0]['assigned'] != $curr_assigned){
				echo 'was_update';
				return false;
			}
		}


		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',array('assigned'=>$message_receiver,'date_assigned'=>$nowtime),array('id'=>$quest_id));

		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps',array('assigned'=>$message_receiver,'date_assigned'=>$nowtime),array('id'=>$quest_id));

		}
		
		$org_module = $this->session->userdata('org_module');
		$active_module = substr($org_module, 0, 1);
		$cc_id = $this->session->userdata('team_cc');
		$crt_id = $this->session->userdata('logged_crt_login_id');
		$org_id = $this->session->userdata('crt_selected_orgnaization');

		$date_added = $this->common_model->userdatetime();
		
		//set values to store
		$arr = array(
			'org_id'=>$org_id,
			'cc_id'=>$cc_id,
			'crt_id'=>$crt_id,
			'module_id'=>$active_module,
			'type'=>'2', //pickup,
			'inci_task_id'=>$quest_id,
			'date_added'=>$date_added
		);
		
		//insert arr to audit_log table
		$this->master_model->insertRecord('audit_log', $arr);
		
		
		$this->session->set_flashdata('success',' Task has been assigned successfully.');
		echo 'success';
	
	}
	
	
	public function pick_all_tasks(){
		
		$step_no = $_POST['step_no'];
		$recall_id = $_POST['recall_id'];
		$crt_id = $_POST['crt_id'];
		$to_check = $_POST['to_check']; //array
		
		$q = 0;
		foreach($to_check as $tc){
			
			$kyu=$this->master_model->getRecords('cf_recall_steps',array('id'=>$tc));
			
			if ($kyu[0]['assigned'] == ''){
				$this->master_model->updateRecord('cf_recall_steps',array('assigned'=>$crt_id),array('recall_id'=>$recall_id,'id'=>$tc));
			}
			else{
				$q++;
			}

			
			echo $tc.',';
		}
		
			if ($q == 0){
				$this->session->set_flashdata('success',' Task successfully assigned to you.');
			}
			else if($q == count($to_check)){
				$this->session->set_flashdata('error',' Task(s) already assigned.');
			}
			else{
				$this->session->set_flashdata('success',' Tasks successfully assigned to you but there are '.$q.' tasks you selected that has been already assigned.');
			}
		
		echo 'success';
	
	}
	
	
	public function add_message_respond(){
		
		$quest_id = $_POST['question_id'];
		$txt_message = $_POST['txt_message'];
		


		
		$nowtime = $this->common_model->userdatetime();
		
		if ($this->session->userdata('logged_crt_login_id') !=''){
			$the_auth = $this->session->userdata('logged_crt_login_id');
		}
		else{
			$the_auth = $this->session->userdata('logged_crt_login_id');
		}
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',array('answer'=>$txt_message,'assigned'=>$the_auth,'date_updated'=>$nowtime),array('id'=>$quest_id));
		}
		else{
			$this->master_model->updateRecord('cf_continuity_steps',array('answer'=>$txt_message,'assigned'=>$the_auth,'date_updated'=>$nowtime),array('id'=>$quest_id));
		}
		
	//	$this->session->set_flashdata('success',' Task has been Completed');
		echo 'success';
	}
	
	public function get_respond_to_edit(){
		$task_id = $_POST['task_id'];
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){
			$kyu=$this->master_model->getRecords('cf_recall_steps',array('id'=>$task_id));
		}
		else{
			$kyu=$this->master_model->getRecords('cf_continuity_steps',array('id'=>$task_id));
		}
		
		echo $kyu[0]['answer'];
	
	}
	

	public function get_task_response(){
	
		$task_id = $_POST['task_id'];

		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){
			$kyu=$this->master_model->getRecords('cf_recall_steps',array('id'=>$task_id));
		}
		else{
			$kyu=$this->master_model->getRecords('cf_continuity_steps',array('id'=>$task_id));
		}

		$recall_file = $this->master_model->getRecords('cf_recall_steps_file',array('message_id'=>$kyu[0]['id']));
		
        echo '<p class="lead">Task Response</p>';
		echo '<div class="well" style="color: #333; border-radius: 0;">'.nl2br($kyu[0]['answer']).'</div> ';	
		echo '<div class="well" style="color: #333; border-radius: 0;">'.$this->getcrtname($kyu[0]['assigned']).' - '.date_format(date_create($kyu[0]['date_updated']), 'jS F Y, g:ia').'</div>';
		
		if(count($recall_file) > 0){
			echo '<p>File Attached: ';
		}
		
		foreach($recall_file as $ff=>$rff){
			echo ' <a href="'.base_url().'uploads/recallsteps-uploads/'.$rff['file_upload_name'].'" target="_blank" id = "file_del'.$rff['file_upload_id'].'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.substr($rff['file_upload_name'], 13).'</a>';
			
			if($kyu[0]['status'] != '1' && $kyu[0]['assigned'] == $this->session->userdata('logged_crt_login_id') ){
			
			echo ' <span onclick="$(\'#file_del'.$rff['file_upload_id'].'\').hide();$(this).hide();delete_the_recall_file(event.target.id);" style="cursor: default" id="delete_file'.$rff['file_upload_id'].'">&times;</span>';
			}

		}//foreach
		
		echo '</p>';
		
	}
		
	


	public function save_stp3_table()
	{
		$severity = $_POST['severity'];
		$presence = $_POST['presence'];
		$like_dist = $_POST['like_dist'];
		$distribution = $_POST['distribution'];
		$identification = $_POST['identification'];
		$hazard = $_POST['hazard'];
		$step_3_table_id = $_POST['step_3_table_id'];
		
		$step3table_answer = $severity. ' ' .$presence. ' ' .$like_dist. ' ' .$distribution. ' ' .$identification. ' ' .$hazard;
	
	
		$nowtime = $this->common_model->userdatetime();
		
		if ($this->session->userdata('logged_cc_login_id') !=''){
			$the_auth = $this->session->userdata('logged_cc_login_id');
		}
		else{
			$the_auth = $this->session->userdata('logged_crt_login_id');
		}
	
	
		$active_mod  = substr($this->session->userdata('org_module'),0,1);

		//active module is recall
		if($active_mod == '5'){
			$this->master_model->updateRecord('cf_recall_steps',array('answer'=>$step3table_answer,'assigned'=>$the_auth,'date_updated'=>$nowtime),array('id'=>$step_3_table_id));
		}else{
			$this->master_model->updateRecord('cf_continuity_steps',array('answer'=>$step3table_answer,'assigned'=>$the_auth,'date_updated'=>$nowtime),array('id'=>$step_3_table_id));
		}

		echo 'success';
		
	}		


	public function download()

	{
		$this->load->helper('download');

		$doc_name=$this->uri->segment(4);

		$data=file_get_contents("uploads/recallsteps-uploads/".$doc_name);

		$name= $doc_name;

		force_download($name,$data);

	}
	
	
	public function upload(){

		
		$output_dir = "uploads/recallsteps-uploads/";
		
		
		if(isset($_FILES["myfile"]))
		{
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0)
			{
			  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
			}
			else
			{
				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
				$task_id = $_POST['the_question_id'];
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); 
				
					
					$insrt_arr = array (
						'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
						'cc_id'=>$this->session->userdata('logged_crt_login_id'),
						'org_id'=>$this->session->userdata('crt_selected_orgnaization'),
						'temp_id'=>$_POST['uniquetime'],
						'message_id'=>$task_id //$cc_case_id
					);					
					if ($file_id = $this->master_model->insertRecord('cf_recall_steps_file',$insrt_arr,true)){
						
						echo ' <a href="'.base_url().'uploads/recallsteps-uploads/'.$thefilename.'" target="_blank" id = "file_del'.$file_id.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$_FILES["myfile"]["name"].'</a> <span onclick="$(\'#file_del'.$file_id.'\').hide();$(this).hide();delete_the_recall_file(event.target.id);" style="cursor: default" id="delete_file'.$file_id.'">&times;</span>';
					
					}
					else{
					  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
					}
			}
		
		}		
		/*else{
			
			$data=array('page_title'=>"Inbox",'middle_content'=>'test-upload');
			$this->load->view('cc/cc-view',$data);
		}*/
	}
	
	public function delete_message_file(){
		
		$thenum = $_POST['thenum'];

	    $file = $this->master_model->getRecords('cf_recall_steps_file',array('file_upload_id'=>$thenum));
		
		$file_name = $file[0]['file_upload_name'];
		
		if($this->master_model->deleteRecord('cf_recall_steps_file','file_upload_id',$thenum))

		{

			@unlink('uploads/recallsteps-uploads/'.$file_name);

			//$this->session->set_flashdata('success',' Document successfully deleted.');
			echo 'success';

		}

	
	}	
	
	
	
	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));
		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);
			
			return $crtname;
		}
		else{
			return 'Non-existing User';
		}
		
		
	}
	
	//blocker functions
	public function load_blocker_note(){
		$id = $this->uri->segment(4);
		$blocker = $this->master_model->getRecords('cf_recall_blockers',array('id'=>$id));
		
		echo nl2br($blocker[0]['note']);	
	}
	
	
	public function save_new_blocker(){
		$blocker = $_POST['blocker_name'];
		$blocker_issue = $_POST['blocker_issue'];
		$blocker_impact = $_POST['blocker_impact'];
		$recall_id = $_POST['recall_id'];

		$nowtime = $this->common_model->userdatetime();
		
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$themod = 'recall_id';
		}
		else{
			$themod = 'continuity_id';
		}
		
		echo $blocker_impact;
		$arr = array(
			'cc_id'=>$this->session->userdata('logged_parent_crt'),
			'crt_id'=>$this->session->userdata('logged_crt_login_id'),
			$themod=>$recall_id,
			'blocker'=>$blocker,
			'issue'=>$blocker_issue,
			'impact'=>$blocker_impact,
			'status'=>'0',
			'date_created'=>$nowtime
		);
		
		if($this->master_model->insertRecord('cf_recall_blockers',$arr,true)){
			$this->session->set_flashdata('success',' Blocker has been raised successfully.');
			echo 'success';
		}else{
			$this->session->set_flashdata('error',' Something went wrong. Please try again.');
			echo 'error';
		}
	}
		
}
?>
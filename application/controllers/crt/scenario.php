<?php
class Scenario extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('crt_check_session');
		$this->crt_check_session->checksessionvalue();
	}
	
	public function add()
	{
		if(isset($_POST['add_scen']))
		{
			$this->form_validation->set_rules('scenario_desc','Scenario','required|xss_clean');
			if($this->form_validation->run())
			{
				$scenario_desc=$this->input->post('scenario_desc',true);
				$insrt_arr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),
					'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario'=>$scenario_desc,'scenario_status'=>'0');
				if($scenario_id=$this->master_model->insertRecord('cf_scenario',$insrt_arr,TRUE))
				{
					redirect(base_url().'cc/scenario/addtask/'.$scenario_id);
				}
				else
				{
					$this->session->set_flashdata('error','Error while adding scenario');
					redirect('cc/scenario/add');
				}
			}
		}
		
		$data=array('page_title'=>'Add Scenario','middle_content'=>'add-scenario');
		$this->load->view('cc/cc-view',$data);
	}
	
	public function addtask()
	{
		$scenario_id=$this->uri->segment(4);
		
		if(isset($_POST['add_task']))
		{
			$this->form_validation->set_rules('task_desc','Task Description','required|xss_clean');
			$this->form_validation->set_rules('assign_to','Select CRT','required|xss_clean');
			
			if($this->form_validation->run())
			{
				$task_desc=$this->input->post('task_desc',true);
				$assign_to=$this->input->post('assign_to',true);	
				
				$insrt_arr=array('scenario_id'=>$scenario_id,'task'=>$task_desc,'crt_id'=>$assign_to,
							'cc_id'=>$this->session->userdata('logged_cc_login_id'),
							'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'task_status'=>'0');
							
				if($this->master_model->insertRecord('cf_task',$insrt_arr))
				{
					$this->session->set_flashdata('success','Task added successfully');
					redirect(base_url().'cc/scenario/addtask/'.$scenario_id);
				}
				else
				{
					$this->session->set_flashdata('error','Error while adding task');
					redirect(base_url().'cc/scenario/addtask/'.$scenario_id);
				}
			}
		}
		
		/* get last added scenario */
		$scn_info=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenario_id));
		
		/* fetch CR list to assing task, CRT will be of current selected organization */
		$whr_arr=array('crt.org_id'=>$this->session->userdata('cc_selected_orgnaization'),
				'crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1','login.user_level'=>'1');
		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";
		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');
		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');
		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);
		
		$data=array('page_title'=>'Add Task','middle_content'=>'add-task','scn_info'=>$scn_info,
			'crt_list'=>$crt_list);
		$this->load->view('cc/cc-view',$data);
	}
	
	public function managetask()
	{
		$my_task="";
		$team_task="";
		$waiting_crtmem=$waiting_stkmem=$cc_document=$cc_response_plan=array();
		$crt_id=$this->session->userdata('logged_crt_login_id');
		$org_id=$this->session->userdata('crt_selected_orgnaization');
		$main_status=$this->common_model->get_main_status($this->session->userdata('logged_parent_crt'),$org_id);
		
		/* fetch all task assign to logged CRT  */
		$whr_arr=array('task.crt_id'=>$crt_id,'scn.org_id'=>$org_id);
		$order=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');
		
		$this->db->join('cf_task as task','task.scenario_id=scn.scenario_id');
		$this->db->join('cf_crisis_response_team as team','team.login_id=task.crt_id');
		$my_task=$this->master_model->getRecords('cf_scenario as scn',$whr_arr,'',$order);
		/* fetch all team task */
		$whr_arr2=array('task.crt_id != '=>$crt_id,'scn.org_id'=>$org_id);		
		$this->db->join('cf_task as task','task.scenario_id=scn.scenario_id');
		$this->db->join('cf_crisis_response_team as team','team.login_id=task.crt_id');
		$team_task=$this->master_model->getRecords('cf_scenario as scn',$whr_arr2,'',$order);
			
		$data=array('page_title'=>'Manage action plan','middle_content'=>'manage-task','my_task'=>$my_task,'team_task'=>$team_task);
		$this->load->view('crt/crt-view',$data);
	}
	
	/* update task */
	public function update()
	{
		$task_id=$this->uri->segment(4);
		
		if(isset($_POST['add_task']))
		{
			$this->form_validation->set_rules('task_desc','Task Description','required|xss_clean');
			$this->form_validation->set_rules('assign_to','Select CRT','required|xss_clean');
			
			if($this->form_validation->run())
			{
				$task_desc=$this->input->post('task_desc',true);
				$assign_to=$this->input->post('assign_to',true);	
				
				$updt_arr=array('task'=>$task_desc,'crt_id'=>$assign_to);
							
				if($this->master_model->updateRecord('cf_task',$updt_arr,array('task_id'=>$task_id)))
				{
					$this->session->set_flashdata('success','Task updated successfully');
					redirect(base_url().'cc/scenario/update/'.$task_id);
				}
				else
				{
					$this->session->set_flashdata('error','Error while updating task');
					redirect(base_url().'cc/scenario/update/'.$task_id);
				}
			}
		}
		
		
		/* get task added scenario */
		$whr_arr=array('task.task_id'=>$task_id);
		$select="scn.*,task.*";
		$this->db->join('cf_scenario as scn','scn.scenario_id=task.scenario_id','left');
		$task_info=$this->master_model->getRecords('cf_task as task',$whr_arr,$select);
		
		/* fetch CR list to assing task, CRT will be of current selected organization */
		$whr_arr=array('crt.org_id'=>$this->session->userdata('cc_selected_orgnaization'),
				'crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1','login.user_level'=>'1');
		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";
		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');
		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');
		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);
		
		$data=array('page_title'=>'Update Task','middle_content'=>'update-task','task_info'=>$task_info,
			'crt_list'=>$crt_list);
		$this->load->view('cc/cc-view',$data);
	}
	
	#----------------------------------------------->>Change Task status<<------------------------------------------------
	public function changestatus()
	{
			$task_id=$this->uri->segment(4);
		$adminemail=$this->master_model->getRecords('email_id_master');
		
		/* get cc info */
		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.cc_id');
		$cc_info=$this->master_model->getRecords('cf_task as task',array('task.task_id'=>$task_id));
		/* get crt info */
		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_task.crt_id');
		$this->db->join('cf_scenario','cf_scenario.scenario_id=cf_task.scenario_id');
		$task_info=$this->master_model->getRecords('cf_task',array('task_id'=>$task_id));
		
		$nowtime = date("Y-m-d H:i:s");

		if($this->master_model->updateRecord('cf_task',array('task_status'=>'2','completed_date'=>$nowtime),array('task_id'=>$task_id)))
		{
			/*$info_arr=array('from'=>$adminemail[0]['contact_email'],
						'to'=>$cc_info[0]['crt_email'],
						'subject'=>'Task completion Mail',
						'view'=>'task_completion_mail_to_cc');
		
			 $other_info=array(
						  'first_name'=>$cc_info[0]['crt_first_name'],
						  'last_name'=>$cc_info[0]['crt_last_name'],
						  'scenario_name'=>$task_info[0]['scenario'],
						  'task_name'=>$task_info[0]['task'],
						  'crt_name'=>$task_info[0]['crt_first_name'].' '.$task_info[0]['crt_last_name']);
		
			$this->email_sending->sendmail($info_arr,$other_info);*/
		
			$this->session->set_flashdata('success',' Task successfully marked as completed.');	
			redirect(base_url().'crt/scenario/managetask/');
		}
	}
	
	public function details()
	{
		$task_id=$this->uri->segment(4);
		$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_task.crt_id');
		$this->db->join('cf_scenario','cf_task.scenario_id=cf_scenario.scenario_id');
		$task_info=$this->master_model->getRecords('cf_task',array('cf_task.task_id'=>$task_id));
		//print_r($task_info); exit;
		$data=array('page_title'=>'Task Details','middle_content'=>'details-task','task_info'=>$task_info,
			);
		$this->load->view('crt/crt-view',$data);
	}
}
?>
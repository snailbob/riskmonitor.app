<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Settings extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('crt_check_session');
		$this->crt_check_session->checksessionvalue();
		
	}
	
	public function index(){


		//$crt_record=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		$data=array('page_title'=>'Crisis Team Member details','error'=>'','middle_content'=>'settings-menu',

		'success'=>'');

		

		$this->load->view('crt/crt-view',$data);

	}

	
	
	#--------------------------------------------->>updating admin password<<-------------------------------------
	
	public function changePassword()
	{
		if(isset($_POST['change_cc_pw']))
		{
			$this->form_validation->set_rules('new_pass','Password','required|xss_clean');
			$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean');	
			
			if($this->form_validation->run())
			{
				$new_pass=$this->input->post('new_pass');
				$confirm_pass=$this->input->post('confirm_pass');
				if($new_pass==$confirm_pass)
				{
					$data_array=array('pass_word'=>md5($new_pass));
					
					if($this->master_model->updateRecord('cf_login_master',$data_array,array('login_id'=>$this->session->userdata('logged_crt_login_id'))))
					{
						$this->session->set_flashdata('success','Password successfully updated.');
						redirect(base_url().'crt/settings/changepassword/');
					}
					else
					{
						$this->session->set_flashdata('error','Error while updating information');
						redirect(base_url().'crt/settings/changepassword/');
					}
				}
				else
				{
					$this->session->set_flashdata('error','Wrong Password Confirmation.');
					redirect(base_url().'crt/settings/changepassword/');
				}
			}
		}
		
		$details=$this->master_model->getRecords('cf_login_master',
		array('login_id'=>$this->session->userdata('logged_crt_login_id')));
		$data=array('middle_content'=>'change-password','page_title'=>'Change Password','details'=>$details);	
		$this->load->view('crt/crt-view',$data);
	}
	
	
	#-------------------------------------->>Function for updating emails<<-----------------------------------
	public function updateinfo()
	{
		if(isset($_POST['update_personal_info_crt']))
		{
			$this->form_validation->set_rules('f_name','First Name','required|xss_clean');
			$this->form_validation->set_rules('l_name','Last Name','required|xss_clean');
			$this->form_validation->set_rules('ph_no','Support Email','required|xss_clean');	
			$this->form_validation->set_rules('position','position','required|xss_clean');	
			
			if($this->form_validation->run())
			{
				$f_name=$this->input->post('f_name','',true);
				$l_name=$this->input->post('l_name','',true);
 
 				$countrycode = $this->input->post('countrycode','',true);
 				$countrycode = explode(' ', $countrycode);
				$country_id = $countrycode[0];
				$country_calling = $countrycode[1];

				$crt_digits = ltrim($this->input->post('ph_no','',true), '0');

				$ph_no= $country_calling.$crt_digits;
				
				$position=$this->input->post('position','',true);
				
				$data_array=array(
					'crt_first_name'=>$this->master_model->encryptIt($f_name),
					'crt_last_name'=>$this->master_model->encryptIt($l_name),
					'countrycode'=>$this->master_model->encryptIt($country_calling),
					'country_id'=>$country_id,
					'crt_digits'=>$this->master_model->encryptIt($crt_digits),
					'crt_mobile'=>$this->master_model->encryptIt($ph_no),
					'crt_position'=>$position
				);
				//print_r($data_array); exit;
				if($this->master_model->updateRecord('cf_crisis_response_team',$data_array,array('login_id'=>$this->session->userdata('logged_crt_login_id'))))
				{
					$this->session->set_flashdata('success','Your details have been successfully updated.');
					redirect(base_url().'crt/settings/updateinfo/');
				}
				else
				{
					$this->session->set_flashdata('error','Error while updating Personal Infomation');
				}
			}
		}	
		
		$countriescode = $this->master_model->getRecords('country_t');
			
		$personal_info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_crt_login_id')));
		$data = array(
			'middle_content'=>'update-crt-info',
			'page_title'=>'Update Personal infomation',
			'personal_info'=>$personal_info,
			'countriescode'=>$countriescode
		);	
		
		$this->load->view('crt/crt-view',$data);
		
	}
	

	public function verifications(){
		$user_id = $this->session->userdata('logged_crt_login_id');
		$user_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id));
		$data = array(
			'page_title'=>'Crisis Team Member details',
			'error'=>'',
			'middle_content'=>'settings-verifications',
			'success'=>'',
			'user_info'=>$user_info
		);

		$this->load->view('crt/crt-view',$data);
	}
	
	
	public function add_connect(){
		$id = $_POST['id'];
		$email = $_POST['email'];
		$type = $_POST['type'];
		$user_id = $this->session->userdata('logged_crt_login_id');
		
		if($type == 'linkedin'){
			$arr = array(
				'linkedin_id'=>$id
			);
		}
		
		else if($type == 'gplus'){
			$arr = array(
				'gplus_id'=>$id
			);
		}
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		
		echo json_encode($arr);
	
	}
	
	public function remove_connect(){
		$type = $_POST['type'];
		$user_id = $this->session->userdata('logged_crt_login_id');

		if($type == 'linkedin'){
			$arr = array(
				'linkedin_id'=>''
			);
		}
		
		else if($type == 'gplus'){
			$arr = array(
				'gplus_id'=>''
			);
		}
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		echo json_encode($arr);
	}
	
		
}
<?php
class Standbymessage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('crt_check_session');
		$this->crt_check_session->checksessionvalue();
	}
	
	public function manage()
	{  
		$whr=array('stand_msg.cc_id'=>$this->session->userdata('logged_parent_crt'),
				   'stand_msg.org_id'=>$this->session->userdata('crt_selected_orgnaization'));
		$order=array('scn.scenario_id'=>'ASC');
		
		$this->db->join('cf_scenario as scn','scn.scenario_id=stand_msg.scenario_id');
		$msg_list=$this->master_model->getRecords('cf_standby_messages as stand_msg',$whr);
		
		$data=array('page_title'=>'Manage stand by message','middle_content'=>'manange-standbymsg','msg_list'=>$msg_list);
		$this->load->view('crt/crt-view',$data);
	}
	
	public function details()
	{
		$message_id=$this->uri->segment(4); 
		/* fetch scenarion added by current CC and for selected organization */
		$whr=array('cf_standby_messages.stn_msg_id'=>$message_id);
		$this->db->join('cf_standby_messages','cf_standby_messages.scenario_id=cf_scenario.scenario_id');
		$scn_list=$this->master_model->getRecords('cf_scenario',$whr);
		
		$data=array('page_title'=>'Stand by Message Details','middle_content'=>'details-standbymsg','scn_list'=>$scn_list);
		$this->load->view('crt/crt-view',$data);
		
	}
	
}
?>
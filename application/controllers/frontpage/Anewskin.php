<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Anewskin extends CI_Controller{


	public function __construct(){

		parent::__construct();

	}




	public function index(){
//		$this->load->view('frontpage/live-login');
//		return false;

		$price = 0;
		$the_gst = 0;
		$total_price = 0;
		$plan = '';

		if(isset($_GET['price'])){
			$price = $_GET['price'];
			$the_gst = $price * 0.1;
			$total_price = $price + $the_gst;
			$total_price = round($total_price, 2);
		}

		if(isset($_GET['plan'])){
			$plan = $_GET['plan'];
		}

		if($this->session->userdata('stripe_plans') != ''){
			$data['stripe_plans'] = array(); //$this->session->userdata('stripe_plans');
		}
		else{
			$data['stripe_plans'] = array(); //$this->common_model->all_plans();
			$this->session->set_userdata('stripe_plans', $data['stripe_plans']);
		}

		$firsthomesection = $this->master_model->getRecords('admin_contents', array('type'=>'firsthomesection'));

		$alternatingcontenthead = $this->master_model->getRecords('admin_contents', array('type'=>'alternatingcontenthead'));
		$alternatingcontents = $this->master_model->getRecords('admin_contents', array('type'=>'alternatingcontents'));

		$threepartshead = $this->master_model->getRecords('admin_contents', array('type'=>'threepartshead'));
		$threeparts = $this->master_model->getRecords('admin_contents', array('type'=>'threeparts'));
		$threeparts = $this->master_model->getRecords('admin_contents', array('type'=>'threeparts'));
		$collab = $this->master_model->getRecords('admin_contents', array('type'=>'collab'));
		$collabs = $this->master_model->getRecords('admin_contents', array('type'=>'collabs'));

		$easiestway = $this->master_model->getRecords('admin_contents', array('type'=>'easiestway'));
		$easiestways = $this->master_model->getRecords('admin_contents', array('type'=>'easiestways'));

		$banners = $this->master_model->getRecords('admin_contents', array('type'=>'banner'));

		$admin_info = $this->master_model->getRecords('admin_login', array('user_name'=>'admin'));

		$data['page_title'] = "Signup";
		$data['admin_info'] = $admin_info;
		$data['plan'] = $plan;
		$data['price'] = $price;
		$data['the_gst'] = round($the_gst, 2);
		$data['total_price'] = $total_price;

		$data['firsthomesection'] = $firsthomesection;

		$data['alternatingcontenthead'] = $alternatingcontenthead;
		$data['alternatingcontents'] = $alternatingcontents;

		$data['collab'] = $collab;
		$data['collabs'] = $collabs;

		$data['easiestway'] = $easiestway;
		$data['easiestways'] = $easiestways;

		$data['banners'] = $banners;

		$data['threepartshead'] = $threepartshead;
		$data['threeparts'] = $threeparts;

		$data['page_title'] = "Cloud-based Crisis Management System";
		$data['mobile_code'] = $this->master_model->getRecords('country_t');

		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/anewskin_view',$data);
		$this->load->view('frontpage/footer_view');


	}


	//Send request demo
	public function reqdemo(){

		$inputName = $this->input->post('inputName');
		$price_selected = $this->input->post('price_selected');
		$inputContactNo = $this->input->post('inputContactNo');
		$inputEmail = $this->input->post('inputEmail');
		$inputCompany = $this->input->post('inputCompany');
		$inputCountry = $this->input->post('inputCountry');
		$modules = $this->input->post('checkboxes',true);

		$modlist = '<ol>';
		foreach($modules as $mod){
			$modlist .= '<li>'.$mod.'</li>';
		}
		$modlist .= '</ol>';


		$this->load->library('email');

		$this->email->set_mailtype("html");
		$this->email->from($inputEmail, $inputName);
		$this->email->to('admin@crisisflo.com');

		$this->email->subject('A CrisisFlo Visitor wants to try a demo.');

		$msg = '

			<html>
				<body>
					<p>Name: '.$inputName .'</p>
					<p>Contact No.: '.$inputContactNo .'</p>
					<p>Email Address: '.$inputEmail .'</p>
					<p>Company: '.$inputCompany .'</p>
					<p>Country: '.$inputCountry .'</p>
					<p>Price Selected: UAD$'.$price_selected .'</p>
					<p>Modules: <br> '.$modlist.'</p>
				</body>
			</html>

		';
		$this->email->message($msg);

		$this->email->send();

		echo json_encode(array('msg' => $msg)); //$this->email->print_debugger()));
	}


	//Send contact us
	public function contactus(){

				$inputName = $this->input->post('inputname');
				$inputCompany = $this->input->post('inputwebsite');
				$inputEmail = $this->input->post('inputemail');
				$inputsubject = $this->input->post('inputsubject');
				$inputmessage = $this->input->post('inputmessage');
				$inputCountry = $this->input->post('inputCountry');

				// $this->load->library('email');
				//
				// $this->email->set_mailtype("html");
				// $this->email->from($inputEmail, $inputName);
				// $this->email->to('admin@crisisflo.com');
				//
				// $this->email->subject('A CrisisFlo Visitor contacted you.');



				$msg = '

							<p>Name: '.$inputName .'</p>
							<p>Email Address: '.$inputEmail .'</p>
							<p>Company: '.$inputCompany .'</p>
							<p>Country: '.$inputCountry .'</p>
							<p>Subject: '.$inputsubject .'</p>
							<p>Message: '.$inputmessage .'</p>
				';


				// Admin email
				$whr = array(
					'id'=>'1'
				);

				$adminemail = $this->master_model->getRecords('email_id_master',$whr,'*');

				// Sending mail to CC only
				$info_arr_cc=array(
					'from'=>'',
					'to'=>'info@crisisflo.com',
					'subject'=>'A CrisisFlo Visitor contacted you.',
					'view'=>'incident-occurence-mail-to-cc'
				);

				$other_info_cc = array(
					'name'=>'Admin',
					'details'=>$msg
				);

				$this->email_sending->sendmail($info_arr_cc,$other_info_cc);



				// $this->email->message($msg);
				//
				// $this->email->send();

				echo json_encode(array('other_info_cc' => $other_info_cc, 'info_arr_cc' => $info_arr_cc));
	}


	public function check_exist_cc(){
		$email = $_POST['ccemail'];
		$cc = $this->master_model->getRecords('cf_login_master', array('user_level'=>'0'));

		$data['result'] = 'ok';

		if(count($cc) > 0){
			foreach($cc as $r=>$value){
				$cc_email = $this->master_model->decryptIt($value['email_id']);
				if($cc_email == $email){
					$data['result'] = 'duplicate';
				}
			}
		}

		echo json_encode($data);
	}

	public function check_domain_available(){
		$domain = $_POST['domain'];
		$org = $this->master_model->getRecordCount('organization_master', array('domain'=>$domain));

		$arr = array(
			'count'=>$org
		);
		echo json_encode($arr);

	}

	public function trial_customer(){

		$message = $this->master_model->getRecords('validation_messages', array('name'=>'trial_request'));


		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$organization = $_POST['organization'];
		
		$address = (isset($_POST['address'])) ? $_POST['address'] : '';
		$lat = (isset($_POST['lat'])) ? $_POST['lat'] : '';
		$lng = (isset($_POST['lng'])) ? $_POST['lng'] : '';
		$country_code = (isset($_POST['country_code'])) ? $_POST['country_code'] : '';
		$calling_code = (isset($_POST['calling_code'])) ? $_POST['calling_code'] : '';
		$mobile = (isset($_POST['mobile'])) ? $_POST['mobile'] : '';
		$mobile = ltrim($_POST['mobile'], '0');
		$complete_mobile = $calling_code.$mobile;
		$country_short = (isset($_POST['country_short'])) ? $_POST['country_short']  : '';
		$plan = (isset($_POST['plan'])) ? $_POST['plan'] : '';
		$domain = (isset($_POST['domain'])) ? $_POST['domain'] : '';

		$recall_max = 1000; //$plan;


		//google plus
		$gplus_id = '';
		if(isset($_POST['gplus_id'])){
			$gplus_id = $_POST['gplus_id'];
		}

		//linkedin_id
		$linkedin_id = '';
		if(isset($_POST['linkedin_id'])){
			$linkedin_id = $_POST['linkedin_id'];
		}

		//default demo data
		$stripe_cust_id = '0';
		$purchaser_id = '';
		$user_type = 'demo';
		$org_status = 'Y'; //'N';

		//data form array
		$form_data = $_POST['form_data'];
		$form_data[] = array(
			'name'=>'mobile_number',
			'value'=>$complete_mobile
		);




		//data for paid user
		if(isset($_POST['stripe_cust_id'])){
			$stripe_cust_id = $_POST['stripe_cust_id'];
			$user_type = 'live';
			$org_status = 'Y';
		}

		if(isset($_POST['purchaser_id'])){
			$purchaser_id = $_POST['purchaser_id'];
		}

		$onetime_key = md5(microtime());

		$cc = $this->master_model->getRecords('cf_login_master', array('user_level'=>'0'));

		//check cc email duplicate
		$data['result'] = 'ok';

		if(count($cc) > 0){
			foreach($cc as $r=>$value){
				$cc_email = $this->master_model->decryptIt($value['email_id']);
				if($cc_email == $email){
					$data['result'] = 'duplicate';
				}
			}
		}


		if($data['result'] == 'ok'){
			$admin_info = $this->master_model->getRecords('admin_login', array('user_name'=>'admin'));
			$login_arr = array(
				'email_id'=>$this->master_model->encryptIt($email),
				'user_type'=>$user_type,
				'user_level'=>'0',
				'onetime_key'=>$onetime_key,
				'user_status'=>'0',
				'gplus_id'=>$gplus_id,
				'linkedin_id'=>$linkedin_id,
				'stripe_customer_id'=>$stripe_cust_id,
				'single_user'=>($recall_max == 1000) ? 'y' : 'n',
				'single_price'=>($recall_max == 1000) ? $admin_info[0]['single_rate'] : ''
			);

			$data['login_arr'] = $login_arr;

			if($login_id = $this->master_model->insertRecord('cf_login_master',$login_arr,TRUE)){

				$mods = $this->common_model->preselected_modules();

				$active_module = $mods['active_module']; //'m5m-m3m-m6m-m7m-m9m-m10m-m12m';
				$active_mod = $mods['active_mod']; //serialize($modules);

				//insert data to organization_master
				$org_arr = array(
					'organization_name'=>$organization,
					'domain'=>$organization,
					'active_module'=>$active_module, //$themods,
					'active_mod'=>$active_mod,
					'cust_type'=>'1', //not_aig $cust_type,
					'cc_id'=>$login_id,
					'enabled'=>$org_status,
					'recall_max'=>$recall_max,
					'date_added'=>date('Y-m-d H:i:s'),
					// 'domain'=>$domain
				);

				$data['org_arr'] = $org_arr;


				$org_id = $this->master_model->insertRecord('organization_master',$org_arr,TRUE);

				//add default org for cc
				$this->master_model->updateRecord('cf_login_master',array('default_org'=>$org_id),array('login_id'=>$login_id));

				$cc_arr = array(
					'login_id'=>$login_id,
					'crt_first_name'=>$this->master_model->encryptIt($first_name),
					'crt_last_name'=>$this->master_model->encryptIt($last_name),
					'crt_organisation'=>$organization,
					'crt_email'=>$this->master_model->encryptIt($email),
					'location'=>$address,
					'location_lat'=>$lat,
					'location_lng'=>$lng,
					'cc_id'=>$login_id,
					'country_id'=>$country_code,
					'countrycode'=>$this->master_model->encryptIt($calling_code),
					'crt_digits'=>$this->master_model->encryptIt($mobile),
					'crt_mobile'=>$this->master_model->encryptIt($complete_mobile),
					'org_id'=>0
				);

				$data['cc_arr'] = $cc_arr;

				//create text file for organization chat room
				$room_name = 'chat_room/org';
				$room_name .= $org_id;
				$room_name .= '.txt';
				$myfile = fopen($room_name, "w");

				if($this->master_model->insertRecord('cf_crisis_response_team', $cc_arr)){


					//email to admin for demo signup
					// if($stripe_cust_id == '0'){

						//send email to admin
						$admin_email = $this->master_model->getRecords('email_id_master');

						$info_arr = array(
							'to'=>'admin@crisisflo.com', //snailbob01@gmail.com',//$email,
							'from'=>$admin_email[0]['info_email'],
							'subject'=>'A CrisisFlo Visitor signed up for a trial account.',
							'view'=>'mail-to-admin-for-trial-customer'
						);

						$other_info = array(
							'form_data'=>$form_data,
							'org_id'=>$org_id,
							'email'=>$email
						);

						$this->email_sending->sendmail($info_arr,$other_info);
						$data['info_arr'] = $info_arr;
						$data['other_info'] = $other_info;

						//send customer the link for activation
						$data['activate_customer_demo'] = $this->common_model->activate_customer_demo($login_id);

					// }
					// else{
						//update purchaser data
						// $chaser_data = array(
						// 	'cc_id'=>$login_id,
						// 	'org_id'=>$org_id,
						// 	'status'=>'Y'
						// );

						// $this->master_model->updateRecord('organization_signups', $chaser_data, array('id'=>$purchaser_id));

						//send verification email to cc
						$admin_email = $this->master_model->getRecords('email_id_master');

						$info_arr = array(
							'to'=>$email,
							'from'=>$admin_email[0]['info_email'],
							'subject'=>'Welcome to CrisisFlo',
							'view'=>'registration-mail-to-cc'
						);

						$other_info = array(
							'first_name'=>$first_name,
							'last_name'=>$last_name,
							'onetime_key'=>$onetime_key,
							'email'=>$email
						);

						$this->email_sending->sendmail($info_arr,$other_info);
						$data['info_arr'] = $info_arr;
						$data['other_info'] = $other_info;
					// }//paid user

					$user_login_for_session = $this->common_model->user_login_for_session($login_id);
					$this->session->set_userdata($user_login_for_session);

				}//end if cf_crisis_response_team created

				$data['message'] = "We've just sent you an email with details on how to complete your registration, please check your inbox."; //$message[0]['success_message'];

			}//end if login master created




		}//no duplicate

		echo json_encode($data);
	}

	public function preview_email(){

		// $info_arr = array(
		// 	'to'=>$email,
		// 	'from'=>$admin_email[0]['info_email'],
		// 	'subject'=>'Welcome to CrisisFlo',
		// 	'view'=>'registration-mail-to-cc'
		// );

		$other_info = array(
			'first_name'=>'ff',
			'last_name'=>'ll',
			'onetime_key'=>'423dsfds',
			'email'=>'asdf@asd.com'
		);

		$this->load->view('email/registration-mail-to-cc',$other_info);
		
	}


	public function signup_payment(){ //used in front page sign up and user panel upgrade
		//first_name=asdf&last_name=sdfs&email=stripe%40stripe.com&address=Diliman%2C+Quezon+City%2C+Metro+Manila%2C+Philippines&lat=14.6470124&long=121.05070980000005&organization=ddfds&plan=RM20_quarterly_sub__CrisisFlo+Recall+Manager+-+20+-+quarterly+subscription__421.20__42.12__463.32&stripeToken=tok_16C2qoD6oIAYAzZLj3sucMhS

	//organization=asdf&address=sdfs&lat=&lng=&first_name=sdfsdsf&last_name=sdf&email=sdfsd%40sf.com&country_code=174&mobile=23324&country_short=&rc_is_ap=Y&ccfirst_name=sdfsdsf&cclast_name=sdf&ccemail=sdfsd%40sf.com&cccountry_code=174&ccmobile=23324&country_short=&plan=RM5_quarterly_sub__CrisisFlo+Recall+Manager+-+5+-+quarterly+subscription__270.00__27__297.00&stripeToken=tok_16Co9dD6oIAYAzZLMWuz7844


//		$first_name = $_POST['ccfirst_name'];
//		$last_name = $_POST['cclast_name'];
//		$email = $_POST['ccemail'];
//		$country_code = $_POST['cccountry_code'];
//		$mobile = $_POST['ccmobile'];
//		$country_short = $_POST['cccountry_short'];

		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$country_code = $_POST['country_code'];
		$mobile = ltrim($_POST['mobile'], '0');
		$country_short = $_POST['country_short'];
		$calling_code = $_POST['calling_code'];


		$address = $_POST['address'];
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		$organization = $_POST['organization'];
		$plan = $_POST['plan'];

		$data_arr = $_POST['data_arr'];
		//remove extra fields
		$new_data_arr = array();
		$new_data_arr_count = 0;
		foreach($data_arr as $dr){
			if($new_data_arr_count < 11){
				$new_data_arr[] = $dr;
			}
			$new_data_arr_count++;
		}

		$data_arr = $new_data_arr;

		$plan_details = explode('__', $plan);
		$subs_plan_id = $plan_details[0];
		$data_arr[] = array(
			'name'=>'subs_plan_id',
			'value'=>$subs_plan_id
		);

		//$recall_max = explode('__', $plan); // explode(' - ', $plan);
		$recall_max = preg_replace("/[^0-9]/","",$subs_plan_id); // $recall_max[1];
		$data_arr[] = array(
			'name'=>'recall_max',
			'value'=>(isset($plan_details[1])) ? $recall_max : $plan
		);

		$data_arr[] = array(
			'name'=>'mobile_number',
			'value'=>$calling_code.$mobile
		);

		$data_arr[] = array(
			'name'=>'plan_name',
			'value'=>(isset($plan_details[1])) ? $plan_details[1] : $plan
		);


		$cc_id = '';
		$org_id = '';

		if(isset($_POST['cc_id'])){
			$cc_id = $_POST['cc_id'];
			$org_id = $_POST['org_id'];
		}

		//check if token is generated
		if(!isset($_POST['stripeToken'])){
			$data['result'] = 'no_token';

			echo json_encode($data_arr);
			return false;
		}
		else{
			$stripeToken = $_POST['stripeToken'];
		}

		$val_mess = ($cc_id != '') ? 'trial_upgrade' : 'plan_signup';
		$message = $this->master_model->getRecords('validation_messages', array('name'=>$val_mess));
		$onetime_key = md5(microtime());
		$data['result'] = 'ok';

//		echo json_encode($data_arr);
//		return false;

		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account
		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; //(base_url() == '') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';

		Stripe::setApiKey($apikey);

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			$stripe_data = array(
			  "description" => $organization,
			  "email" => $email,
			  //"plan" => $subs_plan_id, //subscrib user to plan
			  "card" => $stripeToken // obtained with Stripe.js
			);

			if(isset($plan_details[1])){
				$stripe_data['plan'] = $subs_plan_id;
			}

			// Create a Customer
			$customer = Stripe_Customer::create($stripe_data);
			$the_customer = (array) $customer;

			$data['customer'] = $the_customer;


			$cu = Stripe_Customer::retrieve($customer->id); //$org_stripe[0]['stripe_id']);
			$the_sub = (array) $cu;
			$data['sub_data'] = $the_sub; //$cu->subscriptions;
			$sub_total_count = $cu->subscriptions->total_count;


			$the_subsription_data = array();
			$subscription_id = '';
			$current_period_end = date("Y-m-d H:i:s");
			if($sub_total_count > 0){
				for($i = 0; $i < $sub_total_count; $i++){
					$plan_data = $cu->subscriptions->data[$i];
					$subscription_id = $plan_data->id;

					//get period end date
					$current_period_start = $plan_data->current_period_start;
					$date = new DateTime("@$current_period_start");
					$current_period_start = $date->format('Y-m-d H:i:s');

					$current_period_end = $plan_data->current_period_end;
					$date = new DateTime("@$current_period_end");
					$current_period_end = $date->format('Y-m-d H:i:s');


					$subscription_data = $this->common_model->subscription_data($plan_data);

					$the_subsription_data[] = $subscription_data;
				}
			}
			$data['subsription_data'] = $the_subsription_data;

			//authorized data to store
			$personnel_data = array(
				'auth_email'=>$email,
				'content'=>serialize($data_arr),
				'stripe_data'=>serialize($the_customer),
				'subscription_data'=>serialize($the_subsription_data),
				'stripe_cust_id'=>$customer->id,
				'status'=>'N'
			);


			//make user live if cc logged in
			if($cc_id != ''){
				$login_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
				$personnel_data['cc_id'] = $cc_id;
				$personnel_data['org_id'] = $org_id;
				$personnel_data['status'] = 'Y';

				$cc_arr = array(
					'stripe_customer_id'=>$customer->id,
					'stripe_subscription_id'=>$subscription_id,
					'current_sub_end_date'=>$current_period_end,
					'user_type'=>'live'
				);

				if($login_info[0]['trial_end'] == '0000-00-00 00:00:00'){
					$nowtime = date("Y-m-d h:i:s");
					$cc_arr['trial_end'] = $nowtime;
				}

				$org_arr = array(
					'recall_max'=>$recall_max
				);

				$this->session->set_userdata('logged_user_type', 'live');
				$this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$cc_id));

				$this->master_model->updateRecord('organization_master', $org_arr, array('organization_id'=>$org_id));

			}

			$personnel_id = $this->master_model->insertRecord('organization_signups', $personnel_data, true);


			$whr = array('id'=>'1');
			$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

			$info_arr = array(
				'from'=>$adminemail[0]['recovery_email'],
				'to'=>$email,
				'subject'=>'Welcome to CrisisFlo',
				'view'=>'signup_email_to_authorized_purchaser'
			);


			//change email template
			if($cc_id != ''){
				$info_arr['view'] = 'signup_email_to_authorized_purchaser_on_upgrade';
			}


			$other_info = array(
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'personnel_id'=> md5($personnel_id),
				'the_subsription_data'=>$the_subsription_data
			);

			$this->email_sending->sendmail($info_arr, $other_info);


			//send email to admin
			$admin_email = $this->master_model->getRecords('email_id_master');

			$info_arr = array(
				'to'=>'admin@crisisflo.com', //'snailbob01@gmail.com',
				'from'=>$admin_email[0]['info_email'],
				'subject'=>'New Customer Sign Up.',
				'view'=>'mail-to-admin-for-new-customer'
			);

			if($cc_id != ''){
				$info_arr['subject'] = 'New Customer Upgrade.';
			}

			$other_info = array(
				'form_data'=>$data_arr,
				'the_subsription_data'=>$the_subsription_data,
				'org_id'=>$org_id,
				'email'=>$email
			);

			$this->email_sending->sendmail($info_arr,$other_info);


			$data['info_arr'] = $info_arr;
			$data['other_info'] = $other_info;
			$data['result'] = 'success';
			$data['message'] = $message[0]['success_message'];


		} catch(Stripe_CardError $e) {
		  // The card has been declined
			$this->session->set_flashdata('error','The card has been declined.');
			$data['result'] = 'declined';
			$data['message'] = 'The card has been declined.'; // $message[0]['error_message'];
		}

		echo json_encode($data);

	}

	//test retrieve purchaser_get
	public function purchaser_get(){

		$purch_id = $this->uri->segment(4);
		$personnel_id = $this->master_model->getRecords('organization_signups', array('id'=>$purch_id));
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		$stripe_data = unserialize($personnel_id[0]['subscription_data']);
		$plan_name = $stripe_data[0]['plan']['name'];
		//echo preg_replace("/[^0-9]/","",$plan_name);
		//echo str_replace(array("\u0000*", "\u0000_"), "", json_encode($stripe_data,JSON_PRETTY_PRINT));
		echo json_encode($stripe_data, JSON_PRETTY_PRINT);

	}

	public function signup(){
		$price = 0;
		$the_gst = 0;
		$total_price = 0;
		$plan = '';

		if(isset($_GET['price'])){
			$price = $_GET['price'];
			$the_gst = $price * 0.1;
			$total_price = $price + $the_gst;
			$total_price = round($total_price, 2);
		}

		if(isset($_GET['plan'])){
			$plan = $_GET['plan'];
		}

		$data['stripe_plans'] = $this->all_plans();
		$data['page_title'] = "Signup";
		$data['plan'] = $plan;
		$data['price'] = $price;
		$data['the_gst'] = round($the_gst, 2);
		$data['total_price'] = $total_price;

		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/signup_view', $data);
		$this->load->view('frontpage/footer_view');

	}

	public function all_plans(){
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		$all_plans = Stripe_Plan::all(array("limit" => 30));
		return $all_plans;
	}



	public function show_country_code() {
		$counrty_code = $_POST['country_short'];

		$mobile_code = $this->master_model->getRecords('country_t',array('iso2'=>$counrty_code));

		$data['calling_code'] = '';
		$data['country_id'] = '';
		$data['result'] = 'empty';

		if(count($mobile_code) > 0){
			$data['calling_code'] = $mobile_code[0]['calling_code'];
			$data['country_id'] = $mobile_code[0]['country_id'];
			$data['result'] = 'success';
		}


		echo json_encode($data);

	}


	public function terms(){
		$terms = $this->master_model->getRecords('admin_contents', array('type'=>'terms'));
		$data = array(
			'terms'=>$terms
		);
		$this->load->view('frontpage/terms_of_use',$data);
	}

	public function privacy(){
		$privacy = $this->master_model->getRecords('admin_contents', array('type'=>'privacy'));
		$data = array(
			'privacy'=>$privacy
		);
		$this->load->view('frontpage/privacy_policy',$data);
	}


	public function check_gplus(){
		$id = $_POST['id'];
		$email = $_POST['email'];
		$type = $_POST['type'];

		$data['id'] = $id;
		$data['email'] = $email;
		$data['result'] = 'ok';

		if($type == 'linkedin'){
			$whr_soc = array(
				'linkedin_id'=>$id
			);
		}
		else if($type == 'gplus'){
			$whr_soc = array(
				'gplus_id'=>$id
			);
		}

		$users = $this->master_model->getRecords('cf_login_master', $whr_soc);

		if(count($users) > 0){
			$data['result'] = 'exist';
		}

		echo json_encode($data);
	}


	public function save_incident_response(){
		$data = $_POST;
		$nowtime = date("Y-m-d h:i:s");
		$cc_id = $_POST['cc_id'];
		unset($data['cc_id']);
		$arr = array(
			'content'=>serialize($data),
			'cc_id'=>$cc_id,
			'answer'=>'Open',
			'created_at'=>$nowtime,
		);
		$this->master_model->insertRecord('incident_form_fields', $arr);
		echo json_encode($data);
	}
}


?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {
	
	public function __construct()


	{


		parent::__construct();


	}


	public function index()
	{
		$pagetype = 'aboutus';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
			
		$data['page_title']="About Us";
		$data['page_content']=$admin_info[0];

		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/about_view');
		$this->load->view('frontpage/footer_view');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
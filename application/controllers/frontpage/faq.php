<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller {


	public function index()
	{
		$faq = $this->master_model->getRecords('faq');
		
		$data['page_title'] = "FAQ";
		$data['faq'] = $faq;
		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/faq_view', $data);
		$this->load->view('frontpage/footer_view');
	}
	
	
}


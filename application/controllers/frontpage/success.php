<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Success extends CI_Controller {
	
	public function __construct()


	{


		parent::__construct();


	}


	public function index()
	{

		if($_POST)
		{
		
		 
			// EDIT THE 2 LINES BELOW AS REQUIRED
		 
			$email_to = "nga.personal@gmail.com";
		 
			$email_subject = "A Crisis FLO visitor has contacted you.";
		 
			 
		
			$name = $_POST['inputname']; // required
		 
			$email_from = $_POST['inputemail']; // required
		 
			$website = $_POST['inputwebsite']; //  required aka company
			
			$country = $_POST['inputCountry']; // not required
		 
			$subject = $_POST['inputsubject']; // not required
		 
			$message = $_POST['inputmessage']; // required
		 
			$email_message ='';
		 
			$email_message .= "Name: ".clean_string($name)."\n";
		 
			$email_message .= "Email: ".clean_string($email_from)."\n";
			
			$email_message .= "Company: ".clean_string($website)."\n";
		 
			$email_message .= "Country: ".clean_string($country)."\n";
		 
			$email_message .= "Subject: ".clean_string($subject)."\n";
		 
			$email_message .= "Message: ".clean_string($message)."\n";
		 
			 
		 
			 
		 
			// create email headers
			 
			$headers = 'From: '.$email_from."\r\n".
			 
			'Reply-To: '.$email_from."\r\n" .
			 
			'X-Mailer: PHP/' . phpversion();
			 
			@mail($email_to, $email_subject, $email_message, $headers);  
			
		$data['page_title']="Thank You for contacting us.";
		
	
		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/success_view');
		$this->load->view('frontpage/footer_view');
	

		}
		
		else{
		header('Location: '.base_url().'contact');
		}
		
	
	}



	public function reqdemo()
	{

		if($_POST)
		{
		
		 
			// EDIT THE 2 LINES BELOW AS REQUIRED
		 
			$email_to = "nga.personal@gmail.com";
		 
			$email_subject = "A CrisisFlo visitor requested a demo.";
		 
			 
		
			$inputName = $_POST['inputName']; // required
		 
			$inputEmail = $_POST['inputEmail']; // required
		 
			$inputContactNo = $_POST['inputContactNo']; // required
		 
			$inputCompany = $_POST['inputCompany']; // not required
		 
			$inputCountry = $_POST['inputCountry']; // required
		 

		 	$email_message = "";
			$email_message .= "Name: ".clean_string($inputName)."\n";
		 
			$email_message .= "Contact Number: ".clean_string($inputContactNo)."\n";
			
			$email_message .= "Email Address: ".clean_string($inputEmail)."\n";
		 
			$email_message .= "Company: ".clean_string($inputCompany)."\n";
		 
			$email_message .= "Country: ".clean_string($inputCountry)."\n";
		 
			 
		 
			 
		 
			// create email headers
			 
			$headers = 'From: '.$inputEmail."\r\n".
			 
			'Reply-To: '.$inputEmail."\r\n" .
			 
			'X-Mailer: PHP/' . phpversion();
			 
			@mail($email_to, $email_subject, $email_message, $headers);  

		$data['page_title']="Thank You for your interest.";
		
	
		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/success_reqdemo_view');
		$this->load->view('frontpage/footer_view');
	

		}
		
		else{
		header('Location: '.base_url().'reqdemo');
		}
		
	
	}


}

function clean_string($string) {

  $bad = array("content-type","bcc:","to:","cc:","href");

  return str_replace($bad,"",$string);

}





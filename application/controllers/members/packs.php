<?php 

class Packs extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		$this->load->library('upload');

		$this->load->helper('download');


	}

	
	//-----------------------------------add pack----------------------------------------///
	public function add()

	{

		if(isset($_POST['add_pack']))

		{

			$this->form_validation->set_rules('pack_name','Pack Name','required|xss_clean');


			if($this->form_validation->run())

			{

				$pack_name=$this->input->post('pack_name',true);	

				if($pack_id = $this->master_model->insertRecord('input_packs',array('name'=>$pack_name,'reseller_id'=>$_SESSION['reseller_id']), true))

				{
					

					$this->session->set_flashdata('success','Input Pack successfully added');

					redirect(base_url().'reseller/packs/manage'); 

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding Orgnization');

					redirect(base_url().'reseller/organization/manage');

				}

			}

		}

		



		$data=array('page_title'=>'Add Organization','middle_content'=>'add-packs');

		$this->load->view('reseller/admin-view',$data);

	}

	
	//-----------------------------------manage pack----------------------------------------///
	public function manage()

	{

		$order_arr=array('name'=>'ASC');

		$packs=$this->master_model->getRecords('input_packs',array('reseller_id'=>$_SESSION['reseller_id']),'*',$order_arr);



		$data=array('page_title'=>'Manage Packs','middle_content'=>'manage-packs','packs'=>$packs);

		$this->load->view('reseller/admin-view',$data);

	}

	//-----------------------------------view pack----------------------------------------///
	public function view()

	{
		$pack_id=$this->uri->segment(4);


		$whrr = array(
			'input_pack_id'=>$pack_id
		);
	
	
		$the_packs=$this->master_model->getRecords('input_packs',array('id'=>$pack_id),'*');
	
		$file_upload=$this->master_model->getRecords('cf_file_upload',$whrr,'*');
		
		//get tasks
		$whr_arr=array(
			'scn.input_pack_id'=>$pack_id
			//'scn.cc_id'=>$this->session->userdata('logged_cc_login_id'),
			//'scn.org_id'=>$this->session->userdata('cc_selected_orgnaization')
		);

		$order=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');

		$select="scn.*,task.*,crt.crt_first_name,crt.crt_last_name";

		$this->db->join('cf_scenario as scn','scn.scenario_id=task.scenario_id','left');

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.crt_id','left');

		$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr,$select,$order);



		$orderby=array('scn.scenario_id'=>'ASC');
		
		$this->db->join('cf_scenario as scn','scn.scenario_id=stand_msg.scenario_id');
		$msg_list=$this->master_model->getRecords('cf_standby_messages as stand_msg',$whr_arr,'',$orderby);

		//$scenario=$this->master_model->getRecords('cf_scenario',$whrr,'*');

		//$tasks=$this->master_model->getRecords('cf_task',$whrr,'*');

		$standby_messages=$this->master_model->getRecords('cf_standby_messages','','*');

		

		$data=array('page_title'=>'Manage Packs','middle_content'=>'view-packs','file_upload'=>$file_upload, 'the_packs'=>$the_packs, 'msg_list'=>$msg_list,'task_list'=>$task_list,'standby_messages'=>$standby_messages);

		$this->load->view('reseller/admin-view',$data);

	}


	//-----------------------------------add message----------------------------------------///
	public function add_message()
	{
		
		$pack_id=$this->uri->segment(4);
		
		if(isset($_POST['add_stand_by']))
		{
			$this->form_validation->set_rules('sel_scenario','Select Scenario','required|xss_clean');
			$this->form_validation->set_rules('stand_by_msg','Message','required|xss_clean');
			
			if($this->form_validation->run())
			{
				$sel_scenario=$this->input->post('sel_scenario',true);
				$stand_by_msg=$this->input->post('stand_by_msg',true);
				
				/*check wether message is alredy exists for same scenario or not*/				
				$chk_whr_arr=array('scenario_id'=>$sel_scenario);
				$chk_scn=$this->master_model->getRecordCount('cf_standby_messages',$chk_whr_arr);
				if($chk_scn == 0)
				{
					$insrt_arr=array(
						'scenario_id'=>$sel_scenario,
						'cc_id'=>'0',
						'org_id'=>'1',
						'input_pack_id'=>$pack_id,
						'standby_message'=>$stand_by_msg
					);
					if($this->master_model->insertRecord('cf_standby_messages',$insrt_arr))
					{
						//indicates scenario has message
						$thesceneid=array('scenario_id'=>$sel_scenario);
						$standmessrec=$this->master_model->getRecords('cf_standby_messages',$thesceneid);
						$this->master_model->updateRecord('cf_scenario',array('has_standmess'=>$standmessrec[0]['stn_msg_id']),array('scenario_id'=>$sel_scenario));
						
						$this->session->set_flashdata('success',' Standby message successfully added.');
						redirect(base_url().'reseller/packs/view/'.$pack_id);
					}
					else
					{
						$this->session->set_flashdata('error','Error while adding message.');
						redirect(base_url().'reseller/packs/add_message/'.$pack_id);
					}
				}
				else
				{
					$this->session->set_flashdata('error','Message already exists for selected scenario.');
					redirect(base_url().'reseller/packs/add_message/'.$pack_id);
				}
			}
		}
		
		/* fetch scenarion added by current CC and for selected organization */
		$whr=array(
			'input_pack_id'=>$pack_id,
			'has_standmess'=>'0'
		);
		$scn_list=$this->master_model->getRecords('cf_scenario',$whr);
		
		$data=array('page_title'=>'Add Stand by Message','middle_content'=>'add-standbymsg','scn_list'=>$scn_list);
		$this->load->view('reseller/admin-view',$data);
	}

	//-----------------------------------delete message----------------------------------------///
	public function delete_message()
	{
		
		$pack_id=$this->uri->segment(4);
		$stn_msg_id=$this->uri->segment(5);
		
		if($this->master_model->deleteRecord('cf_standby_messages','stn_msg_id',$stn_msg_id))
		{
			//added to reset message
			$this->master_model->updateRecord('cf_scenario',array('has_standmess'=>'0'),array('has_standmess'=>$stn_msg_id));
			
			$this->session->set_flashdata('success',' Standby message successfully deleted.');
			redirect(base_url().'reseller/packs/view/'.$pack_id);
		}
		else
		{
			$this->session->set_flashdata('error',' Error while deleting record');
			redirect(base_url().'reseller/packs/view/'.$pack_id);
		}
	}





	//-----------------------------------add scenario----------------------------------------///
	public function add_scenario()

	{
		$pack_id=$this->uri->segment(4);

		if(isset($_POST['add_scen']))

		{

			$this->form_validation->set_rules('scenario_desc','Scenario','required|xss_clean');

			if($this->form_validation->run())

			{

				$scenario_desc=$this->input->post('scenario_desc',true);

				$insrt_arr=array(
					'cc_id'=>'0',
					'org_id'=>'1',
					'scenario'=>$scenario_desc,
					'input_pack_id'=>$pack_id,
					'scenario_status'=>'0'
					);

				if($scenario_id=$this->master_model->insertRecord('cf_scenario',$insrt_arr,TRUE))

				{

					redirect(base_url().'reseller/packs/add_task/'.$scenario_id.'/'.$pack_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding scenario');

					redirect('reseller/packs/add_scenario'.$pack_id);

				}

			}

		}

		

		$data=array('page_title'=>'Add Scenario','middle_content'=>'add-scenario');

		$this->load->view('reseller/admin-view',$data);

	}
	
	
	//-----------------------------------add task----------------------------------------///
	public function add_task()

	{

		$scenario_id=$this->uri->segment(4);
		$pack_id=$this->uri->segment(5);
		

		if(isset($_POST['add_task']))

		{

			$this->form_validation->set_rules('task_desc','Task Description','required|xss_clean');

			$this->form_validation->set_rules('task_guide','Task Guidance','required|xss_clean');

			//$this->form_validation->set_rules('assign_to','Select CRT','required|xss_clean');

			

			if($this->form_validation->run())

			{

				$task_desc=$this->input->post('task_desc',true);

				$task_guide=$this->input->post('task_guide',true);

				//$assign_to=$this->input->post('assign_to',true);	

				if($this->uri->segment(5) !="" && $this->uri->segment(5)=='res')

				{$task_status='1';}

				else

				{$task_status='0';}
				//add current response date to include into task log
				$scene_resp=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenario_id),'initiation_date');

				$insrt_arr=array(
					'scenario_id'=>$scenario_id,
					'task'=>$task_desc,
					'task_guide'=>$task_guide,
					'crt_id'=>'0',
					'input_pack_id'=>$pack_id,
					'cc_id'=>'0',
					'current_response' =>$scene_resp[0]['initiation_date'], //add current response date
					'org_id'=>'1',
					'task_status'=>$task_status
				);

							

				if($this->master_model->insertRecord('cf_task',$insrt_arr))

				{

					$this->session->set_flashdata('success',' Task successfully added. Please add another task for current scenario if required. Alternatively, you can create another scenario.');

					if($this->uri->segment(5) !="" && $this->uri->segment(5)=='res')

					{redirect(base_url().'cc/dashboard');}

					else

					{redirect(base_url().'reseller/packs/add_task/'.$scenario_id.'/'.$pack_id);}

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding task');

					if($this->uri->segment(5) !="" && $this->uri->segment(5)=='res')

					{redirect(base_url().'reseller/dashboard');}

					else

					{redirect(base_url().'reseller/packs/add_task/'.$scenario_id.'/'.$pack_id);}

				}

			}

		}

		

		/* get last added scenario */

		$scn_info=$this->master_model->getRecords('cf_scenario',array('scenario_id'=>$scenario_id));

		

		/* fetch CR list to assing task, CRT will be of current selected organization */

		/*$whr_arr=array('crt.cc_id'=>$this->session->userdata('logged_cc_login_id'),'login.user_status'=>'1');

		$whr_str="(crt.org_id=".$this->session->userdata('cc_selected_orgnaization')." OR crt.org_id=0) AND (login.user_level='0' OR login.user_level='1')";

		$select="crt.login_id,crt.crt_first_name,crt.crt_last_name";

		$order=array('crt.crt_first_name'=>'ASC','crt.crt_last_name'=>'ASC');

		

		$this->db->join('cf_login_master as login','login.login_id=crt.login_id');

		$this->db->where($whr_str,NULL,FALSE);

		$crt_list=$this->master_model->getRecords('cf_crisis_response_team as crt',$whr_arr,$select,$order);*/

		

		/* To display task list for a scenarion */

		$whr_arr1=array('task.scenario_id'=>$scenario_id);

		$order1=array('scn.scenario_id'=>'ASC','task.task_id'=>'ASC');

		$select1="scn.*,task.*,crt.crt_first_name,crt.crt_last_name";

		

		$this->db->join('cf_scenario as scn','scn.scenario_id=task.scenario_id','left');

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.crt_id','left');

		$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr1,$select1,$order1);

		/* Task list display  END */

		

		

		$data=array('page_title'=>'Add Task','middle_content'=>'add-task','scn_info'=>$scn_info,/*'crt_list'=>$crt_list,*/'task_list'=>$task_list);

		$this->load->view('reseller/admin-view',$data);

	}

	
	
	//-----------------------------------delete scene----------------------------------------///
	public function delete_task()

	{
		
		

		$data['success']=$data['error']="";
		
		$scenario_id=$this->uri->segment(4);
		
		$pack_id=$this->uri->segment(5);
		
		$task_id=$this->uri->segment(6);

		

		if($this->master_model->deleteRecord('cf_task','task_id',$task_id))

		{	

			$this->master_model->deleteRecord('cf_task','task_id',$task_id);

			$this->session->set_flashdata('success',' Task successfully deleted.');

			redirect(base_url()."reseller/packs/view/".$pack_id);

		}

		else

		{

			$this->session->set_flashdata('error',' Error while deleting task.');

			redirect(base_url()."reseller/packs/view/".$pack_id);

		}
		


	}	
	
	
	
	
	//-----------------------------------add doc----------------------------------------///
	public function add_document()

	{
		$pack_id=$this->uri->segment(4);


		if(isset($_POST['upload_doc']))

		{
			
			//$this->form_validation->set_rules('file_upload_name','File Input','required');

			//$this->form_validation->set_rules('group','User Group','required');

			//if($this->form_validation->run())

			//{
				
				//$group = $this->input->post('group');

				if($_FILES['file_upload_name']['name']!="" && $_FILES['file_upload_name']['error']==0)
	
				{
	
					$doc_config=array('file_name'=>uniqid().$_FILES['file_upload_name']['name'],
	
									'allowed_types'=>'doc|docx|pdf|txt',
	
									'upload_path'=>'uploads/crisis-document/',
	
									'max_size'=>'0');
	
					$this->upload->initialize($doc_config);
	
					//print_r($_FILES['file_upload_name']);exit;
	
					if($this->upload->do_upload('file_upload_name'))
	
					{
	
						$upload_data=$this->upload->data();
	
						$file_name=$upload_data['file_name'];
	
						$insert_array=array(
							'file_upload_name'=>$file_name,
							'cc_id'=>'0',
							'group_id'=>'0',
							'input_pack_id'=>$pack_id,
							'org_id'=>'1'
						);
	
						if($thedoc = $this->master_model->insertRecord('cf_file_upload',$insert_array,true))
	
						{
							
							//$this->session->set_flashdata('success',' Document successfully uploaded.');
	
							redirect(base_url().'reseller/packs/scan/'.$pack_id.'/'.$thedoc);	
	
						}
	
					}
	
					else
	
					{ 
	
						$this->session->set_flashdata('error',$this->upload->display_errors());
	
						redirect(base_url().'reseller/packs/view/'.$pack_id);	
	
						
	
					}
//				}
//
//				else
//
//				{
//
//					$this->session->set_flashdata('error','The uploaded file exceeds the maximum allowed size.');
//
//					redirect(base_url().'reseller/packs/add_document/'.$pack_id);
//
//				}


			}

			


		}
		
		$all_group=$this->master_model->getRecords('user_group');
		
		$the_packs=$this->master_model->getRecords('input_packs',array('id'=>$pack_id),'*');

		$data=array('page_title'=>'Upload Document','middle_content'=>'upload-document','all_group'=>$all_group,'the_packs'=>$the_packs);

		$this->load->view('reseller/admin-view',$data);

	}


	
	
	

	//-----------------------------------Scan doc----------------------------------------///
	public function scan()

	{
		$doc_id = $this->uri->segment(5);

		$pack_id = $this->uri->segment(4);

		$up_doc=$this->master_model->getRecords('cf_file_upload',array('file_upload_id'=>$doc_id));

		// Config to scan.
		$api    = 'https://scan.metascan-online.com/v2/file';
		$apikey = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		$file   = base_url();
		$file.='uploads/crisis-document/';
		$file.=$up_doc[0]['file_upload_name'];
		
		// Build headers array.
		$headers = array(
			'apikey: '.$apikey,
			'filename: '.basename($file)
		);
		
		// Build options array.
		$options = array(
			CURLOPT_URL     => $api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_POST        => true,
			CURLOPT_POSTFIELDS  => file_get_contents($file),
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		// Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$response = json_decode(curl_exec($ch), true);
		
		
		$insert_array = array(
			'data_id'=>$response['data_id'],
			'rest_ip'=>$response['rest_ip']
		);
		$this->master_model->updateRecord('cf_file_upload',$insert_array,array('file_upload_id'=>$doc_id));

		redirect(base_url().'reseller/packs/view/'.$pack_id.'/'.$doc_id);


	}
	
	

	//-----------------------------------Scan report----------------------------------------///
	public function scan_response()

	{
		$thedoc = $_POST['file_id'];

		$up_doc=$this->master_model->getRecords('cf_file_upload',array('file_upload_id'=>$thedoc));
		
		

		//Config for req.
		$req_api = 'https://';
		$req_api .= $up_doc[0]['rest_ip']; //'scan05.metascan-online.com:443/v2'; //$rest_ip;
		$req_api .= '/file/';
		$req_api .=  $up_doc[0]['data_id']; //'48271edc06b34edeb86cf27d3fc54377';//$data_id;
		
		$apikey     = 'e288ca038c5ccd9d4f4fe2b82f5d0416';
		
		//Build headers array.
		$headers = array(
			'apikey: '.$apikey
		);
		
		//Build options array.
		$options = array(
			CURLOPT_URL     => $req_api,
			CURLOPT_HTTPHEADER  => $headers,
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_SSL_VERIFYPEER  => false
		);
		
		$response = "";
		//Init & execute API call.
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		do {
			$response = json_decode(curl_exec($ch), true);
		}
		while ($response["scan_results"]["progress_percentage"] != 100);
		
		//print_r($response);
		
		
		
		echo '<p class="lead" style="margin-bottom: 5px;">';
		//doc icon
		$fileext = explode ('.',$up_doc[0]['file_upload_name']);
		if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
		
		echo '<i class="fa fa-file-pdf-o text-red"></i> ';
		
		}
		
		else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
		
		echo '<i class="fa fa-file-word-o text-blue"></i> ';
		
		}
		
		else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
		
		echo '<i class="fa fa-file-text-o text-muted"></i> ';
		
		}
		
		
		echo substr($up_doc[0]['file_upload_name'],13,50);		
		echo '</p>';
		echo '<p class="lead"  style="margin-bottom: 5px;">';
		echo 'Over all Scan Result: '.$response['scan_results']['scan_all_result_a'];		
		
		echo '</p>';
		
		
		echo ' <div class="table-responsive"> <table class="table table-hover table-bordered table-green" id="response-table"> <thead> <tr>  <th width="25%">Antivirus</th>  <th width="25%">Result</th>  <th width="25%">Scan Time</th> <th width="25%">Update</th> </tr> </thead> <tbody>';
								
								
		foreach ($response['scan_results'] as $key => $scan_details){
			if(is_array($scan_details)){
				foreach($scan_details as $subkey => $subvalue){
					echo '<tr> <td>'.$subkey.'</td>';
					if(is_array($subvalue)){
						//foreach($subvalue as $key => $subsubvalue){
							echo '<td class="text-center">';
							if($subvalue['threat_found']==""){
								echo '<i class="fa fa-check-circle text-success" title="No Threat Found"></i>';
							}
							else{
								echo '<i class="fa fa-exclamation-circle text-danger" title="Threat Found"></i><br>'.$subvalue['threat_found'];
							}
							echo '</td>';
							//echo '<td>'.$subvalue['threat_found'].'</td>'; $subvalue['scan_result_i']
							echo '<td>'.$subvalue['scan_time'].' ms</td>';
							echo '<td>'.substr($subvalue['def_time'], 0, 10).'</td>';
						//}
						
					echo '</tr>';
					} else {
						echo $subkey.' '.$subvalue."<br />";
					}
				}
			} else {
			//	echo $key.': '.$scan_details."<br />";
			}
		}
		
		//scan result	
		$this->master_model->updateRecord('cf_file_upload',array('scan_result'=>$response['scan_results']['scan_all_result_a']),array('file_upload_id'=>$thedoc));
		
		echo ' </tbody> </table> </div><p class="small hidden">Virus scan results from metascan-online.com</p>';
			
	}
	
	

	//-----------------------------------delete doc----------------------------------------///
	public function download_doc()

	{

		$doc_name=$this->uri->segment(4);

		$data=file_get_contents("uploads/crisis-document/".$doc_name);

		$name=substr($doc_name,13,50);

		force_download($name,$data);

	}




	//-----------------------------------delete doc----------------------------------------///
	public function delete_doc()

	{

		$pack_id=$this->uri->segment(4);

		$doc_id=$this->uri->segment(5);

		$doc_name=$this->uri->segment(6);	

		if($this->master_model->deleteRecord('cf_file_upload','file_upload_id',$doc_id))

		{

			@unlink('uploads/crisis-document/'.$doc_name);

			$this->session->set_flashdata('success',' Document successfully deleted.');

			redirect(base_url().'reseller/packs/view/'.$pack_id);

		}

	}
		

	
	
	
	//-----------------------------------update pack----------------------------------------///
	public function update()

	{

		$pack_id=$this->uri->segment(4);

		

		if(isset($_POST['add_pack']))

		{

			$this->form_validation->set_rules('pack_name','Pack Name','required|xss_clean');


			if($this->form_validation->run())

			{

				$pack_name=$this->input->post('pack_name',true);	

				$updt_arr=array('name'=>$pack_name);

				if($this->master_model->updateRecord('input_packs',$updt_arr,array('id'=>$pack_id)))

				{

					$this->session->set_flashdata('success',' Input Pack successfully updated');

					redirect(base_url().'reseller/packs/manage');

				}

				else

				{

					$this->session->set_flashdata('error','Error while updating Input Pack');

					redirect(base_url().'reseller/packs/update/'.$pack_id);

				}



			}

		}

		
		
		$pack_list=$this->master_model->getRecords('input_packs',array('id'=>$pack_id),'*');

		

		$data=array('page_title'=>'Add Organization','middle_content'=>'update-packs', 'pack_list'=>$pack_list);

		$this->load->view('reseller/admin-view',$data);

	}

	

	//-----------------------------------delete pack----------------------------------------///

	public function delete()

	{

		$pack_id=$this->uri->segment(4);

	
		if($this->master_model->deleteRecord('input_packs','id',$pack_id))

		{


			$this->session->set_flashdata('success','Input pack successfully deleted.');

			redirect(base_url().'reseller/packs/manage');



		}

		else

		{

			$this->session->set_flashdata('success','Error deleting Input pack.');

			redirect(base_url().'reseller/packs/manage');

		}


	}

	

}

?>
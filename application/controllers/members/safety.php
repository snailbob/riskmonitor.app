<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Safety extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();

		$this->load->model('email_sending');

	}

	

	#--------------------------------------------->>dashboard view loading<<-------------------------------------

	public function index()

	{
		//awaiting cc list
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		$all_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0', 'cc.activated'=>'no'));

		//threat found docs
		$docs=$this->master_model->getRecords('cf_file_upload',array('scan_result !='=>'Clean'));

		//org with no credit
		$order_arr=array('org.organization_name'=>'ASC');

		$sel_str="cc.crt_first_name,cc.crt_last_name,org.*";

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=org.cc_id');

		$org_list=$this->master_model->getRecords('organization_master as org',array('org.sms_credit'=>'0'),$sel_str,$order_arr);


		$data=array('page_title'=>'Dashboard','middle_content'=>'dashboard','all_cc'=>$all_cc,'org_list'=>$org_list,'docs'=>$docs);

		$this->load->view('members/admin-view',$data);

	}

	

	#---------------------------------------------->>Admin login page<<-----------------------------------------

	public function login()

	{
		$error="";

		if(isset($_SESSION['logged_member']))

		{

			redirect(base_url().'members/safety');


		}

		else

		{

			if(isset($_POST['btn_admin_login']))

			{

				

				$this->form_validation->set_rules('user_name','Username','required|xss_clean');

				$this->form_validation->set_rules('pass_word','Password','required|xss_clean');

				

				if($this->form_validation->run())

				{

					$chk_arr=array(
						'email_id'=>$this->master_model->encryptIt($this->input->post('user_name',true)),
						'pass_word'=>md5($this->input->post('pass_word',true)) 
					);

					

					if($row=$this->master_model->getRecords('employee_master',$chk_arr))

					{

						$_SESSION['logged_member']=$this->master_model->decryptIt($row[0]['email_id']);
						$_SESSION['mfirst_name'] = $this->master_model->decryptIt($row[0]['first_name']);
						$_SESSION['mlast_name'] = $this->master_model->decryptIt($row[0]['last_name']);
						$_SESSION['member_id'] = $row[0]['login_id'];
						//$this->session->set_userdata('logged_member',$this->input->post('user_name',true));
					
						//echo $this->session->userdata('logged_member');
						
						//return false;
						
						
						redirect(base_url().'members/safety');

					}

					else

					{$error="Invalid Credentials";}

				}

				else

				{$error=$this->form_validation->error_string();}

			}

			$data=array('page_title'=>'Admin Login','error'=>$error);

			$this->load->view('members/index',$data);	

		}

	}


	#---------------------------------------------->>CC Forgot password<<-----------------------------------------


	public function forgotpassword()

	{
		
		$success="";

		$error="";

		$data=array('page_title'=>"Forget Password",'middle_content'=>'forget-password','success'=>'','error'=>'');

		if(isset($_POST['btn_forget']))

		{ 

			$this->form_validation->set_rules('user_name','Email address','trim|required|xss_clean|valid_email');

			if($this->form_validation->run())

			{

				$email=$this->master_model->encryptIt($this->input->post('user_name',true));

				$whr=array('email_id'=>$email);

//				$this->db->join('cf_coordinator_master','cf_coordinator_master.login_id=cf_login_master.login_id');
				//$this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=cf_login_master.login_id');

				$email_id=$this->master_model->getRecords('reseller_master',$whr,'*');
				
				
//				$whr2=array('login_id'=>$email_id[0]['login_id']);
//				$cc_name=$this->master_model->getRecords('cf_crisis_response_team',$whr2,'*');
//


				$whr=array('id'=>'1');

				$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

				    //generate random password

					$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

					$pass = array(); //remember to declare $pass as an array

					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

					for ($i = 0; $i < 8; $i++) 

					{

						$n = rand(0, $alphaLength);

						$pass[] = $alphabet[$n];

					}

					$random_pass= implode($pass);

				

//				if(count($email_id) > 0)
				if($email_id != null)

				{	

					$this->master_model->updateRecord('reseller_master',array('pass_word'=>md5($random_pass)),

					array('login_id'=>$email_id[0]['login_id']));

					

					$info_arr=array('from'=>$adminemail[0]['recovery_email'],

									'to'=>$this->master_model->decryptIt($email),

									'subject'=>'Password Recovery',

									'view'=>'forget-password-mail-to-reseller');

					

					$other_info=array('password'=>$random_pass,

									  'email'=>$this->master_model->decryptIt($email),
									  
									  'first_name'=>'',//$cc_name[0]['cc_first_name'],

									  'last_name'=>''//$email_id[0]['cc_last_name']

									);

					

					if($this->email_sending->sendmail($info_arr,$other_info))

					{
						
						$this->master_model->updateRecord('reseller_master',array('pass_word'=>md5($random_pass)),

						array('login_id'=>$email_id[0]['login_id']));

//						$this->session->set_flashdata('success','Please check your email.');
						$data['success']='Please check your email.';
//						redirect(base_url().'signin');
					}

				}

				else

				{
//					$this->session->set_flashdata('error','Email Address is invalid.');
					$data['error']='Email Address is invalid.';
				}

			}

			else

			{
				$data['error']=$this->form_validation->error_string();
//				$this->session->set_flashdata('error',$this->form_validation->error_string());
			}

		}

		$this->load->view('members/forget-password',$data);	

	}

	#---------------------------------------------->>Admin Forgot password<<-----------------------------------------

	/*

	public function forgotpassword()

	{

		$success="";

		$error="";

		$whr=array('id'=>'1');

		$email_id=$this->master_model->getRecords('admin_login',$whr,'*');

		$data=array('page_title'=>"Forget Password",'middle_content'=>'forget-password','success'=>'','error'=>'');

		if(isset($_POST['btn_forget']))

		{ 

			$this->form_validation->set_rules('user_name','User name','trim|required|xss_clean|valid_email');

			if($this->form_validation->run())

			{

				$email=$this->input->post('user_name',true);

				if($email==$email_id[0]['recovery_email'])

				{

					$info_arr=array('from'=>$email_id[0]['recovery_email'],

					'to'=>$email_id[0]['recovery_email'],'subject'=>'Password Recovery',

					'view'=>'forget-password-mail-to-admin');

					$other_info=array('password'=>$email_id[0]['pass_word'],'email'=>$email);

					$this->email_sending->sendmail($info_arr,$other_info);

					$data['success']='Password mail send successfully.';

				}

				else

				{

					$data['error']='Email Address is invalid.';

				}

			}

			else

			{

				$data['error']=$this->form_validation->error_string();

    		}

		}

		$this->load->view('members/forget-password',$data);	

	}*/

	

	#----------------------------------------------->>Admin login<<------------------------------------------------

	public function logout()

	{

		unset($_SESSION['logged_member']);
		unset($_SESSION['mfirst_name']);
		unset($_SESSION['mlast_name']);
		unset($_SESSION['member_id']);
		//$this->session->unset_userdata('logged_member');
		redirect(base_url().'members');

	}
	
	
	
	
	

	//------------------------------ Delete document-------------------------------//

	public function delete()

	{

		$doc_id=$this->uri->segment(4);

		$doc_name=$this->uri->segment(5);	

		if($this->master_model->deleteRecord('cf_file_upload','file_upload_id',$doc_id))

		{

			@unlink('uploads/crisis-document/'.$doc_name);

			$this->session->set_flashdata('success',' Document successfully deleted.');

			redirect(base_url().'members/dashboard');

		}

	}

}
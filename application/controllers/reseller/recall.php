<?php 

class Recall extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}

	

	public function table(){
		/*for($i=1; $i < 90; $i++){
			$this->master_model->updateRecord('cf_recall_guidance',array('guidance'=>''),array('id'=>$i));
			echo 'success';
		}*/
		
		echo $this->master_model->decryptIt('ZjHV1D8sDGcwWi35UGtk6ceKkc7TSY19te79lqcPZtc=');
	}


	public function index() {
		$consultant_id = $_SESSION['reseller_id'];

		$consultant_pack = $this->master_model->getRecords('cf_recall_packs', array('consultant_id'=>$consultant_id));
		
		$pack_id = 18; //recall pack id
		if(count($consultant_pack) > 0){
			$pack_id = $consultant_pack[0]['id'];
		}
		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'manage-recall-tasks',
			'packname'=>$packname,
			'packsteps'=>$packsteps,
			'category'=>$category,
			'pack_id'=>$pack_id
		);
		
		$this->load->view('reseller/admin-view',$data);
		
	}
	/*public function index()

	{
		$reseller_info = $this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));
		$tabs = $reseller_info[0]['deactivated_tab'];
		
		$single_tab = explode(' ',$tabs);

		
		if (!in_array("recall", $single_tab)){
			redirect('consultant');
		}
		
		$step2 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'2'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step3 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'3'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step4 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'4'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step5 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'5'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step6 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'6'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step7 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'7'),'',array('category'=>'ASC','arrangement'=>'ASC'));
		$step8 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'8'),'',array('category'=>'ASC','arrangement'=>'ASC'));

		$category=$this->master_model->getRecords('cf_recall_steps_category',array('deleted'=>'0'),'',array('step_no'=>'ASC','id'=>'ASC'));
		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'manage-recall-tasks',
			'step2'=>$step2,
			'step3'=>$step3,
			'step4'=>$step4,
			'step5'=>$step5,
			'step6'=>$step6,
			'step7'=>$step7,
			'step8'=>$step8,
			'category'=>$category,
		);

		$this->load->view('reseller/admin-view',$data);


	}*/




	public function guidance()

	{
		$step2 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'2'),'',array('category'=>'ASC'));
		$step3 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'3'),'',array('category'=>'ASC'));
		$step4 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'4'),'',array('category'=>'ASC'));
		$step5 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'5'),'',array('category'=>'ASC'));
		$step6 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'6'),'',array('category'=>'ASC'));
		$step7 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'7'),'',array('category'=>'ASC'));
		$step8 = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>'8'),'',array('category'=>'ASC'));

		$data = array(
			'page_title'=>'Add Organization',
			'middle_content'=>'add-task-guidance',
			'step2'=>$step2,
			'step3'=>$step3,
			'step4'=>$step4,
			'step5'=>$step5,
			'step6'=>$step6,
			'step7'=>$step7,
			'step8'=>$step8
		);


		$this->load->view('reseller/admin-view',$data);

	}

	
	
	
	public function update_task(){
	
		$task = $_POST['task'];
		$task_id = $_POST['task_id'];
		
		$updt_arr = array(
			'question'=>$task
		);
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}
		
	
	public function subcategory(){
		
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('deleted'=>'0'));

		$data=array('page_title'=>'Add Organization','middle_content'=>'manage-task-category','category'=>$category);

		$this->load->view('reseller/admin-view',$data);
	}


		
	
	public function update_category(){
	
		$category = $_POST['category'];
		$category_id = $_POST['category_id'];
		
		$updt_arr = array(
			'category_name'=>$category
		);
		if($this->master_model->updateRecord('cf_recall_steps_category',$updt_arr,array('id'=>$category_id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}
		
	
	public function select_new_category(){
	
		$category = $_POST['category'];
		$task_id = $_POST['task_id'];
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '78';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$updt_arr = array(
			'category'=>$category
		);
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			
			$sub_category = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$category));
			$this->session->set_flashdata('success', $action_message['success']);
			if (count($sub_category) > 0 ){
				echo $sub_category[0]['category_name'];
			} else{ 
				echo 'No Category';
			}
			
		}
		else{
			echo 'error';
		}
	}



	public function save_new_category(){
		$catt = $_POST['catt'];
		$step = $_POST['step'];
		$updt_arr = array(
			'category_name'=>$catt,
			'step_no'=>$step
		);
		if($this->master_model->insertRecord('cf_recall_steps_category',$updt_arr)){
			$this->session->set_flashdata('success','<span class="new_cat">Category successfully added.</span>');
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error','Category failed to be added.');
			echo 'error';
		}
	}



	public function select_step(){
		
		$step_no = $_POST['step'];
		
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$step_no,'deleted'=>'0'));


		echo '<label class="control-label" for="select_subcat">Category</label>
		<select id="select_subcat" name="select_subcat" class="form-control">
		  <option value="0">Select</option>';
		  
		foreach($category as $r=>$cat){ 
			
			echo '<option value="';
			echo $cat['id'];
			echo '">';
			echo $cat['category_name'].'</option>';
		}
		echo '</select>';
	}


	public function toggle_task_enable(){
		$task_id = $_POST['id'];
		$disabled = $_POST['disabled'];
		
		if ($disabled == '0'){
			$disable = '1';
		}
		else{
			$disable = '0';
		}
		$updt_arr = array(
			'disabled'=>$disable
		);
		
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			echo 'success';
		}
		
	}
	public function save_sort(){
		$the_id = $_POST['ID'];
		$counter = 1;
		echo count($the_id);
		foreach($the_id as $id){
			$updt_arr = array(
				'arrangement'=>$counter
			);
			if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$id))){
				//echo 'success';
			}
		$counter++;	
		}
		
	}
	
	public function save_sort_step(){
		$the_id = $_POST['ID'];
		$counter = 2;
		echo count($the_id);
		foreach($the_id as $id){
			$the_step = $this->master_model->getRecords('cf_recall_packs_steps', array('id'=>$id));
			
			if($the_step[0]['step_no'] == '2'){
				$updt_arr = array(
					'order'=>'1'
				);
				$this->master_model->updateRecord('cf_recall_packs_steps',$updt_arr,array('id'=>$id));

			}
			else{

				$updt_arr = array(
					'order'=>$counter
				);
				if($this->master_model->updateRecord('cf_recall_packs_steps',$updt_arr,array('id'=>$id))){
					
					
					
					echo 'success';
				}
				
				$counter++;					
			
			}
			
		}
		
	}
	
	

	public function save_new_task(){
		$task = $_POST['task'];
		$task_guidance = $_POST['task_guidance'];
		$step = $_POST['category'];
		$sub_cat = $_POST['sub_cat'];
		
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '81';
		$action_message = $this->common_model->get_message($action_message_id);
		
		
		$updt_arr = array(
			'step_no'=>$step,
			'guidance'=>$task_guidance,
			'question'=>$task,
			'category'=>$sub_cat,
		);
		if($this->master_model->insertRecord('cf_recall_guidance',$updt_arr)){
			$this->session->set_flashdata('success', $action_message['success']);
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}
	}
	
	
	
	
	
	
	public function add_guide(){
	
		$guidance = $_POST['guidance'];
		$task_id = $_POST['task_id'];
		
		$updt_arr = array(
			'guidance'=>$guidance
		);
		if($this->master_model->updateRecord('cf_recall_guidance',$updt_arr,array('id'=>$task_id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}








	public function check_dulication($org_name,$org_id)

	{

		$whr=array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num=$this->master_model->getRecordCount('organization_master',$whr);

		if($num==0)

		{return true;}

		else

		{return false;}

	}

	

	public function ajax_delete(){
		$id = $_POST['id'];
		$table = $_POST['table'];
		
		if ($table == 'cf_recall_steps_category'){
			if($this->master_model->updateRecord($table,array('deleted'=>'1'),array('id'=>$id))){
				$this->master_model->updateRecord('cf_recall_guidance',array('category'=>'0'),array('category'=>$id));
				
				echo 'success';
			} else{
				echo 'error';
			}
		}
		
		else if ($table == 'cf_recall_guidance'){
			if($this->master_model->deleteRecord($table,'id',$id)){
				echo 'success';
			} else{
				echo 'error';
			}
		}
		
		
	}

	/* delete organization */
//
//	public function delete() {
//
//		$org_id=$this->uri->segment(4);
//
//		/* check documents present for organization if documents are present then unlink document first*/
//
//		$documents=$this->master_model->getRecords('cf_file_upload',array('org_id'=>$org_id));
//
//		if(count($documents)>0)
//
//		{
//
//			foreach($documents as $doc)
//
//			{
//
//				@unlink('uploads/crisis-document/'.$doc['file_upload_name']);
//
//			}
//
//		}
//
//		
//
//		/*select cc id assign for organization */
//
//		$cc_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
//
//		
//
//		$del_org="DELETE login,crt FROM organization_master as org 
//
//			LEFT JOIN cf_crisis_response_team as crt ON crt.org_id=org.organization_id 
//
//			LEFT JOIN cf_login_master as login ON login.login_id=crt.login_id 
//
//			WHERE org.organization_id=".$org_id;
//
//		
//
//		if($this->db->query($del_org)) {
//
//			/* delete stakeholders */
//
//			
//
//			$del_stk="DELETE org,login,stakeholder FROM organization_master as org 
//
//			LEFT JOIN cf_stakeholder as stakeholder ON stakeholder.org_id=org.organization_id 
//
//			LEFT JOIN cf_login_master as login ON login.login_id=stakeholder.login_id 
//
//			WHERE org.organization_id=".$org_id;
//
//			$this->db->query($del_stk);
//
//			
//
//		}
//
//		
//		$this->session->set_flashdata('success','Organization successfully deleted.');
//		redirect(base_url().'reseller/organization/manage');
//
//	}

	public function load_emptycat(){
		$stepno = $this->uri->segment(4);
		$pack_id = $this->uri->segment(5);
		
		$categories = $this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$stepno,'recall_pack_id'=>$pack_id, 'deleted'=>'0'));
		
		if(count($categories) > 0){
			foreach($categories as $r=>$value){
				$tasks = $this->master_model->getRecordCount('cf_recall_guidance',array('step_no'=>$stepno,'recall_pack_id'=>$pack_id, 'category'=>$value['id']));
				
				if($tasks > 0){
					echo '<p class="text-muted">No dormat categories on file.</p>';
	 				return false;
				}
				else{
					echo '<div class="checkboxx">
					  <label>
						<input type="checkbox" name="empty_cat[]" value="'.$value['id'].'">
						'.$value['category_name'].'
					  </label>
					</div>';
				
				}
			}
		}else{
			echo '<p class="text-muted">No category was added.</p>';
		}
	}
	
	public function delete_empty_cat(){
		//get validation message from admin gen update = 69, delete = 63, add = 81, user_update = 93, user_delete = 94, category_update = 78, category_add = 96
		$action_message_id = '63';
		$action_message = $this->common_model->get_message($action_message_id);
		
		if(isset($_POST['cate'])){
			
			foreach($_POST['cate'] as $value){
				$this->master_model->updateRecord('cf_recall_steps_category', array('deleted'=>'1'), array('id'=>$value));
				echo 'wow ';
			}

			$this->session->set_flashdata('success', $action_message['success']);
		}
		else{
			$this->session->set_flashdata('error', $action_message['error']);
			echo 'error';
		}
	}

}

?>
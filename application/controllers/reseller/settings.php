<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Settings extends CI_Controller 

{

	public function __construct()

	{

		parent::__construct();


	}

	#--------------------------------------------->>updating admin password<<-------------------------------------

	

	public function changepassword()

	{

		if(isset($_POST['change_cc_pw']))

		{

			$this->form_validation->set_rules('new_pass','Password','required|xss_clean');

			$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean');	

			

			if($this->form_validation->run())

			{

				$new_pass=$this->input->post('new_pass');

				$confirm_pass=$this->input->post('confirm_pass');

				if($new_pass==$confirm_pass)

				{

					$data_array=array('pass_word'=>md5($new_pass));

					

					if($this->master_model->updateRecord('reseller_master',$data_array,array('login_id'=>$_SESSION['reseller_id'])))

					{

						$this->session->set_flashdata('success','Password successfully updated.');

						redirect(base_url().'reseller/settings/changepassword/');

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating information');

						redirect(base_url().'reseller/settings/changepassword/');

					}

				}

				else

				{

					$this->session->set_flashdata('error','Wrong Password Confirmation.');

					redirect(base_url().'reseller/settings/changepassword/');

				}

			}

		}

		$details=$this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));

		$data=array('middle_content'=>'change-password','page_title'=>'Change Password','details'=>$details);	

		$this->load->view('reseller/admin-view',$data);

	}

	

	

	#-------------------------------------->>Function for updating timezone<<-----------------------------------
	public function timezone()

	{

		if(isset($_POST['update_timezone'])) {

			$this->form_validation->set_rules('sel_timezone','Timezone','required|xss_clean');

			if($this->form_validation->run())

			{

				$sel_timezone=$this->input->post('sel_timezone','',true);
				
				$arrr = array('timezone'=>$sel_timezone);
				
				if($this->master_model->updateRecord('cf_crisis_response_team',$arrr,array('login_id'=>$this->session->userdata('logged_cc_login_id')))) {

					$this->session->set_userdata($arrr);
					
					$this->session->set_flashdata('success','Timezone successfully updated.');

					redirect(base_url().'reseller/settings/timezone');

				}

				else {

					$this->session->set_flashdata('error','Error while updating Timezone');
					redirect(base_url().'reseller/settings/timezone');
				}


			}
			
						
		}
		
		$personal_info=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_cc_login_id')));

		$data=array('middle_content'=>'update-timezone','page_title'=>'Update Personal infomation','personal_info'=>$personal_info);	
	
		$this->load->view('reseller/admin-view',$data);


	}
	
	
	
	#-------------------------------------->>Function for updating emails<<-----------------------------------

	public function updateinfo()

	{


		if(isset($_POST['update_personal_info']))

		{

			$this->form_validation->set_rules('f_name','First Name','required|xss_clean');

			$this->form_validation->set_rules('l_name','Last Name','required|xss_clean');

			$this->form_validation->set_rules('ph_no','Mobile No.','required|xss_clean|is_natural');	


			if($this->form_validation->run())

			{

				$f_name=$this->input->post('f_name','',true);

				$l_name=$this->input->post('l_name','',true);

				$address=$this->input->post('address','',true);

				$countrycode = $this->input->post('countrycode','',true);

				$crt_digits = $this->input->post('ph_no','',true);

				$ph_no= $countrycode.$crt_digits;


				

				$data_array=array(
					'first_name'=>$this->master_model->encryptIt($f_name),
					'last_name'=>$this->master_model->encryptIt($l_name),
					'countrycode'=>$this->master_model->encryptIt($countrycode),
					'digits'=>$this->master_model->encryptIt($crt_digits),
					'mobile'=>$this->master_model->encryptIt($ph_no)
				);

				

				if($this->master_model->updateRecord('reseller_master',$data_array,array('login_id'=>$_SESSION['reseller_id'])))

				{

					$this->session->set_flashdata('success','Your details have been successfully updated.');

					redirect(base_url().'reseller/settings/updateinfo');

				}

				else

				{

					$this->session->set_flashdata('error','Error while updating Personal Infomation');
					redirect(base_url().'reseller/settings/updateinfo');
				}

			}

		}	

	$personal_info=$this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));

	$countriescode=$this->master_model->getRecords('country_t');
		

	$data=array('middle_content'=>'update-cc-info','page_title'=>'Update Personal infomation','personal_info'=>$personal_info,'countriescode'=>$countriescode);	

	$this->load->view('reseller/admin-view',$data);

	}

	

}
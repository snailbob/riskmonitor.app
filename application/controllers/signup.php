<?php

class Signup extends CI_Controller{

	public function index(){

		$error = '';
		$data = array(
			'page_title'=>'User Signup',
			'error'=>$error,
			'success'=>''
		);

		$this->load->view('cc/index_signup',$data);	

		

	}

	public function validate_domain(){

		$domain = $_POST['domain'];
		$arr = array(
			'domain'=>$domain
		);

		$org = $this->master_model->getRecords('organization_master', $arr);
		$arr['count'] = count($org);
		$arr['org'] = $org;

		echo json_encode($arr);
	}

	public function mysession(){

		// $url = $_SERVER["SERVER_NAME"];
		// $account = str_replace(".crisisflo.com","",$url);

		// $domain = ($account != '' && $account != 'crisisflo.com' && $account != 'localhost') ? 'https://'.$url.'/' : base_url();
		
		// echo $domain;
		// return false;
		$sess = $this->session->all_userdata();
		
		echo json_encode($sess);
	}
}

?>
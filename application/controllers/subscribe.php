<?php

class Subscribe extends CI_Controller

{

	public function index()

	{

		$error="";



			if(isset($_POST['btn_cc_login']))

			{

				$this->form_validation->set_rules('first_name','First Name','required|xss_clean');

				$this->form_validation->set_rules('last_name','Last Name','required|xss_clean');

				$this->form_validation->set_rules('email_add','Email Address','required|xss_clean|valid_email|is_unique[subscribe_request.email]');

				

				if($this->form_validation->run())

				{
					$fname = $this->input->post('first_name');
					$lname= $this->input->post('last_name');
					$email= $this->input->post('email_add');


					$customer_array=array(
						'name'=>$fname.' '.$lname,
						'date_created'=> date('Y-m-d H:i:s', time()),
						'email'=>$email
					);

					if($this->master_model->insertRecord('subscribe_request',$customer_array,true)){
					
						$this->session->set_flashdata('success',' Subscription request successfully sent.');

						redirect(base_url().'subscribe');
						
					
					}
				}


				else

				{
					$error=$this->form_validation->error_string();
				}

			}

			$data=array('page_title'=>'User Login','error'=>$error,'success'=>'');

			$this->load->view('subscribe-view',$data);	
	}



	public function md55(){
		echo md5('snailbob01@gmail.com');
	}
	
	public function form()

	{
		$cust_id = $this->uri->segment(3);
		$cust_em = $this->uri->segment(4);
		
		$cust_info=$this->master_model->getRecords('subscribe_request',array('id'=>$cust_id));
		
		


		if(isset($_POST['add_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('cc_email','Email','required|xss_clean|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			

			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$cc_email=$this->input->post('cc_email',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				$cc_org=$this->input->post('cc_org',true);

				$modules=$this->input->post('modules',true);

				$cust_type=$this->input->post('cust_type',true);

				$onetime_key=md5(microtime());

				
				
				$themods = "";

				foreach($modules as $check) {
					$themods .= $check; 
				}		
				
				$login_arr=array(
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'user_type'=>$cc_type,
					'user_level'=>'0',
					'onetime_key'=>$onetime_key,
					'user_status'=>'0'
				);



				//filter duplicate email
				$resellers=$this->master_model->getRecords('cf_login_master');
				
				foreach ($resellers as $r=>$value){
					if ($this->master_model->encryptIt($cc_email) == $value['email_id']){
						
						$this->session->set_flashdata('error','Coordinator\'s email already exist in the database.');

						redirect(base_url().'subscribe/form/'.$cust_id.'/'.$cust_em);
					}
				}



				if($login_id=$this->master_model->insertRecord('cf_login_master',$login_arr,TRUE))

				{
					//insert data to organization_master
					$org_arr=array(
						'organization_name'=>$cc_org,
						'active_module'=>$themods,
						'cust_type'=>$cust_type,
						'cc_id'=>$login_id
					);
					
					$org_id=$this->master_model->insertRecord('organization_master',$org_arr,TRUE);



					$cc_arr=array(
						'login_id'=>$login_id,
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_organisation'=>$cc_org,
						'crt_email'=>$this->master_model->encryptIt($cc_email),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code),
						'cc_id'=>$login_id,
						'cust_id'=>$cust_id,
						'org_id'=>0
					);

					//create text file for organization chat room
					$room_name = 'chat_room/org';
					$room_name .= $org_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	


					if($this->master_model->insertRecord('cf_crisis_response_team',$cc_arr))

					{

						//$this->session->set_flashdata('success','CC added successfully');


						$this->master_model->updateRecord('subscribe_request',array('added_cc'=>'1'),array('id'=>$cust_id));



		
		$config = array (
			'mode' => 'sandbox' , 
			
//			'acct1.UserName' => 'jb-us-seller_api1.paypal.com',
//			'acct1.Password' => 'WX4WTU3S8MY44S7F', 
//			'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy'	
			
					
					
			
			
			'acct1.UserName' => 'sdk-three_api1.sdk.com',
			'acct1.Password' => 'QFZCWN5HZM8VBG7Q',
			'acct1.Signature' => 'A-IzJhZZjhg29XQ2qnhapuwxIDzyAZQ92FRP5dqBzVesOkzbdUONzmOU'


//			'acct1.UserName' => 'bobby-facilitator_api1.francismedia.net',//'jb-us-seller_api1.paypal.com',
//			'acct1.Password' => '1407901320',// 'WX4WTU3S8MY44S7F', 
//			'acct1.Signature' => 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AOtL6Y.S4aq.mcFj3YjQgZHjVRTs' //'AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy'
		);
		$paypalService = new PayPalAPIInterfaceServiceService($config);
		$paymentDetails= new PaymentDetailsType();
		
		
		
		$prod = array();
		$prod_p = array();
		
		foreach($modules as $m_id) {
			$moduless = $this->master_model->getRecords('module_master',array('id'=>$m_id));
			array_push($prod, $moduless[0]['name']);
			array_push($prod_p, $moduless[0]['price']);
		}		
		
		
		// details about payment
		$paymentDetails = new PaymentDetailsType();
		$itemTotalValue = 0;
		$taxTotalValue = 0;
		/*
		 * iterate trhough each item and add to atem detaisl
		 */
		for($i=0; $i<count($prod); $i++) {
			$itemAmount = new BasicAmountType('USD', $prod_p[$i]);	
			$itemTotalValue += $prod_p[$i] * 1; 
			$taxTotalValue += 0 * 1; //$_REQUEST['itemSalesTax'][$i] * $_REQUEST['itemQuantity'][$i];
			$itemDetails = new PaymentDetailsItemType();
			$itemDetails->Name = $prod[$i];
			$itemDetails->Amount = $itemAmount;
			$itemDetails->Quantity = 1; //$_REQUEST['itemQuantity'][$i];
			/*
			 * Indicates whether an item is digital or physical. For digital goods, this field is required and must be set to Digital. It is one of the following values:
		
			Digital
		
			Physical
		
			 */
			$itemDetails->ItemCategory = 'Digital'; //$_REQUEST['itemCategory'][$i];
			$itemDetails->Tax = new BasicAmountType('USD', 0);	
			
			$paymentDetails->PaymentDetailsItem[$i] = $itemDetails;	
		}
		
		/*
		 * The total cost of the transaction to the buyer. If shipping cost and tax charges are known, include them in this value. If not, this value should be the current subtotal of the order. If the transaction includes one or more one-time purchases, this field must be equal to the sum of the purchases. If the transaction does not include a one-time purchase such as when you set up a billing agreement for a recurring payment, set this field to 0.
		 */
		$orderTotalValue =  $itemTotalValue + $taxTotalValue;
		
		
		
		/*$itemDetails = new PaymentDetailsItemType();
		$itemDetails->Name = 'Standard CrisisFlo';
		$itemAmount = '1.00';
		$itemDetails->Amount = $itemAmount;
		$itemQuantity = '1';
		$itemDetails->Quantity = $itemQuantity;

		
		$paymentDetails->PaymentDetailsItem[0] = $itemDetails;

		$orderTotal = new BasicAmountType();
		$orderTotal->currencyID = 'USD';
		$orderTotal->value = $itemAmount * $itemQuantity; */
		
		$paymentDetails->OrderTotal = $itemTotalValue + $taxTotalValue; //$orderTotal;
		$paymentDetails->PaymentAction = 'Sale';
		
		$setECReqDetails = new SetExpressCheckoutRequestDetailsType();
		$setECReqDetails->PaymentDetails[0] = $paymentDetails;
		$setECReqDetails->CancelURL = base_url().'subscribe/checkout/failed/'.$cust_id.'/'.$login_id;
		$setECReqDetails->ReturnURL = base_url().'subscribe/checkout/success/'.$cust_id.'/'.$login_id;
		
		$setECReqType = new SetExpressCheckoutRequestType();
		$setECReqType->Version = '104.0';
		$setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
		
		$setECReq = new SetExpressCheckoutReq();
		$setECReq->SetExpressCheckoutRequest = $setECReqType;
		
		$setECResponse = $paypalService->SetExpressCheckout($setECReq);
			
		if(isset($setECResponse)) {
			/*echo "<table>";
			echo "<tr><td>Ack :</td><td><div id='Ack'>$setECResponse->Ack</div> </td></tr>";
			echo "<tr><td>Token :</td><td><div id='Token'>$setECResponse->Token</div> </td></tr>";
			echo "</table>";
			echo '<pre>';
			print_r($setECResponse);
			echo '</pre>';*/
			if($setECResponse->Ack =='Success') {
				$token = $setECResponse->Token;
				// Redirect to paypal.com here
				
				redirect('https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . $token);
				
				//$payPalURL = 'https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . $token;
				//echo" <a href=$payPalURL><b>* Redirect to PayPal to login </b></a><br>";
			}
		}			
				



						$this->session->set_flashdata('success',' CC added successfully.');

						//redirect(base_url().'subscribe/form/'.$cust_id.'/'.$cust_em);

						

					}

					else

					{

						$this->session->set_flashdata('error','Error while adding CC');

						redirect(base_url().'subscribe/form/'.$cust_id.'/'.$cust_em);

					}

				}

			}

		}

		if ((count($cust_info) > 0) && (md5($cust_info[0]['email']) == $cust_em)){
		
			$countriescode=$this->master_model->getRecords('country_t');
			
			$modules=$this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
			
			$data=array('page_title'=>'Setup CC','countriescode'=>$countriescode,'modules'=>$modules);
	
			$this->load->view('subscribe-cc-form',$data);
		}
		
		else{
		echo '<div><h1 style="text-align: center; vertical-align: center;">Sorry!! :(</h1></div>';
		
		}
	}


	public function checkout(){
		$result = $this->uri->segment(3);
		$cust_id = $this->uri->segment(4);
		$login_id = $this->uri->segment(5);


			


/*		$this->load->library('configuration');
		
		$token = $_GET['token'];
		
		$getExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType($token);
		
		$getExpressCheckoutReq = new GetExpressCheckoutDetailsReq();
		$getExpressCheckoutReq->GetExpressCheckoutDetailsRequest = $getExpressCheckoutDetailsRequest;
*/

		
		if ($result == 'success'){
			
			$this->master_model->updateRecord('subscribe_request',array('pp_token'=>$_GET['token']),array('id'=>$cust_id));
			$this->master_model->updateRecord('cf_crisis_response_team',array('subscription'=>'2'),array('login_id'=>$login_id));
			
			$admin_email=$this->master_model->getRecords('email_id_master');

			$info_arr=array('to'=>$admin_email[0]['info_email'],'from'=>'admin@crisisflo.com', //

			'subject'=>'A customer successfully added a CC','view'=>'registration-mail-to-admin');

			

			$other_info=array(/*'first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,

			'email'=>$cc_email*/);

			

			$this->email_sending->sendmail($info_arr,$other_info);
			
			
			
			redirect('subscribe/success');
		}
		
		else{
			
			
			$this->master_model->updateRecord('subscribe_request',array('pp_token'=>$_GET['token']),array('id'=>$cust_id));
			$this->master_model->updateRecord('cf_crisis_response_team',array('subscription'=>'1'),array('login_id'=>$login_id));
			redirect('subscribe/failed');
		}
			
			

		
	}
	
	public function success(){
		
		$this->load->view('subscribe-sucess');
	
	}
	
	public function failed(){
		
		$this->load->view('subscribe-failed');

	}

	public function activate_extension(){
		$id = $this->uri->segment(3);
		$user = $this->master_model->getRecords('cf_login_master', array('login_id'=>$id));
		
		if(!empty($user)){
			$trial = $user[0]['trial_end'];
			$trial_start = $user[0]['trial_start'];
			

			$date1= strtotime($trial_start);
			
			$date2= strtotime($trial);

			//Calculate the difference.
			$difference = $date2 - $date1;

			//Convert seconds into days.
			$total_diff = floor($difference / (60*60*24) );
			$total_diff = max($total_diff,0);
			
			if($total_diff < 32){

				$effectiveDate = date('Y-m-d h:i:s', strtotime("+1 months", strtotime($trial)));
				
				$this->master_model->updateRecord('cf_login_master', array('trial_end'=>$effectiveDate), array('login_id'=>$id));

				$this->load->view('trial_extension_success');
				
			}
			else{
				$this->load->view('trial_extension_fail');
			}

		}

	}

}

?>
<?php 

class Verification extends CI_Controller

{

	public function __construct() {

		parent::__construct();

	}

	

	public function cc() {

		$key=$this->uri->segment(3);

		if(isset($_POST['verify_cc']))

		{

			$this->form_validation->set_rules('cc_password','Password','required|xss_clean');

			$this->form_validation->set_rules('confirm_cc_password','Confirm Password','required|xss_clean|matches[cc_password]');

			

			if($this->form_validation->run()) {

				$cc_password = md5($this->input->post('cc_password',true));

				$cc_login_id = $this->input->post('cc_login_id',true);
				$cc_login_email = $this->input->post('cc_login_email',true);
				$cc_firstname = $this->input->post('cc_firstname',true);
				$cc_lastname = $this->input->post('cc_lastname',true);

				$first_name = $this->input->post('first_name',true);
				$last_name = $this->input->post('last_name',true);
				$position = $this->input->post('position',true);
				$crisis_function = $this->input->post('crisis_function',true);
				
				$nowtime = date("Y-m-d h:i:s");
				$cc_arr = array(
					'date_profile_activated'=>$nowtime,
					'pass_word'=>$cc_password,
					'user_status'=>'1'
				);
				
				$succ_mess ='You have successfully activated your New Recall Coordinator Account. You can now sign in <a href="'.base_url().'signin">here</a>.';

				//check if demo user
				$cc = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_login_id));
						
				if(count($cc) > 0){
					if($cc[0]['user_type'] == 'demo' || $cc[0]['single_user'] == 'y'){
						if($cc[0]['trial_start'] == '0000-00-00 00:00:00' && ($cc[0]['stripe_customer_id'] == '0' || $cc[0]['stripe_customer_id'] == '')){
							
							$start = date('Y-m-d H:i:s');
							$end = date('Y-m-d H:i:s', strtotime("+1 months"));
							
							$cc_arr['trial_start'] = $start;
							$cc_arr['trial_end'] = $end;

							$succ_mess ='You have successfully activated your trial account. You can now sign in <a href="'.base_url().'signin">here</a>.';
							

							//$this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$cc_login_id));

							
						}
						else{
							$cc_arr['trial_start'] = date('Y-m-d H:i:s', strtotime("-30 days"));
							$cc_arr['trial_end'] = date('Y-m-d H:i:s');
						}
					}
				}
				
				
				if($this->master_model->updateRecord('cf_login_master', $cc_arr, array('login_id'=>$cc_login_id))) {
			

					$location = $this->input->post('crt_location');

					$location_lat = $this->input->post('lat');

					$location_lng = $this->input->post('lng');

					$countrycode = $this->input->post('countrycode');

					$crt_digits = ltrim($this->input->post('crt_no'), '0');

					$crt_no= $countrycode.$crt_digits;

					// $group = $this->input->post('group');

					$crt_func = $this->input->post('crt_func');

					$alt_member = $this->input->post('alter_member');
					
			
					//update name info and position
					$arr = array(
						'crt_first_name'=>$this->master_model->encryptIt($first_name),
						'crt_last_name'=>$this->master_model->encryptIt($last_name),
						'crt_position'=>$position,
						'crisis_function'=>$crisis_function,
						'location'=>$location,
						'location_lat'=>$location_lat,
						'location_lng'=>$location_lng,
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($crt_no),
						'crisis_function'=>$crt_func,
					);
					$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$cc_login_id));
				
				
					/**script to send email after confirmation*/
					$whr = array('id'=>'1');

					$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

						
					$info_arr = array(
						'from'=>$adminemail[0]['recovery_email'],
						'to'=>$cc_login_email,
						'subject'=>'CrisisFlo account successfully activated',
						'view'=>'registration-mail-to-cc-success'
					);
					
					$other_info = array(
						'first_name'=>$this->master_model->decryptIt($cc_firstname),
						'last_name'=>''//$this->master_model->decryptIt($cc_lastname)
					);
					
					$this->email_sending->sendmail($info_arr, $other_info);
					/**.script to send email after confirmation*/


					$this->session->set_flashdata('success',$succ_mess);

					$user_login_for_session = $this->common_model->user_login_for_session($cc_login_id);

					$this->session->set_userdata($user_login_for_session);

					redirect('cc/dashboard');
					// redirect(base_url().'verification/cc/'.$key);		

				}

				//redirect(base_url().'signin');

			}

		}

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=login.login_id');

		$cc_info = $this->master_model->getRecords('cf_login_master as login',array('login.onetime_key'=>$key));
		// header('Content-Type: application/json');
		// header('Access-Control-Allow-Origin: *');
		// echo json_encode($cc_info);
		// return false;
		
		$data = array('page_title'=>'CC Verification','cc_info'=>$cc_info);

		$this->load->view('verify-cc',$data);


	}

	

	public function stk()

	{

		$success=$error=$incorrect='';

		$onetimekey=$this->uri->segment(3);

		

		if(isset($_POST['verify_correct']))

		{ 

			if($this->master_model->updateRecord('cf_login_master',array('user_status'=>'1'),array('onetime_key'=>'"'.$onetimekey.'"')))

			{   

				$success='Your contact details successfully verified.';

			}

			else

			{

				$error='Error while Verification.';

			}

		}

		if(isset($_POST['verify_incorrect']))

		{

			$incorrect='1';

		}	

		if(isset($_POST['edit_info']))

		{ 

			$incorrect='1';

			$this->form_validation->set_rules('stk_first_name','First Name','required');

			$this->form_validation->set_rules('stk_last_name','Last Name','required');

			$this->form_validation->set_rules('stk_position','Position','required');

			$this->form_validation->set_rules('stk_mobile_num','Mobile Number','required');

			if($this->form_validation->run())

			{

				$login_id=$this->input->post('login_id');

				$stk_first_name=$this->input->post('stk_first_name');

				$stk_last_name=$this->input->post('stk_last_name');

				$stk_position=$this->input->post('stk_position');

				$stk_mobile_num=$this->input->post('stk_mobile_num');

				$update_array=array(
					'stk_first_name'=>$this->master_model->encryptIt($stk_first_name),
					'stk_last_name'=>$this->master_model->encryptIt($stk_last_name),
					'stk_position'=>$stk_position,
					'stk_mobile_num'=>$this->master_model->encryptIt($stk_mobile_num)					
				);

				if($this->master_model->updateRecord('cf_stakeholder',$update_array,array('login_id'=>$login_id)))

				{

					$success="Your contact details have been successfully updated. If your details are now correct, please click the Correct button below to confirm.";

					$incorrect='';

				}

			}

		}

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_stakeholder.login_id');

		$stk_info=$this->master_model->getRecords('cf_stakeholder',array('cf_login_master.onetime_key'=>$onetimekey));	

		//print_r(stk_info);

		$data=array('page_title'=>"Verify CRT",'middle_content'=>'verify-stk','stk_info'=>$stk_info,'incorrect'=>$incorrect,'success'=>$success,'error'=>$error);

		$this->load->view('cc/verify-stk',$data);

	}

	
	
	public function purchaser(){
		$id = $this->uri->segment(3);
		
		//echo md5(37);
		$purchasers = $this->master_model->getRecords('organization_signups');
		
		$data['result'] = 'empty';
		$data['page_title'] = 'Create Coordinator\'s Account';
		$data['purchase_data'] = array();
		$data['mobile_code'] = $this->master_model->getRecords('country_t');
		
		if(count($purchasers) > 0){
			foreach($purchasers as $r=>$value){
				if(md5($value['id']) == $id){
					$data['result'] = 'match';
					$data['purchase_data'] = $value;
				}
			}
		}

		
		$this->load->view('cc/verify-purchaser',$data);
	}
	

	public function crt2(){

		$onetime_key = $this->uri->segment(3);
		
		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
		
		$all_crt = $this->master_model->getRecords('cf_crisis_response_team',array('cf_login_master.onetime_key'=>$onetime_key));
		
		$countriescode = $this->master_model->getRecords('country_t');
		
		$data = array(
			'page_title'=>"Verify CRT",
			'middle_content'=>'verify-crt2',
			'success'=>'',
			'error'=>'',
			'crt_info'=>$all_crt,
			'countriescode'=>$countriescode
		);

		$this->load->view('cc/verify-crt2',$data);
		 
	}
	
	
	public function crt2_confirm(){
		$user_status = $_POST['options'];
		$crt_id = $_POST['crt_id'];
		
		$input_array = array(
			'user_status'=>$user_status,
			'pass_word'=>'',
			'received_sms'=>'1'
		);

		$this->master_model->updateRecord('cf_login_master', $input_array, array('login_id'=> $crt_id));
		
		echo 'success';
		
	}

	public function crt(){

		 $onetime_key = $this->uri->segment(3);

		

			if(isset($_POST['verify_crt'])){

				$this->form_validation->set_rules('options','Received SMS','required');

				$this->form_validation->set_rules('crt_password','Password','required');

				$this->form_validation->set_rules('confirm_crt_password','Confirm Password','required');

				

				if($this->form_validation->run()){

					$crt_password=$this->input->post('crt_password');
					
					$received_sms=$this->input->post('options');

					$confirm_crt_password=$this->input->post('confirm_crt_password');
					
					$cc_login_email=$this->input->post('crt_email',true);
					$cc_firstname=$this->input->post('crt_first_name',true);
					$cc_lastname=$this->input->post('crt_last_name',true);

					if($crt_password==$confirm_crt_password)

					{   
						$nowtime = date("Y-m-d h:i:s");
						$input_array = array(
							'user_status'=>'1',
							'pass_word'=>md5($crt_password),
							'received_sms'=>$received_sms,
							'date_profile_activated'=>$nowtime
						);

					

						if($this->master_model->updateRecord('cf_login_master',$input_array,

						array('onetime_key'=> "'".$onetime_key."'")))

						//echo $this->db->last_query(); exit;

						{
							
							
							/**script to send email after confirmation*/
							$whr = array('id'=>'1');
		
							$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');
		
								
							$info_arr=array(
								'from'=>$adminemail[0]['recovery_email'],
								'to'=>$cc_login_email,
								'subject'=>'CrisisFlo account successfully activated',
								'view'=>'crt_registration_on_crisesflo_success'
							);
							
							$other_info=array(
							  'first_name'=>$this->master_model->decryptIt($cc_firstname),
							  'last_name'=>$this->master_model->decryptIt($cc_lastname)
							);
							
							$this->email_sending->sendmail($info_arr,$other_info);

							
							$this->session->set_flashdata('success','Password confirmation successful. You can now login <a href="'.base_url().'signin">here</a>.');
							redirect(base_url().'verification/crt/'.$onetime_key);
							
							
						}

					}

					else{

						$data['error']='Password Confirmation is wrong.';

					}

				}

			}

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');

		$all_crt=$this->master_model->getRecords('cf_crisis_response_team',array('cf_login_master.onetime_key'=>$onetime_key));

		$countriescode=$this->master_model->getRecords('country_t');
		

		$data = array(
			'page_title'=>"Verify CRT",
			'middle_content'=>'verify-crt',
			'success'=>'',
			'error'=>'',
			'crt_info'=>$all_crt,
			'countriescode'=>$countriescode
		);

		$this->load->view('cc/verify-crt',$data);

	}

	
	//-----------------------------------------update mobile in crt verify
	public function update_mobile() {
		
        // load library
        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');
		
		
		$country = $_POST['countryc'];
		$crt = $_POST['crtno'];

		$mobile = $country.$crt;
		$crt_login_id = $_POST['crt_login_id'];

		$arr = array(
			'countrycode'=>$this->master_model->encryptIt($country),
			'crt_digits'=>$this->master_model->encryptIt($crt),
			'crt_mobile'=>$this->master_model->encryptIt($mobile)
		);
		
		if($this->master_model->updateRecord('cf_crisis_response_team',$arr,array('login_id'=>$crt_login_id))){

			//send sms
			$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'crt-confirm-mobile'));

			$from = 'CrisisFlo';
			$message = array(
				'text' => $sms_message[0]['text']
			);
			
			
			$to = $mobile;
			$response = $this->nexmo->send_message($from, $to, $message);
			
			foreach ($response->messages as $messageinfo) {
				$recipient = $messageinfo->{'to'};
				$status = $messageinfo->{'status'};
				
			}

			if($status=='0'){
				echo 'SMS successfully sent to mobile number '.$recipient.'.';
			}else{
				echo 'SMS not sent. Please try again';
			}
						
						
		}
	}


	public function contactcrt()

	{

		$success=$error=$incorrect='';

		$onetime_key=$this->uri->segment(3);

		if(isset($_POST['verify_correct']))

		{ 

			if($this->master_model->updateRecord('cf_login_master',array('contact_verified'=>'1'),array('onetime_key'=>'"'.$onetime_key.'"')))

			{   

				$success='Thanks for the update!';

			}

			else

			{

				$error='Error while confirming account.';

			}

		}

		if(isset($_POST['verify_incorrect']))

		{

			$incorrect='1';

		}

		if(isset($_POST['edit_info']))

		{ 

			$incorrect='1';

			$this->form_validation->set_rules('crt_first_name','First Name','required');

			$this->form_validation->set_rules('crt_last_name','Last Name','required');

			$this->form_validation->set_rules('crt_position','Position','required');

			$this->form_validation->set_rules('crt_mobile','Mobile Number','required');

			if($this->form_validation->run())

			{

				$login_id=$this->input->post('login_id');

				$crt_first_name=$this->input->post('crt_first_name');

				$crt_last_name=$this->input->post('crt_last_name');

				$crt_position=$this->input->post('crt_position');

				$crt_mobile=$this->input->post('crt_mobile');

				$update_array=array(
					'crt_first_name'=>$this->master_model->encryptIt($crt_first_name),
					'crt_last_name'=>$this->master_model->encryptIt($crt_last_name),
					'crt_position'=>$crt_position,
					'crt_mobile'=>$this->master_model->encryptIt($crt_mobile)
				);

				if($this->master_model->updateRecord('cf_crisis_response_team',$update_array,array('login_id'=>$login_id)))

				{

					$success="<b>Success!</b> Your contact details have been successfully updated. If your details are now correct, please click the Correct button below to confirm.";

					$incorrect='';

				}

			}

		}

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');

		$crt_info=$this->master_model->getRecords('cf_crisis_response_team',array('cf_login_master.onetime_key'=>$onetime_key));

		$data=array('page_title'=>"Verify Stackholder",'success'=>$success,

		'error'=>$error,'crt_info'=>$crt_info,'incorrect'=>$incorrect);

		$this->load->view('verify-crt-contact',$data);

	}

	

	public function contactstk()

	{    

		$success=$error=$incorrect='';

		$onetime_key=$this->uri->segment(3);

		if(isset($_POST['verify_correct']))

		{ 

			if($this->master_model->updateRecord('cf_login_master',array('contact_verified'=>'1'),array('onetime_key'=>'"'.$onetime_key.'"')))

			{   

				$success='Thanks for the update!';

			}

			else

			{

				$error='Error while confirming account.';

			}

		}

		if(isset($_POST['verify_incorrect']))

		{

			$incorrect='1';

		}

		if(isset($_POST['edit_info']))

		{ 

			$incorrect='1';

			$this->form_validation->set_rules('stk_first_name','First Name','required');

			$this->form_validation->set_rules('stk_last_name','Last Name','required');

			$this->form_validation->set_rules('stk_position','Position','required');

			$this->form_validation->set_rules('stk_mobile_num','Mobile Number','required');

			if($this->form_validation->run())

			{

				$login_id=$this->input->post('login_id');

				$stk_first_name=$this->input->post('stk_first_name');

				$stk_last_name=$this->input->post('stk_last_name');

				$stk_position=$this->input->post('stk_position');

				$stk_mobile_num=$this->input->post('stk_mobile_num');

				$update_array=array(
					'stk_first_name'=>$this->master_model->encryptIt($stk_first_name),
					'stk_last_name'=>$this->master_model->encryptIt($stk_last_name),
					'stk_position'=>$stk_position,
					'stk_mobile_num'=>$this->master_model->encryptIt($stk_mobile_num)
				);

				if($this->master_model->updateRecord('cf_stakeholder',$update_array,array('login_id'=>$login_id)))

				{

					$success="<b>Success!</b> Your contact details have been successfully updated. If your details are now correct, please click the Correct button below to confirm.";

					$incorrect='';

				}

			}

		}

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_stakeholder.login_id');

		$stk_info=$this->master_model->getRecords('cf_stakeholder',array('cf_login_master.onetime_key'=>$onetime_key));

		$data=array('page_title'=>"Verify Stackholder",'success'=>$success,

		'error'=>$error,'stk_info'=>$stk_info,'incorrect'=>$incorrect);

		$this->load->view('verify-stk-contact',$data);

	}
	
	
	
	

	public function consultant()

	{

		$key=$this->uri->segment(3);

		if(isset($_POST['verify_cc']))

		{

			$this->form_validation->set_rules('cc_password','Password','required|xss_clean');

			$this->form_validation->set_rules('confirm_cc_password','Confirm Password','required|xss_clean|matches[cc_password]');

			

			if($this->form_validation->run())

			{

				$cc_password=md5($this->input->post('cc_password',true));

				$cc_login_id=$this->input->post('cc_login_id',true);
				$cc_login_email=$this->input->post('cc_login_email',true);
				$cc_firstname=$this->input->post('cc_firstname',true);
				$cc_lastname=$this->input->post('cc_lastname',true);


				if($this->master_model->updateRecord('reseller_master',array('pass_word'=>$cc_password,'user_status'=>'1'),array('login_id'=>$cc_login_id)))

				{
					/**script to send email after confirmation*/
					$whr=array('id'=>'1');

					$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

						
					$info_arr=array('from'=>$adminemail[0]['recovery_email'],
	
					'to'=>$cc_login_email,
	
					'subject'=>'CrisisFlo account successfully activated',
	
					'view'=>'registration-mail-to-reseller-success');
					
					$other_info=array(
					  'first_name'=>$this->master_model->decryptIt($cc_firstname),

					  'last_name'=>$this->master_model->decryptIt($cc_lastname)
					);
					
					$this->email_sending->sendmail($info_arr,$other_info);
					/**.script to send email after confirmation*/


					//$this->session->set_flashdata('success','You have successfully activated your Crisis Coordinator account. Once an organization code has been assigned to your account, you may log into your user panel.');
					$succ_mess ='You have successfully activated your Reseller\'s account. You can now sign in <a href="'.base_url().'consultant/">here</a>.';
					$this->session->set_flashdata('success',$succ_mess);

					redirect(base_url().'verification/consultant/'.$key);		

				}

				

				//redirect(base_url().'signin');

			}

		}

		//$this->db->join('cf_crisis_response_team as cc','cc.login_id=login.login_id');

		$cc_info=$this->master_model->getRecords('reseller_master',array('onetime_key'=>$key));

		$data=array('page_title'=>'CC Verification','cc_info'=>$cc_info);

		$this->load->view('verify-reseller',$data);

		

	}



	public function members()

	{

		 $onetime_key=$this->uri->segment(3);

		

			if(isset($_POST['verify_mem']))

			{

				$this->form_validation->set_rules('first_name','First Name','required');

				$this->form_validation->set_rules('last_name','Last Name','required');


				$this->form_validation->set_rules('ph_no','Mobile Number','required|numeric');

				$this->form_validation->set_rules('countrycode','Country Code','required');


				$this->form_validation->set_rules('crt_password','Password','required');

				$this->form_validation->set_rules('confirm_crt_password','Confirm Password','required');

				

				if($this->form_validation->run())

				{

					$crt_password=$this->input->post('crt_password');

					$confirm_crt_password=$this->input->post('confirm_crt_password');
					
					$cc_login_email=$this->input->post('email_id',true);
					$firstname=$this->input->post('first_name',true);
					$lastname=$this->input->post('last_name',true);
					$countrycode=$this->input->post('countrycode',true);
	
					$crt_digits=$this->input->post('ph_no',true);
					
					$mobile_num= $countrycode.''.$crt_digits;


					if($crt_password==$confirm_crt_password)

					{   

						$input_array=array(
							'user_status'=>'1',
							'pass_word'=>md5($crt_password),
							'first_name'=>$this->master_model->encryptIt($firstname),
							'last_name'=>$this->master_model->encryptIt($lastname),
							'countrycode'=>$this->master_model->encryptIt($countrycode),
							'digits'=>$this->master_model->encryptIt($crt_digits),
							'mobile'=>$this->master_model->encryptIt($mobile_num)						
						);

					

						if($this->master_model->updateRecord('employee_master',$input_array,

						array('onetime_key'=> "'".$onetime_key."'")))

						//echo $this->db->last_query(); exit;

						{
							
							
							/**script to send email after confirmation*/
							$whr=array('id'=>'1');
		
							$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');
		
								
							$info_arr=array('from'=>$adminemail[0]['recovery_email'],
			
							'to'=>$cc_login_email,
			
							'subject'=>'CrisisFlo account successfully activated',
			
							'view'=>'cmember_registration_on_crisesflo_success');
							
							$other_info=array(
							  'first_name'=>$this->master_model->decryptIt($firstname),
		
							  'last_name'=>$this->master_model->decryptIt($lastname)
							);
							
							$this->email_sending->sendmail($info_arr,$other_info);
							/**.script to send email after confirmation*/
		
							$data['success']='Account Verified Successfully.';
							redirect(base_url().'members');
							
							
						}

					}

					else

					{

						$data['error']='Password Confirmation is wrong.';

					}

				}

			}

		$all_mem=$this->master_model->getRecords('employee_master',array('onetime_key'=>$onetime_key));


		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>"Verify Members",'middle_content'=>'verify-crt','success'=>'',

		'error'=>'','all_mem'=>$all_mem,'countriescode'=>$countriescode);

		$this->load->view('cc/verify-members',$data);

	}


	public function bulkemail(){
		$bulk_id = $this->uri->segment(3);
		$email = $this->uri->segment(4);
		$bulkmd5 = $this->uri->segment(5);
		
		$status = 'error';
		if($bulk_id != '' && $bulkmd5 != '' && $email != ''){
			$bulk_data = $this->master_model->getRecords('bulk_data', array('id'=>$bulk_id));
			
			if(count($bulk_data) > 0){
				
				if(md5($bulk_data[0]['emails']) == $email && md5($bulk_data[0]['id']) == $bulkmd5){
					$this->master_model->updateRecord('bulk_data', array('email_confirmed'=>'1'), array('id'=>$bulk_data[0]['id']));
					
					$status = 'verified';
				}
			}//check if bulk data not empty
		}


		$data=array(
			'page_title'=>"Verify Members",
			'bulkmd5'=>$bulkmd5,
			'email'=>$email,
			'bulk_id'=>$bulk_id,
			'status'=>$status
			
		);

		$this->load->view('cc/verify-bulk-email',$data);
	}

	public function kpi(){
		$bulk_id = $this->uri->segment(3);
		$email = $this->uri->segment(4);
		$bulkmd5 = $this->uri->segment(5);
		
		$status = 'error';
		$bulk_data = $this->master_model->getRecords('bulk_kpi_owners', array('id'=>$bulk_id,'deleted'=>'0'));
		if($bulk_id != '' && $bulkmd5 != '' && $email != ''){
			
			if(count($bulk_data) > 0){
				
				if(md5($bulk_data[0]['email']) == $email && md5($bulk_data[0]['id']) == $bulkmd5){
					$status = 'allowed';
				}
			}//check if bulk data not empty
		}


		$data=array(
			'page_title'=>"Update KPI",
			'bulkmd5'=>$bulkmd5,
			'email'=>$email,
			'bulk_id'=>$bulk_id,
			'status'=>$status,
			'bulk_data'=>$bulk_data
			
		);

		$this->load->view('cc/verify-kpi-owner',$data);
	}
	
	
	public function kpiupdate(){
		$owner_id = $_POST['owner_id'];
		$owner_type = $_POST['owner_type'];
		$kpi_id = $_POST['kpi_id'];
		$incident_id = $_POST['incident_id'];
		$incident_type = $_POST['incident_type'];


		$whr_kpi = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'id !='=>$owner_id
		);
		
		
		$kpi = $this->master_model->getRecords('bulk_kpi_owners', $whr_kpi);
	
		$main_total_unit = '0';
		$main_unit_return = '0';
		$main_u_disposed = '0';
		$main_u_transformed = '0';
		$main_u_corrections = '0';
		$main_the_kpi = '0';


		
		if($owner_type == 'dist_data'){
			$kpi_type = 'dist';
			$kpi_type_del = 'deleted_dist';
		}
		else{
			$kpi_type = 'mkt';
			$kpi_type_del = 'deleted_mkt';
		}

	
		//check count of kpi
		if(count($kpi) > 0) {
			
			foreach($kpi as $r=>$value){
				
				if(($value['kpi_type'] == 'all' && $value[$kpi_type_del] == 0) || ($value['kpi_type'] == $kpi_type && $value[$kpi_type_del] == 0)){
					$dbcell = unserialize($value[$owner_type]);
					if($dbcell != ''){ //get each kpi db cell data
						$main_total_unit += $dbcell['total_unit'];
						$main_unit_return += $dbcell['unit_return'];
						$main_u_disposed += $dbcell['u_disposed'];
						$main_u_transformed += $dbcell['u_transformed'];
						$main_u_corrections += $dbcell['u_corrections'];
						$main_the_kpi += $dbcell['the_kpi'];
					
					}
				
				
				}
				
				
			}
			
			
		}



		$total_unit =  str_replace(",", "",$_POST['total_unit']);
		$unit_return =  str_replace(",", "",$_POST['unit_return']);
		$u_disposed =  str_replace(",", "",$_POST['u_disposed']);
		$u_transformed =  str_replace(",", "",$_POST['u_transformed']);
		
		
		
		
		if($incident_type == '5'){
			$theincident = $this->master_model->getRecords('cf_recall', array('id'=>$incident_id));
		}
		else{
			$theincident = $this->master_model->getRecords('cf_continuity', array('id'=>$incident_id));
		}
		//cc info timezone
		$cc_timezone = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$theincident[0]['cc_id']));
		
		$nowtime = $this->common_model->usertime($cc_timezone[0]['timezone']);
//		echo $nowtime;
//		return false;

	
		$data['total_unit']= $total_unit;
		$data['unit_return'] = $unit_return;
		$data['u_disposed'] = $u_disposed;
		$data['u_transformed'] = $u_transformed;

		$main_data['total_unit']= $main_total_unit + $total_unit;
		$main_data['unit_return'] = $main_unit_return + $unit_return;
		$main_data['u_disposed'] = $main_u_disposed + $u_disposed;
		$main_data['u_transformed'] = $main_u_transformed + $u_transformed;


		//invalid total if zero
		if($total_unit == '0'){
			
			$data['type'] = 'invalid_total';
			$data['message'] = 'Total Units cannot be null.';
			echo json_encode($data);
			return false;
		}
		
		
		$u_corrections = $data['u_disposed'] + $data['u_transformed'];
		
		//check total > correction+return
		$corr_plus_return = $data['total_unit'] - ($u_corrections + $data['unit_return']);
		
		if($corr_plus_return < 0){
			
			$data['type'] = 'greater';
			$data['message'] = 'Units Returned + Corrective Actions cannot be more than Total Units.';
			echo json_encode($data);
			return false;
		}
		
		
		$data['u_corrections'] = $u_corrections;
		$the_kpi = (($unit_return + $u_corrections) / $data['total_unit']) * 100;
		$data['the_kpi'] = $the_kpi;



		$main_data['u_corrections'] = $main_data['u_disposed'] + $main_data['u_transformed'];
		$main_data['the_kpi'] = (($main_data['unit_return'] + $main_data['u_corrections']) / $main_data['total_unit']) * 100;


		$data['date_updated'] = $nowtime;
		$main_data['date_updated'] = $nowtime;
		
		
		//get threshold
		if($kpi_id !=''){
			$thresh = $this->master_model->getRecords('bulk_kpi', array('id'=>$kpi_id));
			$kpidata = unserialize($thresh[0][$owner_type]);
			$main_data['threshold'] = $kpidata['threshold'];
		}
		else{
			$main_data['threshold'] = 0;
		}
		

		
//		echo json_encode($main_data);
//		return false;
		
		//store kpi type data
		$rowdata[$owner_type] = serialize($main_data);
		$kpi_cell = $owner_type;
		//detect type of kpi
		if($owner_type == 'dist_data'){
			$data['kpi_type'] = 'Units in distribution';
		}
		else{
			$data['kpi_type'] = 'Units in consumer market';
		}
		
		if($kpi_id !=''){
			$rowdata['date_updated'] = $nowtime;
			
			$this->master_model->updateRecord( 'bulk_kpi', $rowdata, array('id'=>$kpi_id));
			$this->master_model->updateRecord( 'bulk_kpi_owners', array($kpi_cell=>serialize($data), 'kpi_id'=>$kpi_id, $kpi_type_del=>'0'), array('id'=>$owner_id));
			
			$data['type'] = 'update'; 

		}
		else{
			$rowdata['cc_id'] = $theincident[0]['cc_id'];
			$rowdata['org_id'] = $theincident[0]['org_id'];
			$rowdata['date_added'] = $nowtime;
			$rowdata['incident_id'] = $incident_id;
			$rowdata['incident_type'] = $incident_type;

			$kpi_id = $this->master_model->insertRecord('bulk_kpi', $rowdata, true);
			$this->master_model->updateRecord( 'bulk_kpi_owners', array($kpi_cell=>serialize($data), 'kpi_id'=>$kpi_id, $kpi_type_del=>'0'), array('id'=>$owner_id));
		
			$data['type'] = 'add'; 
			$data['id'] = $kpi_id; 
		
		
		}
		
		echo json_encode($data);
	}



	
	public function org_status(){
		$status = $this->uri->segment(3);
		$org_id = $this->uri->segment(4);
		$the_org = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$cc = $this->master_model->getRecords('cf_login_master', array('login_id'=>$the_org[0]['cc_id']));


		$id = $the_org[0]['cc_id'];


		/* fetch  crt members*/
		$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$the_org[0]['cc_id']));
		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$the_org[0]['cc_id']));


		$whr=array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

		$info_arr = array(
			'from'=>$adminemail[0]['recovery_email'],
			'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
			'subject'=>'Welcome to CrisisFlo',
			'view'=>'registration-mail-to-cc'
		);

	

		$other_info = array(
			'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),
			'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),
			'onetime_key'=>$crt_onetimekey[0]['onetime_key'],
			'email'=>$this->master_model->decryptIt($crt_record[0]['crt_email'])

		);


		if($this->email_sending->sendmail($info_arr,$other_info)){
			$newstat = 'Y';
			if($status == 'Y'){
				$newstat = 'N';
			}
	
			
			$arr = array(
				'enabled'=>$newstat
			);

			$this->master_model->updateRecord('organization_master', $arr, array('organization_id'=>$org_id));
			
			echo '<h2 style="text-align: center; margin-top: 150px; color: #3c763d; padding: 20px; border: 1px solid #3c763d; margin: 0 50px;">User organization successfully enabled.</h2>';
		}
		else{
			echo '<h2 style="text-align: center; margin-top: 150px;">Something went wrong.</h2>';
		}


	}	
	


	public function add_connect(){
		$id = $_POST['id'];
		$email = $_POST['email'];
		$type = $_POST['type'];
		$user_id = $_POST['user_id'];
		
		if($type == 'linkedin'){
			$arr = array(
				'linkedin_id'=>$id
			);
		}
		
		else if($type == 'gplus'){
			$arr = array(
				'gplus_id'=>$id
			);
		}
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		
		echo json_encode($arr);
	
	}
	
	public function remove_connect(){
		$type = $_POST['type'];
		$user_id = $_POST['user_id'];

		if($type == 'linkedin'){
			$arr = array(
				'linkedin_id'=>''
			);
		}
		
		else if($type == 'gplus'){
			$arr = array(
				'gplus_id'=>''
			);
		}
		
		$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$user_id));
		echo json_encode($arr);
	}

}



?>
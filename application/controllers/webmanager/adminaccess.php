<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminaccess extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		
	}

	public function index(){
		
		$admins = $this->master_model->getRecords('admin_login', array('id !='=>'1'));

		$data = array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-access',
			'admins'=>$admins
		);

		$this->load->view('admin/admin-view',$data);

		
	}
	
	public function addupdate(){
		$username = $_POST['username'];
		$userid = $_POST['userid'];
		$email = $_POST['email'];
		$privileges = $_POST['privileges'];
		
		
		$arr = array(
			'user_name'=>$username,
			'recovery_email'=>$email,
			'privileges'=>serialize($privileges)
		);
		
		
		if($userid == ''){
			
			//check duplicate
			$the_admins = $this->master_model->getRecords('admin_login', array('user_name'=>$username));
			
			if(count($the_admins) > 0){
				echo 'duplicate'; 
				return false;
			}
				
				
			$this->master_model->insertRecord('admin_login', $arr);
			$this->session->set_flashdata('success', 'Admin added successfully.');
			

			$whr = array('id'=>'1');
	
			$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');
	
				
			$info_arr = array(
				'from'=>$adminemail[0]['recovery_email'],
				'to'=>$email,
				'subject'=>'Welcome to CrisisFlo',
				'view'=>'admin-access-noti'
			);
	
		
			$other_info=array(
				'username'=>$username,
				'email'=>$email,
				'usermd5'=>md5($username)
			);
	
			$this->email_sending->sendmail($info_arr,$other_info);
			
		}
		else{
			$this->master_model->updateRecord('admin_login', $arr, array('id'=>$userid));
			$this->session->set_flashdata('success', 'Admin updated successfully.');
		}
		echo 'yeah';
	}

	public function delete_admin(){
		$id = $_POST['id'];
		
		$this->master_model->deleteRecord('admin_login', 'id', $id);
		
		echo 'yeah';
	}

}

?>
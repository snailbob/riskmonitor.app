<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contents extends CI_Controller 

{

	public function __construct(){

		parent::__construct();}


		
	
	public function legal(){
		$admin_info = $this->master_model->getRecords('admin');
	
		$data = array(
			'middle_content'=>'manage-legals',
			'title'=>'Legal',
			'admin_info'=>$admin_info,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	public function bannertext(){
		
		$thefield = 'banner_text';
		$admin_info = $this->master_model->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Banner Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function pagetext(){
		
		$thefield = 'light_section';
		$admin_info = $this->master_model->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Page Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function savetexts(){
		$contentfield = $_POST['contentfield'];
		$title = $_POST['title'];
		$titlefield = $_POST['titlefield'];
		$type = $_POST['type'];
		$id = $_POST['id'];
		$icon = (isset($_POST['icon']) || !empty($_POST['icon'])) ? $_POST['icon'] : 'https://crisisflo.com/uploads/avatars/icon-lightbulb-green.svg';
		
		$arr = array(
			'title'=>$titlefield,
			'icon'=>$icon,
			'content'=>$contentfield
		);

		
		
		if($id == ''){
			$arr['type'] = $type; 
			$id = $this->master_model->insertRecord('admin_contents', $arr, true);
			$arr['id'] = $id;
		}
		else{
			$this->master_model->updateRecord('admin_contents', $arr, array('id'=>$id));
		}
		
		
		echo json_encode($arr);

	}
	
	public function aboutus(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'aboutus';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'About Us Page',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function firstsection(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'firsthomesection';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'What is Crisisflo',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	

	public function index(){
//		echo 'sdf';
//		return false;
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'alternatingcontenthead';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-solutions',
			'title'=>'Our Solutions Head',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	
	public function manage_alternates(){
		$pagetype = 'alternatingcontents';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-alternates',
			'title'=>'Our Solutions Contents',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}

	
	public function alternate(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'alternatingcontents';
		$admin_info = $this->master_model->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'icon'=>'https://crisisflo.com/uploads/avatars/icon-lightbulb-green.svg',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Our Solutions Contents',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	public function delete_alt(){
		$user_id = $this->uri->segment(4);

		if($this->master_model->deleteRecord('admin_contents','id',$user_id)) {	
			$success_mess = 'Content successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
		}
		
		redirect('webmanager/contents/manage_alternates');
	}
	
	public function collab(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'collab';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Collaboration Features',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function manage_collabs(){
		$pagetype = 'collabs';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-collabs',
			'title'=>'Collaboration Features Items',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}


	public function collabs(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'collabs';
		$admin_info = $this->master_model->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Collaboration Features Items',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}		
	
	
	public function easiestway(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'easiestway';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Easiest Features',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function manage_easiestways(){
		$pagetype = 'easiestways';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-easiestways',
			'title'=>'Easiest Features Items',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}


	public function easiestways(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'easiestways';
		$admin_info = $this->master_model->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Easiest Features Items',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}		
	

	public function privacy(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'privacy';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Privacy Policy',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}

	public function terms(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'terms';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Terms of Use',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	

	public function threeparts(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'threepartshead';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'More Reasons Why Header',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	
	public function manage_threeparts(){
		$pagetype = 'threeparts';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-threeparts',
			'title'=>'More Reasons Why Contents',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}

	
	public function threepart(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'threeparts';
		$admin_info = $this->master_model->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'More Reasons Why Content',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}	

	public function banners(){
		$pagetype = 'banner';
		$admin_info = $this->master_model->getRecords('admin_contents', array('type'=>$pagetype),'*', '', '0', '1');
	
		$data = array(
			'middle_content'=>'manage-banners',
			'title'=>'Homepage Banner Text',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}

	
	public function banner(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'banner';
		$admin_info = $this->master_model->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'manage-aboutus', //add-alternate',
			'title'=>'Homepage Banner Text',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}	
	

	/***************************
		update_avatar
	***************************/
	public function update_avatar() {
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$output_dir = "uploads/avatars/";
		
		$pageid = (isset($_POST['pageid'])) ? $_POST['pageid'] : '';
		$img_type = (isset($_POST['img_type'])) ? $_POST['img_type'] : '';
		
		if(isset($_FILES["myfile"])) {
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0) {
			  echo 'error';
			}
			else {
				$file_name = $_FILES["myfile"]["name"];
				$file_name2 = substr($file_name, -4);
				$file_name = substr($file_name, -3);
				
				if($file_name != 'jpg' && $file_name != 'gif' && $file_name != 'png' && $file_name != 'JPG' && $file_name != 'GIF' && $file_name != 'PNG' && $file_name2 != 'jpeg' && $file_name2 != 'JPEG'){
					echo 'not_img';
					return false;
				}
				
				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
				
				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); 
				
						
				if($img_type != ''){
									
					if($pageid != ''){
						$arr = array (
							'image'=>$thefilename
						);					
						$this->master_model->updateRecord('admin_contents',$arr, array('id'=>$pageid));
					}
									
				}

				echo $output_dir.$thefilename;
					
				
			}
		
		}		
	
	}	
	
	
	/***************************
		save_profile_pic
	***************************/
	public function save_profile_pic() {
		$img_url = $_POST['img_url'];
		$img_name = $_POST['img_name'];
		$img_type = $_POST['img_type'];
		$page_type = $_POST['page_type'];
		$page_id = $_POST['page_id'];

		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		list($type, $img_url) = explode(';', $img_url);
		list(, $img_url)      = explode(',', $img_url);
		$data = base64_decode($img_url);
		
		
		if($page_type != ''){
			$img_dr = $img_name;
			
			//$this->master_model->updateRecord('admin_contents', array('image'=>$img_name), array('id'=>$page_id));
			
		}
		
		else {
			$img_dr = "assets/frontpage/corporate/images/crisisflo-logo-medium.png";
		}
		
		file_put_contents($img_dr, $data);
		
		$arr = array(
			//'a'=>$img_url,
			'image'=>$img_name //$this->common->avatar($user_id, $type)
		); 
		echo json_encode($arr);
	
	}		

}
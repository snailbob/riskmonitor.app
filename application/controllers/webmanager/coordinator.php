<?php

class Coordinator extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		if(!isset($_SESSION['logged_admin'])) {
			redirect(base_url().'webmanager');
		}

	}


	public function testCrypt()

	{
		
		
		/*$str = 'apple';
		
		
		//echo md5('Nga Nguyen');
		*/
		$input = "ZjHV1D8sDGcwWi35UGtk6ceKkc7TSY19te79lqcPZtc=";
		
		$encrypted = $this->master_model->encryptIt($input);
		$decrypted = $this->master_model->decryptIt($encrypted);
		
		echo '<h1> Encrypted: '. $encrypted .'<br>';
		echo 'Decrypted: '. $decrypted .'<br></h1>';
		
		
		
	}
	
	public function setupcc(){
		
		if(isset($_POST['add_cc'])){

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('cc_email','Email','required|xss_clean|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			//$this->form_validation->set_rules('maxu','Max User','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run()){

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$cc_email=$this->input->post('cc_email',true);

				$countrycode=$this->input->post('countrycode',true);
				
				$c_id_code = explode(" ", $countrycode);

				$crt_digits = ltrim($this->input->post('cc_phone_number',true),'0');
				
				$mobile_num = $c_id_code[1].''.$crt_digits;

				$cc_address = $this->input->post('cc_address',true);

				$cc_city = $this->input->post('cc_city',true);

				$cc_state = $this->input->post('cc_state',true);

				$cc_zip_code = $this->input->post('cc_zip_code',true);

				$cc_type = $this->input->post('cc_type',true);

				$cc_org = $this->input->post('cc_org',true);
				
				$reseller = $this->input->post('reseller',true);

				$modules = $this->input->post('modules',true);

				$cust_type = $this->input->post('cust_type',true);

				//$maxu=$this->input->post('maxu',true);

				$onetime_key = md5(microtime());

				$standard_max = $this->input->post('maxu1',true);
				$recall_max = $this->input->post('maxu5',true);
				$case_max = $this->input->post('maxu2',true);
				$member_max = $this->input->post('maxu4',true);
				$continuity_max = $this->input->post('maxu8',true);
				$simulation_modules = $this->input->post('simulation_modules',true);
				


				$themods = "";
				$count_mod = 1;
				foreach($modules as $check) {
					$themods .= 'm'.$check.'m';
					
					if(count($modules) != $count_mod){
						$themods .='-';
					}
					$count_mod++;
				}		

				$login_arr=array(
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'user_type'=>$cc_type,
					'user_level'=>'0',
					'onetime_key'=>$onetime_key,
					'user_status'=>'0'
				);



				//filter duplicate email
				$resellers=$this->master_model->getRecords('cf_login_master');
				
				foreach ($resellers as $r=>$value){
					if ($this->master_model->encryptIt($cc_email) == $value['email_id']){
						
						$this->session->set_flashdata('error','Coordinator\'s email already exist in the database.');

						redirect(base_url().'webmanager/coordinator/setupcc');
					}
				}



				if($login_id=$this->master_model->insertRecord('cf_login_master',$login_arr,TRUE))

				{
					//insert data to organization_master
					$org_arr = array(
						'organization_name'=>$cc_org,
						'active_module'=>$themods,
						'active_mod'=>serialize($modules),
						'reseller_id'=>$reseller,
						'cust_type'=>$cust_type,
						'cc_id'=>$login_id,
						'standard_max'=>$standard_max,
						'recall_max'=>$recall_max,
						'case_max'=>$case_max,
						'continuity_max'=>$continuity_max,
						'member_max'=>$member_max,
						'simulation_id'=>$simulation_modules,
						'date_added'=>date('Y-m-d H:i:s')
					);
					
					$org_id=$this->master_model->insertRecord('organization_master',$org_arr,TRUE);
					
					//add default org for cc
					$this->master_model->updateRecord('cf_login_master',array('default_org'=>$org_id),array('login_id'=>$login_id));

					//add default org for reseller
					if($reseller != ''){
						$this->master_model->updateRecord('reseller_master', array('org_id'=>$org_id),array('login_id'=>$reseller));
					}



					$cc_arr=array(
						'login_id'=>$login_id,
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_organisation'=>$cc_org,
						'crt_email'=>$this->master_model->encryptIt($cc_email),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'country_id'=>$c_id_code[0],
						'countrycode'=>$this->master_model->encryptIt($c_id_code[1]),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code),
						'cc_id'=>$login_id,
						'org_id'=>0
					);

					//create text file for organization chat room
					$room_name = 'chat_room/org';
					$room_name .= $org_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	


					if($this->master_model->insertRecord('cf_crisis_response_team',$cc_arr))

					{

						//$this->session->set_flashdata('success','CC added successfully');

						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],

						'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

						

						$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,

						'email'=>$cc_email);

						

						$this->email_sending->sendmail($info_arr,$other_info);

						

						$this->session->set_flashdata('success','CC added successfully.');

						redirect(base_url().'webmanager/coordinator/manage');

						

					}

					else

					{

						$this->session->set_flashdata('error','Error while adding CC');

						redirect(base_url().'webmanager/coordinator/manage');

					}

				}

			}

		}

		$modules = $this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
		
		$resellers = $this->master_model->getRecords('reseller_master');

		
		$countriescode = $this->master_model->getRecords('country_t');

		$data = array(
			'page_title'=>'Setup CC',
			'middle_content'=>'add-cc',
			'countriescode'=>$countriescode,
			'modules'=>$modules,
			'resellers'=>$resellers
		);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function manage(){

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$all_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0'));

		$data=array('page_title'=>'Manage CC','middle_content'=>'manage-cc','all_cc'=>$all_cc);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function update()

	{

		$cc_id=$this->uri->segment(4);

		if(isset($_POST['update_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$countrycode=$this->input->post('countrycode',true);
				
				$c_id_code = explode(" ", $countrycode);

				$crt_digits=ltrim($this->input->post('cc_phone_number',true),'0');
				
				$mobile_num= $c_id_code[1].''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				

				

				$login_arr=array('user_type'=>$cc_type);

				if($login_id=$this->master_model->updateRecord('cf_login_master',$login_arr,array('login_id'=>$cc_id)))

				{

					$cc_arr=array(
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'country_id'=>$c_id_code[0],
						'countrycode'=>$this->master_model->encryptIt($c_id_code[1]),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code)
					);

							

					if($this->master_model->updateRecord('cf_crisis_response_team',$cc_arr,array('login_id'=>$cc_id)))

					{

						$this->session->set_flashdata('success','CC updated successfully');

						redirect(base_url().'webmanager/coordinator/update/'.$cc_id);

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating CC');

						redirect(base_url().'webmanager/coordinator/update/'.$cc_id);

					}

				}

			}

		}

		

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');

		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Update CC','middle_content'=>'update-cc','cc_info'=>$cc_info,'countriescode'=>$countriescode);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function details()

	{

		$cc_id=$this->uri->segment(4);

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

//		$cc_info=$this->master_model->getRecords('cf_coordinator_master as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		

		$data=array('page_title'=>'Details CC','middle_content'=>'details-cc','cc_info'=>$cc_info);

		$this->load->view('admin/admin-view',$data);

	}

	
	//------------------------------------resend crt
	public function resend_request(){
		
		$id = $_POST['login_id'];


		/* fetch  crt members*/
		$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$id));
		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));


		$whr=array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

		$info_arr=array(
			'from'=>$adminemail[0]['recovery_email'],
		
			'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
		
			'subject'=>'Welcome to CrisisFlo',
		
			'view'=>'registration-mail-to-cc'
		);

	

		$other_info=array(
			
			'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),
			
			'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),
			
			'onetime_key'=>$crt_onetimekey[0]['onetime_key'],
			
			'email'=>$this->master_model->decryptIt($crt_record[0]['crt_email'])
			
			
		);


		if($this->email_sending->sendmail($info_arr,$other_info)){
			$this->session->set_flashdata('success',' Email request successfully re-sent.');
			echo 'success';
		}
		else{
			$this->session->set_flashdata('error',' Email request failed to re-sent.');
			echo 'error';
		}

		
	}
	

	public function access_cc(){
	
		$cc_id = $this->uri->segment(4);

		$chk_arr = array(
			'login_id'=>$cc_id	
		);

		$row = $this->master_model->getRecords('cf_crisis_response_team',$chk_arr);
		$login_master = $this->master_model->getRecords('cf_login_master',$chk_arr);

		
		if(count($row) > 0){

			$name_of_org = $this->master_model->getRecords('organization_master',array('organization_id'=>$login_master[0]['default_org']),'organization_name');

			$user_data = array(
				'logged_cc_login_id'=>$row[0]['login_id'],
				'logged_cc_email_id'=>$this->master_model->decryptIt($row[0]['crt_email']),
				'logged_parent_cc'=>$row[0]['cc_id'],
				'team_cc'=>$row[0]['cc_id'],
				'timezone'=>$row[0]['timezone'],
				'country_id'=>$row[0]['country_id'],
				'forum_last_visit'=>$row[0]['forum_last_visit'],
				'location'=>$row[0]['location'],
				'my_id'=>$row[0]['login_id'],
				'logged_user_type'=>$row[0]['user_type'],
				'logged_display_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
				'logged_user_level'=>$row[0]['user_level'],
				'cc_selected_orgnaization'=>$login_master[0]['default_org'],
				'task_review_date'=>$row[0]['tasks_review_date'],
				'cc_selected_orgnaization_name'=>$name_of_org[0]['organization_name']
							 
			 );

			

			$this->session->set_userdata($user_data);

			//redirect(base_url().'cc/dashboard/selectorganization');
			redirect(base_url().'cc/dashboard/selectmodule');

		}


	}
	
	public function delete()

	{

		$cc_id=$this->uri->segment(4);

		if($this->master_model->deleteRecord('cf_login_master','login_id',$cc_id))

		{

			if($this->master_model->deleteRecord('cf_crisis_response_team','cc_id',$cc_id))

			{

				$this->session->set_flashdata('success','CC successfully deleted.');

				redirect(base_url().'webmanager/coordinator/manage');

			}

		}

		else

		{

			$this->session->set_flashdata('success','Error Deleting CC.');

			redirect(base_url().'webmanager/coordinator/manage');

		}

	}

}

?>
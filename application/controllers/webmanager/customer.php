<?php

class Customer extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}


	public function testCrypt()

	{
		
		
		/*$str = 'apple';
		
		
		//echo md5('Nga Nguyen');
		*/
		$input = "ZjHV1D8sDGcwWi35UGtk6ceKkc7TSY19te79lqcPZtc=";
		
		$encrypted = $this->master_model->encryptIt($input);
		$decrypted = $this->master_model->decryptIt($encrypted);
		
		echo '<h1> Encrypted: '. $encrypted .'<br>';
		echo 'Decrypted: '. $decrypted .'<br></h1>';
		
		
		
	}
	
	
	
	
	
	
	
	public function request()

	{

		$cust = $this->uri->segment(4);

		$cust_info=$this->master_model->getRecords('subscribe_request',array('id'=>$cust));
		
		$whr=array('id'=>'1');

		$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');

		

		$info_arr=array(
					
					'from'=>$adminemail[0]['recovery_email'],

					'to'=>$cust_info[0]['email'],

					'subject'=>'CrisisFlo Form Subscription',

					'view'=>'customer-request'
					
		);

	

		 $other_info=array(

					  'first_name'=>$cust_info[0]['name'],
					  
					  'cust_id'=>$cust_info[0]['id'],
					  
					  'email'=> md5($cust_info[0]['email'])
		);


		$this->email_sending->sendmail($info_arr,$other_info);



		$this->master_model->updateRecord('subscribe_request',array('granted'=>'1'),array('id'=>$cust));
		
		
		

		$this->session->set_flashdata('success',' Order Form successfully sent.');

		redirect(base_url().'webmanager/customer/manage');

	}
	
	
	
	
	
	
	public function setupcc()

	{
		
		if(isset($_POST['add_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('cc_email','Email','required|xss_clean|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$cc_email=$this->input->post('cc_email',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				$cc_org=$this->input->post('cc_org',true);

				$modules=$this->input->post('modules',true);

				$cust_type=$this->input->post('cust_type',true);

				$onetime_key=md5(microtime());



				$themods = "";
				foreach($modules as $check) {
					$themods .= $check; 
				}		

				$login_arr=array(
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'user_type'=>$cc_type,
					'user_level'=>'0',
					'onetime_key'=>$onetime_key,
					'user_status'=>'0'
				);



				//filter duplicate email
				$resellers=$this->master_model->getRecords('cf_login_master');
				
				foreach ($resellers as $r=>$value){
					if ($this->master_model->encryptIt($cc_email) == $value['email_id']){
						
						$this->session->set_flashdata('error','Coordinator\'s email already exist in the database.');

						redirect(base_url().'webmanager/coordinator/setupcc');
					}
				}



				if($login_id=$this->master_model->insertRecord('cf_login_master',$login_arr,TRUE))

				{
					//insert data to organization_master
					$org_arr=array(
						'organization_name'=>$cc_org,
						'active_module'=>$themods,
						'cust_type'=>$cust_type,
						'cc_id'=>$login_id
					);
					
					$org_id=$this->master_model->insertRecord('organization_master',$org_arr,TRUE);



					$cc_arr=array(
						'login_id'=>$login_id,
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_organisation'=>$cc_org,
						'crt_email'=>$this->master_model->encryptIt($cc_email),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code),
						'cc_id'=>$login_id,
						'org_id'=>0
					);

					//create text file for organization chat room
					$room_name = 'chat_room/org';
					$room_name .= $org_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	


					if($this->master_model->insertRecord('cf_crisis_response_team',$cc_arr))

					{

						//$this->session->set_flashdata('success','CC added successfully');

						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],

						'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

						

						$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,

						'email'=>$cc_email);

						

						$this->email_sending->sendmail($info_arr,$other_info);

						

						$this->session->set_flashdata('success','CC added successfully.');

						redirect(base_url().'webmanager/coordinator/manage');

						

					}

					else

					{

						$this->session->set_flashdata('error','Error while adding CC');

						redirect(base_url().'webmanager/coordinator/manage');

					}

				}

			}

		}


		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Setup CC','middle_content'=>'add-cc','countriescode'=>$countriescode);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function manage()

	{


		$all_cc=$this->master_model->getRecords('subscribe_request',array('stripe'=>'0'));
		
		
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		$cust_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.cust_id !='=>'0','cc.subscription !='=>'0'));

		$data=array('page_title'=>'Manage CC','middle_content'=>'manage-customer','all_cc'=>$all_cc,'cust_cc'=>$cust_cc);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function update()

	{

		$cc_id=$this->uri->segment(4);

		if(isset($_POST['add_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				

				

				$login_arr=array('user_type'=>$cc_type);

				if($login_id=$this->master_model->updateRecord('cf_login_master',$login_arr,array('login_id'=>$cc_id)))

				{

					$cc_arr=array(
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code)
					);

							

					if($this->master_model->updateRecord('cf_crisis_response_team',$cc_arr,array('login_id'=>$cc_id)))

					{

						$this->session->set_flashdata('success','CC updated successfully');

						redirect(base_url().'webmanager/coordinator/update/'.$cc_id);

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating CC');

						redirect(base_url().'webmanager/coordinator/update/'.$cc_id);

					}

				}

			}

		}

		

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');

		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Update CC','middle_content'=>'update-cc','cc_info'=>$cc_info,'countriescode'=>$countriescode);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function activate()

	{

		$cc_id=$this->uri->segment(4);

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

//		$cc_info=$this->master_model->getRecords('cf_coordinator_master as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		
		$cc_arr=array(
			'date_activated'=>date("Y-m-d H:i:s"),
			'activated'=>'yes'
		);
		
		$cc_email =  $this->master_model->decryptIt($cc_info[0]['crt_email']);

		$cc_firstname =  $this->master_model->decryptIt($cc_info[0]['crt_first_name']);

		$cc_lastname =  $this->master_model->decryptIt($cc_info[0]['crt_last_name']);

		$onetime_key = md5(microtime());



		if($this->master_model->updateRecord('cf_crisis_response_team',$cc_arr,array('login_id'=>$cc_id)))

		{
			
				$login_arr=array(
					'onetime_key'=>$onetime_key,
					'pass_word'=>''
				);

				 $this->master_model->updateRecord('cf_login_master',$login_arr,array('login_id'=>$cc_id));
			
			//$this->session->set_flashdata('success','CC added successfully');

			$admin_email = $this->master_model->getRecords('email_id_master');
			
			//send email to cc
			$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],
			'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

			$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,
			'email'=>$cc_email);
						
			$this->email_sending->sendmail($info_arr,$other_info);
			
			$this->session->set_flashdata('success',' CC successfully activated.');

			redirect(base_url().'webmanager/customer/manage');

		}
		else{
		

			$this->session->set_flashdata('error','Error while activating CC');

			redirect(base_url().'webmanager/customer/manage');
		}

	}


	public function details()

	{

		$cc_id=$this->uri->segment(4);

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

//		$cc_info=$this->master_model->getRecords('cf_coordinator_master as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		

		$data=array('page_title'=>'Details CC','middle_content'=>'details-cc','cc_info'=>$cc_info);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function delete()

	{

		$cc_id=$this->uri->segment(4);

		if($this->master_model->deleteRecord('cf_login_master','login_id',$cc_id))

		{

			if($this->master_model->deleteRecord('cf_crisis_response_team','cc_id',$cc_id))

			{

				$this->session->set_flashdata('success','CC successfully deleted.');

				redirect(base_url().'webmanager/coordinator/manage');

			}

		}

		else

		{

			$this->session->set_flashdata('success','Error Deleting CC.');

			redirect(base_url().'webmanager/coordinator/manage');

		}

	}

}

?>
<?php

class Customers extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		if(!isset($_SESSION['logged_admin'])) {
			redirect(base_url().'webmanager');
		}

	}
	

	public function get_premod(){
		$mods = $this->common_model->preselected_modules();
		
		echo $mods['active_module'];
	}


	public function testCrypt(){
		
		
		/*$str = 'apple';
		
		
		//echo md5('Nga Nguyen');
		*/
		$input = "ZjHV1D8sDGcwWi35UGtk6ceKkc7TSY19te79lqcPZtc=";
		
		$encrypted = $this->master_model->encryptIt($input);
		$decrypted = $this->master_model->decryptIt($encrypted);
		
		echo '<h1> Encrypted: '. $encrypted .'<br>';
		echo 'Decrypted: '. $decrypted .'<br></h1>';
		
		
		
	}
	
	public function setupcc(){
		
		if(isset($_POST['add_cc'])){

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('cc_email','Email','required|xss_clean|valid_email|is_unique[cf_login_master.email_id]');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			//$this->form_validation->set_rules('maxu','Max User','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run()){

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$cc_email=$this->input->post('cc_email',true);

				$countrycode=$this->input->post('countrycode',true);
				
				$c_id_code = explode(" ", $countrycode);

				$crt_digits = ltrim($this->input->post('cc_phone_number',true),'0');
				
				$mobile_num = $c_id_code[1].''.$crt_digits;

				$cc_address = $this->input->post('cc_address',true);

				$cc_city = $this->input->post('cc_city',true);

				$cc_state = $this->input->post('cc_state',true);

				$cc_zip_code = $this->input->post('cc_zip_code',true);

				$cc_type = $this->input->post('cc_type',true);

				$utype = $this->input->post('utype',true);
				
				$single_price = $this->input->post('single_price',true);

				$cc_org = $this->input->post('cc_org',true);
				
				$reseller = $this->input->post('reseller',true);

				$modules = $this->input->post('modules',true);

				$cust_type = $this->input->post('cust_type',true);

				$packs = $this->input->post('input_packs',true);

				$old_packs = $this->input->post('old_packs',true);

				$onetime_key = md5(microtime());

				$standard_max = $this->input->post('maxu1',true);
				$recall_max = $this->input->post('maxu5',true);
				$case_max = $this->input->post('maxu2',true);
				$member_max = $this->input->post('maxu4',true);
				$continuity_max = $this->input->post('maxu8',true);
				$simulation_modules = $this->input->post('simulation_modules',true);
				


				$themods = "";
				$count_mod = 1;
				foreach($modules as $check) {
					$themods .= 'm'.$check.'m';
					
					if(count($modules) != $count_mod){
						$themods .='-';
					}
					$count_mod++;
				}		

				$login_arr=array(
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'user_type'=>$cc_type,
					'user_level'=>'0',
					'onetime_key'=>$onetime_key,
					'single_user'=>$utype,
					'single_price'=>$single_price,
					'user_status'=>'0'
				);



				//filter duplicate email
				$resellers=$this->master_model->getRecords('cf_login_master');
				
				foreach ($resellers as $r=>$value){
					if ($this->master_model->encryptIt($cc_email) == $value['email_id']){
						
						$this->session->set_flashdata('error','Coordinator\'s email already exist in the database.');

						redirect(base_url().'webmanager/customers/setupcc');
					}
				}



				if($login_id=$this->master_model->insertRecord('cf_login_master',$login_arr,TRUE))

				{
					//insert data to organization_master
					$org_arr = array(
						'organization_name'=>$cc_org,
						'active_module'=>$themods,
						'active_mod'=>serialize($modules),
						'reseller_id'=>$reseller,
						'input_pack_id'=>$packs,
						'cust_type'=>$cust_type,
						'cc_id'=>$login_id,
						'standard_max'=>$standard_max,
						'recall_max'=>$recall_max,
						'case_max'=>$case_max,
						'continuity_max'=>$continuity_max,
						'member_max'=>$member_max,
						'simulation_id'=>$simulation_modules,
						'date_added'=>date('Y-m-d H:i:s')
					);
					
					$org_id=$this->master_model->insertRecord('organization_master',$org_arr,TRUE);
					
					//add default org for cc
					$this->master_model->updateRecord('cf_login_master',array('default_org'=>$org_id),array('login_id'=>$login_id));

					//add default org for reseller
					if($reseller != ''){
						$this->master_model->updateRecord('reseller_master', array('org_id'=>$org_id),array('login_id'=>$reseller));
					}



					$cc_arr=array(
						'login_id'=>$login_id,
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_organisation'=>$cc_org,
						'crt_email'=>$this->master_model->encryptIt($cc_email),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'country_id'=>$c_id_code[0],
						'countrycode'=>$this->master_model->encryptIt($c_id_code[1]),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code),
						'cc_id'=>$login_id,
						'org_id'=>0
					);

					//create text file for organization chat room
					$room_name = 'chat_room/org';
					$room_name .= $org_id;
					$room_name .= '.txt';
					$myfile = fopen($room_name, "w");	


					if($this->master_model->insertRecord('cf_crisis_response_team',$cc_arr))

					{

						//$this->session->set_flashdata('success','CC added successfully');

						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],

						'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

						

						$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,

						'email'=>$cc_email);

						

						$this->email_sending->sendmail($info_arr,$other_info);

						

						$this->session->set_flashdata('success','CC added successfully.');

						redirect(base_url().'webmanager/customers');

						

					}

					else

					{

						$this->session->set_flashdata('error','Error while adding CC');

						redirect(base_url().'webmanager/customers');

					}

				}

			}

		}
		$admin_info = $this->master_model->getRecords('admin_login', array('user_name'=>'admin'));

		$modules = $this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
		
		$resellers = $this->master_model->getRecords('reseller_master');

		$input_packs = $this->master_model->getRecords('input_packs');
		$countriescode = $this->master_model->getRecords('country_t');

		$data = array(
			'page_title'=>'Setup CC',
			'middle_content'=>'add-customers',
			'countriescode'=>$countriescode,
			'modules'=>$modules,
			'resellers'=>$resellers,
			'admin_info'=>$admin_info,
			'input_packs'=>$input_packs
		);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function index(){

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		
		$this->db->join('organization_master as org','cc.login_id=org.cc_id');

		$all_cc = $this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0', 'login.user_type'=>'live'));



		$data = array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-customers',
			'all_cc'=>$all_cc
		);

		$this->load->view('admin/admin-view',$data);

	}

	public function check_dulication($org_name,$org_id) {

		$whr = array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num = $this->master_model->getRecordCount('organization_master',$whr);

		if($num == 0) {
			return true;
		}

		else {
			return false;
		}

	}

	public function update(){

		$cc_id = $this->uri->segment(4);

		$the_org = $this->master_model->getRecords('organization_master', array('cc_id' =>$cc_id));

		$org_id = $the_org[0]['organization_id'];

		if(isset($_POST['update_cc'])) {

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');
			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');
//			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');
//			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');
//			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');
//			$this->form_validation->set_rules('cc_city','City','required|xss_clean');
//			$this->form_validation->set_rules('cc_state','State','required|xss_clean');
//			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');
			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');


			//org validate
			$this->form_validation->set_rules('organization_name','Organization','required|xss_clean');
			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');
			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');
			//$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run()){

				$cc_firstname=$this->input->post('cc_firstname',true);
				$cc_lastname=$this->input->post('cc_lastname',true);
				$countrycode=$this->input->post('countrycode',true);
				$c_id_code = explode(" ", $countrycode);
				$crt_digits=ltrim($this->input->post('cc_phone_number',true),'0');
				$mobile_num= $c_id_code[1].''.$crt_digits;
				$cc_address=$this->input->post('cc_address',true);
				$cc_city=$this->input->post('cc_city',true);
				$cc_state=$this->input->post('cc_state',true);
				$cc_zip_code=$this->input->post('cc_zip_code',true);
				$cc_type = $this->input->post('cc_type',true);

				$utype = $this->input->post('utype',true);
				
				$single_price = $this->input->post('single_price',true);

				

				//org vars
				$organization_name = $this->input->post('organization_name',true);	
				$reseller = $this->input->post('reseller',true);
				$modules=$this->input->post('modules',true);
				$cust_type=$this->input->post('cust_type',true);
				$packs = $this->input->post('input_packs',true);
				$old_packs = $this->input->post('old_packs',true);
				$standard_max = $this->input->post('maxu1',true);
				$recall_max = $this->input->post('maxu5',true);
				$case_max = $this->input->post('maxu2',true);
				$member_max = $this->input->post('maxu4',true);
				$continuity_max = $this->input->post('maxu8',true);
				$simulation_modules = $this->input->post('simulation_modules',true);


				$themods = "";
				$count_mod = 1;
				foreach($modules as $check) {
					$themods .= 'm'.$check.'m';
					
					if(count($modules) != $count_mod){
						$themods .='-';
					}
					$count_mod++;
				}		


				$login_arr = array(
					'single_user'=>$utype,
					'single_price'=>$single_price,
					'user_type'=>$cc_type
				);

				
				//check org duplicate name

				if($this->check_dulication($organization_name,$org_id)) {

					$updt_arr=array(
						'organization_name'=>$organization_name,
	//					'cc_id'=>$sel_cc,
						'input_pack_id'=>$packs,
						'cust_type'=>$cust_type,
						'active_module'=>$themods,
						'active_mod'=>serialize($modules),
						'reseller_id'=>$reseller,
						'standard_max'=>$standard_max,
						'recall_max'=>$recall_max,
						'case_max'=>$case_max,
						'continuity_max'=>$continuity_max,
						'member_max'=>$member_max,
						'simulation_id'=>$simulation_modules
					);
					
					
					
					//update org of reseller first
					$org_of_reseller = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
					
					if($org_of_reseller[0]['reseller_id'] != $reseller){
						//remove previous resellers org
						//$this->master_model->updateRecord('reseller_master', array('org_id'=>'0'),array('login_id'=>$org_of_reseller[0]['reseller_id']));

					}
					
					if($this->master_model->updateRecord('organization_master',$updt_arr,array('organization_id'=>$org_id))){
						
								
						//update login master
						if($login_id = $this->master_model->updateRecord('cf_login_master',$login_arr,array('login_id'=>$cc_id))) {
		
							//update cf_crisis_response_team
							$cc_arr = array(
								'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
								'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
								'crt_digits'=>$this->master_model->encryptIt($crt_digits),
								'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
								'country_id'=>$c_id_code[0],
								'countrycode'=>$this->master_model->encryptIt($c_id_code[1]),
								'crt_address'=>$this->master_model->encryptIt($cc_address),
								'crt_city'=>$this->master_model->encryptIt($cc_city),
								'crt_state'=>$this->master_model->encryptIt($cc_state),
								'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code),
								'crt_organisation'=>$organization_name
							);
		
							$this->master_model->updateRecord('cf_crisis_response_team', $cc_arr, array('login_id'=>$cc_id));
						}//end login master					

						//update new reseller with org
						if($reseller != ''){
							//$this->master_model->updateRecord('reseller_master', array('org_id'=>$org_id),array('login_id'=>$reseller));
						}
						
						$updt_old_pack = array(
							'org_id'=>'1',
							'cc_id'=>'0'
						);

						//update org_id and cc_id to each inputs
						if ($packs != '0'){
							
							$updt_pack = array(
								'org_id'=>$org_id,
								'cc_id'=>$sel_cc
							);
							
							
							if ($old_packs != '0'){
								//reset old selected pack
								$this->master_model->updateRecord('cf_file_upload',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_task',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_scenario',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_standby_messages',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
							}
						
							
							//assign new pack
							$this->master_model->updateRecord('cf_file_upload',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_task',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_pack,array('input_pack_id'=>$packs));
						}
						
						//update packs with no assignment
						if ($old_packs != '0' && $packs == '0'){
							//reset old selected pack
							$this->master_model->updateRecord('cf_file_upload',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_task',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
						}
					
					
						//update org id in input_packs table
						$this->master_model->updateRecord('input_packs',array('org_id'=>'0'),array('org_id'=>$org_id));
						$this->master_model->updateRecord('input_packs',array('org_id'=>$org_id),array('id'=>$packs));
						$this->master_model->updateRecord('organization_master',array('input_pack_id'=>'0'),array('input_pack_id'=>$old_packs,'organization_id !='=>$org_id));
	
							
							

						$this->session->set_flashdata('success','Customer updated successfully');

						redirect(base_url().'webmanager/customers/update/'.$cc_id);

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating Organization');

						redirect(base_url().'webmanager/customers/update/'.$cc_id);

					}

				}

				else

				{

					$this->session->set_flashdata('error','Organization already exists');

					redirect(base_url().'webmanager/customers/update/'.$cc_id);

				}
				

			}//if validated

		}//if form isset

		

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_info = $this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.*');

		$org_info = $this->master_model->getRecords('organization_master',array('cc_id'=>$cc_id));

		$countriescode = $this->master_model->getRecords('country_t');

		$modules = $this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
		
		$resellers = $this->master_model->getRecords('reseller_master');

		$input_packs = $this->master_model->getRecords('input_packs');

		$data = array(
			'page_title'=>'Update CC',
			'middle_content'=>'update-customers',
			'cc_info'=>$cc_info,
			'countriescode'=>$countriescode,
			'modules'=>$modules,
			'resellers'=>$resellers,
			'input_packs'=>$input_packs,
			'org_info'=>$org_info
		);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function details(){

		$cc_id=$this->uri->segment(4);

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');


		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');
		

		$data=array('page_title'=>'Details CC','middle_content'=>'details-cc','cc_info'=>$cc_info);

		$this->load->view('admin/admin-view',$data);

	}

	
	//------------------------------------resend crt
	public function resend_request(){
		
		$id = $_POST['login_id'];


		/* fetch  crt members*/
		$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$id));
		$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));


		$whr=array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

		$info_arr=array(
			'from'=>$adminemail[0]['recovery_email'],
		
			'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),
		
			'subject'=>'Welcome to CrisisFlo',
		
			'view'=>'registration-mail-to-cc'
		);

	

		$other_info = array(
			
			'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),
			
			'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),
			
			'onetime_key'=>$crt_onetimekey[0]['onetime_key'],
			
			'email'=>$this->master_model->decryptIt($crt_record[0]['crt_email'])

		);


		if($this->email_sending->sendmail($info_arr,$other_info)){
//			$this->session->set_flashdata('success',' Email request successfully re-sent.');
			echo 'success';
		}
		else{
//			$this->session->set_flashdata('error',' Email request failed to re-sent.');
			echo 'error';
		}

		
	}
	

	public function access_cc(){
	
		$cc_id = $this->uri->segment(4);

		$chk_arr = array(
			'login_id'=>$cc_id	
		);

		$row = $this->master_model->getRecords('cf_crisis_response_team',$chk_arr);
		$login_master = $this->master_model->getRecords('cf_login_master',$chk_arr);

		
		if(count($row) > 0){

			$name_of_org = $this->master_model->getRecords('organization_master',array('organization_id'=>$login_master[0]['default_org']),'organization_name');

			$user_data = array(
				'logged_cc_login_id'=>$row[0]['login_id'],
				'logged_cc_email_id'=>$this->master_model->decryptIt($row[0]['crt_email']),
				'logged_parent_cc'=>$row[0]['cc_id'],
				'team_cc'=>$row[0]['cc_id'],
				'timezone'=>$row[0]['timezone'],
				'country_id'=>$row[0]['country_id'],
				'forum_last_visit'=>$row[0]['forum_last_visit'],
				'location'=>$row[0]['location'],
				'location_lat'=>$row[0]['location_lat'],
				'location_lng'=>$row[0]['location_lng'],
				'country_id'=>$row[0]['country_id'],
				'countrycode'=>$this->master_model->decryptIt($row[0]['countrycode']),
				'crt_digits'=>$this->master_model->decryptIt($row[0]['crt_digits']),
				'my_id'=>$row[0]['login_id'],
				'logged_user_type'=>$login_master[0]['user_type'],
				'logged_display_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
				'first_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']),
				'last_name'=>$this->master_model->decryptIt($row[0]['crt_last_name']),
				'logged_user_level'=>$login_master[0]['user_level'],
				'cc_selected_orgnaization'=>$login_master[0]['default_org'],
				'task_review_date'=>$row[0]['tasks_review_date'],
				'cc_selected_orgnaization_name'=>$name_of_org[0]['organization_name']
							 
			 );
			 
			 
//			header('Content-Type: application/json');
//			header('Access-Control-Allow-Origin: *');
//			echo json_encode($user_data, JSON_PRETTY_PRINT);
//			return false;
			
			
			$this->session->set_userdata($user_data);

			//redirect(base_url().'cc/dashboard/selectorganization');
			redirect(base_url().'cc/dashboard/selectmodule');

		}


	}
	
	public function delete(){

		$cc_id = $this->uri->segment(4);

		if($this->master_model->deleteRecord('cf_login_master', 'login_id', $cc_id)){
			//delete org
			$this->master_model->deleteRecord('organization_master', 'cc_id', $cc_id);
			//delete crts
			$crt_id = $this->master_model->getRecords('cf_crisis_response_team', array('cc_id'=>$cc_id));
			
			if(count($crt_id) > 0){
				foreach($crt_id as $r=>$value){
					$this->master_model->deleteRecord('cf_login_master', 'login_id', $value['login_id']);
				}
			}
			
			//delete stks
			$stk_id = $this->master_model->getRecords('cf_stakeholder', array('cc_id'=>$cc_id));
			
			if(count($stk_id) > 0){
				foreach($stk_id as $r=>$value){
					$this->master_model->deleteRecord('cf_login_master', 'login_id', $value['login_id']);
				}
			}
			//delete another data from stks
			$this->master_model->deleteRecord('cf_stakeholder', 'cc_id', $cc_id);
			

			//delete another data from crt
			if($this->master_model->deleteRecord('cf_crisis_response_team', 'cc_id', $cc_id)){
				//delete another data from cc
				$this->master_model->deleteRecord('cf_crisis_response_team', 'login_id', $cc_id);
				$this->session->set_flashdata('success','Customer successfully deleted.');
				redirect(base_url().'webmanager/customers');

			}

		}

		else{

			$this->session->set_flashdata('success','Error Deleting Customer.');

			redirect(base_url().'webmanager/customers');

		}

	}
	
	
	
	
	
	public function org_details(){
		$org_id = $this->uri->segment(4); //$_POST['org_id'];
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$org_stripe = $this->master_model->getRecords('stripe_customers', array('description'=>$org_info[0]['organization_name']), 'email, description, pre_selected_plan, stripe_id, created, cust_data');
		
		$cc = $this->master_model->getRecords('cf_login_master', array('login_id'=>$org_info[0]['cc_id']));

		$whr = array(
			'org_id'=>$org_id,
			'type !='=>'2' //not task pickup
		);
		
		$logs = $this->master_model->getRecords('audit_log', $whr, '*', array('date_added'=>'DESC'));
	
		$the_logs = array();
		if(count($logs) > 0){
			foreach($logs as $r=>$value){
				if($value['type'] != '3'){
					$value['date_format'] = date_format(date_create($value['date_added']), 'jS F Y, g:ia');
					$value['module_name'] = $this->common_model->getmodulename($value['module_id']);
					$value['user_name'] = $this->common_model->getcrtname($value['crt_id']);
					$value['action'] = 'logged into';
					if($value['type'] != '0' && $value['type'] != '4'){
						$value['action'] = 'logged out from';
					}
					$the_logs[] = $value;
				}//filter task complete
			}
		}
		
		
		
		$from_stripe = array();
		$stripe_data = array();
		$org_data = array();
		if(count($org_info) > 0){
			foreach($org_info as $r=>$value){
				$value['date_added'] = date_format(date_create($value['date_added']), 'M d Y');
				$value['stakeholders'] = unserialize($value['stakeholders']);
				$value['cc_name'] = $this->common_model->getname($value['cc_id']);
				$stakeholder = array();
				if(is_array($value['stakeholders'])){
					
					if(count($value['stakeholders']) > 0){
						foreach($value['stakeholders'] as $stake){
							
							$stakeholder[] = $this->common_model->getstkname($stake);
						}
					}
				}
				$value['stakeholders'] = $stakeholder;
				$value['module_crts'] = array();
				
				$active_module = $value['active_module'];
				$the_mods = array();
				if (strpos($active_module, 'm1m') !== false){
					$the_mods[] = 'Crisis Manager';
					$case_man = array('name'=>'Crisis Manager','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '1'));
					$value['module_crts'][] = $case_man;
				}
				if (strpos($active_module, 'm5m') !== false){
					$the_mods[] = 'Recall Manager';
					$case_man = array('name'=>'Recall Manager','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '5'));
					$value['module_crts'][] = $case_man;
				}
				if (strpos($active_module, 'm2m') !== false){
					$the_mods[] = 'Case Management';
					$case_man = array('name'=>'Case Management','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '2'));
					$value['module_crts'][] = $case_man;
				}
				if (strpos($active_module, 'm4m') !== false){
					$the_mods[] = 'Member Safety';
					$case_man = array('name'=>'Member Safety','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '4'));
					$value['module_crts'][] = $case_man;
					
				}
				if (strpos($active_module, 'm8m') !== false){
					$the_mods[] = 'Continuity Manager';
					$case_man = array('name'=>'Continuity Manager','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '8'));
					$value['module_crts'][] = $case_man;
					
				}
				if (strpos($active_module, 'm6m') !== false){
					$the_mods[] = 'Cost Monitor [Feature]';
				}
				if (strpos($active_module, 'm3m') !== false){
					$the_mods[] = 'Video Conferencing [Feature]';
				}
				if (strpos($active_module, 'm7m') !== false){
					$the_mods[] = 'SMS Notification [Feature]';
				}
				if (strpos($active_module, 'm9m') !== false){
					$the_mods[] = 'Bulk Notification [Feature]';
				}
				if (strpos($active_module, 'm10m') !== false){
					$the_mods[] = 'Recall KPIs [Feature]';
				}
				if (strpos($active_module, 'm11m') !== false){
					$the_mods[] = 'Simulation[Feature]';
				}
				if (strpos($active_module, 'm12m') !== false){
					$the_mods[] = 'Availability[Feature]';
				}
				
				$value['modules'] = $the_mods;
				
				$org_data[] = $value;
			}
				
		}
		
		$from_stripe['the_subsription_data'] = array();
		
		if($cc[0]['stripe_customer_id'] != '' && $cc[0]['stripe_customer_id'] != '0'){ //count($org_stripe) > 0

			$cu = Stripe_Customer::retrieve($cc[0]['stripe_customer_id']); //$org_stripe[0]['stripe_id']);

			$data['stripe_id'] = $cc[0]['stripe_customer_id']; //$org_stripe[0]['stripe_id'];
			//$org_stripe = $org_stripe[0];
			$from_stripe['sub_total_count'] = $cu->subscriptions->total_count;
			
			$the_subsription_data = array();
			
			if($from_stripe['sub_total_count'] > 0){
				for($i = 0; $i < $from_stripe['sub_total_count']; $i++){
					$plan_data = $cu->subscriptions->data[$i];
					
					$current_period_start = $plan_data->current_period_start;
					$date = new DateTime("@$current_period_start");
					$current_period_start = $date->format('M d Y');
					
					$current_period_end = $plan_data->current_period_end;
					$date = new DateTime("@$current_period_end");
					$current_period_end = $date->format('M d Y');
					
					
					$the_subsription_data[] = array(
						
						"id"=>$plan_data->id,
						"plan"=> array(
						  "interval"=>$plan_data->plan->interval,
						  "name"=>$plan_data->plan->name,
						  "created"=>$plan_data->plan->created,
						  "amount"=>$plan_data->plan->amount,
						  "currency"=>$plan_data->plan->currency,
						  "id"=>$plan_data->plan->id,
						  "object"=>$plan_data->plan->object,
						  "livemode"=>$plan_data->plan->livemode,
						  "interval_count"=>$plan_data->plan->interval_count,
						  "trial_period_days"=>$plan_data->plan->trial_period_days,
						  "statement_descriptor"=>$plan_data->plan->statement_descriptor
						),
						"object"=>$plan_data->object,
						"start"=>$plan_data->start,
						"status"=>$plan_data->status,
						"customer"=>$plan_data->customer,
						"cancel_at_period_end"=>$plan_data->cancel_at_period_end,
						"current_period_start"=>$current_period_start,
						"current_period_end"=>$current_period_end,
						"ended_at"=>$plan_data->ended_at,
						"trial_start"=>$plan_data->trial_start,
						"trial_end"=>$plan_data->trial_end,
						"canceled_at"=>$plan_data->canceled_at,
						"quantity"=>$plan_data->quantity,
						"application_fee_percent"=>$plan_data->application_fee_percent,
						"discount"=>$plan_data->discount,
						"tax_percent"=>$plan_data->tax_percent,

					);
				}
			}
			
			$from_stripe['the_subsription_data'] = $the_subsription_data;
			
			
			
			foreach($org_stripe as $r=>$value){
				$cust_data = serialize($value['cust_data']);
				//$value['customer_data'] = $cust_data;
				unset($value['cust_data']);
				$ts = $value['created'];
				$date = new DateTime("@$ts");
				$the_date = $date->format('M d Y'); //'Y-m-d H:i:s');	
							
				$value['created'] = $the_date;
				$thekey = key($org_stripe);
				$stripe_data[]= $value; 
				break;
			}
		}
		
		
		$data['org_id'] = $org_id;
		$data['org_data'] = $org_data;
		$data['the_logs'] = $the_logs;
		
		$data['from_stripe'] = $from_stripe;
		$data['org_stripe'] = $stripe_data;
		$data['cc'] = $cc;
		$data['purchaser_data'] = $this->master_model->getRecords('organization_signups', array('org_id'=>$org_id));
		//echo json_encode($data);
			
		$data['middle_content'] = 'view-organization-details';
		$data['page_title'] = 'Organization';
	
		$this->load->view('admin/admin-view',$data);

	
	}
		
	
	public function org_status(){
		$status = $this->uri->segment(4);
		$org_id = $this->uri->segment(5);
		$the_org = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$cc = $this->master_model->getRecords('cf_login_master', array('login_id'=>$the_org[0]['cc_id']));

//		if(count($cc) > 0){
//			if($status == 'N'){
//				if($cc[0]['trial_start'] == '0000-00-00 00:00:00' && $cc[0]['stripe_customer_id'] == '0'){
//					
//					$start = date('Y-m-d H:i:s');
//					$end = date('Y-m-d H:i:s', strtotime("+14 days"));
//					
//					$arr = array(
//						'trial_start'=>$start,
//						'trial_end'=>$end
//					);
//					
//					$this->master_model->updateRecord('cf_login_master', $arr, array('login_id'=>$the_org[0]['cc_id']));
//					
//				}
//			}
//		}

		$newstat = 'Y';
		if($status == 'Y'){
			$newstat = 'N';
		}

		
		$arr = array(
			'enabled'=>$newstat
		);

		$this->session->set_flashdata('success', ' Organization status successfully updated.');
		
		$this->master_model->updateRecord('organization_master', $arr, array('organization_id'=>$org_id));
		
		redirect('webmanager/customers');
	}	
	

}

?>
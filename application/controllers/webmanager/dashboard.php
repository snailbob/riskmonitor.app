<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Dashboard extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('email_sending');

	}

	


	/*==============================
		Admin index
	===============================*/

	public function index(){
		//awaiting cc list
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		$all_cc=$this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0','login.user_status'=>'0'));

		//awaiting cc list
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');
		$reseller_req = $this->master_model->getRecords('cf_crisis_response_team as cc',array('login.user_level'=>'0','cc.activated'=>'no','cc.reseller_id !='=>'0'));

		//threat found docs
		$docs=$this->master_model->getRecords('cf_file_upload',array('scan_result !='=>'Clean'));

		//org with no credit
		$order_arr=array('org.organization_name'=>'ASC');

		$sel_str="cc.crt_first_name,cc.crt_last_name,org.*";

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=org.cc_id');

		$org_list=$this->master_model->getRecords('organization_master as org',array('org.sms_credit'=>'0'),$sel_str,$order_arr);


		$data=array('page_title'=>'Dashboard','middle_content'=>'dashboard','all_cc'=>$all_cc,'org_list'=>$org_list,'docs'=>$docs,'reseller_req'=>$reseller_req);

		$this->load->view('admin/admin-view',$data);

	}

	


	/*==============================
		Admin login
	===============================*/

	public function login(){

		$error="";

		if(isset($_SESSION['logged_admin'])){

			redirect(base_url().'webmanager/dashboard');

		}

		else{

			if(isset($_POST['btn_admin_login'])){

				$this->form_validation->set_rules('user_name','Username','required|xss_clean');
				$this->form_validation->set_rules('pass_word','Password','required|xss_clean');
				$this->form_validation->set_message('required', 'Mandatory field');

				if($this->form_validation->run()){

					$chk_arr = array(
						'user_name'=>$this->input->post('user_name',true),
						'pass_word'=>$this->input->post('pass_word',true)
					);

					if($row = $this->master_model->getRecords('admin_login',$chk_arr)){
						$_SESSION['logged_admin'] = $row[0]['user_name'];
						$_SESSION['admin_id'] = $row[0]['id'];
						$_SESSION['admin_email'] = $row[0]['recovery_email'];
						
						//set privileges in session
						$privil = unserialize($row[0]['privileges']);
						if(!is_array($privil)){
							$privil = array();
						}
						
						if($row[0]['user_name'] == 'admin'){
							$privil[] = 'customers';
							$privil[] = 'trialcustomers';
							$privil[] = 'help';
							$privil[] = 'message_validation';
							$privil[] = 'solutions';						
							
						}
						
						$_SESSION['admin_privileges'] = $privil;
						
						redirect(base_url().'webmanager/dashboard');

					}

					else {
						$error = "Invalid Credentials";
					}

				}
				else{
					$error = $this->form_validation->error_string();
				}

			}

			$data = array(
				'page_title'=>'Admin Login',
				'error'=>$error
			);

			$this->load->view('admin/index',$data);	

		}

	}

	


	/*==============================
		Admin logout
	===============================*/

	public function logout(){
		unset($_SESSION['logged_admin']);
		unset($_SESSION['admin_id']);
		unset($_SESSION['admin_email']);
		unset($_SESSION['admin_privileges']);
		
		redirect(base_url().'webmanager/dashboard/login');

	}
	



	/*==============================
		Admin Forgot password
	===============================*/

	public function forgotpassword()

	{

		$success = "";
		$error = "";

		$whr=array('id'=>'1');
		$email_id = $this->master_model->getRecords('admin_login',$whr,'*');

		$data = array(
			'page_title'=>"Forget Password",
			'middle_content'=>'forget-password',
			'success'=>'',
			'error'=>''
		);

		if(isset($_POST['btn_forget'])){ 

			$this->form_validation->set_rules('user_name','Email','trim|required|xss_clean|valid_email');
			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()){

				$email = $this->input->post('user_name',true);
				
				$admins = $this->master_model->getRecords('admin_login', array('recovery_email'=>$email));
				

				if(count($admins) > 0){ //$email == $email_id[0]['recovery_email']){

					$info_arr = array(
						'from'=>$email_id[0]['recovery_email'],
						'to'=>$email,
						'subject'=>'Password Recovery',
						'view'=>'forget-password-mail-to-admin'
					);

					$other_info = array(
						'password'=>$admins[0]['pass_word'],
						'username'=>$admins[0]['user_name'],
						'email'=>$email
					);

					$this->email_sending->sendmail($info_arr,$other_info);

					$data['success']='Password successfully sent to '.$email.'.';

				}

				else{

					$data['error'] = 'Email Address is invalid.';

				}

			}

			else{

				$data['error'] = $this->form_validation->error_string();

    		}

		}

		$this->load->view('admin/forget-password',$data);	

	}

	
	
	
	

	/*==============================
		Admin delete file
	===============================*/

	public function delete(){

		$doc_id=$this->uri->segment(4);

		$doc_name=$this->uri->segment(5);	

		if($this->master_model->deleteRecord('cf_file_upload','file_upload_id',$doc_id)){

			@unlink('uploads/crisis-document/'.$doc_name);

			$this->session->set_flashdata('success',' Document successfully deleted.');

			redirect(base_url().'webmanager/dashboard');

		}

	}
	
	
	
	
	/*==============================
		Admin new_admin
	===============================*/
	public function new_admin(){

		$username = $this->uri->segment(4);
		$realusername = '';
		$adminid = '';
		$adminemail = '';
		$error = '';

		$admins = $this->master_model->getRecords('admin_login', array('id !='=>'1'));
		$admin_exist = 0;
		if(count($admins) > 0){
			foreach($admins as $r=>$value){
				if($username == md5($value['user_name'])){
					$admin_exist++;
					$realusername = $value['user_name'];
					$adminid = $value['id'];
					$adminemail = $value['recovery_email'];
				}
			}
		}
		
		
		if($admin_exist == 0){
			$error = 'Link invalid.';
		}

		$data = array(
			'page_title'=>'New Admin Password',
			'error'=>$error,
			'realusername'=>$realusername,
			'adminid'=>$adminid,
			'adminemail'=>$adminemail
		);

		$this->load->view('admin/newadmin',$data);	

	
	}
	
	
	
	
	
	/*==============================
		Admin save_admin_password
	===============================*/
	public function save_admin_password(){
		$password = $_POST['password'];
		$admin_id = $_POST['admin_id'];
		$admin_name = $_POST['admin_name'];
		$admin_email = $_POST['admin_email'];
		
		$this->master_model->updateRecord('admin_login', array('pass_word'=>$password), array('id'=>$admin_id));
	
		$whr = array('id'=>'1');

		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

			
		$info_arr = array(
			'from'=>$adminemail[0]['recovery_email'],
			'to'=>$admin_email,
			'subject'=>'Welcome to CrisisFlo',
			'view'=>'admin-access-success-noti'
		);

	
		$other_info=array(
			'username'=>$admin_name,
			'email'=>$admin_email
		);

		$this->email_sending->sendmail($info_arr,$other_info);
		
		echo 'yeah';

	}
	

}
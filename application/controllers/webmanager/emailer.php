<?php

class Emailer extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}


	public function testCrypt()

	{
		
		
		/*$str = 'apple';
		
		
		//echo md5('Nga Nguyen');
		*/
		$input = "ZjHV1D8sDGcwWi35UGtk6ceKkc7TSY19te79lqcPZtc=";
		
		$encrypted = $this->master_model->encryptIt($input);
		$decrypted = $this->master_model->decryptIt($encrypted);
		
		echo '<h1> Encrypted: '. $encrypted .'<br>';
		echo 'Decrypted: '. $decrypted .'<br></h1>';
		
		
		
	}
	
	public function add()

	{
		
		if(isset($_POST['add_reseller']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('cc_email','Email','required|xss_clean|valid_email|is_unique[reseller_master.email_id]');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|is_natural|xss_clean');

			
			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$cc_email=$this->input->post('cc_email',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;
/*
				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				$cc_org=$this->input->post('cc_org',true);

				$modules=$this->input->post('modules',true);

				$cust_type=$this->input->post('cust_type',true);
*/
				$onetime_key=md5(microtime());

	

				$login_arr=array(
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'first_name'=>$this->master_model->encryptIt($cc_firstname),
					'last_name'=>$this->master_model->encryptIt($cc_lastname),
					'email_id'=>$this->master_model->encryptIt($cc_email),
					'digits'=>$this->master_model->encryptIt($crt_digits),
					'mobile'=>$this->master_model->encryptIt($mobile_num),
					'countrycode'=>$this->master_model->encryptIt($countrycode),
					'onetime_key'=>$onetime_key,
					'user_status'=>'0'
				);


				$resellers=$this->master_model->getRecords('reseller_master');
				
				foreach ($resellers as $r=>$value){
					if ($this->master_model->encryptIt($cc_email) == $value['email_id']){
						
						$this->session->set_flashdata('error','Reseller\'s email already exist in the database.');

						redirect(base_url().'webmanager/reseller/add');
					}
				}


				if($login_id=$this->master_model->insertRecord('reseller_master',$login_arr,TRUE))

				{

						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],

						'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-reseller');

						

						$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,

						'email'=>$cc_email);

						

						$this->email_sending->sendmail($info_arr,$other_info);

						

						$this->session->set_flashdata('success','Reseller added successfully.');

						redirect(base_url().'webmanager/reseller/manage');

						




				}

				else

				{

					$this->session->set_flashdata('error','Error while adding CC');

					redirect(base_url().'webmanager/coordinator/manage');

				}
			}

		}


		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Setup CC','middle_content'=>'add-reseller','countriescode'=>$countriescode);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function manage()

	{

		$all_emails = $this->master_model->getRecords('email_messages_master', array('module'=>''));

		$data = array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-emailer',
			'all_emails'=>$all_emails
		);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function update()

	{

		$mess_id = $this->uri->segment(4);

		if(isset($_POST['add_reseller']))

		{

			//$this->form_validation->set_rules('e_name','Name','required|xss_clean');

			$this->form_validation->set_rules('content','Content','required|xss_clean');
			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run())

			{
				

				$e_name=$this->input->post('e_name',true);

				$content=$this->input->post('content',true);


				$mess_arr=array(
				//	'name'=>$e_name,
					'content'=>$content
					
				);

						

				if($this->master_model->updateRecord('email_messages_master',$mess_arr,array('id'=>$mess_id)))

				{

					$this->session->set_flashdata('success',' Email updated successfully');

					redirect(base_url().'webmanager/emailer/update/'.$mess_id);

				}

				else

				{

					$this->session->set_flashdata('error','Error while updating reseller');

					redirect(base_url().'webmanager/emailer/update/'.$mess_id);

				}


			}

		}

		

		//$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$mess_info=$this->master_model->getRecords('email_messages_master',array('id'=>$mess_id));


		$data=array('page_title'=>'Update CC','middle_content'=>'update-emailer','mess_info'=>$mess_info);

		$this->load->view('admin/admin-view',$data);

	}

	
	

	public function delete()

	{

		$cc_id=$this->uri->segment(4);

		if($this->master_model->deleteRecord('reseller_master','login_id',$cc_id))

		{

			if($this->master_model->deleteRecord('cf_crisis_response_team','cc_id',$cc_id))

			{

				$this->session->set_flashdata('success','Reseller successfully deleted.');

				redirect(base_url().'webmanager/reseller/manage');

			}

		}

		else

		{

			$this->session->set_flashdata('success','Error Deleting CC.');

			redirect(base_url().'webmanager/coordinator/manage');

		}

	}
	
	
	

	public function check_duplication($org_name,$org_id)

	{

		$whr=array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num=$this->master_model->getRecordCount('organization_master',$whr);

		if($num==0)

		{return true;}

		else

		{return false;}

	}
	
	public function update_subject(){
		$id = $_POST['id'];
		$subject = $_POST['subject'];

		if($this->master_model->updateRecord('email_messages_master',array('subject'=>$subject),array('id'=>$id))){
			echo 'success';
		}
		
		
	}
	
	//----------------------------------------activate customer----------------------------------------//
	public function activate()

	{

		$reseller_id =$this->uri->segment(4);
		$cc_id = $this->uri->segment(5);

		if(isset($_POST['add_cc']))

		{

			$this->form_validation->set_rules('cc_firstname','First name','required|xss_clean');

			$this->form_validation->set_rules('cc_lastname','Last name','required|xss_clean');

			$this->form_validation->set_rules('countrycode','Country Code','required|xss_clean');

			$this->form_validation->set_rules('cc_phone_number','Mobile number','required|xss_clean');

			$this->form_validation->set_rules('cc_address','Address','required|xss_clean');

			$this->form_validation->set_rules('cc_city','City','required|xss_clean');

			$this->form_validation->set_rules('cc_state','State','required|xss_clean');

			$this->form_validation->set_rules('cc_zip_code','Zipcode','required|xss_clean');

			$this->form_validation->set_rules('cc_type','User Type','required|xss_clean');

			$this->form_validation->set_rules('cc_org','Organization','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())

			{

				$cc_firstname=$this->input->post('cc_firstname',true);

				$cc_lastname=$this->input->post('cc_lastname',true);

				$countrycode=$this->input->post('countrycode',true);

				$crt_digits=$this->input->post('cc_phone_number',true);
				
				$mobile_num= $countrycode.''.$crt_digits;

				$cc_address=$this->input->post('cc_address',true);

				$cc_city=$this->input->post('cc_city',true);

				$cc_state=$this->input->post('cc_state',true);

				$cc_zip_code=$this->input->post('cc_zip_code',true);

				$cc_type=$this->input->post('cc_type',true);

				$cc_org=$this->input->post('cc_org',true);

				$org_id=$this->input->post('org_id',true);

				$reseller_id=$this->input->post('reseller_id',true);

				$cc_email=$this->input->post('email_id',true);

				$modules=$this->input->post('modules',true);

				$cust_type=$this->input->post('cust_type',true);

				$onetime_key=md5(microtime());



				$themods = "";
				foreach($modules as $check) {
					$themods .= $check; 
				}		


				if($this->check_duplication($cc_org,$org_id)){
					//insert data to organization_master
					$org_arr=array(
						'organization_name'=>$cc_org,
						'active_module'=>$themods,
						'cust_type'=>$cust_type,
						'cc_id'=>$cc_id
					);

					$this->master_model->updateRecord('organization_master',$org_arr,array('organization_id'=>$org_id));
				}
				
				else{

					$this->session->set_flashdata('error','Organization name already exist in the database.');

					redirect(base_url().'webmanager/reseller/activate/'.$reseller_id.'/'.$cc_id);
				
				}


				$login_arr=array(
					'user_type'=>$cc_type,
					'onetime_key'=>$onetime_key
				);

				if($login_id=$this->master_model->updateRecord('cf_login_master',$login_arr,array('login_id'=>$cc_id)))

				{

					$cc_arr=array(
						'crt_first_name'=>$this->master_model->encryptIt($cc_firstname),
						'crt_last_name'=>$this->master_model->encryptIt($cc_lastname),
						'crt_digits'=>$this->master_model->encryptIt($crt_digits),
						'crt_mobile'=>$this->master_model->encryptIt($mobile_num),
						'countrycode'=>$this->master_model->encryptIt($countrycode),
						'crt_address'=>$this->master_model->encryptIt($cc_address),
						'crt_city'=>$this->master_model->encryptIt($cc_city),
						'crt_state'=>$this->master_model->encryptIt($cc_state),
						'activated'=>'yes',
						'crt_zip_code'=>$this->master_model->encryptIt($cc_zip_code)
					);

							

					if($this->master_model->updateRecord('cf_crisis_response_team',$cc_arr,array('login_id'=>$cc_id)))

					{
						
						//$this->session->set_flashdata('success','CC added successfully');

						$admin_email=$this->master_model->getRecords('email_id_master');
						
						//send email to cc
						$info_arr=array('to'=>$cc_email,'from'=>$admin_email[0]['info_email'],
						'subject'=>'Welcome to CrisisFlo','view'=>'registration-mail-to-cc');

						$other_info=array('first_name'=>$cc_firstname,'last_name'=>$cc_lastname,'onetime_key'=>$onetime_key,
						'email'=>$cc_email);
						
						
						//send email to reseller
						$reseller = $this->master_model->getRecords('reseller_master',array('login_id'=>$reseller_id));

						$info_arr2=array('to'=>$this->master_model->decryptIt($reseller[0]['email_id']),'from'=>$admin_email[0]['info_email'],
						'subject'=>'Customer Request Activation Granted','view'=>'activation-mail-to-reseller');

						$other_info2=array(
							'first_name'=>$this->master_model->decryptIt($reseller[0]['first_name']),
							'last_name'=>$this->master_model->decryptIt($reseller[0]['last_name']),
							'email'=>$this->master_model->decryptIt($reseller[0]['email_id'])
						);

						

						$this->email_sending->sendmail($info_arr,$other_info);

						$this->email_sending->sendmail($info_arr2,$other_info2);

						
						
						
						
						$this->session->set_flashdata('success','Customer successfully activated.');

						redirect(base_url().'webmanager/dashboard');

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating CC');

						redirect(base_url().'webmanager/coordinator/update/'.$cc_id);

					}

				}

			}

		}

		

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_info=$this->master_model->getRecords('cf_crisis_response_team as cc',array('cc.login_id'=>$cc_id),'cc.*,login.user_type');

		$the_org=$this->master_model->getRecords('organization_master',array('organization_name'=>$cc_info[0]['crt_organisation']));

		$countriescode=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Update CC','middle_content'=>'activate-reseller-request','cc_info'=>$cc_info,'countriescode'=>$countriescode,'org_info'=>$the_org);

		$this->load->view('admin/admin-view',$data);

	}
	
	public function button_text(){
		$txt = (isset($_POST['txt'])) ? $_POST['txt'] : 'Access CrisisFlo';
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$arr = array(
			'button_text'=>$txt
		);
		
		$emailer = $this->master_model->getRecords('email_messages_master');
		if($id != ''){
			$emailer = $this->master_model->getRecords('email_messages_master', array('id'=>$id));
		}
		
		
		foreach($emailer as $r=>$value){
			$this->master_model->updateRecord('email_messages_master', $arr, array('id'=>$value['id']));
		}
		
		echo json_encode($arr);
	}

		
	

}

?>
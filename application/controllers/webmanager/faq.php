<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/faq/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$faqs = $this->master_model->getRecords('faq','','*',array('id'=>'DESC'));

		$data = array(
			'middle_content'=>'manage-faq',
			'page_title'=>'Change Password',
			'faqs'=>$faqs
		);	
		$this->load->view('admin/admin-view',$data);


	}

	
	#--------------------------------------------->>add_faq view loading<<-------------------------------------
	public function add_faq() {
		$q = $_POST['question'];
		$a = $_POST['answer'];
		$faq_id = $_POST['faq_id'];

		if($faq_id !=''){
			if($this->master_model->updateRecord('faq',array('question'=>$q,'answer'=>$a),array('id'=>$faq_id))) {	
				$success_mess = 'FAQ successfully updated';
				$this->session->set_flashdata('success',$success_mess);
				echo 'success';
			}
			else{
				$this->session->set_flashdata('error','Something went wrong. Please try again.');
				echo 'error';
			}
		}
		else{
			if($id = $this->master_model->insertRecord('faq',array('question'=>$q,'answer'=>$a),true)) {	
				$success_mess = 'FAQ successfully added';
				$this->session->set_flashdata('success',$success_mess);
				echo 'success';
			}
			else{
				$this->session->set_flashdata('error','Something went wrong. Please try again.');
				echo 'error';
			}
			
		}
		
	}
	
	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function get_info() {
		$id = $_POST['id'];
		
		$faq = $this->master_model->getRecords('faq',array('id'=>$id));
		
		
		$info = array(
			'question'=>$faq[0]['question'],
			'answer'=>$faq[0]['answer']
		);

		echo json_encode($info);

	}	
	
	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$id = $_POST['id'];

		$this->master_model->deleteRecord('faq','id',$id);
		$data['id'] = $id;
		
		echo json_encode($data);
	}

	

}
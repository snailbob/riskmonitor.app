<?php

class Help extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}



	public function manage(){

		$help = $this->master_model->getRecords('admin_login', array('id'=>'1'));

		$content = $this->master_model->getRecords('admin_help_function', array('module'=>'recall'));

		$data = array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-help',
			'help'=>$help,
			'content'=>$content
		);

		$this->load->view('admin/admin-view',$data);

	}
	
	public function get_help_content(){
		$id = $_POST['id'];
		$content = $this->master_model->getRecords('admin_help_function', array('id'=>$id));
		
		$the_help = '';
		if(count($content) > 0){
			$the_help = $content[0]['help_content'];
		}
		
		echo $the_help;
	}
	
	
	
	public function save_help(){
		$help_content = $_POST['shtml'];
		$id = $_POST['id'];
	
		$this->master_model->updateRecord('admin_help_function', array('help_content'=>$help_content), array('id'=>$id));
	
		//$this->master_model->updateRecord('admin_login', array('help_content'=>$help_content), array('id'=>'1'));
		echo $help_content;
	}
	
	
	

}

?>
<?php

class Modules extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}



	public function manage(){

		$mods = $this->master_model->getRecords('module_master', '', '', array('sort'=>'ASC'));

		$data = array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-module',
			'mods'=>$mods
		);

		$this->load->view('admin/admin-view',$data);

	}
	
	
	
	
	
	
	public function update(){
		$mod_id = $this->uri->segment(4);

		if(isset($_POST['add_mod'])){

			$this->form_validation->set_rules('name','Module Name','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run()){

				$name = $this->input->post('name',true);

				$max_users = $this->input->post('max_users',true);
				$preselected = $this->input->post('preselected',true);
				//$price = $this->input->post('price',true);



				$arrr=array(
					'name'=>$name,
					'preselected'=>$preselected,
					'default_users'=>$max_users,
					//'price'=>$price
				);

				if($login_id=$this->master_model->updateRecord('module_master',$arrr,array('id'=>$mod_id))){


					$this->session->set_flashdata('success','Module updated successfully');

					redirect(base_url().'webmanager/modules/update/'.$mod_id);

				}

			}

		}


		$mods = $this->master_model->getRecords('module_master',array('id'=>$mod_id));

		$data=array('page_title'=>'Update CC','middle_content'=>'update-module','mods'=>$mods);

		$this->load->view('admin/admin-view',$data);

		
	}
	

}

?>
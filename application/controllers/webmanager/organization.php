<?php 

class Organization extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}

	
	public function org_status(){
		$status = $this->uri->segment(4);
		$org_id = $this->uri->segment(5);
		
		if($status == 'Y'){
			$newstat = 'N';
		}
		else {
			$newstat = 'Y';
		}
		
		$arr = array(
			'enabled'=>$newstat
		);

		$this->session->set_flashdata('success', ' Organization status successfully updated.');
		
		$this->master_model->updateRecord('organization_master', $arr, array('organization_id'=>$org_id));
		
		redirect('webmanager/organization/manage');
	}

	public function add()

	{

		if(isset($_POST['add_orgz']))

		{

			$this->form_validation->set_rules('organization_name','Organization','required|xss_clean|is_unique[organization_master.organization_name]');

			$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');

			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');
			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run())

			{

				$organization_name=$this->input->post('organization_name',true);	

				$sel_cc=$this->input->post('sel_cc',true);
				
				$modules=$this->input->post('modules',true);
				
				$cust_type=$this->input->post('cust_type',true);

				$packs = $this->input->post('input_packs',true);

				$themods = "";
				$count_mod = 1;
				foreach($modules as $check) {
					$themods .= 'm'.$check.'m';
					
					if(count($modules) != $count_mod){
						$themods .='-';
					}
					$count_mod++;
				}		

				$id_email = explode(' ', $sel_cc);

				$insrt_arr=array(
					'organization_name'=>$organization_name,
					'cc_id'=>$id_email[0],
					'input_pack_id'=>$packs,
					'cust_type'=>$cust_type,
					'active_module'=>$themods
				);
				
				if($org_id = $this->master_model->insertRecord('organization_master',$insrt_arr, true))

				{
					
					//update org id in input_packs table
					$this->master_model->updateRecord('input_packs',array('org_id'=>$org_id),array('id'=>$packs));
					
					
					//update org_id and cc_id for each inputs
					if ($packs != '0'){
						
						$updt_arr = array(
							'org_id'=>$org_id,
							'cc_id'=>$id_email[0]
						);
						
						$this->master_model->updateRecord('cf_file_upload',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_task',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_scenario',$updt_arr,array('input_pack_id'=>$packs));
						
						$this->master_model->updateRecord('cf_standby_messages',$updt_arr,array('input_pack_id'=>$packs));
					}



						$admin_email=$this->master_model->getRecords('email_id_master');

						$info_arr=array('to'=>$id_email[1],'from'=>$admin_email[0]['info_email'],
		
						'subject'=>'CrisisFlo Access',
		
						'view'=>'registration-mail-to-cc-org');
						
						$other_info=array(
						  'first_name'=>$id_email[2],
	
						  'last_name'=>$id_email[3]);
						
						$this->email_sending->sendmail($info_arr,$other_info);
	
						//create text file for organization chat room
						$room_name = 'chat_room/org';
						$room_name .= $org_id;
						$room_name .= '.txt';
						
						$myfile = fopen($room_name, "w");	
						
//					/**script to send email after confirmation*/
//					$whr=array('id'=>'1');
//
//					$adminemail=$this->master_model->getRecords('admin_login',$whr,'*');
//
//						
//					$info_arr=array('from'=>$adminemail[0]['recovery_email'],
//	
//					'to'=>$id_email[1],
//	
//					'subject'=>'CrisisFlo Access',
//	
//					'view'=>'registration-mail-to-cc-org');
//					
//					$other_info=array(
//					  'first_name'=>$id_email[2],
//
//					  'last_name'=>$id_email[3]);
//					
//					$this->email_sending->sendmail($info_arr,$other_info);
//					/**.script to send email after confirmation*/
					
					

					$this->session->set_flashdata('success','Orgnization added successfully');

					redirect(base_url().'webmanager/organization/manage'); 

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding Orgnization');

					redirect(base_url().'webmanager/organization/manage');

				}

			}

		}

		

		$whr_arr=array('login.user_level'=>'0');//'login.user_status'=>'1',

		$order_arr=array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str="cc.login_id,crt_first_name,crt_last_name,crt_email";
		
		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list=$this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		$input_packs=$this->master_model->getRecords('input_packs',array('org_id'=>'0'));

		$modules=$this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));
		

		$data=array('page_title'=>'Add Organization','middle_content'=>'add-organization','cc_list'=>$cc_list,'input_packs'=>$input_packs,'modules'=>$modules);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function manage()

	{

		$order_arr = array('org.organization_name'=>'ASC');

		$sel_str = "cc.crt_first_name,cc.crt_last_name,cc.login_id,org.*";

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=org.cc_id');

		$org_list=$this->master_model->getRecords('organization_master as org','',$sel_str,$order_arr);

		

		$data=array('page_title'=>'Manage Organization','middle_content'=>'manage-organization','org_list'=>$org_list);

		$this->load->view('admin/admin-view',$data);

	}

	

	public function check_dulication($org_name,$org_id) {

		$whr = array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num = $this->master_model->getRecordCount('organization_master',$whr);

		if($num == 0) {
			return true;
		}

		else {
			return false;
		}

	}

	

	public function update(){

		$org_id=$this->uri->segment(4);

		

		if(isset($_POST['add_orgz'])){

			$this->form_validation->set_rules('organization_name','Organization','required|xss_clean');

			$this->form_validation->set_rules('modules[]','Modules','required|xss_clean');
			
			$this->form_validation->set_rules('cust_type','Customer Type','required|xss_clean');

			$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			//$this->form_validation->set_rules('maxu','Select CC','required|xss_clean');
			
			//$this->form_validation->set_rules('input_packs','Input Pack','required|xss_clean');
			$this->form_validation->set_message('required', 'Mandatory field');

			

			if($this->form_validation->run()) {

				$organization_name = $this->input->post('organization_name',true);	
				
				$reseller = $this->input->post('reseller',true);

				$sel_cc=$this->input->post('sel_cc',true);
				
				$modules=$this->input->post('modules',true);
				
				$cust_type=$this->input->post('cust_type',true);
				
				$packs = $this->input->post('input_packs',true);

				$old_packs = $this->input->post('old_packs',true);

				$standard_max = $this->input->post('maxu1',true);
				$recall_max = $this->input->post('maxu5',true);
				$case_max = $this->input->post('maxu2',true);
				$member_max = $this->input->post('maxu4',true);
				$continuity_max = $this->input->post('maxu8',true);
				$simulation_modules = $this->input->post('simulation_modules',true);




				$themods = "";
				$count_mod = 1;
				foreach($modules as $check) {
					$themods .= 'm'.$check.'m';
					
					if(count($modules) != $count_mod){
						$themods .='-';
					}
					$count_mod++;
				}		

				
				

				if($this->check_dulication($organization_name,$org_id)) {

					$updt_arr=array(
						'organization_name'=>$organization_name,
						'cc_id'=>$sel_cc,
						'input_pack_id'=>$packs,
						'cust_type'=>$cust_type,
						'active_module'=>$themods,
						'active_mod'=>serialize($modules),
						'reseller_id'=>$reseller,
						'standard_max'=>$standard_max,
						'recall_max'=>$recall_max,
						'case_max'=>$case_max,
						'continuity_max'=>$continuity_max,
						'member_max'=>$member_max,
						'simulation_id'=>$simulation_modules
					);
					
					
					
					//update org of reseller first
					$org_of_reseller = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
					
					if($org_of_reseller[0]['reseller_id'] != $reseller){
						//remove previous resellers org
						//$this->master_model->updateRecord('reseller_master', array('org_id'=>'0'),array('login_id'=>$org_of_reseller[0]['reseller_id']));

					}
					
					if($this->master_model->updateRecord('organization_master',$updt_arr,array('organization_id'=>$org_id))){
						//update new reseller with org
						if($reseller != ''){
							//$this->master_model->updateRecord('reseller_master', array('org_id'=>$org_id),array('login_id'=>$reseller));
						}
						
						$updt_old_pack = array(
							'org_id'=>'1',
							'cc_id'=>'0'
						);

						//update org_id and cc_id to each inputs
						if ($packs != '0'){
							
							$updt_pack = array(
								'org_id'=>$org_id,
								'cc_id'=>$sel_cc
							);
							
							
							if ($old_packs != '0'){
								//reset old selected pack
								$this->master_model->updateRecord('cf_file_upload',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_task',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_scenario',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
								$this->master_model->updateRecord('cf_standby_messages',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
								
							}
						
							
							//assign new pack
							$this->master_model->updateRecord('cf_file_upload',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_task',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_pack,array('input_pack_id'=>$packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_pack,array('input_pack_id'=>$packs));
						}
						
						//update packs with no assignment
						if ($old_packs != '0' && $packs == '0'){
							//reset old selected pack
							$this->master_model->updateRecord('cf_file_upload',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_task',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_scenario',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
							$this->master_model->updateRecord('cf_standby_messages',$updt_old_pack,array('org_id !='=>'1','input_pack_id'=>$old_packs));
							
						}
					
					
						//update org id in input_packs table
						$this->master_model->updateRecord('input_packs',array('org_id'=>'0'),array('org_id'=>$org_id));
						$this->master_model->updateRecord('input_packs',array('org_id'=>$org_id),array('id'=>$packs));
						$this->master_model->updateRecord('organization_master',array('input_pack_id'=>'0'),array('input_pack_id'=>$old_packs,'organization_id !='=>$org_id));
	
							
							

						$this->session->set_flashdata('success','Organization updated successfully');

						redirect(base_url().'webmanager/organization/update/'.$org_id);

					}

					else

					{

						$this->session->set_flashdata('error','Error while updating Organization');

						redirect(base_url().'webmanager/organization/update/'.$org_id);

					}

				}

				else

				{

					$this->session->set_flashdata('error','Organization already exists');

					redirect(base_url().'webmanager/organization/update/'.$org_id);

				}

			}

		}

		

		$org_info = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));


		$modules=$this->master_model->getRecords('module_master','','',array('sort'=>'ASC'));

		$whr_arr=array(/*'login.user_status'=>'1',*/'login.user_level'=>'0');

		$order_arr=array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str="cc.login_id,crt_first_name,crt_last_name";

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list=$this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		$input_packs=$this->master_model->getRecords('input_packs');
		
		$resellers = $this->master_model->getRecords('reseller_master');
		$org_reseller = $this->master_model->getRecords('reseller_master', array('org_id'=>$org_id));

		
		$data=array(
			'page_title'=>'Add Organization',
			'middle_content'=>'update-organization',
			'cc_list'=>$cc_list,
			'org_info'=>$org_info,
			'input_packs'=>$input_packs,
			'modules'=>$modules,
			'resellers'=>$resellers,
			'org_reseller'=>$org_reseller
		);

		$this->load->view('admin/admin-view',$data);

	}

	

	/* delete organization */

	public function delete()

	{

		$org_id=$this->uri->segment(4);

		/* check documents present for organization if documents are present then unlink document first*/

		$documents=$this->master_model->getRecords('cf_file_upload',array('org_id'=>$org_id));

		if(count($documents)>0)

		{

			foreach($documents as $doc)

			{

				@unlink('uploads/crisis-document/'.$doc['file_upload_name']);

			}

		}

		

		/*select cc id assign for organization */

		$cc_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		

		$del_org="DELETE login,crt FROM organization_master as org 

			LEFT JOIN cf_crisis_response_team as crt ON crt.org_id=org.organization_id 

			LEFT JOIN cf_login_master as login ON login.login_id=crt.login_id 

			WHERE org.organization_id=".$org_id;

		

		if($this->db->query($del_org))

		{

			/* delete stakeholders */

			

			$del_stk="DELETE org,login,stakeholder FROM organization_master as org 

			LEFT JOIN cf_stakeholder as stakeholder ON stakeholder.org_id=org.organization_id 

			LEFT JOIN cf_login_master as login ON login.login_id=stakeholder.login_id 

			WHERE org.organization_id=".$org_id;

			$this->db->query($del_stk);

			

		}

		
		$this->session->set_flashdata('success','Organization successfully deleted.');
		redirect(base_url().'webmanager/organization/manage');

	}


	
	public function simulationlist(){
		$id = $_POST['id'];

		
		$whr_simu = array(
			'module_id'=>$id
		);
		

		$simulation_mod = $this->master_model->getRecords('simulation_modules', array('id'=>$id));
		$simulations = $this->master_model->getRecords('simulation', $whr_simu);
		$simulations_media = $this->master_model->getRecords('private_message_file', array('simulation_id !='=> '0'));
		
		
		
		if(isset($_POST['step_no'])){
			$step_index = $_POST['step_no'] - 3;
			
			
			
			if(count($simulations) > $step_index){
				$whr_simu['id'] = $simulations[$step_index]['id'];
				$simulations = $this->master_model->getRecords('simulation', $whr_simu);
			}
			else{
				$simulations = array();
			}
			
			
		}
		
		
		$the_simulations_media = array();
		if(count($simulations_media) > 0){
			foreach($simulations_media as $r=>$value){
				$the_simulations_media[] = array(
					'id'=>$value['file_upload_id'],
					'name'=>substr($value['file_upload_name'], 13),
					'ext'=>substr($value['file_upload_name'], -3),
					'simulation_id'=>$value['simulation_id'],
					'file_url'=>base_url().'uploads/simulations/'.$value['file_upload_name']
				);
			}
		}
		
		$data['simulation_mod'] = $simulation_mod;
		$data['simulations'] = $simulations;
		$data['simulations_media'] = $the_simulations_media;
		
		echo json_encode($data);
	}
	
	
	public function details(){
		$org_id = $this->uri->segment(4); //$_POST['org_id'];
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		
		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$org_stripe = $this->master_model->getRecords('stripe_customers', array('description'=>$org_info[0]['organization_name']), 'email, description, pre_selected_plan, stripe_id, created, cust_data');
		
		
		$whr = array(
			'org_id'=>$org_id,
			'type !='=>'2' //not task pickup
		);
		
		$logs = $this->master_model->getRecords('audit_log', $whr, '*', array('date_added'=>'DESC'));
	
		$the_logs = array();
		if(count($logs) > 0){
			foreach($logs as $r=>$value){
				if($value['type'] != '3'){
					$value['date_format'] = date_format(date_create($value['date_added']), 'jS F Y, g:ia');
					$value['module_name'] = $this->common_model->getmodulename($value['module_id']);
					$value['user_name'] = $this->common_model->getcrtname($value['crt_id']);
					$value['action'] = 'logged into';
					if($value['type'] != '0' && $value['type'] != '4'){
						$value['action'] = 'logged out from';
					}
					$the_logs[] = $value;
				}//filter task complete
			}
		}
		
		
		
		$from_stripe = array();
		$stripe_data = array();
		$org_data = array();
		if(count($org_info) > 0){
			foreach($org_info as $r=>$value){
				$value['date_added'] = date_format(date_create($value['date_added']), 'M d Y');
				$value['stakeholders'] = unserialize($value['stakeholders']);
				$value['cc_name'] = $this->common_model->getname($value['cc_id']);
				$stakeholder = array();
				if(is_array($value['stakeholders'])){
					
					if(count($value['stakeholders']) > 0){
						foreach($value['stakeholders'] as $stake){
							
							$stakeholder[] = $this->common_model->getstkname($stake);
						}
					}
				}
				$value['stakeholders'] = $stakeholder;
				$value['module_crts'] = array();
				
				$active_module = $value['active_module'];
				$the_mods = array();
				if (strpos($active_module, 'm1m') !== false){
					$the_mods[] = 'Crisis Manager';
					$case_man = array('name'=>'Crisis Manager','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '1'));
					$value['module_crts'][] = $case_man;
				}
				if (strpos($active_module, 'm5m') !== false){
					$the_mods[] = 'Recall Manager';
					$case_man = array('name'=>'Recall Manager','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '5'));
					$value['module_crts'][] = $case_man;
				}
				if (strpos($active_module, 'm2m') !== false){
					$the_mods[] = 'Case Management';
					$case_man = array('name'=>'Case Management','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '2'));
					$value['module_crts'][] = $case_man;
				}
				if (strpos($active_module, 'm4m') !== false){
					$the_mods[] = 'Member Safety';
					$case_man = array('name'=>'Member Safety','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '4'));
					$value['module_crts'][] = $case_man;
					
				}
				if (strpos($active_module, 'm8m') !== false){
					$the_mods[] = 'Continuity Manager';
					$case_man = array('name'=>'Continuity Manager','crts'=>$this->common_model->get_org_crts($value['cc_id'],$org_id, $active_mod = '8'));
					$value['module_crts'][] = $case_man;
					
				}
				if (strpos($active_module, 'm6m') !== false){
					$the_mods[] = 'Cost Monitor [Feature]';
				}
				if (strpos($active_module, 'm3m') !== false){
					$the_mods[] = 'Video Conferencing [Feature]';
				}
				if (strpos($active_module, 'm7m') !== false){
					$the_mods[] = 'SMS Notification [Feature]';
				}
				if (strpos($active_module, 'm9m') !== false){
					$the_mods[] = 'Bulk Notification [Feature]';
				}
				if (strpos($active_module, 'm10m') !== false){
					$the_mods[] = 'Recall KPIs [Feature]';
				}
				if (strpos($active_module, 'm11m') !== false){
					$the_mods[] = 'Simulation[Feature]';
				}
				if (strpos($active_module, 'm12m') !== false){
					$the_mods[] = 'Availability[Feature]';
				}
				
				$value['modules'] = $the_mods;
				
				$org_data[] = $value;
			}
				
		}
		if(count($org_stripe) > 0){

			$cu = Stripe_Customer::retrieve($org_stripe[0]['stripe_id']);

			$data['stripe_id'] = $org_stripe[0]['stripe_id'];
			//	$org_stripe = $org_stripe[0];
			$from_stripe['sub_total_count'] = $cu->subscriptions->total_count;
			
			$the_subsription_data = array();
			if($from_stripe['sub_total_count'] > 0){
				for($i = 0; $i < $from_stripe['sub_total_count']; $i++){
					$plan_data = $cu->subscriptions->data[$i];
					
					$current_period_start = $plan_data->current_period_start;
					$date = new DateTime("@$current_period_start");
					$current_period_start = $date->format('M d Y');
					
					$current_period_end = $plan_data->current_period_end;
					$date = new DateTime("@$current_period_end");
					$current_period_end = $date->format('M d Y');
					
					
					$the_subsription_data[] = array(
						
						"id"=>$plan_data->id,
						"plan"=> array(
						  "interval"=>$plan_data->plan->interval,
						  "name"=>$plan_data->plan->name,
						  "created"=>$plan_data->plan->created,
						  "amount"=>$plan_data->plan->amount,
						  "currency"=>$plan_data->plan->currency,
						  "id"=>$plan_data->plan->id,
						  "object"=>$plan_data->plan->object,
						  "livemode"=>$plan_data->plan->livemode,
						  "interval_count"=>$plan_data->plan->interval_count,
						  "trial_period_days"=>$plan_data->plan->trial_period_days,
						  "statement_descriptor"=>$plan_data->plan->statement_descriptor
						),
						"object"=>$plan_data->object,
						"start"=>$plan_data->start,
						"status"=>$plan_data->status,
						"customer"=>$plan_data->customer,
						"cancel_at_period_end"=>$plan_data->cancel_at_period_end,
						"current_period_start"=>$current_period_start,
						"current_period_end"=>$current_period_end,
						"ended_at"=>$plan_data->ended_at,
						"trial_start"=>$plan_data->trial_start,
						"trial_end"=>$plan_data->trial_end,
						"canceled_at"=>$plan_data->canceled_at,
						"quantity"=>$plan_data->quantity,
						"application_fee_percent"=>$plan_data->application_fee_percent,
						"discount"=>$plan_data->discount,
						"tax_percent"=>$plan_data->tax_percent,

					);
				}
			}
			
			$from_stripe['the_subsription_data'] = $the_subsription_data;
			
			
			
			foreach($org_stripe as $r=>$value){
				$cust_data = serialize($value['cust_data']);
				//$value['customer_data'] = $cust_data;
				unset($value['cust_data']);
				$ts = $value['created'];
				$date = new DateTime("@$ts");
				$the_date = $date->format('M d Y'); //'Y-m-d H:i:s');	
							
				$value['created'] = $the_date;
				$thekey = key($org_stripe);
				$stripe_data[]= $value; 
				break;
			}
		}
		
		
		$data['org_id'] = $org_id;
		$data['org_data'] = $org_data;
		$data['the_logs'] = $the_logs;
		
		$data['from_stripe'] = $from_stripe;
		$data['org_stripe'] = $stripe_data;
		//echo json_encode($data);
			
		$data['middle_content'] = 'view-organization-details';
		$data['page_title'] = 'Organization';
	
		$this->load->view('admin/admin-view',$data);

	
	}
	
}

?>
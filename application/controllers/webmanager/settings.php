<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Settings extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		
	}
	#--------------------------------------------->>resetdb<<-------------------------------------
	
	public function resetdb() {
		
		
		if(isset($_POST['action'])){
			/*do not include
			admin_login,
			cf_recall_guidance,
			cf_recall_packs,
			cf_recall_packs_steps,
			cf_recall_steps_category,
			countries_currency,
			country_t,
			email_id_master,
			email_messages_master,
			faq,
			forum_category_master,
			module_master,
			organization_master
			reminder_master
			sms_messages
			stripe_webhook
			time_zone
			
			*/
			
			//custom delete organization_master
			$dbname = 'organization_master';
			$this->master_model->deleteRecord($dbname,'organization_id !=','1');
			$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 2';
			$this->db->query($reset_query);
			echo $dbname.' - reset/empty success<br>';
	
	
			//custom delete for cf_recall_packs
			$dbname = 'cf_recall_packs';
			$this->master_model->deleteRecord($dbname,'continuity !=','1');
			$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 19';
			$this->db->query($reset_query);
			echo $dbname.' - reset/empty success<br>';
			
			
			$dbs = array(
				'audit_log',
				'bulk_attachments',
				'bulk_data',
				'bulk_kpi',
				'bulk_kpi_owners',
				'bulk_notification',
				'case_document_report',
				'cf_cc_reminders',
				'cf_continuity',
				'cf_continuity_steps',
				'cf_coordinator_master',
				'cf_cost_category',
				'cf_cost_category_item',
				'cf_crisis_response_team',
	//			'cf_crisis_response_team_2',
				'cf_file_history',
				'cf_file_upload',
				'cf_login_master',
	//			'cf_login_master_2',
				'cf_recall',
				'cf_recall_approvers',
				'cf_recall_blockers',
				'cf_recall_markers',
				'cf_recall_steps',
				'cf_recall_steps_file',
				'cf_scenario',
				'cf_stakeholder',
				'cf_standby_messages',
				'cf_task',
				'ci_sessions',
				'common_messages',
				'conference_participants',
				'conference_tokbox',
				'document_category',
				'employee_master',
				'forum_post_master',
				'input_packs',
				'log_scene_master',
				'log_task_master',
				'private_instant_messages',
				'private_messages',
				'private_message_file',
				'reporting',
				'reseller_master',
				'schedules',
				'sms',
				'states_master',
				'stripe_customers',
				'subscribe_request',
				'user_group',
				'video'
			);
			
			
			foreach($dbs as $dbname){
				
				$query = 'DELETE FROM '.$dbname;
				$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 1';
				$this->db->query($query);
				$this->db->query($reset_query);
				echo $dbname.' -  reset/empty success<br>';
			}	
			
			//$this->session->set_flashdata('success','Database reset successful.');	
		
			return false;
		
		}//reset end
		
		
		$admin_details = $this->master_model->getRecords('admin_login');
		$data = array(
			'middle_content'=>'reset-db-view',
			'page_title'=>'Reset Database',
		);	
		$this->load->view('admin/admin-view',$data);
		
	
	
	}
	
	
	#--------------------------------------------->>updating admin password<<-------------------------------------
	public function changePassword(){
		if(isset($_POST['change_admin_pw'])){
			$this->form_validation->set_rules('password','Password','required|xss_clean');
			$this->form_validation->set_rules('email','Email','trim|required|xss_clean|valid_email');	
			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()){
				
				$pass = $this->input->post('password');
				$admin_email = $this->input->post('email');
				$data_array = array('pass_word'=>$pass,'recovery_email'=>$admin_email);
				
				if($this->master_model->updateRecord('admin_login',$data_array,array('id'=>$_SESSION['admin_id']))){
					$this->session->set_flashdata('success','Infomation updated successfully.');
					redirect(base_url().'webmanager/settings/changePassword/');
				}
				else{
					$this->session->set_flashdata('error','Error while updating information');
				}
			}
		}
		
		$admin_details = $this->master_model->getRecords('admin_login', array('id'=>$_SESSION['admin_id']));
		
		$data = array(
			'middle_content'=>'change-password',
			'page_title'=>'Change Password',
			'admin_details'=>$admin_details
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	
	#-------------------------------------->>Function for updating emails<<-----------------------------------
	public function manageEmail()
	{
	if(isset($_POST['admin_update_email']))
		{
			$this->form_validation->set_rules('contact_email','Contact Email','required|xss_clean|valid_email');
			$this->form_validation->set_rules('info_email','info email','required|xss_clean|valid_email');
			$this->form_validation->set_rules('support_email','Support Email','required|xss_clean|valid_email');	
			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())
			{
				$contact_email=$this->input->post('contact_email','',true);
				$info_email=$this->input->post('info_email','',true);
				$support_email=$this->input->post('support_email','',true);
				$data_array=array('contact_email '=>$contact_email,'info_email'=>$info_email,'support_email'=>$support_email);
				
				if($this->master_model->updateRecord('email_id_master',$data_array,array('id'=>'1')))
				{
					$this->session->set_flashdata('success','Admin Email updated successfully.');
					redirect(base_url().'webmanager/settings/manageEmail/');
				}
				else
				{
					$this->session->set_flashdata('error','Error while updating Admin Email');
				}
			}
		}	
		$admin_email=$this->master_model->getRecords('email_id_master');
		
		$data=array('middle_content'=>'update-admin-email','page_title'=>'Update Admin Email','admin_email'=>$admin_email);	
		$this->load->view('admin/admin-view',$data);
	}
	
	public function rate(){
		$admin_info = $this->master_model->getRecords('admin_login', array('user_name'=>'admin'));
		
		if(isset($_POST['single_rate'])){
			$single_rate = $_POST['single_rate'];
			
			$this->master_model->updateRecord('admin_login', array('single_rate'=>$single_rate), array('id'=>$admin_info[0]['id']));
			
			
			$this->session->set_flashdata('success','Single User Rate updated successfully.');
			
			redirect(base_url().'webmanager/settings/rate');
			
		}
		
		$data = array(
			'middle_content'=>'update-single-rate',
			'page_title'=>'Update Admin Email',
			'admin_info'=>$admin_info
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	#-------------------------------------->>function for managing front pages<<------------------------------------
	
	public function managefrontpages()
	{
		$frontpages_info=$this->master_model->getRecords('front_pages');
		$data=array('middle_content'=>'manage-front-pages','page_title'=>'Manage Front Pages',
					'frontpages_info'=>$frontpages_info);
		$this->load->view('admin/admin-view',$data);	
	}
	
	//----------------------------------function for edit front content--------------------------------------
	
	public function updatefrontpages()
	{
		$front_id=$this->uri->segment(4);
		$frontpages_info=$this->master_model->getRecords('front_pages',array('front_id'=>$front_id));
			if(isset($_POST['btn_update']))
			{
				$this->form_validation->set_rules('page_title','title','required');
				$this->form_validation->set_rules('page_description','Description','required');
				$this->form_validation->set_message('required', 'Mandatory field');

				if($this->form_validation->run())
				{
					$page_title=$this->input->post('page_title','',true);
					$page_description=$this->input->post('page_description','',true);
					
					$data_array=array('front_title'=>$page_title,'front_description'=>$page_description);
					if($this->master_model->updateRecord('front_pages',$data_array,array('front_id'=>$front_id)))
					{
						$this->session->set_flashdata('success','Front Information updated successfully.');
						redirect(base_url().'webmanager/settings/updatefrontpages/'.$front_id);
					}
					else
					{
						$this->session->set_flashdata('error','Error while updating Front Information');
					}
				}
			}
		$data=array('middle_content'=>'edit-front-pages','page_title'=>'Update Front Pages',
					'frontpages_info'=>$frontpages_info);
		$this->load->view('admin/admin-view',$data);
	}
	//----------------------------------function for managing addons-------------------------------------
	
	public function manageaddons()
	{
		$addons=$this->master_model->getRecords('addons_master');
		$data=array('middle_content'=>'manage-addons','page_title'=>'Manage Addons','addons'=>$addons);
		$this->load->view('admin/admin-view',$data);
	}
	
	//------------------------------------function for edit addon----------------------------------------
	public function updateaddon()
	{
		$id=$this->uri->segment(4);
		if(isset($_POST['btn_update']))
		{
			$this->form_validation->set_rules('price','Price','required');
			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())
			{
					$price=$this->input->post('price','',true);
					if($this->master_model->updateRecord('addons_master',array('price'=>$price),array('addon_id'=>$id)))
					{
						$this->session->set_flashdata('success','Addon Information Updated successfully');
						redirect(base_url().'webmanager/settings/updateaddon/'.$id);
					}
					else
					{
						$this->session->set_flashdata('error','Error while updating Front Information');
					}
				}
		}
		$addoninfo = $this->master_model->getRecords('addons_master',array('addon_id'=>$id));
		$data = array('middle_content'=>'edit-addon','page_title'=>'Edit Addons','addoninfo'=>$addoninfo);
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function monthly_billing(){
		$billing = $this->common_model->single_payment(0, 'preview');
		
		$arr = array();
		if(count($billing['orgs']) > 0){
			foreach($billing['orgs'] as $r=>$value){
				if($value['total_bill'] > 0 && $value['last_payment'] != '0000-00-00 00:00:00'){
					$arr[] = $this->common_model->paynow_single($value['cc_info'], $value['total_bill'], $billing['next_month']);
				}
			}
		}
		echo json_encode($arr);
		
//		echo json_encode($billing);
	}
	
	public function log_sessions(){
		$log_sessions = $this->common_model->log_sessions();
		echo json_encode($log_sessions);
	}
}
<?php 

class Smscenter extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		
//        // load library
//        $this->load->library('nexmo');
//        // set response format: xml or json, default json
//        $this->nexmo->set_format('json');


	}


	

	public function manage(){
			
		$usdvalue = $this->common_model->get_nexmo_balance();

	
		$order_arr = array(
			'org.organization_name'=>'ASC'
		);

		$sel_str = "cc.crt_first_name,cc.crt_last_name,org.*";

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=org.cc_id');

		$org_list = $this->master_model->getRecords('organization_master as org','',$sel_str,$order_arr);


		$data = array(
			'page_title'=>'Nexmo Information Center',
			'middle_content'=>'nexmo-info',
			'org_list'=>$org_list,
			'usdvalue'=>$usdvalue
		);

		$this->load->view('admin/admin-view',$data);

	}

	
	public function messages(){
	
		$messages=$this->master_model->getRecords('sms_messages');

		$data=array('page_title'=>'Nexmo Information Center','middle_content'=>'manage-sms-messages','messages'=>$messages);

		$this->load->view('admin/admin-view',$data);
		
	}
	
	
	public function update_message(){
	
		$text = $_POST['text'];
		$id = $_POST['id'];
		
		$updt_arr = array(
			'text'=>$text
		);
		if($this->master_model->updateRecord('sms_messages',$updt_arr,array('id'=>$id))){
			echo 'success';
		}
		else{
			echo 'error';
		}
	}
	
	public function check_dulication($org_name,$org_id)

	{

		$whr=array('organization_name' =>$org_name,'organization_id !=' => $org_id);

		$num=$this->master_model->getRecordCount('organization_master',$whr);

		if($num==0)

		{return true;}

		else

		{return false;}

	}

	

	public function update()

	{
		//get balance
		$usdvalue = $this->common_model->get_nexmo_balance();

		$org_id = $this->uri->segment(4);

		

		if(isset($_POST['add_sms'])) {

			$this->form_validation->set_rules('sms_credit','Credit','required|is_natural|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run()) {

				$organization_name = $this->input->post('organization_name',true);	

				$sel_cc = $this->input->post('sel_cc',true);

				$sms_credit = $this->input->post('sms_credit',true);
				
				

				if($this->check_dulication($organization_name,$org_id)) {

					$updt_arr=array('sms_credit'=>$sms_credit);

					if($this->master_model->updateRecord('organization_master',$updt_arr,array('organization_id'=>$org_id))) {

						$this->session->set_flashdata('success',' Credit successfully added.');

						redirect(base_url().'webmanager/smscenter/manage');

					}

					else {

						$this->session->set_flashdata('error','Error while adding credit');

						redirect(base_url().'webmanager/organization/update/'.$org_id);

					}

				}

				else {

					$this->session->set_flashdata('error','Organization already exists');

					redirect(base_url().'webmanager/organization/update/'.$org_id);

				}

			}

		}

		

		$org_info = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));


		$whr_arr = array('login.user_status'=>'1','login.user_level'=>'0');

		$order_arr = array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str = "cc.login_id,crt_first_name,crt_last_name";

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list = $this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		

		$data=array(
			'page_title'=>'Add Organization',
			'middle_content'=>'add-smscredit',
			'cc_list'=>$cc_list,
			'org_info'=>$org_info,
			'usdvalue'=>$usdvalue
		);

		$this->load->view('admin/admin-view',$data);

	}

	public function transfer_waiting_sms(){
		
		$awaiting = $_POST['awaiting'];
		$credit = $_POST['credit'] + $awaiting;		
		$org_id = $_POST['orgid'];
		
		$this->master_model->updateRecord('organization_master', array('waiting_sms_credit'=>'0', 'sms_credit'=>$credit), array('organization_id'=>$org_id));
		
		echo round($credit, 2);
	}
	
	
	
	public function usage(){
		
		$org_id = $this->uri->segment(4);
		$sms_success = $this->master_model->getRecordCount('sms', array('org_id'=>$org_id, 'status'=>'0'));
		$sms_failed = $this->master_model->getRecordCount('sms', array('org_id'=>$org_id, 'status !='=>'0'));
		$sms_total = $this->master_model->getRecordCount('sms', array('org_id'=>$org_id));
		$sms_data = $this->master_model->getRecords('sms', array('org_id'=>$org_id));
		
		$data = array(
			'page_title'=>'SMS Usage',
			'middle_content'=>'sms-usage',
			'sms_success'=>$sms_success,
			'sms_failed'=>$sms_failed,
			'sms_total'=>$sms_total,
			'sms_data'=>$sms_data,
		);

		$this->load->view('admin/admin-view',$data);
	}

}

?>
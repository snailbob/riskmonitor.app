<?php

class States extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}



	public function manage()

	{

		$states=$this->master_model->getRecords('states_master');

		$data=array('page_title'=>'Manage CC','middle_content'=>'manage-states','states'=>$states);

		$this->load->view('admin/admin-view',$data);

	}
	
	
	
	//-----------------------------------add states----------------------------------------///
	public function add()

	{

		if(isset($_POST['add_pack']))

		{

			$this->form_validation->set_rules('state_name','States','required|xss_clean');

			$this->form_validation->set_rules('country','Country','required|xss_clean');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())

			{

				$state_name=$this->input->post('state_name',true);	
				$country=$this->input->post('country',true);	

				if($pack_id = $this->master_model->insertRecord('states_master',array('name'=>$state_name,'country_id'=>$country), true))

				{
					

					$this->session->set_flashdata('success','States successfully added');

					redirect(base_url().'webmanager/states/manage'); 

				}

				else

				{

					$this->session->set_flashdata('error','Error while adding States');

					redirect(base_url().'webmanager/states/manage');

				}

			}

		}

		

		$all_country=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Add States','middle_content'=>'add-states','all_country'=>$all_country);

		$this->load->view('admin/admin-view',$data);

	}

	
	
	
	public function update()
	{
		$state_id=$this->uri->segment(4);

		if(isset($_POST['add_mod']))

		{

			$this->form_validation->set_rules('name','States','required|xss_clean');

			$this->form_validation->set_rules('country','Country','required|xss_clean|numeric');

			$this->form_validation->set_message('required', 'Mandatory field');


			if($this->form_validation->run())

			{

				$name=$this->input->post('name',true);

				$country=$this->input->post('country',true);



				$arrr=array(
					'name'=>$name,
					'country_id'=>$country
				
				);

				if($login_id=$this->master_model->updateRecord('states_master',$arrr,array('id'=>$state_id)))

				{


						$this->session->set_flashdata('success','States updated successfully');

						redirect(base_url().'webmanager/states/update/'.$state_id);

				}

			}

		}

		

		$states=$this->master_model->getRecords('states_master',array('id'=>$state_id));

		$all_country=$this->master_model->getRecords('country_t');

		$data=array('page_title'=>'Update CC','middle_content'=>'update-states','states'=>$states,'all_country'=>$all_country);

		$this->load->view('admin/admin-view',$data);

		
	}
	

}

?>
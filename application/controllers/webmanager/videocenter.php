<?php 

class Videocenter extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

	}


	public function manage()

	{

		$order_arr=array('org.organization_name'=>'ASC');

		$sel_str="cc.crt_first_name,cc.crt_last_name,org.*";

		$this->db->join('cf_crisis_response_team as cc','cc.login_id=org.cc_id');

		$org_list=$this->master_model->getRecords('organization_master as org','',$sel_str,$order_arr);

		

		$data=array('page_title'=>'Manage Organization','middle_content'=>'video-info','org_list'=>$org_list);

		$this->load->view('admin/admin-view',$data);

	}

	

	

	public function update()

	{

		$org_id=$this->uri->segment(4);

		

		if(isset($_POST['add_orgz']))

		{

			$this->form_validation->set_rules('organization_name','Organization','required|xss_clean');

			$this->form_validation->set_rules('sel_cc','Select CC','required|xss_clean');

			$this->form_validation->set_rules('conf_time','No. of Video to Stream','required|numeric|xss_clean');

			$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');

			$this->form_validation->set_message('required', 'Mandatory field');

			if($this->form_validation->run())

			{	

				$organization_name=$this->input->post('organization_name',true);	

				$sel_cc=$this->input->post('sel_cc',true);

				$conf_time=$this->input->post('conf_time',true);

					$updt_arr=array('organization_name'=>$organization_name,'cc_id'=>$sel_cc,'conf_stream'=>$conf_time);

					if($this->master_model->updateRecord('organization_master',$updt_arr,array('organization_id'=>$org_id)))

					{

						$this->session->set_flashdata('success',' Video Stream Allocation successfully updated.');

						redirect(base_url().'webmanager/videocenter/manage');

					}

					else

					{

						$this->session->set_flashdata('error',' Error while updating video stream');

						redirect(base_url().'webmanager/videocenter/update/'.$org_id);

					}



			}

		}

		

		$org_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		

		$whr_arr=array('login.user_status'=>'1','login.user_level'=>'0');

		$order_arr=array('cc.crt_first_name'=>'ASC','cc.crt_last_name'=>'ASC');

		$sel_str="cc.login_id,crt_first_name,crt_last_name";

		$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$cc_list=$this->master_model->getRecords('cf_crisis_response_team as cc',$whr_arr,$sel_str,$order_arr);

		

		$data=array('page_title'=>'Add Organization','middle_content'=>'add-videotime','cc_list'=>$cc_list,

					'org_info'=>$org_info);

		$this->load->view('admin/admin-view',$data);

	}

	public function usage(){
		$org_id = $this->uri->segment(4);
		$org_info=$this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));
		$usage_info=$this->master_model->getRecords('video',array('org_id'=>$org_id),'*',array('setup_date'=>'ASC'));
		
		$total_minute = 0;
		$total_seconds = 0;
		$total_vid = 0;
		
		foreach($usage_info as $a=>$us){
			$time = explode(' ', $us['lapse_time']);
			$total_minute = $total_minute + $time[4];
			$total_seconds = $total_seconds + $time[5];
			
			$total_vid = $total_vid + $us['streamed'];
		}
		$the_min = $total_seconds/60;
		$the_total_time = $the_min + $total_minute;

		$data=array(
			'page_title'=>'Add Organization',
			'middle_content'=>'video-usage',
			'usage_info'=>$usage_info,
			'the_total_time'=>$the_total_time,
			'total_vid'=>$total_vid,
			'org_info'=>$org_info
		);

		$this->load->view('admin/admin-view',$data);
	
	}
	
	public function getdata_usage(){
		$org_id  = $_POST['org_id'];

		$usage_info = $this->master_model->getRecords('video',array('org_id'=>$org_id),'*');
	
	
	
		echo '[';
		foreach($usage_info as $us=>$info){
			$setup_date = date_create($info['setup_date']);

			echo '{ ';
			echo 'd: \''.date_format($setup_date, 'Y-m-d').'\', ';
			echo 'visits: '.$info['streamed'].', ';
			echo 'pageViews: '.$info['peers'];
			echo ' }, ';
		}
		echo ']';
		
	}

}

?>
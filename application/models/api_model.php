<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_model extends CI_Model
{
	public function __construct()
	{
		// parent::__construct();
	}
	
	
	public function hi()
	{
		return 'hi';
	}
	
	public function user_format($id){
		
		$personal_info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));
		
		$arr = array();
		if(count($personal_info) > 0){
			foreach($personal_info as $r=>$value){
				$a['countrycode'] = $this->master_model->decryptIt($value['countrycode']);
				$a['crt_address'] = $this->master_model->decryptIt($value['crt_address']);
				$a['crt_city'] = $this->master_model->decryptIt($value['crt_city']);
				$a['crt_digits'] = $this->master_model->decryptIt($value['crt_digits']);
				$a['crt_first_name'] = $this->master_model->decryptIt($value['crt_first_name']);
				$a['crt_last_name'] = $this->master_model->decryptIt($value['crt_last_name']);
				$a['crt_mobile'] = $this->master_model->decryptIt($value['crt_mobile']);
				$a['crt_state'] = $this->master_model->decryptIt($value['crt_state']);
				$a['crt_zip_code'] = $this->master_model->decryptIt($value['crt_zip_code']);
				$a['crt_email'] = $this->master_model->decryptIt($value['crt_email']);
				$a['crt_position'] = $value['crt_position'];
				$a['country_id'] = $value['country_id'];
				
				$arr[] = $a;
			}
		}
		
		return $arr;
	
	}
	

	public function db_field_id($db, $field, $id, $idfield = 'id'){
		$fieldname = $idfield;
		if($db == 'space_seekers'){
			$fieldname = 'seeker_id';
		}
		$data = $this->master_model->getRecords($db, array($fieldname=>$id));
		$name = 'Not specified';
		if(count($data) > 0){
			$name = $data[0][$field];
		}
		return $name;
	}
	
	
	public function kpi($recall_id, $cc_id, $org_id, $login_id)
	{


			$active_mod  = '5';
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$login_id));
			$nowtime = $this->common_model->usertime($user_info[0]['timezone']);
			$data['result'] = 'success';
			
			
			
			$whr_inci = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'closed'=>'0'
			);
			
			if($active_mod == '5'){
				$inci_type = 'recall';
				$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
			}
			else{ //continuity = 8
				$inci_type = 'continuity';
				$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
			}
			
			
			$whr_noti = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'type'=>$inci_type,
				'incident_id'=>$recall_id
			);
			
			$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
			
			$email_sent_count = 0;
			$email_confirmed_count = 0;
			$sms_sent_count = 0;
			if(count($notices_sent) > 0){                
				foreach($notices_sent as $r=>$ns){
					//email sent count
					$whr_email_sent = array(
						'bulk_id'=>$ns['id'],
						'email_sent'=>'1'
					);
					$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

					//email confirmed count
					$whr_email_confirmed = array(
						'bulk_id'=>$ns['id'],
						'email_confirmed'=>'1'
					);
					$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


					//sms sent count
					$whr_sms_sent = array(
						'bulk_id'=>$ns['id'],
						'sms_sent'=>'1'
					);
					$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
					
					$email_sent_count += $bulk_data_email_sent;
					$email_confirmed_count += $bulk_data_email_confirmed;
					$sms_sent_count += $bulk_data_sms_sent;
				
				}
				
			}
	
			//get notification kpi
			$noti_kpi = 0;
			if($email_sent_count != 0){
				$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
			}
			$notif['email_sent_count'] = $email_sent_count;
			$notif['email_confirmed_count'] = $email_confirmed_count;
			$notif['sms_sent_count'] = $sms_sent_count;
			$notif['noti_kpi'] = $noti_kpi;
			$notif['bgcolor'] = 'purple';
			$notif['name'] = 'Outbound notifications';

			
			$data['out_noti'] = $notif;
			//$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);

			$whr_kpis = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'incident_id'=>$recall_id,
				'incident_type'=>$active_mod
			);
			
			$kpis = $this->master_model->getRecords('bulk_kpi', $whr_kpis, '*', array('date_added'=>'DESC'));


			$inb_contacts['contact_cntr'] = 'No data';
			$inb_contacts['kpi_email'] = 'No data';
			$inb_contacts['kpi_website'] = 'No data';
			$inb_contacts['kpi_trading'] = 'No data';

			$inb_contacts['contact_cntr2'] = 'No data';
			$inb_contacts['kpi_email2'] = 'No data';
			$inb_contacts['kpi_website2'] = 'No data';
			$inb_contacts['kpi_trading2'] = 'No data';
			$inb_contacts['name'] = 'Inbound customer contacts';
			$inb_contacts['bgcolor'] = 'red';
			
			if(count($kpis) > 0){
				
				if($kpis[0]['contact_cntr'] != ''){
					$inb_contacts['contact_cntr'] = number_format($kpis[0]['contact_cntr']);
					$inb_contacts['kpi_email'] = number_format($kpis[0]['kpi_email']);
					$inb_contacts['kpi_website'] = number_format($kpis[0]['kpi_website']);
					$inb_contacts['kpi_trading'] = number_format($kpis[0]['kpi_trading']);
				
					$inb_contacts['contact_cntr2'] = $kpis[0]['contact_cntr'];
					$inb_contacts['kpi_email2'] = $kpis[0]['kpi_email'];
					$inb_contacts['kpi_website2'] = $kpis[0]['kpi_website'];
					$inb_contacts['kpi_trading2'] = $kpis[0]['kpi_trading'];
				
				}
			
			}
			
			$data['in_contacts'] = $inb_contacts;

	
			//set kpi variables
			$all_kpi = array();
			
			$all_kpi[] = array(
				'type'=>'comp_kpi',
				'dbcell'=>'comp_data',
				'bgcolor'=>'orange',
				'name'=>'Units under Company control'
			);
		
			$all_kpi[] = array(
				'type'=>'dist_kpi',
				'dbcell'=>'dist_data',
				'bgcolor'=>'blue',
				'name'=>'Units in distribution'
			);
		
			$all_kpi[] = array(
				'type'=>'market_kpi',
				'dbcell'=>'market_data',
				'bgcolor'=>'green',
				'name'=>'Units in consumer market'
			);



			$count_all_kpi = 1;
			$the_kpis = array();
			foreach($all_kpi as $r=>$value){
				$the_val['kpi_cell']= $value['dbcell'];
				$the_val['type']= $value['type'];
				$the_val['bgcolor']= $value['bgcolor'];
				$the_val['name']= $value['name'];

				
				//set value
				$the_val['total_unit'] = '';
				$the_val['unit_return'] = '';
				$the_val['u_disposed'] = '';
				$the_val['u_transformed'] = '';
				$the_val['u_corrections'] = 'NA';
				$the_val['the_kpi'] = '0';
				$the_val['u_date_update'] = '';
				$the_val['u_threshold'] = '0';
				
				//check count of kpi
				if(count($kpis) > 0) {
					$kpi_id = $kpis[0]['id'];
					$dbcell = unserialize($kpis[0][$value['dbcell']]);
					if($dbcell != ''){ //get each kpi db cell data
						$the_val['total_unit'] = number_format($dbcell['total_unit']);
						$the_val['unit_return'] = number_format($dbcell['unit_return']);
						$the_val['u_disposed'] = number_format($dbcell['u_disposed']);
						$the_val['u_transformed'] = number_format($dbcell['u_transformed']);
						$the_val['u_corrections'] = number_format($dbcell['u_corrections']);
						$the_val['the_kpi'] = round($dbcell['the_kpi'], 0);
						$the_val['u_date_update'] = date_format(date_create($dbcell['date_updated']), 'M d, Y, g:i A');
						$the_val['u_threshold'] = round($dbcell['threshold'], 0);
					}
				}
				
				$the_val['kpi_status'] = '';
				if($the_val['the_kpi'] < $the_val['u_threshold']){
					$the_val['kpi_status'] = '<i class="fa fa-circle text-red pull-right"></i>';
				}
				
				
				$the_kpis[] = $the_val;
			}
			
			$data['the_kpis'] = $the_kpis;
			
			return $data;
				
	
	}
	
	public function dashboard($incident_id, $cc_id, $org_id)
	{

		$active_module = '5';
		
		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
	
	
		if($active_module == '5'){
			$incident_type = '5';
			$inci_type = 'recall';
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

		}
		else{ //continuity = 8
			$incident_type = '8';
			$inci_type = 'continuity';
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
		}
	
		//check if incident empty
//		if(count($the_incident) == 0){
//			redirect('cc/kpi');
//		}

		//get kpi owners
		$whr_dist_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'dist',
			'deleted_dist'=>'0'
		);
		$whr_market_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'mkt',
			'deleted_mkt'=>'0'
		);
		$whr_all_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type
		);
		$whr_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all'
		);
		$whr_notdeldist_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all',
			'deleted_dist'=>'0'
		);
		$whr_notdelmkt_both_owner = array(
			'incident_id'=>$incident_id,
			'incident_type'=>$incident_type,
			'kpi_type'=>'all',
			'deleted_mkt'=>'0'
		);
		$dist_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_dist_owner);
		$market_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_market_owner);
		$all_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_all_owner);
		$both_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_both_owner);
		$both_notdeldist_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_notdeldist_both_owner);
		$both_notdelmkt_owners = $this->master_model->getRecords('bulk_kpi_owners', $whr_notdelmkt_both_owner);
		
		
		//get noti count kpi
		$whr_noti = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type'=>$inci_type,
			'incident_id'=>$incident_id
		);
		
		$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
		

		$email_sent_count = 0;
		$email_confirmed_count = 0;
		$sms_sent_count = 0;
		if(count($notices_sent) > 0){                
			foreach($notices_sent as $r=>$ns){
				//email sent count
				$whr_email_sent = array(
					'bulk_id'=>$ns['id'],
					'email_sent'=>'1'
				);
				$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

				//email confirmed count
				$whr_email_confirmed = array(
					'bulk_id'=>$ns['id'],
					'email_confirmed'=>'1'
				);
				$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


				//sms sent count
				$whr_sms_sent = array(
					'bulk_id'=>$ns['id'],
					'sms_sent'=>'1'
				);
				$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
				
				$email_sent_count += $bulk_data_email_sent;
				$email_confirmed_count += $bulk_data_email_confirmed;
				$sms_sent_count += $bulk_data_sms_sent;
			
			}
			
		}

		//get notification kpi
		$noti_kpi = 0;
		if($email_sent_count != 0){
			$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
		}
		


	
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'incident_type'=>$incident_type,
			'incident_id'=>$incident_id
		);
		
		$kpis = $this->master_model->getRecords('bulk_kpi', $whr, '*', array('date_added'=>'DESC'));
	
	
		//set kpi variables
		$all_kpi = array();
		
		$all_kpi[] = array(
			'type'=>'comp_kpi',
			'dbcell'=>'comp_data',
			'kpistatus'=>'comp_stat',
			'name'=>'Units under Company control'
		);
	
		$all_kpi[] = array(
			'type'=>'dist_kpi',
			'dbcell'=>'dist_data',
			'kpistatus'=>'dist_stat',
			'name'=>'Units in distribution'
		);
	
		$all_kpi[] = array(
			'type'=>'market_kpi',
			'dbcell'=>'market_data',
			'kpistatus'=>'market_stat',
			'name'=>'Units in consumer market'
		);

	
		//format kpi table data
		$count_all_kpi = 1;
		$kpi_table = array();
		foreach($all_kpi as $r=>$value){
			$kpi_cell = $value['dbcell'];
			$akpiclass= '';
			$akpistyle= '';
		
			if($value['type'] != 'comp_kpi') {
				$akpiclass = 'text-primary';
				$akpistyle = 'cursor: pointer;';
				
				if($value['type'] == 'dist_kpi'){
					$akpiclass .=" dist_toggle_btn";
				}
				else if($value['type'] == 'market_kpi'){
					$akpiclass .=" market_toggle_btn";
				}
			}

			
			//set variables
			$total_unit = '';
			$unit_return = '';
			$u_disposed = '';
			$u_transformed = '';
			$u_corrections = 'NA';
			$the_kpi = 'NA';
			$u_threshold = '0';
	
			$kpi_id = '';
			
			//check count of kpi
			if(count($kpis) > 0) {
				$kpi_id = $kpis[0]['id'];
				$dbcell = unserialize($kpis[0][$value['dbcell']]);
				if($dbcell != ''){ //get each kpi db cell data
					$total_unit = number_format($dbcell['total_unit']);
					$unit_return = number_format($dbcell['unit_return']);
					$u_disposed = number_format($dbcell['u_disposed']);
					$u_transformed = number_format($dbcell['u_transformed']);
					$u_corrections = number_format($dbcell['u_corrections']);
					$the_kpi = round($dbcell['the_kpi'], 0).'%';
					$u_threshold = round($dbcell['threshold'], 0);
				
				}
			}
			
			
			$karr = array(
				'kpi_id'=>$kpi_id,
				'total_unit'=>$total_unit,
				'unit_return'=>$unit_return,
				'u_disposed'=>$u_disposed,
				'u_transformed'=>$u_transformed,
				'u_corrections'=>$u_corrections,
				'the_kpi'=>$the_kpi,
				'u_threshold'=>$u_threshold,
				'akpiclass'=>$akpiclass,
				'akpistyle'=>$akpistyle,
			);	
			
			$kpi_table[] = $karr;
		}
		
	
	
		$data=array(
			'page_title'=>'Communication File',
			'middle_content'=>'bulk_kpi_manage',
			'error'=>'',
			'success'=>'',
			'kpis'=>$kpis,
			'kpi_table'=>$kpi_table,
			//'incidents'=>$incidents,
			'the_incident'=>$the_incident,
			'email_sent_count'=>$email_sent_count,
			'email_confirmed_count'=>$email_confirmed_count,
			'noti_kpi'=>$noti_kpi,
			'market_owners'=>$market_owners,
			'dist_owners'=>$dist_owners,
			'both_owners'=>$both_owners,
			'all_owners'=>$all_owners,
			'both_notdeldist_owners'=>$both_notdeldist_owners,
			'both_notdelmkt_owners'=>$both_notdelmkt_owners,
			'all_kpi'=>$all_kpi

		);
		
		return $data;
		
	}
	
	public function auditlog($cc_id, $org_id){

		$active_module = '5';
		
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type !='=>'4' //not cc_login
		);
		
		$logs = $this->master_model->getRecords('audit_log', $whr, '*', array('date_added'=>'DESC'));
	
		$the_logs = array();
		if(count($logs) > 0){
			foreach($logs as $r=>$value){
				if($value['type'] != '5'){
					
					$value['message'] = '';	
					$value['format_date'] = date_format(date_create($value['date_added']), 'jS F Y, g:ia');					
					if($value['type'] == 0){ //login
						$value['message'] = $this->common_model->getcrtname($value['crt_id']).' logged into '.$this->common_model->getmodulename($value['module_id']);
	
						
					}
					if($value['type'] == 1){ //logout
						$value['message'] = $this->common_model->getcrtname($value['crt_id']).' logged out '.$this->common_model->getmodulename($value['module_id']);
					}
					if($value['type'] == 2){ //pickup task
						$value['message'] = $this->common_model->getcrtname($value['crt_id']).' picked up task <i>'.$this->common_model->gettaskname($value['inci_task_id'], $value['module_id']).'</i> in '.$this->common_model->getmodulename($value['module_id']);
						
					}
					if($value['type'] == 3){ //completed task
						$value['message'] = $this->common_model->getcrtname($value['crt_id']).' marked completed task <i>'.$this->common_model->gettaskname($value['inci_task_id'], $value['module_id']).'</i> in '.$this->common_model->getmodulename($value['module_id']);
						
					}
		
					
					$the_logs[] = $value;
					
					
				}//filter cc_logout
			}
		}

		$data = array(
			'page_title'=>'Activity Log',
			'logs'=>$the_logs
		);
		
		return $data;

	}
	
	
	public function steptasks($recall_id, $incident_type = 5){
		
		$arr = array();
		
		for($step_no = 3; $step_no < 10; $step_no++){

			$tasks = $this->master_model->getRecords('cf_recall_steps', array('step_no'=>$step_no, 'recall_id'=>$recall_id), 'question, id', array('id'=>'DESC'));

			$arr[] = array(
				'step_no'=>$step_no,
				'tasks'=>$tasks
			);
		
		}
		
		return $arr;
		
	}
	
	public function crt_modules($org_id){
		$whr = array('organization_id'=>$org_id);
		$org_modules = $this->master_model->getRecords('organization_master',$whr);
		$org_module = '5';
		
		
		if(count($org_modules) > 0){
			if (strpos($org_modules[0]['active_module'], 'm3m') !== false){
				$vid_mod = '3';
			} else{
				$vid_mod = '';
			}
			
			if (strpos($org_modules[0]['active_module'], 'm6m') !== false){
				$cost_mod = '6';
			} else{
				$cost_mod = '';
			}
			
			if (strpos($org_modules[0]['active_module'], 'm7m') !== false){
				$sms_mod = '7';
			} else{
				$sms_mod = '';
			}
			
			if (strpos($org_modules[0]['active_module'], 'm9m') !== false){
				$bulk_mod = '9';
			} else{
				$bulk_mod = '';
			}
			
			if (strpos($org_modules[0]['active_module'], 'm10m') !== false){
				$kpi_mod = '0';
			} else{
				$kpi_mod = '';
			}
			
			if (strpos($org_modules[0]['active_module'], 'm11m') !== false){
				$sim_mod = 'a';
			} else{
				$sim_mod = '';
			}
			
			if (strpos($org_modules[0]['active_module'], 'm12m') !== false){
				$ava_mod = 'b';
			} else{
				$ava_mod = '';
			}
			
			
			$org_module .= '-'.$cost_mod.'-'.$sms_mod.'-'.$vid_mod.'-'.$bulk_mod.'-'.$kpi_mod.'-'.$sim_mod.$ava_mod;
		}
		
		return $org_module;
			
	}
	
	
	public function newcrt($cc_id, $org_id, $user_info){

        $this->load->library('nexmo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');
		
	 	$eurtousdval = $this->common_model->get_nexmo_balance('exchange');

		$active_module = $this->crt_modules($org_id);
		$current_module = substr($active_module, 0, 1);
		$timezone = $this->db_field_id('cf_crisis_response_team', 'timezone', $cc_id, 'login_id');
		

		$whr = array(
			'organization_id'=>$org_id
		);

		$org_name = $this->master_model->getRecords('organization_master',$whr,'*');

		
//		alt_member: "asdf"
//		country_id: "7"
//		crisis_function: "asdf"
//		crt_digits: "43524"
//		crt_email: "asdfa@asdf.com"
//		crt_first_name: "adsfs"
//		crt_last_name: "sadf"
//		crt_position: "asdf"
//		group: "9"
		

		$group = $user_info['group'];
		$access = $this->db_field_id('user_group', 'access', $group, 'id');
		
		$cct_firstname = $user_info['crt_first_name'];
		$cct_lastname = $user_info['crt_last_name'];
		$crt_position = $user_info['crt_position'];
		$crt_email = $user_info['crt_email'];
		$location = '';
		$location_lat = '';
		$location_lng = '';
		$crt_func = $user_info['crisis_function'];
		$alt_member = $user_info['alt_member'];
	
	
		$countrycodedd = $user_info['country_id'];
		$countrycode = $this->api_model->db_field_id('country_t', 'calling_code', $countrycodedd, 'country_id');
		$crt_digits = ltrim($user_info['crt_digits'], '0');
		$crt_no = $countrycode.$crt_digits;

		

		//get validation message from admin
		$action_message_id = '74';
		$action_message = $this->common_model->get_message($action_message_id);
		
		$onetime_key = md5(microtime());


		//check existing crt on a module and org
		$whr_same_exist = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'crt_email'=>$this->master_model->encryptIt($crt_email)
		);
		$the_same_crt = $this->master_model->getRecords('cf_crisis_response_team', $whr_same_exist);	
		
		if(count($the_same_crt) > 0){
			foreach($the_same_crt as $r=>$val){
				$crt_active_org = substr($val['module'], 0, 1);
				if($crt_active_org == $current_module){
					$data['result'] = 'exist';
					$data['message'] = $action_message['error'];
					return $data;
					return false;
				}
			}
		}
		
		
		
		//check existing email
		$users = $this->master_model->getRecords('cf_login_master');	
		$user_count = 0;

		foreach($users as $ss=>$usr){
			if($usr['email_id'] == $this->master_model->encryptIt($crt_email)){
				$user_count++;
			}
		}

		$nowtime = date("Y-m-d h:i:s");

		$admin_array = array(
			'email_id'=>$this->master_model->encryptIt($crt_email),
			'user_level'=>'1',
			'default_org'=>$org_id,
			'onetime_key'=>$onetime_key,
			'date_added'=>$nowtime
		);

		$login_id = $this->master_model->insertRecord('cf_login_master', $admin_array, true);




		//response_team inputs
		$input_array = array(
			'login_id'=>$login_id,
			'group_id'=>$group,
			'crt_first_name'=>$this->master_model->encryptIt($cct_firstname),
			'crt_last_name'=>$this->master_model->encryptIt($cct_lastname),
			'crt_organisation'=>$org_name[0]['organization_name'],
			'crt_position'=>$crt_position,
			'crt_email'=>$this->master_model->encryptIt($crt_email),
			'location'=>$location,
			'location_lat'=>$location_lat,
			'location_lng'=>$location_lng,
			'country_id'=>$countrycodedd,
			'countrycode'=>$this->master_model->encryptIt($countrycode),
			'crt_digits'=>$this->master_model->encryptIt($crt_digits),
			'crt_mobile'=>$this->master_model->encryptIt($crt_no),
			'crisis_function'=>$crt_func,
			'alt_member'=>$alt_member,
			'access'=>$access,
			
			
			'module'=>$active_module,
			'timezone'=>$timezone,
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);

		$new_crt_id = $this->master_model->insertRecord('cf_crisis_response_team', $input_array, true);




		//insert im room
		$wherrr = array(
			'org_id'=>$org_id,
			'login_id !='=>$login_id
		);
		
		$crt_memb = $this->master_model->getRecords('cf_crisis_response_team',$wherrr,'*');
		
		$chat_id = 'im_'.$login_id.'-'.$cc_id;
		$im_cc = array(
			'sender_id'=>$login_id,
			'receiver_id'=>$cc_id,
			'chat_id'=>'im_'.$login_id.'-'.$cc_id,
			
		);
		
		$this->master_model->insertRecord('private_instant_messages',$im_cc);
		
		$im_cc2 = array(
			'sender_id'=>$cc_id,
			'receiver_id'=>$login_id,
			'chat_id'=>'im_'.$login_id.'-'.$cc_id,
			
		);

		$this->master_model->insertRecord('private_instant_messages',$im_cc2);
		
		//create file room for crt and cc
		$room_name = 'chat_room/im/';
		$room_name .= $chat_id;
		$room_name .= '.txt';
		$myfile = fopen($room_name, "w");	
		
		if(count($crt_memb) > 0){
			foreach($crt_memb as $ccc => $memb){
				$chat_id = 'im_'.$login_id.'-'.$memb['login_id'];
				$im = array(
					'sender_id'=>$login_id,
					'receiver_id'=>$memb['login_id'],
					'chat_id'=>$chat_id,
					
				);
				
				$this->master_model->insertRecord('private_instant_messages',$im);
				
				$im2 = array(
					'sender_id'=>$memb['login_id'],
					'receiver_id'=>$login_id,
					'chat_id'=>$chat_id,
					
				);
				
				$this->master_model->insertRecord('private_instant_messages',$im2);
				
				
				//create file room for crt and crts
				$room_name = 'chat_room/im/';
				$room_name .= $chat_id;
				$room_name .= '.txt';
				
				$myfile = fopen($room_name, "w");	
				
			}
			
		} //.insert im room

		//send sms
		$sms_message = $this->master_model->getRecords('sms_messages', array('name'=>'crt-registration'));

		$from = 'CrisisFlo';
		$message = array(
			'text' => $sms_message[0]['text']
		);
		
		
		$to = $crt_no;
		$response = $this->nexmo->send_message($from, $to, $message);

		foreach ($response->messages as $messageinfo) {
			$recipient = $messageinfo->{'to'};
			$status = $messageinfo->{'status'};
		}

			$det_contact = $cct_firstname;
			$det_contact .= ' ';
			$det_contact .= $cct_lastname;
			$det_contact .= ' - ';
			$det_contact .= $recipient;

		if ($status == '0'){

		$date_sent = $this->common_model->userdatetime($cc_id);

			foreach ($response->messages as $messageinfo) {
				$message_id = $messageinfo->{'message-id'};
				$messprice = $messageinfo->{'message-price'};
				$message_price = $eurtousdval * $messprice;
				$network = $messageinfo->{'network'};
			}
			
			
			$sms_data = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'message_id'=>$message_id,
				'network'=>$network,
				'recipient'=>$det_contact,
				'price'=>$message_price,
				'status'=>$status,
				'date_sent'=>$date_sent
			);
			
			$my_sms = $this->master_model->insertRecord('sms', $sms_data, true);
			
			
			
		}
		else{
			
			foreach ($response->messages as $messageinfo) {
				$err_txt = $messageinfo->{'error-text'};
			}

			$err_stat = $status;
			$err_stat .= ' - ';
			$err_stat .= $err_txt;
			
			$sms_data = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'recipient'=>$det_contact,
				'price'=>'0',
				'status'=>$err_stat
			);
			
			$my_sms = $this->master_model->insertRecord('sms', $sms_data, true);
		}

		//.send sms

		$whr = array('id'=>'1');
		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');


		$info_arr = array(
			'from'=>$adminemail[0]['recovery_email'],
			'to'=>$crt_email,
			'subject'=>'Welcome to CrisisFlo',
			'view'=>'crt_registration_on_crisesflo'
		);



		if($user_count > 0){
			$info_arr['view'] = 'crt_registration_on_crisesflo_2';
		}


		$other_info = array(
			'first_name'=>$cct_firstname,
			'last_name'=>$cct_lastname,
			'login_id'=>$onetime_key
		);

		$this->email_sending->sendmail($info_arr,$other_info);

		//get validation message from admin
		$action_message_id = '55';
		$action_message = $this->common_model->get_message($action_message_id);

		$input_array['message'] = $action_message['success'];
		$input_array['result'] = 'OK';
		$input_array['aa_crt_number'] = $crt_no;
		
		return $input_array;
	
	}

	public function onlines($cc_id, $login_id, $org_id){

		$this->db->order_by("last_activity", "desc"); 
		$this->db->distinct();
		$this->db->group_by('user_data');
		$onlines = $this->db->get('ci_sessions');

		$the_org_crts = $this->common_model->get_org_crts($cc_id, $org_id, '5', 'add_cc'); //

		$online_arr = array();
		$mycontacts = array();
		//$this->master_model->getRecords('ci_sessions');
		if ($onlines->num_rows() > 0){
			$ccid = $cc_id; //$this->session->userdata('team_cc');
			$check_duplicate = array();
		
			foreach ($onlines->result() as $value){
			
				$userdata = unserialize($value->user_data);
				
				$idle = time() - $value->last_activity;
			  
				$dele = time() - 7200;
				$this->db->where('last_activity <', $dele);
				$this->db->delete('ci_sessions'); 

				
				
			  if (isset($userdata['logged_display_name']) && isset($userdata['team_cc']) && $userdata['team_cc'] == $ccid){
				  
				if( isset($userdata['cc_selected_orgnaization']) && $org_id){
				  
					if(in_array($userdata['my_id'], $check_duplicate) == false && $userdata['my_id'] != $login_id){
					
						$mycontacts[] = array(
							'status'=>'online',
							'name'=>$userdata['logged_display_name']
						);
						$online_arr[] = (isset($userdata['logged_crt_login_id'])) ? $userdata['logged_crt_login_id'] : $userdata['logged_cc_login_id'];
						
//							  echo '<li><i class="fa fa-circle text-green"></i> ';
//								  echo $userdata['logged_display_name'];
//							  echo '</li>';
						array_push($check_duplicate, $userdata['my_id']);
						  
					}
				 }
			  }
			  
			  if(count($check_duplicate) == '0'){
					//echo '<span class="text-muted">No online users.</span>';
			  }
			  
		  }
		}
		
		$allusers = array();
		//get users to display
		if(count($the_org_crts) > 0){
			foreach($the_org_crts as $r=>$value){
				$value['status'] = 'offline';
				
				if(in_array($value['login_id'], $online_arr)){
					$value['status'] = 'online';
				}
				
				if($value['login_id'] != $login_id){
					$allusers[] = $value;
				}
			}
		}
		
		$sortedusers = array();
		
		if(count($allusers) > 0){
			$sortedusers = $this->array_orderby($allusers, 'status', SORT_DESC, 'full_name', SORT_ASC);
		}
		
		//get team conversations
		$chat = $this->process_chat($org_id, 'update');
		$thechat = array();
		if(count($chat['text']) > 0){
			foreach($chat['text'] as $r=>$value){
				$chatt = array(
					'text'=>$value,
					'userId'=>'12345',
					'time'=>''
				);
				
				$thechat[] = $chatt;
			}
		}
		
		
		
		
		$arr = array(
			'online_id'=>$online_arr,
			'messages'=>$thechat,
			'online'=>$sortedusers
		);
		return $arr;

	}
	

	public function array_orderby(){
		$args = func_get_args();
		$data = array_shift($args);
		foreach ($args as $n => $field) {
			if (is_string($field)) {
				$tmp = array();
				foreach ($data as $key => $row)
					$tmp[$key] = $row[$field];
				$args[$n] = $tmp;
				}
		}
		$args[] = &$data;
		call_user_func_array('array_multisort', $args);
		return array_pop($args);
	}		
	
	
	//process chat 
	public function process_chat($org_id, $actiontype = 'update', $chat = NULL) {

		$function = $actiontype;
		$org = $org_id;
		
		
		$log = array();
		
		$org_room = 'chat_room/org';
		$org_room .= $org;
		$org_room .= '.txt';
		
		switch($function) {
		
			 case('getState'):
				 if(file_exists($org_room)){
				   $lines = file($org_room);
				 }
				 $log['state'] = count($lines); 
				 break;	
			
			 case('update'):
				$state = 0; //$_POST['state'];
				if(file_exists($org_room)){
				   $lines = file($org_room);
				 }
				 $count =  count($lines);
				 if($state == $count){
					 $log['state'] = $state;
					 $log['text'] = false;
					 
					 }
					 else{
						 $text= array();
						 $log['state'] = $state + count($lines) - $state;
						 foreach ($lines as $line_num => $line)
						   {
							   if($line_num >= $state){
							 $text[] = str_replace("\n", "", $line);
							   }
			 
							}
						 $log['text'] = $text; 
					 }
				  
				 break;
			 
			 case('send'):
//				$nickname = htmlentities(strip_tags($_POST['nickname']));
//				
//				$nowsenttime = time();
//				$timezone = $this->session->userdata('timezone');
//				$thesenttime = gmt_to_local($nowsenttime, $timezone, FALSE); //(timestamp, timezone, daylight_saving)
//				
//				//$time_sent = gmdate("g:i:sa d/m/Y", $thesenttime);
//				$time_sent = gmdate("g:i:a", $thesenttime); //g:i:sa d/m/Y
//				$time_sent_hover_title = gmdate("g:i:sa d/m/Y", $thesenttime); //g:i:sa d/m/Y
//			
//				$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
//				$message = htmlentities(strip_tags($_POST['message']));
//				
//			 if(($message) != "\n"){
//				
//				 if(preg_match($reg_exUrl, $message, $url)) {
//					$message = preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $message);
//					} 
//				 
//				
//				 fwrite(fopen($org_room, 'a'), "<span>". $nickname . "</span>" . $message = str_replace("\n", " ", $message) . "<br><span class='pull-right small chat_date' title='".$time_sent_hover_title."'>" . $time_sent . "</span>" . "\n"); 
//			 }
//				 break;

				 fwrite(fopen($org_room, 'a'), $chat . "\n"); 

			 
				 $log['status'] = 'sent'; 
				 break;
			
		}
		
		//echo json_encode($log);	
		$log['org_room'] = $org_room;
		return $log;
	}
		
	
}
?>
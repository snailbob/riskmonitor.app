<?php
class Common_model extends CI_Model
{
	public function __construct()
	{
		// parent::__construct();

		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".crisisflo.com","",$url);

		$base_link = ($account != '' && $account != 'crisisflo.com' && $account != 'localhost' && $account != 'ec2-52-23-232-34.compute-1.amazonaws.com') ? 'https://'.$url.'/' : base_url();

		if($account == 'mobile'){
			redirect('https://mobile.crisisflo.com/mob/');
		}
		else{
			$this->config->set_item('base_url', $base_link);
		}
		
	}

	public function appName(){
		return 'RiskMonitor';
	}

	public function all_plans(){
		Stripe::setApiKey("sk_test_zC4nyIzsM6pXKLXhsrJZHiB0");
		$all_plans = Stripe_Plan::all(array("limit" => 30));
		return $all_plans;
	}


	public function the_domain(){
		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".crisisflo.com","",$url);

		return ($account != '' && $account != 'crisisflo.com' && $account != 'localhost') ? $account : '';
	}

	public function base_url(){
		$the_domain = $this->the_domain();

		$base_link = ($the_domain != 'crisisflo.com' && $the_domain != 'localhost') ? 'https://'.$url.'/' : base_url();
		return $base_link;
	}

	public function bbase_url(){
		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".crisisflo.com","",$url);

		$account = ($account != 'localhost' && $this->session->userdata('domain') != '') ? $this->session->userdata('domain') : 'localhost';
		$base_link = ($account != 'crisisflo.com' && $account != 'localhost') ? 'https://'.$account.'.crisisflo.com/' : base_url();
		return $base_link;
	}



	public function incidentname($incident_id){
		$active_module  = substr($this->session->userdata('org_module'),0,1);

		$whr_inci = array(
			'id'=>$incident_id
		);

		if($active_module == '5'){
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
		}
		else{
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
		}

		if(count($incidents) > 0){
			$incident_name = $incidents[0]['incident_no'].': '.$incidents[0]['incident_name'];
		}
		else{
			$incident_name = 'Unknown Incident';
		}

		return $incident_name;

	}


	public function get_main_status($cc_id,$org_id)
	{
		$this->db->select('count(*) as num_scenario');
		$this->db->where('scenario_status','1');
		$this->db->where('cc_id',$cc_id);
		$this->db->where('org_id',$org_id);
		$main_status_rst=$this->db->get('cf_scenario');
		$row=$main_status_rst->row();

		if($row->num_scenario > 0 )
		{$status='Response Phase';}
		else
		{$status='Pre-Incident Phase';}
		return $status;
	}

	public function recall_main_status(){
		$status = 'ok';

		//change view if pre incident
		//active modules
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');


		$main_status = $this->get_main_status($cc_id,$org_id);

		$main_active_module = substr($this->session->userdata('org_module'), 0,1);
		$active_feature_modules = $this->active_module($org_id);

		$active_module =  $main_active_module.$active_feature_modules;

		$the_active_module = array('0'=>array('active_module'=>$active_module));


		$recall = $this->master_model->getRecords('cf_recall',array('cc_id'=>$cc_id,'org_id'=>$org_id,'closed'=>'0'),'*',array('id'=>'DESC'));


		$mymodyul = substr($this->session->userdata('org_module'),0,1);
		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));

		if(($main_status=="Pre-Incident Phase" && strpos($the_active_module[0]['active_module'], '1') !== false) || (count($recall) == 0 && strpos($the_active_module[0]['active_module'], '5') !== false) || (count($recall) == 0 && strpos($the_active_module[0]['active_module'], '8') !== false)){ // || (count($casereport) == 0  && strpos($the_active_module[0]['active_module'], '2') !== false)

			if( $num_group==0 || $num_crt==0 || $num_stk==0 || $num_plan==0 || count($cc_document)==0 || $num_standby_message==0 || $num_reminder==0){

				$status = 'setup';

			}
		}

		return $status;
	}

	public function get_closest($search, $arr) {

	   $the_closest_id = array();

	   for($i = 0; $i < count($arr); $i++){

		   $closest_lat = null;
		   $closest_lng = null;
		   $the_closest = '';

		   foreach($arr as $r=>$item) {


			   if($item['user_status'] == '1'){

				   if(!in_array($item['login_id'], $the_closest_id)){

					  if($closest_lat == null || abs($search['location_lat'] - $closest_lat) > abs($item['location_lat'] - $search['location_lat']) && $closest_lng == null || abs($search['location_lng'] - $closest_lng) > abs($item['location_lng'] - $search['location_lng'])) {
						 $closest_lat = $item['location_lat'];
						 $closest_lng = $item['location_lng'];
						 $the_closest = $item['login_id'];
					  }
				   }//check if already filtered

			   }//check if verified

		   }

		   $the_closest_id[] = $the_closest;
	   }

	   return $the_closest_id;
	}

	public function plan_max_user(){
		$plan = $this->session->userdata('billing');
		$max_user = 5;

		if($plan){
			if($plan['selected_plan'] == '1'){
				$max_user = 10;
			}
			if($plan['selected_plan'] == '2'){
				$max_user = 30;
			}
		}

		return $max_user;
	}

	public function plan_price(){
		$plan = $this->session->userdata('billing');
		$price = 0;

		if($plan){
			if($plan['selected_plan'] == '1'){
				$price = 49;
			}
			if($plan['selected_plan'] == '2'){
				$price = 79;
			}
		}

		return $price;
	}

	public function format_billing($user_id){
		$whr = array(
			'user_id'=>$user_id
		);
		$stripe_customers = $this->master_model->getRecords('stripe_customers', $whr, '*', array('id'=>'DESC'));

		$arr = null;
		if(!empty($stripe_customers)){
			$card_info = unserialize($stripe_customers[0]['card_info']);
			$form_info = unserialize($stripe_customers[0]['form_info']);
			$arr = $form_info;
			$arr['number'] = '************'.$card_info['last4'];
			$arr['last4'] = $card_info['last4'];
		}

		return $arr;
	}

	public function format_messages($id = null, $user_info = array()){
		$cc_id = (isset($user_info['cc_id'])) ? $user_info['cc_id']: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($user_info['org_id'])) ? $user_info['org_id']: $this->session->userdata('cc_selected_orgnaization');

	
		$whr = array(
			'org_id'=>$org_id
		);

		$group_messages = $this->master_model->getRecords('group_messages', $whr);

		$arr = array();


		if(!empty($group_messages)){
			foreach($group_messages as $r => $value) {
				$value['date_format'] = date_format(date_create($value['created_at']), 'm/d/Y');
				$value['time_sent'] = date_format(date_create($value['created_at']), 'h:i A');
				$value['owner'] = ($value['user_id'] == $cc_id) ? 'me' : 'not_me';
				$value['author'] = $this->format_crt($value['user_id']);

				$whr = array(
					'message_id'=>$value['id']
				);

				$files = $this->format_gm_files($whr);

				$value['files'] = $files;
				$arr[] = $value;
			}
		}

		return $arr;
		
	}

	public function format_crt($user_id){
		$whr = array(
			'login.login_id'=>$user_id
		);

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');

		$crt = $this->master_model->getRecords('cf_login_master as login', $whr);

		// $crt = $this->master_model->getRecords('cf_crisis_response_team', $whr);
		// $crt2 = $this->master_model->getRecords('cf_login_master', $whr);
		$the_crt = null;

		if(!empty($crt)){
			foreach($crt as $r => $value) {
				$user_status = 'Awaiting Acceptance';
				if($value['user_status'] == 1){
					$user_status = 'Accepted';

				}

				$sms_received =  'Yes';
				if ($value['received_sms'] == 0){
					$sms_received =  'No';
				}

				$availability_status = $this->availability($value['login_id']);
				
				$u = $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']);

				$e = $this->master_model->decryptIt($value['crt_email']);
				$value['avatar'] = ($value['avatar'] != '') ? $value['avatar'] : 'http://via.placeholder.com/100x100?text=CF+User';

				$timezone = $this->master_model->getRecords('time_zone', array('ci_timezone'=>$value['timezone']));
				$country_code = $this->master_model->getRecords('country_t', array('calling_code'=>$this->master_model->decryptIt($value['countrycode'])));
				
				$billing_contact = (!empty($value['billing_contact'])) ? unserialize($value['billing_contact']) : null;

				$setup_status_completed = $this->common_model->setup_status_completed($value['login_id'], $value['default_org']);

				$setup_status_details = $this->common_model->setup_status_details($value['login_id'], $value['default_org']);
				
				$trial_days = $this->trial_days($value['login_id']);
				
				// if (strpos($value['module'], substr($active_module, 0, 1)) !== false){
					$the_crt = array(
						'login_id'=>$value['login_id'],
						'org_id'=>$value['default_org'],
						'setup_status_completed'=>$setup_status_completed,
						'setup_status_details'=>$setup_status_details,
						'first_name'=>$this->master_model->decryptIt($value['crt_first_name']),
						'last_name'=>$this->master_model->decryptIt($value['crt_last_name']),
						'full_name'=>($u != ' ') ? $u : $e,
						'avatar'=>$value['avatar'],
						'crt_email'=>$e,
						'crt_mobile'=>$this->master_model->decryptIt($value['crt_mobile']),
						'countrycode'=>$this->master_model->decryptIt($value['countrycode']),
						'crt_digits'=>$this->master_model->decryptIt($value['crt_digits']),
						'country_id'=>$value['country_id'],
						'countrycode_details'=>(!empty($country_code)) ? $country_code[0] : null,
						'country_details'=>(!empty($country_code)) ? $country_code[0] : null,
						'crisis_function'=>$value['crisis_function'],
						'alt_member'=>$value['alt_member'],
						'location'=>$value['location'],
						'location_lat'=>$value['location_lat'],
						'location_lng'=>$value['location_lng'],
						'crt_position'=>$value['crt_position'],
						'user_level'=>$value['user_level'],
						'user_type'=>($value['user_level'] == '1') ? 'User' : 'Administrator',
						'subscription_cancelled'=>$value['subscription_cancelled'],
						'user_status'=>$value['user_status'],
						'billing_contact'=>$billing_contact,
						'gplus_id'=>$value['gplus_id'],
						'linkedin_id'=>$value['linkedin_id'],
						'user_status_name'=>$user_status,
						'date_profile_activated'=>$value['date_profile_activated'],
						'tasks_review_date'=>$value['tasks_review_date'],
						'sms_received'=>$sms_received,
						'availability_status'=>$availability_status,
						'trial_days'=>$trial_days,
						'timezone'=>$value['timezone'],
						'stripe_customer_id'=>$value['stripe_customer_id'],
						'timezone_details'=>(!empty($timezone)) ? $timezone[0] : null

					);

					if($value['date_deleted'] != '0000-00-00 00:00:00'){
						$the_crt['first_name'] = '';
						$the_crt['last_name'] = '';
						$the_crt['crt_email'] = '';
						$the_crt['crt_mobile'] = '';
						$the_crt['crt_digits'] = '';
						$the_crt['countrycode'] = '';
						$the_crt['crisis_function'] = '';
						$the_crt['location'] = '';
						$the_crt['crt_position'] = '';
						
					}
				// }
			}
		}

		return $the_crt;
	}

	public function get_billing_contact($cc_id){
		$whr = array(
			'login_id'=>$cc_id
		);

		$crt = $this->master_model->getRecords('cf_crisis_response_team', $whr);
		$arr = null;
		if(!empty($crt)){
			$arr = (!empty($crt[0]['billing_contact'])) ? unserialize($crt[0]['billing_contact']) : null;
		}

		return $arr;

	}

	public function get_org_crts($cc_id,$org_id, $active_mod = NULL, $include_cc = NULL){
		// $whr = array(
		// 	'cc_id'=>$cc_id,
		// 	'org_id'=>$org_id
		// );

		$whr_not = array(
			'cc_id !='=>$cc_id,
			'org_id !='=>$org_id
		);

		// $this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');

		// $this->db->order_by('cf_crisis_response_team.login_id','DESC');

		// $all_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr);
		

		$whr = array(
			'login.default_org'=>$org_id,
			'login.date_deleted'=>'0000-00-00 00:00:00',
			'user_status'=>'1'
		);

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');
		$all_crt = $this->master_model->getRecords('cf_login_master as login', $whr);

		
		$all_notmy_crt = $this->master_model->getRecords('cf_crisis_response_team',$whr_not);

		$active_module = $this->session->userdata('org_module');

		if(!is_null($active_mod)){
			$active_module = $active_mod;
		}


		$the_crts = array();


		$whr_cc = array(
			'login_id'=>$cc_id
		);

		$the_cc_master = $this->master_model->getRecords('cf_login_master',$whr_cc);

		if(!is_null($include_cc)){ //add cc info
			$the_cc = $this->master_model->getRecords('cf_crisis_response_team',$whr_cc);


			if(false){ //count($the_cc) > 0){
				foreach($the_cc as $r => $value) {
					$user_status = 'Awaiting Acceptance';
					$sms_received =  'No';
					if($the_cc_master[0]['user_status'] == 1){
						$user_status = 'Accepted';
						if ($the_cc_master[0]['received_sms'] == 1){

							$sms_received =  'Yes';
						}

					}

					$availability_status = $this->availability($value['login_id']);

					$fname = $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']);

					$email_to_display = $this->master_model->decryptIt($value['crt_email']); //($include_cc == 'assign') ? 'Assign to me' : ;
					
					$fname = ($fname == ' ') ? 'No name ('.$email_to_display.')' : $fname;
					
					$value['avatar'] = ($value['avatar'] != '') ? $value['avatar'] : 'http://via.placeholder.com/100x100?text=CF+User';

					$the_crts[] = array(
						'login_id'=>$value['login_id'],
						'first_name'=>$this->master_model->decryptIt($value['crt_first_name']),
						'last_name'=>$this->master_model->decryptIt($value['crt_last_name']),
						'crt_first_name'=>$this->master_model->decryptIt($value['crt_first_name']),
						'crt_last_name'=>$this->master_model->decryptIt($value['crt_last_name']),
						'full_name'=>$fname,
						'avatar'=>$value['avatar'],
						'crt_email'=>$email_to_display,
						'email'=>$this->master_model->decryptIt($value['crt_email']),
						'crt_mobile'=>$this->master_model->decryptIt($value['crt_mobile']),
						'country_id'=>$value['country_id'],
						'countrycode'=>$this->master_model->decryptIt($value['countrycode']),
						'crt_digits'=>$this->master_model->decryptIt($value['crt_digits']),
						'group'=>(isset($value['group'])) ? $value['group'] : '',
						'crisis_function'=>$value['crisis_function'],
						'alt_member'=>$value['alt_member'],
						'location'=>$value['location'],
						'location_lat'=>$value['location_lat'],
						'location_lng'=>$value['location_lng'],
						'crt_position'=>$value['crt_position'],
						'user_status'=>$the_cc_master[0]['user_status'],
						'user_status_name'=>$user_status,
						'date_profile_activated'=>$the_cc_master[0]['date_profile_activated'],
						'sms_received'=>$sms_received,
						'availability_status'=>$availability_status

					);

				}
			}

		} //end add cc info





		if(count($all_crt) > 0){
			foreach($all_crt as $r => $value) {
				$user_status = 'Awaiting Acceptance';
				if($value['user_status'] == 1){
					$user_status = 'Accepted';

				}

				$sms_received =  'Yes';
				if ($value['received_sms'] == 0){
					$sms_received =  'No';
				}

				$availability_status = $this->availability($value['login_id']);

				$fname = $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']);

				$email_to_display = $this->master_model->decryptIt($value['crt_email']); //($include_cc == 'assign') ? 'Assign to me' : ;
				$fname = ($fname == ' ') ? 'No name ('.$email_to_display.')' : $fname;

				$value['avatar'] = ($value['avatar'] != '') ? $value['avatar'] : 'http://via.placeholder.com/100x100?text=CF+User';


				// if (strpos($value['module'], substr($active_module, 0, 1)) !== false){
					$the_crts[] = array(
						'login_id'=>$value['login_id'],
						'first_name'=>$this->master_model->decryptIt($value['crt_first_name']),
						'last_name'=>$this->master_model->decryptIt($value['crt_last_name']),
						'full_name'=>$fname,
						'avatar'=>$value['avatar'],						'crt_email'=>$email_to_display,
						'crt_mobile'=>$this->master_model->decryptIt($value['crt_mobile']),
						'crisis_function'=>$value['crisis_function'],
						'alt_member'=>$value['alt_member'],
						'location'=>$value['location'],
						'location_lat'=>$value['location_lat'],
						'location_lng'=>$value['location_lng'],
						'crt_position'=>$value['crt_position'],
						'user_status'=>$value['user_status'],
						'user_status_name'=>$user_status,
						'date_profile_activated'=>$the_cc_master[0]['date_profile_activated'],
						'sms_received'=>$sms_received,
						'availability_status'=>$availability_status

					);
				// }
			}
		}




		//get the other crts
//		$the_other_crts = 0;
//		if(count($all_notmy_crt) > 0){
//			foreach($all_notmy_crt as $c=>$rt){
//				$usertype = $this->master_model->getRecords('cf_login_master', array('login_id'=>$rt['login_id']));
//
//				$user_status = 'Awaiting Acceptance';
//				if($usertype[0]['user_status'] == 1){
//					$user_status = 'Accepted';
//					if ($usertype[0]['received_sms'] == 0){
//						$sms_received =  'No';
//					}
//					else{
//						$sms_received =  'Yes';
//					}
//
//				}
//
//
//
//				if($usertype[0]['user_level'] == '1'){ //check if a crt
//					$crt_orgs = unserialize($rt['org_list']);
//
//					if(count($crt_orgs) > 0){
//						//echo $active_module;
//
//						$count_module_exist = 0;
//						foreach($crt_orgs as $crt_row=>$crt_val){
//							//echo $crt_val['active_module'];
//							if(substr($active_module, 0, 1) == $crt_val['active_module'] && $org_id == $crt_val['org_id']){
//								$the_other_crts++;
//
//								$availability_status = $this->availability($rt['login_id']);
//
//								$xthe_crts = array();
//								$xthe_crts[] = array(
//									'login_id'=>$rt['login_id'],
//									'first_name'=>$this->master_model->decryptIt($rt['crt_first_name']),
//									'last_name'=>$this->master_model->decryptIt($rt['crt_last_name']),
//									'full_name'=>$this->master_model->decryptIt($rt['crt_first_name']).' '.$this->master_model->decryptIt($rt['crt_last_name']),
//									'crt_email'=>$this->master_model->decryptIt($rt['crt_email']),
//									'crt_mobile'=>$this->master_model->decryptIt($rt['crt_mobile']),
//									'crisis_function'=>$rt['crisis_function'],
//									'alt_member'=>$rt['alt_member'],
//									'location'=>$rt['location'],
//									'location_lat'=>$rt['location_lat'],
//									'location_lng'=>$rt['location_lng'],
//									'crt_position'=>$rt['crt_position'],
//									'user_status'=>$usertype[0]['user_status'],
//									'user_status_name'=>$user_status,
//									'sms_received'=>$sms_received,
//									'availability_status'=>$availability_status
//
//								);
//
//							}
//						}
//					}
//				}
//			}
//		}

		$total_crt_count = count($all_crt); // + $the_other_crts;

		return $the_crts;

	}


	public function get_task_approvers($task_id, $user_level = NULL, $active_mod = NULL){



		$active_module = substr($this->session->userdata('org_module'), 0 ,1);

		if(!is_null($active_mod)){
			$active_module = $active_mod;
		}

		if($active_module == '5'){
			$the_task = $this->master_model->getRecords('cf_recall_steps', array('id'=>$task_id));
		}

		else{
			$the_task = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$task_id));
		}


		if(count($the_task) == 0){
			return array();
			return false;
		}//return empty array


		foreach($the_task as $q=>$kyu){

			$the_approvers = unserialize($kyu['approvers']);
			$the_approvers_status = unserialize($kyu['approvers_status']);
			$approvers_count = 0;
			$approvers_status = 0;

			$theapprovers = array();

			$crt_assigned = $kyu['assigned'];
			$crt_date_assigned = ' ('.date_format(date_create($kyu['date_assigned']), 'jS F Y, g:ia').')';
			$crt_approve_btn = '';

			$crt_assigned_match = 'yes';

		//	if($crt_assigned == $this->session->userdata('logged_cc_login_id')){
		//		$crt_assigned_match = 'yes';
		//	}

			$crt_view_approvers_btn = '<li class="crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'" style="cursor: pointer;"><a>Task Approvers</a></li>';

			if(is_array($the_approvers)){
				$approvers_count = count($the_approvers);
			}
			else{
				$the_approvers = array();
			}

			if(is_array($the_approvers_status)){
				$approvers_status = count($the_approvers_status);
			}
			else{
				$the_approvers_status = array();
			}

			//get approvers list
			$approvers_name = array();
			$unapproved_count = count($the_approvers);

			if(count($the_approvers) > 0){
				foreach($the_approvers as $tapp){
					$user_approved = '';
					if(in_array($tapp, $the_approvers_status)){
						$user_approved = ' - Approved';
						$unapproved_count--;
					}

					$approvers_name[] = array(
						'id'=>$tapp,
						'name'=>$this->getname($tapp),
						'status'=>$user_approved
					);

				}
			}

			$theapprovers = $approvers_name;

			$data['total_approvers'] = $approvers_count;
			$data['unapproved_count'] = $unapproved_count;
			$data['theapprovers'] = $theapprovers;
			return $data;


		}



	}




	public function markapproved($task_id, $user_id, $active_module = NULL ){
//		$task_id = $_POST['task_id'];
//		$user_id = $this->session->userdata('my_id');

		$data['task_id'] = $task_id;
		$data['user_id'] = $user_id;
		$data['active_module'] = $active_module;

		$active_mod = substr($this->session->userdata('org_module'),0,1);

		if(!is_null($active_module)){
			$active_mod = $active_module;
		}


		$my_approvers_info = $this->get_task_approvers($task_id, NULL, $active_mod);


		if($my_approvers_info['unapproved_count'] == 1){



			$the_new_status = '1';

			//active module is recall
			if($active_mod == '5'){
				$task = $this->master_model->getRecords('cf_recall_steps',array('id'=>$task_id));

				$rc = $this->master_model->getRecords('cf_recall',array('id'=>$task[0]['recall_id']));
			}
			else{
				$task = $this->master_model->getRecords('cf_continuity_steps',array('id'=>$task_id));

				$rc = $this->master_model->getRecords('cf_continuity',array('id'=>$task[0]['recall_id']));
			}


			//get the date completed
			$initiation_date = new DateTime($rc[0]['initiation_date']);


			$user_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$user_id));
			$nowtime = $this->usertime($user_info[0]['timezone']);


			$task_completed = new DateTime($nowtime);
			$difference = $initiation_date->diff($task_completed);

			$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i;


			$task_tot = explode(' ',$interval);
			$unit_time = array(
				'Y : ',
				'Mo : ',
				'D : ',
				'H : ',
				'M' // : ',
			//	'S'
			);
			$u = 0;
			$time_lapse = '';
			foreach($task_tot as $tot){
				if ($tot == 0 ){
				}
				else if ($tot == 1 ){
					$time_lapse .= $tot.' '.$unit_time[$u].' ';
				}
				else{
					$time_lapse .= $tot.' '.$unit_time[$u].' ';
				}
				$u++;
			}


			$arr = array(
				'status'=>$the_new_status,
				'date_completed'=>$nowtime,
				'time_lapse'=>$time_lapse

			);


			//active module is recall
			if($active_mod == '5'){
				$this->master_model->updateRecord('cf_recall_steps',$arr,array('id'=>$task_id));

			}
			else{
				$this->master_model->updateRecord('cf_continuity_steps',$arr,array('id'=>$task_id));
			}


		}//end of mark completed if one approver is remaining


		//active module is recall
		if($active_mod == '5'){
			$db_name = 'cf_recall_steps';
			$task = $this->master_model->getRecords('cf_recall_steps', array('id'=>$task_id));

		}
		else{
			$db_name = 'cf_continuity_steps';
			$task = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$task_id));
		}


		//update approvers status
		$the_approvers = unserialize($task[0]['approvers']);
		$the_approvers_status = unserialize($task[0]['approvers_status']);
		$approvers_count = 0;
		$approvers_status = 0;

		$new_task_owner = '';




		if(is_array($the_approvers)){
			$approvers_count = count($the_approvers);
		}
		else{
			$the_approvers = array();
		}

		if(is_array($the_approvers_status)){
			$approvers_status = count($the_approvers_status);
		}
		else{
			$the_approvers_status = array();
		}


		array_push($the_approvers_status, $user_id);

		//$the_approvers_status[] = $user_id;

		$arr = array(
			'approvers_status'=>serialize($the_approvers_status)
		);

		$this->master_model->updateRecord($db_name, $arr, array('id'=>$task_id));


		//get new task owner
		$status_text = 'Awaiting Approval';
		$status_class = 'text-warning';

		//array_diff — Computes the difference of arrays
		$result = array_diff($the_approvers, $the_approvers_status);
		$data['countapprr'] = count($result);
		if(count($result) > 0){
			$new_arr = array();
			foreach($result as $rs){
				$new_arr[] = $rs;
			}
			$new_task_owner = $this->getname($new_arr[0]);
		}


		else{
			$status_text = 'Completed';
			$status_class = 'text-success';

			$new_task_owner = $this->getname($task[0]['assigned']);

		}

		$data['new_task_owner'] = $new_task_owner;
		$data['status_text'] = $status_text;
		$data['status_class'] = $status_class;

		return $data;
	}




	public function get_waiting_crt($cc_id,$org_id)
	{

		$crts = $this->get_org_crts($cc_id,$org_id);
		$count = array();

		foreach($crts as $r=>$value){

			if($value['user_status'] == '0'){
				$count[] = $value; //$value['login_id'];
			}

		}

//		$this->db->join('cf_crisis_response_team as crt','crt.login_id=login.login_id');
//		$this->db->where('crt.cc_id',$cc_id);
//		$this->db->where('crt.org_id',$org_id);
//		$this->db->where('login.user_status','0');
//		$waiting_crt_rst=$this->db->get('cf_login_master as login');

		return $count; // $waiting_crt_rst->result();
	}

	public function get_org_documents($cc_id, $org_id, $login_id){

		$arr = array(
			'org_id'=>$org_id
		);

		$arr2 = array(
			'org_id'=>$org_id,
			'category_id'=>0
		);
		$all_doc = $this->master_model->getRecords('cf_file_upload', $arr2);
		$format_docs = $this->cf_file_upload_format($all_doc);
		$user_group = $this->master_model->getRecords('user_group', $arr);

		$categories = $this->master_model->getRecords('document_category', $arr);

		$all_cat = array(
			array(
				'id'=>'0',
				'name'=>'Uncategorized',
				'group'=>array('all'),
				'docs'=>$format_docs
			)
		);

		if(count($categories) > 0){
			foreach($categories as $r=>$value){
				$arr2 = array(
					'org_id'=>$org_id,
					'category_id'=>$value['id']
				);

				$all_doc = $this->master_model->getRecords('cf_file_upload', $arr2);

				$format_docs = $this->cf_file_upload_format($all_doc);

				$group = (is_array(unserialize($value['groups']))) ? unserialize($value['groups']) : array('all');
				$all_cat[] = array(
					'id'=>$value['id'],
					'name'=>$value['name'],
					'group'=>$group,
					'docs'=>$format_docs
				);
			}
		}

		$data = array(
			'user_group'=>$user_group,
			'categories'=>$all_cat
		);

		return $data;

	}

	public function cf_file_upload_format($docs){
		$doc = array();

		if(count($docs) > 0){
			foreach($docs as $r=>$value){
				$value['file_name'] = substr($value['file_upload_name'],13,50);
				$doc[] = $value;
			}
		}

		return $doc;
	}

	public function get_org_stks($cc_id, $org_id){

		$whr = array(
			'cc_id'=>$cc_id,
			'organization_id'=>$org_id,

		);

		$the_org = $this->master_model->getRecords('organization_master',$whr);
		$stks = (count($the_org) > 0) ? unserialize($the_org[0]['stakeholders']) : array();

		$stks = (is_array($stks)) ? $stks : array();
		$arr = array();

		foreach($stks as $value) {
			$this->db->join('cf_login_master','cf_login_master.login_id=cf_stakeholder.login_id');

			$this->db->order_by('cf_stakeholder.login_id','DESC');


			$thestk = $this->master_model->getRecords('cf_stakeholder',array('cf_stakeholder.login_id'=>$value));

			if(count($thestk) > 0){
				foreach($thestk as $r=>$value){
					$value['first_name'] = $this->master_model->decryptIt($value['stk_first_name']);
					$value['last_name'] = $this->master_model->decryptIt($value['stk_last_name']);
					$value['full_name'] = $value['first_name'].' '.$value['last_name'];
					$value['email'] = $this->master_model->decryptIt($value['stk_email_address']);
					$value['countrycode'] = $this->master_model->decryptIt($value['countrycode']);
					$value['digits'] = $this->master_model->decryptIt($value['stk_digits']);
					$value['mobile'] = $this->master_model->decryptIt($value['stk_mobile_num']);
					$value['status'] = ($value['user_status'] == '0') ? 'Awaiting Acceptance' : 'Accepted';
					$arr[] = $value;
				}
			}
		}




		return $arr;

	}



	public function get_waiting_stk($cc_id,$org_id)
	{
		$this->db->join('cf_stakeholder as stake_holder','stake_holder.login_id=login.login_id');
		$this->db->where('stake_holder.cc_id',$cc_id);
		$this->db->where('stake_holder.org_id',$org_id);
		$this->db->where('login.user_status','0');
		$waiting_stack_rst=$this->db->get('cf_login_master as login');
		return $waiting_stack_rst->result();
	}

	public function get_documents($cc_id, $org_id)
	{
		$this->db->where('org_id',$org_id);
		$this->db->where('cc_id',$cc_id);
		$document_rst=$this->db->get('cf_file_upload');
		return $document_rst->result_array();
  	}

	public function check_response_plan_generated($cc_id,$org_id) {
		$this->db->select('count(*)as response_plan');
		$this->db->where('org_id',$cc_id);
		$this->db->where('cc_id',$cc_id);
		$scenario_rst = $this->db->get('cf_scenario');
		$scenario_row = $scenario_rst->row();
		return $scenario_row->response_plan;
	}

	public function get_waiting_contact_crt($cc_id,$org_id) {
		$this->db->join('cf_crisis_response_team as crt','crt.login_id=login.login_id');
		$this->db->where('crt.cc_id',$cc_id);
		$this->db->where('crt.org_id',$org_id);
		$this->db->where('login.contact_verified','2');
		$waiting_crt_rst = $this->db->get('cf_login_master as login');
		return $waiting_crt_rst->result();
	}

	public function get_waiting_contact_stk($cc_id,$org_id)
	{
		$this->db->join('cf_stakeholder as stake_holder','stake_holder.login_id=login.login_id');
		$this->db->where('stake_holder.cc_id',$cc_id);
		$this->db->where('stake_holder.org_id',$org_id);
		$this->db->where('login.contact_verified','2');
		$waiting_stack_rst=$this->db->get('cf_login_master as login');
		return $waiting_stack_rst->result();
	}

	/* display all task of initiated scenarion and also CRT assign to that task */
	public function get_initiated_scenario($cc_id,$org_id)
	{
		$whr_arr=array('scn.cc_id'=>$cc_id,'scn.org_id'=>$org_id,'scn.scenario_status'=>'1');
		$order=array('scn.scenario_id'=>'ASC');
		$select="scn.*";
		$initiated_scn=$this->master_model->getRecords('cf_scenario as scn',$whr_arr,$select,$order);
		return $initiated_scn;
	}

	public function get_scenario_task($scenario_id)
	{
		$whr_arr=array('task.scenario_id'=>$scenario_id,'task.task_status'=>'1');
		$order=array('task.task_id'=>'ASC');

		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.crt_id');
		$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr,'',$order);

		return $task_list;
	}
	/*completed task taking */
	public function get_completed_scenario_task($scenario_id,$initiation_date)
	{
		$whr_arr=array('task.scenario_id'=>$scenario_id,'task.current_response'=>$initiation_date);
		$order=array('task.task_id'=>'ASC');

//		$this->db->join('cf_crisis_response_team as crt','crt.login_id=task.crt_id');
		$task_list=$this->master_model->getRecords('log_task_master as task',$whr_arr,'',$order);
		return $task_list;
	}

	/* get count of unread message to display in sidebar */
	public function get_unread_count($user_id)
	{
		$whr_arr=array('is_read'=>'0','is_receiver_del'=>'0','receiver_id'=>$user_id);
		$unread_message=$this->master_model->getRecordCount('private_messages',$whr_arr);
		return $unread_message;
	}


	public function offset_timezone($tz){
		$time_zone = $this->master_model->getRecords('time_zone');

		$i = 0;
		foreach($time_zone as $r=>$value){
			if($value['ci_timezone'] == $tz){
				$i = $r;
			}
		}

		$new_i = $i; // - 2;

		return (isset($time_zone[$new_i]['ci_timezone'])) ? $time_zone[$new_i]['ci_timezone'] : 'UP10';
	}

	public function userdatetime($login_id = NULL){

		// $nowinitiatetime = time();

		//add server time by 1m10s  = 70 seconds
		$date = date("Y-m-d H:i:s");
		$time = strtotime($date);
		$time = $time - 60; //seconds
		$date = date("Y-m-d H:i:s", $time);
		$nowinitiatetime = strtotime($date);
		
		$timezone = $this->session->userdata('timezone');
		$timezone = $this->offset_timezone($timezone);
		
		if($login_id != NULL){
			$whr_user = array('login_id'=>$login_id);
			$user_info = $this->master_model->getRecords('cf_crisis_response_team', $whr_user);

			$tz = $this->offset_timezone($user_info[0]['timezone']);
			$timezone = $this->usertime($tz);
		}

		$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, TRUE); //time, timezone, daylightsaving

		//$nowtime = date("Y-m-d h:i:s");

		$nowtime = gmdate("Y-m-d H:i:s", $theinitiatetime);

		return $nowtime;
	}

	public function usertime($timezone){

		$nowinitiatetime = time();

		$theinitiatetime = gmt_to_local($nowinitiatetime, $timezone, TRUE); //time, timezone, daylightsaving

		$nowtime = gmdate("Y-m-d H:i:s", $theinitiatetime);

		return $nowtime;
	}



	public function ago($date)
	{

		if(empty($date)) {
			return "No date provided";
		}

		$periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$lengths         = array("60","60","24","7","4.35","12","10");


		$nowtime = new DateTime($this->userdatetime());

		$now             = $nowtime->getTimestamp(); // time();
		$unix_date         = strtotime($date);

		   // check validity of date
		if(empty($unix_date)) {
			return "Bad date";
		}

		// is it future date or past date
		if($now >= $unix_date) {
			$difference     = $now - $unix_date;
			$tense         = "ago";

		} else {
			$difference     = $unix_date - $now;
			$tense         = "from now";
		}

		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}

		$difference = round($difference);

		if($difference != 1) {
			$periods[$j].= "s";
		}

		return "$difference $periods[$j] {$tense}";

	}



	//fetch name for stk
	public function getstkname($login_id)
	{
		$info = $this->master_model->getRecords('cf_stakeholder',array('login_id'=>$login_id));

		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['stk_first_name']).' '.$this->master_model->decryptIt($info[0]['stk_last_name']);

			return $crtname;
		}
		else{
			return 'Non-existing User';
		}

	}

	public function stkstatus($login_id)
	{
		$info = $this->master_model->getRecords('cf_login_master',array('login_id'=>$login_id));

		if(count($info) > 0){
			$status = $info[0]['user_status'];

			return $status;
		}
		else{
			return 'Not Found';
		}

	}

	public function forumtopics($cc_id, $org_id, $login_id){
		$last_visit = '2014-01-13 00:00:00';
		$whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			//'date_created >'=>$last_visit,
			'post_parent'=>'0'
		);

		$forum_post = $this->master_model->getRecords('forum_post_master', $whr,'*',array('date_created'=>'DESC'));

		$arr = array();

		if(count($forum_post) > 0){
			foreach($forum_post as $r=>$value){
				$data = $this->topic_wreply_format($value);
				if(isset($data[0]['fp_id'])){
					$arr[] = $data[0];
				}
			}
		}
		return $arr;
	}

	public function topic_format($topic = array()){

		if(isset($topic['fp_id'])){
			$recall_id = substr($topic['scenario_id'], 1);

			$recall = $this->master_model->getRecords('cf_recall', array('id'=>$recall_id));
			$recall_name = (count($recall) > 0) ? $recall[0]['incident_name'] : 'Unknown Incident';
			$topic['recall_name'] = $recall_name;
			$topic['recall_id'] = $recall_id;
			$topic['category_name'] = $this->forumcategory($topic['fp_category_id']);
			$topic['author_name'] = $this->getcrtname($topic['fp_author_id']);
			$topic['date_created_format'] = date_format(date_create($topic['date_created']), 'd M, Y, h:i a');
			$topic['date_updated_format'] = date_format(date_create($topic['date_updated']), 'd M, Y, h:i a');
		}

		return $topic;

	}

	public function topic_wreply_format($value = array()){
		$arr = array();

		if(isset($value['fp_id'])){
			$topic_format = $this->topic_format($value);
			$whr = array(
				'post_parent'=>$value['fp_id']
			);
			$respond = $this->master_model->getRecords('forum_post_master', $whr,'*',array('date_created'=>'ASC'));

			$responds = array();
			if(count($respond) > 0){
				foreach($respond as $res=>$resvalue){
					$res_format = $this->topic_format($resvalue);
					$responds[] = $res_format;
				}
			}

			$topic_format['responds'] = $responds;

			$arr[] = $topic_format;
		}

		return $arr;

	}

	//fetch name for crt on trash view
	public function forumcategory($category_id){
		$info = $this->master_model->getRecords('forum_category_master',array('fc_id'=>$category_id));

		if(count($info) > 0){
			$data = $info[0]['fc_name'];

			return $data;
		}
		else{
			return 'Uncategorized';
		}
	}

	//fetch name for crt on trash view
	public function getname($login_id){
		$info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);

			return $crtname;
		}
		else{
			return 'Non-existing User';
		}


	}

	//fetch name for crt on trash view
	public function getcrtname($login_id)
	{
		$info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		if(count($info) > 0){
			$crtname = $this->master_model->decryptIt($info[0]['crt_first_name']).' '.$this->master_model->decryptIt($info[0]['crt_last_name']);

			return $crtname;
		}
		else{
			return 'Non-existing User';
		}

	}

	//fetch name for crt on trash view
	public function getorgname($org_id)
	{
		$info = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		if(count($info) > 0 && $org_id != 0){

			$crtname = $info[0]['organization_name'];

			return $crtname;
		}
		else{
			return 'Organization not found';
		}

	}

	public function getcrtorgs($crt_email){

		$crt_email_encrypted = $this->master_model->encryptIt($crt_email);

		$this->db->join('cf_login_master','cf_login_master.login_id=cf_crisis_response_team.login_id');
		$crt_orgs = $this->master_model->getRecords('cf_crisis_response_team', array('cf_login_master.email_id'=>$crt_email_encrypted,'cf_login_master.user_status'=>'1'));


		$org_ids = array();
		if(count($crt_orgs) > 0){
			foreach($crt_orgs as $r=>$val){
				if(!in_array($val['org_id'], $org_ids)){
					$the_crt_orgs[] = $val;
					$org_ids[] = array(
						'org_id'=>$val['default_org'], //org_id'];
						'name'=>$this->getorgname($val['default_org'])
					);
				}

			}
		}

		return $org_ids;

	}

	public function getcrtorg_modules($crt_email, $org_id){

		$crt_email_encrypted = $this->master_model->encryptIt($crt_email);

		$crt_orgs = $this->master_model->getRecords('cf_crisis_response_team', array('crt_email'=>$crt_email_encrypted, 'org_id'=>$org_id));


		$org_modules = array();
		$org_modules_data = array();
		if(count($crt_orgs) > 0){
			foreach($crt_orgs as $r=>$val){
				$main_module = substr($val['module'], 0 ,1);
				if(!in_array($main_module, $org_modules)){
					$org_modules[] = $main_module;
					$org_modules_data[] = array(
						'modules'=>$val['module'],
						'main_module'=>$main_module,
						'module_name'=>$this->getmodulename($main_module)
					);
				}

			}
		}

		return $org_modules;


	}


	//recall cost monitor
	public function currencycode($id) {

		$currency = $this->master_model->getRecords('countries_currency',array('idCountry'=>$id));
		if(count($currency) > 0){
			return $currency[0]['currencyCode'];
		}else{
			return 'Currency Unknown';
		}
	}



	public function accumulate($recall, $active_module = NULL){


		if(count($recall) == 0){
			$myaccumulate = 'No Cost Items';
			return $myaccumulate;
		}
		else{

			$active_mod  = substr($this->session->userdata('org_module'),0,1);

			if(!is_null($active_module)){
				$active_mod  = $active_module;
			}
			//active module is recall
			if($active_mod == '5'){
				$themod = 'recall_id';
			}
			else{
				$themod = 'continuity_id';
			}


			$recall_items = $this->master_model->getRecords('cf_cost_category_item',array($themod=>$recall),'*',array('currency'=>'ASC'));

			$myaccumulate = '';
			if(count($recall_items) > 0){

				$mycurr = $recall_items[0]['currency'];
				$totalcurr = 0;
				$m = 0;

				foreach($recall_items as $it=>$ems){
					if($mycurr != $ems['currency']){

						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2).' + ';

						$mycurr = $ems['currency'];
						$totalcurr = $ems['item_cost'];


					}else{

						$totalcurr += $ems['item_cost'];
					}

					if((count($recall_items) - 1) == $m){
						$myaccumulate .= $this->currencycode($mycurr).' '.number_format($totalcurr,2);
					}

					$m++;
				}


			}else{
				$myaccumulate .= 'No Cost Items';
			}
			return $myaccumulate;

		}


	}




	//fetch name for crt on trash view
	public function getmodulename($module_id)
	{
		$info = $this->master_model->getRecords('module_master',array('id'=>$module_id));
		if(count($info) > 0){
			$name = $info[0]['mod_name'];

			return $name;
		}
		else{
			return 'Unexisting Module';
		}
	}

	public function gettaskname($task_id, $module_id){
		if($module_id == '5'){
			$task = $this->master_model->getRecords('cf_recall_steps', array('id'=>$task_id));
			if(count($task) > 0){
				$incident = $this->master_model->getRecords('cf_recall', array('id'=>$task[0]['recall_id']));
				return $task[0]['question']. ' from '.$incident[0]['incident_no'];
			}else{
				return 'Task not found!';
			}
		}
		else{
			$task = $this->master_model->getRecords('cf_continuity_steps', array('id'=>$task_id));
			if(count($task) > 0){
				$incident = $this->master_model->getRecords('cf_continuity', array('id'=>$task[0]['recall_id']));
				return $task[0]['question']. ' from '.$incident[0]['incident_no'];
			}else{
				return 'Task not found!';
			}

		}
	}

	public function availability($login_id){
		$scheds = $this->master_model->getRecords('schedules', array('login_id'=>$login_id));

		if(count($scheds) > 0){
			$available_now = 0;
			$arr = array();
			foreach($scheds as $r=>$sched){
				$start = $sched['start'];
				$end = $sched['end'];

				//format end
				if($end != ''){
					if(strlen($end) > 10){
						$end = substr($end, 0, 10). ' '.substr($end, 11, 8);
					}
				}
				else{
					$end = substr($start, 0, 10). ' 23:59:59';
				}


				//format start
				if(strlen($start) == 10){
					$start = substr($start, 0 , 10). ' 00:00:00';

				}

				else if(strlen($start) > 10){
					$start = substr($start, 0, 10). ' '.substr($start, 11, 8);
				}


				//compare the dates
				$today = $this->userdatetime($login_id);



				$the_start_date = substr($start, 0, 10);
				$the_end_date = substr($end, 0, 10);
				$today_date = substr($today, 0, 10);

				$strt_date = new DateTime($start);
				$end_date = new DateTime($end);
				$today = new DateTime($today);

				$diff = $strt_date->diff($today);
				$diff2 = $strt_date->diff($end_date);
				$strt_to_today = $diff->y * 365 + $diff->m * 30 + $diff->d + $diff->h/24; + ($diff->i / 60)/ 24; //
				$end_to_strt = $diff2->y * 365 + $diff2->m * 30 + $diff2->d + $diff2->h/24 + ($diff2->i / 60)/ 24;



				if($the_end_date == $the_start_date){
					if($the_start_date == $today_date){ //match date

						if($today > $strt_date && $today < $end_date){
							$available_now++;
							$arr[] = array('id'=>$sched['id'], 'startdiff'=>$strt_to_today, 'enddiff'=>$end_to_strt);
						}


					}
				}//end check if not a long day

				else{
					if($today > $strt_date){
						if($strt_to_today < $end_to_strt){
							$available_now++;
							$arr[] = array('id'=>$sched['id'], 'startdiff'=>$strt_to_today, 'enddiff'=>$end_to_strt, 'today'=>$today, 'start'=>$strt_date);
						}
					}


				}//end check if a long day





			}

			if($available_now > 0){
				return 'Available';
			}
			else{
				return 'Unavailable';
			}

		}
		else{
			return 'Update Availability';
		}
	}

	public function countryformat($whr = ''){

		$countries = $this->master_model->getRecords('country_t',$whr,'*', array('short_name'=>'ASC'));
		$arr = array();

		foreach($countries as $r=>$value){
			$value['short_calling'] = $value['short_name'].' (+'.$value['calling_code'].')';
			$arr[] = $value;
		}

		return $arr;
	}
	
	public function logged_info(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('my_id');

		$user_login_for_session = $this->common_model->user_login_for_session($cc_id);
		$format_crt = $this->common_model->format_crt($cc_id);
		$combined = array_merge($user_login_for_session, $format_crt);
		
		return $combined;
	}

	public function get_billing_info(){
		$cc_id = (isset($_GET['cc_id'])) ? $_GET['cc_id'] : $this->session->userdata('logged_cc_login_id');
		$format_billing = $this->common_model->format_billing($cc_id);
		$this->session->set_userdata('billing', $format_billing);
		return $format_billing;
	}

	public function bulknoti($cc_id, $org_id, $bulk_id, $active_module){
		$active_module = substr($this->session->userdata('org_module'), 0, 1);

		$org_info = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$countries = $this->countryformat();

		//check if a stripe customer
		$org_stripe = 0;
		$stripe_customer_id = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
		if($stripe_customer_id[0]['stripe_customer_id'] != '0'){
			$org_stripe = 1;
		}
		$nexmo_balance = '32'; //$this->get_nexmo_balance();


		$whr = array(
//			'cc_id'=>$cc_id,
//			'org_id'=>$org_id,
			'id'=>$bulk_id,
			//'module_id'=>$active_module
		);

		$bulk = $this->master_model->getRecords('bulk_notification', $whr, '*', array('date_added'=>'DESC'));

		$data_whr = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'bulk_id'=>$bulk[0]['id'],
		);
		$bulk_data = $this->master_model->getRecords('bulk_data', $data_whr, '*', array('id'=>'DESC'));

		$data = array(
			'page_title'=>'Manage Communication File',
			'middle_content'=>'bulk_view_manage',
			'error'=>'',
			'success'=>'',
			'bulk'=>$bulk,
			'bulk_data'=>$bulk_data,
			'countries'=>$countries,
			'org_info'=>$org_info,
			'org_stripe'=>$org_stripe,
			'nexmo_balance_round'=>round($nexmo_balance, 2),
			'nexmo_balance'=>$nexmo_balance
		);

		return $data;

	}

	public function loop_kpi($cc_id, $org_id, $recall_id, $inci_type, $incident_type){

		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);


		$whr_noti = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'type'=>$inci_type,
			'incident_id'=>$recall_id
		);

		$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);

		$email_sent_count = 0;
		$email_confirmed_count = 0;
		$sms_sent_count = 0;
		if(count($notices_sent) > 0){
			foreach($notices_sent as $r=>$ns){
				//email sent count
				$whr_email_sent = array(
					'bulk_id'=>$ns['id'],
					'email_sent'=>'1'
				);
				$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

				//email confirmed count
				$whr_email_confirmed = array(
					'bulk_id'=>$ns['id'],
					'email_confirmed'=>'1'
				);
				$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


				//sms sent count
				$whr_sms_sent = array(
					'bulk_id'=>$ns['id'],
					'sms_sent'=>'1'
				);
				$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);

				$email_sent_count += $bulk_data_email_sent;
				$email_confirmed_count += $bulk_data_email_confirmed;
				$sms_sent_count += $bulk_data_sms_sent;

			}

		}

		//get notification kpi
		$noti_kpi = 0;
		if($email_sent_count != 0){
			$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
		}


		//$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);

		$whr_kpis = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'incident_id'=>$recall_id,
			'incident_type'=>$incident_type
		);

		$kpis = $this->master_model->getRecords('bulk_kpi', $whr_kpis, '*', array('date_added'=>'DESC'));


		//set kpi variables
		$all_kpi = array();

		$all_kpi[] = array(
			'type'=>'comp_kpi',
			'dbcell'=>'comp_data',
			'bgcolor'=>'orange',
			'name'=>'Units under Company control'
		);

		$all_kpi[] = array(
			'type'=>'dist_kpi',
			'dbcell'=>'dist_data',
			'bgcolor'=>'blue',
			'name'=>'Units in distribution'
		);

		$all_kpi[] = array(
			'type'=>'market_kpi',
			'dbcell'=>'market_data',
			'bgcolor'=>'green',
			'name'=>'Units in consumer market'
		);


		$the_kpi_dash = array();

		foreach($all_kpi as $r=>$value){
			$kpi_cell = $value['dbcell'];

			$kpi_cell = $value['dbcell'];

			$value['akpiclass'] = '';
			$value['akpistyle'] = '';

			if($value['type'] != 'comp_kpi') {
				$value['akpiclass'] = 'text-primary';
				$value['akpistyle'] = 'cursor: pointer;';

				if($value['type'] == 'dist_kpi'){
					$value['akpiclass'] .=" dist_toggle_btn";
				}
			}

			$value['total_unit'] = '';
			$value['unit_return'] = '';
			$value['u_disposed'] = '';
			$value['u_transformed'] = '';
			$value['u_corrections'] = 'NA';
			$value['the_kpi'] = '0';
			$value['u_date_update'] = '';
			$value['u_threshold'] = '0';



			//check count of kpi
			if(count($kpis) > 0) {
				$kpi_id = $kpis[0]['id'];
				$dbcell = unserialize($kpis[0][$value['dbcell']]);
				if($dbcell != ''){ //get each kpi db cell data
					$value['total_unit'] = number_format($dbcell['total_unit']);
					$value['unit_return'] = number_format($dbcell['unit_return']);
					$value['u_disposed'] = number_format($dbcell['u_disposed']);
					$value['u_transformed'] = number_format($dbcell['u_transformed']);
					$value['u_corrections'] = number_format($dbcell['u_corrections']);
					$value['the_kpi'] = round($dbcell['the_kpi'], 0);
					$value['u_date_update'] = $dbcell['date_updated'];
					$value['u_threshold'] = round($dbcell['threshold'], 0);
				}
			}



			$value['kpi_status'] = '<i class="fa fa-circle text-red pull-right" data-toggle="tooltip" title="Status: Not OK" style="font-size: 160%;"></i>';
			$value['kpi_status_text'] = 'Not OK';

			if($value['the_kpi'] >= $value['u_threshold']){
				$value['kpi_status'] = '';
				$value['kpi_status_text'] = 'OK';
			}



			$the_kpi_dash[] = $value;

		}



		$inboundnoti['name'] = 'Inbound customer contacts';
		$inboundnoti['contact_cntr'] = '';
		$inboundnoti['kpi_email'] = '';
		$inboundnoti['kpi_website'] = '';
		$inboundnoti['kpi_trading'] = '';


		if(count($kpis) > 0) {
			if($kpis[0]['contact_cntr'] !=''){
				$inboundnoti['contact_cntr'] = number_format($kpis[0]['contact_cntr']);
				$inboundnoti['kpi_email'] = number_format($kpis[0]['kpi_email']);
				$inboundnoti['kpi_website'] = number_format($kpis[0]['kpi_website']);
				$inboundnoti['kpi_trading'] = number_format($kpis[0]['kpi_trading']);

			}
		}

		$outboundnoti['name'] = 'Outbound notifications';
		$outboundnoti['noti_kpi'] = $noti_kpi;
		$outboundnoti['email_sent_count'] = $email_sent_count;
		$outboundnoti['email_confirmed_count'] = $email_confirmed_count;
		$outboundnoti['sms_sent_count'] = $sms_sent_count;



		//$data['all_kpi'] = $all_kpi;
		$data['all_kpi'] = $the_kpi_dash;
		$data['outbound_notification'] = $outboundnoti;
		$data['inbound_notification'] = $inboundnoti;

		return $data;
	}



	public function cost_monitor($cc_id, $org_id, $recall_id){

		$wher_cost_cat = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'recall_id'=>$recall_id
		);

		$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));

		$the_cost_data = array();

		if(count($cost_category) > 0){

			foreach($cost_category as $r=>$category){
				$cost_id = $category['cost_id'];
				$items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$cost_id, 'recall_id'=>$recall_id));

				$the_items = array();
				if(count($items) > 0){
					foreach($items as $r=>$value){
						$value['currency_name'] = $this->currencycode($value['currency']);
						$the_items[] = $value;
					}
				}


				$category['items'] = $the_items;

				$the_cost_data[] = $category;
			}
		}


		return $the_cost_data;
	}

	public function blockers($cc_id, $recall_id){

		//get blockers
		$bb = array(
			'cc_id'=>$cc_id,
			'recall_id'=>$recall_id
		);


		$blockers = $this->master_model->getRecords('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));

		$the_blockers = array();

		if(count($blockers) > 0){
			foreach($blockers as $r=>$value){
				$value['status_name'] = 'Unresolved';
				if($value['status'] == '1'){
					$value['status_name'] = 'Resolved';
				}

				$value['raised_by'] = $this->getname($value['crt_id']);

				$the_blockers[] = $value;
			}
		}

		return $the_blockers;

	}

	public function taskname($task_id, $db_name){
		$task = $this->master_model->getRecords($db_name, array('id'=>$task_id));
		$task_name = '';
		if(count($task) > 0){
			$task_name = ' [ Step #'.($task[0]['step_no'] - 1).' ] '.$task[0]['question'];
		}

		return $task_name;
	}

	public function getdoc_cat($category_id){
		$categories = $this->master_model->getRecords('document_category', array('id'=>$category_id));
		if(count($categories) > 0){
			return $categories[0]['name'];
		}
		else{
			return 'No category';
		}
	}
	


	public function format_group($groups){
		$arr = array();
		if(count($groups) > 0){
			foreach($groups as $r=>$value){
				$a = array(
					array(
						'name'=>'KPI Dashboard',
						'type'=>'kpi',
						'enabled'=>false
					),
					array(
						'name'=>'Cost Monitor',
						'type'=>'cost',
						'enabled'=>false
					),
					array(
						'name'=>'Issues Board',
						'type'=>'issues',
						'enabled'=>false
					)
				);

				$access = (($value['access'] != '')) ? unserialize($value['access']) : array();
				if(count($access) > 0){
					foreach($access as $acc_r=>$acc_value){
						if($acc_value == 'kpi'){
							$a[0]['enabled'] = true;
						}
						if($acc_value == 'cost'){
							$a[1]['enabled'] = true;
						}
						if($acc_value == 'issues'){
							$a[2]['enabled'] = true;
						}
					}
				}

				$value['access_format'] = $a;
				$value['access_arr'] = $access;

				$crts = $this->master_model->getRecords('cf_crisis_response_team',array('group_id'=>$value['id']));


				$value['crts'] = $this->cf_crisis_response_team_format($crts);


				$arr[] = $value;
			}

		}

		return $arr;
	}

	public function cf_crisis_response_team_format($crts){
		$arr = array();
		if(count($crts) > 0){
			foreach($crts as $r=>$value){
				$value['name'] = $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']);
				$value['first_name'] = $this->master_model->decryptIt($value['crt_first_name']);
				$value['last_name'] = $this->master_model->decryptIt($value['crt_last_name']);
				$arr[] = $value;
			}
		}

		return $arr;
	}

	public function getgroup_name($group_id){
		$user_group = $this->master_model->getRecords('user_group',array('id'=>$group_id));
		if(count($user_group) > 0){
			return $user_group[0]['group_name'];
		}
		else{
			return 'All';
		}
	}

	public function user_access($login_id){
		$info = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$login_id));

		if(count($info) > 0){
			$access = unserialize($info[0]['access']);

			if(!is_array($access)){
				$access = array();
			}

			return $access;
		}
		else{
			return array();
		}

	}

	public function checkduplicate($email){ //not used
		//$email = $_POST['email'];
		$users = $this->master_model->getRecords('cf_login_master');
		$count = 0;
		foreach($users as $ss=>$usr){
			if($usr['email_id'] == $this->master_model->encryptIt($email)){
				$count++;
				//echo 'duplicate';
				//return false;
			}
		}

		return $count;
	}



	public function get_nexmo_balance($exchange = NULL){

		$url = "http://currency-api.appspot.com/api/EUR/USD.json?key=0d58e6fb444400a40e03e3f04bc80cb7647b0912";

		$result = file_get_contents($url);
		$result = json_decode($result);


		//get usd value for EUR/USD
		if(!is_null($exchange)){
			$usdvalue = 1.3628 ;//static value for euro to usd

			if (is_object($result)){
				if ($result->success){
					$usdvalue = $result->{'rate'};
				}

			}
			return $usdvalue;
		}

		//get nexmo balance in usd
		else{
			// load library
			$this->load->library('nexmo');
			// set response format: xml or json, default json
			$this->nexmo->set_format('json');
			//get balance
			$response = $this->nexmo->get_balance();

			$usdvalue = $response->value * 1.3628 ;//static value for euro to usd

			if (is_object($result)){
				if ($result->success){
					$usdvalue = $response->value * $result->{'rate'};
				}

			}
			return $usdvalue;
		}

	}


	public function subscription_data($plan_data){

		$current_period_start = $plan_data->current_period_start;
		$date = new DateTime("@$current_period_start");
		$current_period_start = $date->format('M d Y');

		$current_period_end = $plan_data->current_period_end;
		$date = new DateTime("@$current_period_end");
		$current_period_end = $date->format('M d Y');


		$subscription_data = array(

			"id"=>$plan_data->id,
			"plan"=> array(
			  "interval"=>$plan_data->plan->interval,
			  "name"=>$plan_data->plan->name,
			  "created"=>$plan_data->plan->created,
			  "amount"=>$plan_data->plan->amount,
			  "currency"=>$plan_data->plan->currency,
			  "id"=>$plan_data->plan->id,
			  "object"=>$plan_data->plan->object,
			  "livemode"=>$plan_data->plan->livemode,
			  "interval_count"=>$plan_data->plan->interval_count,
			  "trial_period_days"=>$plan_data->plan->trial_period_days,
			  "statement_descriptor"=>$plan_data->plan->statement_descriptor
			),
			"object"=>$plan_data->object,
			"start"=>$plan_data->start,
			"status"=>$plan_data->status,
			"customer"=>$plan_data->customer,
			"cancel_at_period_end"=>$plan_data->cancel_at_period_end,
			"current_period_start"=>$current_period_start,
			"current_period_end"=>$current_period_end,
			"ended_at"=>$plan_data->ended_at,
			"trial_start"=>$plan_data->trial_start,
			"trial_end"=>$plan_data->trial_end,
			"canceled_at"=>$plan_data->canceled_at,
			"quantity"=>$plan_data->quantity,
			"application_fee_percent"=>$plan_data->application_fee_percent,
			"discount"=>$plan_data->discount,
			"tax_percent"=>$plan_data->tax_percent,

		);

		return $subscription_data;
	}

	public function active_module($org_id){
		$org_modules = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		//feature modules
		$the_modules = '';
		if (strpos($org_modules[0]['active_module'], 'm3m') !== false){
			$the_modules .= '3';
		}

		if (strpos($org_modules[0]['active_module'], 'm6m') !== false){
			$the_modules .= '6';
		}

		if (strpos($org_modules[0]['active_module'], 'm7m') !== false){
			$the_modules .= '7';
		}

		if (strpos($org_modules[0]['active_module'], 'm9m') !== false){
			$the_modules .= '9';
		}

		if (strpos($org_modules[0]['active_module'], 'm10m') !== false){
			$the_modules .= '0';
		}

		if (strpos($org_modules[0]['active_module'], 'm11m') !== false){
			$the_modules .= 'a';
		}

		if (strpos($org_modules[0]['active_module'], 'm12m') !== false){
			$the_modules .= 'b';
		}

		return $the_modules;

	}

	public function preselected_modules(){
		$mods = $this->master_model->getRecords('module_master', array('preselected'=>'Y'));
		$modules = array();

		if(count($mods) > 0){
			foreach($mods as $r=>$val){
				$modules[] = $val['id'];
			}
		}


		$themods = "";
		$count_mod = 1;
		foreach($modules as $check){
			$themods .= 'm'.$check.'m';

			if(count($modules) != $count_mod){
				$themods .='-';
			}

			$count_mod++;
		}


		$active_module = $themods; //'m5m-m3m-m6m-m7m-m9m-m10m-m12m';
		$active_mod = serialize($modules);

		$arr['active_mod'] = $active_mod;
		$arr['active_module'] = $active_module;

		return $arr;
	}

	public function get_admin_help(){
		return true;
	}


	public function get_message($id = NULL){
		$message['id'] = $id;
		$message['success'] = '<i class="fa fa-check-circle"></i> Action successful.';
		$message['error'] = '<i class="fa fa-warning"></i> Action failed to execute.';
		$message['info'] = '<i class="fa fa-info-circle"></i> No issue found.';
		$message['confirm'] = '<i class="fa fa-question-circle text-info"></i> Confirm action.';

		if($id != NULL){
			$mess = $this->master_model->getRecords('validation_messages', array('id'=>$id));

			if(count($mess) > 0){
				$message['success'] = '<i class="fa fa-check-circle"></i> '.$mess[0]['success_message'];

				if($mess[0]['error_message'] != ''){
					$message['error'] = '<i class="fa fa-warning"></i> '.$mess[0]['error_message'];
				}

				if($mess[0]['info_message'] != ''){
					$message['info'] = '<i class="fa fa-info-circle"></i> '.$mess[0]['info_message'];
				}

				if($mess[0]['confirm_message'] != ''){
					$message['confirm'] = $mess[0]['confirm_message']; //'<i class="fa fa-question-circle text-info"></i> '.$mess[0]['confirm_message'];
				}
			}

			return $message;

		}
		else{
			$messages = array();
			$mess = $this->master_model->getRecords('validation_messages');
			$recall = array();
			$standard = array();
			$gloval = array();
			foreach($mess as $r=>$value){
				$confirm_fa = ''; //<i class="fa fa-question-circle text-info"></i> ';
				if($value['module'] == 'recall'){
					$recall[$value['name']] = $confirm_fa.$value['confirm_message'];
				}
				else if($value['module'] == ''){
					$gloval[$value['name']] = $confirm_fa.$value['confirm_message'];
				}
				else if($value['module'] == 'standard'){
					$standard[$value['name']] = $confirm_fa.$value['confirm_message'];
				}
			}
			$messages['recall'] = $recall;
			$messages['standard'] = $standard;
			$messages['global'] = $gloval;
			return $messages;

		}


	}

	public function org_details($org_id){
		$org = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
		$arr = array();
		if(count($org) > 0){
			foreach($org as $r=>$value){
				$active_module = explode('-', $value['active_module']);
				$value['modules'] = $active_module;
				$value['active_mod'] = unserialize($value['active_mod']);
				$value['stakeholders'] = unserialize($value['stakeholders']);
				$arr = $value;
			}
		}

		return $arr;
	}

	public function org_location($cc_id, $org_id){
		$purchaser = $this->master_model->getRecords('organization_signups', array('cc_id'=>$cc_id, 'org_id'=>$org_id), '*', array(), 0, 1);

		$location = '';
		if(count($purchaser) > 0){
			foreach($purchaser as $r=>$value){
				$value['content'] = unserialize($value['content']);
				//format_content
				$purchaser_info = array();
				foreach($value['content'] as $r=>$value){
					$purchaser_info[$value['name']] = $value['value'];
				}
				$value['info'] = $purchaser_info;
				$address = explode(', ', $purchaser_info['address']);
				$value['location'] = $address;
				$count = count($address) - 1;

				$location = $address[$count];
				//$value['stripe_data'] = unserialize($value['stripe_data']);
			}
			//$data[] = $value;
		}
		else{
			$cc_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$cc_id));

			if(count($cc_info) > 0){
				$address = explode(', ', $cc_info[0]['location']);
				$count = count($address) - 1;

				$location = $address[$count];

			}
		}

		return $location;
	}


	public function cc_location($cc_id){
		$cc_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$cc_id));
		$cc_loc = array();
		if(count($cc_info) > 0){
			$cc_loc = explode(', ', $cc_info[0]['location']);
		}
		return $cc_loc;
	}

	public function trial_days($cc_id){
		$user = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));

		$date1= time();
		$date2= strtotime($user[0]['trial_end']);
		//Calculate the difference.
		$difference = $date2 - $date1;
		//Convert seconds into days.
		$remaining = floor($difference / (60*60*24) );
		$remaining = max($remaining,0);
		

		// $date1 = new DateTime(date('Y-m-d H:i:s'));
		// $date2 = new DateTime($user[0]['trial_end']);
		// $remaining = ($user[0]['subscription_cancelled'] == '1') ? 0 : $date1->diff($date2)->format("%d");

		$arr = array(
			'remaining'=>$remaining,
			'percent'=>round($remaining/30 * 100, 0)
		);
		return $arr;
	}

	public function single_payment($cc_id = 0, $type = 'cronjob'){
		$whr_single = array(
			'user_level'=>'0'
		);

		if($cc_id != 0){
			$whr_single['login_id'] = $cc_id;
		}
		if($type == 'cronjob'){
			$whr_single['single_user'] = 'y';
			$whr_single['stripe_customer_id !='] = '0';
		}

		$single_user = $this->master_model->getRecords('cf_login_master', $whr_single);


		$data = array(
			'whr_single'=>$whr_single,
			// 'next_month'=>$next_month,
			// 'next_month_format'=>date_format(date_create($next_month), 'd/m/Y'),
			'org_count'=>count($single_user)
		);

		$org = array();

		if(count($single_user) > 0){
			foreach($single_user as $r=>$value){
				$format_billing = $this->common_model->format_billing($cc_id);

				if($format_billing){
						
					$info['total_bill'] = 0;
					//$info['purchaser_info'] = $this->purchaser_format($value['login_id'], $value['default_org']);


					$info['org_info'] = $this->master_model->getRecords('organization_master', array('organization_id'=>$value['default_org']));

					$info['last_payment'] = $value['trial_end'];
					if($info['org_info'][0]['last_payment'] != '0000-00-00 00:00:00'){
						$info['last_payment'] = $info['org_info'][0]['last_payment'];
					}
					$info['last_payment_format'] = date_format(date_create($info['last_payment']), 'd/m/Y');


					// $next_month = ($type == 'cronjob') ? date('Y-m-1 H:i:s') : date('Y-m-1 H:i:s', strtotime("+1 year"));
					$next_month = ($type == 'cronjob') ? date('Y-m-01 H:i:s') : date('Y-m-01 H:i:s', strtotime("+1 month", strtotime($info['last_payment'])));
					$data['next_month'] = $next_month;
					$data['next_month_format'] = date_format(date_create($next_month), 'd/m/Y');


					//for cc
					$date1 = new DateTime($info['last_payment']);
					$date2 = new DateTime($next_month);
					$the_month = ($type == 'cronjob') ? date('m', strtotime("-1 month")) : date('m', strtotime("+1 month"));
					$info['days_of_month'] = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime("-1 month")), date('Y')); //30; //


					//get user address
					$value['location'] = $this->org_location($value['login_id'], $value['default_org']);

					//get admin single value
					$admin_value = $this->master_model->getRecords('admin_login', array('user_name'=>'admin'), 'single_rate');
					$value['admin'] = $admin_value[0];


					$value['single_price'] = $this->plan_price(); //$value['admin']['single_rate'];

					$info['gst'] = ($value['location'] == 'Australia') ? ($value['single_price'] * 0.1) : 0;
					$value['single_price_nogst'] = $value['single_price'];
					$value['single_price'] = $value['single_price'] + $info['gst'];


					$value['user_days'] = $date1->diff($date2)->format("%R%a");

					$value['user_bill'] = $value['single_price']; //($value['single_price'] * ($value['user_days']/$info['days_of_month'])); // * 12;
					
					//zero negate value
					$value['user_bill'] = ($value['user_bill'] > 0) ? $value['user_bill'] : 0;

					$info['cc_info'] = $value;



					$info['total_bill'] += $value['user_bill'];

					$first_five_total = $value['user_bill'];


					$org_members = $this->master_model->getRecords('cf_login_master', array('default_org'=>$value['default_org'],'user_level !='=>'2')); //, 'user_status'=>'1', 'date_deleted'=>'0000-00-00 00:00:00'

					$info['org_members_count'] = count($org_members);


					$org_crts  = array();
					$member_count = 1;
					//get each crt bills
					// if(count($org_members) > 0){
					// 	foreach($org_members as $om=>$omval){
					// 		$date_activated = new DateTime($omval['date_profile_activated']);
					// 		$last_payment = new DateTime($info['last_payment']);

					// 		$omval['last_active_date'] = $date1->diff($date_activated)->format("%R%a");
					// 		//check last_active_date from last payment
					// 		$date1 = $last_payment;
					// 		if($omval['last_active_date'] > 0){
					// 			$date1 = $date_activated;
					// 		}

					// 		$date2 = new DateTime($next_month);

					// 		$omval['user_days'] = $date1->diff($date2)->format("%R%a");

					// 		$the_multiplier = ($omval['user_days'] > 359) ? 12 : ($omval['user_days']/$info['days_of_month']);

					// 		$omval['user_bill'] = ($value['single_price']) * $the_multiplier;
					// 		//zero negate value
					// 		$omval['user_bill'] = ($omval['user_bill'] > 0) ? $omval['user_bill'] : 0;

					// 		$info['total_bill'] += $omval['user_bill'];

					// 		$org_crts[] = $omval;

					// 		if($member_count < 6){
					// 			$first_five_total += $omval['user_bill'];
					// 		}
					// 		$member_count++;
							
					// 	}
					// }

					$info['first_five_total'] = round($first_five_total, 2);
					$info['first_five_total_discount'] = $info['first_five_total'] * 0.33;
					$info['member_count'] = $member_count;
					
					//check member count
					// if($member_count <= 5){
					// 	$user_bill = (50 * ($value['user_days']/$info['days_of_month'])); //take cc activation date for prorating flat rate
					// 	$info['total_bill'] = round(5, 2);
					// }
					// else{
					// 	//round total bill
					// 	$info['total_bill'] = round($info['total_bill'], 2) - $info['first_five_total_discount'];
					// }
					
					$info['org_crts'] = $org_crts;

					$org[] = $info;					
				}

			}

		}
		$data['orgs'] = $org;

		return $data;

	}

	public function paynow_single($cc_info, $total_bill, $billing_date, $billing_details = array()){
		$data = array(
			'cc_info'=>$cc_info,
			'total_bill'=>$total_bill
		);

		$apikey = 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0'; //(base_url() == '') ? 'sk_test_siLUn5Xh93UtuUkMLpnolKoi' : 'sk_test_zC4nyIzsM6pXKLXhsrJZHiB0';

		Stripe::setApiKey($apikey);

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			// Charge the Customer instead of the card
			$customer = Stripe_Charge::create(array(
			  "amount" => $total_bill * 100, # amount in cents, again
			  "currency" => "aud",
			  "customer" => $cc_info['stripe_customer_id'],
			  "description" => "annual payment of organization in crisisflo"
			));
			$data['result'] = 'success';
		} catch(Stripe_CardError $e) {
		  // The card has been declined

			$data['result'] = 'declined';
		}

		if($data['result'] == 'success'){
			//$nowtime = date("Y-m-d h:i:s");
			$this->master_model->updateRecord('organization_master', array('last_payment'=>$billing_date), array('organization_id'=>$cc_info['default_org']));

			$stripe_arr = array(
				'cc_id'=>$cc_info['login_id'],
				'org_id'=>$cc_info['default_org'],
				'content'=>serialize($billing_details)
			);
			$this->master_model->insertRecord('stripe_subscriptions', $stripe_arr);
			$data['stripe_arr'] = $stripe_arr;
		}

		return $data;

	}

	public function format_incident($whr){
		$open_recall = $this->master_model->getRecords('cf_recall', $whr, '*', array('id'=>'DESC'));

		$arr = array();

		if(!empty($open_recall)){
			foreach($open_recall as $r=>$value){
				$value['incident_status_text'] = ($value['incident_status'] == '0') ? 'Open' : 'Closed';
				$value['template_id'] = ($value['template_id'] != '0') ? $value['template_id'] : '18';
				$value['init_date_format'] =  date_format(date_create($value['initiation_date']), 'jS F Y - g:ia');
				$value['closed_date_format'] =  date_format(date_create($value['closed_date']), 'jS F Y - g:ia');
				$value['total_time'] =  $this->format_date_diff($value);

				$value['completion_rate'] = $this->task_completion_rate($value['id'], $value['template_id']);
				
				$arr[] = $value;
			}
		}

		return $arr;
	}

	public function format_date_diff($incident){

		$recall_initiated = new DateTime($incident['initiation_date']);

		$nowtime = $incident['closed_date'];
		
		$recall_closed = new DateTime($nowtime);
		$difference = $recall_initiated->diff($recall_closed);
		
		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i; //.' '.$difference->s.' ';

		$inci_tot = explode(' ',$interval);
		$unit_time = array(
			'year',
			'month',
			'day',
			'hr',
			'min',
			'second'
		);
		$u = 0; 
		$txt = '';
		foreach($inci_tot as $tot){
			if ($tot == 0 ){
			}
			else if ($tot == 1 ){
				$txt .= $tot.' '.$unit_time[$u].' ';
			}
			else{
				$txt .= $tot.' '.$unit_time[$u].'s ';
			}
			$u++;
		}
		return $txt;
	}


	public function format_date_diff2($incident){

		$recall_initiated = new DateTime($incident['start_date']);

		$nowtime = $incident['end_date'];
		
		$recall_closed = new DateTime($nowtime);
		$difference = $recall_initiated->diff($recall_closed);
		
		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';

		$inci_tot = explode(' ',$interval);
		$unit_time = array(
			'year',
			'month',
			'day',
			'hour',
			'minute',
			'second'
		);
		$u = 0; 
		$txt = '';
		foreach($inci_tot as $tot){
			if ($tot == 0 ){
			}
			else if ($tot == 1 ){
				$txt .= $tot.' '.$unit_time[$u].' ';
			}
			else{
				$txt .= $tot.' '.$unit_time[$u].'s ';
			}
			$u++;
		}
		return $txt;
	}


	public function format_date_diff3($incident){ //get passed/remaining time

		$date = date("Y-m-d H:i:s");
		$time = strtotime($date);
		$time = $time + (70); //seconds
		$date = date("Y-m-d H:i:s", $time);
		$nowinitiatetime = strtotime($date);
		
		$recall_initiated = new DateTime($incident['start_date']);

		$nowtime = $incident['end_date'];
		
		$recall_closed = new DateTime($nowtime);
		$difference = $recall_initiated->diff($recall_closed);
		
		$interval = $difference->y.' '.$difference->m.' '.$difference->d.' '.$difference->h.' '.$difference->i.' '.$difference->s.' ';

		$inci_tot = explode(' ',$interval);
		$unit_time = array(
			'year',
			'month',
			'day',
			'hour',
			'minute',
			'second'
		);
		$u = 0; 
		$txt = '';
		foreach($inci_tot as $tot){
			if ($tot == 0 ){
			}
			else if ($tot == 1 ){
				$txt .= $tot.' '.$unit_time[$u].' ';
			}
			else{
				$txt .= $tot.' '.$unit_time[$u].'s ';
			}
			$u++;
		}
		return $txt;
	}




	public function generate_incident_id($cc_id = '', $org_id = ''){
		$cc_incident_id = 1;
		
		$cc_id = (isset($cc_id)) ? $cc_id: $this->session->userdata('logged_cc_login_id');
		$org_id = (isset($org_id)) ? $org_id: $this->session->userdata('cc_selected_orgnaization');

		$whr_xcase = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);
		
		$excase_id = $this->master_model->getRecords('cf_recall', $whr_xcase, 'cc_incident_id');
		if(count($excase_id) > 0){

			$numItems = count($excase_id);
			$i = 0;
			foreach($excase_id as $key=>$value) {
			  if(++$i === $numItems) {
				  
				$cc_incident_id = $value['cc_incident_id'] + 1;

			  }
			}  
				
		}
		
		return $cc_incident_id;	
	}

	public function format_inci_id($no){
		$d = sprintf("R%03s", $no);
		return $d;	
	}


	public function format_task_id($no){
		$d = sprintf("%04s", $no);
		return $d;	
	}

	public function format_transaction_history($whr){
		$stripe_subscriptions = $this->master_model->getRecords('stripe_subscriptions', $whr, 'id, org_id, cc_id, content, date_added', array('id'=>'DESC'));

		$arr = array();

		if(!empty($stripe_subscriptions)){
			foreach($stripe_subscriptions as $r=>$value){
				$value['content'] = unserialize($value['content']);
				$value['content']['id'] = $value['id'];
				$arr[] = $value;
			}
		}

		return $arr;
	}

	public function log_sessions($user_id = false, $logged_cc_login_id = false){
		$user_id = ($user_id) ? $user_id : $this->session->userdata('user_id');
		$logged_cc_login_id = ($logged_cc_login_id) ? $logged_cc_login_id : $this->session->userdata('logged_cc_login_id');
		$sessions = $this->master_model->getRecords('ci_sessions');

		$match = 0;
		$arr = array(
			'count'=>count($sessions),
			'match'=>$match,
			'user_id'=>$user_id,
			'logged_cc_login_id'=>$user_id,
		);
		if(count($sessions) > 0){
			foreach($sessions as $r=>$value){
				$ts = $value['last_activity'];
				$date = new DateTime("@$ts");
				$value['last_activity_format'] = $date->format('Y-m-d H:i:s');
				$value['user_data'] = unserialize($value['user_data']);

				$value['time'] = strtotime($value['last_activity_format']);
				$value['curtime'] = time();
				$value['timelapse'] = $value['curtime']-$value['time'];

				$arr['sessions'][] = $value;

				if($value['user_data']){
					$udata = $value['user_data'];
					if(isset($udata['logged_cc_login_id']) && isset($udata['user_id'])){
						if($logged_cc_login_id == $udata['logged_cc_login_id'] && $user_id != $udata['user_id'] && $value['timelapse'] < 7200){//cc id, user id, and 2hrs session time
							// $match++;
							$arr['logged_user'][] = $value;
						}
					}
				}
			}
		}
		$arr['match'] = $match;
		return $arr;
	}

	public function gen_incidentno($cc_id, $org_id){
		$whr_xcase = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id
		);

		$excase_id = $this->master_model->getRecords('cf_recall', $whr_xcase, 'cc_incident_id');

		$cc_incident_id = 1;

		if(count($excase_id) > 0){

			$numItems = count($excase_id);
			$i = 0;
			foreach($excase_id as $key=>$value) {
			  if(++$i === $numItems) {

				$cc_incident_id = $value['cc_incident_id'] + 1;

			  }
			}


		}

		return sprintf("R%03s", $cc_incident_id);

	}

	public function admin_email_sender(){
		
		$whr = array('id'=>'1');
		$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');
		return $adminemail[0]['recovery_email'];
	}

	public function activate_customer_demo($login_id = null){

		$id = $login_id;
		$result =  'error';

		if(!empty($id)){
			/* fetch  crt members*/
			$crt_onetimekey = $this->master_model->getRecords('cf_login_master',array('login_id'=>$id));
			$crt_record = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$id));


			$whr=array('id'=>'1');

			$adminemail = $this->master_model->getRecords('admin_login',$whr,'*');

			$info_arr=array(
				'from'=>$adminemail[0]['recovery_email'],

				'to'=>$this->master_model->decryptIt($crt_record[0]['crt_email']),

				'subject'=>'Welcome to CrisisFlo',

				'view'=>'registration-mail-to-cc'
			);



			$other_info = array(

				'first_name'=>$this->master_model->decryptIt($crt_record[0]['crt_first_name']),

				'last_name'=>$this->master_model->decryptIt($crt_record[0]['crt_last_name']),

				'onetime_key'=>$crt_onetimekey[0]['onetime_key'],

				'email'=>$this->master_model->decryptIt($crt_record[0]['crt_email'])

			);


			if($this->email_sending->sendmail($info_arr,$other_info)){
	//			$this->session->set_flashdata('success',' Email request successfully re-sent.');
				$result =  'success';
			}

		}

		return $result;


	}

	public function user_login_for_session($user_id){
		$chk_arr = array(
			'login.login_id'=>$user_id
		);

		$this->db->join('cf_crisis_response_team as crt','login.login_id=crt.login_id');

		$row = $this->master_model->getRecords('cf_login_master as login', $chk_arr);

		
		$name_of_org = $this->master_model->getRecords('organization_master',array('organization_id'=>$row[0]['default_org']),'*');



		$user_data = array(
			'logged_cc_login_id'=>$name_of_org[0]['cc_id'], //row[0]['login_id'],
			'logged_cc_email_id'=>$this->master_model->decryptIt($row[0]['crt_email']),
			'logged_parent_cc'=>$name_of_org[0]['cc_id'], //row[0]['cc_id'],
			'team_cc'=>$name_of_org[0]['cc_id'], //row[0]['cc_id'],
			'user_id'=>$row[0]['login_id'], //row[0]['cc_id'],
			'timezone'=>$row[0]['timezone'],
			'country_id'=>$row[0]['country_id'],
			'forum_last_visit'=>$row[0]['forum_last_visit'],
			'location'=>$row[0]['location'],
			'location_lat'=>$row[0]['location_lat'],
			'location_lng'=>$row[0]['location_lng'],
			'country_id'=>$row[0]['country_id'],
			'countrycode'=>$this->master_model->decryptIt($row[0]['countrycode']),
			'crt_digits'=>$this->master_model->decryptIt($row[0]['crt_digits']),
			'my_id'=>$row[0]['login_id'],
			'logged_user_type'=>$row[0]['user_type'],
			'avatar'=>$row[0]['avatar'],
			'logged_display_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
			'first_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']),
			'last_name'=>$this->master_model->decryptIt($row[0]['crt_last_name']),
			'logged_user_level'=>$row[0]['user_level'],
			'cc_selected_orgnaization'=>$row[0]['default_org'],
			'task_review_date'=>$row[0]['tasks_review_date'],
			'cc_selected_orgnaization_name'=>$name_of_org[0]['organization_name']
		);

		$user_data['user_orgs'] = $this->common_model->getcrtorgs($user_data['logged_cc_email_id']);

		return $user_data;
		
		// $user_data = array(
		// 	'logged_cc_login_id'=>$name_of_org[0]['cc_id'], //row[0]['login_id'],
		// 	'logged_cc_email_id'=>$this->master_model->decryptIt($row[0]['crt_email']),
		// 	'logged_parent_cc'=>$name_of_org[0]['cc_id'], //row[0]['cc_id'],
		// 	'team_cc'=>$name_of_org[0]['cc_id'], //row[0]['cc_id'],
		// 	'user_id'=>$row[0]['login_id'], //row[0]['cc_id'],
		// 	'timezone'=>$row[0]['timezone'],
		// 	'country_id'=>$row[0]['country_id'],
		// 	'forum_last_visit'=>$row[0]['forum_last_visit'],
		// 	'location'=>$row[0]['location'],
		// 	'location_lat'=>$row[0]['location_lat'],
		// 	'location_lng'=>$row[0]['location_lng'],
		// 	'country_id'=>$row[0]['country_id'],
		// 	'countrycode'=>$this->master_model->decryptIt($row[0]['countrycode']),
		// 	'crt_digits'=>$this->master_model->decryptIt($row[0]['crt_digits']),
		// 	'my_id'=>$row[0]['login_id'],
		// 	'logged_user_type'=>$row[0]['user_type'],
		// 	'logged_display_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']).' '.$this->master_model->decryptIt($row[0]['crt_last_name']),
		// 	'first_name'=>$this->master_model->decryptIt($row[0]['crt_first_name']),
		// 	'last_name'=>$this->master_model->decryptIt($row[0]['crt_last_name']),
		// 	'logged_user_level'=>$row[0]['user_level'],
		// 	'cc_selected_orgnaization'=>$row[0]['default_org'],
		// 	'task_review_date'=>$row[0]['tasks_review_date'],
		// 	'cc_selected_orgnaization_name'=>$name_of_org[0]['organization_name']
							
		// );


	}

	public function pack_id(){
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$pack_id = 18; //recall pack id
				
			if(count($mypack)> 0){
				$input_pack_id = $mypack[0]['input_pack_id'];
				
				//if org has input_pack
				if($input_pack_id != 0){
					$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('input_pack_id'=>$input_pack_id));
					
					if (count($myrecallpack) > 0){
						$pack_id = $myrecallpack[0]['id'];
					}
	
				}
				else{
					//check if has reseller assigned
					if($mypack[0]['reseller_id'] != '0'){
						$reseller_pack = $this->master_model->getRecords('cf_recall_packs',array('consultant_id'=>$mypack[0]['reseller_id']));
						
						if(count($reseller_pack) > 0){
							$pack_id = $reseller_pack[0]['id'];
						}
					}
	
				}
				
			}	//count($mypack)> 0		
			
		}
		else{
			$pack_id = 17; //continuity pack id
		}

		return $pack_id;
	}

	public function setup_status_completed($cc_id = '', $org_id = ''){
		$cc_id = (!empty($cc_id)) ? $cc_id : $this->session->userdata('logged_cc_login_id');
		$org_id = (!empty($org_id)) ? $org_id : $this->session->userdata('cc_selected_orgnaization');

		$cc_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$cc_id), '*');

		$crt_updated = $this->master_model->getRecords('cf_login_master', array('default_org'=>$org_id), 'date_updated', array('date_updated'=>'DESC'));

		$crt_count = (count($crt_updated) > 1) ? 'Last Updated: '.date_format(date_create($crt_updated[0]['date_updated']), ' d M Y') : '';

		$status = false;

		if($cc_info[0]['tasks_review_date'] != '0000-00-00 00:00:00' && !empty($crt_count) && $cc_info[0]['timezone_updated'] != '0000-00-00 00:00:00'){
			$status = true;
		}

		return $status;
	}


	public function setup_status_details($cc_id = '', $org_id = ''){
		$cc_id = (!empty($cc_id)) ? $cc_id : $this->session->userdata('logged_cc_login_id');
		$org_id = (!empty($org_id)) ? $org_id : $this->session->userdata('cc_selected_orgnaization');

		$cc_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$cc_id), '*');

		$crt_updated = $this->master_model->getRecords('cf_login_master', array('default_org'=>$org_id), 'date_updated', array('date_updated'=>'DESC'));

		$crt_count = (count($crt_updated) > 1) ? 'Last Updated: '.date_format(date_create($crt_updated[0]['date_updated']), ' d M Y') : '';

		$status = array(
			array(
				'name'=>'Set timezone',
				'status'=>($cc_info[0]['timezone_updated'] != '0000-00-00 00:00:00') ? 'ok' : 'pending',
				'icon'=>($cc_info[0]['timezone_updated'] != '0000-00-00 00:00:00') ? 'fa-check'  : 'fa-clock-o' 
			),
			array(
				'name'=>'Create team',
				'status'=>(!empty($crt_count)) ? 'ok' : 'pending',
				'icon'=>(!empty($crt_count)) ? 'fa-check' : 'fa-user'  
			),
			array(
				'name'=>'Review standby tasks',
				'status'=>($cc_info[0]['tasks_review_date'] != '0000-00-00 00:00:00') ? 'ok' : 'pending',
				'icon'=>($cc_info[0]['tasks_review_date'] != '0000-00-00 00:00:00') ? 'fa-check' : 'fa-list-alt' 
			),

		);

		return $status;
	}
	
	public function format_gm_files($whr, $sort = array()){
		$wiki_files = $this->master_model->getRecords('group_messages_files', $whr); 
		$arr = array();

		if(!empty($wiki_files)){
			foreach($wiki_files as $r=>$value){
				$value['link'] = base_url().'uploads/gmessage-files/'.$value['file_name']; 
				$value['author'] = $this->format_crt($value['cc_id']);
				$arr[] = $value;
			}
		}
		return $arr;
	}
	public function format_files($whr, $sort = array()){
		$wiki_files = $this->master_model->getRecords('wiki_files', $whr); 
		$arr = array();

		if(!empty($wiki_files)){
			foreach($wiki_files as $r=>$value){
				$value['link'] = base_url().'uploads/wiki-files/'.$value['file_name']; 
				$value['author'] = $this->format_crt($value['cc_id']);
				$arr[] = $value;

			}
		}
		return $arr;
	}


	public function task_answer_bool($whr){
		$task_answer = $this->master_model->getRecords('recall_task_answers', $whr, 'answer', array('id'=>'DESC'), false, '1');
		$ans = false;

		if(!empty($task_answer)){
			$ans = ($task_answer[0]['answer'] == 'true') ? true : false;
		}
		return $ans;
	}

	public function task_answer_progress($whr){
		$task_answer = $this->master_model->getRecords('recall_task_answers', $whr, '*', array('id'=>'DESC'), false, '1');
		$ans = false;
		$arr = array(
			'blocked'=>'0',
			'answer'=>$ans,
			'task_answer'=>null
		);
		if(!empty($task_answer) && !empty($whr['incident_id'])){
			$ans = ($task_answer[0]['sort'] == '1') ? true : false;
			$arr['answer'] = $ans;
			$arr['blocked'] = $task_answer[0]['blocked'];
			$arr['task_answer'] = $task_answer[0];
		}
		return $arr;
	}

	public function format_pages($whr, $sort = array()){
		$wiki_pages = $this->master_model->getRecords('wiki_pages',$whr, '*', $sort);
		$arr = array();

		if(!empty($wiki_pages)){
			foreach($wiki_pages as $r=>$value){

				$value['author'] = $this->format_crt($value['user_id']);
				$comments = $this->master_model->getRecords('wiki_comments', array('page_id'=>$value['id']));

				$carr = array();
				if(!empty($comments)){
					foreach($comments as $cr=>$cvalue){
						$cvalue['created_at'] = date_format(date_create($cvalue['created_at']), 'M d, Y');
						$cvalue['author'] = $this->format_crt($cvalue['user_id']);
						$carr[] = $cvalue;

					}
				}

				// $value['content'] = ''; //$carr;
				$value['comments'] = $carr;
				$value['created_at_time'] = date_format(date_create($value['created_at']), 'M d, Y h:i');
				$value['created_at'] = date_format(date_create($value['created_at']), 'M d, Y');
				$arr[] = $value;
			}
		}

		return $arr;
	}


	public function pdf_report_data($incident_id, $template_id = '18'){

		// $incident_id = (isset($_GET['incident_id'])) ? $_GET['incident_id']: '';
		// $template_id = (isset($_GET['template_id'])) ? $_GET['template_id']: '18';
		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		$the_crts = $this->get_org_crts($cc_id,$org_id);
	
		//fetch incident date
		$incident = $this->master_model->getRecords('cf_recall', array('id'=>$incident_id));
				

		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$pack_id = (!empty($template_id)) ? $template_id : $this->pack_id();
		

		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id, 'deleted'=>'0'),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		$arr = array();

		if(count($packsteps)>0) {
			$i = 0;
			foreach($packsteps as $r => $value) {
				$step_no = $value['step_no'];

				$countqq = 0;
				$curr_sub_c = '';
				
				//get tasks
				$tasks = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id, 'date_deleted'=>'0000-00-00 00:00:00'),'',array('category'=>'ASC','arrangement'=>'ASC'));
				
				$value['tasks'] = array();
				$value['show'] = true;
			

				$todo = array();
				$in_progress = array();
				$donetask = array();
				
				if(!empty($tasks)){
					foreach($tasks as $tr=>$tvalue){
						//get answer of task
						$whr_ans = array(
							'incident_id'=>$incident_id,
							'task_id'=>$tvalue['id']
						);

						if(!empty($incident)){
							$tvalue['incident_date'] = $incident[0]['initiation_date'];
						}

						$task_answer = $this->task_answer_bool($whr_ans);
						$tvalue['checked'] = $task_answer;
						
						$task_answer_progress = $this->task_answer_progress($whr_ans);
						$tvalue['in_progress'] = $task_answer_progress['answer'];
						$tvalue['task_answer'] = $task_answer_progress['task_answer'];

						$tvalue['format_task_id'] = $this->format_task_id($tvalue['id']);
						
						$status_txt = ($tvalue['checked']) ? 'Done' : 'Backlog';
						$status_txt = ($tvalue['in_progress']) ? 'In Progress' : $status_txt;
						$tvalue['status_text'] = $status_txt;
						$tvalue['timer'] = 'N/A';
						
						//generate formatted timer
						if(!empty($tvalue['task_answer']) && isset($tvalue['incident_date'])){
							$dd = array(
								'start_date'=>$tvalue['task_answer']['created_at'],
								'end_date'=>$tvalue['incident_date']
							);

							if($tvalue['status_text'] == 'In Progress'){
								$dd['end_date'] = $this->userdatetime();
							}
							$timer = $this->format_date_diff2($dd);
							$tvalue['timer'] = $timer;
							
						}

						
						$contents = $this->master_model->getRecords('checklist_contents',array('checklist_id'=>$tvalue['id']),'',array('sort'=>'ASC'));

						$arr_content = array();

						//get assigned
						$tvalue['assigned'] = $this->format_crt($tvalue['assigned_id']);
						$tvalue['assigned_info'] = $this->format_crt($tvalue['assigned_id']);


						// $nowtime = date("m/d/y");
						$tvalue['due_date_format'] = $tvalue['due_date']; 
						$tvalue['due_date'] = ($tvalue['due_date']) ? date_format(date_create($tvalue['due_date']), 'm/d/y') : '';

						$tvalue['due_time'] = ($tvalue['due_time']) ? $tvalue['due_time'] : '7:00pm';
						

						if(!empty($contents)){
							foreach($contents as $cr=>$cvalue){
								$whr = array(
									'content_id'=>$cvalue['id']
								);

								$email = $this->master_model->getRecords('content_emails', $whr);

								$cvalue['email'] = (!empty($email)) ? $email[0] : null;


								$cvalue['field'] = $this->master_model->getRecords('content_fields', $whr);

								$cvalue['options'] = $this->master_model->getRecords('content_options', $whr);

								$arr_content[] = $cvalue;
							}
						}

						//comments
						$comments = $this->master_model->getRecords('recall_task_comments', array('recall_id'=>$incident_id, 'task_id'=>$tvalue['id']));

						$carr = array();
						if(!empty($comments)){
							foreach($comments as $cr=>$cvalue){
								$cvalue['created_at'] = date_format(date_create($cvalue['created_at']), 'M d, Y');
								$cvalue['author'] = $this->format_crt($cvalue['user_id']);
								$carr[] = $cvalue;
		
							}
						}
		
						$tvalue['comments'] = $carr;
						$tvalue['incident_id'] = $incident_id;
						

						
						// $tvalue['contents'] = $arr_content;
						$value['tasks'][] = $tvalue;
						$value['incident_id'] = $incident_id;
						

					}
				}


				$arr[] = $value;

				$i++;
			}
		}

		return $arr;

	}

	public function unread_message_count($user_id = '', $org_id = ''){
		$user_id = (!empty($user_id)) ? $user_id : $this->session->userdata('user_id');
		$org_id = (!empty($org_id)) ? $org_id : $this->session->userdata('cc_selected_orgnaization');
		
		$unread_count = 0;
		if(!empty($user_id)){
			$last = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$user_id), 'forum_last_visit');

			$unread_count = $this->master_model->getRecordCount('group_messages', array('org_id'=>$org_id, 'user_id !='=>$user_id, 'created_at >'=>$last[0]['forum_last_visit']));

			return $unread_count;
		}

		
	}

	public function visited_messages($u = null){
		$user_id = (!empty($u)) ? $u : $this->session->userdata('user_id');
		$nowtime = $this->userdatetime();
		
		$arr = array(
			'forum_last_visit'=>$nowtime
		);

		$this->master_model->updateRecord('cf_crisis_response_team', $arr, array('login_id'=>$user_id));
		
		return $arr;
	}


	public function format_templates($org_id, $user_type = 'cc'){
		$whr = array('org_id'=>$org_id);

		$whr2 = array('date_created !='=>'null', 'org_id'=>'0');
		$cf_recall_admin = $this->master_model->getRecords('cf_recall_packs', $whr2);
		
		if($user_type == 'webmanager'){
			$whr = array('date_created !='=>'null', 'org_id'=>'0');
			$cf_recall_admin = array();
		}

		$cf_recall_packs = $this->master_model->getRecords('cf_recall_packs', $whr);

		$arr = array(
			array(
				'id'=>"18",
				'name'=>"Product Recall",
				'org_id'=>"",
				'user_id'=>"",
				'input_pack_id'=>"0",
				'continuity'=>"0",
				'consultant_id'=>"0",
				'date_created'=>"2017-09-07 15:02:07",
				'date_updated'=>null,
				'date_deleted'=>null
			)
		);
		$all = array_merge($arr, $cf_recall_admin, $cf_recall_packs);
		return $all;
	}


	public function task_completion_rate($incident_id = '', $template_id = '18'){

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		// $the_crts = $this->get_org_crts($cc_id,$org_id);

		//fetch incident date
		$incident = $this->master_model->getRecords('cf_recall', array('id'=>$incident_id));
		
		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$pack_id = (!empty($template_id)) ? $template_id : $this->pack_id();
		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id, 'deleted'=>'0'),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		$arr = array();

		$done_count_all = 0;
		$tasks_count_all = 0;

		if(count($packsteps)>0) {
			$i = 0;
			foreach($packsteps as $r => $value) {
				$step_no = $value['step_no'];

				// $countqq = 0;
				// $curr_sub_c = '';
				$done_count = 0;
				$tasks_count = 0;
				
				//get tasks
				$tasks = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id, 'date_deleted'=>'0000-00-00 00:00:00'),''); //,array('category'=>'ASC','arrangement'=>'ASC'));
				
				$value['tasks'] = array();
				$value['allowed_type'] = array('header'.$value['id']);
				$value['incident_id'] = $incident_id;
				
				$todo = array();
				$in_progress = array();
				$donetask = array();
				
				if(!empty($tasks)){
					$task_index = 0;
					foreach($tasks as $tr=>$tvalue){
						$tasks_count++;
						$tasks_count_all++;
						
						//get answer of task
						$whr_ans = array(
							'incident_id'=>$incident_id,
							'task_id'=>$tvalue['id']
						);

						$tvalue['incident_date'] = '';
						if(!empty($incident)){
							$tvalue['incident_date'] = $incident[0]['initiation_date'];
						}

						$task_answer = $this->task_answer_bool($whr_ans);
						$tvalue['checked'] = $task_answer;
						
						$task_answer_progress = $this->task_answer_progress($whr_ans);
						$tvalue['in_progress'] = $task_answer_progress['answer'];
						$tvalue['task_answer'] = $task_answer_progress['task_answer'];
						
					
						$tvalue['format_task_id'] = $this->format_task_id($tvalue['id']);
						
						//disect types of tasks
						$tvalue['task_type'] = 'header'.$value['id'];
						$tvalue['task_index'] = $task_index;
						
						if($tvalue['in_progress']){
							$in_progress[] = $tvalue;
						}
						else if($tvalue['checked']){
							$donetask[] = $tvalue;
							$done_count++;
							$done_count_all++;
							
						}
						else{
							$todo[] = $tvalue;
						}

						$task_index++;
					}
				}

				$value['tasks_count'] = $tasks_count;
				$value['done_count'] = $done_count;

				$value['disected_tasks'] = array(
					$todo,
					$in_progress,
					$donetask
				);

				$arr[] = $value;

				$i++;
			}
		}

		$percentage = 0;
		if(!empty($tasks_count_all)){
			$percentage = ($done_count_all/$tasks_count_all);
			$percentage = round($percentage, 2) * 100;
		}

		$details = array(
			'percentage'=>$percentage,
			'task_count'=>$tasks_count_all,
			'done_count'=>$done_count_all
		);
		return $details;
		
	}
	public function kanban_details($incident_id = '', $template_id = '18'){

		$org_id = $this->session->userdata('cc_selected_orgnaization');
		$cc_id = $this->session->userdata('logged_cc_login_id');

		// $the_crts = $this->get_org_crts($cc_id,$org_id);

		//fetch incident date
		$incident = $this->master_model->getRecords('cf_recall', array('id'=>$incident_id));
		
		//org for recall pack
		$mypack = $this->master_model->getRecords('organization_master',array('organization_id'=>$org_id));

		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		$pack_id = (!empty($template_id)) ? $template_id : $this->pack_id();
		
		$packname = $this->master_model->getRecords('cf_recall_packs',array('id'=>$pack_id));
		$packsteps = $this->master_model->getRecords('cf_recall_packs_steps',array('recall_pack_id'=>$pack_id, 'deleted'=>'0'),'*', array('order'=>'ASC'));
		$category=$this->master_model->getRecords('cf_recall_steps_category',array('recall_pack_id'=>$pack_id,'deleted'=>'0'),'*',array('step_no'=>'ASC','id'=>'ASC'));
		
		$arr = array();

		$done_count_all = 0;
		$tasks_count_all = 0;

		if(count($packsteps)>0) {
			$i = 0;
			foreach($packsteps as $r => $value) {
				$step_no = $value['step_no'];

				// $countqq = 0;
				// $curr_sub_c = '';
				$done_count = 0;
				$tasks_count = 0;
				
				//get tasks
				$tasks = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id, 'date_deleted'=>'0000-00-00 00:00:00'),''); //,array('category'=>'ASC','arrangement'=>'ASC'));
				
				$value['tasks'] = array();
				$value['allowed_type'] = array('header'.$value['id']);
				$value['incident_id'] = $incident_id;
				
				$todo = array();
				$in_progress = array();
				$donetask = array();
				
				if(!empty($tasks)){
					$task_index = 0;
					foreach($tasks as $tr=>$tvalue){
						$tasks_count++;
						$tasks_count_all++;
						
						//get answer of task
						$whr_ans = array(
							'incident_id'=>$incident_id,
							'task_id'=>$tvalue['id']
						);

						$tvalue['incident_date'] = '';
						if(!empty($incident)){
							$tvalue['incident_date'] = $incident[0]['initiation_date'];
						}

						$task_answer = $this->task_answer_bool($whr_ans);
						$tvalue['checked'] = $task_answer;
						
						$task_answer_progress = $this->task_answer_progress($whr_ans);
						$tvalue['in_progress'] = $task_answer_progress['answer'];
						$tvalue['task_answer'] = $task_answer_progress['task_answer'];
						$tvalue['blocked'] = $task_answer_progress['blocked'];
						

						$contents = $this->master_model->getRecords('checklist_contents',array('checklist_id'=>$tvalue['id'], 'module'=>'text'),'*',array('sort'=>'ASC'), '0', '1');

						$arr_content = array();

						//get assigned
						// $tvalue['assigned'] = $this->format_crt($tvalue['assigned_id']);
						$tvalue['assigned_info'] = $this->format_crt($tvalue['assigned_id']);


						// $nowtime = date("m/d/y");
						// $tvalue['due_date_format'] = $tvalue['due_date']; 
						// $tvalue['due_date'] = ($tvalue['due_date']) ? date_format(date_create($tvalue['due_date']), 'm/d/y') : '';

						// $tvalue['due_time'] = ($tvalue['due_time']) ? $tvalue['due_time'] : '7:00pm';
						

						if(!empty($contents)){
							foreach($contents as $cr=>$cvalue){
								$whr = array(
									'content_id'=>$cvalue['id']
								);

								$email = $this->master_model->getRecords('content_emails', $whr);

								$cvalue['email'] = (!empty($email)) ? $email[0] : null;
								

								$cvalue['field'] = $this->master_model->getRecords('content_fields', $whr);

								$cvalue['options'] = $this->master_model->getRecords('content_options', $whr);

								$arr_content[] = $cvalue;
							}
						}
						else{
							$arr_content = array(
								array(
									'checklist_id'=>$tvalue['id'],
									'module'=>'text',
									'sort'=>0,
									'content'=>'',
									'id'=>'',
								)
							);
						}

						//comments
						$comments = $this->master_model->getRecords('recall_task_comments', array('recall_id'=>$incident_id, 'task_id'=>$tvalue['id']));

						$carr = array();
						if(!empty($comments)){
							foreach($comments as $cr=>$cvalue){
								$cvalue['created_at'] = date_format(date_create($cvalue['created_at']), 'M d, Y');
								$cvalue['author'] = $this->common_model->format_crt($cvalue['user_id']);
								$carr[] = $cvalue;
		
							}
						}
		
						$tvalue['comments'] = $carr;
						$tvalue['incident_id'] = $incident_id;
						

						$tvalue['contents'] = $arr_content;
						// $value['tasks'][] = $tvalue;
					
						$tvalue['format_task_id'] = $this->format_task_id($tvalue['id']);
						
						//disect types of tasks
						$tvalue['task_type'] = 'header'.$value['id'];
						$tvalue['task_index'] = $task_index;
						
						if($tvalue['in_progress']){
							$in_progress[] = $tvalue;
						}
						else if($tvalue['checked']){
							$donetask[] = $tvalue;
							$done_count++;
							$done_count_all++;
							
						}
						else{
							$todo[] = $tvalue;
						}

						$task_index++;
					}
				}

				$value['tasks_count'] = $tasks_count;
				$value['done_count'] = $done_count;

				$value['disected_tasks'] = array(
					$todo,
					$in_progress,
					$donetask
				);

				$arr[] = $value;

				$i++;
			}
		}

		return $arr;

	}
	
	public function format_sublist($arr){
		$data = array();

		if(count($arr)){
			foreach($arr as $r=>$value){
				$value['answer'] = ($value['answer'] == 'true') ? true : false;
				$data[] = $value;
			}
		}
		return $data;
	}

	public function format_incident_form($incident_form_fields){
		$formatted = array();
		if(count($incident_form_fields)){
			foreach($incident_form_fields as $r=>$value){
				$value['content'] = unserialize($value['content']);
				$fields = array();
				foreach($value['content'] as $cr=>$cvalue){
					$question_id = substr($cr, 5);
					$question = $this->master_model->getRecords('content_fields', array('id'=>$question_id));
					if(count($question)){
						$fields[] = array(
							'id'=>$question[0]['id'],
							'question_key'=>$cr,
							'question'=>$question[0]['text'],
							'type'=>$question[0]['type'],
							'response'=>$cvalue,
						);
					}
				}
				$value['fields'] = $fields;

				$formatted[] = $value;
			}
		}
		return $formatted;
	}
}

?>

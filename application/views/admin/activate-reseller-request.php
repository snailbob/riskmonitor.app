                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Activate Customer</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Activate Customer</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Activate Reseller's Customer</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_firstname" name="cc_firstname" placeholder="" required data-msg-required="Please enter first name" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_first_name']) ?>"><?php echo form_error('cc_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cc_lastname" name="cc_lastname" placeholder="" required data-msg-required="Please enter last name" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_last_name']) ?>"><?php echo form_error('cc_lastname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="organisation" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">

                            <input type="email" class="form-control" id="cc_email" name="cc_email" placeholder="" required data-msg-required="Please enter email" readonly="readonly" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_email']); ?>"><?php echo form_error('cc_email'); ?>

                        	</div>

						</div>

                        
                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['calling_code'].'"';
                                        
                                        if ($this->master_model->decryptIt($cc_info[0]['countrycode']) == $countries['calling_code']){
                                            echo ' selected="selected"';
                                        }
                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                            <input type="text" class="form-control" id="cc_phone_number" name="cc_phone_number" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_digits']); ?>"><?php echo form_error('cc_phone_number'); ?>

                            </div>

                        </div>

                        
                        
                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_address" name="cc_address" placeholder="" required data-msg-required="Please enter address" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_address']); ?>"><?php echo form_error('cc_address'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">City</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_city" name="cc_city" placeholder="" required  data-msg-required="Please enter city" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_city']); ?>"><?php echo form_error('cc_city'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">State</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_state" name="cc_state" placeholder="" required  data-msg-required="Please enter state" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_state']); ?>"><?php echo form_error('cc_state'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Zipcode</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_zip_code" name="cc_zip_code" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_zip_code']); ?>"><?php echo form_error('cc_zip_code'); ?>

                            </div>

                        </div>

                         <div class="form-group">

                            <label class="col-sm-2 control-label">User Type</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="cc_type" id="cc_type">

                                    <option value="">Select</option>

                                    <option value="demo" <?php if($cc_info[0]['user_type']=='demo'){echo 'selected="selected"';} ?>>Demo</option>

                                    <option value="live" <?php if($cc_info[0]['user_type']=='live'){echo 'selected="selected"';} ?>>Live</option>

                               </select>

                            </div>

                        </div>



                         <div class="form-group" style="border-top: 1px #eee solid; padding-top: 15px;"><!--add org-->


                            <label class="col-sm-2 control-label">Organization</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_org" name="cc_org" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo $org_info[0]['organization_name']; ?>"><?php echo form_error('cc_org'); ?>
                                <input type="hidden" class="form-control" id="org_id" name="org_id" value="<?php echo $org_info[0]['organization_id']; ?>">
                                <input type="hidden" class="form-control" id="reseller_id" name="reseller_id" value="<?php echo $cc_info[0]['reseller_id']; ?>">
                                <input type="hidden" class="form-control" id="email_id" name="email_id" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_email']); ?>">

                            </div>


                        </div><!--.add org-->


                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">Modules</label>

                            <div class="col-sm-10">

                              <div class="checkbox">
                                <label for="checkboxes-0">
                                  <input type="checkbox" name="modules[]" id="checkboxes-0" value="1" <?php if ($org_info[0]['active_module'] == '123' || $org_info[0]['active_module'] == '13' || $org_info[0]['active_module'] == '12' || $org_info[0]['active_module'] == '1'){ echo 'checked="checked"';}?>>
                                  Standard CrisisFlo
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-1">
                                  <input type="checkbox" name="modules[]" id="checkboxes-1" value="2" <?php if ($org_info[0]['active_module'] == '123' || $org_info[0]['active_module'] == '12' || $org_info[0]['active_module'] == '2' || $org_info[0]['active_module'] == '23'){ echo 'checked="checked"';} ?> >
                                  Case Management
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-2">
                                  <input type="checkbox" name="modules[]" id="checkboxes-2" value="3" <?php if ($org_info[0]['active_module'] == '123' || $org_info[0]['active_module'] == '13' || $org_info[0]['active_module'] == '23' || $org_info[0]['active_module'] == '3'){ echo 'checked="checked"';} ?> >
                                  Video Conferencing
                                </label>
                              </div>
                              <?php echo form_error('modules[]'); ?>
                            </div>

                        </div>
                        
                        
                        
                        
                        

                        
                        
                        
                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-md-2 control-label" for="radios">Customer Type</label>
                          <div class="col-md-10">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0" <?php if ($org_info[0]['cust_type'] == '0'){ echo 'checked="checked"'; } ?>>
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1" <?php if ($org_info[0]['cust_type'] == '1'){ echo 'checked="checked"'; } ?>>
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>
                        
                        








                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_cc" id="add_cc">Activate</button>

                            </div>

                        </div>

                        

                        

                    </form>									

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




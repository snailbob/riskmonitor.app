                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Add new CC



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">New Crisis Co-ordinator</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php

						}

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						}

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Crisis Co-ordinator</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">



						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="firstname" class="col-sm-3 control-label">First Name</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="cc_firstname" name="cc_firstname" placeholder="" required data-msg-required="Please enter first name" value="<?php echo set_value('cc_firstname'); ?>"><?php echo form_error('cc_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-3 control-label">Last Name</label>

                            <div class="col-sm-9">

                            <input type="text" class="form-control" id="cc_lastname" name="cc_lastname" placeholder="" required data-msg-required="Please enter last name" value="<?php echo set_value('cc_lastname'); ?>"><?php echo form_error('cc_lastname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="organisation" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-9">

                            <input type="email" class="form-control" id="cc_email" name="cc_email" value="<?php echo set_value('cc_email'); ?>"><?php echo form_error('cc_email'); ?>

                        	</div>

						</div>


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>
                                <?php if(count($countriescode)!="0"){

                                    foreach($countriescode as $countries){


                                        echo '<option value="'.$countries['country_id'].' '.$countries['calling_code'].'"';



                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';

                                    }
                                }

                                ?>
                                </select>

                            </div>
                            <div class="col-sm-5">

                            <input type="text" class="form-control" id="cc_phone_number" name="cc_phone_number" value="<?php echo set_value('cc_phone_number'); ?>" required data-msg-required="Please enter first name"><?php echo form_error('cc_phone_number'); ?>

                            </div>

                        </div>




                        <div class="form-group">

                            <label class="col-sm-3 control-label">Address</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="cc_address" name="cc_address" placeholder="" required data-msg-required="Please enter address" value="<?php echo set_value('cc_address'); ?>"><?php echo form_error('cc_address'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label">City</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="cc_city" name="cc_city" placeholder="" required  data-msg-required="Please enter city" value="<?php echo set_value('cc_city'); ?>"><?php echo form_error('cc_city'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label">State</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="cc_state" name="cc_state" placeholder="" required  data-msg-required="Please enter state" value="<?php echo set_value('cc_state'); ?>"><?php echo form_error('cc_state'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Zipcode</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="cc_zip_code" name="cc_zip_code" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo set_value('cc_zip_code'); ?>"><?php echo form_error('cc_zip_code'); ?>

                            </div>

                        </div>

                         <div class="form-group">

                            <label class="col-sm-3 control-label">User Type</label>

                            <div class="col-sm-9">

                               <select class="form-control" name="cc_type" id="cc_type">

                                    <option value="">Select</option>

                                    <option value="demo" <?php if($this->input->post('cc_type')=='demo'){echo 'selected="selected"';} ?>>Demo</option>

                                    <option value="live" <?php if($this->input->post('cc_type')=='live'){echo 'selected="selected"';} ?>>Live</option>

                               </select>

                            </div>

                        </div>

                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="radios">User Subscription Type</label>
                          <div class="col-sm-9">
                          <div class="radio">
                            <label for="utype-0">
                              <input type="radio" name="utype" id="utype-0" value="y" onchange="$('.single_price').slideDown('fast'); $('.max_mod_dd').find('select').val('1000')">
                              Single User
                            </label>
                            </div>
                          <div class="radio">
                            <label for="utype-1">
                              <input type="radio" name="utype" id="utype-1" value="n" checked="checked" onchange="$('.single_price').slideUp('fast'); $('.max_mod_dd').find('select').val('0')">
                              Not Single User
                            </label>
                            </div>
                              <?php echo form_error('utype'); ?>
                          </div>
                        </div>


                        <div class="form-group single_price" style="display: none;">

                            <label class="col-sm-3 control-label">Single User Rate per month</label>

                            <div class="col-sm-9">

                                <input type="number" class="form-control" id="single_price" name="single_price" placeholder="" value="<?php echo $admin_info[0]['single_rate']; ?>"><?php echo form_error('single_price'); ?>

                            </div>

                        </div>


                         <div class="form-group" style="border-top: 1px #eee solid; padding-top: 15px;"><!--add org-->


                            <label class="col-sm-3 control-label">Organization</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="cc_org" name="cc_org" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo set_value('cc_org'); ?>"><?php echo form_error('cc_org'); ?>

                            </div>


                        </div><!--.add org-->


                         <div class="form-group">

                            <label class="col-sm-3 control-label">Reseller</label>

                            <div class="col-sm-9">

                               <select name="reseller" id="reseller" class="form-control">
                               		<option value="">Select</option>
                                    <?php
										if(count($resellers) > 0){
											foreach($resellers as $r=>$value){
												echo '<option value="'.$value['login_id'].'">'.$this->master_model->decryptIt($value['first_name']).' '.$this->master_model->decryptIt($value['last_name']).'</value>';
											}
										}
									?>
                               </select>

                            </div>


                        </div><!--.add Reseller-->





                        <div class="form-group hidden">

                            <label for="firstname" class="col-sm-3 control-label">Modules</label>

                            <div class="col-sm-9">
								<?php

								foreach($modules as $md=>$mds){

									echo '
										  <div class="checkbox">
											<label for="checkboxes-'.$mds['id'].'">
											  <input type="checkbox" name="modules[]" id="checkboxes-'.$mds['id'].'" value="'.$mds['id'].'" onclick="check_module(\''.$mds['id'].'\');"';


											  if($mds['preselected'] == 'Y'){
											  	echo 'checked="checked"';
											  }

											  echo '>
											  <input type="hidden" name="modulesp[]" id="module-'.$mds['id'].'" value="'.$mds['price'].'">'.
											  $mds['name']
										 .'</div>

									';
								?>

                        <div class="form-group max_mod_dd <?php if($mds['id'] != 1 && $mds['id'] != 5 && $mds['id'] != 2 && $mds['id'] != 4 && $mds['id'] != 8){ echo 'hidden'; }?>" style="display: none; padding-left: 15px; padding-right: 15px;">

                                <label class="control-label">Max Users</label>
                                <select class="form-control" id="maxu<?php echo $mds['id'] ?>" name="maxu<?php echo $mds['id'] ?>">
                                    <option value="0">Select</option>

                                    <option value="5" <?php if($mds['default_users'] == '5') { echo 'selected="selected"';}?>>up to 5 users</option>

                                    <option value="10" <?php if($mds['default_users'] == '10') { echo 'selected="selected"';}?>>up to 10 users</option>

                                    <option value="20" <?php if($mds['default_users'] == '20') { echo 'selected="selected"';}?>>up to 20 users</option>

                                    <option value="50" <?php if($mds['default_users'] == '50') { echo 'selected="selected"';}?>>up to 50 users</option>

                                    <option value="1000" <?php if($mds['default_users'] == '1000') { echo 'selected="selected"';}?>>unlimited(for single user)</option>

                                </select><span class="form-helper text-red" id="maxu<?php echo $mds['id'] ?>_error"></span>

                        </div>




                                <?php
								}

								?>


                              <?php echo form_error('modules[]'); ?><span class="form-helper text-red" id="modules_error"></span>
                            </div>

                        </div>





        				<div class="form-group">
                          <label class="col-sm-3 control-label"></label>

                          <div class="col-sm-9">
                                <div class="col-sm-12">


                                    <div class="simulation_module" style="display: none; ">
                                        <?php
                                            $simulation_modules = $this->master_model->getRecords('simulation_modules');

                                            if(count($simulation_modules) > 0){
                                                echo '
                                                <div class="form-group well">
                                                <label>Simulation Modules</label>';
                                                foreach($simulation_modules as $r=>$value){

                                        ?>


            <div class="radio">
            <label>
            <input type="radio" name="simulation_modules" value="<?php echo $value['id'] ?>">
            <?php echo $value['name'] ?>
            </label><a href="#" class="view_simu_list_btn" data-id="<?php echo $value['id']; ?>">View List</a>
            </div>

                                        <?php
                                                }
                                                echo '</div>';
                                            }
                                        ?>
                                    </div>


                                </div>



                          </div>


                        </div>








                        <!-- Multiple Radios -->
                        <div class="form-group hidden">
                          <label class="col-sm-3 control-label" for="radios">Customer Type</label>
                          <div class="col-sm-9">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0">
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1" checked="checked">
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>





                        <div class="form-group">

                            <label class="col-sm-3 control-label">Preload Input Pack (Optional)</label>

                            <div class="col-sm-9">
                               <input type="text" class="form-control hidden" id="old_packs" name="old_packs" value="">

                               <select class="form-control" name="input_packs" id="input_packs">

                                    <option value="0" >Select</option>

                                    <?php

									if(count($input_packs)>0){

										foreach($input_packs as $packs){

									?>

                                    	<option value="<?php echo $packs['id']; ?>"> <?php echo $packs['name']; ?> </option>

                                    <?php
										}
									}
									?>

                               </select>
                              <?php echo form_error('input_packs'); ?>
                            </div>

                        </div>






                        <div class="form-group">

                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-9">

                                <button type="submit" class="btn btn-primary" name="add_cc" id="add_cc">Submit</button>

                            </div>

                        </div>





                    </form>

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->





<!-- Modal -->
<div class="modal fade" id="viewSimulationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="body" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="lead">Simulation List</p>
        <p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i></p>

        <div class="simulist">

            <div class="panel-group" id="accordion" role="tablist">

            </div>

        </div>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

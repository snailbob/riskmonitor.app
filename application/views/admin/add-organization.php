                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Add new Organization

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">New Organization</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Crisis Organization</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-organization" id="frm-organization" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="organization_name" class="col-sm-3 control-label">Organization Name</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="" required data-msg-required="Please enter organization name"><?php echo form_error('organization_name'); ?>

                            </div>

                        </div>



                        <div class="form-group">

                            <label for="firstname" class="col-sm-3 control-label">Modules</label>

                            <div class="col-sm-9">
								<?php 
								
								foreach($modules as $md=>$mds){
								
									echo '
										  <div class="checkbox">
											<label for="checkboxes-'.$mds['id'].'">
											  <input type="checkbox" name="modules[]" id="checkboxes-'.$mds['id'].'" value="'.$mds['id'].'">
											  <input type="hidden" name="modulesp[]" id="module-'.$mds['id'].'" value="'.$mds['price'].'">'.
											  $mds['name']
										 .'</div>
									
									';
								
								
								}
								
								?>
                            
                            
                              <?php echo form_error('modules[]'); ?>
                            </div>

                        </div>


                        
                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-md-3 control-label" for="radios">Customer Type</label>
                          <div class="col-md-9">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0">
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1">
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>
                        
                        
                                                

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Select CC</label>

                            <div class="col-sm-9">

                               <select class="form-control" name="sel_cc" id="sel_cc">

                                    <option value="">Select</option>

                                    <?php

									if(count($cc_list)>0)

									{

										foreach($cc_list as $cc)

										{

									?>

                                    <option value="<?php echo $cc['login_id'].' '.$this->master_model->decryptIt($cc['crt_email']).' '.$this->master_model->decryptIt($cc['crt_first_name']).' '.$this->master_model->decryptIt($cc['crt_last_name']); ?>" <?php if ($this->uri->segment(4) == $cc['login_id']) { echo 'selected="selected"'; }?>>

										<?php echo $this->master_model->decryptIt($cc['crt_first_name']).' '.$this->master_model->decryptIt($cc['crt_last_name']); ?>

                                    </option>

                                    

                                    <?php

										}

									} 

									?>

                               </select>

                            </div>

                        </div>


                        

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Preload Input Pack (Optional)</label>

                            <div class="col-sm-9">
                               <select class="form-control" name="input_packs" id="input_packs">

                                    <option value="0">Select</option>

                                    <?php

									if(count($input_packs)>0)

									{

										foreach($input_packs as $packs)

										{

									?>

                                    <option value="<?php echo $packs['id']; ?>"> <?php echo $packs['name']; ?> </option>

                                    <?php

										}

									} 

									?>

                               </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-9">

                            <button type="submit" class="btn btn-primary" name="add_orgz" id="add_orgz">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




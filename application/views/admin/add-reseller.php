                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Add Reseller

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">New Reseller</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Reseller</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_firstname" name="cc_firstname" placeholder=""  value="<?php echo set_value('cc_firstname'); ?>"><?php echo form_error('cc_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cc_lastname" name="cc_lastname" placeholder="" value="<?php echo set_value('cc_lastname'); ?>"><?php echo form_error('cc_lastname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="organisation" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">

                            <input type="email" class="form-control" id="cc_email" name="cc_email" value="<?php echo set_value('cc_email'); ?>"><?php echo form_error('cc_email'); ?>

                        	</div>

						</div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['calling_code'].'"';
                                        

                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select>
								<?php echo form_error('countrycode'); ?>
                            </div>
                            <div class="col-sm-6">

                            <input type="text" class="form-control" id="cc_phone_number" name="cc_phone_number" value="<?php echo set_value('cc_phone_number'); ?>"><?php echo form_error('cc_phone_number'); ?>

                            </div>

                        </div>


                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="checkboxes">Active Tabs</label>
                          <div class="col-sm-10">

                          <div class="checkbox">
                            <label for="checkboxes-portal">
                              <input type="checkbox" name="checkboxes[]" id="checkboxes-portal" value="portal" >
                              Portal Tab
                            </label>
                            </div>
                

                          <div class="checkbox">
                            <label for="checkboxes-activated">
                              <input type="checkbox" name="checkboxes[]" id="checkboxes-activated" value="activated" >
                              Activated Customers Tab
                            </label>
                            </div>

                          <div class="checkbox">
                            <label for="checkboxes-request">
                              <input type="checkbox" name="checkboxes[]" id="checkboxes-request" value="request" >
                              Request Activation Tab
                            </label>
                            </div>

                          <div class="checkbox">
                            <label for="checkboxes-recall">
                              <input type="checkbox" name="checkboxes[]" id="checkboxes-recall" value="recall" >
                              Recall Task Tab
                            </label>
                            </div>
                        	<?php echo form_error('checkboxes[]'); ?>
                        
                          </div>
                        </div>
                        
		

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_reseller" id="add_reseller">Submit</button>

                            </div>

                        </div>

                        

                        

                    </form>									

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




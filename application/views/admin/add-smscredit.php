                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Allocate Credit

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Allocate Credit</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">
                
                	<div class="col-lg-12" style="height: 40px; margin-left: 15px;">
                        <p class="lead"><b>Balance: </b><?php echo $usdvalue; ?> $</p>
                    </div>

                

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Organization SMS Credit</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-update-organization" id="frm-update-organization" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group hidden">

                            <label for="firstname" class="col-sm-2 control-label">Organization Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="" required data-msg-required="Please enter organization name" value="<?php echo $org_info[0]['organization_name']; ?>"><?php echo form_error('organization_name'); ?>

                            </div>

                        </div>

                        

                        <div class="form-group hidden">

                            <label class="col-sm-2 control-label">Select CC</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="sel_cc" id="sel_cc">

                                    <option value="">Select</option>

                                    <?php

									if(count($cc_list)>0)

									{

										foreach($cc_list as $cc)

										{

									?>

                                    <option value="<?php echo $cc['login_id']; ?>" <?php if($cc['login_id']==$org_info[0]['cc_id'])echo 'selected="selected"'; ?>>

										<?php echo $cc['crt_first_name'].' '.$cc['crt_last_name']; ?>

                                    </option>

                                    

                                    <?php

										}

									} 

									?>

                               </select>

                            </div>

                        </div>
                        

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">Credit ($)</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="sms_credit" name="sms_credit" placeholder="" data-msg-required="Please enter credit" value="<?php echo $org_info[0]['sms_credit']; ?>"><?php echo form_error('sms_credit'); ?>

                            </div>

                        </div>
                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                            <button type="submit" class="btn btn-primary" name="add_sms" id="add_sms">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




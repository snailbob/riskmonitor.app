                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Add Standby Message

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Add Standby Message</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: &nbsp;</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Add Standby Message</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-standby-msg" id="frm-add-standby-msg" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Select Scenario</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="sel_scenario" id="sel_scenario">

                               	<option value=""> Select </option>

                                <?php

									if(count($scn_list) > 0)

									{

										foreach($scn_list as $scn)

										{

								?>

                                		<option value="<?php echo $scn['scenario_id'] ?>">

                                        <?php echo $scn['scenario']; ?>

                                        </option>

                                <?php

										}

									}

                                ?>

                               </select>		

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Enter Message</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="stand_by_msg" name="stand_by_msg" placeholder="Enter Message"></textarea><?php echo form_error('task_desc'); ?>							

                           	</div>

                        </div>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>webmanager/packs/view/<?php echo $this->uri->segment(4); ?>">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_stand_by" id="add_stand_by">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




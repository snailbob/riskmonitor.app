            </div>

        </div>

    </div>
    <!-- /.container -->


    <hr />
    <div class="container-fluid">

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-muted">© 2014 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/moment.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/bootstrap-datetimepicker.min.js"></script>


    
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/wysiwyg-demo.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/2/js/plugins/cropper/dist/cropper.min.js"></script>



    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/consultant.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/admin-validation.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/jquery-ui.min.js"></script>
    
		<?php if ($this->uri->segment(2) == 'packs' && $this->uri->segment(3) == 'view' && $this->uri->segment(5) != ''  ){ ?>
    <script>
		$( document ).ready(function() {
		
			var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close hidden" id="hide_x" data-dismiss="modal" aria-hidden="true">&times;</button><p class="lead" id="hide_spin">';
			
			var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Your file is being analysed.</p><p id="scan_result">';
			
			var myModal3 = '</p></div></div></div> ';

			var myModal = myModal1 + myModal2 + myModal3;
			
			$(myModal).modal('show');
			
			var file_id = <?php echo $this->uri->segment(5); ?>;
			$.ajax({
				  type: "POST",
				  
				  url: "<?php echo base_url().'webmanager/packs/scan_response'; ?>",
				  
				  data: { file_id: file_id },
				  
				  success: function(data) {
					console.log(<?php echo $this->uri->segment(5); ?>);
					  $('#scan_result').html(data);
					  $('#hide_spin').addClass('hidden');
					  $('#hide_x').removeClass('hidden');
					  
				  }
				  
			});
			
				
		});

    </script>
		<?php
		}
		?>




    <script>
	
		$( document ).ready(function() {
			$('.content_text').on('click',function(){
				$(this).hide();
				$('.edit_content').show();
				$('.edit_content').focus();
				$('.submit_content').show();
			});
			
						
			$(document).keyup(function(e) {
			  if (e.keyCode == 27) {
				$('.edit_content').hide();
				$('.content_text').show();
				$('.submit_content').hide();
				   }   // esc
			});

		});
	
	
	
	$('.common_msg_click').on('click', function ( e ) { 
				$.fn.custombox( this );
				e.preventDefault();
	});
	
	
	function managePortal(reseller_id){
			$('#portalsuccess_holder').html('');
			$('#portaltabs_holder').html('<p class="text-center text-muted" style="padding: 50px;"><i class="fa fa-spinner fa-spin fa-2x"></i><br/>Loading content..</p>');
			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'webmanager/reseller/getPortal',
				  
				  data: { reseller_id: reseller_id },
				  
				  success: function(data) {
					//console.log(data);
					$('#portaltabs_holder').html(data);
				  }
				  
			});
	}
	
	function savePortal(){
			$('#portalsuccess_holder').html('');
			$('#save_prtal').addClass('disabled');
			$('#save_prtal').html('Loading..');
			
			if($('#checkboxes-portal').prop('checked') === true){
				var portal = $('#checkboxes-portal').val();					
			}else{
				 var portal = '';
			}	
			
			if($('#checkboxes-activated').prop('checked') === true){
				var activated = $('#checkboxes-activated').val();					
			}else{
				 var activated = '';
			}	
			
			if($('#checkboxes-request').prop('checked') === true){
				var request = $('#checkboxes-request').val();					
			}else{
				 var request = '';
			}	
			
			if($('#checkboxes-recall').prop('checked') === true){
				var recall = $('#checkboxes-recall').val();					
			}else{
				 var recall = '';
			}	
					
			var reseller_id = $('#reseller_id').val();					
			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'webmanager/reseller/savePortal',
				  
				  data: {
					  portal: portal,
					  activated: activated,
					  request: request,
					  recall: recall,
					  reseller_id: reseller_id,
				  },
				  
				  success: function(data) {
					console.log(data);
					if (data == 'success'){
						$('#portalsuccess_holder').html('<div class="alert alert-success fade in" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>Changes successfully saved.</div>');
						setTimeout(function(){
							$('#managePortal').modal('hide');
							$('#save_prtal').removeClass('disabled');
							$('#save_prtal').html('Save changes');
						}, 1000);
						
					}else{
						$('#portalsuccess_holder').html('<div class="alert alert-danger fade in" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>Changes not saved. Please try again.</div>');
					}
				  }
				  
			});
	}
	
	
	
	
	function submitSubject(id){
		$('.form_subj').find('button.btn-primary').html('Loading..').addClass('disabled');
		var subb = $('#subject_name').val();
		if (subb == ''){
			bootbox.alert('<p class="lead">Please add subject</p>');
		}else{
		
			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'webmanager/emailer/update_subject',
				  
				  data: {
					  subject: subb,
					  id: id
					  
				  },
				  
				  success: function(data) {
						console.log(data);
						$('#subjj').html(subb);
						$('.form_subj').find('button.btn-primary').html('Saved').removeClass('disabled');
						setTimeout(function(){
							$('.form_subj').hide();
							$('#subjj').show();
							$('.form_subj').find('button.btn-primary').html('Submit');							
						}, 1000);
				  }
				  
			});
		
		
		}
	}

	function submitSMSMessage(toggle_p,a_link,guidance_val,task_id,btn){
		//var btnsub = $(event.target);
		//btnsub.addClass('disabled');
	
		var guidance = $(guidance_val).val();
		if (guidance == ''){
			bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Please add message</p>');
			return false;
		}
		$.ajax({
			  type: "POST",
			  
			  url: base_url + 'webmanager/smscenter/update_message',
			  
			  data: {
				  text: guidance,
				  id: task_id
			  },
			  
			  success: function(data) {
				 console.log(data);
				if (data == 'success'){
					$('#'+btn).attr('data-original-title',guidance);
					$('#'+toggle_p).slideUp('slow');
					$('#'+a_link).html(guidance);
					$('#'+a_link).delay(600).fadeIn();
					//btnsub.removeClass('disabled');
				}
				else{
					bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Something went wrong! Please try to refresh page</p>');
					return false;
				}
	
			  }
			  
		});
		
	}//submitSMSMessage
	
	
	
	

	
	<?php if ($this->uri->segment(3) == '' && ($this->uri->segment(2) == 'recall' || $this->uri->segment(2) == 'continuity')){?>
	//activate last visited tab
	 $(document).ready(function() {
		if(location.hash) {
		   $('a[href=' + location.hash + ']').tab('show');
		}
	  $(document.body).on("click", "a[data-toggle]", function(event) {
		  if (this.getAttribute("href") !='#'){
			location.hash = this.getAttribute("href");
			return false;
		  }
	  });
	});
	  $(window).on('popstate', function() {
		var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
		$('a[href=' + anchor + ']').tab('show');
	  });
	<?php } ?>

	
	
	
    </script>
    <style>
		.content_text:hover{
			cursor: pointer;
			color: #428bca;
		}
		.edit_content{
			border-radius: 0;
			border: 0;
			padding-left: 0;
		}

	    #arrange_arrows{
			cursor: move;
		}
		
	</style>




    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/jquery-ui.min.js"></script>

    <!-- JavaScript angularjs -->
	<script src="<?php echo base_url() ?>assets/plugins/angularjs/angular.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.4.8/angular-sanitize.js"></script>	<script src="<?php echo base_url() ?>assets/plugins/angularjs/app.js"></script>



	<!-- Scripts -->
	<!-- <script type="text/javascript" src="//code.angularjs.org/1.5.8/angular.min.js"></script> -->
	<script type="text/javascript" src="//cdn.quilljs.com/1.1.5/quill.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/quill/'; ?>ng-quill.js"></script>

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/bootstrap/'; ?>ui-bootstrap-tpls-2.5.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/bootstrap/'; ?>ui-bootstrap-dialogs.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/bootstrap/'; ?>dialogs.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/ng-sortable/'; ?>sortable.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-vertilize/'; ?>angular-vertilize.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>moment-precise-range.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>countdown.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>moment-countdown.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-moment/1.0.1/angular-moment.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-drag-and-drop-lists/'; ?>angular-drag-and-drop-lists.min.js"></script> 

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'; ?>summernote.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'; ?>angular-summernote.js"></script>
	
	<script src="<?php echo base_url().'assets/plugins/angularjs/angular-file-upload/'; ?>dist/angular-file-upload.min.js"></script>

	<script src="<?php echo base_url().'assets/plugins/angularjs/ngToast/'; ?>angular-animate.min.js"></script>
	<script src="<?php echo base_url().'assets/plugins/angularjs/ngToast/'; ?>ngToast.min.js"></script>	




</body>

</html>

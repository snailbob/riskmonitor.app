<?php if(!isset($_SESSION['logged_admin'])){redirect(base_url().'webmanager');} ?>

<?php
	//variables
	$privileges = $_SESSION['admin_privileges'];

//	if($_SESSION['admin_privileges'] != 'admin' && $this->uri->segment(2) != 'dashboard' && $this->uri->segment(3) != 'changePassword' && $this->uri->segment(3) != 'manageEmail' && !in_array($this->uri->segment(2), $privileges) && !in_array($this->uri->segment(3), $privileges)){
//
//		$this->session->set_flashdata('error', 'Page not available.');
//		redirect('webmanager/dashboard');
//	}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CrisisFlo | Web Manager</title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?>
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
		<link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">




    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Style quilljs -->
    <link rel="stylesheet" href="//cdn.quilljs.com/1.1.5/quill.snow.css">
    <link rel="stylesheet" href="//cdn.quilljs.com/1.1.5/quill.bubble.css">

    <!-- Style croppie -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.0/croppie.css
"> 
    <!-- Style angular-summernote -->
     <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'.'summernote.css'; ?>"> 



    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/2/js/plugins/cropper/dist/cropper.min.css" type="text/css">


    <!-- Popup css-->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->


    <script type="text/javascript" language="javascript">
        var base_url='<?php  echo base_url(); ?>';

		var uri_1 = "<?php echo $this->uri->segment(1) ?>";
		var uri_2 = "<?php echo $this->uri->segment(2) ?>";
		var uri_3 = "<?php echo $this->uri->segment(3) ?>";
		var uri_4 = "<?php echo $this->uri->segment(4) ?>";
		var uri_5 = "<?php echo $this->uri->segment(5) ?>";
		var uri_6 = "<?php echo $this->uri->segment(6) ?>";
                
    </script>

</head>

<body>


    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="padding-bottom: 20px;">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-xs preloadThis" href="<?php echo base_url()?>webmanager"><img src="<?php echo base_url()?>assets/2/img/logo.png" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url()?>webmanager"><img src="<?php echo base_url()?>assets/2/img/logo.png" height="48" style="margin-top: 0px;"></a>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 hidden-xs" >
                <h3 class="big text-primary" style="text-align: center">Web Manager Centre</h3>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 visible-xs" >
                <h4 class="big text-primary" style="text-align: center">Web Manager Centre</h4>
            </div>

			<?php /*GET THE DATA FROM THE SESSION**************************/

				//active case report count
				$num_report=$this->master_model->getRecordCount('case_master',array('crt_id'=>$this->session->userdata('logged_crt_login_id'),'cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'0'));

				$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');

            /*********************************************************/?>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#"></a>
                    </li>
                   <li class="page-scroll"> <a href="<?php echo base_url() ?>webmanager" class="visible-xs  active"><i class="fa fa-dashboard"></i> Dashboard</a></li>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>webmanager/coordinator/manage" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-user"></i> Manage CC</a></li>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>webmanager/organization/manage" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-building-o"></i> Manage Organization</a></li>

                    <li class="dropdown hidden-sm hidden-xs">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>webmanager/settings/changePassword"><i class="fa fa-key"></i> Change Password</a></li>

						<?php if($_SESSION['logged_admin'] == 'admin'){?>
                        <li><a href="<?php echo base_url() ?>webmanager/settings/manageEmail"><i class="fa fa-info"></i> Manage Email ID</a></li>
                        <?php } ?>


                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>webmanager/dashboard/logout"><i class="fa fa-sign-out"></i> Logout</a></li>


                      </ul>
                    </li>

                    <li class="visible-xs"><a href="<?php echo base_url(); ?>webmanager/settings/changePassword"><i class="fa fa-key"></i> Change Password</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>webmanager/settings/manageEmail"><i class="fa fa-info"></i> Manage Email ID</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url();?>webmanager/dashboard/logout"><i class="fa fa-sign-out"></i> Logout</a></li>





                    <li class="dropdown dropdown-large visible-sm"><!--menu for small devices-->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>

                        <ul class="dropdown-menu dropdown-menu-large row">
                            <li class="col-sm-6">
                                <ul>
                                       <li> <a href="<?php echo base_url(); ?>webmanager" class="dropdown-header preloadThis"><i class="fa fa-dashboard"></i> Dashboard</a></li>

                                    <li class="divider"></li>

                                       <li> <a href="<?php echo base_url(); ?>webmanager/coordinator/manage" class="dropdown-header preloadThis"><i class="fa fa-user"></i> Manage CC</a></li>

                                    <li class="divider"></li>

                                       <li> <a href="<?php echo base_url(); ?>webmanager/organization/manage" class="dropdown-header preloadThis"><i class="fa fa-building-o"></i> Manage Organization</a></li>




                                </ul>
                            </li>



                            <li class="col-sm-6">
                                <ul>


                                   <li> <a href="<?php echo base_url(); ?>webmanager/smscenter/manage" class="dropdown-header preloadThis"><i class="fa fa-mobile"></i> SMS Center</a></li>

                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-gear"></i> Settings</li>

                                    <li><a href="<?php echo base_url(); ?>webmanager/settings/changePassword" class="preloadThis">Change Password</a></li>
                                    <li><a href="<?php echo base_url() ?>webmanager/settings/manageEmail" class="preloadThis">Manage Email ID</a></li>
                                    <li><a href="<?php echo base_url() ?>webmanager/settings/resetdb" class="preloadThis">Reset Database</a></li>

                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url();?>webmanager/dashboard/logout" class="dropdown-header"><i class="fa fa-sign-out"></i> Logout</a></li>
                                </ul>
                            </li>




                        </ul>

                    </li><!---.end of large dropdown-->

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container-fluid" style="background: #fff;"><?php //main_container ?>

        <div class="row">

            <?php if($this->uri->segment(2) != 'recall') {?> 
            <div class="col-lg-2 col-md-2 hidden-sm hidden-xs sidebar_nav scrollable">
            	<div class="text-center" style="padding-top: 20px;">
            	<!--<img  class="img-rounded img-thumbnail" src="<?php echo base_url()?>assets/2/img/icon-user-default.png" width="130" height="130">-->
                    <p class="text-muted">
                    	<em><i class="fa fa-key"></i> Logged in as</em>
                        </p>
                    <p class="lead"><?php echo $_SESSION['logged_admin']; ?></p>
                </div>
                <div class="main_nav">
                    <a href="<?php echo base_url() ?>webmanager" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="dashboard"){echo "active";}?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>

                	<?php if(in_array('customers', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/customers" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="customers"){echo "active";}?>"><i class="fa fa-user fa-fw"></i> Manage Customers</a>
					<?php } ?>



                	<?php if(in_array('coordinator', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/coordinator/manage" class="hidden list-group-item preloadThis <?php if($this->uri->segment(2)=="coordinator"){echo "active";}?>"><i class="fa fa-user fa-fw"></i> Manage CC</a>
					<?php } ?>

                	<?php if(in_array('organization', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/organization/manage" class="hidden list-group-item preloadThis <?php if($this->uri->segment(2)=="organization"){echo "active";}?>"><i class="fa fa-building-o fa-fw"></i> Manage Org</a>
					<?php } ?>

                	<?php if(in_array('message_validation', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/message_validation/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="message_validation"){echo "active";}?>"><i class="fa fa-envelope fa-fw"></i> Validation Message</a>
                    <?php } ?>
                    
                    <a href="<?php echo base_url()?>webmanager/popups/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="popups"){echo "active";}?>"><i class="fa fa-info-circle fa-fw"></i> Pop up Message</a>

                	<?php if(in_array('smscenter', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/smscenter/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="smscenter"){echo "active";}?>"><i class="fa fa-mobile fa-fw"></i> SMS Center</a>
					<?php } ?>

                	<?php if(in_array('videocenter', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/videocenter/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="videocenter"){echo "active";}?>"><i class="fa fa-video-camera fa-fw"></i> Video Center</a>
					<?php } ?>

                	<?php if(in_array('customer', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/customer/manage" class="hidden list-group-item preloadThis <?php if($this->uri->segment(2)=="customer"){echo "active";}?>"><i class="fa fa-bullseye fa-fw"></i> Manage Customer</a>
					<?php } ?>

                	<?php if(in_array('stripe', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/stripe/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="stripe"){echo "active";}?>"><i class="fa fa-cc-stripe fa-fw"></i> Manage Stripe Customer</a>
					<?php } ?>

                	<?php if(in_array('faq', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/faq/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="faq"){echo "active";}?>"><i class="fa fa-question-circle fa-fw"></i> Manage FAQs</a>
					<?php } ?>

                	<?php if(in_array('emailer', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/emailer/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="emailer"){echo "active";}?>"><i class="fa fa-envelope-square fa-fw"></i> Outgoing Emails</a>
					<?php } ?>

                	<?php if(in_array('recall', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/recall" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="recall"  && ($this->uri->segment(3)=="" || $this->uri->segment(3)=="index")){echo "active";}?>"><i class="fa fa-history fa-fw"></i> Task templates</a>
					<?php } ?>

                	<?php if(in_array('adminaccess', $privileges)){?>
                    <a href="<?php echo base_url()?>webmanager/adminaccess" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="adminaccess"){echo "active";}?>"><i class="fa fa-lock fa-fw"></i> Manage Admin</a>
					<?php } ?>


                    <a href="javascript:void(0);" class="list-group-item toggleCaretSet" onClick="$('.contents_nav').slideToggle();$('.preincidentItems').slideUp()"><i class="fa fa-file-text-o fa-fw"></i> Contents <i class="fa fa-caret-up pull-right"></i></a>

                        <div class="contents_nav" <?php if($this->uri->segment(2) == "contents"){echo ' style="display:block;"';} else {echo ' style="display:none;"'; }?>>


                            <a href="<?php echo base_url()?>webmanager/contents/aboutus" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "aboutus" || $this->uri->segment(3)== "banner"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> About Us</a>

                            <a href="<?php echo base_url()?>webmanager/contents/banners" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "banners" || $this->uri->segment(3)== "banner"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> Banner Texts</a>

                            <a href="<?php echo base_url()?>webmanager/contents/easiestway" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "easiestway"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> Easiest Way</a>

                            <a href="<?php echo base_url()?>webmanager/contents" class="list-group-item preloadThis settingChild <?php if(($this->uri->segment(2)== "contents" && $this->uri->segment(3)== "") || $this->uri->segment(3)== "alternate" || $this->uri->segment(3)== "manage_alternates"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> Our Solutions</a>

                            <a href="<?php echo base_url()?>webmanager/contents/collab" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "collabs" || $this->uri->segment(3)== "collab" || $this->uri->segment(3)== "manage_collabs"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> Collab Features</a>

                            <a href="<?php echo base_url()?>webmanager/contents/threeparts" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "threeparts" || $this->uri->segment(3)== "manage_threeparts" || $this->uri->segment(3)== "threepart"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> More Reasons Why</a>

                            <a href="<?php echo base_url()?>webmanager/contents/terms" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "terms"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> Terms of Use</a>

                            <a href="<?php echo base_url()?>webmanager/contents/privacy" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)== "privacy"){echo "active";}?>"><i class="fa fa-file-text fa-fw"></i> Privacy Policy</a>


                        </div>


                    <a href="javascript:void(0);" class="list-group-item toggleCaretSet" onClick="$('.settings_nav').slideToggle();$('.preincidentItems').slideUp()"><i class="fa fa-gear fa-fw"></i> Settings <i class="fa fa-caret-up pull-right"></i></a>

                        <div class="settings_nav" <?php if($this->uri->segment(2)=="settings"){echo ' style="display:block;"';} else {echo ' style="display:none;"'; }?>>
                            <a href="<?php echo base_url()?>webmanager/settings/changePassword" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="changePassword"){echo "active";}?>"><i class="fa fa-key fa-fw"></i> Change Password</a>

                            <?php if($_SESSION['logged_admin'] == 'admin'){?>
                            <a href="<?php echo base_url()?>webmanager/settings/manageEmail" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="manageEmail"){echo "active";}?>"><i class="fa fa-info fa-fw"></i> Manage Email ID</a>
                            <?php } ?>

                            <a href="<?php echo base_url()?>webmanager/settings/rate" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="rate"){echo "active";}?>"><i class="fa fa-tag fa-fw"></i> Single User Rate</a>

                            <?php if(in_array('resetdb', $privileges)){?>
                            <a href="<?php echo base_url()?>webmanager/settings/resetdb" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="resetdb"){echo "active";}?>"><i class="fa fa-archive fa-fw"></i> Reset Database</a>
                            <?php } ?>
                        </div>
                </div>







            </div>
            <?php } ?>

            <!--main container-->
            <div class="<?php echo ($this->uri->segment(2) != 'recall') ? 'col-md-10' : 'col-md-12'; ?> col-xs-12 col-sm-12 scrollable main_content" style="border-left: 1px solid #e7e7e7">

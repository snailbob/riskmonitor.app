<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->


<?php
    //variables
    $privileges = $_SESSION['admin_privileges'];
?>

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1> Crisis FLO Admin Dashboard</h1>

      <ol class="breadcrumb">

        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>

      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 




</div>

<!-- /.row --> 

<!-- end PAGE TITLE AREA -->



                

                <div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>



                	<?php if(in_array('reseller', $privileges)){?>
                    
                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">
                                    <h4><i class="fa fa-briefcase fa-fw"></i>Reseller's Request Activation</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-body">


                            <?php 

                            if(count($reseller_req)>0){
								
									foreach($reseller_req as $r => $value){
										
										$str='';

										$organisations = $this->master_model->getRecords('organization_master',array('cc_id'=>$value['login_id']));

										if(count($organisations)>0){

											for($i=0; $i < count($organisations); $i++) {

												if($i<(count($organisations)-1)){$suffix=', ';}else{$suffix='';}
												$str .= $organisations[$i]['organization_name'].$suffix;
											}

										}
										
							?>
                            
                            

<!-- Modal -->
<div class="modal fade" id="ccdetails<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CC Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_mobile']); ?></span></p>
			<p>Organization: <span class="text-muted"><?php if($str!=''){echo $str;}else{echo "NA";}?></span></p>
			<p>User Type: <span class="text-muted"><?php echo  $value['user_type']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
        


<?php if($str == ''){?>
<!-- Modal -->
<div class="modal fade" id="mydelccModal<?php echo $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="lead">Do you really want to delete Coordinator <?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']);  ?>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url().'webmanager/trialcustomers/delete/'.$value['login_id']; ?>" class="btn btn-primary">OK</a>
      </div>
    </div>
  </div>
</div>
<?php } ?>                            
                            
                            
                            <?php			
									}
								
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-striped" id="response-table">

                                <thead>

                                    <tr>

                                        <th width="25%">First Name</th>

                                        <th width="25%">Last Name</th>

                                        <th width="30%">Organizations</th>

                                        <th width="20%">Reseller</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>
                                
                                <?php

									foreach($reseller_req as $r => $value){
										
										$str='';

										$organisations = $this->master_model->getRecords('organization_master',array('cc_id'=>$value['login_id']));

										if(count($organisations)>0){

											for($i=0; $i < count($organisations); $i++) {

												if($i<(count($organisations)-1)){$suffix=', ';}else{$suffix='';}
												$str .= $organisations[$i]['organization_name'].$suffix;
											}

										}

								?>

                                    <tr>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_last_name']);  ?></td>

                                        <td><?php if($str!=''){echo $str;}else{echo "NA";}?></td>

                                        <td><?php
										
										//get reseller
										$reseller = $this->master_model->getRecords('reseller_master',array('login_id'=>$value['reseller_id']));
										
										echo $this->master_model->decryptIt($reseller[0]['first_name']).' '.$this->master_model->decryptIt($reseller[0]['last_name']);
										?></td>

                                    <td>

       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>webmanager/reseller/activate/<?php echo $value['reseller_id'].'/'.$value['login_id']; ?>">Activate</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#ccdetails<?php echo  $value['login_id']; ?>">Details</a> 
            </li>

			<?php if($str==''){?>
            <li>
                <a href="#" data-toggle="modal" data-target="#mydelccModal<?php echo $value['login_id']; ?>">Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>webmanager/organization/add/<?php echo $value['login_id']; ?>">Add Org</a>
            </li>
			<?php } ?>


          </ul>
        </div>   
        
                                    </td>

                                    </tr>

								<?php 

									}

								?>

                                              </tbody>

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php
								}
								else{
								
									echo '<p class="text-center text-muted" style="margin-top: 20px;">No activation request.</p>';
								}
								?>


                            </div>

                        </div>

                        <!-- /.panel -->

					<?php }  ?>



                    

                	<?php if(in_array('coordinator', $privileges)){?>


                        <div class="panel panel-default" >

                            <div class="panel-heading">

                                <div class="panel-title">
                                    <h4><i class="fa fa-user fa-fw"></i>Awaiting CC Acceptance</h4>
                                </div>
                                <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">


                            <?php 

                            if(count($all_cc) > 0){
								
									foreach($all_cc as $r => $value){

									  if(($value['activated'] == "yes" && $value['reseller_id'] != "0")||($value['activated'] == "no" && $value['reseller_id'] == "0")){ 


										$str='';

										$organisations = $this->master_model->getRecords('organization_master',array('cc_id'=>$value['login_id']));

										if(count($organisations)>0){

											for($i=0;$i<count($organisations);$i++){
												if($i<(count($organisations)-1)){$suffix=', ';}else{$suffix='';}
												$str.=$organisations[$i]['organization_name'].$suffix;
											}

										} 
										
							?>
                                        
        
<!-- Modal -->
<div class="modal fade" id="ccdetails<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CC Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_mobile']); ?></span></p>
			<p>Organization: <span class="text-muted"><?php if($str!=''){echo $str;}else{echo "NA";}?></span></p>
			<p>User Type: <span class="text-muted"><?php echo  $value['user_type']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
        
        
        
<?php if($str==''){?>
<!-- Modal -->
<div class="modal fade" id="mydelccModal<?php echo $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="lead">Do you really want to delete Coordinator <?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']);  ?>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url().'webmanager/trialcustomers/delete/'.$value['login_id']; ?>" class="btn btn-primary">OK</a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
                                 
                                        
                             <?php			
										
									  }
									}
								
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="25%">First Name</th>

                                        <th width="25%">Last Name</th>

                                        <th width="30%">Organizations</th>

                                        <th width="20%">Type</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>
                                
                                <?php

									foreach($all_cc as $r => $value){

									  if(($value['activated'] == "yes" && $value['reseller_id'] != "0")||($value['activated'] == "no" && $value['reseller_id'] == "0")){ 


										$str='';

										$organisations = $this->master_model->getRecords('organization_master',array('cc_id'=>$value['login_id']));

										if(count($organisations)>0){

											for($i=0;$i<count($organisations);$i++){
												if($i<(count($organisations)-1)){$suffix=', ';}else{$suffix='';}
												$str.=$organisations[$i]['organization_name'].$suffix;
											}

										}

								?>

                                    <tr>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?></td>
                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_last_name']);  ?></td>

                                        <td><?php if($str!=''){echo $str;}else{echo "NA";}?></td>

                                        <td><?php echo ucfirst($value['user_type']); ?></td>

                                    <td>

       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>webmanager/trialcustomers/update/<?php echo  $value['login_id']; ?>">Edit</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#ccdetails<?php echo  $value['login_id']; ?>">Details</a> 
            </li>

			<?php if($str==''){?>
            <li>
                <a href="#" data-toggle="modal" data-target="#mydelccModal<?php echo $value['login_id']; ?>">Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>webmanager/organization/add/<?php echo $value['login_id']; ?>">Add Org</a>
            </li>
			<?php } ?>


          </ul>
        </div>   
                                    </td>

                                    </tr>

								<?php 

									 }
									}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php
								}
								else{
								
									echo '<p class="text-center text-muted">No awaiting Crisis Coordinator</p>';
								}
								?>






                            </div>

                        </div>

                        <!-- /.panel -->
					<?php }  ?>



                	<?php if(in_array('smscenter', $privileges)){?>
                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa fa-mobile fa-fw"></i>Organizations with Zero SMS Credit</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">
							<?php
                            if(count($org_list)>0)

                            {
								?>


							<div class="table-responsive">

							<table class="table table-hover table-striped" id="stake-table">

                                <thead>

                                    <tr>

                                        <th width="40%">Organization Name</th>

                                        <th width="40%">Co-ordinator</th>

                                        <th width="20%">SMS Credits</th>

                                        <th> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

									foreach($org_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['organization_name']; ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); ?></td>
                                        
                                    	<td><?php echo $value['sms_credit'].' $'; ?></td>

                                    	<td>

                                        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>webmanager/smscenter/update/<?php echo  $value['organization_id']; ?>">Allocate Credit</a> 

                                    	</td>

                                    </tr>

								<?php 

									}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->



								<?php
								}
								else{
								
									echo '<p class="text-center text-muted">No Organization with Zero SMS Credit</p>';
								}
								?>






                            </div>

                        </div>

                        <!-- /.panel -->
					<?php }  ?>




                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa fa-paperclip fa-fw"></i>Suspicious Documents</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">
							<?php
                            if(count($docs)>0){
							
							

								foreach($docs as $d => $dvalue){
									
									if($dvalue['scan_result'] != ''){
										
								?>
                                
<!-- Modal -->
<div class="modal fade" id="mydelccModal<?php echo $dvalue['file_upload_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="lead">Do you really want to delete selected file?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url().'webmanager/dashboard/delete/'.$dvalue['file_upload_id'].'/'.$dvalue['file_upload_name']; ?>" class="btn btn-primary">OK</a>
      </div>
    </div>
  </div>
</div>

                                <?php		
										
									}
								}
							
							?>


							<div class="table-responsive">

							<table class="table table-hover table-striped" id="response-inis-table">

                                <thead>

                                    <tr>

                                        <th width="100%">Document Name</th>

                                        <th> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

									foreach($docs as $d => $dvalue){
										
										if($dvalue['scan_result'] != ''){
								?>

                                                        

                                    <tr>

                                    	<td>
											<?php
										
											$fileext = explode ('.',$dvalue['file_upload_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
											echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
											echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
											echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											
											 echo substr($dvalue['file_upload_name'],13,50);?>                                        
                                        </td>

                                    	<td>
                                            <a href="#" class="btn btn-sm btn-default" data-toggle="modal" data-target="#mydelccModal<?php echo $dvalue['file_upload_id']; ?>">Delete</a>
                                            
                                         </td>


                                    </tr>

								<?php 
										}
									}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->



								<?php
								}
								else{
								
									echo '<p class="text-center text-muted">No Suspicious documents detected.</p>';
								}
								?>






                            </div>

                        </div>

                        <!-- /.panel -->


                    </div>



                </div><!--.row -->

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/2/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    

</head>

<body style="background: #efefef;">



    <div class="container">

        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "<?php echo base_url() ?>"; ?>"><img src="<?php echo base_url()?>assets/2/img/logo.png" style="height: 60px;" ></a>

                </div>

                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Admin Login</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

    
                        <?php if($error !=''){ ?> 
    
                            <div class="alert alert-danger alert-dismissable fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <?php echo $error ?>
                                      </div>
                        <?php } ?>	
    
                      

                        <div id='login_form'>
                            <form action='' method='post' name="admin-login" id="admin-login">

                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" type='text' name='user_name' id='user_name' />
                                    <span class="text-red"></span>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" type='password' name='pass_word' id='pass_word'/>							
                                    <span class="text-red"></span>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" name="btn_admin_login" id="btn_admin_login">Login</button>
                                </div>

                            </form>

                            <p class="">
                                <a href="<?php echo base_url().'webmanager/dashboard/forgotpassword' ?>">Forgot your password?</a>
                            </p>
                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>

	<script>
        $(document).ready(function(){
            $('#admin-login').on('submit', function(){
				$(this).find('div.form-group').removeClass('has-error');
                $(this).find('span').html('');
				if($('#user_name').val() == ''){
					$('#user_name').focus().closest('div').addClass('has-error');
					$('#user_name').siblings('span').html('Required field.');
					return false;
				}
				else if($('#pass_word').val() == ''){
					$('#pass_word').focus().closest('div').addClass('has-error');
					$('#pass_word').siblings('span').html('Required field.');
					return false;
				}
				else{
					$(this).find('.btn-primary').addClass('disabled').html('Loading..');
				}
			});
            $(".user_name").focus();
        });
    </script>
</body>

</html>

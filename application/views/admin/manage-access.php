                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage States</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage States</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    
                            <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
                        </div>

                        <?php    

						} 

						if($this->session->flashdata('error')!=""){

						?>

                        <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    
                            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
                        </div>

                        <?php

						} 

						?>
                        

                        
    
                        <a class="btn btn-primary pull-right add_admin_btn">Add Admin Access</a> 
    
                        <div class="clearfix" style="margin-bottom: 15px;"></div>
                        

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Manage Admin Access</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="40%">Username</th>

                                        <th width="40%">Email</th>
                                        <th width="20%">Status</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($admins)>0)

								{

									foreach($admins as $r => $value)

									{

							?>

                                    <tr>

                                    	<td><?php echo $value['user_name']; ?></td>

                                    	<td><?php echo $value['recovery_email']; ?></td>

                                    	<td>
										
											<?php
											
											if ($value['pass_word'] != ''){
												echo '<span class="text-success"><i class="fa fa-circle"></i> Completed</span>';
											}
											else{
												echo '<span class="text-red"><i class="fa fa-circle"></i> Awaiting</span>';
											}
											
											?>
                                        
                                        </td>

                                    	<td>
                                        <?php
											$privil = unserialize($value['privileges']);
											
											if(!is_array($privil)){
												$privil = array();
											}
											$priv_text = '[';
											if(count($privil) > 0){
												$i = 1;
												foreach($privil as $privs){
													$priv_text .= '"'.$privs.'"';
													if($i < count($privil)){
														$priv_text .= ',';
													}
													
													$i++;
												}
											}
											$priv_text .= ']';
										?>

        <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="javascript:;" class="update_admin_btn" data-id="<?php echo $value['id']; ?>" data-username="<?php echo $value['user_name']; ?>" data-email="<?php echo $value['recovery_email']; ?>" data-privileges='<?php echo $priv_text; ?>'>Edit</a>
            </li>
            <li>
                <a href="javascript:;" class="delete_admin_btn" data-id="<?php echo $value['id'];?>">Delete</a>
            </li>

          </ul>
        </div>   
        
                                       </td>

                                    </tr>

								<?php 


 
									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					

<!-- Modal -->
<div class="modal fade" id="adminAccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		
        <form id="admin_access_form" novalidate="novalidate">
        	<div class="form-group">
            	<label>Username</label>
                <input type="text" class="form-control" name="username" />
                <input type="hidden" class="form-control" name="userid" />
                <span class="text-red"></span>
            </div>
        	<div class="form-group">
            	<label>Email</label>
                <input type="email" class="form-control" name="email" />
                <span class="text-red"></span>
            </div>
            
        	<div class="form-group">
            	<label>Privileges</label>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="customers">
                    Manage Customers
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="trialcustomers">
                    Trial Customers
                  </label>
                </div>
                
                <div class="checkbox hidden">
                  <label>
                    <input type="checkbox" name="privileges[]" value="coordinator">
                    Manage CC
                  </label>
                </div>
                <div class="checkbox hidden">
                  <label>
                    <input type="checkbox" name="privileges[]" value="organization">
                    Manage Organization
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="help">
                    Help Function
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="message_validation">
                    Validation Messages
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="packs">
                    Input Packs
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="smscenter">
                    SMS Center
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="videocenter">
                    Video Center
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="reseller">
                    Manage Consultants
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="customer">
                    Manage Customer
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="stripe">
                    Manage Stripe Customer
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="modules">
                    Manage Modules
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="continuity">
                    Continuity Tasks
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="faq">
                    Manage FAQs
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="states">
                    Manage States
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="emailer">
                    Outgoing Emails
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="recall">
                    Manage Recall
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="adminaccess">
                    Manage Admin
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="privileges[]" value="resetdb">
                    Reset DB
                  </label>
                </div>
                <span class="text-red"></span>
            </div>
            
            
        	<div class="form-group">
                <button class="btn btn-primary">Save changes</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



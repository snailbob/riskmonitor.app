                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Recall Input Pack</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage <?php echo $packname[0]['name'] ?> Recall Input Pack</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12">
                          <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addRPackStep" style="margin-left: 5px;">Add Step</button>
     
                          
<!-- Modal -->
<div class="modal fade" id="addRPackStep" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- Text input-->
        <div class="form-group">
          <label class="control-label" for="step_name">Step</label>  
          <input id="step_name" name="step_name" type="text" placeholder="eg. Step 1: Notify" class="form-control input-md">
          <span class="help-block task_err text-red" style="display: none;"></span>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveNewRPackCat('<?php echo $pack_id?>');">Save changes</button>
      </div>
    </div>
  </div>
</div>      
               
<!-- Modal updatestep-->
<div class="modal fade" id="updateStep" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- Text input-->
        <div class="form-group">
          <label class="control-label" for="step_name">Step</label>  
          <input id="pack_id" name="pack_id" type="hidden" value="<?php echo $pack_id?>">
          <input id="step_id" name="step_id" type="hidden" value="">
          <input id="step_name" name="step_name" type="text" placeholder="eg. Step 1: Notification" class="form-control input-md">
          <span class="help-block task_err text-red" style="display: none;"></span>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveUpdateStep();">Save changes</button>
      </div>
    </div>
  </div>
</div>      
               
               


<!-- Modal -->
<div class="modal fade" id="myAddSubCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
        <p><br />
        
<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="add_cat">Category</label>  
  <input id="add_cat" name="add_cat" type="text" placeholder="Category" class="form-control input-md">
  <span class="help-block text-red" id="category_err"></span>  
</div>


<!-- Select Basic -->
<div class="form-group steps-drop hidden">
  <label class="control-label" for="select_step">Step</label>
    <select id="select_step" name="select_step" class="form-control">
      <option value="">Select</option>
      <?php
	  	if(count($packsteps)>0){
			$cat_count = 2;
			foreach($packsteps as $c=>$cat){
				echo '<option value="'.$cat_count.'">'.$cat['name'].'</option>';
				$cat_count++;
			}
		}
	  ?>
    </select>
  <span class="help-block step_err text-red"></span>  
</div>

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="savePackSubCat();">Save changes</button>
      </div>
    </div>
  </div>
</div>
                  


                        
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Task</h4>
        <p><br />

<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="text_task">Task Name</label>  
  <input id="text_task" name="text_task" type="text" placeholder="Task" class="form-control input-md">
  <span class="help-block task_err text-red" style="display: none;"></span>  
</div>


<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="text_task_guidance">Task Guidance</label>  
  <textarea id="text_task_guidance" name="text_task_guidance" type="text" placeholder="Task Guidance" class="form-control input-md" rows="5"></textarea>
  <span class="help-block task_guidance_err text-red" style="display: none;"></span>  
</div>


<!-- Select Basic -->
<div class="form-group hidden">
  <label class="control-label" for="select_pack_cat">Step</label>
    <select id="select_pack_cat" name="select_pack_cat" class="form-control">
      <option value="">Select</option>
      <?php
	  	if(count($packsteps)>0){
			$cat_count = 2;
			foreach($packsteps as $c=>$cat){
				echo '<option value="'.$cat_count.'">'.$cat['name'].'</option>';
				$cat_count++;
			}
		}
	  ?>
    </select>
  <span class="help-block category_err text-red" style="display: none;"></span>  
</div>

<!-- Select Basic -->
<div class="form-group" id="sub_cat">
</div>






        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="saveNewPackTask();">Save changes</button>
      </div>
    </div>
  </div>
</div>






                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>




                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4><i class="fa fa-history fa-fw"></i>Standby Tasks <span class="save_steps_indicate"></span></h4>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                        <div class="panel-body">
                        
                        	<div class="row">
                            
                            	<div class="col-md-3  col-lg-3 steps_holder">
                       
                                   
                            <ul class="nav nav-pills nav-stacked navigation" id="steporder" style="margin-bottom: 10px;">
                                        <?php
                                        if(count($packsteps)>0) {
            
                                            $i = 0;
                                            foreach($packsteps as $r => $value) { ?>
            
            
                                <li class="<?php if($i == 0) { echo 'active'; } ?>" id="ID_<?php echo $value['id']?>">
                                    <a data-toggle="tab" href="#steptab<?php echo $value['id'] ?>" class="text-info bg-info">
                                    <?php if($i != 0) { ?><i class="fa fa-arrows fa-fw" style="visibility: hidden; "></i><?php } else { echo '<i class="fa fa-user fa-fw" style="visibility: hidden; "></i>'; } ?> <?php echo $value['name']; ?> <i class="fa fa-angle-double-right" style="display: none;"></i></a>
                                </li>

                                        <?php $i++; } } ?>
                                   
                                
                
    
                            </ul>                                   

                                
                                </div>
                            
                            	<div class="col-md-9 col-lg-9 user-body">
                                
                                    
                                      <!-- Tab panes -->
                                      <div class="tab-content">
                                         
                                         
                                         

                            <?php 

                            if($packsteps>0)

                            {
								//$step_no = 2;
								$i = 0;
								foreach($packsteps as $ps=>$pcs){
								$step_no = $pcs['step_no'];
							?>


                            <div role="tabpanel" class="tab-pane <?php if($i == 0) { echo 	'active'; } ?>" id="steptab<?php echo $pcs['id'] ?>">

							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<thead>
                                
                                
                                    <tr>
                                    	<td colspan="6" style="background-color:#fff;">
                                        
                                        <h4>
                                        <span class="step-name">
                                            <a href="#" data-toggle="modal" data-target="#updateStep" onclick="addStepName('<?php echo $pcs['name']?>','<?php echo $pcs['id']?>');" title="click to edit" class="btn btn-default"><?php echo $pcs['name']?></a>
                                           <?php if($step_no != 2) { ?>
                                           <small style="display: none;"><a href="javascript:;" id="<?php echo 'del-rc-pack-cat-'.$pcs['id']?>" onclick="delRPackCat('<?php echo $pcs['id']?>','<?php echo 'del-rc-pack-cat-'.$value['id']?>');"><i class="fa  fa-trash-o fa-fw"></i></a></small>
                                           <?php } ?>
                                        </span>
                                        <span id="load_save_step"></span>
                          
                                              <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#myAddSubCat" style="margin-left:5px; width: 97px;" onclick="selStepForCat('<?php echo $step_no; ?>');">Add Category</button>
                                              
                                              <button type="button" class="btn btn-primary btn-sm pull-right <?php if (count($packsteps)== 0){ echo 'disabled'; }?>" data-toggle="modal" data-target="#myModal" style="margin-left: 5px; width: 97px;" onclick="selStep('<?php echo $step_no; ?>');">Add Task</button>
                                        
                                        </h4></td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#fff; width: 15px;"><i class="fa fa-arrows" style="visibility: hidden;"></i></th>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="50%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="20%" style="background-color:#fff;" class="text-center hidden">Category</th>
                                        <th style="background-color:#fff;"></th>
                                        <th style="background-color:#fff;"></th>
                                    </tr>
                                </thead>   
								<tbody id="draggablePanelList<?php if ($step_no != 2){ echo $step_no; }?>" class="tasks_draggable">
                                    
                                    <?php
									
									$countqq = 0;
									$curr_sub_c = '';
									
									${"step" . $step_no} = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id),'',array('category'=>'ASC','arrangement'=>'ASC'));
									
									if(count(${"step" . $step_no}) > 0){
								
									foreach (${"step" . $step_no} as $r=>$value){?>
                                    
                            
                                  <?php if ($value['category'] != 0){ //get subcategory
                        
									//$tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$value['step_no'],'id'=>$value['category'],'recall_pack_id'=>$value['recall_pack_id']));
						
                                    $tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$value['category']));
                                    if ($curr_sub_c != $tsk_cat[0]['id']){ ?>
                                    
                                  <tr class="info">
                                    <td colspan="6">
                                           <p id="toggle_stx<?php echo $i; ?>" style="display: none;"><textarea id="my_stext<?php echo $i; ?>" name="my_stext<?php echo $i; ?>" class="form-control" autofocus><?php echo $tsk_cat[0]['category_name']; ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitCategoryUpdate('toggle_stx<?php echo $i; ?>','empty_scat_label<?php echo $i; ?>','#my_stext<?php echo $i; ?>','<?php echo $tsk_cat[0]['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_stx<?php echo $i; ?>').hide();$('#empty_scat_label<?php echo $i; ?>').show();">Cancel</button></p>
                                    
                                        <h4 style="text-transform:uppercase; font-weight:bold;">
                                        
                                        <span class="category-name">
                                            <?php if($tsk_cat[0]['category_name'] !="Hazard Analysis"){ ?>
                                        
                                            <a id="empty_scat_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_stx<?php echo $i; ?>').show();" title="click to edit"><?php echo $tsk_cat[0]['category_name']; ?></a>
                                            <?php } else{ ?>
                                            <a href="javascript: void(0);" title="editing disabled"><?php echo $tsk_cat[0]['category_name']; ?></a>
                                            
                                            <?php } ?>
                                            <?php if($tsk_cat[0]['category_name'] !="Hazard Analysis"){ ?>
                                            <small style="display: none;"><a href="javascript: void(0);" id="del_btn_cat_<?php echo $countqq; ?>" class="tooltip-test " onclick="confirmDelete('<?php echo $tsk_cat[0]['id']?>','cf_recall_steps_category','del_btn_cat_<?php echo $countqq; ?>');"><i class="fa fa-trash-o"></i></a></small>
                                            <?php } ?>
                                            </span>
                                        
                                            <?php // echo $tsk_cat[0]['category_name']?>
                                            
                                            
                                            
                                            
                                            
                                        </h4>
                                    </td>
                                  </tr>
                                    
                                    <?php } } ?>
                                                            
                                    <tr id="ID_<?php echo $value['id']?>">



                                    	<td class="text-left">
                                        	<span id="arrange_arrows" style="display: none" title="move">
                                                <i class="fa fa-arrows"></i>
                                            </span>
                                        </td>

                                    	<td>
                                        
                                        <?php if($value['question'] =="Hazard Analysis"){ ?>
                                            <a href="javascript:;" class="text-muted" style="text-decoration: none; cursor: pointer" title="editing disabled"><?php echo $value['question']; ?></a>
                                        <?php } else{ ?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['question'] ?></a>
                                        <?php }?>
                                        
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['question'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitTaskUpdate('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                           
                                        </td>
                                        

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="task_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tg_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tg_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_tg_text<?php echo $i; ?>" name="my_tg_text<?php echo $i; ?>" class="form-control" rows="7"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tg_tx<?php echo $i; ?>','task_guide_label<?php echo $i; ?>','#my_tg_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tg_tx<?php echo $i; ?>').hide();$('#task_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="task_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tg_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tg_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_tg_text<?php echo $i; ?>" name="my_tg_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tg_tx<?php echo $i; ?>','task_guide_label<?php echo $i; ?>','#my_tg_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tg_tx<?php echo $i; ?>').hide();$('#task_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>
                                        
                                    	<td class="text-center hidden"><?php
											//get category name
											$the_cat = $this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$value['step_no'],'id'=>$value['category'],'recall_pack_id'=>$value['recall_pack_id']));
											$step_cats = $this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$value['step_no'],'deleted'=>'0','recall_pack_id'=>$value['recall_pack_id'])); //,'recall_pack_id'=>$value['recall_pack_id']
											
											if (count($the_cat) > 0){ ?>
                                                <a class="text-muted" id="category_holder_<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_cat<?php echo $i; ?>').show();" title="click to edit"><?php echo $the_cat[0]['category_name']; ?></a>
												
                                                <!-- Select Basic -->
                                                <div class="form-group" id="toggle_cat<?php echo $i; ?>" style="display: none;">
                                                    <select id="my_sel_cat<?php echo $i; ?>" name="my_sel_cat<?php echo $i; ?>" class="form-control">
                                                      <option value="0">Select</option>
                                                      
                                                      <?php foreach($step_cats as $sc=>$scats){ ?>
                                                      <option value="<?php echo $scats['id'] ?>" <?php if ($scats['id'] == $value['category']) { echo 'selected="selected"'; } ?>><?php echo $scats['category_name'] ?></option>
                                                      <?php } ?>
                                                      
                                                    </select>
                                                   <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="selectTaskCat('toggle_cat<?php echo $i; ?>','category_holder_<?php echo $i; ?>','#my_sel_cat<?php echo $i; ?>','<?php echo $value['id']; ?>');">Submit</button>
                                                   <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_cat<?php echo $i; ?>').hide();$('#category_holder_<?php echo $i; ?>').show();">Cancel</button>
                                                    
                                                </div>
                                                
                                                
                                                												
											<?php
                                            }
											else{ ?>
                                                <a class="text-muted" id="category_holder_<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_cat<?php echo $i; ?>').show();" title="click to edit">No Category</a>
                                                <!-- Select Basic -->
                                                <div class="form-group" id="toggle_cat<?php echo $i; ?>" style="display: none;">
                                                    <select id="my_sel_cat<?php echo $i; ?>" name="my_sel_cat<?php echo $i; ?>" class="form-control">
                                                      <option value="0">Select</option>
                                                      
                                                      <?php foreach($step_cats as $sc=>$scats){ ?>
                                                      <option value="<?php echo $scats['id'] ?>"><?php echo $scats['category_name'] ?></option>
                                                      <?php } ?>
                                                      
                                                    </select>
                                                   <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="selectTaskCat('toggle_cat<?php echo $i; ?>','category_holder_<?php echo $i; ?>','#my_sel_cat<?php echo $i; ?>','<?php echo $value['id']; ?>');">Submit</button>
                                                   <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_cat<?php echo $i; ?>').hide();$('#category_holder_<?php echo $i; ?>').show();">Cancel</button>
                                                    
                                                </div>
                                           
											<?php
                                            }
										?></td>
                                        
                                        
                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="enabled_btn_<?php echo $i; ?>" class="btn btn-success btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enabled" onclick="toggleDisable('<?php echo $value['id']?>','0','enabled_btn_<?php echo $i; ?>');" <?php if ($value['disabled']== '1') { echo 'style="display: none;"'; } ?>><i class="fa fa-check fa-fw"></i></button>
                                            
                                            <button id="disabled_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Disabled" onclick="toggleDisable('<?php echo $value['id']?>','1','disabled_btn_<?php echo $i; ?>');" <?php if ($value['disabled']== '0') { echo 'style="display: none;"'; } ?>><i class="fa fa-times fa-fw"></i></button>
                                        </td>  
                                    	<td class="text-center" style="vertical-align: middle">
                                            <button id="del_btn_<?php echo $i; ?>" class="btn btn-danger btn-sm tooltip-test <?php if($value['question'] =="Hazard Analysis"){ echo 'disabled'; } ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="confirmDelete('<?php echo $value['id']?>','cf_recall_guidance','del_btn_<?php echo $i; ?>');"><i class="fa fa-trash-o fa-fw"></i></button>
                                        </td>


                                    </tr>
									
									<?php
									
										//display matrix
										if($value['question'] == "Hazard Analysis"){ 
										
										$s3_assessment = array('','','','','','');
										
									?>
                                    
                                    <tr style="background-color: #fff;">
                                    	<td colspan="6">
                  		
                        
                        <div>
                      	<button class="btn btn-primary btn-sm save_stp3_table hidden" style="display: none;">Save</button>  
                
                
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Severity</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[0] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="hidden" name="step_3_table_id" value=""/>
                            	<input type="radio" name="severity" class="input_hidden" value="Minor" <?php if ($s3_assessment[0] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[0] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Moderate" <?php if ($s3_assessment[0] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[0] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Major" <?php if ($s3_assessment[0] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[0] == 'Serious') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Serious" <?php if ($s3_assessment[0] == 'Serious') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Serious</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Presence</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[1] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Rare" <?php if ($s3_assessment[1] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[1] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Some" <?php if ($s3_assessment[1] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[1] == 'Most') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Most" <?php if ($s3_assessment[1] == 'Most') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Most</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[1] == 'All') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="All" <?php if ($s3_assessment[1] == 'All') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">All</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">
                        
                        

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Likely Injury</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[2] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Rare" <?php if ($s3_assessment[2] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[2] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Some" <?php if ($s3_assessment[2] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[2] == 'Many') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Many" <?php if ($s3_assessment[2] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[2] == 'Certain') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Certain" <?php if ($s3_assessment[2] == 'Certain') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Certain</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Distribution</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[3] == 'Small') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Small" <?php if ($s3_assessment[3] == 'Small') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Small</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[3] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Some" <?php if ($s3_assessment[3] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[3] == 'Many') { echo 'selected'; }?>">

                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Many" <?php if ($s3_assessment[3] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[3] == 'National') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="National" <?php if ($s3_assessment[3] == 'National') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">National</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Identification</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[4] == 'Easy') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Easy" <?php if ($s3_assessment[4] == 'Easy') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Easy</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[4] == 'Possible') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Possible" <?php if ($s3_assessment[4] == 'Possible') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Possible</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[4] == 'Difficult') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Difficult" <?php if ($s3_assessment[4] == 'Difficult') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Difficult</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[4] == 'Undetectable') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Undetectable" <?php if ($s3_assessment[4] == 'Undetectable') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Undetectable</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Hazard</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[5] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Minor" <?php if ($s3_assessment[5] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[5] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Moderate" <?php if ($s3_assessment[5] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[5] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Major" <?php if ($s3_assessment[5] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[5] == 'Severe') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Severe" <?php if ($s3_assessment[5] == 'Severe') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Severe</span></p>
                            </label>
                          </div>
                        </div>
                        </div>
                        
                
                
                
                                                        
                                    
                                    </td>
                                </tr>
                                        
                                        
                                        <?php
										}
									?>
									
									
									
									
									
									
									
									
									
									<?php
									 
									$i++;
									$countqq++;
									$curr_sub_c = $value['category'];
									
									
									}//end foreach

								}else{ ?>
                                
                                    <tr>
                                    	<td class="text-center text-muted" style="vertical-align: middle" colspan="6">No Tasks added.</td>
                                    </tr>
                                    
                                    
								<?php }//end no task?>
									
                                    
                                </tbody>
                            </table>

                        </div> <!-- /.table-responsive -->
                    </div>                                         
                        
                        <?php 
									//$step_no++;
								}//end foreach steps
                        }
                        else{
                        
                            echo '<p class="text-center text-muted" style="margin-top: 20px;">No task available.</p>';
                        }
                        ?>

            
            
            
                                      </div><!--end of tab content-->
                                
                                </div>
                            
                            </div>

                        
                        </div><!--end of panel-body-->
                     </div>    




                    </div>



                </div><!--.row -->

					




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                
<?php
	$customer_type = $this->uri->segment(2);
?>
                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Crisis Co-ordinator</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Co-ordinator</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">


                    <div class="col-lg-12" style="height: 40px;">

                    <a class="btn btn-primary pull-right" href="<?php echo base_url();?>webmanager/<?php echo $customer_type; ?>/setupcc">Add New Customer</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><?php if($this->uri->segment(2) == 'customers') { echo '<i class="fa fa-user fa-fw"></i> Live '; } else { echo '<i class="fa fa-flash fa-fw"></i> Trial '; }?> Customers</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>
                                        <th width="20%">First Name</th>
                                        <th width="20%">Last Name</th>
                                        <th width="20%">Organizations</th>
                                        <th width="25%">CC Status</th>
                                        <th width="15%">Org Status</th>
                                        <th></th>
                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($all_cc)>0)

								{

									foreach($all_cc as $r => $value) {
  										$str='';


								?>

                                    <tr>

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_first_name']); ?></td>

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_last_name']);  ?></td>


                                        <td>
										<a href="<?php echo base_url().'webmanager/'.$customer_type.'/org_details/'.$value['organization_id'] ?>" title="click to view details"><?php echo $value['organization_name']; ?></a>
										<?php // if($str!=''){echo $str;}else{echo "NA";}?></td>

                                        
                                        <td>
											<?php 
			
												if($value['user_status'] == 1 ){
													$status = '<i class="fa fa-circle text-green"></i> <span class="text-green">CC Accepted</span>';
												} else {	
													$status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting CC Acceptance</span>';
												}
												
												echo  $status;
												
												//show remaining days for trial
												if($customer_type != 'customers'){
													$trialtext = 'Trial Not Applied';
													if($value['enabled'] == 'N'){
														$tdate = date_format(date_create($value['date_added']), 'M d, Y');
														$trialtext = 'Request from '.$tdate;
													}
													
													else {
														if($value['trial_end'] != '0000-00-00 00:00:00'){
																									
															// $date1= new DateTime(date('Y-m-d H:i:s'));
															// $date2= new DateTime($value['trial_end']);
                                                            // $remaining = $date1->diff($date2)->format("%d");
                                                         
                                                            $date1= time();
                                                            
															$date2= strtotime($value['trial_end']);

                                                            //Calculate the difference.
                                                            $difference = $date2 - $date1;

                                                            //Convert seconds into days.
                                                            $remaining = floor($difference / (60*60*24) );
                                                            $remaining = max($remaining,0);
                                                            
															if($remaining > 0){
																$trialtext = $remaining.' trial days remaining';
															}
															else{
																$the_date = date_format(date_create($value['trial_end']), 'M d, Y');
																$trialtext = 'Trial expired ('.$the_date.')';
															}
														}
													}
													
													echo '<br /><span class="small text-muted">'.$trialtext.'</span>';

												}
												
											?>
                                        </td>
                                        
                                        <td>
                                            <?php
                                                if($value['enabled'] == 'Y'){
                                                    echo '<span class="text-success">Enabled</span>';
                                                }
                                                else{
                                                    echo '<span class="text-red">Disabled</span>';
                                                }
                                            ?>
                                        </td>
                                        
                                        <td>



       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu">
            <li>
                <a href="<?php echo base_url().'webmanager/'.$customer_type.'/update/'.$value['login_id']; ?>">Update</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#ccdetails<?php echo  $value['login_id']; ?>">CC Details</a> 
            </li>
            <?php if($value['user_status'] == 0){?>
            <li>
                <a href="javascript: resendCC('<?php echo $value['login_id']?>');">Re-send</a> 
            </li>
            <?php } ?>
            <li>
                <a href="<?php echo base_url().'webmanager/'.$customer_type.'/access_cc/'.$value['login_id']; ?>" target="_blank">Access CC Panel</a> 
            </li>
            
            <?php if($value['trial_end'] != '0000-00-00 00:00:00'){ ?>
                <li>
                    <a class="extend_trial" data-trial="<?php echo $value['trial_end']?>" data-id="<?php echo $value['login_id']; ?>">Extend 1 Month</a> 
                </li>
                
            <?php } ?>

			<?php if($str==''){?>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirmed('<?php echo base_url().'webmanager/'.$customer_type.'/delete/'.$value['login_id']; ?>');">Delete</a>
            </li>
			<?php } ?>

            
            <li class="divider"></li>
            <li>
                <a href="javascript:void(0);" onclick="toggleOrgStat('<?php echo $value['organization_id']; ?>','<?php echo $value['enabled']?>', '<?php echo $customer_type?>', '<?php echo $value['login_id'] ?>','<?php echo $value['user_status']; ?>');">
					<?php
                    if($value['enabled'] == 'Y'){
                        echo 'Disable';
                    }
                    else{
                        echo 'Enable';
                    }
                    ?>
                </a>
            </li>
            


          </ul>
        </div>   
                
       
                                    </td>

                                    </tr>

								<?php 

									//   }
 
									}//end foreach

								}//end check not empty

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->
                
                
                
                
                <?php
					if(count($all_cc) > 0){
						foreach($all_cc as $r=>$value){ ?>
                        

<!-- Modal -->
<div class="modal fade" id="ccdetails<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CC Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo $this->master_model->decryptIt($value['crt_last_name']); ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo $this->master_model->decryptIt($value['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo $this->master_model->decryptIt($value['crt_mobile']); ?></span></p>
			<p>Organization: <span class="text-muted"><?php echo $value['organization_name']; ?></span></p>
			<p>User Type: <span class="text-muted"><?php echo  $value['user_type']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                        
                        
                        <?php
						
						}
					}
				?>

					




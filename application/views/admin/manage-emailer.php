<style>
	.dataTables_filter, .dataTables_length{
		visibility: hidden;
		display: none;
	}
</style>      
      
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Outgoing Emails</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Outgoing Emails</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-envelope-square fa-fw"></i> Outgoing Emails</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">

                       



								<?php 

								if(count($all_emails) > 0){

									foreach($all_emails as $r => $value){
								?>
                                
<!-- Modal -->
<div class="modal fade" id="ccdetails<?php echo  $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Outgoing Email Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo $value['name']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                
                                <?php		
									}
								}
										
								?>


							<div class="table-responsive">
                            

							<table class="table table-hover table-striped" id="example-table">

                                <thead>
                                    <tr>

                                        <th width="20%">Emailer</th>
                                        
                                        <th width="20%">Subject</th>

                                        <th width="40%">Content</th>


                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($all_emails) > 0){

									foreach($all_emails as $r => $value){
										
								?>

                                    <tr>

                                    	<td><?php echo $value['name']; ?></td>

                                    	<td><?php echo $value['subject']; ?></td>

                                    	<td><?php echo $value['content']; ?></td>


                                    	<td>


       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>webmanager/emailer/update/<?php echo  $value['id']; ?>">Edit</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#ccdetails<?php echo  $value['id']; ?>">Details</a> 
            </li>

          </ul>
        </div>   
   
                                    </td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




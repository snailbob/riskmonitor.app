                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage States</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage States</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    
                            <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
                        </div>

                        <?php    

						} 

						if($this->session->flashdata('error')!=""){

						?>

                        <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    
                            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
                        </div>

                        <?php

						} 

						?>
                        



                        <div class="panel panel-default" >

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Help Function</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">
                            
                            
                            	<form id="help_form">
                                
                                	<div class="form-group">
                                    	<select class="form-control" name="id">
                                        	<option value="">Select Help Function to Update</option>
                                          	<?php
											
											if(count($content) > 0){
											
												foreach($content as $r=>$value){
													$name = str_replace('_', ' ', $value['name']);
													$name = ucfirst($name);
													echo '<option value="'.$value['id'].'">'.$name.'</option>';
												}
											}
											
											?>
                                            
                                            
                                            
                                        </select>
                                    </div>
                                    
                                    <!-- Summernote CMS Example -->
                                    <div id="summernote"><?php // echo $help[0]['help_content']; ?></div>
                                                    
                                
                                	<div class="form-group" style="margin-top: 15px;">
                                    	<button class="btn btn-primary">Submit</button>
                                    	<a href="#" class="btn btn-default preview_help_btn">Preview</a>
                                    </div>
                                </form>




                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					

<!-- Modal -->
<div class="modal fade" id="helpPreviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Preview</h4>

		<div class="well"></div>
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>



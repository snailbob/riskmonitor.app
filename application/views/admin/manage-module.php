                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Modules</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Modules</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->







                <div class="row">



                    <div class="col-lg-12">

                    	<?php

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php

						}

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						}

						?>

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Manage Modules</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped">

                                <thead>

                                    <tr>


                                        <th width="33%">Module</th>
                                        <th width="33%">Preselected</th>
                                        <th width="33%">Default Max Users</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php

								if(count($mods)>0) {
                  unset($mods[0]);
                  unset($mods[2]);
                  unset($mods[3]);
                  unset($mods[4]);
									foreach($mods as $r => $value) {

									?>

                                    <tr>



                                    	<td><?php echo $value['name']; ?></td>

                                    	<td><?php echo $value['preselected']; ?></td>
                                    	<td>
										<?php
											if($value['sort'] > 5) { echo 'NA'; }
											else if($value['default_users'] == '1000') { echo 'Unlimited'; }
											else { echo $value['default_users']; }

										?>

                                        </td>

                                    	<td>

                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-green" role="menu">
                                                <li>
                                                    <a href="<?php echo base_url().'webmanager/modules/update/'.$value['id']; ?>">Update</a>
                                                </li>

                                              </ul>
                                            </div>

                                       </td>

                                    </tr>

								<?php



									}

								}

								?>



                                                </tbody>





                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

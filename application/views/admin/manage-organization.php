                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Organization</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Organization</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12 hidden" style="height: 40px;">

                    <a class="btn btn-primary pull-right" href="<?php echo base_url();?>webmanager/organization/add">Add Organisation</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-user fa-fw"></i>Crisis Organization</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="40%">Organization Name</th>

                                        <th width="50%">Co-ordinator</th>

                                        <th width="10%">Status</th>

                                        <th width=""></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($org_list)>0)

								{

									foreach($org_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td>
                                        
                                        <a href="<?php echo base_url().'webmanager/organization/details/'.$value['organization_id'] ?>" title="click to view details"><?php echo $value['organization_name']; ?></a>
										<?php /*  data-id="<?php echo $value['organization_id'] ?>" data-name="<?php echo $value['organization_name'] ?>" class="view_org_details" */ ?>
                                        
                                        </td>

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); ?></td>

                                    	<td class="text-center">
                                        
                                        	<?php
											if($value['enabled'] == 'Y'){
												echo '<span class="text-success">Enabled</span>';
											}
											else{
												echo '<span class="text-red">Disabled</span>';
											}
											?>
                                        </td>
                                    	<td>

       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu text-right" style="min-width: 100px;">
            <li>
                <a href="<?php echo base_url(); ?>webmanager/organization/update/<?php echo  $value['organization_id']; ?>">Update</a> 
            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirmed('<?php echo base_url().'webmanager/organization/delete/'.$value['organization_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url().'webmanager/coordinator/access_cc/'.$value['login_id']; ?>" target="_blank">Access CC Panel</a> 
            </li>
            
            <li class="divider"></li>
            <li>
                <a href="javascript:void(0);" onclick="toggleOrgStat('<?php echo $value['organization_id']; ?>','<?php echo $value['enabled']?>');">
					<?php
                    if($value['enabled'] == 'Y'){
                        echo 'Disable';
                    }
                    else{
                        echo 'Enable';
                    }
                    ?>
                </a>
            </li>
            
          </ul>
        </div>    

                                    	</td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					


<!-- Modal -->
<div class="modal fade" id="orgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Organization Information</h4>
		<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i> Loading...</p>
        <div class="org_info">
        
        
        	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            	
                
                <div class="panel panel-default">
                	<div class="panel-heading" role="tab" id="headingOne">
                    	<h4 class="panel-title">
                        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseOrgDetails" aria-expanded="true" aria-controls="collapseOrgDetails">Organization Details</a>
                        </h4>
                    </div>
                    <div id="collapseOrgDetails" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                
                
            	<div class="panel panel-default">
                	<div class="panel-heading" role="tab" id="headingCrts">
                    	<h4 class="panel-title">
                        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseCrts" aria-expanded="false" aria-controls="collapseCrts">CRTs assigned to Organization</a>
                        </h4>
                    </div>
                    <div id="collapseCrts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCrts">
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                
                
            	<div class="panel panel-default">
                	<div class="panel-heading" role="tab" id="headingLogs">
                    	<h4 class="panel-title">
                        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseLogs" aria-expanded="false" aria-controls="collapseLogs">CC/CRT Login/Logout Date and Time</a>
                        </h4>
                    </div>
                    <div id="collapseLogs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingLogs">
                        <div class="panel-body">
                            <table class="table table-hover" id="log-table">
                                <thead class="hidden">
                                    <tr>
                                        <th>Event</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <p class="text-muted">Nothing found.</p>
                        </div>
                    </div>    
                </div>
                
                
            	<div class="panel panel-default">
                	<div class="panel-heading" role="tab" id="headingStripe">
                    	<h4 class="panel-title">
                        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseStripe" aria-expanded="false" aria-controls="collapseStripe">Stripe Details</a>
                        </h4>
                    </div>
                    <div id="collapseStripe" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStripe">
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                
                
            
            </div>
            
        </div>
        
        
        
      </div>
    </div>
  </div>
</div>


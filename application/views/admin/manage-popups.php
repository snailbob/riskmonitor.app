                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">
                
                                    <div class="col-lg-12">
                
                                        <div class="page-title">
                
                                            <h1>Manage States</h1>
                
                                            <ol class="breadcrumb">
                
                                                <li><i class="fa fa-dashboard"></i>
                
                                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>
                
                                                </li>
                
                                                <li class="active">Manage States</li>
                
                
                
                                            </ol>
                
                                        </div>
                
                                    </div>
                
                                    <!-- /.col-lg-12 -->
                
                                </div>
                
                                <!-- /.row -->
                
                                <!-- end PAGE TITLE AREA -->
                
                
                
                
                
                                
                
                                <div class="row">
                
                
                
                                    <div class="col-lg-12" style="padding-top:10px;">
                
                                        <?php if($this->session->flashdata('success')!=""){ ?>
                
                                        <div class="alert alert-success alert-dismissable">
                
                                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    
                                            <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
                                        </div>
                
                                        <?php    
                
                                        } 
                
                                        if($this->session->flashdata('error')!=""){
                
                                        ?>
                
                                        <div class="alert alert-danger alert-dismissable">
                
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                                            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
                                        </div>
                
                                        <?php
                
                                        } 
                
                                        ?>
                                        
                
                                        
                    
                                        <a class="btn btn-primary pull-right hidden">Add Admin Access</a> 
                    
                                        <div class="clearfix" style="margin-bottom: 15px;"></div>
                                        
                
                                        <div class="panel panel-default" >
                
                
                
                
                
                                            <div class="panel-heading">
                
                                                <div class="panel-title">
                
                                                    <h4>Manage Pop up Messages</h4>
                
                                                </div>
                
                
                
                                            <div class="clearfix"></div>
                
                                            </div>
                
                                            <div class="panel-body">
                
                
                
                                            <div class="table-responsive">
                
                                            <table class="table table-hover table-striped" id="example-table">
                
                                                <thead>
                
                                                    <tr>
                
                                                        <th width="20%">Name</th>
                                                        <th width="80%">Message</th>
                                                        <!-- <th width="20%">Success Message</th>
                                                        <th width="20%">Error Message</th> -->
                
                                                        <th></th>
                
                                                    </tr>
                
                                                </thead>
                
                                                <tbody>
                
                                                <?php 
                
                                                if(count($messages)>0)
                
                                                {
                
                                                    foreach($messages as $r => $value)
                
                                                    {
                
                                            ?>
                
                                                    <tr>
                
                                                        <td><?php echo $value['name']; ?><br /><small class="text-muted"><?php echo $value['description']; ?></small></td>
                
                                                        <td><?php echo $value['confirm_message']; ?></td>
                
            <!--                                                        <td><?php echo $value['success_message']; ?></td>
            
                                                    <td><?php echo $value['error_message']; ?></td>
-->                                                        
                                                        <td>
                
                
                        <!-- Single button -->
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                            Action <span class="caret"></span>
                            </button>
                          <ul class="dropdown-menu" role="menu" style="font-size: 90%; min-width: 82px;">
                            <li>
                                <a href="#" class="update_valmess_btn" data-err="<?php echo $value['error_message']; ?>" data-succ="<?php echo $value['success_message']; ?>" data-conf="<?php echo $value['confirm_message']; ?>" data-desc="<?php echo $value['description']; ?>" data-id="<?php echo $value['id']; ?>">Edit</a>
                            </li>
                            <li class="hidden">
                                <a href="javascript:;" class="delete_valmess_btn" data-id="<?php echo $value['id'];?>">Delete</a>
                            </li>
                
                          </ul>
                        </div>   
                        
                                                       </td>
                
                                                    </tr>
                
                                                <?php 
                
                
                 
                                                    }
                
                                                }
                
                                                ?>
                
                                                                    
                
                                                                </tbody>
                
                                                                
                
                                                                
                
                                                            </table>
                
                                                        </div>
                
                                                        <!-- /.table-responsive -->
                
                
                
                
                
                
                
                
                
                                            </div>
                
                                        </div>
                
                                        <!-- /.panel -->
                
                                    </div>
                
                
                
                                </div><!--.row -->
                
                                    
                
                <!-- Modal -->
                <div class="modal fade" id="valMessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        
                        <form id="valmess_form" novalidate="novalidate">
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" readonly name="description" />
                                <input type="hidden" class="form-control" name="id" />
                            </div>
                
                            <div class="form-group">
                                <label>Message</label>
                                <input type="text" class="form-control" name="confirm" />
                                <input type="hidden" class="form-control" name="id" />
                            </div>
                
                            <div class="form-group hidden">
                                <label>Success Message</label>
                                <input type="text" class="form-control" name="success" />
                                <input type="hidden" class="form-control" name="id" />
                            </div>
                
                            <div class="form-group hidden">
                                <label>Error Message</label>
                                <input type="text" class="form-control" name="error" />
                                <input type="hidden" class="form-control" name="id" />
                            </div>
                
                
                            <div class="form-group">
                                <button class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                
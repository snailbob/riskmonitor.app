


<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller hidden ng-cloak" ng-controller="headerTasksController" ng-click="closeAllPopUps()">


    <div class="row">
  

        <div class="col-lg-5 col-headers">

          <div class="scroll-content">

            <h4 style="padding: 15px 0;">
              <span ng-if="template_id == '18' || !template_id">Product Recall</span>
              <span ng-if="template_id != '18' && template_id"><?php echo (isset($_GET['template_name'])) ? $_GET['template_name']: 'New Template'; ?></span>
              <!-- <button class="btn btn-default btn-sm pull-right" ng-click="newStandbyModal()">
                <i class="fa fa-plus" aria-hidden="true"></i> Standby Task Template
              </button> -->
            </h4>
            <hr>

            <table class="table table-hover table-bordered table-rtask">
              <tbody id="steporder" ui-sortable="sortableTableOptions" ng-model="headerTasks">

                  <tr ng-class="{'info': activeHeader == $index }" ng-repeat="hTask in headerTasks track by $index" as-sortable-item>
                    <td class="num">
                      <div>
                        {{$index + 1}}
                      </div>
                    </td>
                    <td class="text">

                      <div class="input-group">
                        <button name="" id="" class="delete-header-btn btn-xs btn btn-danger" href="#" role="button" ng-click="deleteHeaderLive($index, hTask)">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>

                        <input type="text" class="form-control form-control-header" placeholder="" ng-model="hTask.name" ng-click="focusHeader($index)">
                        <span class="input-group-btn">
                          <button class="btn btn-link" ng-click=" hTask.show = !hTask.show">
                            <i class="fa" ng-class="{'fa-plus': !hTask.show ||  $index != activeHeader, 'fa-minus' :  $index == activeHeader && hTask.show, 'visibility-hidden' : $index != activeHeader }" aria-hidden="true"></i>
                          </button>
                        </span>
                      </div><!-- /input-group -->


                      <div  class="collapse" ng-class="{'in' : $index == activeHeader && hTask.show}" ng-attr-id="collapse{{$index}}">

                        <div class="list-group" style="margin-top: 15px; margin-right: 37px;">
                          <a href="#" class="list-group-item list-group-task" ng-class="{'active': activeTaskIndex == $index && $parent.$index == activeHeader}" ng-repeat="task in hTask.tasks track by $index" ng-click="clickTask(task.id, $index)">
                                        
                            <button name="" id="" class="pull-right btn-xs btn btn-danger" href="#" role="button" ng-click="deleteTask($index, task)">
                              <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>

                            <span class="fa-stack pull-left" ng-click="completeTask(task, $index)" ng-if="incident.incident_status == '0'">
                              <i class="fa fa-square-o fa-stack-2x"></i>
                              <i class="fa fa-check fa-stack-1x" ng-if="task.checked && !task.loading"></i>
                              <i class="fa fa-spinner fa-spin fa-stack-1x" ng-if="task.loading"></i>
                            </span>
                            
                            <span class="fa-stack pull-left text-muted" ng-if="!incident.id || incident.incident_status != '0'">
                              <i class="fa fa-square-o fa-stack-2x"></i>
                              <i class="fa fa-check fa-stack-1x" ng-if="task.checked && !task.loading"></i>
                              <i class="fa fa-spinner fa-spin fa-stack-1x" ng-if="task.loading"></i>
                            </span>
                          
                            <div style="line-height: 26px; padding-left: 30px">
                              {{task.question}}
                            </div>
                          </a>    

                        </div>

                      </div>
                    </td>
                  </tr>


              </tbody>
            </table>

            <div class="header-actions">

              <div class="pull-right text-right">

                  <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" ng-disabled="!headerTasks.length" ng-click="addTask()">
                    <i class="fa fa-plus" aria-hidden="true"></i> Task
                  </a>
                  <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" ng-click="addHeader()">
                    <i class="fa fa-plus" aria-hidden="true"></i> Heading
                  </a>
                  <a name="" id="" class="btn btn-success btn-sm" href="#" ng-click="saveChanges()" role="button" ng-class="{'disabled': loading}">
                    <span ng-if="!loading">
                        <i class="fa fa-save" aria-hidden="true"></i> Save Changes
                    </span>
                    <span ng-if="loading">
                        <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Saving changes..
                    </span>
                  </a>

              </div>

               <a name="" id="" class="btn btn-link btn-sm" role="button" ng-click="templateModal()">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Standby Task Templates
              </a>


            </div>
          </div>

        </div>
        <div class="col-lg-7 col-contents">
          <div class="scroll-content" ng-if="headerTasks.length">
            <div class="padding">
              <h2>
                <textarea name="" id="" class="form-control form-invisible form-invisible-lg" ng-model="activeTaskInfo.question" rows="auto"></textarea>
              </h2>
              <hr>
              <div class="content-actions">
                <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'assignPopover.html'" popover-placement="bottom" popover-is-open="assignPopSettings.isOpen" >
                  <i class="fa fa-user" aria-hidden="true"></i> <span ng-if="activeTaskInfo.assigned_id == '0'">Assign</span>  <span ng-if="activeTaskInfo.assigned_id != '0'">Assigned - {{activeTaskInfo.assigned_info.full_name}}</span>
                </a>
                <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'datePickerPopover.html'" popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                  <i class="fa fa-clock-o" aria-hidden="true"></i> Due after <span ng-if="activeTaskInfo.months">{{activeTaskInfo.months}} months</span> <span ng-if="activeTaskInfo.days">{{activeTaskInfo.days}} days</span> <span ng-if="activeTaskInfo.hours">{{activeTaskInfo.hours}} hours</span> <span ng-if="activeTaskInfo.mins">{{activeTaskInfo.mins}} mins</span>
                </a>
                <a name="" id="" ng-click="openPreview()" class="btn btn-default btn-sm">
                  Preview
                </a>
              </div>
            </div>

            <p class="text-center text-muted lead" style="padding: 140px 0;" ng-if="!contents.length">
              No content added.
            </p>
            <!--  -->
            <div class="checklist-content" ui-sortable="sortableContentOptions" ng-model="contents">
              <div class="editor-holder" ng-repeat="content in contents track by $index"  as-sortable-item>
                <div class="move-handle">
                  <button class="btn btn-thirdy btn-times btn-sm" role="button" ng-click="removeContent($index, content)">
                    <i aria-hidden="true" class="fa fa-times fa-fw"></i>
                  </button>

                  <button ng-if="$index != 0" class="btn btn-thirdy btn-up btn-sm" role="button" ng-click="moveContent($index, 'up')">
                    <i aria-hidden="true" class="fa fa-chevron-up"></i>
                  </button>
                </div>



                <div class="list-margin-holder" ng-class="{'hidden' : content.module == 'image'}"
                    ng-if="content.module == 'text' || content.module == 'image'">

                  <ng-quill-editor ng-model="content.content" placeholder="" ng-click="focusQuill($index)" modules="getQuillModule(content.module)"></ng-quill-editor>
                  <!-- <summernote ng-model="content.content"></summernote> -->

                </div>


                <div ng-if="content.module == 'video'" class="contents-holder list-margin-holder">
                  <div class="add-video">
                    <div class="form-group">
                      <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Paste YouTube / Vimeo / Wistia URL or embed here">
                    </div>
                  </div>

                  <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
                </div>


                <div class="contents-holder list-margin-holder" ng-if="content.module == 'image'">
                  <div class="text-center" ng-hide="content.content">
                    <button type="button" class="btn btn-default" role="button" onclick="var i2 = $('.ql-image').length - 1; $('.ql-image:eq('+i2+')').click();">
                    <!-- ng-click="showImagePop(content, $event)" -->
                      <i class="fa fa-plus" aria-hidden="true"></i> Upload image...
                    </button>
                  </div>
                  <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
                </div>


                <div class="contents-holder list-margin-holder" ng-if="content.module == 'select'">

                  <div class="field-single">
                    <div class="options-view">

                      <div class="form-group">
                        <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Type label here">
                      </div>

                      <select name="" id="" class="form-control" disabled>
                        <option>An option will be selected from this list:</option>
                      </select>
                      <div class="card card-select">
                        <div class="card-block">
                          <div class="form-check form-check-custom" ng-repeat="option in content.options track by $index">
                            <label class="custom-control custom-checkbox custom-checkbox2">
                              <span class="custom-number">{{$index + 1}}</span>
                              <span class="custom-control-description">
                                  <input type="text" class="form-control form-invisible" id="" placeholder="Type option name here" ng-model="option.text" ng-keydown="keyDownSelect($event, option , $index, $parent.$index)">
                              </span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- options-view -->
                  </div>
                </div>

                <div class="contents-holder list-margin-holder" ng-if="content.module == 'list'">

                  <div class="form-check form-check-custom" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">
                    
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" disabled class="custom-control-input">
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">
                          <input type="text" class="form-control form-invisible" id="inlineFormInput" placeholder="Type here.. hit enter to add sub-task" ng-model="sublist.text" ng-keydown="keyDownSublist($event, sublist , $index, $parent.$index)">
                      </span>
                    </label>
                  </div>



                  <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
                    <div class="form-group" ng-hide="field.type == 'hidden' || field.type == 'select'">
                      <input type="text" class="form-control" ng-model="field.text" aria-describedby="helpId" placeholder="Type label here">
                    </div>


                    <input type="text" ng-if="field.type == 'single'" disabled class="form-control" name="" id="" aria-describedby="helpId" placeholder="Something will be typed here...">
                    
                    <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2" disabled placeholder="Something will be typed here..."></textarea>
                  
                    <div ng-if="field.type == 'email'"  class="input-group">
                      <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" disabled>
                    </div>

                    <div ng-if="field.type == 'url'"  class="input-group">
                      <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" disabled>
                    </div>

                    <div ng-if="field.type == 'hidden'"  class="input-group">
                      <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" ng-model="field.text" placeholder="Type label here" aria-describedby="basic-addon1">
                    </div>


                    <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" disabled>
                      <i class="fa fa-upload" aria-hidden="true"></i> File will be uploaded here
                    </button>

                    <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" disabled>
                      <i class="fa fa-calendar" aria-hidden="true"></i> Date will be set here
                    </button>

                  </div>

                </div>

                <div class="emailer-holder" ng-if="content.module == 'email'">
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">To</label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Cc</label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Bcc</label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Subject</label>
                    <div class="col-sm-9">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Body</label>
                    <div class="col-sm-10">
                      <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                    <div class="col-sm-8">
                      <button class="btn btn-default">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i> Test Send
                      </button>
                    </div>
                  </div>
                </div>

                <div class="move-handle">
                  <button class="btn btn-thirdy btn-copy btn-sm" role="button" ng-click="copyContent(content, $index)" >
                    <i aria-hidden="true" class="fa fa-copy fa-fw"></i>
                  </button>

                  <button ng-if="contents.length > 1; $index != (contents.length - 1)" class="btn btn-thirdy btn-down btn-sm" role="button" ng-click="moveContent($index, 'down')">
                    <i aria-hidden="true" class="fa fa-chevron-down"></i>
                  </button>
                </div>

              </div>
              <!-- editor-holder -->
            </div>









          </div>
        
        </div>
    </div><!--.row -->



    <div class="sticky-rightbar">
      <div class="scroll-content">
        <div class="form-group">
          <button class="btn btn-default btn-block btn-lg" data-toggle="tooltip" data-title="Text" data-placement="left" type="button" ng-click="addText()">
            <i class="fa fa-text-width"></i>
          </button>
        </div>
        <div class="form-group">
          <button class="btn btn-default btn-block btn-lg" container="body" popover-append-to-body="true" uib-popover-template="'mediaPopover.html'" popover-trigger="'focus'" popover-placement="left" type="button" >
            <i class="fa fa-file" data-toggle="tooltip" data-title="Media" data-placement="left"></i>
          </button>
        </div>


        <div class="form-group">
          <button class="btn btn-default btn-block btn-lg" container="body" popover-append-to-body="true" uib-popover-template="'formPopover.html'" popover-trigger="'focus'" popover-placement="left" type="button">
            <i class="fa fa-clipboard" data-toggle="tooltip" data-title="Form field" data-placement="left"></i>
          </button>
        </div>

        
        <div class="form-group">
          <button class="btn btn-default btn-block btn-lg" type="button" ng-click="addSubchecklist()">
            <i class="fa fa-list" data-toggle="tooltip" data-title="Sub checklist" data-placement="left" ></i>
          </button>
        </div>
        <div class="form-group">
          <button class="btn btn-default btn-block btn-lg" data-toggle="tooltip" data-title="Email" data-placement="left" type="button" ng-click="addEmail()">
            <i class="fa fa-envelope"></i>
          </button>
        </div>


      </div>
    </div>
    
    <script type="text/ng-template" id="templateModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                <a ng-click="addTaskTemplate()" class="btn btn-default btn-sm pull-right">
                  <i class="fa fa-plus" aria-hidden="true"></i> Standby Task Template
                </a>

                Standby Tasks Templates
            </h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-default" ng-repeat="template in templates track by $index">
            <div class="panel-body">
                <a ng-href="<?php echo base_url().'webmanager/recall/tasks/0/'?>{{template.id}}?template_name={{template.name}}">
                  {{template.name}}
                </a>
            </div>
          </div>
        </div>

    </script>

    <script type="text/ng-template" id="newStandbyModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Standby Template
            </h4>
        </div>
        <div class="modal-body">
            <ng-form name="nameDialog" novalidate role="form">
                <div class="form-group">
                    <label class="control-label" for="course">Template name</label>
                    <input type="text" class="form-control" name="name" id="name" ng-model="data.name" required>
                </div>
            </ng-form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Submit</button>
        </div>

    </script>

    <script type="text/ng-template" id="datePickerPopover.html">
      <div style="max-width:315px;">
        <div class="row gutter-md">
          <div class="col-xs-12">
            <p class="text-center">Due after</p>
            <hr>
            <p>
              <small>This task will be due:</small>
            </p>
          </div>
          
          <div class="col-xs-6">
            <div class="form-group">

              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.months" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">months</span>
              </div>

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.days" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">days</span>
              </div>
            </div>
          </div>
          
          <div class="col-xs-6">
            <div class="form-group">

              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.hours" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">hours</span>
              </div>

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.mins" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">mins</span>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <p>
              <small>After the checklist start date.</small>
            </p>
          </div>
          <!-- <div class="col-xs-6">
            <div class="form-group">
              <label for="">Date</label>
              <input type="text" ng-model="activeTaskInfo.due_date | date: 'shortDate'" class="form-control">

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <label for="">Time</label>
               <input type="text" ng-model="activeTaskInfo.due_time" class="form-control"> 
            </div>
          </div> -->
          <!-- <div class="col-xs-12">
            <div class="form-group">
              <div uib-datepicker ng-model="activeTaskInfo.due_date" datepicker-options="optionsPicker"></div> 
            </div>
          </div> -->

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-success btn-block" ng-click="saveDate()" role="button">Save</a>
          </div>

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-danger btn-block" ng-click="removeDate()" role="button">Remove</a>
          </div>

        </div>
      </div>

    </script>

    <script type="text/ng-template" id="customTemplate.html">
      <a>
          <!-- <img ng-src="http://upload.wikimedia.org/wikipedia/commons/thumb/{{match.model.flag}}" width="16"> -->
          <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
      </a>
    </script>

    <script type="text/ng-template" id="assignPopover.html">
      <div style="width:240px;">
        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <input type="text" ng-model="activeTaskInfo.assigned" placeholder="" uib-typeahead="crt as crt.full_name for crt in crts | filter:{full_name:$viewValue}" typeahead-template-url="customTemplate.html" class="form-control" typeahead-show-hint="true" typeahead-min-length="0">
              <span class="input-group-btn">
                <button class="btn btn-link" type="button" ng-click="saveAssigned()">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </button>
              </span>
            </div><!-- /input-group -->

            <small>Search for a person in your organization by name.</small>


          </div>

        </div>
      </div>
    </script>

    <script type="text/ng-template" id="confirmPopover.html">
      <div style="width:140px;">
        <div class="row">
          <div class="col-sm-6">
            <a name="" id="" class="btn btn-block btn-danger" href="#" role="button">
              Delete
            </a>
          </div>
          <div class="col-sm-6">
            <a name="" id="" class="btn btn-block btn-default" href="#" role="button">
              Cancel
            </a>
          </div>

        </div>
      </div>
    </script>

    <script type="text/ng-template" id="mediaPopover.html">
      <div style="width: 200px;">
          <button class="btn btn-default btn-block" type="button" ng-click="addImage()"> <i class="fa fa-picture-o"></i> Image </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addVideo()"> <i class="fa fa-video-camera"></i> Video </button>
      </div>

    </script>
    <script type="text/ng-template" id="formPopover.html">

      <div style="width: 200px;">
          <button class="btn btn-default btn-block" type="button" ng-click="addSingleLine()"> <i class="fa fa-edit fa-fw"></i> Single Line Text </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addParagraph()"> <i class="fa fa-paragraph fa-fw"></i> Paragraph Text </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addEmailForm()"> <i class="fa fa-envelope-o fa-fw"></i> Email Address </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addSiteForm()"> <i class="fa fa-globe fa-fw"></i> Web Address </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addFileForm()"> <i class="fa fa-file-o fa-fw"></i> File Upload </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addDateForm()"> <i class="fa fa-calendar fa-fw"></i> Date </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addDropdownForm()"> <i class="fa fa-arrow-circle-down fa-fw"></i> Dropdown </button>
      </div>
    </script>
  </div>
</div>
  
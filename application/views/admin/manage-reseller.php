                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->


<!-- Modal for manage portal -->
<div class="modal fade" id="managePortal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Manage Portal</h4>
      </div>
      <div class="modal-body">

			<div id="portalsuccess_holder"></div>            
			<div id="portaltabs_holder"></div>            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="savePortal();" id="save_prtal">Save changes</button>
      </div>
    </div>
  </div>
</div>
                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Resellers</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Resellers</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">


                    <div class="col-lg-12" style="height: 40px;">

                    <a class="btn btn-primary pull-right" href="<?php echo base_url();?>webmanager/reseller/add">Add Reseller</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-briefcase fa-fw"></i> Resellers</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="35%">First Name</th>

                                        <th width="35%">Last Name</th>

                                        <th width="30%">Status</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($all_cc)>0)

								{

									foreach($all_cc as $r => $value)

									{
										
								?>

                                    <tr>

                                    	<td><?php echo $this->master_model->decryptIt($value['first_name']); ?></td>

                                    	<td><?php echo $this->master_model->decryptIt($value['last_name']);  ?></td>



                                    	<?php 

                                    if($value['user_status'] == 1 ){

                                    $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Accepted</span>';

                                    } else {	

                                    $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting Acceptance</span>';

                                    }

                                    ?>

                                    <td><?php echo  $status ?></td>

                                    <td>



       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>webmanager/reseller/update/<?php echo  $value['login_id']; ?>">Edit</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#ccdetails<?php echo  $value['login_id']; ?>">Details</a> 
            </li>

            <li>
                <a href="javascript:void(0);" onclick="return del_confirmed('<?php echo base_url().'webmanager/reseller/delete/'.$value['login_id']; ?>');">Delete</a>
            </li>

            <li>
                <a href="#" data-toggle="modal" data-target="#managePortal" onclick="managePortal('<?php echo $value['login_id'];?>');">Active Tabs</a>
            </li>
            <li class="hidden">
                <a href="<?php echo base_url().'webmanager/reseller/orgassign/'.$value['login_id']; ?>">Assign Org</a>
            </li>

          </ul>
        </div>   
        
<!-- Modal -->
<div class="modal fade" id="ccdetails<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Reseller Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['first_name']); ?> <?php echo $this->master_model->decryptIt($value['last_name']); ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo $this->master_model->decryptIt($value['email_id']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo $this->master_model->decryptIt($value['mobile']); ?></span></p>
            
            <?php $organizationinfo=$this->master_model->getRecords('organization_master',array('reseller_id'=>$value['login_id']));
				if(count($organizationinfo) > 0 ){
					echo '<p>Assigned Organizations</p><ol class="text-muted">';
					foreach($organizationinfo as $org =>$inff){
						echo '<li>'.$inff['organization_name'].'</li>';
						
					}
					echo '</ol>';
				}
			
			?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
        
        
        
                                    </td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




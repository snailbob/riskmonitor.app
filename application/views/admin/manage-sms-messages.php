                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage SMS Messages

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Manage SMS Messages</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>SMS Messages</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									
                                    



							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<tbody>

                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Name</th>
                                        <th width="50%" style="background-color:#fff;">Text Message</th>
                                    </tr>
                                    
                                    
                                    <?php
									$i = 0;
									foreach ($messages as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['name']; ?></td>

                                    	<td>
										
										
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['text'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['text'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitSMSMessage('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                  
                                        </td>




                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    
                                    
                                </tbody>

                                

                                

                            </table>

                        </div>

                        <!-- /.table-responsive -->



                                    
                                    
                                                                        
                                    
                                    
                                    
                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




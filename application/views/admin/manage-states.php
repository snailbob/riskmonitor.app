                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage States</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage States</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12" style="height: 40px;">

                    <a class="btn btn-primary pull-right" href="<?php echo base_url();?>webmanager/states/add">Add States</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Manage States</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="50%">State Name</th>

                                        <th width="50%">Country</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($states)>0)

								{

									foreach($states as $r => $value)

									{

							?>

                                    <tr>

                                    	<td><?php echo $value['name']; ?></td>

                                    	<td>
										<?php
										
										
											$all_country=$this->master_model->getRecords('country_t',array('country_id'=>$value['country_id']));

											echo $all_country[0]['short_name'];
										
										
										?>
                                        </td>

                                    	<td>

       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>webmanager/states/update/<?php echo  $value['id']; ?>">Edit</a>
            </li>



          </ul>
        </div>   
        
                                       </td>

                                    </tr>

								<?php 


 
									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Modules</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Modules</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>
                        
                    </div>    
                        
            <div class="col-lg-12">
                <a href="<?php echo base_url().'webmanager/contents/threeparts'?>" class="btn btn-default pull-left">Back</a>
                
                <div class="clearfix" style="padding-bottom: 15px;"></div>
                

                <div class="panel panel-default">

                    <div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>


                        <?php if(count($admin_info) > 0) { ?>
                        
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatablex">
                          <thead>
                            <tr>
                              <th>Title</th>
                              <th>Content</th>
                              <th>Image</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($admin_info as $u=>$user){
						?>
                        
                            <tr>
                             
                              <td><?php echo $user['title'] ?></td>
                              <td><?php echo $user['content'] ?></td>

                              <td>
							  	<?php
									if($user['image'] != ''){
										echo '<img src="'.base_url().'uploads/avatars/'.$user['image'].'" style="width: 150px;">';
									}
								?>
                              </td>
                              
                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
									
                                        <li><a href="<?php echo base_url().'webmanager/contents/threepart/'.$user['id']?>">Update</a></li>
                                        <?php /*?><li class="divider"></li>
                                        <li><a href="javascript:;" onclick="deleteAlternate('<?php echo $user['id']; ?>');">Delete Permanently</a></li><?php */?>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">No <?php echo $title; ?></p></div>';	
						}?>
                        
                    <div class="panel-body hidden">

                    </div>

                </div>



            </div>


        </div><!--.row -->

			
            

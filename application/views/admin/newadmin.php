<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
    

<!-- END -->

    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/2/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    

</head>

<body style="background: #efefef;">



    <div class="container">

        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Create Password</strong></h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

						<?php if($error !=''){  ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $error; ?>
                        </div>
                                      
                        <?php } ?>	
						
						<?php if($error != 'Link invalid.' ){  ?>
                        <div id="">

                            <form id="admin_setpassword">

                            
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" type="password" name="password" id="password" />
                                    <input type="hidden" name="admin_id" value="<?php echo $adminid ?>" />
                                    <input type="hidden" name="admin_name" value="<?php echo $realusername; ?>" />
                                    <input type="hidden" name="admin_email" value="<?php echo $adminemail; ?>" />
                                    <span class="text-red"></span>

                                </div>

                                <div class="form-group">

                                    <input class="form-control" placeholder="Verify Password" type="password" name="password2" id="password2"/>							
                                    <span class="text-red"></span>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary btn-block">Save password for <?php echo $realusername; ?></button>
                                    
                                </div>

                            </form>

                        </div>
                        <?php } ?>	
                    </div>

                </div>

            </div>

        </div>

    </div>



    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>

	<script>
        $(document).ready(function(e) {
            $('#password').focus();
            
            $('#admin_setpassword').on('submit', function(){
                
                var $self = $(this);
                var password = $('#password');
                var password2 = $('#password2');
                $self.find('span').html('');
                $self.find('div.form-group').removeClass('has-error');
                
                if(password.val() == ''){
                    password.closest('div').addClass('has-error');
                    password.focus().siblings('span').html('Required field.');
                }
                else if(password.val().length < 6){
                    password.closest('div').addClass('has-error');
                    password.focus().siblings('span').html('Password must be atleast 6 characters.');
                }
                else if(password2.val() == ''){
                    password2.closest('div').addClass('has-error');
                    password2.focus().siblings('span').html('Required field.');
                }
                else if(password2.val() != password.val()){
                    password2.closest('div').addClass('has-error');
                    password2.focus().siblings('span').html('Verify password didn\'t match.');
                }
                else{
                    console.log($self.serialize());
                    $self.find('button.btn-primary').addClass('disabled').html('Saving password..');
                    $.post(
                        base_url+'webmanager/dashboard/save_admin_password',
                        $self.serialize(),
                        function(res){
                            console.log(res);
							
                            if(res == 'yeah'){
								$self.find('button.btn-primary').addClass('disabled').html('Saved!!');
                                bootbox.alert('<p class="lead"><i class="fa fa-check-circle text-success"></i> Password successfully saved. Click OK to login.</p>', function(){
                                    window.location.href = base_url+'webmanager';
                                });
                            }
                        }
                    ).error(function(err){
                        console.log(err);
                    });
                }
                return false;
            });
        });
    </script>
</body>

</html>

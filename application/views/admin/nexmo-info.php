                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Nexmo Information</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Nexmo Information</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">

                	<div class="col-lg-8" style="height: 40px;">
                        <p class="lead"><b>Balance: </b>$ <?php echo round($usdvalue, 2); ?> 
                        
                        
                        <?php 	
						//display warning when allocated credit is greater than balance
						if(count($org_list)>0)
						{
							$i = 0;
							foreach($org_list as $q => $vaalue){
								$i = $i + $vaalue['sms_credit'];
							}

							if($usdvalue < $i){
							echo '<span class="badge"><i class="fa fa-warning"></i> Warning: Allocated credit is greater than your balance.</span>';
							}
									
						} ?>
                        </p>
                    </div>

                	<div class="col-lg-4" style="height: 40px;">
                    	<a href="<?php echo base_url().'webmanager/smscenter/messages'; ?>" class="btn btn-primary pull-right">Manage Messages</a>
                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa fa-mobile fa-fw"></i>Credit Allocation to Organizations</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="30%">Organization Name</th>

                                        <th width="30%">Coordinator</th>

                                        <th width="20%">SMS Credits</th>

                                        <th width="20%">Awaiting Credits</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($org_list)>0)

								{

									foreach($org_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['organization_name']; ?></td>

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); ?></td>
                                        
                                    	<td class="sms_credit_col"><?php echo '$'.round($value['sms_credit'], 2); ?></td>

                                    	<td><?php echo '$'.round($value['waiting_sms_credit']); ?> <a href="#" class="btn btn-link btn-xs add_waiting_credit_btn <?php if($value['waiting_sms_credit'] == 0) { echo 'hidden';}?>" data-awaiting="<?php echo $value['waiting_sms_credit']; ?>" data-credit="<?php echo $value['sms_credit']; ?>" data-orgid="<?php echo $value['organization_id']; ?>">Add Now</a></td>

                                    	<td>

<?php /*?>                                        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>webmanager/smscenter/update/<?php echo  $value['organization_id']; ?>">Allocate Credit</a> 
<?php */?>

<div class="dropdown pull-right">
  <button id="dLabel" type="button" class="btn btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
  	<li><a href="<?php echo base_url().'webmanager/smscenter/update/'.$value['organization_id']; ?>">Update Allocation</a></li>
  	<li><a href="<?php echo base_url().'webmanager/smscenter/usage/'.$value['organization_id']; ?>">View SMS Usage</a></li>
  </ul>
</div>


                                    	</td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Video Center</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Video Center</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">

                	<div align="right">

                         <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                            <a class="btn btn-orange" href="<?php echo base_url();?>webmanager/organization/add">Add Organisation</a> 

                            </div>

                        </div>	

               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="portlet portlet-default">





                            <div class="portlet-heading">

                                <div class="portlet-title">

                                    <h4><i class="fa fa-user fa-fw"></i>Crisis Organization</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">



							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th>Organization Name</th>

                                        <th>Co-ordinator</th>

                                        <th>Actions</th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($org_list)>0)

								{

									foreach($org_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['organization_name']; ?></td>

                                    	<td><?php echo  $value['crt_first_name'].' '.$value['crt_last_name']; ?></td>

                                    	<td>

                                        <a class="btn btn-green btn-xs" href="<?php echo base_url(); ?>webmanager/organization/update/<?php echo  $value['organization_id']; ?>">Edit</a> 

                                    <a class="btn btn-orange btn-xs" href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'webmanager/organization/delete/'.$value['organization_id']; ?>');">Delete</a>

                                    	</td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.portlet -->

                    </div>



                </div><!--.row -->

					




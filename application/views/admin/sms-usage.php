                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Allocate Credit

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Allocate Credit</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!=""){

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-success">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">
                    	<a href="<?php echo base_url().'webmanager/smscenter/manage' ?>" class="btn btn-default" style="margin-bottom: 15px;"><i class="fa fa-angle-double-left"></i> Back</a>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title"><h4>Organization SMS Usage</h4></div>

                            </div>


                            <div class="panel-body">
                            	<p>SMS sent successful: <?php echo $sms_success; ?></p>
                            	<p>SMS sent failed: <?php echo $sms_failed; ?></p>
                            	<p>Total SMS: <?php echo $sms_total; ?></p>

                                
								
							<?php
								if(count($sms_data) > 0){ ?>
                                <hr />
                                
                                <table class="table table-hover" id="example-table">
                                	<thead>
                                    	<tr>
                                        	<th>Recipient</th>
                                        	<th>Price</th>
                                        	<th>Date Sent</th>
                                        	<th>SMS type</th>
                                        	<th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                
                                <?php 
									$total_price = 0;
									foreach($sms_data as $r=>$value){
										
										?>
                                    	
                                    
                                    	<tr>
                                        	<td><?php echo $value['recipient']?></td>
                                            
                                        	<td><?php echo '$'.$value['price']?></td>
                                            
                                        	<td title="<?php echo $value['date_sent']; ?>"><?php echo date_format(date_create($value['date_sent']), 'M d Y'); ?></td>
                                            
                                        	<td><?php $type = explode('_', $value['log_scenario_id']);
											
												if(count($type) > 1){
													echo 'notification';
												}
												else{
													echo 'recall incident';
												}
											 ?></td>
                                            
                                        	<td>
											<?php if($value['status'] == '0') { echo 'Success';} else { echo $value['status']; }?>
                                            </td>
                                        </tr>
                                    
                                    <?php
										$total_price += $value['price'];

									}
									?>
                                    </tbody>
                                </table>    
								<?php
								}
							
							?>
                                    
                            <div class="well"><strong>Total SMS Credit Spent: $<?php echo $total_price; ?></strong></div>

                            </div>
                        </div>

                    </div>



                </div><!--.row-->




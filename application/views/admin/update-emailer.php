                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Outgoing Emails

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Update Outgoing Emails</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Outgoing Emails</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									


						

                        <div class="form-group">

                            <div class="col-sm-10 col-sm-offset-1">
								<p class="lead">File Name: <br />
                                <span class="text-muted"><?php echo $mess_info[0]['name']; ?></span></p>

                            </div>

                            <div class="col-sm-10 col-sm-offset-1" style="padding-bottom: 25px;">
								
                                <p class="lead">Subject:<br /><span class="text-muted" id="subjj" onclick="$(this).hide();$('.form_subj').show();" style="cursor: pointer;" title="click to edit"><?php echo $mess_info[0]['subject']; ?></span>
                                <span class="form_subj" style="display: none;">
                                <input type="text" id="subject_name" class="form-control" value="<?php echo  $mess_info[0]['subject']; ?>" style="width: 100%; padding:3px; font-size: 21px; color: #999;"/>
                                <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px;" onclick="submitSubject('<?php echo $mess_info[0]['id'];?>');">Submit</button> 
                                <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;margin-right: 5px;" onclick="$('.form_subj').hide();$('#subjj').show();">Cancel</button></span>
                                </p>
                                

                            </div>

                        </div>

						<?php

							$healthy = array(
								"[case_id]",
								"[click_here]",
								"[clicking_here]",
								"[password]",
								"[scenario_name]",
								"[incident_name]",
								"[user_email]",
								"[org_name]"
							);
							
							$yummy = array(
								'<span class="text-info">*CASE_ID*</span>',
								'<span class="text-info">click here</span>',
								'<span class="text-info">clicking here</span>',
								'<span class="text-info">*TEMP_PW*</span>',
								'<span class="text-info">*SCENARIO_NAME*</span>',
								'<span class="text-info">*INCIDENT_NAME*</span>',
								'<span class="text-info">*USER_EMAIL*</span>',
								'<span class="text-info">*ORG_NAME*</span>'
							);
							
							$newphrase = str_replace($healthy, $yummy, $mess_info[0]['content']);
							?>
    
						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>
                            
                            <div class="col-sm-10 col-sm-offset-1 thumbnail" style="padding: 30px;">
    
                                <p>
                                    <img src="<?php echo base_url() ?>assets/2/img/crisisflo-logo-medium.png" />
                                    
                                    <br /><br /><br />
                                    <h4 class="text-muted">Hello Mr./Mrs.</h4><br />
                                </p>
                                <p><span class="content_text" title="click to edit"><?php echo $newphrase; ?></span>
                                </p>
                                
                                <div class="form-group">
                                    <textarea rows="3" class="form-control edit_content" id="content" name="content" placeholder="" value="" style="display: none;"><?php echo $mess_info[0]['content']; ?></textarea><?php echo form_error('content'); ?>
                                    <a class="submit_content btn btn-default pull-right" style="display: none; margin-top: 15px;" onclick="$('.edit_content').hide();$('.submit_content').hide();$('.content_text').show()">Cancel</a>
                                    <button type="submit" class="submit_content btn btn-primary pull-right" style="display: none; margin-top: 15px; margin-right: 5px;" name="add_reseller" id="add_reseller">Submit</button>
        
                                
                                </div>

                              <?php
							  if ($this->uri->segment(4) == '21'){
								echo '<span class="text-info">*MESSAGE HERE*</span><br><br>';  
								echo '<span class="text-info">*LIST OF CRISIS DOCUMENT HERE*</span><br><br>';  
   							    echo 'Please <span class="text-info">click here</span> to log into your user panel.';
							  }
							  if ($this->uri->segment(4) == '22'){
								echo 'Message from org: <span class="text-info">*MESSAGE HERE*</span><br><br>';  
							  }
							  if ($this->uri->segment(4) == '29'){
								echo '<span class="text-info">*STEP CATEGORY STATUS TABLE HERE*</span><br><br>';  
							  }
							  ?>  
                              
                                <br> <a href="javascript:;" title="Click to edit button text" class="btn btn-primary btn-lg btn-block email_blue_btn"><?php echo $mess_info[0]['button_text']; //if($this->uri->segment(4) == '31') { echo 'Confirm Receipt'; } else { echo 'Access CrisisFlo'; } ?></a>
                                

                                <div class="row btn_form" style="display: none;">
                                    <div class="col-sm-8">
                                        <input type=" text" data-id="<?php echo $mess_info[0]['id']; ?>" class="form-control" name="btn_txt" value="<?php echo $mess_info[0]['button_text']; ?>" />
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="#" class="btn btn-primary">Submit</a>
                                        <a href="#" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>

                              
                                <p><br />Regards,<br /><span class="text-info">CrisisFlo</span></p>
    
    
                            </div>
                    </form>									
    
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-1">


                            </div>

                        </div>

                        

                        


                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




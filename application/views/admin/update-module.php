                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Module

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Update Module</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>


                    </div>
                    
                    
                    

                    <div class="col-lg-12">
                    	<a href="<?php echo base_url().'webmanager/modules/manage' ?>" class="btn btn-default" style="margin-bottom: 15px;"><i class="fa fa-angle-double-left"></i> Back</a>
                    

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update Module</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Module Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $mods[0]['name']; ?>"><?php echo form_error('name'); ?>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Preselected</label>
                            <div class="col-sm-10">
                                
                                <div class="btn-group" data-toggle="buttons">

                                  <label class="btn btn-default <?php if($mods[0]['preselected'] == 'Y') echo 'active';?>">
                                    <input type="radio" name="preselected" id="option2" value="Y" autocomplete="off" <?php if($mods[0]['preselected'] == 'Y') echo 'checked="checked"';?>> Yes
                                  </label>
                                  <label class="btn btn-default <?php if($mods[0]['preselected'] == 'N') echo 'active';?>">
                                    <input type="radio" name="preselected" id="option3" value="N" autocomplete="off" <?php if($mods[0]['preselected'] == 'N') echo 'checked="checked"';?>> No
                                  </label>
                                </div>

                            </div>

                        </div>

                        <div class="form-group <?php if($mods[0]['sort'] > 5) { echo 'hidden'; }?>">
                            <label class="col-sm-2 control-label">Default Max Users</label>
                            <div class="col-sm-10">
                                <select name="max_users" class="form-control">
                                	<option value="5" <?php if($mods[0]['default_users'] == '5') { echo 'selected="selected"';}?>>5</option>
                                	<option value="10" <?php if($mods[0]['default_users'] == '10') { echo 'selected="selected"';}?>>10</option>
                                	<option value="20" <?php if($mods[0]['default_users'] == '20') { echo 'selected="selected"';}?>>20</option>
                                	<option value="50" <?php if($mods[0]['default_users'] == '50') { echo 'selected="selected"';}?>>50</option>
                                	<option value="1000" <?php if($mods[0]['default_users'] == '1000') { echo 'selected="selected"';}?>>unlimited(for single user)</option>
                                
                                </select>
                            </div>

                        </div>

                    
                    
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary" name="add_mod" id="add_mod">Submit</button>
                            </div>

                        </div>

                        

                        

                    </form>									

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




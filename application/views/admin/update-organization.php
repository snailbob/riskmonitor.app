                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Organization

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Update Organization</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Crisis Organization</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-update-organization" id="frm-update-organization" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-3 control-label">Organization Name</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="" required data-msg-required="Please enter organization name" value="<?php echo $org_info[0]['organization_name']; ?>"><?php echo form_error('organization_name'); ?>

                            </div>

                        </div>

                        
                        
                         <div class="form-group">

                            <label class="col-sm-3 control-label">Reseller</label>

                            <div class="col-sm-9">

                               <select name="reseller" id="reseller" class="form-control">
                               		<option value="">Select</option>
                                    <?php

										if(count($resellers) > 0){
											foreach($resellers as $r=>$value){
												echo '<option value="'.$value['login_id'].'"';
												
												if($value['login_id'] == $org_info[0]['reseller_id']){
													echo ' selected="selected"';
												}
												
												echo '>'.$this->master_model->decryptIt($value['first_name']).' '.$this->master_model->decryptIt($value['last_name']).'</value>';
											}
										}
									?>
                               </select>

                            </div>


                        </div><!--.add Reseller-->
                        
                        

                        <div class="form-group">

                            <label for="firstname" class="col-sm-3 control-label">Modules</label>

                            <div class="col-sm-9">
								<?php 
								
								foreach($modules as $md=>$mds){
								
									echo '
										  <div class="checkbox">
											<label for="checkboxes-'.$mds['id'].'">
											  <input type="checkbox" name="modules[]" id="checkboxes-'.$mds['id'].'" value="'.$mds['id'].'"';
											  
									if (strpos($org_info[0]['active_module'], 'm'.$mds['id'].'m') !== false){
										echo 'checked="checked"';
									}
									echo '  onclick="check_module(\''.$mds['id'].'\');">
											  <input type="hidden" name="modulesp[]" id="module-'.$mds['id'].'" value="'.$mds['price'].'">'.
											  $mds['name']
										 .'</div>
									
									';
								
								?>
                                
                        <div class="form-group <?php if($mds['id'] != 1 && $mds['id'] != 5 && $mds['id'] != 2 && $mds['id'] != 4 && $mds['id'] != 8){ echo 'hidden'; }?>" style="display: none; padding-left: 15px; padding-right: 15px;">
								<?php
									if($mds['id'] == 1){
										$mymodule = 'standard_max';
									}
									else if($mds['id'] == 5){
										$mymodule = 'recall_max';
									}
									else if($mds['id'] == 2){
										$mymodule = 'case_max';
									}
									else if($mds['id'] == 4){
										$mymodule = 'member_max';
									}
									else if($mds['id'] == 8){
										$mymodule = 'continuity_max';
									}
								?>

                                <label class="control-label">Max Users</label>
                                <select class="form-control" id="maxu<?php echo $mds['id'] ?>" name="maxu<?php echo $mds['id'] ?>">
                                    <option value="0">Select</option>	

                                    <option value="5" <?php if ($org_info[0][$mymodule] == '5') { echo 'selected="selected"'; } ?>>up to 5 users</option>	

                                    <option value="10" <?php if ($org_info[0][$mymodule] == '10') { echo 'selected="selected"'; } ?>>up to 10 users</option>	

                                    <option value="20" <?php if ($org_info[0][$mymodule] == '20') { echo 'selected="selected"'; } ?>>up to 20 users</option>	

                                    <option value="50" <?php if ($org_info[0][$mymodule] == '50') { echo 'selected="selected"'; } ?>>up to 50 users</option>	

                                </select><span class="form-helper text-red" id="maxu<?php echo $mds['id'] ?>_error"></span>

                        </div>

                        
                                
                                
                                <?php
								}
								
								?>

                              <?php echo form_error('modules[]'); ?><span class="form-helper text-red" id="modules_error"></span>
                            </div>

                        </div>

                        
        				<div class="form-group">
                          <label class="col-sm-3 control-label"></label>
                          
                          <div class="col-sm-9">
                                <div class="col-sm-12">
                                
                                    
                                    <div class="simulation_module" style="display: none; ">
                                        <?php
                                            $simulation_modules = $this->master_model->getRecords('simulation_modules');
                                            
                                            if(count($simulation_modules) > 0){
                                                echo '
                                                <div class="form-group well">
                                                <label>Simulation Modules</label>';
                                                foreach($simulation_modules as $r=>$value){
        
                                        ?>
                                        
                                        
            <div class="radio">
            <label>
            <input type="radio" name="simulation_modules" value="<?php echo $value['id'] ?>" <?php if($org_info[0]['simulation_id'] == $value['id']) { echo 'checked="checked"'; } ?>>
            <?php echo $value['name'] ?>
            </label><a href="#" class="view_simu_list_btn" data-id="<?php echo $value['id']; ?>">View List</a>
            </div>
                                         
                                        <?php
                                                }
                                                echo '</div>';
                                            }
                                        ?>
                                    </div>
                             
                             
                                </div>
                               
    
                          
                          </div>
                        
                        
                        </div>

                               
                        
                        
                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-sm-3 control-label" for="radios">Customer Type</label>
                          <div class="col-sm-9">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0" <?php if ($org_info[0]['cust_type'] == '0'){ echo 'checked="checked"'; } ?>>
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1" <?php if ($org_info[0]['cust_type'] == '1'){ echo 'checked="checked"'; } ?>>
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>
                        
                        

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Select CC</label>

                            <div class="col-sm-9">

                               <select class="form-control" name="sel_cc" id="sel_cc">

                                    <option value="">Select</option>

                                    <?php

									if(count($cc_list)>0)

									{

										foreach($cc_list as $cc)

										{

									?>

                                    <option value="<?php echo $cc['login_id']; ?>" <?php if($cc['login_id']==$org_info[0]['cc_id'])echo 'selected="selected"'; ?>>

										<?php echo $this->master_model->decryptIt($cc['crt_first_name']).' '.$this->master_model->decryptIt($cc['crt_last_name']); ?>

                                    </option>

                                    

                                    <?php

										}

									} 

									?>

                               </select>

                            </div>

                        </div>
                        
                        


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Preload Input Pack (Optional)</label>

                            <div class="col-sm-9">
                               <input type="text" class="form-control hidden" id="old_packs" name="old_packs" value="<?php echo $org_info[0]['input_pack_id']; ?>">
                                
                               <select class="form-control" name="input_packs" id="input_packs">

                                    <option value="0" >Select</option>
                                    <?php /*?><option value="0" <?php if($org_info[0]['input_pack_id']=='0'){ echo 'selected="selected"'; } ?>>No Preload</option><?php */?>

                                    <?php

									if(count($input_packs)>0)

									{

										foreach($input_packs as $packs)

										{

									?>

											<?php if($org_info[0]['input_pack_id'] == $packs['id'] || $packs['org_id'] =="0"){ ?>
                                    
                                    	<option value="<?php echo $packs['id']; ?>" <?php if($org_info[0]['input_pack_id'] == $packs['id']){ echo 'selected="selected"'; }?>> <?php echo $packs['name']; ?> </option>
                                        
                                        

                                    <?php
											}
										}

									} 

									?>

                               </select>
                              <?php echo form_error('input_packs'); ?>
                            </div>

                        </div>
                        
                        
                        
                        

                        <div class="form-group">

                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-9">

                            <button type="submit" class="btn btn-primary" name="add_orgz" id="add_orgz">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




<!-- Modal -->
<div class="modal fade" id="viewSimulationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="body" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="lead">Simulation List</p>
        <p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i></p>
		
        <div class="simulist">
                    
            <div class="panel-group" id="accordion" role="tablist">
                     
            </div>        
        
        </div>
		
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update States

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Dashboard</a></li>

                                <li class="active">Update States</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update States</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">States</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="name" name="name" placeholder="" data-msg-required="Please enter first name" value="<?php echo $states[0]['name']; ?>"><?php echo form_error('name'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Country</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="country" name="country">
                                    <option value="">Select..</option>	
                                <?php if(count($all_country)!="0"){
                                    
                                    foreach($all_country as $country){
                                        
                                        
                                        echo '<option value="'.$country['country_id'].'"';
										
										if ($states[0]['country_id'] == $country['country_id']){
											echo 'selected="selected"';
										}
										
										echo '>'.$country['short_name'].'</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select><?php echo form_error('country'); ?>

                            </div>


                        </div>


                      
                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_mod" id="add_mod">Submit</button>

                            </div>

                        </div>

                        

                        

                    </form>									

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




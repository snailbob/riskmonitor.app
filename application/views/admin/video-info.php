                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Video Conference Center</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Video Conference Center</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa fa-video-camera"></i> Video Conferencing Usage Monitoring</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="50%">Organization Name</th>

                                        <th width="50%">Co-ordinator</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($org_list)>0)

								{

									foreach($org_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['organization_name']; ?></td>

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); ?></td>
                                        
                                    	<?php /*?><td><?php echo $value['conf_stream']; ?></td><?php */?>

                                    	<td class="text-right">

                                        <a class="hidden btn btn-default btn-sm" href="<?php echo base_url(); ?>webmanager/videocenter/update/<?php echo  $value['organization_id']; ?>">Allocate</a> 
                                        
                                        <a class="btn btn-default btn-sm"  href="<?php echo base_url(); ?>webmanager/videocenter/usage/<?php echo  $value['organization_id']; ?>">Show Data</a> 

                                    	</td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Video Conferencing Center</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Video Conferencing Usage</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>


                                        
                       <?php /*?> <!-- Line Chart Example -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4><?php echo $org_info[0]['organization_name']?> Line Chart</h4>
                                </div>
                                <div class="panel-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#lineChart"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="lineChart" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div id="morris-chart-line"></div>
                                </div>
                            </div>
                        </div><?php */?>


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4><?php echo $org_info[0]['organization_name']?> Statistics</h4>
                                </div>
                                <div class="panel-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#lineChart"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="lineChart" class="panel-collapse collapse in">
                                <div class="panel-body">

									<?php
           
										if(count($usage_info)>0)
		
										{
    									
									?>
									<div class="well" style="background:;">
    
                                        <p>Total Time: <?php echo $the_total_time; ?> minutes</p>
                                        <p>Total Video Streamed: <?php echo $total_vid; ?></p>
                                        <p>First Usage: <?php echo date_format(date_create($usage_info[0]['setup_date']),'Y-m-d - g:i:sa'); ?></p>
                                        <p>Latest Usage: <?php echo date_format(date_create($usage_info[count($usage_info)-1]['setup_date']),'Y-m-d - g:i:sa'); ?></p>
                                        <p><a class="btn btn-sm btn-default" href="javascript: $('#the_table').toggle();">Show Table</a></p>

									</div>
                                    <div class="table-responsive" id="the_table" style="display: none;">
            
                                        <table class="table table-hover table-bordered table-green" id="example-table">
            
                                            <thead>
            
                                                <tr>
            
                                                    <th>Meeting Name</th>
            
                                                    <th>Video Streamed</th>
            
                                                    <th>Date Initiated</th>
            
                                                    <th>Time Consumed</th>
        
                                                </tr>
            
                                            </thead>
            
                                            <tbody>
            
                                            <?php 
         
                                                foreach($usage_info as $r => $value)
            
                                                {
            
                                            ?>
            
                                                                    
            
                                                <tr>
            
                                                    <td><?php echo  $value['meeting_name']; ?></td>
        
                                                    <td><?php echo  $value['streamed']; ?></td>
        
                                                    <td><?php
														$completed_date = date_create($value['setup_date']);
														echo date_format($completed_date, 'Y-m-d - g:i:sa');
													
													?></td>
        
                                                    <td><?php
																					
								
														 $date_pieces = explode(" ",$value['lapse_time']);
														 /*if ($date_pieces[0] !=0 ){echo $date_pieces[0].' years '; }
														 if ($date_pieces[1] !=0 ){echo $date_pieces[1].' months '; }
														 if ($date_pieces[2] !=0 ){echo $date_pieces[2].' days '; }
														 if ($date_pieces[3] !=0 ){echo $date_pieces[3].' hours '; }
														 if ($date_pieces[4] !=0 ){echo $date_pieces[4].' minutes '; }
														 if ($date_pieces[5] !=0 ){echo $date_pieces[5].' seconds'; }*/
														 
														 if ($date_pieces[0] ==0 ){}
														 else if ($date_pieces[0] ==1 ){
															 echo $date_pieces[0].' year '; }
														 else{
															 echo $date_pieces[0].' years '; }
															 
														 if ($date_pieces[1] ==0 ){}
														 else if ($date_pieces[1] ==1 ){
															 echo $date_pieces[1].' month '; }
														 else{
															 echo $date_pieces[1].' months '; }
															 
														 if ($date_pieces[2] ==0 ){}
														 else if ($date_pieces[2] ==1 ){
															 echo $date_pieces[2].' day '; }
														 else{
															 echo $date_pieces[2].' days '; }
									
														 if ($date_pieces[3] ==0 ){}
														 else if ($date_pieces[3] ==1 ){
															 echo $date_pieces[3].' hour '; }
														 else{
															 echo $date_pieces[3].' hours '; }
									
														 if ($date_pieces[4] ==0 ){}
														 else if ($date_pieces[4] ==1 ){
															 echo $date_pieces[4].' minute '; }
														 else{
															 echo $date_pieces[4].' minutes '; }
									
														 if ($date_pieces[5] ==0 ){}
														 else if ($date_pieces[5] ==1 ){
															 echo $date_pieces[5].' second '; }
														 else{
															 echo $date_pieces[5].' seconds '; }
														 
														 
								
																					
													?></td>
        
            
                                                </tr>
            
                                            <?php 
            
                                                }
            

                                            ?>
            
                                                                
        
                                            </tbody>
        
                                            
        
                                            
        
                                        </table>
        
                                    </div>
        
                                    <!-- /.table-responsive -->
								  <?php 
    

                                    }else{ ?>
										<p class="text-center text-muted">No data to show.</p>
									<?php }
    
                                    ?>
                                      
                                    
                                    
                                </div>
                            </div>
                        </div>




                    </div>
                    <!-- /.col-lg-12 -->                                        




                </div><!--.row -->

					




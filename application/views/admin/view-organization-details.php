<style>

.dataTables_filter, .dataTables_length{
	display: none;
	visibility: hidden;
}
</style>   

<?php
	$customer_type = $this->uri->segment(2);
?>
          
             
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Organization</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage Organization</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12 hidden" style="height: 40px;">

                    <a class="btn btn-primary pull-right" href="<?php echo base_url();?>webmanager/organization/add">Add Organisation</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>


		<a href="<?php echo base_url().'webmanager/'.$customer_type; ?>" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</a>
        <div class="clearfix" style="margin-top: 15px;"></div>

   <?php /*?> <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"><?php */?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4>
                    <span data-toggle="collapse" data-parent="#accordion" href="#collapseOrgDetails" aria-expanded="true" aria-controls="collapseOrgDetails">Organization Details</span>
                </h4>
            </div>
            <div id="collapseOrgDetails" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                	<?php
						echo '<p>Organization Name: '.$org_data[0]['organization_name'].'</p>';
						echo '<p>CC Assigned: '.$org_data[0]['cc_name'].'</p>';
						echo '<p>Date Organization was provisioned: '.$org_data[0]['date_added'].'</p>';
						
						$modules = $org_data[0]['modules'];
						echo '<p>Active Modules';
						if(count($modules) > 0){
							echo '<ol>';
							foreach($modules as $module){
								echo '<li>'.$module.'</li>';
							
							}
							echo '</ol>';
						}
						else{
							echo '<br><span class="text-muted">No Modules</span>';
						}
						echo '</p>';
						
						
						$stks = $org_data[0]['stakeholders'];
						echo '<p>Stakeholders';
						if(count($stks) > 0){
							echo '<ol>';
							foreach($stks as $module){
								echo '<li>'.$module.'</li>';
							
							}
							echo '</ol>';
						}
						else{
							echo '<br><span class="text-muted">No Stakeholders</span>';
						}
						echo '</p>';
					?>
                </div>
            </div>
        </div>
        
        
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingCrts">
                <h4>
                    <span data-toggle="collapse" data-parent="#accordion" href="#collapseCrts" aria-expanded="true" aria-controls="collapseCrts">CRTs assigned to Organization</span>
                </h4>
            </div>
            <div id="collapseCrts" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingCrts">
                <div class="panel-body">
                
                	<?php
						$module_crts = $org_data[0]['module_crts'];
						if(count($module_crts) > 0){
							foreach($module_crts as $r=>$module){
								echo '<p><strong>'.$module['name'].'</strong></p>';
								$crts = $module['crts'];
								
								if(count($crts) > 0){
									echo '<ol>';
									foreach($crts as $rts=>$crt){
										echo '<li>'.$crt['full_name'].'</li>';
									}
									echo '</ol>';
								}
								else{
									echo '<p class="text-muted">No CRT found.</p>';
								}
							}
						}
						else{
							echo '<p class="text-muted">No module.</p>';
						}
					?>
                </div>
            </div>
        </div>
        
        
        
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPurchaser">
                <h4>
                    <span data-toggle="collapse" data-parent="#accordion" href="#collapsePurchaser" aria-expanded="true" aria-controls="collapsePurchaser">Authorized Purchaser Details</span>
                </h4>
            </div>
            <div id="collapsePurchaser" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingPurchaser">
                <div class="panel-body">
    				<?php
					
					if(count($purchaser_data) > 0){

						foreach($purchaser_data as $r=>$value){
							$content = unserialize($value['content']);
							foreach($content as $cont){
								$name = $cont['name']; 
								if($name != 'lat' && $name != 'lng' && $name != 'country_code' && $name != 'mobile' && $name != 'country_short' && $name != 'stripeToken' && $name != 'subs_plan_id'){
									$value = $cont['value'];
									if($name == 'plan'){
										$value = explode('__', $value);
										$value = $value[1];
									}
									
									$name = $cont['name']; // str_replace('_', ' ', $name);
									$name = ucfirst($name);
									echo $name.': <span class="text-muted">'.$value.'</span><br/>';	
								}
							}
						}
						
					}
					else{
						echo '<p class="text-muted">No authorized purchaser found.</p>';
					}					
					?>
                </div>
            </div>
        </div>

        
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingStripe">
                <h4>
                    <span data-toggle="collapse" data-parent="#accordion" href="#collapseStripe" aria-expanded="true" aria-controls="collapseStripe">Stripe Details</span>
                </h4>
            </div>
            <div id="collapseStripe" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingStripe">
                <div class="panel-body">
					<?php

					
					if(count($cc) == 0){
						echo '<p class="text-muted">No stripe details found.</p>';
					}
					else{
						$from_stripe = $from_stripe['the_subsription_data'];
						if(count($from_stripe) > 0){
							
							foreach($from_stripe as $r=>$stripe){
								echo '<p>Plan Subscribed: '.$stripe['plan']['name'].'</p>';
								echo '<p>Current Subscription Period Started: '.$stripe['current_period_start'].'</p>';
								echo '<p>Current Subscription Period End: '.$stripe['current_period_end'].'</p>';
								$price = $stripe['plan']['amount'] /100;
								
								$price = number_format((float)$price, 2, '.', '');
								echo '<p><span class="bg-success">Price: $'.$price.' (Paid)</span></p>';
							}
						}
						else{
							echo '<p class="text-muted">No plan subscription.</p>';
						}
						
					}
					
					?>
                </div>
            </div>
        </div>
        
                
        
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingLogs">
                <h4>
                    <span data-toggle="collapse" data-parent="#accordion" href="#collapseLogs" aria-expanded="true" aria-controls="collapseLogs">CC/CRT Login/Logout Date and Time</span>
                </h4>
            </div>
            <div id="collapseLogs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingLogs">
                <div class="panel-body">
                	<?php
					if(count($the_logs) > 0){
					?>
                    <table class="table table-hover" id="example-table">
                        <thead class="hidden">
                            <tr>
                                <th class="hidden">Sort</th>
                                <th>Event</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
							$i= 1;
							foreach($the_logs as $r=>$logs){
								echo '<tr><td class="hidden">'.$i.'</td><td>'.$logs['user_name'].' '.$logs['action'].' '.$logs['module_name'].' on '.$logs['date_format'].'</td></tr>';
								$i++;
							
							}
							?>
                        </tbody>
                    </table>
                    
                    <?php } //if(count($the_logs) > 0) 
						else{ ?>
                        <p class="text-muted">No Log found.</p>
                        <?php }?>
                    
                </div>
            </div>    
        </div>
        
    
    <?php /*?></div><?php */?>
    


                        <div class="panel panel-default hidden">
                            <div class="panel-heading">

                                <div class="panel-title">
                                    <h4><i class="fa fa-user fa-fw"></i>Crisis Organization</h4>
                                </div>
                                <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">

                            </div>
                        </div><!-- /.panel -->








                        

                    </div>



                </div><!--.row -->

					


<!-- Modal -->
<div class="modal fade" id="orgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Organization Information</h4>
		<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i> Loading...</p>
        <div class="org_info">
        	
        </div>
        
        
        
      </div>
    </div>
  </div>
</div>


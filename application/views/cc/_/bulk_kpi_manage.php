<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Recall KPI
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Recall KPI</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">

                    <div class="col-lg-12">

						<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                             <?php echo $this->session->flashdata('success');   ?>

                        </div>

                        <?php } if($this->session->flashdata('error')!=""){ ?>

                        <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
                        </div>

                        <?php } ?>

                        </div>

                    </div>
                    
                    
                    
                    <button class="btn btn-primary pull-left" data-toggle="modal" data-target="#myRecalls"><?php echo count($incidents); ?> Open Incidents</button>      
                    <a href="<?php echo base_url().'cc/kpi/owners/'.$this->uri->segment(4); ?>" class="btn btn-primary pull-right">Manage KPI Owners</a>
                    <div class="clearfix" style="margin-bottom: 15px;"></div>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> <?php echo $the_incident[0]['incident_no'].': '.$the_incident[0]['incident_name']; ?> <?php if ($the_incident[0]['initiation_type']==0){echo '<span class="label label-default">Mock</span> - '; } else{ echo '<span class="label label-info">Live</span>'; } ?> Recall KPI</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#kpipanel"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="kpipanel" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                




<div class="table-responsive">

<table class="table kpirecontable" id="example-tablexx">

    <thead>

        <tr>
            
            <th>Product Reconcilliation</th>
            
            <th width="10%" class="text-center bg-primary" style=" vertical-align: middle; border: 2px solid #285e8e; border-bottom: 1px solid #ddd; border-right: 1px solid #ddd;">Total Units</th>
            <th width="10%" class="text-center bg-primary" style=" vertical-align: middle; border: 2px solid #285e8e; border-bottom: 1px solid #ddd; border-right: 1px solid #ddd;">Units Returned</th>
            <th width="32%" class="text-center bg-primary" style="padding: 0 0 0 0 !important;  border: 2px solid #285e8e; border-bottom: 1px">
            <h5 style="font-weight: bold;">Corrective Actions</h5>
            
                <table class="table" style="margin-left: -1px; margin-bottom: -1px !important; ">
                  <thead>
                    <tr class="bg-primary">
                        <td style=" border: 1px solid #ddd; border-bottom: none;" width="33%">Units Disposed</td>
                        <td style=" border: 1px solid #ddd; border-bottom: none;" width="33%">Units Transformed</td>
                        <td style=" border: 1px solid #ddd; border-right: none; border-bottom: none;" width="33%">Total Corrections</td>
                    </tr>
                  </thead>
                </table>
            
            </th>
            <th width="10%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">KPI</th>
            <th width="10%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Threshold</th>
            <th width="5%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;"></th>



        </tr>

    </thead>

    <tbody>
    
        <?php
		
			$count_all_kpi = 1;
			foreach($all_kpi as $r=>$value){
                $kpi_cell = $value['dbcell'];
                if($value['type'] != 'comp_kpi') {
                    $akpiclass = 'text-primary';
                    $akpistyle = 'cursor: pointer;';
                    
                    if($value['type'] == 'dist_kpi'){
                        $akpiclass .=" dist_toggle_btn";
                    }
                    else if($value['type'] == 'market_kpi'){
                        $akpiclass .=" market_toggle_btn";
                    }
                }
                else{
                    $akpiclass= '';
                    $akpistyle= '';
                }
				
				//set variables
				$total_unit = '';
				$unit_return = '';
				$u_disposed = '';
				$u_transformed = '';
				$u_corrections = 'NA';
				$the_kpi = 'NA';
				$u_threshold = '0';
		
				$kpi_id = '';
				
                //check count of kpi
                if(count($kpis) > 0) {
                    $kpi_id = $kpis[0]['id'];
                    $dbcell = unserialize($kpis[0][$value['dbcell']]);
                    if($dbcell != ''){ //get each kpi db cell data
                        $total_unit = number_format($dbcell['total_unit']);
                        $unit_return = number_format($dbcell['unit_return']);
                        $u_disposed = number_format($dbcell['u_disposed']);
                        $u_transformed = number_format($dbcell['u_transformed']);
                        $u_corrections = number_format($dbcell['u_corrections']);
                        $the_kpi = round($dbcell['the_kpi'], 0).'%';
						$u_threshold = round($dbcell['threshold'], 0);
                    
                    }
                }
            ?>
                
        <tr class="tr_main">
            <td><i class="<?php echo $akpiclass; ?>" style="<?php echo $akpistyle; ?>"><?php echo $value['name']; ?>
            <?php
			//display caret
			if($count_all_kpi > 1){
				echo '<span class="fa fa-caret-down"></span>';
			}
			?>
            
            </i><br />
                <button class="btn btn-primary submit_kpis btn-xs hidden disabled submit_kpi_changes" type="submit">Save KPI</button>
                <a class="btn btn-default btn-xs hidden cancel_kpi_btn">Cancel</a>
                
                <button class="btn btn-primary btn-xs hidden thresh_btn disabled submit_kpi_changes" type="submit">Save Threshold</button>
                <a class="btn btn-default btn-xs hidden cancel_thresh_btn">Cancel</a>
            </td>
            
            <td class="text-center" style="border: 2px solid #285e8e; border-top: 1px solid #ddd; <?php if ($count_all_kpi != count($all_kpi)) { echo ' border-bottom: 1px solid #ddd;'; } ?>">
				<?php //check count of owners
				if((count($both_notdeldist_owners) > 0 && $count_all_kpi == 2) || (count($both_notdelmkt_owners) > 0 && $count_all_kpi == 3) || (count($dist_owners) > 0 && $count_all_kpi == 2) || (count($market_owners) > 0 && $count_all_kpi == 3)){ ?>
                    <span class="empty_kpi"><?php echo $total_unit; ?></span>
                <?php }
                else{ ?>
                    <a href="#" class="empty_kpi" title="click to edit"><?php echo $total_unit?></a>
                <?php } ?>
           
            
                <input type="text" class="form-control kpi_input hidden" name="total_unit" value="<?php echo $total_unit; ?>"/>
                <input type="hidden" class="form-control kpi_input hidden" name="id" value="<?php echo $kpi_id; ?>"/>
                <input type="hidden" class="form-control kpi_input hidden" name="kpi_cell" value="<?php echo $kpi_cell; ?>"/>
                <input type="hidden" class="form-control kpi_input hidden" name="incident_id" value="<?php echo $this->uri->segment(4); ?>"/>
            
            </td><!--/total_unit-->
            <td class="text-center" style="border: 2px solid #285e8e; border-top: 1px solid #ddd; <?php if ($count_all_kpi != count($all_kpi)) { echo ' border-bottom: 1px solid #ddd;'; } ?>">
				<?php //check count of owners
				if((count($both_notdeldist_owners) > 0 && $count_all_kpi == 2) || (count($both_notdelmkt_owners) > 0 && $count_all_kpi == 3) || (count($dist_owners) > 0 && $count_all_kpi == 2) || (count($market_owners) > 0 && $count_all_kpi == 3)){ ?>
                    <span class="empty_kpi"><?php echo $unit_return; ?></span>
                <?php }
                else{ ?>
                    <a href="#" class="empty_kpi" title="click to edit"><?php echo $unit_return; ?></a>
                <?php } ?>
           
           
                
                <input type="text" class="form-control kpi_input hidden" name="unit_return" value="<?php echo $unit_return; ?>" />
            </td><!--/unit_return-->
            
            
            <td class="text-center" style="padding: 0 0 0 0 !important; border: 2px solid #285e8e; border-top: 1px solid #ddd; <?php if ($count_all_kpi != count($all_kpi)) { echo ' border-bottom: 1px solid #ddd;'; } ?>">
                <table class="table" style="margin-top: -1px; margin-left: -1px; margin-bottom: -1px !important; ">
                  <tbody>
                    <tr>
                        
                        <td style=" border: 1px solid #ddd; border-bottom: none;" width="33%">
                        	<?php //check count of owners
							if((count($both_notdeldist_owners) > 0 && $count_all_kpi == 2) || (count($both_notdelmkt_owners) > 0 && $count_all_kpi == 3) || (count($dist_owners) > 0 && $count_all_kpi == 2) || (count($market_owners) > 0 && $count_all_kpi == 3)){ ?>
                                <span class="empty_kpi"><?php echo $u_disposed; ?></span>
							<?php }
							else{ ?>
                                <a href="#" class="empty_kpi" title="click to edit"><?php echo $u_disposed; ?></a>
							<?php } ?>
                            
                            
                            <input type="text" class="form-control kpi_input hidden" name="u_disposed" value="<?php echo $u_disposed; ?>" />
                        </td><!--/u_disposed-->
                        <td style=" border: 1px solid #ddd; border-bottom: none;" class="kpi_transformed" width="33%">
                        	<?php //check count of owners
							if((count($both_notdeldist_owners) > 0 && $count_all_kpi == 2) || (count($both_notdelmkt_owners) > 0 && $count_all_kpi == 3) || (count($dist_owners) > 0 && $count_all_kpi == 2) || (count($market_owners) > 0 && $count_all_kpi == 3)){ ?>
                                <span class="empty_kpi"><?php echo $u_transformed; ?></span>
							<?php }
							else{ ?>
                                <a href="#" class="empty_kpi" title="click to edit"><?php echo $u_transformed; ?></a>
							<?php } ?>
						
							
								
                            
                            <input type="text" class="form-control kpi_input hidden" name="u_transformed" value="<?php echo $u_transformed; ?>" />
                        </td><!--/u_transformed-->
                        <td style=" border: 1px solid #ddd; border-bottom: none; border-right: none;" width="33%">
                            <?php echo $u_corrections; ?>
                        </td><!--/u_corrections-->
                    </tr>
                  </tbody>
                </table>
            
            
            </td><!--/Corrective Actions-->
            <td class="text-center bg-success" style=" border: 1px solid #ddd;">
                <?php echo $the_kpi; ?>
            </td><!--/the_kpi-->
            
            <td class="text-center" style=" border: 1px solid #ddd;">
                <a href="#" class="empty_thresh" title="click to edit"><?php echo $u_threshold; ?>%</a>

                <input type="text" class="form-control hidden kpi_thresh" name="threshold" value="<?php echo $u_threshold; ?>" />
            </td><!--/the_kpi-->
            

            
            <?php
                //stat settings
                if(count($kpis) > 0){
                    if($kpis[0][$value['kpistatus']] == 0){
                        $comp_stat = 'enabled';
                        $comp_class = 'btn-success';
                        $comp_font = 'fa-check';
                    }
                    else{
                        $comp_stat = 'disabled';
                        $comp_class = 'btn-danger';
                        $comp_font = 'fa-times';
                    }
                    
            
                }else{
                    $comp_stat = 'enabled';
                    $comp_class = 'btn-success disabled';
                        $comp_font = 'fa-check';
                }
            ?>



            <td class="text-center" style=" border: 1px solid #ddd;">
                <a href="#"
                class="btn <?php echo $comp_class; ?> btn-xs kpi_stat_btn"
                data-toggle="tooltip" title="<?php echo $comp_stat; ?>"
                data-kpitype="<?php echo $value['kpistatus']; ?>"

                data-kpiid="<?php if(count($kpis) > 0){ echo $kpis[0]['id']; } ?>">
                
                    <i class="fa <?php echo $comp_font; ?> fa-fw"></i>
                    
                </a>
            </td>
            
            
            
        </tr>
        
        <?php
		//display kpi company owners inputs
		if($count_all_kpi > 1) {
			if($count_all_kpi == 2){
				$ownr_class = 'dist_kpi_owners';
			}
			else if($count_all_kpi == 3){
				$ownr_class = 'market_kpi_owners';
			}
		?>
        <tr>
        	<td colspan="7" class="<?php echo $ownr_class; ?>" style="display: none;">
			
            
    <div class="table-responsive" style="margin-bottom: 30px;">
    
        <h4 class="text-left" style="margin-bottom: 15px;">Input from KPI owner(s)
        </h4>
        
        <table class="table table-bordered">
          <thead>
            <tr class="bg-info hidden-lg hidden-md">
              <th class="text-left">Company</th>
              <th width="10%" class="text-center">Total Units</th>
              <th width="10%" class="text-center">Units Returned</th>
              <th width="11%" class="text-center">Units Disposed</th>
              <th width="11%" class="text-center">Units Transformed</th>
              <th width="10%" class="text-center">Total Corrections</th>
              <th width="20%" class="text-center">KPI</th>
              <th width="5%"></th>
            </tr>
          </thead>
          <tbody>
            <?php
			
				if($count_all_kpi == 2){
					$the_owners = $dist_owners;
					$the_kpi_type = 'dist';
					$the_kpi_del_type = 'deleted_dist';
				}
				else if($count_all_kpi == 3){
					$the_owners = $market_owners;
					$the_kpi_type = 'mkt';
					$the_kpi_del_type = 'deleted_mkt';
				}
			
                if(count($all_owners) > 0){
					$count_the_owner = 0;
                    foreach($all_owners as $r=>$ownr){
						
						if(($ownr['kpi_type'] == $the_kpi_type && $ownr[$the_kpi_del_type] == 0) || ($ownr['kpi_type'] == 'all' && $ownr[$the_kpi_del_type] == 0)){
							
							
					
							$dbcell = unserialize($ownr[$value['dbcell']]);
							if($dbcell != ''){ //get each kpi db cell data
								$total_unit = number_format($dbcell['total_unit']);
								$unit_return = number_format($dbcell['unit_return']);
								$u_disposed = number_format($dbcell['u_disposed']);
								$u_transformed = number_format($dbcell['u_transformed']);
								$u_corrections = number_format($dbcell['u_corrections']);
								$the_kpi = round($dbcell['the_kpi'], 0).'%';
								$owner_date_update = $dbcell['date_updated'];
							}
							else{
								$total_unit = 'No data';
								$unit_return = 'No data';
								$u_disposed = 'No data';
								$u_transformed = 'No data';
								$u_corrections = 'NA';
								$the_kpi = 'NA';
								$owner_date_update = 'No data';
							}
						
			?>
                    
                    
            <tr class="text-center">
              <td class="text-left"><?php echo $ownr['company'] ?></td>

              <td width="10%"><span class="text-muted"><?php echo $total_unit; ?></span></td>
              
              <td width="10%"><span class="text-muted"><?php echo $unit_return; ?></span></td>
              
              <td width="11%"><span class="text-muted"><?php echo $u_disposed; ?></span></td>
              
              <td width="11%"><span class="text-muted"><?php echo $u_transformed; ?></span></td>
              
              <td width="10%"><span class="text-muted"><?php echo $u_corrections; ?></span></td>
              
              <td width="20%" class="bg-success"><span class="text-muted"><?php echo $the_kpi; ?></span></td>
              
              <td width="5%"><button class="btn btn-danger btn-xs delete_kpidata_btn" data-toggle="tooltip" title="Delete Data" data-id="<?php echo $ownr['id']; ?>" data-deltype="data" data-type="<?php echo $the_kpi_type; ?>"><i class="fa fa-trash-o"></i></button></td>
              
            </tr>
                    
                    <?php
						$count_the_owner++;
						}//end check if both or particular type of owner
						
						

					
					} //foreach?>
            
            
            <?php
				//display blank on empty owner
				if($count_the_owner == 0){
					echo '  <tr>
							  <td colspan="8">No owners found.</td>
							</tr>';
				
				}

			}else {?>
            <tr>
              <td colspan="8">No owners added.</td>
            </tr>
            <?php }?>
    
          </tbody>
        </table>
    
    </div><!--/dist_kpi_owners-->
            
            
            
            
            </td>
        </tr>
        <?php } ?>
            
        <?php
		$count_all_kpi++;

		} //foreach all_kpi ?><!--/enable/disale-->
    
    

        <tr>
            <td>&nbsp;</td>
            <td colspan="6">
                <p class="hidden"><a href="#" data-toggle="modal" data-target="#newKPIModal" style="display: none;"><i class="fa fa-edit"></i> Edit</a></p>
            </td>
        </tr>
                        

    </tbody>


</table>


</div><!--responsive product recon table-->










<div class="table-responsive">

<table class="table" id="example-tablexx">

    <thead style="bg-primary">

        <tr>
            
            <th>Notifications</th>
            
            <th width="24%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Emails Sent</th>
            <th width="24%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Confirmed Receipts</th>
            <th width="24%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">KPI</th>
            <th width="5%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;"></th>



        </tr>

    </thead>

    <tbody>
    
        <tr>
            <td><i>Outbound notifications</i></td>
            <td class="text-center" style=" border: 1px solid #ddd;">
                <?php echo $email_sent_count; ?>
            </td>
            <td class="text-center" style=" border: 1px solid #ddd;">
                <?php echo $email_confirmed_count; ?>
            </td>
            <td class="text-center bg-success" style=" border: 1px solid #ddd;">
                <?php echo $noti_kpi; ?>%
            </td>

            
            <?php
                //stat settings
                if(count($kpis) > 0){
                    if($kpis[0]['noti_stat'] == 0){
                        $comp_stat = 'enabled';
                        $comp_class = 'btn-success';
                        $comp_font = 'fa-check';
                    }
                    else{
                        $comp_stat = 'disabled';
                        $comp_class = 'btn-danger';
                        $comp_font = 'fa-times';
                    }
                    
            
                }else{
                    $comp_stat = 'enabled';
                    $comp_class = 'btn-success disabled';
                        $comp_font = 'fa-check';
                }
            ?>



            <td class="text-center" style=" border: 1px solid #ddd;">
                <a href="#"
                class="btn <?php echo $comp_class; ?> btn-xs kpi_stat_btn"
                data-toggle="tooltip" title="<?php echo $comp_stat; ?>"
                data-kpitype="noti_stat"
                data-kpiid="<?php if(count($kpis) > 0){ echo $kpis[0]['id']; } ?>">
                
                    <i class="fa <?php echo $comp_font; ?> fa-fw"></i>
                    
                </a>
            </td>


        </tr><!--consumer market-->

        <tr>
            <td>&nbsp;</td>
            <td colspan="4"></td>
        </tr>

                        

    </tbody>


</table>

</div>

<!-- /.table-responsive -->




<div class="table-responsive">

<table class="table customcontactstable" id="example-tablexx">

    <thead>

        <tr>
            
            <th>Customer Contacts</th>
            
            <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Contact Centre</th>
            <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Email</th>
            <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Website</th>
            <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Trading Partner</th>
            <th width="5%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;"></th>



        </tr>

    </thead>

    <tbody>
    
        <?php
            if(count($kpis) > 0) {
                if($kpis[0]['contact_cntr'] !=''){
                    $contact_cntr = number_format($kpis[0]['contact_cntr']);
                    $kpi_email = number_format($kpis[0]['kpi_email']);
                    $kpi_website = number_format($kpis[0]['kpi_website']);
                    $kpi_trading = number_format($kpis[0]['kpi_trading']);
                
                }else{
                    $contact_cntr = '';
                    $kpi_email = '';
                    $kpi_website = '';
                    $kpi_trading = '';
                    
                }
            }
            else{
                $contact_cntr = '';
                $kpi_email = '';
                $kpi_website = '';
                $kpi_trading = '';
                
            }
        ?>
    
        <tr class="tr_main">
            <td><i>Inbound customer contacts</i><br />
                <button class="btn btn-primary submit_kpis btn-xs hidden disabled submit_kpi_changes" type="submit">Save</button>
                <a class="btn btn-default btn-xs hidden cancel_kpi_btn">Cancel</a>
            
            </td>
            <td class="text-center" style=" border: 1px solid #ddd;">
                <a href="#" class="empty_kpi" title="click to edit"><?php echo $contact_cntr; ?></a>
            

                <input type="text" class="form-control kpi_input hidden" name="contact_cntr" value="<?php echo $contact_cntr; ?>"/>
                
                <input type="hidden" class="form-control kpi_input hidden" name="id" value="<?php if(count($kpis) > 0) { echo $kpis[0]['id']; }?>"/>
                
                <input type="hidden" class="form-control kpi_input hidden" name="incident_id" value="<?php echo $this->uri->segment(4); ?>"/>
                <input type="hidden" class="form-control kpi_input hidden" name="incident_type" value="<?php echo substr($this->session->userdata('org_module'), 0, 1); ?>"/>
                
                
            </td>
            <td class="text-center" style=" border: 1px solid #ddd;">
                
                <a href="#" class="empty_kpi" title="click to edit"><?php echo $kpi_email; ?></a>

                <input type="text" class="form-control kpi_input hidden" name="kpi_email" value="<?php echo $kpi_email; ?>"/>
                
            </td>
            <td class="text-center" style=" border: 1px solid #ddd;">
               
                
                <a href="#" class="empty_kpi" title="click to edit"><?php echo $kpi_website?></a>

                <input type="text" class="form-control kpi_input hidden" name="kpi_website" value="<?php echo $kpi_website; ?>"/>
                
            </td>
            <td class="text-center" style=" border: 1px solid #ddd;">
              
                <a href="#" class="empty_kpi" title="click to edit"><?php echo $kpi_trading; ?></a>

                <input type="text" class="form-control kpi_input hidden" name="kpi_trading" value="<?php echo $kpi_trading;?>"/>
               
    
            </td>
            
            
            <?php
                //stat settings
                if(count($kpis) > 0){
                    if($kpis[0]['contact_stat'] == 0){
                        $comp_stat = 'enabled';
                        $comp_class = 'btn-success';
                        $comp_font = 'fa-check';
                    }
                    else{
                        $comp_stat = 'disabled';
                        $comp_class = 'btn-danger';
                        $comp_font = 'fa-times';
                    }
                    
            
                }else{
                    $comp_stat = 'enabled';
                    $comp_class = 'btn-success disabled';
                        $comp_font = 'fa-check';
                }
            ?>



            <td class="text-center" style=" border: 1px solid #ddd;">
                <a href="#"
                class="btn <?php echo $comp_class; ?> btn-xs kpi_stat_btn"
                data-toggle="tooltip" title="<?php echo $comp_stat; ?>"
                data-kpitype="contact_stat"
                data-kpiid="<?php if(count($kpis) > 0){ echo $kpis[0]['id']; } ?>">
                
                    <i class="fa <?php echo $comp_font; ?> fa-fw"></i>
                    
                </a>
            </td>
            

        </tr><!--consumer market-->
        
        <?php if(count($kpis) > 0) {
            if($kpis[0]['kpi_trading'] !=''){
            ?>


        <tr>
            <td>&nbsp;</td>
            <td colspan="5" style="border: 1px solid #ddd;">
            
            
                                    

                <!-- Pie Chart Example -->
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-chart-pie"></div>
                </div>
                
                <p class="hidden">&nbsp;<a href="#" data-toggle="modal" data-target="#chartDataModal" style="display: none;"><i class="fa fa-edit"></i> Edit</a></p>
            
            
            </td>
        </tr>

        <?php } } ?>

   
                        

    </tbody>


</table>

</div>

<!-- /.table-responsive -->




                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

					

        
        
        
        

<!-- Modal -->
<div class="modal fade" id="chartDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        
        
        <form id="chart_form">
    

            <div class="form-group">
                <label>Contact Centre</label>
                <input class="form-control" type="text" name="contact_cntr" id="contact_cntr" value="<?php if(count($kpis) > 0) { echo $kpis[0]['contact_cntr']; } ?>"/>
                <input class="form-control kpi_id" type="hidden" name="kpi_id" value="<?php if(count($kpis) > 0) { echo $kpis[0]['id']; } ?>"/>
                <input class="form-control" type="hidden" name="assoc_incident" value="<?php echo $this->uri->segment(4); ?>"/>
                <span class="form-helper text-danger"></span>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" name="kpi_email" id="kpi_email" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_email']; } ?>"/>
                <span class="form-helper text-danger"></span>
            </div>

            <div class="form-group">
                <label>Website</label>
                <input class="form-control" type="text" name="kpi_website" id="kpi_website" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_website']; } ?>"/>
                <span class="form-helper text-danger"></span>
            </div>

            <div class="form-group">
                <label>Trading Partner</label>
                <input class="form-control" type="text" name="kpi_trading" id="kpi_trading" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_trading']; } ?>"/>
                <span class="form-helper text-danger"></span>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" disabled="disabled" type="submit">Submit</button>
            </div>
                
        </form>
      </div>
    </div>
  </div>
</div>


        
        
        


<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Incident</h4>
        <ul class="list-group list-default">
            <?php if(count($incidents) > 0){
                $incidents_id = $this->uri->segment(4);
                foreach($incidents as $open=>$opeen){
            ?>
            
            <a href="<?php echo base_url().'cc/kpi/manage/'.$opeen['id'] ?>" class="list-group-item <?php if ($incidents_id == $opeen['id']) { echo 'active'; }?>"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
            
            <?php
                } 
            } ?>
        </ul>


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myAddPinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">New Marker</h4>
      </div>
      <div class="modal-body">
      		<form id="nm_form">
            	<div class="form-group">
                	<label>Location</label>
                    <input type="text" class="form-control nm_location" name="nm_location" id="nm_location" />
                    <input type="hidden" name="nm_recall_id" id="nm_recall_id" value="<?php echo $recall[0]['id']?>"/>
                    <input type="hidden" name="lat" id="lat" value=""/>
                    <input type="hidden" name="lng" id="lng" value=""/>
                    <input type="hidden" name="nm_id" id="nm_id" value=""/>
                    <span class="control-helper text-red hidden">Mandatory field.</span>
                </div>
            	<div class="form-group">
                	<label>Comment</label>
                    <textarea class="form-control" rows="5" name="nm_comment" id="nm_comment"></textarea>
                    <span class="control-helper text-red hidden">Mandatory field.</span>
                </div>
            	<div class="form-group">
                	<button class="btn btn-primary">Save</button>
                </div>
            </form>
      </div>
    </div>
  </div>
</div>

<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Recall KPIs
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Recall KPIs</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">

                    <div class="col-lg-12">

						<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                             <?php echo $this->session->flashdata('success');   ?>

                        </div>

                        <?php } if($this->session->flashdata('error')!=""){ ?>

                        <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
                        </div>

                        <?php } ?>

                        </div>

                    </div>
                    
                    
                    
                    <a href="<?php echo base_url().'cc/kpi/manage/'.$this->uri->segment(4); ?>" class="btn btn-default pull-left"><i class="fa fa-angle-double-left"></i> Manage Dashboard</a>   
                    
                       
                    <button class="btn btn-primary pull-right" style="margin-left: 5px;" data-toggle="modal" data-target="#kpiOwnerModal" onclick="$('#owner_form').find('input[type=text]').val('');$('#owner_id').val('');$('[name=owner_type]').prop('checked',false); $('[name=owner_type][value=dist]').prop('checked',true);$('#owner_form').find('button').attr('disabled','disabled');">Add KPI Owner</button> 
                    <button class="btn btn-primary pull-right disabled send_owner_email_btn">Send Email</button> 


                    <div class="clearfix" style="margin-bottom: 15px;"></div>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> <?php echo $the_incident[0]['incident_no'].': '.$the_incident[0]['incident_name']; ?> <?php if ($the_incident[0]['initiation_type']==0){echo '<span class="label label-default">Mock</span> - '; } else{ echo '<span class="label label-info">Live</span>'; } ?> KPI Owners</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#kpipanel"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="kpipanel" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                







<div class="owners_table_container">

    
    <div class="table-responsive" style="margin-bottom: 30px;">
    
        <h4 class="text-left hidden" style="margin-bottom: 15px;">KPI Owners</h4>
        
        <table class="table table-hover table-bordered">
          <thead>
            <tr class="bg-info">
              <th><input type="checkbox" class="sel_all_kpio" id="sel_all_kpio" name="sel_all_kpio" data-toggle="tooltip" title="" data-original-title="Select All"></th>
              <th width="16%">Contact Name</th>
              <th width="17%">Type</th>
              <th width="16%">Company</th>
              <th width="17%">Email</th>
              <th width="17%">Last Sent</th>
              <th width="17%">Last Updated</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php 
                if(count($all_owners) > 0){
                    foreach($all_owners as $r=>$ownr){ ?>
                    
            <tr>
              <td><input type="checkbox" name="kpio[]" value="<?php echo $ownr['id'] ?>"></td>
              <td><?php echo $ownr['name'] ?></td>
              <td><?php
			  	if($ownr['kpi_type'] == 'dist'){
					echo 'Distributor';
				}
			  	else if($ownr['kpi_type'] == 'mkt'){
					echo 'Retailer';
				}
			  	else{
					echo 'Distributor/Retailer';
				}
				
				?>
              </td>
              <td><?php echo $ownr['company'] ?></td>
              <td><?php echo $ownr['email'] ?></td>
              <td class="last_sent"><?php
              	if( $ownr['date_sent'] != '0000-00-00 00:00:00'){
					echo date_format(date_create($ownr['date_sent']), 'M d, Y, g:i A');
				}
				else{
					echo 'Not yet sent';
				}
			  
			  ?></td>
              <td>
              <?php
				$dbcell = unserialize($ownr['dist_data']);
				if($dbcell != ''){ //get each kpi db cell data
					$total_unit = number_format($dbcell['total_unit']);
					$unit_return = number_format($dbcell['unit_return']);
					$u_disposed = number_format($dbcell['u_disposed']);
					$u_transformed = number_format($dbcell['u_transformed']);
					$u_corrections = number_format($dbcell['u_corrections']);
					$the_kpi = round($dbcell['the_kpi'], 0).'%';
					$owner_date_update = $dbcell['date_updated'];
				
					echo date_format(date_create($owner_date_update), 'M d, Y, g:i A'); //'<span class="text-primary" data-toggle="popover" data-placement="left" data-html="true" data-trigger="hover" title="'.$ownr['name'].'" data-content="'.'Total Unit: '.$total_unit.'<br>'.'Unit Return: '.$unit_return.'<br>'.'Unit Disposed: '.$u_disposed.'<br>'.'Unit Transformed: '.$u_transformed.'">'.date_format(date_create($owner_date_update), 'M d, Y, g:i A').'</span>';
				}else{
					
					echo 'Not yet updated';

				
				}
			  ?>
              </td>
              <td>
                <div class="dropdown pull-right">
                  <button data-toggle="dropdown" class="btn btn-default btn-xs">
                    Action
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#" data-target="#kpiOwnerModal" data-toggle="modal" onclick="$('#owner_name').val('<?php echo $ownr['name']; ?>');$('#owner_company').val('<?php echo $ownr['company']; ?>');$('#owner_email').val('<?php echo $ownr['email']; ?>');$('#owner_id').val('<?php echo $ownr['id']; ?>');$('[name=owner_type]').prop('checked',false); $('[name=owner_type][value=<?php echo $ownr['kpi_type']; ?>]').prop('checked',true);">Edit</a></li>
                    <li><a href="javascript: void(0);" class="delete_btn" data-id="<?php echo $ownr['id'] ?>" data-db="bulk_kpi_owners" data-deltype="contactonly">Delete Contact</a></li>
                    <li class="<?php if($ownr['kpi_type'] == 'all') { echo 'hidden'; } ?>"><a href="javascript: void(0);" class="delete_kpidata_btn" data-id="<?php echo $ownr['id'] ?>" data-deltype="datacontact" data-type="<?php echo $ownr['kpi_type'] ?>">Delete Contact + Data</a></li>
                  </ul>
                </div>          
              </td>
              
            </tr>
                    
                    <?php } //foreach?>
            
            
            <?php }else {?>
            <tr>
              <td colspan="8">No owners added.</td>
            </tr>
            <?php }?>
    
          </tbody>
        </table>
    
    </div><!--/dist_kpi_owners-->
                                        
</div><!--end owners_table_container-->








                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

					

        
        
        
        

<!-- Modal -->
<div class="modal fade" id="kpiOwnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        
        
        <form id="owner_form">
            <div class="form-group">
                <label>Contact Name</label>
                <input class="form-control" type="text" name="owner_name" id="owner_name" value=""/>
                <input class="form-control" type="hidden" name="owner_id" id="owner_id" value=""/>
                <input class="form-control" type="hidden" name="incident_id" id="incident_id" value="<?php echo $this->uri->segment(4); ?>"/>
                <span class="form-helper text-danger"></span>
            </div>
        
            <div class="form-group">
                <label>Owner Type</label>
                <?php /*?><select class="form-control" name="owner_type" id="owner_type">
                	<option value="dist">Distributor</option>
                	<option value="mkt">Retailer</option>
                	<option value="all">Both</option>
                </select><?php */?>
                

                <div class="radiox">
                  <label style="font-weight: normal;">
                    <input type="radio" name="owner_type" value="dist" checked>
                    Distributor
                  </label>
                </div>
                
                <div class="radiox">
                  <label style="font-weight: normal;">
                    <input type="radio" name="owner_type" value="mkt">
                    Retailer
                  </label>
                </div>

                <div class="radiox">
                  <label style="font-weight: normal;">
                    <input type="radio" name="owner_type" value="all">
                    Distributor/Retailer
                  </label>
                </div>


                <span class="form-helper text-danger"></span>
            </div>
        
            <div class="form-group">
                <label>Company</label>
                <input class="form-control" type="text" name="owner_company" id="owner_company" value=""/>
                <span class="form-helper text-danger"></span>
            </div>
        
            <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" name="owner_email" id="owner_email" value=""/>
                <span class="form-helper text-danger"></span>
            </div>
        
            <div class="form-group">
                <button class="btn btn-primary" type="submit" disabled="disabled">Submit</button>
            </div>
        
        
        </form>
      </div>
    </div>
  </div>
</div>


        
<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row ">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Recall KPIs
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Recall KPIs</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row ">

                    <div class="col-lg-12">

						<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                             <?php echo $this->session->flashdata('success');   ?>

                        </div>

                        <?php } if($this->session->flashdata('error')!=""){ ?>

                        <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
                        </div>

                        <?php } ?>

                        </div>

                    </div>
                    
                    
                    <button class="btn btn-primary pull-right hidden" data-toggle="modal" data-target="#newKPIModal" onclick="$('#assoc_incident').val('0');$('#kpis_form').find('input').val('');">Add</button>
                    <div class="clearfix" style="margin-bottom: 15px;"></div>

                        <div class="panel panel-default hidden">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Recall KPIs</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#MailStakeholder"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="MailStakeholder" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                
                                
                                


								<?php 

								if(count($kpis)>0)

								{ ?>

							<div class="table-responsivexx">

							<table class="table table-hover" id="example-table">

                                <thead class="hidden">

                                    <tr>

                                        <th class="hidden">sort</th>
                                        
                                        <th>Name</th>
                                        
                                    	<th class="hidden-xs hidden-sm">date</th>
                                        <th>action</th>



                                    </tr>

                                </thead>

								<tbody>

								<?php
									$kpisort = 1;
									foreach($kpis as $r => $value)

									{

								?>

                                                        

                                    <tr>
										<td class="hidden"><?php echo $kpisort; ?></td>
                                    	<td>
										
										<?php
										
											echo '<p><b>Units under Company control</b><br>';
											echo $value['company_control'].'</p>';
										
											echo '<p><b>Units in distribution</b><br>';
											echo $value['distribution'].'</p>';
										
											echo '<p><b>Units in consumer market</b><br>';
											echo $value['consumer_market'].'</p>';
										
											echo '<p><b>Related Incident</b><br>';
											echo $this->common_model->incidentname($value['incident_id']).'</p>';
										
										?>
                                        
                                        </td>

                                    	<td class="hidden-xs hidden-sm">
                                        <?php if($value['date_updated'] == '0000-00-00 00:00:00'){ ?>
                                        <span class="text-muted small" title="<?php echo $value['date_added'];?>"><?php echo 'added '. $this->common_model->ago($value['date_added']); ?></span></td>
                                        
										<?php } else{ ?>
                                        <span class="text-muted small" title="<?php echo $value['date_updated'];?>"><?php echo 'updated '. $this->common_model->ago($value['date_updated']); ?></span></td>
                                        
                                        <?php } ?>
                                        
                                    	<td>
<div class="dropdown pull-right">
  <button class="btn btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" style="font-size: 90%;">
    <li><a href="#" data-toggle="modal" data-target="#newKPIModal" onclick="$('#company_control').val('<?php echo $value['company_control']; ?>');$('#distribution').val('<?php echo $value['distribution']; ?>');$('#consumer_market').val('<?php echo $value['consumer_market']; ?>');$('#assoc_incident').val('<?php echo $value['incident_id']; ?>');$('#kpi_id').val('<?php echo $value['id']; ?>');">Update</a></li>
     
    <li><a href="javascript: void(0);" onclick="confirmDelKPI('<?php echo $value['id']?>','kpi');">Delete</a></li>
  </ul>
</div>                                        
                                        </td>




                                    </tr>


								<?php 
									$kpisort++;
									}
									
									
								?>

                                                    

                                                </tbody>


                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-calculator" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall KPIs</p>
								<?php }

								?>

                                



                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

					

        
        

<!-- Modal -->
<div class="modal fade" id="newKPIModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        
        
        <form id="kpis_form">
    
    
                <div class="form-group">
                	<label>Units under Company control</label>
                    <input class="form-control" type="text" name="company_control" id="company_control"/>
                    <input class="form-control kpi_id" type="hidden" name="kpi_id" id="kpi_id"/>
                    <span class="form-helper text-danger"></span>
                </div>
                
                <div class="form-group">
                	<label>Units in distribution</label>
                    <input class="form-control" type="text" name="distribution" id="distribution"/>
                    <span class="form-helper text-danger"></span>
                </div>

                <div class="form-group">
                	<label>Units in consumer market</label>
                    <input class="form-control" type="text" name="consumer_market" id="consumer_market"/>
                    <span class="form-helper text-danger"></span>
                </div>
                
                <div class="form-group">
                	<label>Select Related Incident</label>
                    <select class="form-control" name="assoc_incident" id="assoc_incident">
                    	<option value="0">Select</option>
                        <?php
							if(count($incidents) > 0){
							
								foreach($incidents as $r=>$inci){
								
									echo '<option value="'.$inci['id'].'">'.$inci['incident_no'].': '.$inci['incident_name'].'</option>';
								}
							}
						
						?>
                    </select>
                    <span class="form-helper text-danger"></span>
                </div>
                

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
    
                
        </form>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <a class="close" href="<?php echo base_url().'cc/dashboard'?>"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
	<?php
    if(count($incidents) > 0){ ?>
        <h4 id="myModalLabel">Select Incident</h4>
        <ul class="list-group list-default">
		<?php
            foreach($incidents as $open=>$opeen){
        ?>
        
        <a href="<?php echo base_url().'cc/kpi/manage/'.$opeen['id'] ?>" class="list-group-item"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
        
        <?php
            } 
        ?>
        </ul>
	<?php	
    }else{ ?>
        <br /><br />
        <p class="lead text-muted text-center">
        
        	No open incident

		</p>
        

    <?php } ?>

      </div>
      <div class="modal-footer">
            <a href="<?php echo base_url().'cc/dashboard'; ?>" class="btn btn-primary">OK</a>
            <a href="<?php echo base_url().'cc/recall/manage'; ?>" class="btn btn-danger"><i class="fa fa-history"></i> Raise Incident</a>

      </div>
    </div>
  </div>
</div>

<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Notification
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Notification</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">

                    <div class="col-lg-12">

						<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                             <?php echo $this->session->flashdata('success');   ?>

                        </div>

                        <?php } if($this->session->flashdata('error')!=""){ ?>

                        <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
                        </div>

                        <?php } ?>

                        </div>

                    </div>
                    
                    
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#newBulk" onclick="$('#bulk_name').val('Bulk Notification <?php if(count($logs) > 0) { echo $logs[0]['file_count'] + 1; } else { echo '1'; }?>'); $('.bulk_id').val('');$('#assoc_incident').val('0');">New Bulk Notification</button>
                    <div class="clearfix" style="margin-bottom: 15px;"></div>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Notification</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#MailStakeholder"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="MailStakeholder" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                
                                
                                


								<?php 

								if(count($logs)>0)

								{ ?>

							<div class="table-responsivexx">

							<table class="table table-hover" id="example-table">

                                <thead class="hidden">

                                    <tr>

                                        <th class="hidden">sort</th>
                                        
                                        <th>Name</th>

                                        <th class="hidden-xs hidden-sm">Recall</th>

                                        <th class="hidden-xs hidden-sm">date</th>
                                        <th>action</th>



                                    </tr>

                                </thead>

								<tbody>

								<?php
									$logsort = 1;
									foreach($logs as $r => $value)

									{

								?>

                                                        

                                    <tr>
										<td class="hidden"><?php echo $logsort; ?></td>
                                    	<td><?php echo $value['name']; //<i class="fa fa-file-excel-o fa-fw text-primary"></i> echo substr($value['file_name'], 13); ?></td>

                                    	<td class="hidden-xs hidden-sm"><?php echo $this->common_model->incidentname($value['incident_id']); ?></td>

                                    	<td class="hidden-xs hidden-sm"><span class="text-muted small" title="<?php echo $value['date_added'];?>"><?php echo 'added '. $this->common_model->ago($value['date_added']); ?></span></td>
                                        
                                    	<td>
<div class="dropdown pull-right">
  <button class="btn btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" style="font-size: 90%;">
    <li><a href="<?php echo base_url().'cc/communication/manage/'.$value['id']; ?>">Manage</a></li>
    <li><a href="#" data-toggle="modal" data-target="#newBulk" onclick="updateBulk('<?php echo $value
	['id']?>','<?php echo $value['incident_id']?>','<?php echo $value['name']?>');">Update</a></li>
    <li><a href="#" data-toggle="modal" data-target="#uploadBulk" onclick="$('.bulk_id').val('<?php echo $value
	['id']?>');">Upload File</a></li>
     
    <li><a href="javascript: void(0);" onclick="confirmDelBulk('<?php echo $value['id']?>');">Delete</a></li>
  </ul>
</div>                                        
                                        </td>




                                    </tr>


								<?php 
									$logsort++;
									}
									
									
								?>

                                                    

                                                </tbody>


                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-cloud-upload" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No Notification Files.</p>
								<?php }

								?>

                                



                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

					

        
        

<!-- Modal -->
<div class="modal fade" id="newBulk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        
        
        <form id="comm_name_form">
    
    
                <div class="form-group">
                    <input class="form-control" type="text" name="bulk_name" id="bulk_name" placeholder="Bulk Notification Name" value="" />
                    <input class="form-control bulk_id" type="hidden" name="bulk_id"/>
                    <span class="form-helper text-danger"></span>
                </div>
                
                <div class="form-group">

                    <select class="form-control" name="assoc_incident" id="assoc_incident">
                    	<option value="0">Select Related Incident</option>
                        <?php
							if(count($incidents) > 0){
							
								foreach($incidents as $r=>$inci){
								
									echo '<option value="'.$inci['id'].'">'.$inci['incident_no'].': '.$inci['incident_name'].'</option>';
								}
							}
						
						?>
                    </select>
                    <span class="form-helper text-danger"></span>
                </div>
                
                
                
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <a class="btn btn-link hidden" href="javascript: void(0);" onclick="$('#bulk_form').toggle();">Upload CSV File (optional)</a>
                </div>
    
                
        </form>
      </div>
    </div>
  </div>
</div>

        <?php /*?>

<!-- Modal -->
<div class="modal fade" id="renameBulk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        
        
        <form id="comm_name_form">
    
    
                <div class="form-group">
                    <input class="form-control" type="text" name="bulk_name" id="bulk_name" placeholder="Bulk Notification Name" />
                    <input class="form-control bulk_id" type="hidden" name="bulk_id" />
                </div>
                
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <a class="btn btn-link hidden" href="javascript: void(0);" onclick="$('#bulk_form').toggle();">Upload CSV File (optional)</a>
                </div>
    
                
        </form>
      </div>
    </div>
  </div>
</div><?php */?>


<!-- Modal -->
<div class="modal fade" id="uploadBulk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>

      
        <div class="hidden">
            <form id="myCsvForm" action="<?php echo base_url().'cc/communication/upload' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="99999" id="mycsv_input" name="mycsv_input" onchange="$('#myCsvForm').submit();">
                 <input type="hidden" class="bulk_id" name="bulk_id" />
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>                                                   
        </div>
        
        
        <form id="bulk_form">
    
                <div class="form-group hidden">
                    <input class="form-control" type="text" name="name" id="name" placeholder="Bul Notification Name" />
                </div>
                
                <div class="form-group text-center">
                    <a href="javascript:;" class="btn btn-primary btn-lg upload_file_btn" onclick="$('#mycsv_input').click();"><i class="fa fa-cloud-upload"></i> Select File to Upload (CSV)</a>
                </div>
                
                <div class="form-group" style="margin-top: 15px;">
                    <div class="text-danger bulk_error_mess text-center hidden"></div>
                </div>

                <div class="form-group">
                    <div id="progress" class="progress-csv" style="display: none;">
                        <div id="bar" class="bar-csv"></div>
                        <div id="percent" class="percent-csv">0%</div >
                    </div>
                </div>

                <div class="form-group">
                    <div id="file_up_text" style="display: none;"><div id="message"><a href=""></a></div></div>
                </div>

                <div class="form-group" style="border-top: 1px solid #eee; margin-top: 15px; padding-top: 15px;">
                	<p class="text-center">Example File (.csv file extension)</p>
                    <img src="<?php echo base_url().'assets/img/screenshot-communication.jpg' ?>" alt="" class="img-responsive thumbnail" />
                </div>

                <div class="form-group">
                	<a href="<?php echo base_url().'assets/img/communication-file-sample.csv' ?>" target="_blank">Download sample file</a> | 
                    <a href="<?php echo base_url().'cc/communication/countrycode' ?>" target="_blank">Country Code Reference</a>
                </div>

    	  </div>
          <div class="modal-footer hidden">
                <button type="button" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Communication
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Communication</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">

                    <div class="col-lg-12">

						<?php if($this->session->flashdata('success')!=""){ ?>

                        <div class="alert alert-success alert-dismissable">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                             <?php echo $this->session->flashdata('success');   ?>

                        </div>

                        <?php } else if($this->session->flashdata('error')!=""){ ?>

                        <div class="alert alert-danger alert-dismissable">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
                        </div>

                        <?php } else { ?>


                        <div class="clearfix hidden">
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <p><i class="fa fa-info-circle"></i> Select contacts to notify. You can 'Select All' if required.</p>
                            </div>
                        </div>

                        <?php } ?>

                        </div>

                    </div>
                    
                    <a href="<?php echo base_url().'cc/communication' ?>" class="btn btn-default pull-left"><i class="fa fa-angle-double-left"></i> Back</a>
                    <a href="#" class="btn btn-link pull-left disabled display_sms_total" style="opacity: 1;">SMS Credit: <?php echo '$'.round($org_info[0]['sms_credit'], 2); ?></a>
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#uploadBulk" style="margin-left: 5px; width: 144px;" onclick="$('.bulk_id').val('<?php echo $bulk[0]['id']; ?>');">Upload from CSV</button>
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#addContact" style="margin-left: 5px; width: 144px;" onclick="$('.bulk_id').val('<?php echo $bulk[0]['id']; ?>');">Add New Contact</button>
                    <button type="button" class="btn btn-primary pull-right disabled send_email_btn" style="margin-left: 5px; width: 144px;">Send Email</button>
                    <button type="button" class="btn btn-primary pull-right disabled send_sms_btn" style="width: 144px; margin-left: 5px;">Send Email &amp; SMS</button>
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#topUpModal" style="width: 144px; ">Top Up</button>
                  
                    
                    <div class="clearfix" style="margin-bottom: 15px;"></div>

    
                    <div class="clearfix top_up_message" style="margin-bottom: 15px; display: none;">
                        <a href="#" data-toggle="modal" data-target="#topUpModal" class="btn btn-link pull-left" style="opacity: 1;"><span class="pull-left text-muted"><i class="fa fa-warning"></i> Not enough SMS Credit. </span>Top up</a>
                    </div>



                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><?php echo $bulk[0]['name']; //<i class="fa fa-file-excel-o fa-fw text-primary"></i>  substr($bulk[0]['file_name'], 13); ?></h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#MailStakeholder"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="MailStakeholder" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                
                                
                                


								<?php 

								if(count($bulk_data)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover" id="example-tablexx">

                                <thead class="">

                                    <tr>

                                        <th class="hidden">sort</th>
                                        <th><input type="checkbox" id="sel_all_bulk" name="sel_all_bulk" data-toggle="tooltip" title="Select All"/></th>
                                        
                                        <th>Full Name</th>
                                        
                                        <th>
										<?php
										echo '<i class="fa fa-check-circle text-success" style="visibility: hidden"></i> '; ?>Email
                                        </th>
                                        
                                        <th>Country Code</th>
                                        
                                        
                                        <th>
										<?php
										echo '<i class="fa fa-check-circle text-success" style="visibility: hidden"></i> '; ?>Mobile
                                        </th>
                                        
                                        <th></th>
                                        <th></th>



                                    </tr>

                                </thead>

								<tbody>

								<?php
									$logsort = 1;
									foreach($bulk_data as $r => $value)

									{

								?>

                                                        

                                    <tr>
										<td class="hidden"><?php echo $logsort; ?></td>
										<td><input type="checkbox" name="bulk_notis[]" value="<?php echo $value['id']?>" <?php if($value['enabled'] != 0){ echo 'disabled="disabled"'; } ?> /></td>
                                        
                                    	<td><?php echo $value['name']?></td>
                                    	<td>
										<?php
										if($value['email_confirmed'] == '1'){
											echo '<i class="fa fa-check-circle text-success" data-toggle="tooltip" title="Email Confirmed"></i> ';
										}else{
											echo '<i class="fa fa-check-circle text-success" style="visibility: hidden"></i> ';
										}
										echo $value['emails']?>
                                        
                                        </td>
                                    	<td><?php echo $value['countrycode']?></td>
                                    	<td>
										<?php
										if($value['sms_sent'] == '1'){
											echo '<i class="fa fa-check-circle text-success" data-toggle="tooltip" title="SMS Sent"></i> ';
										}else{
											echo '<i class="fa fa-check-circle text-success" style="visibility: hidden"></i> ';
										}
										echo $value['mobile'];
										
										?></td>

                                        
                                    	<td>
<div class="dropdown pull-right">
  <button class="btn btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" style="font-size: 90%;">
    <li><a href="javascript: void(0);" onclick="confirmDelContact('<?php echo $value['id']?>','<?php echo $value['bulk_id']?>');">Delete</a></li>
  </ul>
</div>                        
                
                                        </td>

                                        <?php
											//stat settings
											if($value['enabled'] == 0){
												$comp_stat = 'enabled';
												$comp_class = 'btn-success';
												$comp_font = 'fa-check';
											}
											else{
												$comp_stat = 'disabled';
												$comp_class = 'btn-danger';
												$comp_font = 'fa-times';
											}
										?>


                                    	<td class="text-center">
											<a href="#"
                                            class="btn <?php echo $comp_class; ?> btn-xs enable_toggle_btn"
                                            title="<?php echo $comp_stat ?>" 
                                            data-toggle="tooltip" 
                                            data-db="bulk_data" 
                                            data-id="<?php echo $value['id'] ?>">
                                            
                                                <i class="fa <?php echo $comp_font; ?> fa-fw"></i>
                                                
                                            </a>
                                        </td>



                                    </tr>


								<?php 
									$logsort++;
									}
									
									
								?>

                                                    

                                                </tbody>


                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-phone" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No Contacts</p>
								<?php }

								?>

                                



                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

					

<!-- Modal -->
<div class="modal fade" id="bulkMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
      
        <div class="hidden">
            <form id="bulkattachedform" action="<?php echo base_url().'cc/communication/upload_attach' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="99999" id="bulka_input" name="bulka_input" onchange="$('#bulkattachedform').submit();">
                 <input type="hidden"id="bulk_id" name="bulk_id" value="<?php echo $bulk[0]['id']; ?>">
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>                                                   
        </div>
      
      
        <form id="bulkmessages_form">
             <input type="hidden" id="bulk_type" name="bulk_type" value="">
                
                
            <div role="tabpanel">
            
            
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                
                    <div class="form-group">
                        <label>Text Message for SMS</label>
                        <textarea class="form-control" type="text" name="smsmessage" id="smsmessage" placeholder=""><?php echo $bulk[0]['sms_message'] ?></textarea>
                        <span class="control-helper text-danger" style="display: none;">This field is required.</span>
                    </div>
                    
                    <div class="form-group">
                        <a class="btn btn-default" href="#profile" data-toggle="tab">Next <i class="fa fa-angle-double-right"></i></a>
                    </div>
                            
                
                
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
            
                    <div class="form-group">
                        <label>Email Message</label>
                        <textarea class="form-control" type="text" name="emailmessage" id="emailmessage" placeholder="" rows="7"><?php echo $bulk[0]['email_message'] ?></textarea>
                        <span class="control-helper text-danger" style="display: none;">This field is required.</span>
                    </div>
                    
                            
                    <div class="form-group">
                        <a href="javascript:;" class="btn btn-default upload_file_btn" onclick="$('#bulka_input').click();"><i class="fa fa-paperclip"></i> Attach File</a>
                    </div>
                    
                    <div class="form-group">
                    
                    	<?php
							$file_attached = $this->master_model->getRecords('bulk_attachments',array('bulk_id'=>$bulk[0]['id']));

						?>
                        <div class="bulk_error_mess <?php if(count($file_attached) == 0){ echo 'hidden'; } ?>" style="border-top: 1px solid #eee; margin-top: 15px; padding-top: 15px;">
                            File Attached: <div id="the_attaches"><a href=""></a></div>
                            <?php
								if(count($file_attached) > 0){
									
									foreach($file_attached as $r=>$file){
										
										echo ' <a href="'.base_url().'uploads/users-bulk/attachments/'.$file['file_name'].'" target="_blank" id = "existing_attchment'.$file['id'].'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.substr($file['file_name'], 13).'</a> <span class="delbulk_attach" data-filename="'.$file['file_name'].'" data-attid="'.$file['id'].'" data-unique="existing_attchment'.$file['id'].'">&times;</span>';
									
									}
								}
							?>
                            
                            
                        </div>
                    </div>
            
                    <div class="form-group">
                        <div id="progress" class="progress-csv" style="display: none;">
                            <div id="bar" class="bar-csv"></div>
                            <div id="percent" class="percent-csv">0%</div >
                        </div>
                    </div>
                            
                    
                </div>
              </div><!-- end of Tab-content-->
            
            </div><!--end of tabpanel--> 
            
          </div>    
        
          <div class="modal-footer">
            <a class="btn btn-default" href="#home" data-toggle="tab"><i class="fa fa-angle-double-left"></i> Back</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>

    
          </div>
        </form>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        
        
        
        <form id="new_contact_form">
    
    			<h4>New Contact</h4>
                <div class="form-group">
                    <input class="form-control" type="text" name="bulk_name" id="bulk_name" placeholder="Full Name" />
                    <input class="form-control bulk_id" type="hidden" name="bulk_id" />
                    <span class="form-helper text-danger hidden">This field is required.</span>
                </div>
    
                <div class="form-group">
                    <input class="form-control" type="text" name="contact_email" id="contact_email" placeholder="Email" />
                    <span class="form-helper text-danger hidden">This field is required.</span>
                </div>
    
                <div class="form-group">
                    <input class="form-control hidden" type="text" name="letter_codex" id="letter_codex" placeholder="Country Code" />
                    
                    <select class="form-control" name="letter_code" id="letter_code" placeholder="Country Code">
                        <option value="">Select Country</option>
                    	<?php
						foreach($countries as $r=>$country){
							echo '<option value="'.$country['iso2'].'" data-calling_code="'.$country['calling_code'].'">'.$country['short_name'].' ('.$country['iso2'].')</option>';
						}
						?>
                    </select>
                    <span class="form-helper text-danger hidden">This field is required.</span>
                </div>
                
                <div class="form-group">
                	<div class="row">
                    
                    	<div class="col-sm-3">
                            <input class="form-control" type="text" name="calling_code" id="calling_code" placeholder="Calling Code" disabled="disabled" />
                        </div>
                    
                    	<div class="col-sm-9">
                            <input class="form-control" type="text" name="contact_mobile" id="contact_mobile" placeholder="Mobile" />
                            <span class="form-helper text-danger hidden">This field is required.</span>
                        </div>
                    </div>
                </div>
                
                 
                
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    
                </div>
                
                <div class="form-group hidden">
                    <p><a href="<?php echo base_url().'cc/communication/countrycode' ?>" target="_blank">Country Code Reference</a></p>
                </div>
                
                
                
        </form>
        
        
        
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="uploadBulk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>

      
        <div class="hidden">
            <form id="myCsvForm" action="<?php echo base_url().'cc/communication/upload' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="99999" id="mycsv_input" name="mycsv_input" onchange="$('#myCsvForm').submit();">
                 <input type="hidden" class="bulk_id" name="bulk_id" />
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>                                                   
        </div>
        
        
        <form id="bulk_form">
    
                <div class="form-group hidden">
                    <input class="form-control" type="text" name="name" id="name" placeholder="Bul Notification Name" />
                </div>
                
                <div class="form-group text-center">
                    <a href="javascript:;" class="btn btn-primary btn-lg upload_file_btn" onclick="$('#mycsv_input').click();"><i class="fa fa-cloud-upload"></i> Select File to Upload (CSV)</a>
                </div>
                
                <div class="form-group" style="margin-top: 15px;">
                    <div class="text-danger bulk_error_mess text-center hidden"></div>
                </div>

                <div class="form-group">
                    <div id="progress" class="progress-csv" style="display: none;">
                        <div id="bar" class="bar-csv"></div>
                        <div id="percent" class="percent-csv">0%</div >
                    </div>
                </div>

                <div class="form-group">
                    <div id="file_up_text" style="display: none;"><div id="message"><a href=""></a></div></div>
                </div>

                <div class="form-group" style="border-top: 1px solid #eee; margin-top: 15px; padding-top: 15px;">
                	<p class="text-center">Example File (.csv file extension)</p>
                    <img src="<?php echo base_url().'assets/img/screenshot-communication.jpg' ?>" alt="" class="img-responsive thumbnail" />
                </div>

                <div class="form-group">
                	<a href="<?php echo base_url().'assets/img/communication-file-sample.csv' ?>" target="_blank">Download sample file</a> | 
                    <a href="<?php echo base_url().'cc/communication/countrycode' ?>" target="_blank">Country Code Reference</a>
                </div>
                

    	  </div>
          
          
          <div class="modal-footer hidden">
                <button type="button" class="btn btn-primary">Submit</button>
          </div>
        </form>
        


      </div>
    </div>
  </div>
</div>


<!-- Modal topUpModal -->
<div class="modal fade" id="topUpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Top Up (SMS Credit: $<?php echo round($org_info[0]['sms_credit'], 2)?>)</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-sm alert-info <?php if($nexmo_balance > 50) { echo 'hidden'; }?>"><i class="fa fa-info-circle"></i> Admin has currently a balance of <?php echo '$'.round($nexmo_balance, 2); ?>  on his account. You may purchase larger than this amount. Admin will allocating your credit later.</div>
        
        <form id="top_up_form">
            <div class="alert alert-danger hidden"><span class="payment-errors"></span></div>
        
        	<div class="form-group">
            	<label>Amount</label>
                <select class="form-control" name="amount">
                	<option value="5">$5.00</option>
                	<option value="10">$10.00</option>
                	<option value="20">$20.00</option>
                	<option value="50">$50.00</option>
                </select>
                <input type="hidden" name="nexmo_balance" value="<?php echo $nexmo_balance ?>" />
            </div>
            
                    
            <div class="row credit_fields <?php if($org_stripe > 0) { echo 'hidden'; } ?>">
                <div class="col-sm-12">
                    <div class="well">
                                
                        <div class="form-group">
                          <label><span>Card Number</span></label>
                          <input class="form-control" type="text" size="20" data-stripe="number"/>
                        
                        </div>
                        
                        
                        <div class="row">
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                  <label><span>Expiration (MM)</span></label>
                                  <input class="form-control" type="text" size="2" data-stripe="exp-month"/>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label><span>Expiration (YYYY)</span></label>
                                    <input class="form-control" type="text" size="4" data-stripe="exp-year"/>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                <label><span>CVC</span></label>
                                <input class="form-control" type="text" size="4" data-stripe="cvc"/>
                                </div>
                            </div>
            
                        </div>
                                            
                    
                    
                    </div>
                
                </div>
            </div>
        


            

            <div class="form-group">
                <button class="btn btn-primary">Purchase</button>
            </div>
        </form>
      </div>

    </div>
  </div>
</div>
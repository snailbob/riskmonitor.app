                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Set Review Date
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active">Reminders</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">
                    <!-- <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
               		</div> -->

                    <div class="col-lg-12" style="padding-top:10px;">

							<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                        </div>

                    </div>

                   <?php /*?> <div class="col-md-12 col-sm-12"><?php */?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Set Review Date</h4>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">

                                <form class="form-horizontal" role="form" action="" method='post' novalidate="novalidate">

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">Select</label>

                                       

                                        <div class="col-sm-10">

                                            <select class="form-control" id="reminder" name="reminder">

                                            	<option value="">Select One</option>

                                            <?php if(count($remind_result) > 0) { 

											foreach($remind_result as $rem)

											{

												$month=explode(" ",$rem['reminder_month']) 

											?>

                                            <option value="<?php echo $month[0]; ?>"

                                             <?php if(!empty($selectedremider[0]['reminder_month_id']) && $selectedremider[0]['reminder_month_id']==$month[0])

											 {echo 'selected="selected"';}

											 ?>><?php echo $rem['reminder_month']; ?></option>

                                            <?php } } ?>

                                            </select>

                                        </div>

                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Date</label>
                                        <div class="col-sm-10">

                                            <input type="text" name="review_date" class="review_date form-control" value="<?php echo (isset($selectedremider[0])) ? $selectedremider[0]['reminder_date'] : ''; ?>"/>
                                      </div>

                                    </div>



                                    <div class="form-group">

                                        <div class="col-sm-offset-2 col-sm-10">

                                            <button type="submit" class="btn btn-primary" name="btn_set_reminder" id="btn_set_reminder" >Save</button>
                                  </div>

                                    </div>

                                </form>                            



                            </div>

                        </div>



                  <?php /*?>  </div><?php */?>

                    
<?php /*?>
                    <div class="col-md-4  col-sm-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Send Update Request</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#MailStakeholder"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="MailStakeholder" class="panel-collapse collapse in">

                                <div class="panel-body">

									<a href="<?php echo base_url()?>cc/reminder/crt" type="button" class="btn btn-primary btn-lg btn-block">Response Team Members</a>

                                    <a href="<?php echo base_url()?>cc/reminder/stakeholder" type="button" class="btn btn-primary btn-lg btn-block">Stakeholders</a>

                                </div>

                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>                    

                    <?php */?>





                </div><!--.row -->

					




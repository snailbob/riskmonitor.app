<div class="modal-header">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

    <h4 class="modal-title">
    <span ng-if="headers">Add Task</span>
    <span ng-if="!headers">Update Task</span>
    </h4>
</div>
<div class="modal-body">
    <ng-form name="nameDialog" novalidate role="form">



        <div class="form-group" ng-if="headers">
        <label for="">Header</label>
        <select name="" id="" class="form-control" ng-options="option as option.name for option in headers track by option.id" ng-model="stepnumber.header">
        </select>
        </div>
        <div class="form-group">
            <label for="">Task</label>
            <input type="text" class="form-control" name="question" id="question" ng-model="data.question" required>
        </div>
    </ng-form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
    <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Save</button>
</div>

<div class="modal-header" style="height: 0px; padding: 0;" ng-click="closeAllPopUps()">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body modal-body-kbcontent" ng-click="closeAllPopUps()">

    <div class="checklist-contents" ng-if="!editMode">
        <div class="editor-holder-preview" ng-repeat="content in contents track by $index">

            <div class="list-margin-holderx" ng-class="{'hidden' : content.module == 'image'}" ng-if="content.module == 'text' || content.module == 'image'">
                <div ng-bind-html="htmlSafe(content.module)"></div>

            </div>


            <div ng-if="content.module == 'video'" class="contents-holder list-margin-holderx">
                <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
            </div>


            <div class="contents-holder list-margin-holderx" ng-if="content.module == 'image'">
                <div class="text-center" ng-hide="content.content">

                </div>
                <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
            </div>


            <div class="contents-holder list-margin-holderx" ng-if="content.module == 'select'">

                <div class="field-single">
                    <div class="options-view">
                        <div class="form-groupx">
                            <label for="">{{content.content}}</label>
                        </div>

                        <select name="" id="" class="form-control" ng-options="option as option.text for option in content.options track by option.id"
                            ng-model="option.answer">
                        </select>
                    </div>
                </div>
            </div>

            <div class="contents-holder list-margin-holderx" ng-if="content.module == 'list'">

                <div class="form-checkx form-check-customx" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">

                    <div class="checkboxx">
                        <label style="font-weight: normal">
                            <input type="checkbox" value="" ng-model="sublist.answer" ng-change="saveField(sublist)"> {{sublist.text}}
                        </label>
                    </div>
                </div>



                <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
                    <div class="form-groupx" ng-hide="field.type == 'hidden' || field.type == 'select'">
                        <label for="">{{field.text}}</label>
                    </div>


                    <input type="text" ng-if="field.type == 'single'" class="form-control" name="" id="" aria-describedby="helpId" placeholder=""
                        ng-model="field.answer">
                    <!-- ng-model-options="{debounce: 1000}" ng-change="saveChanges()" -->

                    <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2" placeholder="" placeholder="" ng-model="field.answer"></textarea>
                    <!-- ng-model-options="{debounce: 1000}" ng-change="saveChanges()" -->

                    <div ng-if="field.type == 'email'" class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                        <input type="email" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" placeholder=""
                            ng-model="field.answer">
                        <!-- ng-model-options="{debounce: 1000}" ng-change="saveChanges()" -->
                    </div>

                    <div ng-if="field.type == 'url'" class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                        </span>
                        <input type="url" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" ng-model="field.answer">
                        <!-- ng-model-options="{debounce: 1000}" ng-change="saveChanges()" -->
                    </div>

                    <div ng-if="field.type == 'hidden'" class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-eye-slash" aria-hidden="true"></i>
                        </span>
                        <label for="">{{field.text}}</label>
                    </div>


                    <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" onclick="$(document).find('#uploadFileInput').click()">
                        <i class="fa fa-upload" aria-hidden="true"></i> Add File
                    </button>


                    <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" popover-append-to-body="true" uib-popover-template="'datePicker.html'"
                        popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                        <i class="fa fa-calendar" aria-hidden="true"></i> Add Date
                    </button>

                </div>

            </div>

            <div class="emailer-holderx" ng-if="content.module == 'email'">
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">To</label>
                    <div class="col-sm-8">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Cc</label>
                    <div class="col-sm-8">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Bcc</label>
                    <div class="col-sm-8">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Subject</label>
                    <div class="col-sm-9">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Body</label>
                    <div class="col-sm-10">
                        <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                    <div class="col-sm-8">
                        <button class="btn btn-default">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i> Send
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <!-- editor-holder -->
    </div>

    <div class="edit-modex" ng-if="editMode">

        <div class="padding">
            <h2>
                <div class="input-group">
                    <span class="input-group-addon form-invisible" id="basic-addon1">
                        <i class="fa fa-minus-circle fa-fw text-red fa-2x" ng-if="activeTaskInfo.blocked == '1'" uib-tooltip="Task is blocked" aria-hidden="true"></i>
                    </span>
                    <textarea name="" id="" class="form-control form-invisible form-invisible-lg" ng-model="activeTaskInfo.question" ng-model-options="{ debounce: 1000 }"
                        ng-change="autoSavTask(activeTaskInfo, $index)" rows="1"></textarea>
                </div>

            </h2>
            <hr>
            <div class="content-actions">
                <!-- Simple dropdown -->
                <span uib-dropdown on-toggle="toggled(open)" class="pull-right">
                    <a class="btn btn-primary btn-sm" id="simple-dropdown" uib-dropdown-toggle ng-disabled="activeTaskInfo.blocked == '1'">
                        <span ng-if="!activeTaskInfo.checked && !activeTaskInfo.in_progress">To Do</span>
                        <span ng-if="!activeTaskInfo.checked && activeTaskInfo.in_progress">In Progress</span>
                        <span ng-if="activeTaskInfo.checked">Done</span>

                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
                        <li>
                            <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, false, '0')">To Do</a>
                        </li>
                        <li>
                            <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, false, '1')">In Progress</a>
                        </li>
                        <li>
                            <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, true, '0')">Done</a>
                        </li>
                    </ul>
                </span>


                <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'assignPopover.html'"
                    popover-placement="bottom" popover-is-open="assignPopSettings.isOpen">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span ng-if="activeTaskInfo.assigned_id == '0'">Assign</span>
                    <span ng-if="activeTaskInfo.assigned_id != '0'">Assigned - {{activeTaskInfo.assigned_info.full_name}}</span>
                </a>
                <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'datePickerPopover.html'"
                    popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                    <i class="fa fa-clock-o" aria-hidden="true"></i> Due after
                    <span ng-if="activeTaskInfo.months">{{activeTaskInfo.months}} months</span>
                    <span ng-if="activeTaskInfo.days">{{activeTaskInfo.days}} days</span>
                    <span ng-if="activeTaskInfo.hours">{{activeTaskInfo.hours}} hours</span>
                    <span ng-if="activeTaskInfo.mins">{{activeTaskInfo.mins}} mins</span>
                </a>

                <a class="btn btn-default btn-sm" ng-click="blockTask(activeTaskInfo, activeTaskIndex)">
                    <span ng-if="activeTaskInfo.blocked != '1'">Blocked</span>
                    <span ng-if="activeTaskInfo.blocked == '1'">Unblock</span>
                </a>
            </div>
        </div>

        <div class="checklist-content" ui-sortable="sortableContentOptions" ng-model="contents">
            <div class="editor-holderx col-sm-12" ng-repeat="content in contents track by $index" as-sortable-item>
            

                <div class="list-margin-holder" ng-class="{'hidden' : content.module == 'image'}" ng-if="content.module == 'text' || content.module == 'image'">

                    <summernote ng-model="content.content" config="optionsSummer"></summernote>

                    <!-- <ng-quill-editor ng-model="content.content" placeholder="" ng-click="focusQuill($index)" modules="getQuillModule(content.module)"></ng-quill-editor> -->

                </div>

            
            </div>
            <!-- editor-holder -->
        </div>

        <div class="text-center text-muted" style="padding: 50px 0;" ng-if="!contents.length && edit_mode">
            Drag and drop or click the buttons on the right to add content.
        </div>
        <p class="text-center text-muted" style="padding: 50px 0;" ng-if="!contents.length && !edit_mode">
            No content added.
        </p>
        <div class="padding">
            <table class="table table-hover" ng-if="activeTaskInfo.comments.length">
                <thead>
                    <tr>
                        <th>
                            {{activeTaskInfo.comments.length}} comment(s)
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <tr ng-repeat="comment in activeTaskInfo.comments track by $index">
                        <td>
                            <div class="media">
                                <div class="media-left">
                                    <a>
                                        <img class="media-object img-circle" ng-src="{{comment.author.avatar}}" alt="..." width="45">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <p>
                                        <b>{{comment.author.full_name}}</b>
                                        <br>
                                        <small>{{comment.created_at | date: 'mediumDate'}}</small>
                                    </p>
                                    <p>{{comment.message}}</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="media padding">
            <div class="media-left">
                <a>
                    <img class="media-object img-circle" ng-src="{{myInfo.avatar}}" alt="..." width="45">
                </a>
            </div>
            <div class="media-body">

                <div class="form-group">
                    <textarea class="form-control" ng-model="activeTaskInfo.theComment" placeholder="Write a comment" name="theComment" rows="3"></textarea>
                </div>
                <div class="form-group text-right">
                    <button type="button" class="btn btn-success" role="button" ng-class="{'disabled' : !activeTaskInfo.theComment || activeTaskInfo.commentSubmitting }"
                        ng-click="saveComment(activeTaskInfo.theComment)">
                        <span ng-if="!activeTaskInfo.commentSubmitting">Send</span>
                        <span ng-if="activeTaskInfo.commentSubmitting">
                            <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Sending..
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer" ng-click="closeAllPopUps()">
    <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
    <button type="button" class="btn btn-primary" ng-disabled="loading" ng-click="save()">Save</button>
</div>
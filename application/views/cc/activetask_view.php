


<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller hidden ng-cloak" ng-controller="headerTasksController" ng-class="{'preview_mode container-fluid' : !incident.id}" ng-click="closeAllPopUps()">
    <toast></toast>

    <div class="hidden">
      <input type="file" id="uploadFileInput">
    </div>

    <div class="kanban-view row col-headers" ng-if="!viewListMode">


      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h4 style="padding: 15px 0;" ng-if="!incident_id">Preview Mode</h4>
            <h4 style="padding: 15px 0;" ng-if="incident_id">

              <!-- Single button -->
              <div class="btn-group pull-right" uib-dropdown>
                <button id="single-button" type="button" class="btn btn-sm btn-success" uib-dropdown-toggle  ng-if="incident_id" ng-disabled="loading">
                  <span>
                    <span ng-if="incident.incident_status == '0'">
                      Open</span>
                    <span ng-if="incident.incident_status == '1'">
                      {{incident.incident_status_text}}</span>
                  </span>
                  <!-- <span ng-if="loading">
                      <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Saving changes..
                  </span> -->
                  
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                  <li role="menuitem">
                    <a ng-click="completeIncident()">
                      <span ng-if="incident.incident_status == '0'">
                        Close Incident</span>
                      <span ng-if="incident.incident_status == '1'">
                        Re-activate</span>
                    </a>
                  </li>
                </ul>
              </div>
              <span ng-if="incidents.length">
                <button class="btn btn-success btn-sm" ng-click="incidentsModal()">{{incident.incident_no}} <i class="fa fa-caret-down" aria-hidden="true"></i></button>
              </span>
              <span ng-if="!incidents.length">
                {{incident.incident_no}}
              </span>

               - {{incident.description}}
            </h4>

          </div>
          <div class="col-sm-4">
            <h5><strong>To Do</strong></h5>
            <hr>
          </div>
          <div class="col-sm-4">
            <h5><strong>In Progress</strong></h5>
            <hr>
          </div>
          <div class="col-sm-4">
            <h5><strong>Done</strong></h5>
            <hr>
          </div>
        </div>
      </div>

      <div class="scroll-content" style="padding-top: 15px;">


        <div class="container">
        

          <div class="row kanban-parent" vertilize-container ng-repeat="hTask in headerKanban track by $index" ng-init="headerIndex = $index" style="margin-bottom: 15px;">
            
            <div class="col-sm-12">
              <p class="kanban-header">
                <i class="fa fa-fw"  title="toggle tasks" ng-class="{'fa-caret-down' : !hTask.isCollapsed, 'fa-caret-up' : hTask.isCollapsed}" aria-hidden="true" ng-click="hTask.isCollapsed = !hTask.isCollapsed"></i> 
                <span>{{hTask.name}} </span>
                <a class="btn btn-xs btn-default" title="click to update" ng-click="updateHeaderKan($index, hTask)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                <!-- ng-click="deleteHeaderKan($index, hTask)" -->
              </p>
            </div>    
            <div class="col-sm-4" uib-collapse="hTask.isCollapsed" ng-repeat="disected_task in hTask.disected_tasks track by $index"ng-init="disectedIndex = $index" >
              <div class="panel-kanban-holder dndPlaceholder" vertilize ng-class="{'kanban-empty' : !disected_task.length}"
              dnd-list="disected_task"
              dnd-inserted="logEvent($index, event, item)"
              dnd-allowed-types="hTask.allowed_type"
              >

                <div class="panel panel-default panel-kanban" ng-repeat="task in disected_task track by $index"
                dnd-draggable="task"
                dnd-type="task.task_type"
                dnd-moved="taskMoved($index, $parent.$index, headerIndex)"
                dnd-effect-allowed="move"
                dnd-callback="task.assigned_info.full_name"
                >
                  <div class="panel-body">
                    <i class="fa fa-minus-circle fa-fw text-red fa-3x pull-right" ng-if="task.blocked == '1'" uib-tooltip="Task is blocked" aria-hidden="true"></i>
                  
                    <img src="{{task.assigned_info.avatar}}" class="pull-right img-circle" width="30" alt="" uib-tooltip="{{task.assigned_info.full_name}}" >
                    <p>
                      <!-- <a ng-click="clickTaskBanModal(task, $index, disectedIndex, headerIndex)">{{task.format_task_id}}</a> -->
                      <!-- <span ng-if="disectedIndex == 1 || (disectedIndex != 2 && task.months)"> &middot; </span> -->
                      <small class="text-muted">
                        <!-- <span ng-if="disectedIndex == 1" am-time-ago="task.task_answer.created_at | amParse:'YYYY.MM.DD HH:mm:ss'"></span> -->
                        <span ng-if="disectedIndex == 2" ng-bind-html="formatDoneMoment(task)"></span>
                        <span ng-if="disectedIndex == 1" ng-bind-html="formatInProgressMoment(task.task_answer.created_at)"></span>
                        <span ng-if="disectedIndex != 2 && task.months && disectedIndex == 1">&middot;</span>

                        <span ng-if="disectedIndex != 2 && task.months" ng-bind-html="getDueDiff(task, 'text')" ng-class="getDueDiff(task, 'class')"></span>
                      </small>
                      <br>
                      <a ng-click="clickTaskBanModal(task, $index, disectedIndex, headerIndex, incident_id)">{{task.question}}</a>
                      <!-- ng-click="editTaskModal($index, disectedIndex, headerIndex, task)"                       -->
                    </p>
                    <!-- <span am-time-ago="message.time"></span> -->

                  </div>
                </div>

              </div>

            </div>
          </div>


        </div>


        <div class="header-kanban-actions">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <div class="pull-right text-right">
                  <a class="btn btn-default btn-sm" target="_blank" href="<?php echo base_url().'pdfcreator/pdf/' ?>{{incident_id}}" ng-click="checkGenReport($event)">
                    <i class="fa fa-file-text-o"></i> Generate Report
                  </a>
                  <a name="" id="" class="btn btn-default btn-sm" role="button" ng-click="addHeaderKan()">
                    <i class="fa fa-plus" aria-hidden="true"></i> Heading
                  </a>
                  <a name="" id="" class="btn btn-default btn-sm" role="button" ng-click="addTaskBan()">
                    <i class="fa fa-plus" aria-hidden="true"></i> Task
                  </a>
                </div>
                <a class="btn btn-link btn-sm" ng-click="toggleListView()">
                  <i class="fa fa-exchange" aria-hidden="true"></i> List View
                </a>

              </div>
            </div>
          </div>

        </div>


      </div>

    </div>
    <div class="row" ng-if="viewListMode">


        <div class="col-lg-5 col-headers">

          <div class="scroll-content">
            <!-- <a class="btn btn-link" ng-click="toggleListView()">Switch to Kanban View</a> -->
            <h4 style="padding: 15px 0;" ng-if="!incident_id">Preview Mode</h4>
            <h4 style="padding: 15px 0;" ng-if="incident_id">

              <!-- Single button -->
              <div class="btn-group pull-right" uib-dropdown>
                <button id="single-button" type="button" class="btn btn-sm btn-success" uib-dropdown-toggle  ng-if="incident_id" ng-disabled="loading">
                  <span>
                    <span ng-if="incident.incident_status == '0'">
                      Open</span>
                    <span ng-if="incident.incident_status == '1'">
                      {{incident.incident_status_text}}</span>
                  </span>
                  <!-- <span ng-if="loading">
                      <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Saving changes..
                  </span> -->
                  
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                  <li role="menuitem">
                    <a ng-click="completeIncident()">
                      <span ng-if="incident.incident_status == '0'">
                        Close Incident</span>
                      <span ng-if="incident.incident_status == '1'">
                        Re-activate</span>
                    </a>
                  </li>
                </ul>
              </div>
              <span ng-if="incidents.length">
                <button class="btn btn-success btn-sm" ng-click="incidentsModal()">{{incident.incident_no}} <i class="fa fa-caret-down" aria-hidden="true"></i></button>
              </span>
              <span ng-if="!incidents.length">
                {{incident.incident_no}}
              </span>

               - {{incident.description}}


            </h4>
            
            <table class="table table-hover table-bordered table-rtask">
              <tbody id="steporder" ui-sortable="sortableTableOptions" ng-model="headerTasks">

                  <tr ng-class="{'info': activeHeader == $index }" ng-repeat="hTask in headerTasks track by $index" as-sortable-item>
                    <td class="num" ng-click="focusHeader($index)">
                      <div class="no-dragx">
                        {{$index + 1}}
                      </div>
                    </td>
                    <td class="text">
                      <div class="input-group">
                        <p class="form-control-static" ng-click="focusHeader($index)" ng-if="!edit_mode">
                          <strong>{{hTask.name}}</strong>
                        </p>
                        <button name="" id="" class="delete-header-btn btn-xs btn btn-danger" href="#" role="button" ng-click="deleteHeaderLive($index, hTask)" ng-if="edit_mode">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>

                        <input type="text" ng-click="focusHeader($index)" ng-if="edit_mode" ng-model-options="{ debounce: 1000 }" ng-change="autoSavHeader(hTask, $index)" class="form-control form-control-header" placeholder="" ng-model="hTask.name">
                        <span class="input-group-btn">
                          <button class="btn btn-link" ng-click=" hTask.show = !hTask.show">
                            <i class="fa" ng-class="{'fa-plus': !hTask.show ||  $index != activeHeader, 'fa-minus' :  $index == activeHeader && hTask.show, 'visibility-hidden' : $index != activeHeader }" aria-hidden="true"></i>
                          </button>
                        </span>
                      </div><!-- /input-group -->


                      <div  class="collapse" ng-class="{'in' : $index == activeHeader && hTask.show}" ng-attr-id="collapse{{$index}}">

                        <div class="list-group" style="margin-top: 15px; margin-right: 37px;">
                          <a href="#" class="list-group-item list-group-task" ng-class="{'active': activeTaskIndex == $index && $parent.$index == activeHeader}" ng-repeat="task in hTask.tasks track by $index" ng-click="clickTask(task.id, $index)">
                            
                            <button name="" id="" class="pull-right btn-xs btn btn-danger" href="#" role="button" ng-click="deleteTask($index, task)" ng-if="edit_mode">
                              <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>

                            <span class="fa-stack pull-left" ng-click="completeTask(task, $index)" ng-if="incident.incident_status == '0'">
                              <i class="fa fa-square-o fa-stack-2x" ng-class="{'text-orange': task.in_progress, 'text-green': task.checked}"></i>
                              <i class="fa fa-check fa-stack-1x" ng-if="task.checked && !task.loading" ng-class="{'text-green': task.checked}"></i>
                              <i class="fa fa-spinner fa-spin fa-stack-1x" ng-if="task.loading"></i>
                              <i class="fa fa-minus-circle fa-stack-1x text-red" ng-if="task.blocked == '1'"></i>
                            </span>
                            
                            <span class="fa-stack pull-left text-muted" ng-if="!incident.id || incident.incident_status != '0'">
                              <i class="fa fa-square-o fa-stack-2x" ng-class="{'text-orange': task.in_progress, 'text-green': task.checked}"></i>
                              <i class="fa fa-check fa-stack-1x" ng-if="(task.in_progress || task.checked) && !task.loading" ng-class="{'text-green': task.checked}"></i>
                              <i class="fa fa-spinner fa-spin fa-stack-1x" ng-if="task.loading"></i>
                              <i class="fa fa-minus-circle fa-stack-1x text-red" ng-if="task.blocked == '1'"></i>
                            </span>
                          
                            <div style="line-height: 26px; padding-left: 30px">
                              {{task.question}}
                            </div>
                              
                          </a>    

                        </div>

                      </div>
                    </td>
                  </tr>


              </tbody>
            </table>

            <div class="header-actions">


              <div class="pull-right text-right" ng-if="incident_id && incident_id != '0'">


                  <span ng-if="edit_mode">

                    <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" ng-click="toEdit(false)">
                      <i class="fa fa-times" aria-hidden="true"></i> Cancel
                    </a>
                    <a name="" id="" class="btn btn-default btn-sm" href="#" ng-click="previewIncident()" role="button">
                    <i class="fa fa-eye" aria-hidden="true"></i> Preview
                  </a>

                    <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" ng-click="addHeader()">
                      <i class="fa fa-plus" aria-hidden="true"></i> Heading
                    </a>
                    <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" ng-disabled="!headerTasks.length" ng-click="addTask()">
                      <i class="fa fa-plus" aria-hidden="true"></i> Task
                    </a>

                    <a name="" id="" class="btn btn-success btn-sm" href="#" ng-click="toEdit(false)" role="button" ng-class="{'disabled': loading}" ng-if="!uri_4">
                      <span ng-if="!loading">
                          <i class="fa fa-save" aria-hidden="true"></i> Review Done
                      </span>
                      <span ng-if="loading">
                          <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Saving changes..
                      </span>
                    </a>
                  </span>

                  <span ng-if="!edit_mode">
                    <a class="btn btn-default btn-sm" target="_blank" href="<?php echo base_url().'pdfcreator/pdf/' ?>{{incident_id}}" ng-click="checkGenReport($event)">
                      <i class="fa fa-file-text-o"></i> Generate Report
                    </a>

                    <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" ng-click="toEdit(true)">
                      <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                    </a>
                  </span>


              </div>

              <a name="" id="" class="btn btn-link btn-sm" href="#" role="button" ng-click="toggleListView()">
                <i class="fa fa-exchange" aria-hidden="true"></i> Kanban View
              </a>

            </div>
          </div>

        </div>
        <div class="col-lg-7 col-contents">

          <div ng-if="!edit_mode" class="scroll-content no-margin-right">
              <div class="padding">
                  <h2>
                      <i class="fa fa-minus-circle fa-fw text-red" ng-if="activeTaskInfo.blocked == '1'" uib-tooltip="Task is blocked" aria-hidden="true"></i> {{activeTaskInfo.question}}
                  </h2>
                  <hr>
                  <div class="content-actions">
                      <!-- Simple dropdown -->
                      <span uib-dropdown on-toggle="toggled(open)" class="pull-right">
                        <a class="btn btn-primary btn-sm" id="simple-dropdown" uib-dropdown-toggle ng-disabled="activeTaskInfo.blocked == '1'">
                        <span ng-if="!activeTaskInfo.checked && !activeTaskInfo.in_progress">To Do</span>
                        <span ng-if="!activeTaskInfo.checked && activeTaskInfo.in_progress">In Progress</span>
                        <span ng-if="activeTaskInfo.checked">Done</span>

                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
                          <li>
                            <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, false, '0')">To Do</a>
                          </li>
                          <li>
                            <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, false, '1')">In Progress</a>
                          </li>
                          <li>
                            <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, true, '0')">Done</a>
                          </li>
                        </ul>
                      </span>
                      <a class="btn btn-default btn-sm disabled pull-right" style="margin-right: 5px" ng-hide="!activeTaskInfo.checked && !activeTaskInfo.in_progress">
                        <span ng-if="activeTaskInfo.checked" ng-bind-html="formatDoneMoment(activeTaskInfo)"></span>
                        <span ng-if="activeTaskInfo.in_progress" ng-bind-html="formatInProgressMoment(activeTaskInfo.task_answer.created_at)"></span>
                        <span ng-if="!activeTaskInfo.checked && activeTaskInfo.months && activeTaskInfo.in_progress">&middot;</span>
                        <span ng-if="!activeTaskInfo.checked && activeTaskInfo.months" ng-bind-html="getDueDiff(activeTaskInfo, 'text')" ng-class="getDueDiff(activeTaskInfo, 'class')"></span>
                      </a>
                  
                      <a class="btn btn-default btn-sm" ng-click="blockTask(activeTaskInfo, activeTaskIndex)">
                        <span ng-if="activeTaskInfo.blocked != '1'">Blocked</span>
                        <span ng-if="activeTaskInfo.blocked == '1'">Unblock</span>
                      </a>
                  </div>

              </div>
              <div class="padding">
                
                <div class="checklist-content">
                    <p class="text-center text-muted lead" style="padding: 140px 0;" ng-if="!contents.length && edit_mode">
                     Drag and drop or click the buttons on the right to add content
                    </p>
                    <p class="text-center text-muted lead" style="padding: 140px 0;" ng-if="!contents.length && !edit_mode">
                     No content added.
                    </p>
                    <div class="editor-holder-preview" ng-repeat="content in contents track by $index">
                      <div class="list-margin-holderx" ng-class="{'hidden' : content.module == 'image'}"
                          ng-if="content.module == 'text' || content.module == 'image'">
                          <div ng-bind-html="htmlSafe(content.content)"></div>
                      </div>
                    </div>
                    <!-- editor-holder -->
                </div>
              
              </div>


              <table class="table table-hover" ng-if="activeTaskInfo.comments.length">
                <thead>
                  <tr>
                    <th>
                      {{activeTaskInfo.comments.length}} comment(s)
                    </th>
                  </tr>
                </thead>
                <tbody>

                  <tr ng-repeat="comment in activeTaskInfo.comments track by $index">
                    <td>
                        <div class="media">
                          <div class="media-left">
                            <a>
                              <img class="media-object img-circle" ng-src="{{comment.author.avatar}}" alt="..." width="45">
                            </a>
                          </div>
                          <div class="media-body">
                            <p>
                              <b>{{comment.author.full_name}}</b><br>
                              <small>{{comment.created_at | date: 'mediumDate'}}</small></p>
                            <p>{{comment.message}}</p>
                          </div>
                        </div>
                    </td>
                  </tr>
                </tbody>
              </table>

              <div class="media padding">
                <div class="media-left">
                  <a>
                    <img class="media-object img-circle" ng-src="{{myInfo.avatar}}" alt="..." width="45">
                  </a>
                </div>
                <div class="media-body">

                  <div class="form-group">
                    <textarea class="form-control" ng-model="activeTaskInfo.theComment" placeholder="Write a comment" name="theComment" rows="3"></textarea>
                  </div>
                  <div class="form-group text-right">
                    <button type="button" class="btn btn-success" role="button" ng-class="{'disabled' : !activeTaskInfo.theComment || activeTaskInfo.commentSubmitting }" ng-click="saveComment(activeTaskInfo.theComment)">
                      <span ng-if="!activeTaskInfo.commentSubmitting">Send</span>
                      <span ng-if="activeTaskInfo.commentSubmitting">
                        <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Sending..
                      </span>
                    </button>
                  </div>
                </div>
              </div>
          </div>
          <div class="scroll-content no-margin-right" ng-if="headerTasks.length && edit_mode">
            <div class="padding">
              <h2>
                <div class="input-group">
                  <span class="input-group-addon form-invisible" id="basic-addon1">
                    <i class="fa fa-minus-circle fa-fw text-red fa-2x" ng-if="activeTaskInfo.blocked == '1'" uib-tooltip="Task is blocked" aria-hidden="true"></i> 
                  </span>
                  <textarea name="" id="" class="form-control form-invisible form-invisible-lg" ng-model="activeTaskInfo.question" ng-model-options="{ debounce: 1000 }" ng-change="autoSavTask(activeTaskInfo, $index)" rows="1"></textarea>
                </div>


                
              </h2>
              <hr>
              <div class="content-actions">
                <!-- Simple dropdown -->
                <span uib-dropdown on-toggle="toggled(open)" class="pull-right">
                  <a class="btn btn-primary btn-sm" id="simple-dropdown" uib-dropdown-toggle  ng-disabled="activeTaskInfo.blocked == '1'">
                  <span ng-if="!activeTaskInfo.checked && !activeTaskInfo.in_progress">To Do</span>
                  <span ng-if="!activeTaskInfo.checked && activeTaskInfo.in_progress">In Progress</span>
                  <span ng-if="activeTaskInfo.checked">Done</span>

                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                  </a>
                  <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
                    <li>
                      <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, false, '0')">To Do</a>
                    </li>
                    <li>
                      <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, false, '1')">In Progress</a>
                    </li>
                    <li>
                      <a ng-click="changeTaskStatus(activeTaskInfo, activeTaskIndex, true, '0')">Done</a>
                    </li>
                  </ul>
                </span>

                <a class="btn btn-default btn-sm disabled pull-right" style="margin-right: 5px" ng-hide="!activeTaskInfo.checked && !activeTaskInfo.in_progress">
                  <span ng-if="activeTaskInfo.checked" ng-bind-html="formatDoneMoment(activeTaskInfo)"></span>
                  <span ng-if="activeTaskInfo.in_progress" ng-bind-html="formatInProgressMoment(activeTaskInfo.task_answer.created_at)"></span>
                  <span ng-if="!activeTaskInfo.checked && activeTaskInfo.months && activeTaskInfo.in_progress">&middot;</span>
                  <span ng-if="!activeTaskInfo.checked && activeTaskInfo.months" ng-bind-html="getDueDiff(activeTaskInfo, 'text')" ng-class="getDueDiff(activeTaskInfo, 'class')"></span>
                </a>




                <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'assignPopover.html'" popover-placement="bottom" popover-is-open="assignPopSettings.isOpen" >
                  <i class="fa fa-user" aria-hidden="true"></i> <span ng-if="activeTaskInfo.assigned_id == '0'">Assign</span>  <span ng-if="activeTaskInfo.assigned_id != '0'">Assigned - {{activeTaskInfo.assigned_info.full_name}}</span>
                </a>
                <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'datePickerPopover.html'" popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                  <i class="fa fa-clock-o" aria-hidden="true"></i> Due after <span ng-if="activeTaskInfo.months">{{activeTaskInfo.months}} months</span> <span ng-if="activeTaskInfo.days">{{activeTaskInfo.days}} days</span> <span ng-if="activeTaskInfo.hours">{{activeTaskInfo.hours}} hours</span> <span ng-if="activeTaskInfo.mins">{{activeTaskInfo.mins}} mins</span>
                </a>

                <a class="btn btn-default btn-sm" ng-click="blockTask(activeTaskInfo, activeTaskIndex)">
                  <span ng-if="activeTaskInfo.blocked != '1'">Blocked</span>
                  <span ng-if="activeTaskInfo.blocked == '1'">Unblock</span>
                </a>
              </div>
            </div>
            <p class="text-center text-muted lead" style="padding: 140px 0;" ng-if="!contents.length && edit_mode" drag-channel="A" drop-channel="A" ui-on-Drop="onDrop($event,$data,formElements)">
              Drag and drop or click the buttons on the right to add content.
            </p>
            <p class="text-center text-muted lead" style="padding: 140px 0;" ng-if="!contents.length && !edit_mode">
              No content added.
            </p>
            
            <!-- list view checklist  -->
            <div class="checklist-content" ui-sortable="sortableContentOptions" ng-model="contents" drag-channel="A" drop-channel="A" ui-on-Drop="onDrop($event,$data,formElements)">
              
              <div class="editor-holderx col-sm-12" tabindex="1" ng-keyup="keyUpContent(content)" ng-repeat="content in contents track by $index" as-sortable-item>
                <div class="list-margin-holder" ng-class="{'hidden' : content.module == 'image'}"
                    ng-if="content.module == 'text' || content.module == 'image'">

                  <summernote ng-model="content.content" on-blur="saveChanges()" config="optionsSummer"></summernote>

                  <!-- <ng-quill-editor ng-model="content.content" placeholder="" ng-click="focusQuill($index)" modules="getQuillModule(content.module)"></ng-quill-editor> -->
                </div>              
              </div>
              <!-- editor-holder -->
            </div>
          </div>





        </div>
    </div><!--.row -->


    <script type="text/ng-template" id="incidentsModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Open Incidents
            </h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-default" ng-repeat="incident in incidents track by $index">
            <div class="panel-body">
              <h4>
                <a ng-href="<?php echo base_url().'cc/standbytasks/activetask/'?>{{incident.id}}/{{incident.template_id}}" class="link-no-decor">
                  <span class="text-mutedx">{{incident.incident_no}}</span> <small><span class="label label-success">{{incident.incident_status_text}}</span> {{incident.description}}</small>
                </a>
              </h4>
            </div>
          </div>
        </div>

    </script>
    
    <script type="text/ng-template" id="taskKanbanModal.html">
      <?php
        $this->load->view('cc/activetask_kanban_modal');
      ?>
    </script>
    

    <script type="text/ng-template" id="editTaskModal.html">
      <?php
        $this->load->view('cc/activetask_edit_task');
      ?>                   
    </script>



    <script type="text/ng-template" id="editHeaderModal.html">
      <div class="modal-header">
          <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

          <h4 class="modal-title">
              Update Header
          </h4>
      </div>
      <div class="modal-body">
          <ng-form name="nameDialog" novalidate role="form">
              <div class="form-group">
                  <input type="text" class="form-control" name="name" id="name" ng-model="data.name" required>
              </div>
          </ng-form>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" ng-click="delete()">
            <i class="fa fa-trash" aria-hidden="true"></i>
          </button>

          <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
          <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Save</button>
      </div>
    </script>


    <script type="text/ng-template" id="datePicker.html">
      <div style="max-width:315px;">
        <div class="row gutter-md">
          
          <div class="col-xs-12">
            <div class="form-group">
              <div uib-datepicker ng-model="activeTaskInfo.due_date" datepicker-options="optionsPicker"></div> 
            </div>
          </div>

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-success btn-block" role="button">Save</a>
          </div>

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-danger btn-block" role="button">Remove</a>
          </div>

        </div>
      </div>

    </script>

    <script type="text/ng-template" id="datePickerPopover.html">
      <div style="max-width:315px;">
        <div class="row gutter-md">
          <div class="col-xs-12">
            <p class="text-center">Due after</p>
            <hr>
            <p>
              <small>This task will be due after:</small>
            </p>
          </div>
          
          <div class="col-xs-6">
            <div class="form-group">

              <div class="input-group input-group-trans">
                <input ng-keyup="saveDate()" type="text" class="form-control" ng-model="activeTaskInfo.months" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">months</span>
              </div>

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <div class="input-group input-group-trans">
                <input ng-keyup="saveDate()" type="text" class="form-control" ng-model="activeTaskInfo.days" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">days</span>
              </div>
            </div>
          </div>
          
          <div class="col-xs-6">
            <div class="form-group">

              <div class="input-group input-group-trans">
                <input ng-keyup="saveDate()" type="text" class="form-control" ng-model="activeTaskInfo.hours" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">hours</span>
              </div>

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <div class="input-group input-group-trans">
                <input ng-keyup="saveDate()" type="text" class="form-control" ng-model="activeTaskInfo.mins" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">mins</span>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <p>
              <small>After the checklist start date.</small>
            </p>
          </div>
          <!-- <div class="col-xs-6">
            <a name="" id="" class="btn btn-success btn-block" ng-click="saveDate()" role="button">Save</a>
          </div>

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-danger btn-block" ng-click="removeDate()" role="button">Remove</a>
          </div> -->

        </div>
      </div>

    </script>

    <script type="text/ng-template" id="customTemplate.html">
      <a>
          <!-- <img ng-src="http://upload.wikimedia.org/wikipedia/commons/thumb/{{match.model.flag}}" width="16"> -->
          <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
      </a>
    </script>

    <script type="text/ng-template" id="assignPopover.html">
      <div style="width:240px;">
        <div class="row">
          <div class="col-sm-12">
            <input type="text" ng-model="activeTaskInfo.assigned" placeholder="" uib-typeahead="crt as crt.full_name for crt in crts | filter:{full_name:$viewValue}" typeahead-template-url="customTemplate.html" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="saveAssigned()">
            <small>Search for a person in your organization by name.</small>

          </div>

        </div>
      </div>
    </script>

    <script type="text/ng-template" id="confirmPopover.html">
      <div style="width:140px;">
        <div class="row">
          <div class="col-sm-6">
            <a name="" id="" class="btn btn-block btn-danger" href="#" role="button">
              Delete
            </a>
          </div>
          <div class="col-sm-6">
            <a name="" id="" class="btn btn-block btn-default" href="#" role="button">
              Cancel
            </a>
          </div>

        </div>
      </div>
    </script>

    <script type="text/ng-template" id="mediaPopover.html">
      <div style="width: 200px;">
          <button class="btn btn-default btn-block" type="button" ng-click="addImage()"> <i class="fa fa-picture-o"></i> Image </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addVideo()"> <i class="fa fa-video-camera"></i> Video </button>
      </div>

    </script>
    <script type="text/ng-template" id="formPopover.html">

      <div style="width: 200px;">
          <button class="btn btn-default btn-block" type="button" ng-click="addSingleLine()"> <i class="fa fa-edit fa-fw"></i> Single Line Text </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addParagraph()"> <i class="fa fa-paragraph fa-fw"></i> Paragraph Text </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addEmailForm()"> <i class="fa fa-envelope-o fa-fw"></i> Email Address </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addSiteForm()"> <i class="fa fa-globe fa-fw"></i> Web Address </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addFileForm()"> <i class="fa fa-file-o fa-fw"></i> File Upload </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addDateForm()"> <i class="fa fa-calendar fa-fw"></i> Date </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addDropdownForm()"> <i class="fa fa-arrow-circle-down fa-fw"></i> Dropdown </button>
      </div>
    </script>
  </div>
</div>
  
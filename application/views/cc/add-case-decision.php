                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

        <h1>Case Decision</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Case Decision</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">


                    <div class="col-lg-12" style="padding-top:10px;">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-life-ring"></i> Case Decision </h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-body">

					
                                <form class="form-horizontal" method="post" action="">
                                <fieldset>

                                <!-- Select Basic -->
                                <div class="form-group">
                                  <label class="col-md-3 control-label" for="selectdecision">Decision: </label>
                                  <div class="col-md-9">
                                    <select id="selectdecision" name="selectdecision" class="form-control">
                                      <option value=""<?php if ($casedocs[0]['cc_decision'] == '') {echo 'selected="selected"';} ?>>Select</option>
                                      <option value="0" <?php if ($casedocs[0]['cc_decision'] == '0') {echo 'selected="selected"';} ?>>Close</option>
                                      <option value="1" <?php if ($casedocs[0]['cc_decision'] == '1') {echo 'selected="selected"';} ?>>Remediate</option>
                                     <!-- <option value="2" <?php if ($casedocs[0]['cc_decision'] == ' 2') {echo 'selected="selected"';} ?>>Escalate</option>-->
                                    </select>
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-3 control-label" for="decision_note">Decision Notes: </label>  
                                  <div class="col-md-9">
                                  <textarea rows="4" id="decision_note" name="decision_note" placeholder="decision note" class="form-control" required=""><?php echo $casedocs[0]['cc_note']?></textarea>
                                    
                                  </div>
                                </div>
        
                                <div class="form-group">
                                  <label class="col-md-3 control-label" for=""></label>  
                                  <div class="col-md-9">
                                <button type="submit" class="btn btn-primary" id="cc_decision" name="cc_decision">Submit</button>
                                    
                                  </div>
                                </div>
        
                                
                                </fieldset>
                                </form>
    



                            </div>

                        </div>

                        <!-- /.panel -->
                        


                    </div>

                </div><!--.row -->

					




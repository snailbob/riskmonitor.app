                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

        <h1>Share Case</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Share Case</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">


                    <div class="col-lg-12" style="padding-top:10px;">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); 
						?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-life-ring"></i> Share Case</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-body">

                                <form class="form-horizontal" method="post" action="">
                                <fieldset>
                                    <!-- Multiple Radios (inline) -->
                                    <div class="form-group">
                                      <label class="col-md-2 control-label" for="radios">Share with: </label>
                                      <div class="col-md-10"> 
                                        <label class="radio-inline" for="c_indi">
                                          <input type="radio" name="radios" id="c_indi" value="1">
                                          Individual
                                        </label> 
                                        <label class="radio-inline" for="c_group">
                                          <input type="radio" name="radios" id="c_group" value="2">
                                          Group
                                        </label> 
    
                                      </div>
                                    </div>
                                </fieldset>
                                </form>


                                <form class="form-horizontal share_indi" style="display:none;" method="post" action="">
                                <fieldset>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                      <label class="col-md-2 control-label" for="selectcrt"> </label>
                                      <div class="col-md-10">
                                        <select id="selectcrt" name="selectcrt" class="form-control">
                                            <option value="">Select</option>
                                            <?php if (count($crts)>0){
                                            
                                                foreach($crts as $r=>$value){
                                                
                                                    echo '<option value="'.$value['login_id'].'">'.$this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']).'</option>';
                                                }
                                            
                                            }																		
                                            ?>
                                        </select><span class="selectcrt_error text-red"><?php echo form_error('selectcrt'); ?></span>
    
                                      </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                      <label class="col-md-2 control-label" for=""></label>  
                                      <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary" id="cc_share" name="cc_share">Socialize</button>
                                        
                                      </div>
                                    </div>
        
                                
                                </fieldset>
                                </form>
    
                                
                                <form class="form-horizontal share_group" style="display:none;" method="post" action="">
                                <fieldset>

                                
                                    <!-- Select Basic group -->
                                    <div class="form-group">
                                      <label class="col-md-2 control-label" for="selectgroup"> </label>
                                      <div class="col-md-10">
                                        <select id="selectgroup" name="selectgroup" class="form-control">
                                            <option value="">Select</option>
                                            <?php if (count($usergroup)>0){
                                            
                                                foreach($usergroup as $r=>$value){
                                                
                                                    echo '<option value="'.$value['id'].'">'.$value['group_name'].'</option>';
                                                }
                                            
                                            }																		
                                            ?>
                                        </select><span class="selectgroup_error text-red"><?php echo form_error('selectcrt'); ?></span>
    
                                      </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                      <label class="col-md-2 control-label" for=""></label>  
                                      <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary" id="group_share" name="group_share">Socialize</button>
                                        
                                      </div>
                                    </div>
        
                                
                                </fieldset>
                                </form>
    



                            </div>

                        </div>

                        <!-- /.panel -->
                        

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Shared with</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-body">
                            	
                                <strong style="margin-left: 10px;">Individuals</strong>
								<ol>
								
								<?php
								
								foreach($caseinfo as $r=>$value){
									if($value['share_to'] !=''){
										$checkcrts = explode(' ',$value['share_to']);
											foreach($checkcrts as $c){
												$c = trim($c);
												$thecrts = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$c));
											
												foreach($thecrts as $cc => $ccvalue){
												echo '<li>'.$this->master_model->decryptIt($ccvalue['crt_first_name']).' '.$this->master_model->decryptIt($ccvalue['crt_last_name']).'</li>';
											}
												
										}
									}
									else
									{
										echo '<p class="text-muted" style="margin-left: -30px;"">Not shared to anyone.</p>';
									}
								}

                                ?>																	

    							</ol>

                                <strong style="margin-left: 10px;">Groups</strong>
								<ol>
								
								<?php
								
								foreach($caseinfo as $r=>$value){
									if($value['share_to_group'] !=''){
										$checkcrts = explode(' ',$value['share_to_group']);
											foreach($checkcrts as $c){
												$c = trim($c);
												$thegroups = $this->master_model->getRecords('user_group',array('id'=>$c));
											
												foreach($thegroups as $cc => $ccvalue){
													echo '<li>'.$ccvalue['group_name'].'</li>';
												}
													
											}
									}
									else
									{
										echo '<p class="text-muted" style="margin-left: -30px;">Not shared to any group.</p>';
									}
								}

                                ?>																	

    							</ol>
                                
                                

                            </div>

                        </div>

                        <!-- /.panel -->
                        


                    </div>

                </div><!--.row -->

					




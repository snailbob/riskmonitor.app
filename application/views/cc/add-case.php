<style>
	.popover {
		z-index: 9999;
		position: fixed;
	}
	
	.fc-agendaDay-button, .fc-month-button{
		visibility: hidden;
		display: none;
	}

</style>

                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Add Case

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Add Case</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



				 <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Add New Case</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-scenario" id="frm-add-scenario" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">
                            <label for="cida" class="col-sm-2 control-label">Case ID </label>
                            <div class="col-sm-10">
                                <input type="hidden" id="cc_case_id" name="cc_case_id" value="<?php echo $cc_case_id; ?>"/>
                                <input type="text" class="form-control" id="gen_id" name="gen_id"
                                
                                	value="<?php
										$num_padded = sprintf("CID%05s", $cc_case_id);
										echo $num_padded;
										
										 ?>" readonly="readonly" /><?php echo form_error('scenario_desc'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="case_title" class="col-sm-2 control-label">Case Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="case_title" name="case_title" placeholder="Enter case title" value="<?php echo set_value('case_title'); ?>"/><?php echo form_error('case_title'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="case_det" class="col-sm-2 control-label">Case Details</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="case_det" rows="6" name="case_det" placeholder="Enter case details"><?php echo set_value('case_det'); ?></textarea><?php echo form_error('case_det'); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="crt_mem" class="col-sm-2 control-label">Assign To CRT:</label>
                            <div class="col-sm-10">

                                
                                <div class="btn-group">
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default active" id="show_all">Show all</button>
                                  </div>
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default" id="filter_crt">Filter</button>
                                  </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        
                        
                      <div class="show_all_form" style="display: block;">
                        <div class="form-group">
                            <label for="crt_mem" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <select class="form-control" id="crt_mem" name="crt_mem" rows="6">
                                	<option value="">Select</option>
                                    
									<?php
                                    foreach($crt_list as $crts){ 
									
										if($crts['user_status'] == '1') {
									?>
                                   
                                    <option value="<?php echo $crts['login_id']; ?>"><?php echo $crts['full_name']; ?></option>
                                    
                                    <?php }
									}
                                    
                                    ?>
                                
                                </select><?php echo form_error('crt_mem'); ?>
                            </div>
                        </div>
                      </div>

                       
                        
                        
                      <div class="filter_crt_form" style="display: none">
                      
                      
                        <div class="form-group">
                            <div class="col-sm-2 col-xs-12"></div>

                            <div class="col-sm-5 col-xs-12" style="margin-bottom: 10px;">
                                <input type="text" class="form-control noEnterSubmit searchcaseloc" id="location" name="location" placeholder="Location" style="margin-bottom: 15px;" />
                                
                                <div class="search_latit">
                                    <div class="well well-sm" style="display: none;">
                                    
                                    </div>
                                
                                </div>
                                
                                <input name="city_lat" id="city_lat" type="hidden" value="">
                                <input name="city_lng" id="city_lng" type="hidden" value="">

                                <input type="text" class="hidden form-control noEnterSubmit" id="availability" name="availability" placeholder="Availability" style="margin-bottom: 15px;"/>

                                <input type="text" class="hidden form-control noEnterSubmit" id="experience" name="experience" placeholder="Experience" style="margin-bottom: 15px;"/>
                                
                                <a href= "javascript:;" class="btn btn-primary pull-right" name="filter_btn" id="filter_btn"><i class="fa fa-search"></i> Search</a>
                            </div>

                            <div class="col-sm-5 col-xs-12">
                            <div class="col-xs-12 filter_crt_result">
                            	<p class="text-muted text-center" style="margin-top: 50px;">Results are displayed here.</p>
                            </div>
                            </div>

                        </div>
                        
                        
                      </div>




                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>cc/managecase">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_case" id="add_case">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->



<!-- Modal -->
<div class="modal fade" id="calendarViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
    
    
            <div class="alert alert-info no-sched-alert fade in hidden">
                <button type="button" class="close hidden" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <i class="fa fa-info-circle"></i> <span class="calendar_crt_name"></span> have not added a schedule.
            </div>
    
            <div class="the_cal">
    
                <div id="script-warning">
                    <code>php/get-events.php</code> must be running.
                </div>
            
                <div class="" id="loading">loading...</div>
            
                <div id="cc_calendar"></div>
            
            </div>
    

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

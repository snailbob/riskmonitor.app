                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->



<!-- Modal -->
<div class="modal fade" id="importCRT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Import</h4>
      </div>
      <div class="modal-body">
        <p class="text-center text-muted">Loading..</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="save_import">Save changes</button>
      </div>
    </div>
  </div>
</div>

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                           <h1><?php echo 'Add Response Team Member'; ?>



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>cc/">Dashboard</a></li>

                                <li class="active">Add Response Team Member</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">



                        <?php


						if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $this->session->flashdata('error');   ?></div>

                            <?php }
								if($this->session->flashdata('success')=="" && $this->session->flashdata('error')==""){
							 ?>


								<?php
								if($addmore > 0){
									if($mod_max != '1000')
{								?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <p><i class="fa fa-info-circle"></i> You can add <?php echo $addmore ?> new team members.</p>
                                    </div>

								<?php }
									else{ ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <p><i class="fa fa-info-circle"></i> You are adding new team member for $<?php echo $cc_info[0]['single_price'] ?>/month which will be billed for the next monthly billing cycle.</p>
                                    </div>

								<?php

									}
								}


								 else{ ?>

                                    <div class="alert alert-warning alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <p><i class="fa fa-info-circle"></i> You have reached your subscription limit.</p>
                                    </div>

							 <?php } } ?>


                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Add Response Team Member</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">


                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">

						<form id="frm-add-cc" method='post' class="form-horizontal">



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cct_firstname" name="cct_firstname" value="<?php echo set_value('cct_firstname'); ?>"><?php echo form_error('cct_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cct_lastname" name="cct_lastname" placeholder="" required data-msg-required="Please enter last name"><?php echo form_error('cct_lastname'); ?>

                            </div>

                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label">Position</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="crt_position" name="crt_position" placeholder="" /><?php echo form_error('crt_position'); ?>

                            </div>

                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label">Location</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control geocomplete" id="crt_location" name="crt_location" placeholder="Enter location"/><?php echo form_error('crt_location'); ?>
                                <input name="country_short" id="country_short" type="hidden" value="">
                                <input name="lat" id="lat" type="hidden" value="">
                                <input name="lng" id="lng" type="hidden" value="">
                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Email Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="crt_email" name="crt_email" placeholder="Enter business email address" /><?php echo form_error('crt_email'); ?>

                            </div>

                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <input type="hidden" class="form-control" id="countrycode" name="countrycode" placeholder="Country Code" value=""/><?php echo form_error('countrycode'); ?>
                                <input type="hidden" class="form-control" id="addmore" name="addmore" value="<?php echo $addmore; ?>"/>

                                <select class="form-control" id="countrycodedd" name="countrycodedd">
                                    <option value="" data-code="">Select country code..</option>
                                <?php if(count($countriescode)!="0"){

                                    foreach($countriescode as $countries){

                                        echo '<option value="'.$countries['country_id'].'" data-code="'.$countries['calling_code'].'">'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';

                                    }
                                }

                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="crt_no" name="crt_no"><?php echo form_error('crt_no'); ?>

                            </div>

                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">User Group</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="group" name="group">
                                    <option value="" data-access="[]">Select..</option>
									<?php
                                    if(count($all_group)!="0"){
                                        foreach($all_group as $groups){
                                            echo '<option value="'.$groups['id'].'" ';

											$access = ($groups['access'] != '') ? unserialize($groups['access']) : array();
											echo 'data-access=\''.json_encode($access).'\'';


											echo '>'.$groups['group_name'].'</option>';
                                        }
                                    }
                                    ?>
                                </select><?php echo form_error('group'); ?>

                            </div>


                        </div>



						<?php
						$active_mod = substr($this->session->userdata('org_module'), 0, 1);
						 ?>
                        <div class="form-group <?php if($active_mod != '5' && $active_mod != '8') { echo 'hidden'; } ?>">
                            <label class="col-sm-2 control-label">Access</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">

                                  <div class="checkbox">
                                    <input type="checkbox" name="access[]" value="kpi" id="checkbox1">
                                    <label for="checkbox1">
                                        KPI Dashboard
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <input type="checkbox" name="access[]" value="cost" id="checkbox2">
                                    <label for="checkbox2">
                                        Cost Monitor
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <input type="checkbox" name="access[]" value="issues" id="checkbox3">
                                    <label for="checkbox3">
                                        Issues Board
                                    </label>
                                  </div>
                            </div>

                        </div>





                        <div class="form-group">

                            <label class="col-sm-2 control-label">Crisis Function</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="crt_func" name="crt_func" placeholder="Enter member’s function during a crisis" required><?php echo form_error('crt_func'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Alternate Member</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="alter_member" name="alter_member" placeholder="Enter name, title and contact details (mobile and email)." required><?php echo form_error('alter_member'); ?>

                            </div>

                        </div>




                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                                <a class="btn btn-default" href="<?php echo base_url()?>cc/crisisteam/managecrt">Back</a>
                                <button class="btn btn-primary" name="btn_add_crt_member" id="btn_add_crt_member">Submit</button>

                            </div>

                        </div>



                    </form>

                    </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div><!--.row-->

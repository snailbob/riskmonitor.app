                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                           <h1><?php echo 'Add Member'; ?>



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>cc/">Dashboard</a></li>

                                <li class="active">Add Member</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Add Member</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									<div class="row">
									  <div class="col-md-6 col-md-offset-3">


						<form action='' name="frm-add-cc" id="" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cct_firstname" name="cct_firstname"><?php echo form_error('cct_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cct_lastname" name="cct_lastname"><?php echo form_error('cct_lastname'); ?>

                            </div>

                        </div>




                        <div class="form-group">

                            <label class="col-sm-2 control-label">Email Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="crt_email" name="crt_email"><?php echo form_error('crt_email'); ?>

                            </div>

                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                                <a class="btn btn-default" href="<?php echo base_url()?>cc/members/manage">Back</a>
                                <button type="submit" class="btn btn-primary" name="btn_add_member" id="btn_add_member">Submit</button>

                            </div>

                        </div>





                    </form>
                  </div>
                </div>

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->

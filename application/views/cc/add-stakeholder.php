                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

    <h1>Add Stakeholder</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

        <li class="active">Add Stakeholder</li>

      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 -->

</div>

<!-- /.row -->



                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Add Stakeholder</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">


						<form action='' name="frm-add-stk" id="frm-add-stk" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="stk_firstname" name="stk_firstname" placeholder="" required data-msg-required="Please enter first name"><?php echo form_error('stk_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="stk_lastname" name="stk_lastname" placeholder="" required data-msg-required="Please enter last name"><?php echo form_error('stk_lastname'); ?>

                            </div>

                        </div>



                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Organization</label><!--last: position-->

                                            <div class="col-sm-10">

                                                <input type="text" class="form-control" id="stk_position" name="stk_position" placeholder="" required data-msg-required="Please enter phone number"><?php echo form_error('stk_position'); ?>

                                            </div>

                                        </div>

                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Email Address</label>

                                            <div class="col-sm-10">

                                                <input type="text" class="form-control" id="stk_email" name="stk_email" placeholder="" required data-msg-required="Please enter address"><?php echo form_error('stk_email'); ?>

                                            </div>

                                        </div>


                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Mobile Number</label>

                                            <div class="col-sm-4" style="margin-bottom: 6px;">
												<select class="form-control" id="countrycode" name="countrycode">
                                                	<option value="">Select country code..</option>
												<?php if(count($countriescode)!="0"){

													foreach($countriescode as $countries){


														echo '<option value="'.$countries['calling_code'].'">'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';

													}
												}

												?>
        										</select>

                                            </div>
                                            <div class="col-sm-6">

                                                <input type="text" class="form-control" id="stk_no" name="stk_no" placeholder="" required  data-msg-required="Please enter state"><?php echo form_error('stk_no'); ?>

                                            </div>

                                        </div>





                                        <div class="form-group">

                                            <label class="col-sm-2 control-label"></label>

                                            <div class="col-sm-10">
                                                <a class="btn btn-default" href="<?php echo base_url()?>cc/stakeholder/managestk">Back</a>
                                                <button type="submit" class="btn btn-primary" name="btn_add_stakeholder" id="btn_add_stakeholder">Submit</button>

                                            </div>

                                        </div>





                                    </form>

                                    </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->

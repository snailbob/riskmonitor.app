                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Add Scenario Task

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Add Response Action Plan Task</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

                

				<div class="row" >

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url()?>cc/scenario/add">Create another Scenario</a> 
               		</div>

                	<div class="col-lg-12" style="padding-top:20px;">

                    	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                    </div>

                    

                    

                    

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Add Response Action Plan Task</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-task" id="frm-add-task" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Scenario Description</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="scenario_desc" name="scenario_desc" placeholder="Enter scenario description" disabled="disabled"><?php echo $scn_info[0]['scenario']; ?></textarea><?php echo form_error('scenario_desc'); ?>							

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Task</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="task_desc" name="task_desc" placeholder="Enter short task description"></textarea><?php echo form_error('task_desc'); ?>							

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Task Guidance</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="task_guide" name="task_guide" rows="7" placeholder="Enter any guidance or business rules that apply to this task (Optional)"></textarea><?php echo form_error('task_guide'); ?>							

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Assign to</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="assign_to" id="assign_to">

                               	<option value=""> Select </option>

                                <?php

									if(count($crt_list) > 0)

									{

										foreach($crt_list as $crt)

										{

								?>

                                		<option value="<?php echo $crt['login_id'] ?>">

                                        <?php echo $this->master_model->decryptIt($crt['crt_first_name']).' '.$this->master_model->decryptIt($crt['crt_last_name']); ?>

                                        </option>

                                <?php

										}

									}

                                ?>

                               </select><?php echo form_error('assign_to'); ?>	

                           	</div>

                        </div>

                        

                        <div class="form-group">

                        <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-10">

                        <button type="submit" class="btn btn-primary"   name="add_task" id="add_task" value='0' >Submit</button>

                        </div>

            			</div>	

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>

					<div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-lightbulb-o"></i> Response Action Plan </h4>

                                </div>

                            	<div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-body">



								<?php 

								if(count($task_list)>0)

								{ ?>
                                
							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="45%">Task</th>

                                        <th width="35%">Owner</th>

                                        <th width="25%">Status</th>

                                        <th> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($task_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['task']; ?></td>

                                    	<td>
										<?php
											 if ($value['crt_first_name']=='' && $value['crt_last_name'] ==''){
											 	echo '<span class="text-muted small">Not Assigned</span>';
											 }
											 else{
												 echo  $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']);
											 }
										 ?>
                                        </td>

                                    	<?php 

                                    if($value['task_status'] == 1 ){

                                    $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Response Initiated</span>';

                                    } if($value['task_status'] == 0 ) {	

                                    $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Pre-Incident</span>';

                                    }

									 if($value['task_status'] == 2 ){

                                    $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Completed</span>';

                                    } 

                                    ?>

                                    <td><?php echo  $status ?></td>

                                    <td>
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="#" data-toggle="modal" data-target="#mytaskModal<?php echo $value['task_id']; ?>">View</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url(); ?>cc/scenario/delete/<?php echo  $value['task_id']; ?>')">Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cc/scenario/update/<?php echo  $value['task_id']; ?>">Edit</a> 
            </li>
          </ul>
        </div>    


<!-- Modal -->
<div class="modal fade" id="mytaskModal<?php echo $value['task_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Scenario Task Detail</h4>
      </div>
      <div class="modal-body">
		<p>Task Owner: <span class="text-muted"><?php if ($value['crt_first_name'] !=''){echo $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); } else { echo 'Not Assigned'; }?></span></p>
        
		<p>Scenario: <span class="text-muted"><?php echo $scn_info[0]['scenario']; ?> </span></p>
		<p>Task Description: <span class="text-muted"><?php echo $value['task']; ?> </span></p>
		<p>Task Guidance: <span class="text-muted"><?php echo $value['task_guide']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

                                    <?php /*?><a class="btn btn-green btn-xs" href="<?php echo base_url(); ?>cc/scenario/update/<?php echo  $value['task_id']; ?>">Edit</a> 

								<a class="btn btn-orange btn-xs" href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url(); ?>cc/scenario/delete/<?php echo  $value['task_id']; ?>')">Delete</a><?php */?>
                                

                                    </td>

                                    </tr>

								<?php 

									}


								?>

                                </tbody>

                             </table>

							</div>

                            <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
                                <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No tasks for this scenario</p>
								<?php }

								?>



                            </div>

                        </div>

                    </div>



                </div><!--.row-->




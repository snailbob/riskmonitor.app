<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Access Log
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Access Log</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">

                    <div class="col-lg-12">

							<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                        </div>

                    </div>


                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Access Log</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#MailStakeholder"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="MailStakeholder" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                
                                
                                


								<?php 

								if(count($logs)>0)

								{ ?>

							<div class="table-responsivexx">

							<table class="table table-hover" id="example-table">

                                <thead class="hidden">

                                    <tr>

                                        <th class="hidden">sort</th>
                                        
                                        <th>Event</th>

                                        <th class="hidden">date</th>



                                    </tr>

                                </thead>

								<tbody>

								<?php
									$logsort = 1;
									foreach($logs as $r => $value)

									{

								?>

                                                        

                                    <tr>
										<td class="hidden"><?php echo $logsort; ?></td>
                                    	<td>
										<?php
										if($value['type'] == 0){ //login
										
											echo $this->common_model->getcrtname($value['crt_id']).' logged into '.$this->common_model->getmodulename($value['module_id']).' <span title="'.$this->common_model->ago($value['date_added']).'">'.date_format(date_create($value['date_added']), 'jS F Y, g:ia').'</span>.';
											
										}
										if($value['type'] == 1){ //logout
										
											echo $this->common_model->getcrtname($value['crt_id']).' logged out from '.$this->common_model->getmodulename($value['module_id']).' <span title="'.$this->common_model->ago($value['date_added']).'">'.date_format(date_create($value['date_added']), 'jS F Y, g:ia').'</span>.';
											
										}
										if($value['type'] == 2){ //pickup task
										
											echo $this->common_model->getcrtname($value['crt_id']).' picked up task <i>'.$this->common_model->gettaskname($value['inci_task_id'], $value['module_id']).'</i> in '.$this->common_model->getmodulename($value['module_id']).' <span title="'.$this->common_model->ago($value['date_added']).'">'.date_format(date_create($value['date_added']), 'jS F Y, g:ia').'</span>.';
											
										}
										if($value['type'] == 3){ //completed task
										
											echo $this->common_model->getcrtname($value['crt_id']).' marked completed task <i>'.$this->common_model->gettaskname($value['inci_task_id'], $value['module_id']).'</i> in '.$this->common_model->getmodulename($value['module_id']).' <span title="'.$this->common_model->ago($value['date_added']).'">'.date_format(date_create($value['date_added']), 'jS F Y, g:ia').'</span>.';
											
										}
										?>
                                        </td>

                                    	<td class="hidden"><span class="text-muted small" title="<?php echo $value['date_added'];?>"><?php echo $this->common_model->ago($value['date_added']); ?></span></td>




                                    </tr>


								<?php 
									$logsort++;
									}
									
									
								?>

                                                    

                                                </tbody>


                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-file-text-o" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No data</p>
								<?php }

								?>

                                



                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

					




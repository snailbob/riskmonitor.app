<?php


$cc_id = $this->session->userdata('logged_cc_login_id');
$logged_user_level = $this->session->userdata('logged_user_level');
$org_id = $this->session->userdata('cc_selected_orgnaization');
$full_name = $this->session->userdata('logged_display_name');

// echo $unread_message_count;
// return false;

if($cc_id == "" || $org_id == ""){
    redirect(base_url().'signin');
}

else if($this->session->userdata('org_module')==""){
	redirect('cc/dashboard/selectmodule');
}
else{
    $recall_main_status = $this->common_model->setup_status_completed(); //recall_main_status();

}
$unread_message_count = $this->common_model->unread_message_count();

 ?>

<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><?php echo $this->common_model->appName(); ?> | <?php echo 'Cloud-based product recall management software'; ?> </title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?>
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/2/css/bootstrap-tour.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet"> -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <?php /*?><!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css"><?php */?>


    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">

	<link href="<?php echo base_url()?>assets/2/css/plugins/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

	<link href="<?php echo base_url()?>assets/2/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">


    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote.css" rel="stylesheet">
    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">-->
    <link href="<?php echo base_url()?>assets/2/css/plugins/flipclock/flipclock.css" rel="stylesheet">

    <!-- Popup css-->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">


    <!-- full calendar css-->
    <link href="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">


    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Style quilljs -->
    <link rel="stylesheet" href="//cdn.quilljs.com/1.1.5/quill.snow.css">
    <link rel="stylesheet" href="//cdn.quilljs.com/1.1.5/quill.bubble.css">

    
    <!-- Style croppie -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.0/croppie.css
"> 

    <!-- Style angular-summernote -->
     <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'.'summernote.css'; ?>"> 


    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">

    <!-- Style ngToast -->
    <!-- <link href="<?php echo base_url()?>assets/plugins/angularjs/ngToast/ngToast.css"> -->

    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->

	<script type="text/javascript" src="https://platform.linkedin.com/in.js">
	  api_key: 75eekrdxppqx8g
    </script>

<?php
    /*GET THE DATA FROM THE SESSION**************************/
    $uri_1 = $this->uri->segment(1);
	$uri_2 = $this->uri->segment(2);
	$uri_3 = $this->uri->segment(3);
	$uri_4 = $this->uri->segment(4);
	$uri_5 = $this->uri->segment(5);
	$uri_6 = $this->uri->segment(6);

	//admin help
	// $adminhelp = $this->common_model->get_admin_help($uri_2);

	//form validation global error style
	$this->form_validation->set_error_delimiters('<div class="text-red">', '</div>');

	//cc standby tasks reviewdate
	$revdate = $this->session->userdata('task_review_date');
	$therevdate = date_format(date_create($revdate), 'jS F Y, g:ia');


	$session_userfname = $this->session->userdata('logged_display_name');
	if($this->session->userdata('location') !=''){
		$mylocation = $this->session->userdata('location');
		$myloc_class = 'background: #fff;';
	}
	else{
		$mylocation = 'Enter your location';
		$myloc_class = 'background: #e4b9b9; color: #e74c3c;';
	}

	//get awaiting crt count
	$cc_id = $this->session->userdata('logged_cc_login_id');
	$user_id = $this->session->userdata('user_id');
	$org_id = $this->session->userdata('cc_selected_orgnaization');
	$waiting_crtmem = $this->common_model->get_waiting_crt($cc_id,$org_id);

	$where_recall = array(
		'cc_id'=>$cc_id,
		'org_id'=>$org_id,
		'closed'=>'0'
	);

	$open_recall = $this->master_model->getRecordCount('cf_recall',$where_recall);

	$awaitingcount = 0;
	$awaitingclass = 'background: #fff;';
	$awaitingdisplay = '';

	if(substr($this->session->userdata('org_module'), 0,1) != '5' && $open_recall > 0){
		$awaitingdisplay = 'hidden';
	}

	if(count($waiting_crtmem) > 0){
		$awaitingcount = count($waiting_crtmem);
		$awaitingclass = 'background: #e4b9b9; color: #e74c3c;';
	}
	else{
		$awaitingdisplay = 'hidden';
	}


	$mytimezone = $this->master_model->getRecords('time_zone',array('ci_timezone'=>$this->session->userdata('timezone')));

	$nowtime = $this->common_model->userdatetime();

	$date_zone = date_format(date_create($nowtime), "d F Y D"); //gmdate("d F Y D", $thetimeyeah); //g:i:sa d/m/Y
	$time_zone = date_format(date_create($nowtime), "Y-m-d g:i:s a"); // gmdate("Y-m-d g:i:s a", $thetimeyeah);

	//active case report count
	$num_report = $this->master_model->getRecordCount('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0'));


	//unread forum post count
	$forum_vstr_date = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_cc_login_id')));
	$forum_post = $this->master_model->getRecords('forum_post_master',array('cc_id'=>$this->session->userdata('team_cc'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'date_created >'=>$forum_vstr_date[0]['forum_last_visit'],'post_parent'=>'0'));


	//active modules
	$main_active_module = substr($this->session->userdata('org_module'), 0,1);
	$active_feature_modules = $this->common_model->active_module($org_id);

	$active_module =  $main_active_module.$active_feature_modules;

	$the_active_module = array('0'=>array('active_module'=>$active_module));
	//select org
	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));



	if($main_active_module == '5'){
		$module_name = 'Recall Manager';
	}
	else if($main_active_module == '1'){
		$module_name = 'Crisis Manager';
	}

	else if($main_active_module == '2'){
		$module_name = 'Case Manager';
	}

	else if($main_active_module == '4'){
		$module_name = 'Member Safety';
	}

	else if($main_active_module == '8'){
		$module_name = 'Continuity Manager';
    }
    
    $module_name = '';

	//check if status is demo
	$user_status = $this->session->userdata('logged_user_type');
	$remaining = 0;
    $cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
    
    //if cancelled subscription, set zero payment method
    $cc_info[0]['stripe_customer_id'] = ($cc_info[0]['subscription_cancelled'] == '1') ? '0' : $cc_info[0]['stripe_customer_id'];

    $billing = $this->common_model->single_payment($cc_id, 'preview');
    $total_bill = ($billing['orgs']) ? $billing['orgs'][0]['total_bill'] : 0;
    
	$user_info = ($user_id) ? $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id)) : array();

    $mobile_code = $this->master_model->getRecords('country_t');
    
    
	if(($user_status != 'live' && $cc_info[0]['stripe_customer_id'] == '0') || ($cc_info[0]['single_user'] == 'y' && $cc_info[0]['stripe_customer_id'] == '0')){

        

		if($this->session->userdata('stripe_plans') != ''){
			$stripe_plans = $this->session->userdata('stripe_plans');
		}
		else{
			$stripe_plans = $this->common_model->all_plans();
			$this->session->set_userdata('stripe_plans', $stripe_plans);
		}


		$price = 0;
		$the_gst = 0;
		$total_price = 0;
		$plan = '';

		$trialtext = '';
		$module_name = $trialtext;
	}


	/*********************************************************/
?>


	<script type="text/javascript" language="javascript">
		//declare variables
		var base_url='<?php  echo base_url(); ?>';
		var recall_time;
		var mydatetime = '<?php echo $time_zone ?>';
		var reviewdate = '<?php echo $revdate; ?>';

		var uri_1 = "<?php echo $this->uri->segment(1) ?>";
		var uri_2 = "<?php echo $this->uri->segment(2) ?>";
		var uri_3 = "<?php echo $this->uri->segment(3) ?>";
		var uri_4 = "<?php echo $this->uri->segment(4) ?>";
		var uri_5 = "<?php echo $this->uri->segment(5) ?>";
		var uri_6 = "<?php echo $this->uri->segment(6) ?>";
		var tab_active = "<?php echo (isset($_GET['tab_active'])) ? $_GET['tab_active'] : ''; ?>";
		var cc_id = "<?php echo $this->session->userdata('logged_parent_cc') ?>";
		var org_id = "<?php echo $this->session->userdata('cc_selected_orgnaization') ?>";
		var user_full_name = "<?php echo $this->session->userdata('logged_display_name') ?>";
		var now_time = "<?php echo date("F j, Y, g:i a") ?>";
		var my_location = "<?php echo $this->session->userdata('location'); ?>";
		var endtrial = "<?php echo ($cc_info[0]['stripe_customer_id'] == '0' && $remaining == 0) ? 'expired' : 'ok'; ?>";
		var endtourforever = '<?php echo (isset($user_info[0]['endtourforever'])) ? $user_info[0]['endtourforever'] : 'Y'; ?>';
    </script>


</head>

<body class="<?php echo ($this->uri->segment('2') == 'message' || $this->uri->segment('2') == 'standbytasks' || $this->uri->segment('2') == 'wiki') ? 'no-scroll-body' : ''; ?>">

    <script type="text/javascript">
		if(uri_2 == 'settings' || uri_3 == 'verifications'){

			  //GOOGLE PLUS
			  // Enter a client ID for a web application from the Google Developer Console.
			  // The provided clientId will only work if the sample is run directly from
			  // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
			  // In your Developer Console project, add a JavaScript origin that corresponds to the domain
			  // where you will be running the script.
			  var clientId = '1041052586833-p5gn0f5828unp0ve0t8i0girpp8mgba8.apps.googleusercontent.com';

			  // Enter the API key from the Google Develoepr Console - to handle any unauthenticated
			  // requests in the code.
			  // The provided key works for this sample only when run from
			  // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
			  // To use in your own application, replace this API key with your own.
			  var apiKey = 'AIzaSyCxXrGNIjpnWIf6FlqbbwlSp-oYYgfTADU'; //AIzaSyAVK5bSoKOZh4tjiy3NpJPFXzc8qCktm4w';

			  // To enter one or more authentication scopes, refer to the documentation for the API.
			  var scopes = 'https://www.googleapis.com/auth/plus.profile.emails.read'; //plus.me';

			  // Use a button to handle authentication the first time.
			  function handleClientLoad() {
				gapi.client.setApiKey(apiKey);
				window.setTimeout(checkAuth,1);
			  }

			  function checkAuth() {
				gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true} , handleAuthResult);
			  }


			  function handleAuthResult(authResult) {
				var authorizeButton = document.getElementById('authorize-button');

				 //authorizeButton.style.visibility = '';
				 // authorizeButton.onclick = handleAuthClick;
				  console.log(JSON.stringify(authResult));

					if (authResult && !authResult.error) {
					//authorizeButton.style.visibility = 'hidden';
					//	makeApiCall();
						//console.log(JSON.stringify(authResult));

					}

					else {
						//bootbox.alert('<p class="lead"><i class="fa fa-info-circle"></i> Something went wrong. Please contact admin.</p>');
					}
			  }

			  function handleAuthClick(event) {
				gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, makeApiCall);
				//handleAuthResult);
				//makeApiCall();

				return false;
			  }

			  // Load the API and make an API call.  Display the results on the screen.
			  function makeApiCall() {
				gapi.client.load('plus', 'v1', function() {
				  var request = gapi.client.plus.people.get({
					'userId': 'me'
				  });
				  request.execute(function(resp) {
						//console.log(JSON.stringify(resp));
						console.log(resp.emails[0].value);
						//console.log(resp.displayName);
						console.log(resp.id);
						console.log(resp.name.givenName);
						console.log(resp.name.familyName);
						console.log(resp.image.url);
						//console.log(resp.placesLived[0].value);

						$('.gplus_btn').addClass('disabled');
						var dataa = {
							"email": resp.emails[0].value,
							"id": resp.id,
							"type": 'gplus'
						};

						dataa = $.param(dataa); // $(this).serialize() + "&" +

						$.post(
							base_url+'cc/settings/add_connect',
							dataa,
							function(res){
								console.log(res);
								$('.gplus_btn').removeClass('disabled');
								$('#MyGooglePlusButton').addClass('hidden').siblings().removeClass('hidden');

							},
							'json'
						);



				  });
				});
			  }

		}


    </script>
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>



<!-- Modal for popup incidents list -->
<div class="modal fade" id="myLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Location</h4>
      </div>
      <div class="modal-body">
      	<form id="up_loc_form">
        	<div class="form-group">
            	<label></label>
                <input type="text" class="form-control update_location" name="up_location" id="up_location" placeholder="<?php echo $mylocation; ?>" />
                <input type="hidden" name="lat" />
                <input type="hidden" name="lng" />
                <span class="control-helper text-red" style="display: none;">Mandatory field.</span>
            </div>
        	<div class="form-group">
                <button class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>

</div>
<!-- Modal for popup incidents list -->
<div class="modal fade" id="myModalincidents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Initiated Scenario List</h4>
      </div>
      <div class="modal-body">
		<?php
			/* fetch scenario */

			$whr_arrscenario=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario_status'=>'1');

			$openscenario=$this->master_model->getRecords('cf_scenario',$whr_arrscenario,'*',array('scenario_id'=>'ASC'));

		if(count($openscenario)>0)

		{
			echo '<ol>';
			foreach($openscenario as $openscenario)

			{ echo '<li><p>'.$openscenario['scenario'] ;?>
 - <a data-toggle="modal" data-target=".bs-example-modal-sm<?php echo $openscenario['scenario_id']; ?>" class="btn btn-primary btn-xs hidden">Close Incident</a>
<button class="btn btn-primary btn-xs" onClick="$('.hidd<?php echo $openscenario['scenario_id']; ?>').toggle();">Close Incident</button> </p></li>
 <p class="text-center thumbnail hidd<?php echo $openscenario['scenario_id']; ?>" style="display:none; padding:10px;">
<span class="text-muted">Please confirm you wish to close this incident?</span> <a type="submit" href="<?php echo base_url().'cc/dashboard/closescenario/'.$openscenario['scenario_id']; ?>" class="btn btn-primary btn-sm">Ok</a>
          <button class="btn btn-default btn-sm"  onClick="$('.hidd<?php echo $openscenario['scenario_id']; ?>').hide();" >Cancel</button>
 </p>

            <?php



			}
			echo '</ol>';
		}

		?>




      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<?php

$whr_arrscenario2=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario_status'=>'1');

$openscenario2=$this->master_model->getRecords('cf_scenario',$whr_arrscenario2,'*',array('scenario_id'=>'ASC'));


if(count($openscenario)>0)

{

	foreach($openscenario2 as $openscenario2) {

?>

<div class="modal fade bs-example-modal-sm<?php echo $openscenario2['scenario_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<p>Please confirm you wish to close crisis incident.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a type="submit" href="<?php echo base_url().'cc/dashboard/closescenario/'.$openscenario2['scenario_id']; ?>" class="btn btn-primary">Ok</a>
        </div>
      </div><!-- /.modal-content -->
  </div>
</div>


    <?php



    }

}

?><!-- Modal for popup incidents list -->








    <nav class="navbar navbar-default navbar-blue navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
	   <?php
        if ($theorg[0]['cust_type'] == '0'){
        ?>

                <a class="navbar-brand hidden-xs preloadThis" id="site_logo" href="<?php echo base_url().'cc/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/AIG_rebrand_logo.gif" width="114" height="62" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url().'cc/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/AIG_rebrand_logo.gif" width="88" height="48" style="margin-top: -10px;"></a>


        </div>

            <div class="module-name hidden-xs" >
                <p class="text-center"><span style="font-size: 130%; font-weight: bold;">Welcome back, <?php echo $full_name; ?>!</span></p>
            </div>

            <div class="navbar-header col-lg-10 col-lg-offset-1 visible-xs" >
                <h5 class="big" style="text-align: center">Welcome back, <?php echo $full_name; ?>!</h5>
            </div>
	   <?php }
       else{
        ?>
                <a class="navbar-brand hidden-xs preloadThis" id="site_logo" href="<?php echo base_url().'cc/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/logo.png" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url().'cc/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/logo.png" height="48" style="margin-top: -15px;"></a>
        </div>

            <div class="module-name hidden-xs" >
                <p class="text-primary" style="text-align: center;"><span style="font-size: 130%; font-weight: bold;">Welcome back, <?php echo $full_name; ?>!</span>
                    <br><strong><?php echo date("d/m/Y") ?> &middot; <span class="user-clock"></span></strong>
                </p>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 visible-xs" >
                <h5 class="big text-white" style="text-align: center">Welcome back, <?php echo $full_name; ?>!</h5>
            </div>



		<?php } ?>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden-xs">
                        <a href="<?php echo base_url().'cc/settings/' ?>" class="navbar-link text-white img-displayer" style="margin-top: -5px;">
                            <img class="img-circle" src="<?php echo ($this->session->userdata('avatar') != '') ? $this->session->userdata('avatar') : 'http://via.placeholder.com/100x100?text=CF+User'; ?>" height="35" width="35" alt=""> <?php echo $this->session->userdata('logged_display_name'); ?>
                        </a>
                    </li>
                    <li class="hidden">
                        <a href="#"></a>
                    </li>
                   <li class="page-scroll"> <a href="<?php echo base_url().'cc/dashboard'; ?>" class="visible-xs  active"><i class="fa fa-home fa-fw"></i> Home</a></li>
                     <?php
						$unread=$this->common_model->get_unread_count($this->session->userdata('logged_cc_login_id'));
					?>
                   <li class="page-scroll"> <a href="<?php echo base_url().'cc/message/inbox' ?>" class="visible-xs <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-comments fa-fw"></i> Group Messaging <strong><?php if($unread_message_count > 0){echo '<span class="badge danger pull-right">'.$unread_message_count.'</span>';} ?></strong></a></li>


                    <?php if (strpos($active_module, '5') !== false || strpos($active_module, '8') !== false && strpos($active_module, '1') === false ){ ?>

                        <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/standbytasks" class="visible-xs "><i class="fa fa-list-alt fa-fw"></i> Standby Tasks</a></li>
                        <li class="page-scroll"> <a href="<?php echo base_url().'cc/standbytasks/incident_form/edit/form'; ?>" class="visible-xs "><i class="fa fa-list-alt fa-fw"></i> Incident Form</a></li>
                        <li class="page-scroll"> <a href="<?php echo base_url()?>cc/reminder" class="visible-xs"><i class="fa fa-bell-o fa-fw"></i> Set Review Date</a></li>


                    <?php } ?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/log" class="visible-xs"><i class="fa fa-edit fa-fw"></i> Post-Incident Review</a></li>

                   <?php if($logged_user_level == '0'){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/auditlog" class="visible-xs"><i class="fa fa-file-text-o fa-fw"></i> Audit Log</a></li>
                   <?php } ?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/wiki" class="visible-xs"><i class="fa fa-file fa-fw"></i> Wiki Space</a></li>


                    <li class="page-scroll upgrade-btn-holder">
                        <a href="#" class="visible-xs demo_btn">
                            <i class="fa fa-upload fa-fw"></i> Update Plan
                        </a>
                    </li>

                    <li class="visible-xs"><a href="<?php echo base_url();?>cc/dashboard/logout/"><i class="fa fa-sign-out fa-fw"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>

                    <li class="dropdown hidden-xs">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down fa-fw text-white"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>cc/settings"><i class="fa fa-key fa-fw"></i> My Account</a></li>
                        <li><a href="<?php echo base_url(); ?>cc/crisisteam/managecrt"><i class="fa fa-users fa-fw"></i> My Team</a></li>
                        <li><a href="<?php echo base_url(); ?>cc/auditlog"><i class="fa fa-file-text-o fa-fw"></i> Access Log</a></li>
                        

                        <li><a href="<?php echo base_url();?>cc/dashboard/logout/"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>


                      </ul>
                    </li>


                </ul>



            </div>
            <!-- /.navbar-collapse -->



				<?php //status and org
                $main_status=$this->common_model->get_main_status($this->session->userdata('logged_cc_login_id'),$this->session->userdata('cc_selected_orgnaization')); ?>

                <div class="topLabel bg-info">
                    <div class="pull-left megamenu_clone hidden-xs hidden-lg hidden-md" style="margin-left: 10px; margin-right: 10px;">

                    </div>

                	<div class="hidden-md hidden-lg">
                      <div class="scroller scroller-left" style="display: none;"><i class="glyphicon glyphicon-chevron-left"></i></div>
                      <div class="scroller scroller-right" style="display: block;"><i class="glyphicon glyphicon-chevron-right"></i></div>
                    </div>
                  <div class="wrapper hidden-lg hidden-md">

                    <ul class="nav list" id="myTab" style="left: 0px;">

                        <li class="">
                            <a href="<?php echo base_url().'cc/dashboard'; ?>" class="btn btn-default btn-sm home_nav_btn">
                                <i class="fa fa-home fa-fw"></i>
                            </a>
                        </li>

                        
                        <li class="">
                            <a href="<?php echo base_url().'cc/wiki'; ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Wiki Space">
                                <img src="<?php echo base_url().'assets/img/wiki_icon.png'?>" height="12" alt="">
                            </a>
                        </li>

                        <li class="">
                            <a href="<?php echo base_url().'cc/wiki/sharing'; ?>" class="btn btn-default btn-sm" data-toggle="tooltip"
                                data-title="Document Sharing">
                                <img src="<?php echo base_url().'assets/img/share-ico.png'?>" height="12" alt="">
                                <!-- <i class="fa fa-share-alt fa-fw" aria-hidden="true"></i> -->
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url().'cc/crisisteam/managecrt'; ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="My Team">
                                <i class="fa fa-users fa-fw" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="">
                            <span data-toggle="modal" data-target="#standbyModal">
                                <a href="#" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Standby Tasks">
                                    <i class="fa fa-power-off fa-fw" aria-hidden="true"></i>
                                </a>
                            </span>

                        </li>
                        <li class="">
                            <span>
                                <a href="<?php echo base_url().'cc/standbytasks/incident_form/edit/form'?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Incident Form">
                                    <i class="fa fa-power-off fa-fw" aria-hidden="true"></i>
                                </a>
                            </span>

                        </li>
                        <li class="">
                            <a href="<?php echo base_url().'cc/log'; ?>" class="btn btn-default btn-sm check_link_valid_user" data-toggle="tooltip" data-title="Post Incident Review">
                                <!-- <img src="<?php echo base_url().'assets/img/review-icon.png'?>" height="12" alt=""> -->
                                <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                            </a>
                        </li>


                        <li class="">
                            <a class="btn btn-default btn-sm" onclick="openWin()">
                                <i class="fa fa-video-camera fa-fw" aria-hidden="true"></i>
                            </a>
                        </li>

                        <li class="">
                            <a href="<?php echo base_url().'cc/message/inbox'; ?>" class="btn btn-default btn-sm message_nav_btn" <?php if ($unread_message_count>
                            0) { echo 'style="opacity: 1; background: #e4b9b9; color: #e74c3c;"';} ?>>
                                <?php if ($unread_message_count > 0) { echo $unread_message_count; }  ?>
                                <i class="fa fa-comments fa-fw"></i>
                            </a>
                        </li>

                        <li class="upgrade-btn-holder">
                        <a href="#" class="demo_btn btn btn-sm btn-warning text-white">Update Plan</a>
                        </li>
                        <?php if($recall_main_status){ ?>
                        <!-- <li class="">
                            <a class="btn btn-default btn-sm add_incident_form_btn">
                                <i class="fa fa-plus fa-fw" aria-hidden="true"></i> Incident Form
                            </a>
                        </li> -->
                        <li class="">
                            <a class="btn btn-default btn-sm add_incident_btn">
                                <i class="fa fa-plus fa-fw" aria-hidden="true"></i> Incident
                            </a>
                        </li>
                        <?php } ?>

                        <li class="">
                            <a href="<?php echo base_url().'cc/crisisteam/managecrt' ?>" class="btn btn-default btn-sm <?php echo $awaitingdisplay; ?>"
                                style="opacity: 1; <?php echo $awaitingclass; ?>">
                                <?php echo $awaitingcount; ?> Awaiting registration
                            </a>
                        </li>

                        <li class="">
                            <a class="btn btn-link btn-sm disabled" style="opacity: 1; cursor: default;">
                                <i class="fa fa-building-o"></i>
                                <?php echo $this->session->userdata('cc_selected_orgnaization_name'); ?>
                            </a>
                        </li>

                    </ul>
                  </div>

            		<div class="col-sm-12 hidden-xs hidden-sm">

                        <div class="statLabel hidden-xs">

						<ul class="list-inline blue_btns_list">

                              <li class="" id="lg_home_nav_btn">
                                    <a href="<?php echo base_url().'cc/dashboard'; ?>" class="btn btn-default btn-sm home_nav_btn" data-toggle="tooltip" data-title="Home">
                                        <i class="fa fa-home fa-fw"></i>
                                    </a>
                              </li>


                              <li class="">
                                    <a href="<?php echo base_url().'cc/wiki'; ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Wiki Space">
                                        <img src="<?php echo base_url().'assets/img/wiki_icon.png'?>" height="12" alt="">
                                    </a>
                              </li>

                              <li class="">
                                    <a href="<?php echo base_url().'cc/wiki/sharing'; ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Document Sharing">
                                        <img src="<?php echo base_url().'assets/img/share-ico.png'?>" height="12" alt="">
                                        <!-- <i class="fa fa-share-alt fa-fw" aria-hidden="true"></i> -->
                                    </a>
                              </li>
                              <li class="">
                                    <a href="<?php echo base_url().'cc/crisisteam/managecrt'; ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="My Team">
                                        <i class="fa fa-users fa-fw" aria-hidden="true"></i>
                                    </a>
                              </li>
                              <li class="">
                                    <span data-toggle="modal" data-target="#standbyModal">
                                        <a href="#" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Standby Tasks">
                                            <i class="fa fa-power-off fa-fw" aria-hidden="true"></i>
                                        </a>
                                    </span>

                              </li>
                              <li class="">
                                    <span>
                                        <a href="<?php echo base_url().'cc/standbytasks/incident_form/edit/form'?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Incident Form">
                                            <i class="fa fa-file-text fa-fw" aria-hidden="true"></i>
                                        </a>
                                    </span>
                              </li>
                              <li class="">
                                    <a href="<?php echo base_url().'cc/log'; ?>" class="btn btn-default btn-sm check_link_valid_user" data-toggle="tooltip" data-title="Post Incident Review">
                                        <!-- <img src="<?php echo base_url().'assets/img/review-icon.png'?>" height="12" alt=""> -->
                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                                    </a>
                              </li>


                              <li class="">
                                    <a class="btn btn-default btn-sm" onclick="openWin()"  data-toggle="tooltip" data-title="Video Conferencing">
                                        <i class="fa fa-video-camera fa-fw" aria-hidden="true"></i>
                                    </a>
                              </li>

                              <li class="" id="lg_message_nav_btn">
                                    <a href="<?php echo base_url().'cc/message/inbox'; ?>" class="btn btn-default btn-sm message_nav_btn" <?php if ($unread_message_count > 0) { echo 'style="opacity: 1; background: #e4b9b9; color: #e74c3c;"';} ?> data-toggle="tooltip" data-title="Messaging">
                                        <?php if ($unread_message_count > 0) { echo $unread_message_count; }  ?><i class="fa fa-comments fa-fw"></i>
                                    </a>
                              </li>
                              <li class="upgrade-btn-holder">
                                <a href="#" class="demo_btn btn btn-sm btn-warning text-white">Update Plan</a>
                              </li>
                        </ul>



                        </div>

                        <div class="organisationLabel hidden-xs">
                            <?php if($recall_main_status){ ?>
                            <!-- <a class="btn btn-default btn-sm add_incident_form_btn">
                                <i class="fa fa-plus" aria-hidden="true"></i> Incident Form
                            </a> -->
                            <a class="btn btn-default btn-sm add_incident_btn">
                                <i class="fa fa-plus" aria-hidden="true"></i> Incident
                            </a>
                            <?php } ?>

                        	<a href="<?php echo base_url().'cc/crisisteam/managecrt' ?>" class="btn btn-default btn-sm <?php echo $awaitingdisplay; ?>" style="opacity: 1; <?php echo $awaitingclass; ?>">
                            	<?php echo $awaitingcount; ?> Awaiting registration
                            </a>


                        	<a href="<?php echo base_url().'cc/crisisteam/managecrt' ?>" class="btn btn-link disabled btn-sm" style="opacity: 1;">
                                <i class="fa fa-building-o"></i> <?php echo $this->session->userdata('cc_selected_orgnaization_name'); ?>
                            </a>


                        </div>
                    </div>


                </div>


        </div>
        <!-- /.container -->


    </nav>


    <?php if ($cc_info[0]['stripe_customer_id'] == '0'){ ?>



    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="buyNowModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Upgrade</h1>
                            <hr class="star-primary">

                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active">Details</li>
                                <li>Payment</li>
                                <li>Confirm</li>
                            </ul>


                            <div class="text-center hidden" style="padding-bottom: 25px;">
                                <div class="btn-group btn-group-justified signup_tab_btns">
                                  <a class="btn btn-success btn-xs active step-details-btn" role="button" data-toggle="tab" href="#details">Details</a>
                                  <a class="btn btn-default btn-xs step-payment-btn" role="button" data-toggle="tab" href="#payment">Payment</a>
                                  <a class="btn btn-default btn-xs step-confirm-btn" role="button" data-toggle="tab" href="#confirm">Confirm</a>
                                </div>
                            </div>


                            <form id="signup" class="text-left">


                                <div class="alert alert-danger hidden" style="margin-bottom: 15px; clear: both;"><span class="payment-errors"></span></div>


                                <!-- Tab panes -->
                                <div class="tab-content signup_tabs">
                                  <div class="tab-pane fade in active" id="details">


                                        <!-- Form Name -->
                                        <legend>Authorized Purchaser</legend>


                                      <div class="fields_to_clone">
                                        <div class="row">

                                        	<div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">First Name <span class="text-danger"><strong>*</strong></span></label>
                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name" value="<?php echo $this->session->userdata('first_name')?>"/>
                                                </div>
                                            </div>

                                        	<div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Last Name <span class="text-danger"><strong>*</strong></span></label>
                                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="<?php echo $this->session->userdata('last_name')?>"/>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <label class="control-label">Business Name <span class="text-danger"><strong>*</strong></span></label>
                                            <input type="text" class="form-control" placeholder="Business Name" name="organization" value="<?php echo $this->session->userdata('cc_selected_orgnaization_name')?>"/>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Business Address <span class="text-danger"><strong>*</strong></span></label>
                                            <input type="text" class="form-control user_geolocation" placeholder="Enter location" name="address" value="<?php echo $this->session->userdata('location')?>"/>
                                            <input type="hidden" class="form-control" name="lat" value="<?php echo $this->session->userdata('location_lat')?>"/>
                                            <input type="hidden" class="form-control" name="lng" value="<?php echo $this->session->userdata('location_lng')?>"/>
                                            <input type="hidden" class="form-control" name="cc_id" value="<?php echo $cc_id?>"/>
                                            <input type="hidden" class="form-control" name="org_id" value="<?php echo $org_id?>"/>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Business Email <span class="text-danger"><strong>*</strong></span></label>
                                            <input type="text" class="form-control" placeholder="Business Email" name="email" value="<?php echo $this->session->userdata('logged_cc_email_id')?>"/>
                                        </div>


                                        <div class="row">

                                        	<div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Country Calling Code</label>
                                                    <select class="form-control" id="signup_ccode" name="country_code">
                                                    	<option value="" data-code="">Select..</option>
                                                        <?php
															foreach($mobile_code as $r=>$mc){
																echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

																if($this->session->userdata('country_id') == $mc['country_id']){
																	echo 'selected="selected"';
																}
																echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
															}
														?>
                                                    </select>
                                                </div>
                                            </div>

                                        	<div class="col-sm-7">
                                                <div class="form-group mobile_input">
                                                    <label class="control-label">Mobile Number</label>
                                                    <div class="input-group">
                                                      <span class="input-group-addon" id="addon-shortcode">+</span>
                                                      <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="<?php echo $this->session->userdata('crt_digits') ?>">
                                                    </div>
                                                    <input type="hidden" name="country_short"/>
                                                </div>
                                            </div>

                                        </div>
                                      </div><!--fields_to_clone-->


                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn-primary btn-rounded continue_btn">Continue <i class="fa fa-angle-double-right"></i></a>
                                        </div>
                                  </div><!--details tab-->

                                  <div class="tab-pane fade" id="payment">


                                        <div class="form-group hidden">
                                            <label class="control-label">Subscription Plan</label>
                                            <select class="form-control" id="select_plan" name="plan">
                                                <option value="">Select..</option>
                                                <option value="1000" selected>Unli</option>
                                                <?php

                                                if(is_object($stripe_plans->data) && count($stripe_plans->data) > 0) {
                                                    foreach($stripe_plans->data as $value) {
                                                        $total = number_format($value->amount / 100, 2);
                                                        $amount = $value->amount / 100;
                                                        $gst = $amount / 11;
                                                        $amount = $gst * 10;
                                                        $amount = number_format($amount, 2);
                                                        echo '<option value="'.$value->id.'__'.$value->name.'__'.$amount.'__'.round($gst, 2).'__'.$total.'"';

                                                        if($amount == $price){
                                                            echo ' selected="selected"';
                                                        }

                                                        echo '>'.$value->name .' (US$'.$total.')</option>';

                                                    }
                                                }
                                                ?>


                                            </select>
                                        </div>

                                        <div class="bill_infox hidden <?php //if($total_price == 0) { echo 'hidden'; } ?> ">
                                            <hr />
                                            <span class="text-center text-muted">
                                                You are paying in <strong class="text-primary">AUD$</strong>.  Your total charge is <strong class="text-primary">$<span class="the_total"><?php echo $total_price; ?></span></strong> ($<span class="thee_price"><?php echo $price; ?></span> Subscription Fee + $<span class="the_gst"><?php echo $the_gst; ?></span>, 10% GST).
                                            </span>

                                        </div>


                                        <hr />






                                        <div class="row credit_fields">


                                            <div class="col-sm-12">
                                                <ul class="list-inline pull-right">
                                                    <li><img src="<?php echo base_url().'assets/img/cards/visa.png' ?>" alt="" title="" /></li>
                                                    <li><img src="<?php echo base_url().'assets/img/cards/amex.png' ?>" alt="" title="" /></li>
                                                    <li><img src="<?php echo base_url().'assets/img/cards/mastercard.png' ?>" alt="" title="" /></li>

                                                </ul>
                                           		<span style="font-size: 16px; font-weight: bold">Payment</span>

                                                <div style="clear: both; margin-bottom: 15px;"></div>

                                                <div class="well" style="margin-bottom: 5px; padding-bottom: 0px;">

                                                    <div class="form-group">
                                                      <label><span>Card Number</span></label>
                                                      <input class="form-control" type="text" size="20" data-stripe="number"/>

                                                    </div>


                                                    <div class="row">

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                              <label><span>Expiration (MM)</span></label>
                                                              <input class="form-control" type="text" size="2" data-stripe="exp-month"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group">

                                                                <label><span>Expiration (YYYY)</span></label>
                                                                <input class="form-control" type="text" size="4" data-stripe="exp-year"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                            <label><span>Security Code <i class="fa fa-question-circle text-info security_info"></i></span></label>
                                                            <input class="form-control" type="text" size="4" data-stripe="cvc"/>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div><!--well-->

                                            </div>


                                            <div class="col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1">
                                                <p class="small text-muted text-center" style="margin: 0 auto;">
                                                <img class="pull-left" src="<?php echo base_url().'assets/img/lock-icon.png'?>" alt="" title="" style="margin-left: 10px;"/>
                                               <span class="text-left"> For your security, CrisisFlo does not store credit card details.<br /> Stripe is our secure payment provider. <a href="https://stripe.com" target="_blank">Find out more</a></span></p>

                                                <div id="security_info_content" class="hidden">
                                                    <img class="pull-right" src="<?php echo base_url().'assets/img/cards/visamastercard.png'?>" alt="" title=""/>
                                                    <p><strong>Visa &amp; MasterCard</strong><br />
                                                    The security code is the last 3 digits on the back of your card</p>
                                                    <hr />
                                                    <img class="pull-right" src="<?php echo base_url().'assets/img/cards/amexcard.png'?>" alt="" title=""/>
                                                    <p><strong>American Express</strong><br />
                                                    The security code is the 4 digit number printed on the front of your card</p>


                                                </div>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn-primary btn-rounded continue_btn" style="width: 112px;">Continue <i class="fa fa-angle-double-right"></i></a>
                                            <a href="#" class="pull-left btn btn-default btn-rounded payment_bck_btn" style="width: 112px;"><i class="fa fa-angle-double-left"></i> Back</a>
                                        </div>

                                  </div><!--payment tab-->

                                  <div class="tab-pane fade" id="confirm">

                                        <div class="form-group">
                                            <a href="#" class="btn btn-default btn-xs pull-right update_auth_btn">Update</a>
                                        	<h3>Authorized Purchaser Details</h3>
                                        	<table class="table table-hover table-striped auth_table">

                                            	<tbody>
                                                	<tr>
                                                    	<td>Purchaser Name</td>
                                                        <td>
                                                            <span class="coo_name"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Business Name</td>
                                                        <td>
                                                        	<span class="coo_bssname"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Business Address</td>
                                                        <td>
                                                        	<span class="coo_bssadd"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Business Email</td>
                                                        <td>
                                                        	<span class="coo_email"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Mobile Number</td>
                                                        <td>
                                                            <span class="coo_mobile"></span>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>

                                            <div class="the_clone_authfields hidden"></div>


                                        </div>

                                        <div class="form-group hidden">
                                        	<h3>Subscription Plan</h3>
                                        	<table class="table table-hover table-striped">
                                            	<thead class="hidden">
                                                	<tr>
                                                    	<th>Description</th>
                                                        <th>Price</th>
                                                    </tr>
                                                </thead>
                                            	<tbody>
                                                	<tr>
                                                    	<td colspan="2">
                                                        	<div class="clone_selplan"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>


                                        <div class="form-group">

                                            <button class="btn btn-primary btn-rounded pull-right" style="width: 112px;">Submit</button>
                                            <a href="#" class="pull-left btn btn-default btn-rounded confirm_bck_btn" style="width: 112px;"><i class="fa fa-angle-double-left"></i> Back</a>
                                        </div>

                                  </div><!--confirm tab-->

                                </div>


                            </form>


                        </div><!--modal-body-->
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->





	<?php } //end of demo ?>


    <div class="container-fluid main_container">
        <div class="row">

            <div class="col-xs-12 scrollable main_content">

                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Change Password



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Change Crisis-cordinator Password</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/settings"><i class="fa fa-angle-double-left"></i> Settings Menu</a>
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Change Password</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">


						<form action='' name="cc_change_password" id="cc_change_password" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label"><span style="font-size:96%">Email Address</span></label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $this->master_model->decryptIt($details[0]['email_id'])?>" required data-msg-required="Please enter last name" readonly="readonly"><?php echo form_error('email');  ?>

                            </div>

                        </div>





                        <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label"><span style="font-size:96%">New Password</span></label>

                            <div class="col-sm-10">

                                <input type="password" class="form-control" id="new_pass" name="new_pass" value="" required data-msg-required="Please enter first name"><?php echo form_error('new_pass'); ?>

                                <div class="col-sm-9 my-help-text" style="margin-top:15px; padding-left: 0; padding-right: 0">
                                    <span class="pwstrength_viewport_progress"></span>
                                </div>

                                <div class="col-sm-3 my-help-text" style="margin-top:15px; text-align: right; padding-left: 0; padding-right: 0">
                                     <span class="pwstrength_viewport_verdict"></span>
                                </div>
                                <div class="col-sm-12 mediumerror text-red" style=" padding-left: 0; padding-right: 0">
                                </div>

                            </div>

                        </div>



                         <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label"><span style="font-size:96%">Confirm Password</span></label>

                            <div class="col-sm-10">

                            <input type="password" class="form-control" id="confirm_pass" name="confirm_pass" value="" required data-msg-required="Please enter first name"><?php echo form_error('confirm_pass'); ?>

                            </div>

                        </div>





                                        <div class="form-group">

                                            <label class="col-sm-2 control-label"></label>

                                            <div class="col-sm-10">

                                                <button type="submit" class="btn btn-primary" name="change_cc_pw" id="change_cc_pw">Submit</button>

                                            </div>

                                        </div>



									</form>

                </div>
                </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->

            <!-- select input-->
            <div class="form-group">


                <select name="default_currency_code" id="default_currency_code" class="form-control">
                  <option value="">Select Currency</option>
                  <?php
				  	if(count($currency_code) > 0){
						foreach($currency_code as $cc=>$curcode){
							echo '<option value="'.$curcode['idCountry'].'"';
							if($default_currency == $curcode['idCountry']){
								echo ' selected="selected"';
							}
							echo '>'.$curcode['countryName'].' ('.$curcode['currencyCode'].')</option>';
						}
					
					
					}
				  
				  ?>
                </select>
               <span class="help-block text-red default_currency_code_error"></span>  

            </div>


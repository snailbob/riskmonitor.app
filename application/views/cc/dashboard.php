<style>
.dataTables_filter/*, .dataTables_info */,.dataTables_length{display: none;}
</style>
<script>
	
	var recall_time = <?php echo $the_sec; ?>;
	console.log('<?php echo $the_sec; ?>');
	
</script>
<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<?php
	//active modules
	$org_id = $this->session->userdata('cc_selected_orgnaization');
	$main_active_module = substr($this->session->userdata('org_module'), 0,1);
	$active_feature_modules = $this->common_model->active_module($org_id);
	
	$active_module =  $main_active_module.$active_feature_modules;
	
	$the_active_module = array('0'=>array('active_module'=>$active_module));
	
	
	//$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );
	$mymodyul = substr($this->session->userdata('org_module'),0,1);
	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));
	$task_review_date = $this->session->userdata('task_review_date');


?>


<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Crisis Coordinator’s KPI Dashboard</h1>

      <ol class="breadcrumb">

        <li class="active"><i class="fa fa-dashboard"></i> KPI Dashboard</li>

      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 

<!-- end PAGE TITLE AREA -->



<div class="row">


<?php


	if($this->session->flashdata('success')!="")

	{

	?>
    <div class="col-lg-12">
        <div class="alert alert-success alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    </div>

	<?php    

	} 

	if($this->session->flashdata('error')!="")

	{

	?>
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
    
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
            <?php echo $this->session->flashdata('error'); ?>
        
        </div>
    </div>
   <?php                     

	} 				

?>
          

    <!---/.response team contact list modal--->
<?php
$cc_id = $this->session->userdata('logged_cc_login_id');
$org_id = $this->session->userdata('cc_selected_orgnaization');
$main_status = $this->common_model->get_main_status($cc_id,$org_id);

if(($main_status=="Pre-Incident Phase" && count($recall) > 0) || $main_status!="Pre-Incident Phase" ){ ?>
    <div class="">
      <div class="col-md-6" style="padding-bottom: 0px;">

        <?php //Recall module activated
            if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>
    
        <div class="pull-left">

					<p class="hidden-xs">
                    
						<?php
                            $more_inci = count($open_recall) - 1;
							$my_inci_label ='';
                            if (count($open_recall) == 1){
                                $my_inci_label .= '1 Open Incident '; ?>
                               <button class="btn btn-default disabled"><?php echo $my_inci_label; ?></button> 
						
						<?php
                            }
                            if (count($open_recall) > 1){
                                $my_inci_label .= count($open_recall).' Open Incidents '; ?>
                               <button class="btn btn-primary" data-toggle="modal" data-target="#myRecalls"><?php echo $my_inci_label; ?></button> 
                    
                        <?php
                            }
                            //echo $this->pagination->create_links(); 
                        ?>
                    
					<?php if(count($recall) > 0 && strpos($the_active_module[0]['active_module'], '5') !== false  || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>
                    
                    <span class="hidden-xs hidden-sm hidden-md inci_flip_clock_label"><span style="margin-left: 15px;">Incident Clock :</span></span>
                    <a class="btn btn-default pull-right visible-sm visible-md disabled inci_clock_btn inci_clock_lg_btn " style="opacity: 1; margin-left: 5px;">
                        <i class="fa fa-clock-o fa-fw"></i> Incident Clock: <span id="ct" class="mobile_clock"></span>
                    </a>
                                
                    </p>
            
					<div class="hidden-xs hidden-sm hidden-md">
                    	<div class="clock my_recall_timer"></div>      
                    </div> 
                    <?php }//for recall only ?>
                     
    
        </div>
        <?php } //close recall module activated ?>
        
      </div>
    	<div class="visible-sm visible-xs">
        	<div  style="clear: both;"></div>
        </div>
      <div class="col-md-6" style="padding-bottom: 0px;">
		<?php if ((count($recall) > 0 && strpos($the_active_module[0]['active_module'], '5') !== false  || strpos($the_active_module[0]['active_module'], '8') !== false) || ($main_status != "Pre-Incident Phase" &&  strpos($the_active_module[0]['active_module'], '1') !== false)){ //hide if no recall?>

        <a class="btn btn-default btn-block disabled visible-xs inci_clock_btn" style="opacity: 1">
            <i class="fa fa-clock-o fa-fw"></i> Incident Clock: <span id="ct" class="mobile_clock"></span>
        </a>
        <div class="visible-xs" style="clear:both;padding-top: 10px;"></div>
        <a class="btn btn-primary pull-right visible-lg visible-md" href="javascript:;" style="min-width: 150px;margin-bottom: 15px;" onclick="teamLocalTime();">Team Contact List</a> 
        <a class="btn btn-primary pull-left visible-sm" href="javascript:;" style="margin-bottom: 15px; margin-right: 5px;" onclick="teamLocalTime();">Team Contact List</a> 
        <?php } //end no recall?>
        
        <?php if(count($recall) > 0 && strpos($the_active_module[0]['active_module'], '5') !== false  || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>
        <a class="btn btn-primary pull-right visible-lg visible-md" href="javascript:;" onclick="sendIncidentStatus('<?php echo $recall[0]['id']?>');" style="margin-right: 5px;min-width: 150px;">Email Status Report</a>
        
        <a class="btn btn-primary pull-right visible-lg visible-md" href="javascript:;" onclick="recallMap('<?php echo $recall[0]['id']?>');" style="margin-right: 5px;min-width: 150px;margin-bottom: 15px;">Situation Map</a>

        <a class="btn btn-primary pull-left visible-sm" href="javascript:;" onclick="sendIncidentStatus('<?php echo $recall[0]['id']?>');" style="margin-right: 5px;">Email Status Report</a>
        <a class="btn btn-primary pull-left visible-sm" href="javascript:;" onclick="recallMap('<?php echo $recall[0]['id']?>');" style="margin-right: 5px;margin-bottom: 15px;">Situation Map</a>
        <?php } ?>
        
        
        <?php if(count($recall) > 0 && strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>
			<?php
                $more_inci = count($open_recall) - 1;
                $my_inci_label ='';
                if (count($open_recall) == 1){
                    $my_inci_label .= '1 Open Incident '; ?>
                   <button class="btn btn-default disabled visible-xs btn-block"><?php echo $my_inci_label; ?></button> 
            
            <?php
                }
                if (count($open_recall) > 1){
                    $my_inci_label .= count($open_recall).' Open Incidents '; ?>
                   <button class="btn btn-primary visible-xs btn-block" data-toggle="modal" data-target="#myRecalls"><?php echo $my_inci_label; ?></button> 
            <?php
                }
                //echo $this->pagination->create_links(); 
            ?>

        
        <div class="visible-xs" style="clear:both;padding-top: 10px;"></div>
        <a class="btn btn-primary btn-block visible-xs" href="javascript:;" onclick="recallMap('<?php echo $recall[0]['id']?>');">Situation Map</a>
        <div class="visible-xs" style="clear:both;padding-top: 10px;"></div>
        <a class="btn btn-primary btn-block visible-xs" href="javascript:;" onclick="sendIncidentStatus('<?php echo $recall[0]['id']?>');">Email Status Report</a>
        <?php } ?>
        
        <div class="visible-xs" style="clear:both;padding-top: 10px;"></div>
        <a class="btn btn-primary visible-xs" href="javascript:;" style=" margin-bottom: 15px;" onclick="teamLocalTime();">Team Contact List</a> 
      </div><!--end of col-md-6-->
    </div><!--end of row-->
    <div style="clear:both;"></div>
    
    
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Response Team Contacts</h4>
      </div>
      <div class="modal-body">
      
          <div class="table-responsive">
            <table class="table table-hover table-bordered table-green" id="example-table">
    
                <thead>
    
                    <tr>
    
                        <th width="20%">First</th>
    
                        <th width="20%">Last</th>
    
                        <th width="20%">Position</th>
    
                        <th width="20%">Location</th>
    
                        <?php /*?><th width="20%">Location Time</th><?php */?>
    
                        <th width="20%">Mobile No.</th>
    
                        <th width="20%">More</th>
    
                    </tr>
        
                    </thead>
        
                    <tbody>
              
                    <?php 
            
                    if(count($all_crt)>0){
            
                        foreach($all_crt as $r => $value){

								
//							$ts_date = date_create();
//							$ts_tz = date_timestamp_get($ts_date);							
//							$lat = $value['location_lat'];
//							$lng = $value['location_lng'];
//							$g_api_tz = 'https://maps.googleapis.com/maps/api/timezone/json?location='.$lat.','.$lng.'&timestamp='.$ts_tz;
//							
//							$json = file_get_contents($g_api_tz); // this WILL do an http request for you
//							$tz_data = json_decode($json);
//							echo $data->{'token'};	
							
                    ?>
                    
                    <tr>
        
                        <td><?php echo $value['first_name']; ?></td>
        
                        <td><?php echo $value['last_name']; ?></td>
        
                        <td><?php echo  $value['crt_position']; ?></td>
                        
                        <td><?php echo  $value['location']; ?></td>
                        
                        <?php /*?><td class="my_local_td<?php echo $value['login_id']; ?>"><?php // echo  $tz_data->timeZoneId; ?></td><?php */?>
                        
                        <td><?php echo $value['crt_mobile']; ?></td>
                        
                        <td><span class="text-info" data-toggle="popover" data-html="true" data-content="<?php echo 'Crisis Function: '. $value['crisis_function'] ?><br><?php echo 'Alternate Member: '.$value['alt_member'] ?>" data-trigger="hover" data-placement="left" style="cursor: pointer;">View</span></td>
        
                    </tr>
                    <?php
        
                        }
        
                    }
        
                    ?>
        
                    
    
                </tbody>
    
                
    
                
    
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div><!---/.response team contact list modal--->
<?php } ?>



<!------START CONTINUITY MANAGER OR RECALL MANAGER------>
<?php if (strpos($the_active_module[0]['active_module'], '8') !== false || strpos($the_active_module[0]['active_module'], '5') !== false){ ?>




<?php

	if (count($recall) > 0){
		

		foreach($recall as $r=>$rc){//start loop each recall
			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			if($active_mod == '5'){
				$incident_type = '5';
				$pack_id = $rc['input_pack_id']; 
				$recall_field_name = 'recall_id'; 
				$db_name = 'cf_recall_steps'; 
			}
			else{
				$incident_type = '8';
				$pack_id = 17; 
				$recall_field_name = 'continuity_id'; 
				$db_name = 'cf_continuity_steps'; 
			}
		
			$continuity_id = $pack_id; //17;
			if($rc['input_pack_id'] != 0){
				$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('id'=>$continuity_id)); ?>
                
                
                
                
    <div class="col-lg-12">
        
        <div class="panel panel-default" style="radius: 0px;">
        


            <div class="panel-heading">
            

                <div class="panel-title">
                    <h4>
                        <?php echo $rc['incident_no'].': '.$rc['incident_name'] ?>
                            
						<?php
                        
                        if ($rc['initiation_type'] == 0){
                            
                            echo '<span class="label label-default">Mock</span>';
                        
                        }
                        
                        else{
                        
                            echo '<span class="label label-info">Live</span>';
                            
                        }
                        
                        ?>
                                
                                
                        

                    </h4>
    
                </div>
    
                <div class="panel-widgets hidden">
                    <a data-toggle="collapse" data-parent="#accordion" href="#open_incident" style="text-decoration: none;">
                    <i class="fa fa-chevron-up tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php // echo 'View '.$more_inci.' more incidents.';?>"></i></a>

                </div>
                <div class="clearfix"></div>
        
                </div>
        
                
        
                <div class="panel-body">
    
       
    
    
              		<div class="row">  
                        <div class="col-md-3 col-lg-3 steps_holder">
                     
                     
                            <ul class="nav nav-pills nav-stacked navigation" id="myTab" style="margin-bottom: 10px;">
                                
                                <?php
								
									//get blockers 
									$bb = array(
										'cc_id'=>$this->session->userdata('logged_cc_login_id'),
										$recall_field_name=>$rc['id']
									);
									$bb_unresolved = array(
										'cc_id'=>$this->session->userdata('logged_cc_login_id'),
										$recall_field_name=>$rc['id'],
										'status'=>'0'
									);
									$blockers = $this->master_model->getRecords('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));
									$unresolved_blockers = $this->master_model->getRecords('cf_recall_blockers',$bb_unresolved,'*',array('id'=>'DESC'));
									if(count($unresolved_blockers) > 0){
										$blocker_stat = 'text-red bg-danger';
										$blocker_count = '<span class="label label-danger" style="font-size: 65%; border-radius:50%; padding-left: 7px;padding-right: 7px;">'.count($unresolved_blockers).'</span>';
									}else{
										$blocker_stat = 'text-green bg-success';
										$blocker_count = '<i class="fa fa-check-circle fa-fw"></i>';
									}
								?>   
                                <?php //KPIs module
								if (strpos($the_active_module[0]['active_module'], '0') !== false){ ?>
                                <li class="kpi_menu_tab" id="recall_cost_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#recall_kpi<?php echo $rc['id'] ?>" class="text-info bg-info">
                                    <i class="fa fa-calculator fa-fw"></i> KPI Dashboard <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                <?php } ?>

                                <?php //cost monitor module
								if (strpos($the_active_module[0]['active_module'], '6') !== false){ ?>
                                <li class="costmon_menu_tab" id="recall_cost_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#recall_cost<?php echo $rc['id'] ?>" class="text-info bg-info">
                                    <i class="fa fa-usd fa-fw"></i> Cost Monitor <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                <?php } ?>

                                <li class="blockers_menu_tab" id="blockers_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#blockers-<?php echo $rc['id'] ?>" class="<?php echo $blocker_stat ?>">
                                    <?php echo $blocker_count ?> Issues Board <i class="fa fa-angle-double-right"></i></a>
                                </li>
<?php

if (count($myrecallpack) > 0){ //check if recall input pack
    $recallsteps = $this->master_model->getRecords('cf_recall_packs_steps',
		array('recall_pack_id'=>$myrecallpack[0]['id'], 'date <'=>$rc['initiation_date']),'*',
		array('order'=>'ASC'));
		
    $rpack_step_no = 2;
    foreach($recallsteps as $rst=>$rstep){ //loop step category
		//set step no via steps
		$rpack_step_no = $rstep['step_no'];
		$rpack_sort_no = $rstep['order'];
		
		//disect each step
		${'questions' . $rpack_step_no} = $this->master_model->getRecords($db_name,
			array('recall_id'=>$rc['id'],'step_no'=>$rpack_step_no),'*',
			array('category_id'=>'ASC'));
		
		
		//total question
		${'totq2' . $rpack_step_no} = count(${'questions' . $rpack_step_no});
		
		
		
		//check if each step is completed starts in step 3
		${'step' . $rpack_step_no . '_label'} = 0;
		
		foreach(${'questions' . $rpack_step_no} as $qu=>$qus){
			if($qus['status'] == '0'){
				${'step' . $rpack_step_no . '_label'}++;
			}
		}
		
		//steps label variables
		 if (${'step' . $rpack_step_no . '_label'} == '0'){
			 ${'step' . $rpack_step_no . 'color'} = 'text-green bg-success';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-check-circle fa-fw"></i>';
		} 
		 else if (count(${'questions' . $rpack_step_no}) == ${'step' . $rpack_step_no . '_label'} ){
			 ${'step' . $rpack_step_no . 'color'} = 'text-red bg-danger';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-circle-o fa-fw"></i>';
		} 
		 else{
			 ${'step' . $rpack_step_no . 'color'} = 'text-orange bg-warning';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-minus-circle fa-fw"></i>';
		 }
		
    
        
?>


                    
                                <li class="<?php if($rpack_step_no == 2 && strpos($the_active_module[0]['active_module'], '0') === false) { echo 'active'; }?>" id="step_<?php echo $rpack_step_no; ?>btn">
                                    <a data-toggle="tab" href="#step<?php echo $rpack_step_no.'-'.$rc['id'] ?>" class="<?php echo ${'step' . $rpack_step_no . 'color'}; ?>">
                                    <?php echo ${'step' . $rpack_step_no . 'icon'}; ?> <?php echo ucfirst($rstep['name']); ?> <i class="fa fa-angle-double-right"></i></a>
                                </li>



 <?php   
        $rpack_step_no++;
    }
?>
                
                
    
                            </ul>
                                

                        </div><!--steps_holder-->
                        <div class="col-md-9 col-lg-9">
                            
                          <p class="lead text-muted text-center tasktab_loader" style="margin-top: 65px;">
                          	<i class="fa fa-spinner fa-spin"></i><br />
                            Loading..

                          </p>  
                        
                        <div class="user-body">
                            <div class="tab-content">
                            
                                <div class="tab-pane fade" id="blockers-<?php echo $rc['id'] ?>">
                                
    <h3>Issues Board
    
    

        <!-- Single button -->
        <div class="btn-group pull-right" style="margin-right: 15px;">
          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 60%; min-width: 82px; right: 0; left: auto;">
          
            <li class="active all_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','','.all_blocker_btn'); ">All</a></li>
          
            <li class="open_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','open','.open_blocker_btn');">Open</a></li>
          
            <li class="closed_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','resolved','.closed_blocker_btn'); ">Closed</a></li>
  
            <li class="my_issues"><a href="javascript:;" onclick="filterTasks('-blockers','<?php echo $this->session->userdata('logged_display_name')?>','.my_issues');">My Issues</a></li>
            
            
          </ul>
        </div> 
        
    	<button class="btn btn-primary btn-sm pull-right add_issue_btn" data-id="" style="margin-right: 5px;">Add Issue</button>
            
    </h3>
    
      <table class="table forum table-striped" id="recall-step-filter-table-blockers">
        <thead>
          <tr>
            <th class="cell-stat text-center hidden-xs"></th>
            <th>
                <div class="col-sm-12 hidden">
                    <input type="text" class="form-control" id="recall-step-filter-blockers" data-action="filter" data-filters="#recall-step-filter-table-blockers" placeholder="Filter Tasks"/>
            
                </div>
            
            </th>
            <th class="cell-stat-1-point-5x text-right hidden-xs"></th>
          </tr>
        </thead>
        <tbody>
        
        	<?php
			
			if(count($blockers) > 0){
				foreach( $blockers as $b=>$blck){
			?>
 			<tr>
            	<td class="text-center hidden-xs">
                    <button class="btn btn-default" id="comp_blck<?php echo $blck['id']?>" style="margin-top: 10px; padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if($blck['status'] == '0') { echo 'display: none'; }?>" onclick="checkTheBlocker('0','<?php echo $blck['id']?>','<?php echo count($unresolved_blockers)?>','<?php echo $rc['id']?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                    
                    
                    <button class="btn btn-default" id="inc_blck<?php echo $blck['id']?>" style="padding:3px; margin-top: 10px; <?php if($blck['status'] == '1') { echo 'display: none'; }?>" onclick="checkTheBlocker('1','<?php echo $blck['id']?>','<?php echo count($unresolved_blockers)?>','<?php echo $rc['id']?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>


                </td>
            	<td>
                  <h4><?php echo $blck['blocker']?><br>
                  
                  	<small>
                    
                        <span class="blocker_note_empty" style=" <?php if ($blck['note'] != ''){ echo 'display: none'; } ?>">No notes added</span>
                        
                        <a href="#" class="blocker_note_btn" data-toggle="modal" data-target="#blockerNote" onclick="loadBlockerNote('<?php echo $blck['id']?>');" style=" <?php if ($blck['note'] == ''){ echo 'display: none'; } ?>">View notes</a><br />
                        
                        
                        Issue: <?php echo $blck['issue']?><br />
                        Impact: <?php echo $blck['impact']?><br />
                        Raised by: <span class="label label-warning" style="border-radius: 15px;"><?php echo $my_class->getcrtname($blck['crt_id']);?></span><br />
                        Dependent Task(s): <?php echo $this->common_model->taskname($blck['task_id'], $db_name);?><br />
                        Assigned to: <span class="label label-warning" style="border-radius: 15px;"><?php if($blck['assigned'] != '0') { echo $my_class->getcrtname($blck['assigned']); } else { echo 'Not assigned'; }?></span>
                  
                    </small>
                  </h4>
                  <p>
                	<span class="small">Date Raised: <span class="text-muted"><?php echo date_format(date_create($blck['date_created']), 'm.d.Y, g:ia'); ?></span></span><br />
                	<span class="small date_resolved_holder">
						<?php if ($blck['status'] == '1'){?>
                            Date Resolved: <span class="text-muted"><?php echo date_format(date_create($blck['date_resolved']), 'm.d.Y, g:ia'); ?></span>
						<?php }else { echo '<span style="visibility: hidden">open</span>';} ?>
                    </span>
                  </p>
                  
                  <p class="hidden-lg hidden-md hidden-sm">
                     
                        <button class="btn btn-default" id="comp_blck<?php echo $blck['id']?>" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if($blck['status'] == '0') { echo 'display: none'; }?>" onclick="checkTheBlocker('0','<?php echo $blck['id']?>','<?php echo count($unresolved_blockers)?>','<?php echo $rc['id']?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                        
                        
                        <button class="btn btn-default" id="inc_blck<?php echo $blck['id']?>" style="padding:3px; <?php if($blck['status'] == '1') { echo 'display: none'; }?>" onclick="checkTheBlocker('1','<?php echo $blck['id']?>','<?php echo count($unresolved_blockers)?>','<?php echo $rc['id']?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>
                     
                        <a href="#" class="btn btn-sm btn-default" id="add_note_blck<?php echo $blck['id']?>" style="background:#e0e0e0; <?php if ($blck['status'] == '1'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#resolveBlocker" onclick="openResolveBlocker('<?php echo $rc['id']?>','<?php echo $blck['id']?>');loadBlockerNote('<?php echo $blck['id']?>');"><i class="fa fa-gavel tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolve"></i></a>
                  
                  </p>
                
                </td>
            	<td class="text-right hidden-xs">
                    <a href="#" class="btn btn-sm btn-default" id="add_note_blck<?php echo $blck['id']?>" style="background:#e0e0e0; <?php if ($blck['status'] == '1'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#resolveBlocker" onclick="openResolveBlocker('<?php echo $rc['id']?>','<?php echo $blck['id']?>');loadBlockerNote('<?php echo $blck['id']?>');"><i class="fa fa-gavel tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolve"></i></a>
                
                </td>
            </tr>     
        
        
        <?php
				}
			} else { ?>
        
        
        	<tr>
            	<td colspan="3" class="text-muted">No issue</td>
            </tr>
        	
        <?php } ?>
		</tbody>
      </table>    
    
    
    
                                </div>
                                <!-- end of blockers tab-->
                                
                                
								<?php //Recall feature KPIs module tab-pane fade in
                                    if (strpos($the_active_module[0]['active_module'], '0') !== false){ ?>
                                
                                    
                                <div class="tab-pane steptab_active" id="recall_kpi<?php echo $rc['id'] ?>">
                   
                   
        <?php
			$cc_id = $this->session->userdata('logged_cc_login_id');
			$org_id = $this->session->userdata('cc_selected_orgnaization');
			
			
			$whr_inci = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'closed'=>'0'
			);
			
			if($incident_type == '5'){
				$inci_type = 'recall';
				$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
			}
			else{ //continuity = 8
				$inci_type = 'continuity';
				$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
			}
			
			
			$whr_noti = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'type'=>$inci_type,
				'incident_id'=>$rc['id']
			);
			
			$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
			
			$email_sent_count = 0;
			$email_confirmed_count = 0;
			$sms_sent_count = 0;
			if(count($notices_sent) > 0){                
				foreach($notices_sent as $r=>$ns){
					//email sent count
					$whr_email_sent = array(
						'bulk_id'=>$ns['id'],
						'email_sent'=>'1'
					);
					$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

					//email confirmed count
					$whr_email_confirmed = array(
						'bulk_id'=>$ns['id'],
						'email_confirmed'=>'1'
					);
					$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


					//sms sent count
					$whr_sms_sent = array(
						'bulk_id'=>$ns['id'],
						'sms_sent'=>'1'
					);
					$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
					
					$email_sent_count += $bulk_data_email_sent;
					$email_confirmed_count += $bulk_data_email_confirmed;
					$sms_sent_count += $bulk_data_sms_sent;
				
				}
				
			}
	
			//get notification kpi
			$noti_kpi = 0;
			if($email_sent_count != 0){
				$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
			}
			
			
			//$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);

			$whr_kpis = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'incident_id'=>$rc['id'],
				'incident_type'=>$incident_type
			);
			
			$kpis = $this->master_model->getRecords('bulk_kpi', $whr_kpis, '*', array('date_added'=>'DESC'));

	
			//set kpi variables
			$all_kpi = array();
			
			$all_kpi[] = array(
				'type'=>'comp_kpi',
				'dbcell'=>'comp_data',
				'bgcolor'=>'orange',
				'name'=>'Units under Company control'
			);
		
			$all_kpi[] = array(
				'type'=>'dist_kpi',
				'dbcell'=>'dist_data',
				'bgcolor'=>'blue',
				'name'=>'Units in distribution'
			);
		
			$all_kpi[] = array(
				'type'=>'market_kpi',
				'dbcell'=>'market_data',
				'bgcolor'=>'green',
				'name'=>'Units in consumer market'
			);
	


		?>
        
                   
                   
                                
                                
    <h3>KPI Dashboard
    
        <small class="hidden">
			<?php if(count($kpis) > 0){ 
			
			if($kpis[0]['date_updated'] == '0000-00-00 00:00:00'){ ?>
            <span class="text-muted" title="<?php echo $kpis[0]['date_added'];?>"><?php echo 'added '. $this->common_model->ago($kpis[0]['date_added']); ?></span></td>
            
            <?php } else{ ?>
            <span class="text-muted" title="<?php echo $kpis[0]['date_updated'];?>"><?php echo 'updated '. $this->common_model->ago($kpis[0]['date_updated']); ?></span></td>
            
            <?php } }?>
        </small>
    
    </h3>
    <div class="col-xs-12" style="padding-top: 20px; padding-left: 0px;">
       <div class="row">
    
    
    		<?php
			$count_all_kpi = 1;
			foreach($all_kpi as $r=>$value){
				$kpi_cell = $value['dbcell'];
				if($value['type'] != 'comp_kpi') {
					$akpiclass = 'text-primary';
					$akpistyle = 'cursor: pointer;';
					
					if($value['type'] == 'dist_kpi'){
						$akpiclass .=" dist_toggle_btn";
					}
				}
				else{
					$akpiclass= '';
					$akpistyle= '';
				}
				
				
				//set value
				$total_unit = '';
				$unit_return = '';
				$u_disposed = '';
				$u_transformed = '';
				$u_corrections = 'NA';
				$the_kpi = '0';
				$u_date_update = '';
				$u_threshold = '0';
				
				//check count of kpi
				if(count($kpis) > 0) {
					$kpi_id = $kpis[0]['id'];
					$dbcell = unserialize($kpis[0][$value['dbcell']]);
					if($dbcell != ''){ //get each kpi db cell data
						$total_unit = number_format($dbcell['total_unit']);
						$unit_return = number_format($dbcell['unit_return']);
						$u_disposed = number_format($dbcell['u_disposed']);
						$u_transformed = number_format($dbcell['u_transformed']);
						$u_corrections = number_format($dbcell['u_corrections']);
						$the_kpi = round($dbcell['the_kpi'], 0);
						$u_date_update = $dbcell['date_updated'];
						$u_threshold = round($dbcell['threshold'], 0);
					}
				}
				
				
				if($the_kpi >= $u_threshold){
					$kpi_status = '';
				}
				else{
					$kpi_status = '<i class="fa fa-circle text-red pull-right" data-toggle="tooltip" title="Status: Not OK" style="font-size: 160%;"></i>';
				}
			?>
            
            <div class="col-sm-6">
                <div class="tile <?php echo $value['bgcolor']; ?> dash-demo-tile">
                    <h4><?php echo $value['name'].' '.$kpi_status; ?></h4>
                    <div class="row">
                    	<div class="col-lg-5 text-center">
                            <div id="easy-pie-1" class="easy-pie-chart" data-percent="<?php echo $the_kpi;?>">
                                <span class="percent"></span>
                            </div>
                        </div>
                    
                    	<div class="col-lg-7">
                        	<ul class="list-unstyled easy-pie-data" style="font-size: 90%;">
                            	<?php
								$nokpidata = '<li>No data</li>';
								if(count($kpis) > 0) {
									if($dbcell != '') {
										
										?>
                                
                                
                            	<li>Total Units: <?php echo $total_unit; ?></li>
                            	<li>Units Disposed: <?php echo $u_disposed ?></li>
                            	<li>Units Transformed: <?php echo $u_transformed; ?> </li>
                            	<li>Total Corrections: <?php echo $u_corrections; ?></li>
                            	<li style="border-top: 1px solid; border-color: rgba(255,255,255, .2); color: rgba(255,255,255, .5);"><small> 
                                	<?php echo date_format(date_create($u_date_update), 'M d, Y, g:i A'); ?>
                                 </small></li>
                                
                                <?php
									}
									else {
										echo $nokpidata;
									}
								}
								else{
									echo $nokpidata;
								}
								
								?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
			
			if(($count_all_kpi%2) == 0){
				echo '<div class="clearfix"></div>';
			}
			
			$count_all_kpi++;
			}//end loop kpis ?>




            <div class="col-sm-6">
                <div class="tile purple dash-demo-tile">
                    <h4>Outbound notifications</h4>
                    <div class="row">
                    	<div class="col-lg-5 text-center">
                            <div id="easy-pie-4" class="easy-pie-chart" data-percent="<?php echo $noti_kpi; ?>">
                                <span class="percent"></span>
                            </div>
                        </div>
                    
                    	<div class="col-lg-7">
                        	<ul class="list-unstyled easy-pie-data" style="font-size: 90%;">
                            	<li>Emails Sent: <?php echo $email_sent_count; ?></li>
                            	<li>Confirmed Receipts: <?php echo $email_confirmed_count; ?></li>

                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    
       
       </div><!--end of row--> 		
       

        <div class="table-responsive">
        
        
            <div class="form-group hidden">
                <input class="form-control" type="text" name="contact_cntr" id="contact_cntr" value="<?php if(count($kpis) > 0) { echo $kpis[0]['contact_cntr']; } ?>"/>
                <input class="form-control" type="text" name="kpi_email" id="kpi_email" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_email']; } ?>"/>
                <input class="form-control" type="text" name="kpi_website" id="kpi_website" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_website']; } ?>"/>
                <input class="form-control" type="text" name="kpi_trading" id="kpi_trading" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_trading']; } ?>"/>
            </div>
        

        <table class="table customcontactstable" id="example-tablexx">

            <thead>

                <tr>
                    
                    <th>Customer Contacts</th>
                    
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Contact Centre</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Email</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Website</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Trading Partner</th>



                </tr>

            </thead>

            <tbody>
            
				<?php
				
					$contact_cntr = '';
					$kpi_email = '';
					$kpi_website = '';
					$kpi_trading = '';
					
                    if(count($kpis) > 0) {
                        if($kpis[0]['contact_cntr'] !=''){
                            $contact_cntr = number_format($kpis[0]['contact_cntr']);
                            $kpi_email = number_format($kpis[0]['kpi_email']);
                            $kpi_website = number_format($kpis[0]['kpi_website']);
                            $kpi_trading = number_format($kpis[0]['kpi_trading']);
                        
                        }
                    }
                ?>
            
                <tr class="tr_main">
                    <td><i>Inbound customer contacts</i><br />
                        <button class="btn btn-primary submit_kpis btn-xs hidden disabled submit_kpi_changes" type="submit">Save</button>
                        <a class="btn btn-default btn-xs hidden cancel_kpi_btn">Cancel</a>
                    
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <a href="#" class="empty_kpi" title="click to edit"><?php echo $contact_cntr; ?></a>
                    

                        <input type="text" class="form-control kpi_input hidden" name="contact_cntr" value="<?php echo $contact_cntr; ?>"/>
                        
                        <input type="hidden" class="form-control hidden" name="id" value="<?php if(count($kpis) > 0) { echo $kpis[0]['id']; }?>"/>
                        
                        <input type="hidden" class="form-control hidden" name="incident_id" value="<?php echo $rc['id']; ?>"/>
                        <input type="hidden" class="form-control hidden" name="incident_type" value="<?php echo substr($this->session->userdata('org_module'), 0, 1); ?>"/>
                        
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        
                        <a href="#" class="empty_kpi" title="click to edit"><?php echo $kpi_email; ?></a>

                        <input type="text" class="form-control kpi_input hidden" name="kpi_email" value="<?php echo $kpi_email; ?>"/>
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                       
                        
                        <a href="#" class="empty_kpi" title="click to edit"><?php echo $kpi_website?></a>

                        <input type="text" class="form-control kpi_input hidden" name="kpi_website" value="<?php echo $kpi_website; ?>"/>
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                      
                        <a href="#" class="empty_kpi" title="click to edit"><?php echo $kpi_trading; ?></a>

                        <input type="text" class="form-control kpi_input hidden" name="kpi_trading" value="<?php echo $kpi_trading;?>"/>
                       
            
                    </td>


                </tr><!--consumer market-->
                
                <?php if(count($kpis) > 0) {
                    if($kpis[0]['kpi_trading'] !=''){
                    ?>


                <tr>
                    <td>&nbsp;</td>
                    <td colspan="4" style="border: 1px solid #ddd;">
                    
                    
                                            
    
                        <!-- Pie Chart Example -->
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-chart-pie"></div>
                        </div>
                        
                        <p class="hidden">&nbsp;<a href="#" data-toggle="modal" data-target="#chartDataModal" style="display: none;"><i class="fa fa-edit"></i> Edit</a></p>
                    
                    
                    </td>
                </tr>

                <?php } } ?>

           
                                

            </tbody>


        </table>

    </div>

    <!-- /.table-responsive -->




    
    </div><!--/col-xs-12-->

    
    
    
                                </div>
                                <!-- end of KPIs module tab-->
                                <?php } ?>
                                
								<?php //Recall feature cost monitor module activated tab-pane fade
                                    if (strpos($the_active_module[0]['active_module'], '6') !== false){ ?>
                                    
                                <div class="tab-pane" id="recall_cost<?php echo $rc['id'] ?>">
                                
                                
    <h3>Cost Monitor
    
    

                        <?php if(count($recall)> 0){ ?>
                        <a class="btn btn-primary btn-sm pull-right hidden-xs" href="#" data-toggle="modal" data-target="#addCostCat" style="width: 150px; margin-left: 5px; margin-right: 15px;" onclick="$('#name_cat').val('');$('#cat_id').val('');"><b>+</b> Cost Category</a> 
                        <a class="btn btn-primary btn-sm pull-right hidden-xs" href="#" data-toggle="modal" data-target="#updateDefaultCurrency" style="width: 150px; margin-left: 5px;" onclick="addCurrencyDropdown('<?php echo $recall[0]['default_currency_id']?>','<?php echo $recall[0]['id']?>')">Default Currency</a> 
                        <a class="btn btn-primary btn-sm pull-right hidden-xs" href="#" data-toggle="modal" data-target="#viewAccumulative" style="width: 150px;">Running Cost</a> 
                        
                        <a class="btn btn-primary btn-sm pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#addCostCat" onclick="$('#name_cat').val('');$('#cat_id').val('');"><b>+</b> Cost Category</a> 
                        <a class="btn btn-primary btn-sm pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#updateDefaultCurrency"  onclick="addCurrencyDropdown('<?php echo $recall[0]['default_currency_id']?>','<?php echo $recall[0]['id']?>')">Default Currency</a> 
                        <a class="btn btn-primary btn-sm pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#viewAccumulative">Running Cost</a> 
                        <?php } ?>
    
    </h3>
    
             
<?php 
						
	if (count($recall) > 0){
	?>
    
							<div class="col-xs-12" style="padding-top: 20px; padding-left: 0px;">
    
								<?php 

								if(count($cost_category)>0)

								{ ?>

							<?php /*?><div class="" style="padding-top: 20px;"><?php */?>

							<table class="table" id="example-tablexx">

								<tbody>

								<?php
								
									foreach($cost_category as $r => $value)

									{
										
								?>

                                                        

                                    <tr class="bg-info">

                                    	<td width="100%">
                                        <h4 style="text-transform: uppercase; font-weight: bold"><?php echo  $value['name']; ?></h4>
             
                                        
                                        </td>

                                        <td class="pull-right">
                                    
       <!-- Single button -->
        <div class="btn-group" style="margin-top: 5px;">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; left: auto; right: 0;">
          
            <li>
                <a href="#" data-toggle="modal" data-target="#addCostItem" onclick="resetItemFields('<?php echo $value['cost_id']?>','<?php echo $recall[0]['default_currency_id']?>');">Add Cost Item</a>
            </li>
          
            <li>
                <a href="javascript:;" onclick="updateCostCat('<?php echo $value['cost_id']?>','<?php echo $value['name']?>')">Edit Category Name</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return delete_cost_cat('<?php echo $value['cost_id'] ?>');">Delete Category</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#viewCategorySummary" onclick="showCostSummary('<?php echo $value['cost_id'] ?>')">View Category Sub-Total</a>
            </li>
          </ul>
        </div>    

										
                                    </td>

                                    </tr>

									<tr>
                                    	<td colspan="2">
                                        
                                        
                           
								<?php
                                
                                $items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$value['cost_id']));
                                ?>
                                        
            

								<?php 

								if(count($items)>0)

								{ ?>

							<?php /*?><div class="table-responsive"><?php */?>

							<table class="table table-hover table-bordered table-datatable">

                                <thead>

                                    <tr>

                                        <th width="10%">Date</th>
                                        
                                        <th width="60%">Item</th>

                                        <th width="10%">CUR</th>

                                        <th width="14%">Cost</th>

                                        <th width="6%"></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($items as $r => $value)

									{
										
								?>

                                                        

                                    <tr>

                                    	<td width="10%"><?php echo $value['item_date_invoice']?></td>
                                    	<td width="60%">
										
										<?php
										
										$files = $this->master_model->getRecords('private_message_file',array('cost_item_id'=>$value['item_id']));
										if(count($files) > 0){
											echo '<i class="fa fa-paperclip text-info fa-fw"></i> ';
										}else{
											echo '<i class="fa fa-hand-o-right text-info fa-fw" style="visibility:hidden"></i> ';
										}
										echo  $value['item_name']; ?>
                                        
                                        </td>

                                    	<td width="10%"><?php echo $my_class->currencycode($value['currency']) ?></td>
                                    	<td width="14%" class="text-right"><?php echo number_format($value['item_cost'],2); ?></td>

                                        <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; right: 0; left: auto;">
            <li>
                <a href="javascript:;" onclick="updateItemCost('<?php echo $value['item_id']?>','<?php echo $value['item_name']?>','<?php echo $value['item_cost']?>','<?php echo $value['currency']?>','<?php echo $value['item_date_invoice']?>')">Edit Cost Item</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return delete_cost_item('<?php echo $value['item_id'] ?>');">Delete Cost Item</a>
            </li>
            <li class="hidden">
                <a href="#" data-toggle="modal" data-target="#viewCostItem" onclick="viewCostItemDetails('<?php echo $value['item_id']?>')">View Cost Detail</a>
            </li>
            
		<?php if(count($files) > 0){ ?>
        
            <li>
                <a href="<?php echo base_url().'cc/costmonitor/download/'.$files[0]['file_upload_name']?>" target="_blank"> Download Document</a>
            </li>
            
		<?php } ?>
          </ul>
        </div>    


                                    </td>

                                    </tr>



								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        <?php /*?></div><?php */?>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-list-ul" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost items for <?php echo $value['name']?></p>
								<?php }

								?>

                                                                                
                                        
                                        
                                        
                                        
                                        </td>
                                    </tr>

								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        <?php /*?></div><?php */?>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>

                                            <p class="text-center hidden" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-usd" style="font-size: 90px"></i></p>
                                            <p class="text-center" style="color: #ccc; margin-top: 60px;">No cost category</p>

								<?php }

								?>







    
	<?php
    } else{
	?>
        	
        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-history" style="font-size: 90px"></i></p>
        <p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall</p>
	<?php	
	}
?>
    
    
               </div>
    
    
    
                                </div>
                                <!-- end of recall_cost tab-->
                                
								<?php } ?>
                                
                                
                                
                                
    <?php
	//variables, start of continuity questions/tasks
	$stepscount = 1;
    $rpack_step_no = 2;
	$rpack_sort_no = 0;
    foreach($recallsteps as $rst=>$rstep){ //loop step category
		//set step no via steps
		$rpack_step_no = $rstep['step_no'];
		$rpack_sort_no = $rstep['order'];

		//disect each step
		${'questions' . $rpack_step_no} = $this->master_model->getRecords($db_name,
			array('recall_id'=>$rc['id'],'step_no'=>$rpack_step_no),'*',
			array('category_id'=>'ASC', 'id'=>'DESC'));
		
		
		//total question
		${'totq' . $rpack_step_no} = count(${'questions' . $rpack_step_no});
		
		
		
		//check if each step is completed starts in step 3
		${'step' . $rpack_step_no . '_label'} = 0;
		
		foreach(${'questions' . $rpack_step_no} as $qu=>$qus){
			if($qus['status'] == '0'){
				${'step' . $rpack_step_no . '_label'}++;
			}
		}
		
		//steps label variables
		 if (${'step' . $rpack_step_no . '_label'} == '0'){
			 ${'step' . $rpack_step_no . 'color'} = 'text-green bg-success';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-check-circle fa-fw"></i>';
		} 
		 else if (count(${'questions' . $rpack_step_no}) == ${'step' . $rpack_step_no . '_label'} ){
			 ${'step' . $rpack_step_no . 'color'} = 'text-red bg-danger';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-circle-o fa-fw"></i>';
		} 
		 else{
			 ${'step' . $rpack_step_no . 'color'} = 'text-orange bg-warning';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-minus-circle fa-fw"></i>';
		 }
		

?>                                
                                
                                
                                <div class="tab-pane <?php if($rpack_step_no  == 2 && strpos($the_active_module[0]['active_module'], '0') === false) { echo 'steptab_active'; }?>" id="step<?php echo $rpack_step_no; ?>-<?php echo $rc['id'] ?>">
                        
                        


    <h3><?php echo $rstep['name']; ?>
    
    
    <span id="proc<?php echo $rpack_step_no; ?>" <?php if (${'step' . $rpack_step_no . '_label'} != 0){ echo 'style="display: none;"'; }?>>
        <i class="fa fa-check-circle text-green tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Completed"></i> 
       <span class="<?php if (($rpack_step_no-1) == count($recallsteps)) { echo 'hidden'; } ?>"> <a data-toggle="tab" href="#step<?php echo $rpack_step_no + 1; ?>-<?php echo $rc['id'] ?>" class="small" style="text-decoration: none;" onclick="$('#step_<?php echo $rpack_step_no + 1; ?>btn').click();$('#step_<?php echo $rpack_step_no + 1; ?>btn').addClass('active');$('#step_<?php echo $rpack_step_no + 1; ?>btn').siblings('li').removeClass('active');" >Proceed to Step <?php echo $rpack_step_no; ?> <i class="fa fa-arrow-right"></i></a></span>
        </span>
        
        

        <!-- Single button -->
        <div class="btn-group pull-right" style="margin-right: 15px;">
          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 60%; min-width: 82px; right: 0; left: auto;">
            <li class="active"><a href="javascript:;" class="all_tasks_btn<?php echo $rpack_step_no; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','','.all_tasks_btn<?php echo $rpack_step_no; ?>');">All Tasks</a></li>
            <li><a href="javascript:;" class="not_assigned_btn<?php echo $rpack_step_no; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','Not Assigned','.not_assigned_btn<?php echo $rpack_step_no; ?>');">Not Assigned</a></li>
            
            
            
            <?php 
			
                    if(count($all_crt)>0){
            			$crt_cawnt = 1;
                        foreach($all_crt as $r => $value){

							$mycrtname = $value['full_name'];
							?>
							
							
							<li><a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?><?php echo $crt_cawnt; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','<?php echo $mycrtname ?>','.crt_tasks_btn<?php echo $rpack_step_no; ?><?php echo $crt_cawnt ?>');">
							
							<?php echo $mycrtname; ?>
							
							
							</a></li>
							
							<?php
							$crt_cawnt++;

						}
					
					}
			
			?>
            
            <li class="divider"></li>
            <li>
            	<a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?>inprog" onclick="filterTasks('<?php echo $rpack_step_no; ?>','In Progress','.crt_tasks_btn<?php echo $rpack_step_no; ?>inprog');">
                	In Progress
                </a>
            </li>
            <li>
            	<a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?>awaiting" onclick="filterTasks('<?php echo $rpack_step_no; ?>','Awaiting Approval','.crt_tasks_btn<?php echo $rpack_step_no; ?>awaiting');">
                	Awaiting Approval
                </a>
            </li>
            <li>
            	<a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?>comp" onclick="filterTasks('<?php echo $rpack_step_no; ?>','Completed_yes','.crt_tasks_btn<?php echo $rpack_step_no; ?>comp');">
                	Completed
                </a>
            </li>
          </ul>
        </div>  
        
        
         
        <?php 
			if(($recall[0]['initiation_type'] == '0' && strpos($the_active_module[0]['active_module'], 'a') !== false) && ($rpack_step_no > 2)){
		?> 
        <button class="btn btn-primary btn-sm pull-right simulation_btn <?php if(${'step' . $rpack_step_no . '_label'} > 0) { echo 'hidden'; }?>" data-sid="<?php echo $theorg[0]['simulation_id'] ?>" data-id="<?php echo $recall[0]['id'] ?>" data-step="<?php echo $rpack_step_no; ?>" style="margin-right: 5px;">View Step Simulation</button>
        <?php
			}
		?>
        
        
    </h3>
                        
                             
                             
      <table class="table forum table-striped table-list-search" id="recall-step-table<?php echo $rpack_step_no; ?>">
        <thead>
          <tr>
            <th>
                <div class="col-sm-12 hidden">
                    <input type="text" class="form-control" id="recall-step-filter<?php echo $rpack_step_no; ?>" data-action="filter" data-filters="#recall-step-table<?php echo $rpack_step_no; ?>" placeholder="Filter Tasks"/>
                </div>
            </th>
            <th class="cell-stat-1-point-5x text-right hidden-xs hidden-sm hidden-md"></th>
            <th class="cell-stat-1-n-half text-center hidden-xs"></th>
          </tr>
        </thead>
        <tbody>
                                 
            <tr <?php if ($rpack_step_no != '2') { echo 'class="hidden"'; }?>>
                <td>
                  <h4 class="text-normal">
                    Date of incident<br><small><?php echo $recall[0]['incident_date']; ?></small></h4>
                </td>
                <td class="text-right"></td>
                <td class="text-center">
                    <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                </td>
          </tr>
          <tr <?php if ($rpack_step_no != '2') { echo 'class="hidden"'; }?>>
            <td>
              <h4 class="text-normal">
                Affected sites/locations<br><small>
				<?php 
				
				 $locs = json_decode($recall[0]['affected_location']);
					$loc_c = 1;
				if(count($locs) > 0){
					 foreach($locs[0] as $loc){
						 
						 if($loc !=''){
							 echo $loc_c.'. '.$loc.'<br />';
							 $loc_c++;
						 }
					 }
				}else{
					echo 'No affected location.';
				}

				 ?>
                
                </small></h4>
            </td>
            
            <td class="text-right"></td>
            <td class="text-center">
                <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
            </td>
          </tr>
          
                        
<?php
	$countqq = 0;
	$curr_sub_c = '';
	foreach(${'questions' . $rpack_step_no} as $q=>$kyu){
?>	


<?php
	$the_approvers = unserialize($kyu['approvers']);
	$the_approvers_status = unserialize($kyu['approvers_status']);
	$approvers_count = 0;
	$approvers_status = 0;
	
	$theapprovers = array();
	
	$crt_assigned = $kyu['assigned'];
	$crt_date_assigned = ' ('.date_format(date_create($kyu['date_assigned']), 'jS F Y, g:ia').')';
	$crt_approve_btn = '';
	
	$crt_assigned_match = 'yes';
	
//	if($crt_assigned == $this->session->userdata('logged_cc_login_id')){
//		$crt_assigned_match = 'yes';
//	}
	
	$crt_view_approvers_btn = '<li class="crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'" style="cursor: pointer;"><a>Assign Approver(s)</a></li>';
	
	$add_issue_btn = '<li class="add_issue_btn" id="raise_btn'.$kyu['id'].'" data-id="'.$kyu['id'].'" style="cursor: pointer;"><a>Raise an Issue</a></li>';
	
	if(is_array($the_approvers)){
		$approvers_count = count($the_approvers);
	}
	else{
		$the_approvers = array();
	}
	
	if(is_array($the_approvers_status)){
		$approvers_status = count($the_approvers_status);
	}
	else{
		$the_approvers_status = array();
	}
	
	
	//get approvers list

	$approvers_name = array();
	$unapproved_count = count($the_approvers);
	
	if(count($the_approvers) > 0){
		foreach($the_approvers as $tapp){
			$user_approved = '';
			if(in_array($tapp, $the_approvers_status)){
				$user_approved = ' - Approved';
				$unapproved_count--;
			}
			
			$approvers_name[] = array(
				'name'=>$this->common_model->getname($tapp),
				'status'=>$user_approved
			); 

		}
	}
	
	$theapprovers = $approvers_name;
	$curr_user_is_approver = '';
	
	
	
	if($unapproved_count > 0){ //$approvers_count != $approvers_status){
		$status_text = 'Awaiting Approval';
		$status_class = 'text-warning';
	
		
	
		//array_diff — Computes the difference of arrays
		$result = array_diff($the_approvers, $the_approvers_status);
		
		if(count($result) > 0){
			$new_arr = array();
			foreach($result as $rs){
				$new_arr[] = $rs;
			}

			$crt_assigned = $new_arr[0];
			$crt_date_assigned = '';
			
			if($crt_assigned == $this->session->userdata('logged_cc_login_id')){
				$crt_approve_btn = '<button class="btn btn-success btn-xs crt_approve_btn" data-id="'.$kyu['id'].'"><i class="fa fa-thumbs-o-up"></i> Approve</button>';
				$curr_user_is_approver = 'curr_user_is_approver';
				
			}
			//$crt_view_approvers_btn = '<button class="btn btn-primary btn-xs crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'"><i class="fa fa-eye"></i> View Approvers</button>';
		}
		
	}
	
	else if ($kyu['status'] == '0'){
		$status_text = 'In Progress';
		$status_class = 'text-danger';
	}
	
	else{
		$status_text = 'Completed';
		$status_class = 'text-success';
	}
		
?>     
			
            
            <?php
			//if step 1 = $rpack_step_no = 2
			if ($rpack_step_no == '2') {?>
            <tr>
                <td>
                  <h4 class="text-normal">
                    <?php echo $kyu['question']; ?><br><small><?php echo $kyu['answer']; ?></small></h4>
                </td>
                <td class="text-right"></td>
                <td class="text-center">
                    <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                </td>
                
          </tr>
                
            
            <?php }  ?>
    
          <?php if ($kyu['category_id'] != 0){

			$tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$kyu['category_id']));
			if ($curr_sub_c != $tsk_cat[0]['id']){
			?>
            
          <tr class="info hidden">
			<td colspan="3"></td>
          </tr>
          
          <tr class="info hidden-xs">
			<td colspan="3">
            	<h4 style="text-transform:uppercase; font-weight:bold;">
                	<?php echo $tsk_cat[0]['category_name']?>
                </h4>
            </td>
          </tr>
            
          <tr class="info visible-xs">
			<td>
            	<h4 style="text-transform:uppercase; font-weight:bold;">
                	<?php echo $tsk_cat[0]['category_name']?>
                </h4>
            </td>
          </tr>
            
		  <?php } } ?>

          <tr <?php if ($rpack_step_no == '2') { echo 'class="hidden"';}?>>
          
            <td>
            
            

				<h4>
                
			  <?php
			if($kyu['question'] !='Hazard Analysis'){ //$countqq != 0){
			  echo $kyu['question']; } ?>
              
				  <?php if ($kyu['question'] =='Hazard Analysis'){ ?>
                  
                  <small><button class="btn btn-primary btn-sm save_stp3_table <?php if ($kyu['status'] != '0') {echo 'hidden'; } else if($kyu['status'] != '0'){echo 'disabled'; }?>" style="display: none;">Save</button><span class="stp3_remaining_items text-red" style="padding-left: 5px;"></span></small>
                  
				  <?php } ?>
              
              
              <br><small>
              
              
              
              
              	<?php if ($kyu['question'] !='Hazard Analysis'){ //$countqq != 0){ ?>
                

					<?php if ($kyu['answer'] !=''){
						$note_view = '';
					?>
                    	
               
                    <?php } else if($kyu['answer'] =='' && $kyu['status'] == '0'){
						 $note_view = 'style="display: none;"';
					?>
                        <span id="tsk_not_strted_holder">Task not yet started</span>
                    <?php } else {
						 $note_view = 'style="display: none;"';
					?>
                        <span id="tsk_not_strted_holder">No notes added</span>
                    <?php } ?>
                        <a class="btn btn-default btn-xs" href="#" id="show_rc_respond_btn" <?php echo $note_view; ?> data-toggle="modal" data-target="#recallViewRespond" onclick="getRCTaskRespond('<?php echo $kyu['id']; ?>');">View task notes</a><br>
                    
                
                	<?php
						//get task issues
						$issues = $this->master_model->getRecords('cf_recall_blockers', array($recall_field_name=>$recall[0]['id'], 'task_id'=>$kyu['id']));
						
						$issue_btn = 'style="display: none;"';
						$issue_text = 'Issue';
						if(count($issues) > 0 && $kyu['status'] == '0'){
							$issue_btn = '';
						}
						if(count($issues) > 1){
							$issue_text = 'Issues';
						}
						
					?>
              		<span class="span_issue hidden" <?php echo $issue_btn; ?>><a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-default btn-xs view_issue_btn">View <?php echo count($issues).' '.$issue_text ?></a><br /></span>

                <?php } //check if question 1 
				
				else{
					
					//get value of the table asnwers
					if ($kyu['answer'] != ''){
						$s3_assessment = explode(' ',$kyu['answer']);
					}
					else{
						$s3_assessment = array('','','','','','');
					}
					//show on top assigned in step 3 table
                    if ($crt_assigned != ''){
						   echo  'Assigned to: <span class="label orange" style="border-radius: 15px;">'.$this->common_model->getname($crt_assigned).'</span>'.$crt_date_assigned.' '.$crt_approve_btn;
					   }
					   else{
							echo 'Assigned to: <span class="label danger" style="border-radius: 15px;">Not Assigned</span> '.$crt_approve_btn;
					   }

					if(count($theapprovers) > 0){

						echo '<p class="text-muted" style="margin-top: 10px;">Assigned Approver(s)</p>';
						echo '<ol class="text-muted">';
						foreach($theapprovers as $r=>$thapp){
							echo '<li>'.$thapp['name'].''.$thapp['status'].'</li>';
						}
						
						echo '</ol>';
					}
					
					
					 ?>
                
                  		</small></h4>
                        <div class="visible-md visible-lg">
                            <div id="task_guidance_h<?php echo $kyu['id'] ?>-lg" style="display: none;">
                                <span  class="thumbnail" >
                                <?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide']); ?>
                                </span>
                            </div>
                        </div>
                    
                



                        <div>
                        
                
                
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Severity</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[0] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="hidden" name="step_3_table_id" value="<?php echo $kyu['id']?>"/>
                            	<input type="radio" name="severity" class="input_hidden" value="Minor" <?php if ($s3_assessment[0] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[0] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Moderate" <?php if ($s3_assessment[0] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[0] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Major" <?php if ($s3_assessment[0] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[0] == 'Serious') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Serious" <?php if ($s3_assessment[0] == 'Serious') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Serious</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Presence</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[1] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Rare" <?php if ($s3_assessment[1] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[1] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Some" <?php if ($s3_assessment[1] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[1] == 'Most') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Most" <?php if ($s3_assessment[1] == 'Most') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Most</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[1] == 'All') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="All" <?php if ($s3_assessment[1] == 'All') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">All</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">
                        
                        

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Likely Injury</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[2] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Rare" <?php if ($s3_assessment[2] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[2] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Some" <?php if ($s3_assessment[2] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[2] == 'Many') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Many" <?php if ($s3_assessment[2] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[2] == 'Certain') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Certain" <?php if ($s3_assessment[2] == 'Certain') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Certain</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Distribution</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[3] == 'Small') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Small" <?php if ($s3_assessment[3] == 'Small') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Small</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[3] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Some" <?php if ($s3_assessment[3] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[3] == 'Many') { echo 'selected'; }?>">

                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Many" <?php if ($s3_assessment[3] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[3] == 'National') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="National" <?php if ($s3_assessment[3] == 'National') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">National</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Identification</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[4] == 'Easy') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Easy" <?php if ($s3_assessment[4] == 'Easy') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Easy</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[4] == 'Possible') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Possible" <?php if ($s3_assessment[4] == 'Possible') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Possible</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[4] == 'Difficult') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Difficult" <?php if ($s3_assessment[4] == 'Difficult') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Difficult</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[4] == 'Undetectable') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Undetectable" <?php if ($s3_assessment[4] == 'Undetectable') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Undetectable</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Hazard</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[5] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Minor" <?php if ($s3_assessment[5] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[5] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Moderate" <?php if ($s3_assessment[5] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[5] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Major" <?php if ($s3_assessment[5] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[5] == 'Severe') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Severe" <?php if ($s3_assessment[5] == 'Severe') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Severe</span></p>
                            </label>
                          </div>
                        </div>
                        </div>
                        
                
                
                
                
                
                
                
                
                
                <?php } //end of step 3 table
				
					if ($kyu['question'] != 'Hazard Analysis'){
					
						if ($crt_assigned != ''){
						   echo  'Assigned to: <span class="label orange" style="border-radius: 15px;">'.$this->common_model->getname($crt_assigned).'</span>'.$crt_date_assigned.' '.$crt_approve_btn;
					   }
					   else{
							echo 'Assigned to: <span class="label danger" style="border-radius: 15px;">Not Assigned</span> '.$crt_approve_btn;
					   }
						   
						if(count($theapprovers) > 0){
							echo '<p class="text-muted" style="margin-top: 10px;">Assigned Approver(s)</p>';
							echo '<ol class="text-muted">';
							foreach($theapprovers as $r=>$thapp){
								echo '<li>'.$thapp['name'].''.$thapp['status'].'</li>';
							}
							
							echo '</ol>';
						}
					}
                    ?>        
              

              </small></h4>
              
                <div class="<?php if($kyu['id'] == 'Hazard Analysis') { echo 'visible-sm visible-xs'; } ?>">
					<div id="task_guidance_h<?php echo $kyu['id'] ?>" style="display: none;">
                    	<span  class="thumbnail" >
                    	<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide']); ?>
                        </span>
                    </div>
                </div>



              <p class="visible-sm visible-md">
              
                    <a href="#" class="btn btn-sm btn-default" id="assign_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','','<?php echo $kyu['assigned']?>');">
                        <i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign"></i>
                    </a>
    <a href="#" class="btn btn-sm btn-default complete-task-btn <?php if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id') && $kyu['assigned'] != 0) {echo 'disabled'; }?> <?php if($kyu['id'] == 'Hazard Analysis') { echo ' hidden'; } ?>" id="comptask_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');">
                        <i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i>
                    </a>
                    
                    <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>
                
                    <?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                    
                    <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                        Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                    </span>
                    
              </p>
              
              

              
				<div class="clearfix"></div>
     			<div class="visible-xs" style="margin-top: 10px;">
                
    
                    <div class="dropdown pull-left">
                      <button class="btn btn-default btn-sm" data-toggle="dropdown">
                        Action
                        <span class="caret"></span>
                      </button>
                      
                      <ul class="dropdown-menu text-left">
                        <?php
                            if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id')){ 
                                $assign_stat = 'not_assigned';
                            }
                            else{
                                $assign_stat = 'assigned';
                            }
                        ?>
                        <li>
                            <a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" id="inc_tskm<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','1','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                                Mark as Incomplete
                            </a>
                        </li>
                        <li class="<?php echo $curr_user_is_approver; ?>">
                            <a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" id="comp_tskm<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','0','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                                Mark as Completed
                            </a>
                        </li>
                        
                        <?php echo $crt_view_approvers_btn; ?>
                        
                        <?php echo $add_issue_btn; ?>
                      </ul>
                    </div>       
                    <a class="btn bt-link disabled <?php echo $status_class; ?> task_text_status" style="background-color: transparent;">
                        <?php echo $status_text; ?>
                        <?php if ($status_text == 'Completed'){ echo ' <span class="hidden">Completed_yes</span>'; } ?>
                    </a>
                                      
                                        
                </div>
                
                                  

              <p class="visible-xs">
              
                    <a href="#" class="btn btn-sm btn-default" id="assign_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','','<?php echo $kyu['assigned']?>');">
                        <i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign"></i>
                    </a>
                    
<a href="#" class="btn btn-sm btn-default complete-task-btn <?php if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id') && $kyu['assigned'] != 0) {echo 'disabled'; }?> <?php if($kyu['id'] == 'Hazard Analysis') { echo ' hidden'; } ?>" id="comptask_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');">
                        <i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i>
                    </a>
                
              		<a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>
                
                    <?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>

                    <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    	Time to Complete: <span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                    </span>
                    
                    
              </p>
              
              
            </td>

            
            <td class="text-right hidden-xs hidden-sm hidden-md">
            
            		<a href="#" class="btn btn-sm btn-default" id="assign_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','','<?php echo $kyu['assigned']?>');">
                        <i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign"></i>
                    </a>
                    
<a href="#" class="btn btn-sm btn-default complete-task-btn <?php if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id') && $kyu['assigned'] != 0) {echo 'disabled'; }?> <?php if($kyu['id'] == 'Hazard Analysis') { echo ' hidden'; } ?>" id="comptask_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');">
                        <i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i>
                    </a>
                    
              		<a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    <?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
            		
                    
                    <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    	Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                    </span>
            </td>
            
            

            <td class="text-center hidden-xs">
            	
            	<?php /*?>
                    <div class="checkbox checkbox-custom hidden">
                        <input type="checkbox" class="selectedId<?php echo $rc['id'] ?>" name="selectedId<?php echo $rc['id'] ?>[]" id="selectedId<?php echo $kyu['id'] ?>[]" value="<?php echo $kyu['id'] ?>" <?php if ($kyu['status'] == '1'){ echo 'checked="checked"'; }?> onclick="checkTheCBox('<?php echo $this->uri->segment(5) ?>','<?php echo $kyu['id'] ?>','<?php echo $this->uri->segment(4) ?>','selectedId<?php echo $rc['id'] ?>'); return false; ">
                        <label for="selectedId<?php echo $kyu['id'] ?>[]"> </label>
                    </div> 
                    
                <button class="btn btn-default" id="inc_tsk<?php echo $kyu['id']; ?>" style="padding:3px; <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','0','<?php echo ${'step' . $rpack_step_no . '_label'} ?>','step_<?php echo $rpack_step_no?>btn','<?php echo $totq2; ?>','#proc<?php echo $rpack_step_no?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>
                
                <button class="btn btn-default" id="comp_tsk<?php echo $kyu['id']; ?>" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','1','<?php echo ${'step' . $rpack_step_no . '_label'} ?>','step_<?php echo $rpack_step_no?>btn','<?php echo ${'totq' . $rpack_step_no}; ?>','#proc<?php echo $rpack_step_no?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                <?php */?>
                
                <div class="dropdown pull-right">
                  <button class="btn btn-default btn-sm" data-toggle="dropdown">
                    Action
                    <span class="caret"></span>
                  </button>
                  
                  <ul class="dropdown-menu text-right">
                    <?php
						if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id')){ 
							$assign_stat = 'not_assigned';
						}
						else{
							$assign_stat = 'assigned';
						}
					?>
                    <li>
                    	<a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" id="inc_tsk<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','1','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                        	Mark as Incomplete
                        </a>
                    </li>
                    <li class="<?php echo $curr_user_is_approver; ?>">
                    	<a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" id="comp_tsk<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','0','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                        	Mark as Completed
                        </a>
                    </li>
                    
                    <?php echo $crt_view_approvers_btn; ?>
                        
					<?php echo $add_issue_btn; ?>
                  </ul>
                </div>                    
                
                <p class="small pull-right <?php echo $status_class; ?> task_text_status"><?php echo $status_text; ?></p>
            </td>
            
            
            
            
          </tr>
                                   
	   <?php 
		$countqq++;
		$curr_sub_c = $kyu['category_id'];
		}
         ?>

         <?php if ($stepscount == count($recallsteps)) {?>

          <tr>
            <td>
            	
                <h4>
                <span class="orange">Close out incident</span><br><small>&nbsp;</small>
                </h4>
                
                <div class="visible-xs text-center pull-left">
    
                    <button class="btn btn-default" style="margin-top: 10px; padding:3px;" onclick="checkCloseRC('<?php echo $rc['id']?>');">
                        <i class="fa fa-check text-muted" style="font-size: 165%;"></i>
                    </button>                               
                    

                </div>
                
			</td>
            <td class="cell-stat-1-point-5x text-right hidden-xs hidden-sm hidden-md"></td>
            <td class="text-center hidden-xs">
                <button class="btn btn-default" style="padding:3px;" onclick="checkCloseRC('<?php echo $rc['id']?>');">
                	<i class="fa fa-check text-muted" style="font-size: 165%;"></i>
                </button>                               
                
            
            </td>
            
          </tr>     
    
         <?php } ?>
    
          
                   
          <tr <?php if ($rpack_step_no == '2') { echo 'class="hidden"';}?>>
            <td class="hidden-xs" colspan="2">
            	<h4>
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#recallAddTask" onclick="addNewRCTask('<?php echo $rpack_step_no; ?>','<?php echo $rc['id']; ?>')">Add New Task</button>
                </h4>
            </td>
            <td class="visible-xs">
            	<h4>
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#recallAddTask" onclick="addNewRCTask('<?php echo $rpack_step_no; ?>','<?php echo $rc['id']; ?>')">Add New Task</button>
                </h4>
            </td>
            <td class="hidden-xs hidden-sm hidden-md">
            </td>
          </tr>
             
        </tbody>
      </table>        
          
    <div class="well  <?php if ($rpack_step_no == '2') { echo 'hidden';}?>" style="margin:5px; margin-top: 15px;">
        <h3>Forum <small>Most Recent</small>
        </h3>
        <hr />
        <?php
        
            $cc_id=$this->session->userdata('logged_cc_login_id');
            $org_id=$this->session->userdata('cc_selected_orgnaization');
            $scen_idee = 'r'.$rc['id'];
			$forumcat = $stepscount + 3;
            $forum_step_posts = $this->master_model->getRecords('forum_post_master',array('cc_id'=>$cc_id,'org_id'=>$org_id,'scenario_id'=>$scen_idee,'fp_category_id'=>$forumcat),'*',array('date_created'=>'DESC'),'','2');

            if (count($forum_step_posts) > 0){
                
            foreach($forum_step_posts as $sp=>$fsp){		
            ?>

            <p style="padding-bottom: 15px; border-bottom: 1px solid #eee;">
                <b>Posted by <?php echo $my_class->getcrtname($fsp['fp_author_id']).'';?> </b><br /><?php echo $fsp['fp_title'] ?> <a href="<?php echo base_url().'forum/post/'.$scen_idee.'/'.$fsp['fp_id']; ?>">Read more..</a></p>

            <?php }  } else{
				echo '<p class="text-muted">Nothing found..</p><hr>'; } ?>

            <p><a class="btn btn-primary" href="<?php echo base_url().'forum/topics/'.$scen_idee.'/'.$forumcat; ?>">Add New Post</a></p>
    </div>                           


                    
                                    
                                    
                                </div><!--./step 2-->                                
   <?php                             
		$rpack_step_no++;
		$stepscount++;
	} //end loop of step

	?>                            
                                

                                </div>
                            </div>


                        </div>



    
    
                    </div>
                </div>
                                  
    
    

        </div>
        <!-- /.panel -->
        
     </div>
                                    
   </div>                             
                                                
                <?php		
					//end recall pack
					//return false;
				}
				
				
				
			}//end loop recall
		}
	}//end of check recall
			?>

<?php } ?>

<!------START NOTIFY INCIDENT RECALL AND CONTINUITY MODAL SHARE------>
<?php if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>


<?php if (count($recall) > 0) { ?>

       

                     
<!-- add issue -->
<div class="modal fade" id="issueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <form id="issue_form">
            	<div class="form-group">
                	<label>Blocker</label>
                    <input type="text" class="form-control" name="blocker" />
                    <input type="hidden" name="task_id" />
                    <input type="hidden" name="issue_id" />
                    <input type="hidden" name="recall_id" value="<?php echo $recall[0]['id'] ?>" />
                    <input type="hidden" name="incident_type" value="<?php echo $incident_type ?>" />
                    
                </div>
            	<div class="form-group">
                	<label>Issue</label>
                    <textarea rows="5" class="form-control" name="issue"></textarea>
                </div>
            	<div class="form-group">
                	<label>Impact</label>
                    <textarea rows="5" class="form-control" name="impact"></textarea>
                </div>
            	<div class="form-group">
                	<label>Assign</label>
                    <select class="form-control" name="issue_assign">
                    	<option value="">Select</option>
					   <?php
					    echo '<option value="'.$cc_id.'">'.$this->common_model->getname($cc_id).'</option>';
						if(count($all_crt) > 0){
							foreach($all_crt as $r=>$value){
								if($value['login_id'] != $cc_id){
									echo '<option value="'.$value['login_id'].'">'.$value['full_name'].'</option>';
								}
								
							}
						}
						?>
                        
                        
                    </select>
                </div>
            	<div class="form-group">
                	<label>Step no.</label>
                    <select class="form-control" name="step_no">
                    	<option value="">Select</option>
					   <?php
						if(count($recallsteps) > 0){
							foreach($recallsteps as $r=>$value){
								if($value['step_no'] != 2){
									echo '<option value="'.$value['step_no'].'">'.$value['name'].'</option>';
								}
							}
						}
						?>
                        
                        
                    </select>
                </div>
            	<div class="form-group hidden">
                	<label>Task <i class="fa fa-spinner fa-spin"></i></label>
                    <select class="form-control" name="step_task">
                    	<option value=""></option>
                    </select>
                </div>
            	<div class="form-group">
                	<button class="btn btn-primary" disabled="disabled">Submit</button>
                </div>
            </form>
        
      </div>
    </div>
  </div>
</div>   
                     
<!-- viewIssueModal-->
<div class="modal fade" id="viewIssueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">
        	Task Issue
        </h4>
      </div>
      <div class="modal-body">
        
        
      </div>
    </div>
  </div>
</div>   
                   
<!-- assign approver -->
<div class="modal fade" id="viewSimulationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        
        <p class="lead step_simu_text">Step Simulation</p>
        <p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p>
		
        <div class="simulist">
                    
            <div class="panel-group" id="accordion" role="tablist">
                     
            </div>        
        
        </div>
        
        
        
      </div>
    </div>
  </div>
</div>   
       
                     
<!-- assign approver -->
<div class="modal fade" id="assignApproverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
       
       	<p class="loader text-center lead text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p> 
        
        <div class="approver_list">
              <h4 for="step_name">Assigned Approver(s)</h4>
            <ol>
            </ol>

            <button class="btn btn-default update_approvers_btn">Update Approver(s)</button>
        </div>
        <form id="assign_approver_form_crt">
            <!-- Text input-->
            <div class="form-group">
              <h4 for="step_name">
              	Assign Approver(s) 
                <a href="#" class="btn btn-xs btn-primary clone_approver_btn" data-toggle="tooltip" title="Add Approver"><i class="fa fa-plus"></i></a>
              </h4>  
              <input type="hidden" class="standbytask_id" name="standbytask_id" value="" />
              <input type="hidden" class="approvers_id" name="approvers_id" value="" />
            </div>
            
            
            <div class="form-group clone_this_group">
              <label>Approver <span>1</span></label>
                <select name="approvers[]" class="form-control">
                    <option value="">Select..</option>
                    
				   <?php
                    if(count($all_crt) > 0){
                        foreach($all_crt as $r=>$value){

                            ?>
    
                        <option value="<?php echo $value['login_id'] ?>">
							<?php echo $value['full_name'] ?>
                        </option>
    
    
                            
                        <?php

                        }
                    }
                   ?>               
                
                </select>     
            </div>
            
            <div class="clones_holder"></div>
            
            
            <div class="form-group">
            	
                <button type="submit" class="btn btn-primary" disabled="disabled">Save changes</button>
                <a class="btn btn-default" data-dismiss="modal">Cancel</a>
            </div>
            
        </form>
      </div>
    </div>
  </div>
</div>   
       
       
       
<!-- Modal -->
<div class="modal fade" id="verifyCloseIncident" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <form id="close_inci_form" data-id="<?php echo $recall[0]['id']; ?>">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            
        	<div class="form-group close_for_incomplete">
                <label>Please enter CLOSEOUT in the field below.</label>
            	<input type="text" class="form-control" placeholder="CLOSEOUT" />
                <span class="form-helper text-red"></span>
            </div>
        
        	<div class="form-group close_for_complete">
                <p class="lead">Please confirm you wish to close recall incident.</p>
            </div>
        
        	<div class="form-group">
                <button class="btn btn-primary" disabled="disabled">Close incident</button>
                <a type="button" class="btn btn-default" data-dismiss="modal">Cancel</a>
            </div>
        
        </form>
        
        <p class="lead" style="display: none;">
            Do you want to extract all incident data from CrisisFlo's database and store on a CSV file?<br /><br />
            <button class="btn btn-primary save_csv_btn">OK</button>
            <button class="btn btn-default cancel_csv_btn">Cancel</button>
		</p>

      </div>
    </div>
  </div>
</div>    


<!-- Modal -->
<div class="modal fade" id="refreshPageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p class="lead"><i class="fa fa-info-circle fa-fw text-info"></i> A team member has made an update since your last refresh. Please click 'OK' now to refresh your page to view the update before proceeding.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default hidden" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="reloadForUpdate();">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Incident</h4>
        <ul class="list-group list-default">
            <?php if(count($open_recall) > 0){
                $count_open = 0;
                $uri_5 = $this->uri->segment(5);
                foreach($open_recall as $open=>$opeen){
            ?>
            
            <a href="<?php echo base_url().'cc/dashboard/index/r/'.$count_open ?>" class="list-group-item <?php if ($uri_5 == $count_open) { echo 'active'; }?>"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
            
            <?php
                $count_open++;	
                } 
            } ?>
        </ul>


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myAddPinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">New Marker</h4>
      </div>
      <div class="modal-body">
      		<form id="nm_form">
            	<div class="form-group">
                	<label>Location</label>
                    <input type="text" class="form-control nm_location" name="nm_location" id="nm_location" />
                    <input type="hidden" name="nm_recall_id" id="nm_recall_id" value="<?php echo $recall[0]['id']?>"/>
                    <input type="hidden" name="lat" id="lat" value=""/>
                    <input type="hidden" name="lng" id="lng" value=""/>
                    <input type="hidden" name="nm_id" id="nm_id" value=""/>
                    <span class="control-helper text-red hidden">Mandatory field.</span>
                </div>
            	<div class="form-group">
                	<label>Comment</label>
                    <textarea class="form-control" rows="5" name="nm_comment" id="nm_comment"></textarea>
                    <span class="control-helper text-red hidden">Mandatory field.</span>
                </div>
            	<div class="form-group">
                	<button class="btn btn-primary">Save</button>
                </div>
            </form>
      </div>
    </div>
  </div>
</div>
      
<!-- Modal -->
<div class="modal fade" id="myMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Situation Map</h4>
      </div>
      <div class="modal-body">
      	<div id="map-canvas" style="min-height: 512px; width: 100%;"></div>
      	<div id="map-legend" style="padding-top: 15px;">
        	<p>
            
                <span class=""><img src="<?php echo base_url().'assets/img/map-marker-blue.png' ?>" style="height: 25px;"/> <span class="bluemark-count"></span></span>
                
                <span class="" style="margin-left: 15px;"><img src="<?php echo base_url().'assets/img/map-marker-red.png' ?>" style="height: 25px;"/> <span class="redmark-count"></span></span>
                
                <span class="" style="margin-left: 15px;"><img src="<?php echo base_url().'assets/img/google_purple_icon_sm.png' ?>" style="height: 25px;"/> <span class="yellow-count"></span></span>
                
                <button class="btn btn-sm btn-primary pull-right" onclick="$('#nm_form').find('input#nm_location,input#nm_id, textarea').val('');$('.modal').modal('hide');$('#myAddPinModal').modal('show');">Add New Marker</button>
                
            </p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="updateDefaultCurrency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Default Currency</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="default_currency_btn">Save changes</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="viewCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Item Cost Details</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="addCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="upload_sect col-lg-12" style="display:none; position: absolute; top: 260px; padding-left: 5px; z-index: 1500;">
        	<div class="col-xs-6">
            <form id="myForm2" action="<?php echo base_url().'cc/costmonitor/upload' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm2').submit();">
                 <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>    
             </div>
        	<div class="col-xs-6">
                <div id="progress2" style="display: none;">
                    <div id="bar2"></div>
                    <div id="percent2">0%</div >
                </div>
             </div>
		</div>
                                                        
    <form>
            
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Item</h4>

            
            <fieldset>
        	<br />
            <!-- Text input-->
            <div class="form-group">
              <input id="item_name" name="item_name" type="text" placeholder="Item Name" class="form-control input-md">
              <input id="cost_id" name="cost_id" type="hidden" value="">
              <input id="cost_item_id" name="cost_item_id" type="hidden" value="">
             <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
              <span class="help-block text-red item_name_error"></span>  

            </div>
            
            <!-- select input-->
            <div class="form-group">


                <select name="currency_code" id="currency_code" class="form-control">
                  <option value="">Select Currency</option>
                  <?php
				  	if(count($currency_code) > 0){
						foreach($currency_code as $cc=>$curcode){
							echo '<option value="'.$curcode['idCountry'].'"';
							if($recall[0]['default_currency_id'] == $curcode['idCountry']){
								echo ' selected="selected"';
							}
							echo '>'.$curcode['countryName'].' ('.$curcode['currencyCode'].')</option>';
						}
					
					
					}
				  
				  ?>
                </select>
               <span class="help-block text-red currency_code_error"></span>  

            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <input id="item_cost" name="item_cost" type="text" placeholder="Item Cost" class="form-control input-md">
              <span class="help-block text-red item_cost_error"></span>  

            </div>

            <!-- Text input-->
            <div class="form-group">
            
                <div class='input-group date' id='datetimepicker5'>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type='text' class="form-control" id="inci_date" name="inci_date" data-date-format="DD-MMM-YYYY" placeholder="Invoice Date"/>
                    
               </div>
              <span class="help-block text-red item_invoice_error"></span>  

            </div>
            
            <div class="form-group">

                <a href="javascript:;" onclick="$('.upload_sect').show(); $(this).after('&nbsp;').hide();" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>

  
            </div>

            <div class="form-group">

                <div class="col-xs-12">

                    <div id="file_up_text2" style="display: none;">File Attached: <div id="message2"><a href=""></a></div></div>

                </div>

            </div>


 
            
            </fieldset>
            


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="cost_item_btn" name="cost_item_btn" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>


	<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Incident</h4>
        <table class="table table-hover">
        <?php
			if(count($recall)> 0){
				foreach($recall as $r=>$call){ ?>
                
                  <tr>
                  	<td>
                    	<a href="<?php echo base_url().'cc/costmonitor/index/'.$call['id']; ?>"><?php echo $call['incident_no'] ?> : <?php echo $call['incident_name'] ?></a>
                    </td>
                  </tr>
			<?php }
			}else{
				echo '<tr><td class="text-center text-muted">No recall incidents.</td> </tr>';
			}
			
		?>
                </table>					


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewCategorySummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category Summary</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="viewAccumulative" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Accumulative Cost</h4>
      </div>
      <div class="modal-body">
        <div class="well" style="border-radius: 0px">
			<?php if(count($recall)> 0){
				echo $recall_accumulate;
			} ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="addCostCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form>
            
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category</h4>

            
            <fieldset>
        	<br />
            <!-- Text input-->
            <div class="form-group">
              <input id="name_cat" name="name_cat" type="text" placeholder="Category Name" class="form-control input-md">
              <input id="cat_id" name="cat_id" type="hidden" value="">
              <input id="recall_id" name="recall_id" type="hidden" value="<?php echo $recall[0]['id']?>">
              <span class="help-block text-red name_cat_error"></span>  

            </div>

            
            </fieldset>
            


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="name_cat_btn" name="name_cat_btn" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>
<!--end of cost monitor modals-->

    <!-- Modal for add respond cc-->
    <div class="modal fade" id="recallRespond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <p class="lead">Task Notes <i class="fa fa-question-circle text-info small" style="cursor: pointer;" onclick="$('#modal_guide_holder').slideToggle();" id="modal_guide_btn"></i></p>
			
            <div id="modal_guide_holder" style="display: none;">
                <div class="well" style="#fff;"></div>	
            </div>
            
            <div class="success_holder_mess"></div>
            
            <textarea class="form-control" id="txt_message" name="txt_message" placeholder="Enter task notes" rows="15"></textarea>
			<div class="text_error text-red"></div>	
            

            <div style="padding-top: 15px;">

                <a id="atta_file_btn" href="javascript:;" onclick="$('.upload_section').show(); $(this).after('&nbsp;').hide();" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>

            </div>
            
           
             <div class="upload_section" style="display: none;">
             	<?php $uniquetime= time().mt_rand(); ?>

                 <div class="col-xs-6" style="padding-top: 0">
                    <form id="myForm" action="<?php echo base_url().'cc/recall/upload' ?>" method="post" enctype="multipart/form-data">
                         <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                         <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                         <input type="hidden" id="the_question_id" name="the_question_id" value="">
                         <input type="submit" class="hidden" value="Ajax File Upload">
                     </form>                                                   
                </div>
                 
                 <div class="col-xs-6" style="padding-top: 0">
                    <div id="progress" style="display: none;">
                        <div id="bar"></div>
                        <div id="percent">0%</div >
                    </div>
                </div>

                <div class="col-xs-12">
    
                    <div id="file_up_text" style="display: none;"><p>File Attached: <div id="message"><a href=""></a></div></p></div>
    
                </div>

                                                                   
            </div>
            
            
          </div><div style="clear: both"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" id="save_message_rc">Save changes</button>
          </div>
        </div>
      </div>
    </div>




    <!-- Modal for add new task-->
    <div class="modal fade" id="recallAddTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <p class="lead">Add New Task</p>

            <textarea class="form-control" id="new_tsk_name" name="new_tsk_name" placeholder="Task" rows="2"></textarea>
			<div class="new_tsk_name_err text-red"></div>	<br />
            
            
            <textarea class="form-control" id="new_tsk_guide" name="new_tsk_guide" placeholder="Task Guidance" rows="4"></textarea>
            
			<br />
            <div id="sub_cat"></div>
            
           <input type="hidden" id="newtsk_step_no" value="" />
            
           <input type="hidden" id="newtsk_recall_id" value="" />
            
            
            
          </div><div style="clear: both"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" id="save_new_rc_tsk">Save changes</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal for viewing task respond-->
    <div class="modal fade" id="recallViewRespond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<div id="respondtask_holder">
            </div>
			

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>



    <!-- Modal for blockerNote-->
    <div class="modal fade" id="blockerNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <div class="modal-header">
            <h4>Notes
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </h4>
          </div>
          <div class="modal-body">
                <h2 class="text-center text-muted block_load_holder" style="display: none;"><i class="fa fa-spinner fa-spin"></i></h2>
                <div class="well blocker_note_holder" style="color: #333; border-radius: 0; display: none;"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>                


        <!-- Modal for resolveBlocker-->
        <div class="modal fade" id="resolveBlocker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="form-group">
						<label for="blocker_note">Add notes</label>
                        <h2 class="text-center text-muted block_load_holder" style="display: none;"><i class="fa fa-spinner fa-spin"></i></h2>
                        <textarea class="form-control blocker_note_holder" id="blocker_note" name="blocker_note" rows="5"></textarea>
                        <input type="hidden" id="the_blocker_id" value="" />
                        <span class="text-red blocker_note_error"></span>
                	</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="save_blocker_note">Save changes</button>
              </div>
            </div>
          </div>
        </div>                



        <!-- Modal for assign crt recall-->
        <div class="modal fade" id="recallModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <p class="lead">Assign Task</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

					<div class="success_holder"></div>

                    <div class="form-group">

                         <input type="hidden" id="the_recall_id" name="the_recall_id" value="">
                         <input type="hidden" id="the_current_assigned" name="the_current_assigned" value="">
                        <?php /*?> <input type="hidden" id="the_question_id" name="the_question_id" value=""><?php */?>
                        
                        
                        
                            <?php
                               if(count($all_crt) > 0){
								   $not_available = '';
                                    foreach($all_crt as $mt=>$team){
								
										$availability_status = $team['availability_status'];
										$availability_class = 'label-danger';
										$availability_visible = 'hidden';
										
										if($availability_status == 'Available'){
											$availability_class = 'label-success';
										}

										if (strpos($the_active_module[0]['active_module'], 'b') !== false){
											$availability_visible = '';
										}
											
																
										
										if($availability_status == 'Available'){
                            ?>                        

                            <div class="radio">
                                <input type="radio" name="message_receiver" id="message_receiver<?php echo $team['login_id']?>" value="<?php echo $team['login_id'] ?>">
                                <label for="message_receiver<?php echo $team['login_id']?>">
                                    <?php echo $team['full_name']; ?> <span class="label <?php echo $availability_class. ' '.$availability_visible?>"><?php echo $availability_status ?></span>
                                </label>
                            </div>
                        
                            <?php
										}
										else{
										
											$not_available .= '
							<div class="radio">
                                <input type="radio" name="message_receiver" id="message_receiver'.$team['login_id'].'" value="'.$team['login_id'].'">
                                <label for="message_receiver'.$team['login_id'].'">
                                    '.$team['full_name']. //.' <span class="label '.$availability_class. ' '.$availability_visible.'">'.$availability_status.'</span>
                                '</label>
                            </div>											
											';
										
										}
									}//end foreach
									echo $not_available;
							   }
                            ?>                        
                        
                        
                        
                            <select class="form-control hidden" name="message_receiverx" id="message_receiverx">

                            <option value=""> Select </option>

                            <?php

                               if(count($all_crt) > 0)

                                {

                                    foreach($all_crt as $mt=>$team){

                                    

                            ?>

                                    <option value="<?php echo $team['login_id'] ?>" <?php if($this->uri->segment(4)!="" && base64_decode($this->uri->segment(4))==$team['login_id']){echo 'selected="selected"';} ?>>

                                    <?php echo $team['full_name']; ?>

                                    </option>

                            <?php

                                    }

                                }

                            ?>

                           </select>							
							<div class="err_holder text-red"></div>

                    </div>

    
    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="assign_recall">Save changes</button>
              </div>
            </div>
          </div>
        </div>                

         
<?php }
}//end of modal share for continuity and recall?>


<!-- Modal for individual sms log -->
<div class="modal fade" id="mySmsReportLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">SMS Usage Report</h4>
      </div>
      <div class="modal-body">
        <?php
			$sms_log=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0','log_scenario_id'=>$this->uri->segment(4)));
			
			$failed_sms_log=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status !='=>'0','log_scenario_id'=>$this->uri->segment(4)));
			?>
            
            <!-- Nav tabs category -->
            <ul class="nav nav-tabs faq-cat-tabs" style="margin-bottom: 10px;">
                <li class="active"><a href="#faq-cat-11" data-toggle="tab">Sent</a></li>
                <li><a href="#faq-cat-22" data-toggle="tab">Failed</a></li>
            </ul>


<!-- Tab panes -->
            <div class="tab-content faq-cat-content">
                <div class="tab-pane active in fade" id="faq-cat-11">

					<?php
                    if (count($sms_log) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="example-table"> <thead>
             <tr> <th width="40%">Recipient</th> <th width="29%">Time Sent</th> <th width="30%">Price</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($sms_log as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. date('d M Y - H:i:s',strtotime($value['date_sent'])) .'</td>';
                            echo '<td>US$ '. $value['price'] .'</td></tr>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>

                </div>
                
                
                <!--second tab-->
                <div class="tab-pane fade" id="faq-cat-22">

					<?php
                    if (count($failed_sms_log) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="stake-table"> <thead>
             <tr> <th width="50%">Recipient</th> <th width="50%">Status</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($failed_sms_log as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. $value['status'] .'</td>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>
    


                </div>
            </div>


		
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
<!--./Modal for individual sms log -->

<?php 

if(($main_status=="Pre-Incident Phase" && strpos($the_active_module[0]['active_module'], '1') !== false) || (count($recall) == 0 && strpos($the_active_module[0]['active_module'], '5') !== false) || (count($recall) == 0 && strpos($the_active_module[0]['active_module'], '8') !== false)|| (count($casereport) == 0  && strpos($the_active_module[0]['active_module'], '2') !== false)){ ?>



<?php if( $num_group==0 || $num_crt==0 || $num_stk==0 || $num_plan==0 || count($cc_document)==0 || $num_standby_message==0 || $num_reminder==0){ ?>

	<?php /*GET THE DATA FROM THE SESSION**************************/
		
        $session_userfname  = $this->session->userdata('logged_display_name');


    /*********************************************************/?>



<div class="col-md-12 welcomeScreen">

    <div class="panel panel-default col-md-6 col-md-offset-3 text-center">
    
      <div class="panel-body">
      
        <h2>Hi <?php echo $session_userfname; ?></h2>
		<p class="lead">Welcome to Crisisflo. 
        
        <?php
        if((strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false) && ($num_group==0 || $num_crt==0 || $task_review_date == '0000-00-00 00:00:00' || $num_stk==0 || count($cc_document)==0 || $num_reminder == 0 || $theorg[0]['timezone_set'] == '0' )){
			echo 'The boxes below represents pre-incident activities to be completed.';
			
		}
		
        if($main_status=="Pre-Incident Phase" && strpos($the_active_module[0]['active_module'], '1') !== false){
			echo 'Current status is Pre-Incident Phase.';
		}
		?>
        
        </p>
        
      </div><!--.panel body-->
    </div>
    
</div><!---.welcomeScreen-->


<div style="clear: both;"><hr></div>

	  <?php
	  if (strpos($the_active_module[0]['active_module'], '1') !== false){ 

	  	if($num_group==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile orange">
						<a href="'.base_url().'cc/usergroup/managegroup" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-male fa-fw fa-3x"></i>
							</div>
							Add User Groups
						</a>
			
					</div>
				</div>  
			
			
			';}
            
      ?>
      
         
                
	  <?php if($num_crt==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile green">
						<a href="'.base_url().'cc/crisisteam/managecrt" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-user fa-fw fa-3x"></i>
							</div>
							Add Response Team Members
						</a>
			
					</div>
				</div>   
			
			';} 

      ?>

      <?php if($num_stk==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile green2">
						<a href="'.base_url().'cc/stakeholder/managestk" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-users fa-fw fa-3x"></i>
							</div>
							Add Stakeholders
						</a>
			
					</div>
				</div>   
			
			';} 

      ?>
        
	  <?php if(count($cc_document)==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile red">
						<a href="'.base_url().'cc/document/manage" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-paperclip fa-fw fa-3x"></i>
							</div>
							Upload Documents
						</a>
			
					</div>
				</div>  
			
			
			';} 

      ?>
 
    <?php /*?></div>   
            
    <div class="col-lg-10 col-lg-offset-1"><?php */?>
                
	  <?php if($num_plan == 0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile maroon">
						<a href="'.base_url().'cc/scenario/managetask" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-lightbulb-o fa-fw fa-3x"></i>
							</div>
							Add Response Plan
						</a>
			
					</div>
				</div>   
			
			
			';} 
            
            foreach ($the_scene as $ts=>$tsk){
                if ($tsk['crt_id'] =='0'){
                    echo '
						<div class="col-lg-4 col-sm-6">
							<div class="circle-tile maroon">
								<a href="'.base_url().'cc/scenario/managetask" class="circle-tile-footer">
									<div class="circle-tile-number text-faded">
										<i class="fa fa-lightbulb-o fa-fw fa-3x"></i>
									</div>
									Add Response Plan
								</a>
					
							</div>
						</div>   
					
					
					';
                    break; /* Exit the loop. */
                }
            }

      ?>

      <?php if($num_standby_message == 0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile purple">
						<a href="'.base_url().'cc/standbymessage/manage" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-folder-open fa-fw fa-3x"></i>
							</div>
							Add Standby Message
						</a>
			
					</div>
				</div>   
			
			';} 

      ?>

        
        
	  <?php if($num_reminder == 0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile grey">
						<a href="'.base_url().'cc/reminder" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-bell-o fa-fw fa-3x"></i>
							</div>
							Set Review Date
						</a>
			
					</div>
				</div>   
							
			';} 
			
      ?>
      
      <?php if($theorg[0]['timezone_set'] == '0')

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile purple">
						<a href="'.base_url().'cc/settings/timezone" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-clock-o fa-fw fa-3x"></i>
							</div>
							Set Time Zone
						</a>
			
					</div>
				</div>   
			
			';} 

			
	  }//close standard crisisflo menu
	  
	  
	  
	  
  	  //case management module activated
	  if (strpos($the_active_module[0]['active_module'], '2') !== false && strpos($the_active_module[0]['active_module'], '1') === false && strpos($the_active_module[0]['active_module'], '5') === false ){
		  

	  	if($num_group==0)

            {echo '
				<div class="col-md-4">
					<div class="circle-tile orange">
						<a href="'.base_url().'cc/usergroup/managegroup" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-male fa-fw fa-3x"></i>
							</div>
							Add Group
						</a>
			
					</div>
				</div>  
			
			
			';}
            
      ?>
      
         
                
	  <?php if($num_crt==0)

            {echo '
				<div class="col-md-4">
					<div class="circle-tile green">
						<a href="'.base_url().'cc/crisisteam/managecrt" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-user fa-fw fa-3x"></i>
							</div>
							Add Response Team Members
						</a>
			
					</div>
				</div>   
			
			';} 
      ?>
      
         
      
      <?php if($theorg[0]['timezone_set'] == '0')

            {echo '
				<div class="col-md-4">
					<div class="circle-tile purple">
						<a href="'.base_url().'cc/settings/timezone" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-clock-o fa-fw fa-3x"></i>
							</div>
							Set Time Zone
						</a>
			
					</div>
				</div>   
			
			';} 

	  }//close case management module menu
	  
	  //recall module activated
	  if ((strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false) && strpos($the_active_module[0]['active_module'], '1') === false ){
	  

	  	if($num_group==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile orange">
						<a href="'.base_url().'cc/usergroup/managegroup" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-male fa-fw fa-3x"></i>
							</div>
							Add User Groups
						</a>
			
					</div>
				</div>  
			
			
			';}
            
      ?>
      
         
                
	  <?php if($num_crt==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile green">
						<a href="'.base_url().'cc/crisisteam/managecrt" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-user fa-fw fa-3x"></i>
							</div>
							Add Response Team Members
						</a>
			
					</div>
				</div>   
			
			';} 

      ?>

      <?php if($task_review_date == '0000-00-00 00:00:00')
	  
            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile yellow">
						<a href="'.base_url().'cc/standbytasks" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-list-alt fa-fw fa-3x"></i>
							</div>
							Review Standby Tasks
						</a>
			
					</div>
				</div>   
			
			';} 
			
      ?>

      <?php if($num_stk==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile green2">
						<a href="'.base_url().'cc/stakeholder/managestk" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-users fa-fw fa-3x"></i>
							</div>
							Add Stakeholders
						</a>
			
					</div>
				</div>   
			
			';} 

      ?>
        
	  <?php if(count($cc_document)==0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile red">
						<a href="'.base_url().'cc/document/manage" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-paperclip fa-fw fa-3x"></i>
							</div>
							Upload Documents
						</a>
			
					</div>
				</div>  
			
			
			';} 

      ?>
 
 
        
	  <?php if($num_reminder == 0)

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile grey">
						<a href="'.base_url().'cc/reminder" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-bell-o fa-fw fa-3x"></i>
							</div>
							Set Review Date
						</a>
			
					</div>
				</div>   
							
			';} 	  

      ?>
 
      
      <?php if($theorg[0]['timezone_set'] == '0')

            {echo '
				<div class="col-lg-4 col-sm-6">
					<div class="circle-tile purple">
						<a href="'.base_url().'cc/settings/timezone" class="circle-tile-footer">
							<div class="circle-tile-number text-faded">
								<i class="fa fa-clock-o fa-fw fa-3x"></i>
							</div>
							Set Time Zone
						</a>
			
					</div>
				</div>   
			
			';} 
	  
	  }//close recall module menu

	  
	  ?>
<div style="clear: both;"></div>

<?php } ?>

<!---.col12 initiatedresponse-->

<?php if(count($waiting_crtmem)>0 || count($waiting_stkmem)){ ?>

<div class="col-lg-12">

  <div class="panel panel-default">

    <div class="panel-heading">

      <div class="panel-title">

        <h4>Awaiting acceptance/confirmation</h4>

      </div>

      <div class="clearfix"></div>

    </div>

    <div class="panel-bodyx">

      

      <div id="myTabContent" class="tab-content">

        <div class="tab-pane fade active in" id="home">

          <?php if(count($waiting_crtmem)>0 || count($waiting_stkmem)>0){?>

          <div class="table-responsive">

            <table id="example-table" class="table table-striped table-hover table-green">

              <thead>

                <tr>

                  <th>Name</th>

                  <th>User Type</th>

                </tr>

              </thead>

              <tbody>

                <?php 

				if(count($waiting_crtmem)>0)

				{

					foreach($waiting_crtmem as $r => $value): ?>

               	 	<tr>

                  <td><?php echo $value['full_name']; // echo $this->master_model->decryptIt($value->crt_first_name).' '.$this->master_model->decryptIt($value->crt_last_name);?></td>

                  <td>CRT</td>

                

                </tr>

                <?php 

					endforeach;

				}

				?>

                <?php 

				if(count($waiting_stkmem)>0)

				{

					foreach($waiting_stkmem as $r => $stk): ?>

               	 	<tr>

                  <td><?php echo $this->master_model->decryptIt($stk->stk_first_name).' '.$this->master_model->decryptIt($stk->stk_last_name);?></td>

                  <td>Stakeholder</td>

                  

                </tr>

                <?php 

					endforeach;

				}

				?>

              </tbody>

            </table>

          </div>

          <!-- /.table-responsive -->

          <?php };?>

        </div>

        

        

      </div>

    </div>

    <!-- /.panel-body --> 

  </div>

  <!-- /.panel --> 

  

</div>

<?php } ?>



<?php if(count($waiting_contact_crtmem)>0 || count($waiting_contact_stkmem)){ ?>

<div class="col-lg-12">

  <div class="panel panel-default">

    <div class="panel-heading">

      <div class="panel-title">

        <h4>Awaiting Contact Update Confirmation</h4>

      </div>

      <div class="clearfix"></div>

    </div>

    <div class="panel-bodyx">

      

      <div id="myTabContent" class="tab-content">

        <div class="tab-pane fade active in" id="home">

          <?php if(count($waiting_contact_crtmem)>0 || count($waiting_contact_stkmem)>0){?>

          <div class="table-responsive">

            <table id="example-table" class="table table-striped table-hover table-green">

              <thead>

                <tr>

                  <th>Name</th>

                  <th>User Type</th>

                </tr>

              </thead>

              <tbody>

                <?php 

				if(count($waiting_contact_crtmem)>0)

				{

					foreach($waiting_contact_crtmem as $r => $value): ?>

               	 	<tr>

                  <td><?php echo $this->master_model->decryptIt($value->crt_first_name).' '.$this->master_model->decryptIt($value->crt_last_name);?></td>

                  <td>CRT</td>

                

                </tr>

                <?php 

					endforeach;

				}

				?>

                <?php 

				if(count($waiting_contact_stkmem)>0)

				{

					foreach($waiting_contact_stkmem as $r => $stk): ?>

               	 	<tr>

                  <td><?php echo $this->master_model->decryptIt($stk->stk_first_name).' '. $this->master_model->decryptIt($stk->stk_last_name);?></td>

                  <td>Stakeholder</td>

                  

                </tr>

                <?php 

					endforeach;

				}

				?>

              </tbody>

            </table>

          </div>

          <!-- /.table-responsive -->

          <?php };?>

        </div>

        

        

      </div>

    </div>

    <!-- /.panel-body --> 

  </div>

  <!-- /.panel --> 

  

</div>

<?php } ?>

<?php if(!empty($review_date) && $review_date[0]['reminder_date']!="0000-00-00"){ ?>

<div class="col-lg-12"> 

	<div class="panel panel-default">

    <div class="panel-heading">

      <div class="panel-title">

        <h4>Next Review Date</h4>

      </div>

      <div class="clearfix"></div>

    </div>

    <div class="panel-body">

      	<div id="myTabContent" class="tab-content">

            <div class="tab-pane fade active in" id="home">

              <?php echo date('d M Y',strtotime($review_date[0]['reminder_date']));?>

            </div>

            <div class="clr"></div>

      </div>

    </div>

    <!-- /.panel-body --> 

  </div>

</div>

<?php }





}//end of pre-incident phase

else

{

if (strpos($the_active_module[0]['active_module'], '1') !== false){ //standard crisisflo activated
	

	  if (strpos($the_active_module[0]['active_module'], '5') === false  || strpos($the_active_module[0]['active_module'], '8') === false) {
		  
?>




    <!--initiate response boxes-->
    <div class="col-lg-4 col-sm-6">
        <div class="circle-tile purple">
			<a href="#assigned_task" class="circle-tile-footer">
                <div class="circle-tile-number text-faded">
                    <i class="fa fa-tasks fa-fw fa-3x"></i>
                    
                </div>
                <?php echo count($my_task); ?> Tasks In Progress
            </a>
        </div>
    </div>
    
    <div class="col-lg-4 col-sm-6">
        <div class="circle-tile maroon">
			<a href="#response_init" class="circle-tile-footer">
                <div class="circle-tile-number text-faded">
                    <i class="fa fa-lightbulb-o fa-fw fa-3x"></i>
                    
                </div>
                <?php echo count($initiated_scn); ?> Response Initiated
            </a>
        </div>
    </div>
    
       
    <div class="col-lg-4 col-sm-6">
        <div class="circle-tile blue">
			<a href="<?php echo base_url().'cc/crisisteam/managecrt' ?>" class="circle-tile-footer">
                <div class="circle-tile-number text-faded">
                    <i class="fa fa-user fa-fw fa-3x"></i>
                    
                </div>
                <?php echo count($all_crt); ?> Response Team Members
            </a>
        </div>
    </div>
    
       
    <div class="col-lg-4 col-sm-6">
        <div class="circle-tile green">
			<a href="<?php echo base_url().'cc/stakeholder/managestk' ?>" class="circle-tile-footer">
                <div class="circle-tile-number text-faded">
                    <i class="fa fa-users fa-fw fa-3x"></i>
                    
                </div>
                <?php echo count($cf_stakeholder); ?> Stakeholders
            </a>
        </div>
    </div>
    
       
    <div class="col-lg-4 col-sm-6">
        <div class="circle-tile red">
			<a href="<?php echo base_url().'cc/document/manage' ?>" class="circle-tile-footer">
                <div class="circle-tile-number text-faded">
                    <i class="fa fa-paperclip fa-fw fa-3x"></i>
                    
                </div>
                <?php echo count($cf_file_upload); ?> Recall Documents
            </a>
        </div>
    </div>
    
       
    <div class="col-lg-4 col-sm-6" id="assigned_task">
        <div class="circle-tile purple">
			<a href="<?php echo base_url().'cc/standbymessage/manage' ?>" class="circle-tile-footer">
                <div class="circle-tile-number text-faded">
                    <i class="fa fa-folder-open fa-fw fa-3x"></i>
                    
                </div>
                <?php echo count($standby_messages); ?> Standby Messages
            </a>
        </div>
    </div>
    <!--./initiate response boxes-->
<div style="clear: both;"></div>
    
            
            
            



<?php
	  }//.end hide boxes when recall is present

	if(count($my_task)>0)

	{

?>



	<div class="col-lg-12">

  	<div class="panel panel-default">

    <div class="panel-heading">

      <div class="panel-title">

        <h4>My Assigned Tasks</h4>

      </div>

      	<div class="panel-widgets">

			<a class="" data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

		</div>

      <div class="clearfix"></div>

    </div>

    <div id="validationExamples" class="panel-collapse collapse in">

    <div class="panel-body">

      <div id="myTabContent" class="tab-content">

        <div class="tab-pane fade active in" id="home">

          <?php if(count($my_task)>0){?>

          <div class="table-responsive">

            <table  class="table table-striped table-bordered table-hover table-green myexample-table">

              <thead>

                <tr>

                  <th width="26%">Scenario</th>

                  <th width="26%">Task</th>

                  <th width="18%">Owner</th>

                  <th width="80px">Status</th>

                  <th width="14%">Action</th>

                </tr>

              </thead>

              <tbody>

                <?php 

				if(count($my_task)>0)

				{

					foreach($my_task as $mtask)

					{

						if($mtask['task_status']=='1')

						{ $status = '<i class="fa  text-red"></i> <span class="text-red">In Progress</span>';

						}

						if($mtask['task_status']=='2')

						{ $status = '<i class="fa text-green"></i> <span class="text-green">Completed</span>';

						}

						if($mtask['task_status']=='0')

						{ $status = '<i class="fa  text-red"></i> <span class="text-red">Not Started</span>';

						}

				?>

               	 	<tr>

                    	<td> <?php if ($mtask['initiation_type']==0){echo '<span class="label label-default">Mock</span> - '; } else{ echo '<span class="label label-info">Live</span> - '; }?><?php echo $mtask['scenario']; ?></td>

                        <td><?php echo $mtask['task']; ?></td>

                        <td><?php echo $this->master_model->decryptIt($mtask['crt_first_name']).' '.$this->master_model->decryptIt($mtask['crt_last_name']); ?></td>

                        <td><?php echo $status; ?></td>

                        <td>

                        <?php if($mtask['task_status']=='2'){?>

                        <a class="btn btn-default btn-xs" href="javascript:void(0);">Task Completed</a>

                        <?php }else{ ?>

                        <a class="btn btn-default btn-xs" onclick="return complete_confirm('<?php echo base_url().'cc/dashboard/changestatus/'.$mtask['task_id'];?>');" href="javascript:void(0);">Mark as Completed</a>  

                        <?php } ?>

                        </td>

                    </tr>	

                <?php 

					}

				}

				?>

                

              </tbody>

            </table>

          </div>

          <!-- /.table-responsive -->

          <?php } ?>
		  
		  
        </div>

        

        

      </div>

    </div>

    </div>

    <!-- /.panel-body --> 

  </div>

  <!-- /.panel --> 

</div>

<?php

	}



	/* each initiated scenarion will be display seperatly*/	

	if(count($initiated_scn)>0)

	{

		foreach($initiated_scn as $scenario)

		{

?>
            <div class="col-lg-12"  id="response_init" style="padding-top: 100px; border-top: 1px solid #eee"></div>
            <div class="col-lg-12">
            
              <div class="panel panel-default">

                <div class="panel-heading">

                  <div class="panel-title">

                    <h4>Group Tasks for Scenario: <?php if ($scenario['initiation_type']==0){echo '<span class="label label-default">Mock</span> - '.$scenario['scenario']; } else{ echo '<span class="label label-info">Live</span> - '.$scenario['scenario']; }?></h4>

                  </div>

                  <div class="panel-widgets">
                    <a href="<?php echo base_url().'forum/index/'.$scenario['scenario_id']; ?>" class="btn btn-default btn-xs">
                        Forum
                    </a>
                    <span class="divider"></span>
					<a class="" data-toggle="collapse" data-parent="#accordion_<?php echo $scenario['scenario_id']; ?>" href="#validationExamples_<?php echo $scenario['scenario_id']; ?>"><i class="fa fa-chevron-down"></i></a>

                  </div>

                  <div class="clearfix"></div>

                </div>

                <div id="validationExamples_<?php echo $scenario['scenario_id']; ?>" class="panel-collapse collapse in">

                <div class="panel-body">

                	<div align="right" style="padding-bottom:10px;float:left;"><a class="btn btn-primary btn-sm" href="<?php echo base_url().'cc/scenario/addtask/'.$scenario['scenario_id'].'/res'; ?>">Add New Task</a></div>

                  	<div align="right" style="padding-bottom:10px;"><a class="btn btn-primary btn-sm" onclick="return contained_confirm('<?php echo base_url().'cc/dashboard/closescenario/'.$scenario['scenario_id']; ?>')" href="javascript:void(0);">Close Incident</a></div>

                    

                   	<?php 

					/*display task for each scenario */

					$task_list=$this->common_model->get_scenario_task($scenario['scenario_id']);

					if(count($task_list)>0)

					{

						

					?>

                    <div id="myTabContent" class="tab-content">

                    <div class="tab-pane fade active in" id="home">

                      <div class="table-responsive">

                        <table  class="table table-striped table-bordered table-hover table-green myexample-table"><!--id="example-table"-->

                          <thead>

                            <tr>

                              <th width="55%">Task Name</th>

                              <th width="30%">Owner</th>

                              <th width="15%">Status</th>

                              <th width="10%"> </th>

                            </tr>

                          </thead>

                          <tbody>

                            <?php 

                           	foreach($task_list as $task)

                            {

								 	if($task['task_status'] == 1 ){

                                    $status = ' <span class="text-red">In Progress</span>';

                                    } if($task['task_status'] == 0 ) {	

                                    $status = '<span class="text-red">Pre-Incident</span>';

                                    }

									 if($task['task_status'] == 2 ){

                                    $status = '<span class="text-green">Completed</span>';

                                    } 

                            ?>

                                <tr>

                                    <td><?php echo $task['task']; ?></td>

                                    <td><?php echo $this->master_model->decryptIt($task['crt_first_name']).' '.$this->master_model->decryptIt($task['crt_last_name']); ?></td>

                                    <td><?php echo $status; ?></td>

                                    <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="#" data-toggle="modal" data-target="#mytaskModal<?php echo $task['task_id']; ?>">View</a>
            </li>
            <li>
                <a onclick="return del_confirm('<?php echo base_url().'cc/dashboard/delete_task/'.$task['task_id']; ?>');" href="javascript:void(0);">Delete</a>
            </li>
          </ul>
        </div>    


<!-- Modal -->
<div class="modal fade" id="mytaskModal<?php echo $task['task_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Scenario Task Detail</h4>
      </div>
      <div class="modal-body">
		<p>Task Owner: <span class="text-muted"><?php echo $this->master_model->decryptIt($task['crt_first_name']).' '.$this->master_model->decryptIt($task['crt_last_name']); ?></span></p>
		<p>Scenario: <span class="text-muted"><?php echo $scenario['scenario']; ?></span></p>
		<p>Task Description: <span class="text-muted"><?php echo $task['task']; ?></span></p>
		<p>Task Guidance: <span class="text-muted"><?php echo $task['task_guide']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

                                    
                                    
                                    </td>

                                </tr>	

                            <?php 

							}

                            ?>

                            

                          </tbody>

                        </table>

                      </div> 

                    </div>

                    </div>

                      <!-- /.table-responsive -->

                      <?php }
					  
					  else{ ?>
                            <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
							<p class="text-center" style="margin-top: 20px; color: #ccc">No tasks for this scenario</p>
					  <?php }?>

                    

                    

                  

                </div>

                </div>

                <!-- /.panel-body --> 

              </div>

              <!-- /.panel --> 

              

            </div>

<?php

		}

	}

} //end of standard crisisflo 
}
?>


	<!--cases report--->
    <div class="col-lg-12">
    
		<?php 
		
		if (strpos($the_active_module[0]['active_module'], '2') !== false){ //case management activated 
        

        

        if(count($casereport)>0)

        {
            
            
            foreach($casereport as $r => $value)

            {

        ?>

        <div class="panel panel-default">
    
            <div class="panel-heading">
    
                <div class="panel-title">
    
                    <h4>
						<?php   
                            $caseid = $value['case_id'];

                            $thecase = $this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'id'=>$caseid));
                        
                            echo $thecase[0]['gen_id'].' - '.$thecase[0]['case_title'];
                        ?>
                    
                    
                    </h4>
    
                </div>
    
    
    
            <div class="clearfix"></div>
    
            </div>
    
            
    
            <div class="panel-body">


							<div class="table-responsive">
							<table class="table table-hover table-bordered table-green" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="40%">Assessment Phase</th>

                                        <th width="40%">Owner</th>

                                        <th width="20%">Status</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

                                                        

                                    <tr>

                                  	  <td>Evidence & Documents</td>
                                      
                                  	  <td>
										  <?php 
                                            $ownerr=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$value['crt_id']));
                                            echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']);
                                          ?>
                                      </td>
                                      
                                  	  <td>
										  <?php 
											$casedocss = $this->master_model->getRecords('case_document_report',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'case_id'=>$caseid));
										
										
											if(count($casedocss) > 0){
                                           
                                           echo '<span class="text-green">Completed</span>';
                                           }
                                           else{
                                           echo '<span class="text-red">In-Progress</span>';
                                           }
										  
										   ?>
                                      </td>
                                  	  <td>
                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                                <li>
                                                    <a href="" data-toggle="modal" data-target="#mycasedocsModal<?php echo  $value['id']; ?>">View</a>
                                                
                                                </li>
                                              </ul>
                                            </div>    
                                      
<!-- Modal -->
<div class="modal fade" id="mycasedocsModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Evidence and Documents</h4>
      </div>
      <div class="modal-body">
			<?php if (count($casedocss) > 0){
				
				?>
			
			
            <div class="table-responsive">

            <table class="table table-hover table-bordered table-green" id="example-tablexx">

                <thead>

                    <tr>

                        <th width="50%">Document Name</th>

                        <th width="50%">File Description</th>

                        <th width="50px"> </th>

                    </tr>

                </thead>

                <tbody>

                
                
                <?php
                
                    foreach($casedocss as $casedocss){
                ?>
            
            
                <tr>

                    <td>
                    <?php echo $casedocss['document']; ?>
                    </td>
                    <td>
                    <?php echo $casedocss['file_desc']; ?>
                    </td>
                    <td>
                    <a href="<?php echo base_url().'cc/document/download/'. $casedocss['document']; ?>" class="btn btn-sm btn-default">Download</a>
                    </td>
                </tr>
            

                <?php 

                    }
                ?>

                                </tbody>

                            </table>

                        </div>

                        <!-- /.table-responsive -->

                <?php 

                    }
					else{
						echo '<p class="text-center text-muted" style="margin-top: 30px;">No evidence and documents.</p>';
						
					}
                ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                      </td>

                                    </tr>
                                    
                                    <tr>

                                  	  <td>Hazard Analysis</td>
                                      
                                  	  <td>
										  <?php 
                                            $ownerr = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$value['crt_id']));
                                            echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']);
                                          ?>
                                      </td>
                                      
                                  	  <td>
										  <?php 
                                           if($value['hazard']!=''){
                                           
                                           echo '<span class="text-green">Completed</span>';
                                           }
                                           else{
                                           echo '<span class="text-red">In-Progress</span>';
                                           }
                                          ?>
                                      
                                      </td>
                                  	  <td>
                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                                <li>
                                                    <a href="" data-toggle="modal" data-target="#mycasehazModal<?php echo  $value['id']; ?>">View</a>
                                                
                                                </li>
                                              </ul>
                                            </div>    
                                      
<!-- Modal -->
<div class="modal fade" id="mycasehazModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Case Details</h4>
      </div>
      <div class="modal-body">
			<p>Hazard Analysis: <span class="text-muted"><?php if ($value['hazard']!=''){echo  $value['hazard'];}else { echo 'No assessment yet.';} ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                      
                                      
                                      
                                      
                                      </td>

                                    </tr>
                                    
                                    <tr>

                                  	  <td>Impact Analysis</td>
                                      
                                  	  <td>
										  <?php 
                                            $ownerr=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$value['crt_id']));
                                            echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']);
                                          ?>
                                      </td>
                                      
                                  	  <td>
										  <?php 
                                           if($value['impact_to_bsn']!='' && $value['impact_to_stk']!=''){
                                           
                                           echo '<span class="text-green">Completed</span>';
                                           }
                                           else{
                                           echo '<span class="text-red">In-Progress</span>';
                                           }
                                          ?>
                                      
                                      </td>
                                  	  <td>
                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                                <li>
                                                    <a href="" data-toggle="modal" data-target="#mycaseimpModal<?php echo  $value['id']; ?>">View</a>
                                                
                                                </li>
                                              </ul>
                                            </div>    
                                      
<!-- Modal -->
<div class="modal fade" id="mycaseimpModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Case Details</h4>
      </div>
      <div class="modal-body">
			<p>Impact to business operations: <span class="text-muted"><?php if ($value['impact_to_bsn']!=''){echo  $value['impact_to_bsn'];}else { echo 'No assessment yet.';} ?></span></p>
			<p>Impact to stakeholders: <span class="text-muted"><?php if ($value['impact_to_stk']!=''){echo  $value['impact_to_stk'];}else { echo 'No assessment yet.';} ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                      
                                      
                                      
                                      
                                      </td>

                                    </tr>
                                    
                                    <tr>

                                  	  <td>Control</td>
                                      
                                  	  <td>
										  <?php 
                                            $ownerr=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$value['crt_id']));
                                            echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']);
                                          ?>
                                      </td>
                                      
                                  	  <td>
										  <?php 
                                           if($value['control']!=''){
                                           
                                           echo '<span class="text-green">Completed</span>';
                                           }
                                           else{
                                           echo '<span class="text-red">In-Progress</span>';
                                           }
                                          ?>
                                      
                                      </td>
                                  	  <td>
                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                                <li>
                                                    <a href="" data-toggle="modal" data-target="#mycasecontModal<?php echo  $value['id']; ?>">View</a>
                                                
                                                </li>
                                              </ul>
                                            </div>    
                                      
<!-- Modal -->
<div class="modal fade" id="mycasecontModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Case Details</h4>
      </div>
      <div class="modal-body">
			<p>Control: <span class="text-muted"><?php if ($value['control']!=''){echo  $value['control'];}else { echo 'No assessment yet.';} ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                      
                                      
                                      </td>

                                    </tr>
                                    
                                    <tr>

                                  	  <td>Recommendations</td>
                                      
                                  	  <td>
										  <?php 
                                            $ownerr=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$value['crt_id']));
                                            echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']);
                                          ?>
                                      </td>
                                      
                                  	  <td>
										  <?php 
                                           if($value['recommendation']!=''){
                                           
                                           echo '<span class="text-green">Completed</span>';
                                           }
                                           else{
                                           echo '<span class="text-red">In-Progress</span>';
                                           }
                                          ?>
                                      
                                      </td>
                                  	  <td>
                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                                <li>
                                                    <a href="" data-toggle="modal" data-target="#mycaserecModal<?php echo  $value['id']; ?>">View</a>
                                                
                                                </li>
                                              </ul>
                                            </div>    
                                      
<!-- Modal -->
<div class="modal fade" id="mycaserecModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Case Details</h4>
      </div>
      <div class="modal-body">
			<p>Recommendations: <span class="text-muted"><?php if ($value['recommendation']!=''){echo  $value['recommendation'];}else { echo 'No assessment yet.';} ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                      
                                      
                                      </td>

                                    </tr>
                    
                                </tbody>

                                
                            </table>

                        </div>

                        <!-- /.table-responsive -->

            </div>
    
        </div>
    
        <!-- /.panel -->
        
		<?php
				}  
			}  
        }// end case management
        ?>
        
    </div>    

</div><!-- /.row-->
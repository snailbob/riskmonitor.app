
<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller ng-cloak hidden" ng-controller="dashboardController as $ctrl">
    <toast></toast>



    <div class="container">
      <div class="row" style="padding-top: 45px">
        <div class="col-lg-12">

          <div class="row gutter-10 newdash-panel" ui-sortable="sortableOptions" ng-model="dashItems">

            <div as-sortable-item class="col-sm-4 col-lg-2" ng-repeat="item in dashItems track by $index">
              <div class="panel panel-default" ng-click="dashAction(item)">
                <div class="panel-body">
                  <h4 class="text-muted text-uppercase">
                    {{item.title}}
                  </h4>
                  <p class="leadx">
                    <a ng-bind-html="item.text">
                      
                    </a>
                  </p>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row" style="padding-top: 15px">
        <div class="col-lg-12">
          <uib-tabset active="active">
            <uib-tab index="0" heading="Open incidents ({{incidents.length}})">
              <div class="bg-tab">
                <div class="panel panel-default panel-incidents" ng-repeat="incident in incidents track by $index">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <span class="label label-success">{{incident.incident_status_text}}</span>
                        <h4>
                          <a ng-href="<?php echo base_url().'cc/standbytasks/activetask/'?>{{incident.id}}/{{incident.template_id}}" class="link-no-decor">
                            <small>{{incident.incident_no}} - {{incident.description}}</small>
                          </a>
                        </h4>
                      </div>
                      <div class="col-sm-6">
                        <div class="row gutter-0">
                          <div class="col-sm-5 text-center" style="border-right: 1px solid #999">
                            <h4 class="text-muted">
                              {{incident.completion_rate.percentage}}% <br>
                              <small>Completion rate</small>
                            </h4>

                          </div>
                          <div class="col-sm-7 text-center">
                            <h4 class="text-muted">
                              <span ng-bind-html="formatElapsedMoment(incident)"></span> <br>
                              <small>Elapsed time</small>
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
            </uib-tab>
            <uib-tab index="1" heading="Incident forms ({{submitted_incidents.length}})">
              <div class="bg-tab">
                <div class="panel panel-default panel-incidents" ng-repeat="incident in submitted_incidents track by $index">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <span class="label label-success">{{incident.answer}}</span>
                        <h4>
                          <a ng-click="showFormIncidentFields(incident)" class="link-no-decor">
                            <small>Respond id# - {{incident.id}}</small>
                          </a>
                        </h4>
                      </div>
                      <div class="col-sm-6 text-right">
                        <!-- <button class="btn btn-link" ng-click="addComment(incident)">
                          Add Comment
                        </button> -->
                        <!-- Single button -->
                        <div class="btn-group pull-right" uib-dropdown is-open="status.isopen">
                          <button id="single-button" type="button" class="btn btn-default" uib-dropdown-toggle ng-mouseover="status.isopen = true">
                            <i class="fa fa-ellipsis-v"></i>
                          </button>
                          <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                            <li role="menuitem"><a ng-click="archiveIncident(incident)">Archive</a></li>
                          </ul>
                        </div>
                        
                        <!-- <button class="btn btn-info" ng-click="archiveIncident(incident)">
                          Archive
                        </button> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </uib-tab>
            <uib-tab index="2" heading="Archived incidents ({{submitted_incidents_history.length}})">
              <div class="bg-tab">
                <div class="panel panel-default panel-incidents" ng-repeat="incident in submitted_incidents_history track by $index">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <span class="label label-success">{{incident.answer}}</span>
                        <h4>
                          <a ng-click="showFormIncidentFields(incident)" class="link-no-decor">
                            <small>Respond id# - {{incident.id}}</small>
                          </a>
                        </h4>
                      </div>
                      <div class="col-sm-6">
                        <div class="well well-sm">
                          Comment: {{(incident.module) ? incident.module : 'n/a'}}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
              </div>
            </uib-tab>
          </uib-tabset>
        </div>
      </div>
    </div>


    <script type="text/ng-template" id="templateModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                <a ng-click="addTaskTemplate()" class="btn btn-default btn-sm pull-right">
                  <i class="fa fa-plus" aria-hidden="true"></i> Standby Task Template
                </a>

                Standby Tasks Templates
            </h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-default" ng-repeat="template in templates track by $index">
            <div class="panel-body">
                <a ng-href="<?php echo base_url().'cc/standbytasks/index/0/'?>{{template.id}}?template_name={{template.name}}">
                  {{template.name}}
                </a>
            </div>
          </div>
        </div>

    </script>


    <script type="text/ng-template" id="incidentsModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                <!-- <a ng-click="addIncident()" class="btn btn-default btn-sm pull-right">
                  <i class="fa fa-plus" aria-hidden="true"></i> Incident
                </a> -->
                Open Incidents
            </h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-default" ng-repeat="incident in incidents track by $index">
            <div class="panel-body">
              <h4>
                <a ng-href="<?php echo base_url().'cc/standbytasks/activetask/'?>{{incident.id}}/{{incident.template_id}}" class="link-no-decor">
                  <span class="text-mutedx">{{incident.incident_no}}</span> <small><span class="label label-success">{{incident.incident_status_text}}</span> {{incident.description}}</small>
                </a>
              </h4>
              <!-- <p class="">
                <br> <small>{{incident.incident_status_text}}</small>
              </p> -->
            </div>
          </div>
        </div>

    </script>



    <script type="text/ng-template" id="addTaskTemplateModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Standby Task Template
            </h4>
        </div>
        <div class="modal-body">
            <ng-form name="nameDialog" novalidate role="form">
                <div class="form-group">
                    <label class="control-label" for="course">
                      Name <i class="fa fa-info-circle text-muted" aria-hidden="true" tooltip-placement="top" uib-tooltip="Incident or crisis scenario it relates to"></i>
                    </label>
                    <input type="text" class="form-control" name="name" id="name" ng-model="data.name" required>
                </div>
            </ng-form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Submit</button>
        </div>

    </script>

    <script type="text/ng-template" id="raiseModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Open Incident
            </h4>
        </div>
        <div class="modal-body">
            <ng-form name="nameDialog" novalidate role="form">
                <div class="form-group">
                    <label class="control-label" for="course">Short incident description</label>
                    <input type="text" class="form-control" name="description" id="description" ng-model="data.description" required>
                </div>
                <div class="form-group">
                  <select name="template" id="template" class="form-control" ng-options="tz as tz.name for tz in templates track by tz.country_id" ng-model="data.template_details" ng-change="changeCc(data.template_details)">
                  </select>
                </div>
            </ng-form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Submit</button>
        </div>

    </script>

    <script type="text/ng-template" id="formIncidentFields.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Incident Form Response
            </h4>
        </div>
        <div class="modal-body">
            <div class="well well-sm" ng-repeat="field in incident.fields track by $index">
              <b>{{field.question}}</b> <br>
              <span class="text-info">{{field.response}}</span>
            </div>

            <div class="well well-sm">
              <input type="text" placeholder="Add your comment here.." ng-model="incident.module" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-disabled="!incident.module" ng-click="ok()">OK</button>
        </div>

    </script>

  </div>
</div>

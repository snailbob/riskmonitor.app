                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Details Scenario Task

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Details Scenario Task</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



			  <!-- Form AREA -->

				<div class="row">
                    <div class="col-lg-12" style="height:50px;">

                        <a class="btn btn-default pull-right" href="<?php echo base_url();?>cc/scenario/managetask/">Back</a> 
               		</div>

                    

                 

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Details Scenario Task</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-task" id="frm-add-task" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Scenario Description</label>

                            <div class="col-sm-10">

                                 <div class="details_content">

                                 <?php echo $task_info[0]['scenario']; ?>

                            </div>							

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Task(s)</label>

                            <div class="col-sm-10">

                                <div class="details_content">

                                 <?php echo $task_info[0]['task']; ?>

                            </div>					

                           	</div>

                        </div>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Assign to</label>

                            <div class="col-sm-10">

                               <div class="details_content">

                                 <?php 

								 echo $task_info[0]['crt_first_name'].' '.$task_info[0]['crt_last_name']; ?>

                            </div>

                           	</div>

                        </div>

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




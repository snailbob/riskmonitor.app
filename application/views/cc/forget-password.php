
<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>



    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">


<!-- Popup css-->

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->


	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->


	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}

		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}

		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}



	</style>


</head>

<body style="background: #fff;">


	<nav class="navbar" style="padding-top: 0px">
		<div class="container-fluid">
			<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>">
				<img src="<?php echo base_url()?>assets/frontpage/corporate/images/crisisflo-logo-medium.png" alt="">
			</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row" style="margin-bottom: 50px;">
			<div class="col-md-8 col-md-offset-2 new-modal-section" style="margin-top: 50px">
                <h1 class="text-left new-modal-title">Forgot Password?</h1>
            </div>
            
			<div class="bg-grey2 col-md-8 col-md-offset-2">
				<div class="row row-grey">
					
					<div class="col-md-6" style="border-right: 1px solid #fff;">
                        <p>
                            To reset your password, please use the form below. We’ll send you an email with a link to reset your password.
                        </p>


                        <div id='login_form'>

                            <form action='' method='post' name="admin-login" id="admin-login">


                              <br />

                                 <?php if($success!="") { ?>

                            <div class="alert alert-success alert-dismissable" style="margin-bottom: 15px;">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $success; ?>

                            </div>

                            <?php } if($error!="")  { ?>

                            <div class="alert alert-danger alert-dismissable" style="margin-bottom: 15px;">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $error;?></div>

                            <?php } ?>




                                <fieldset>

                                    <div class="form-group">

                                        <input class="form-control" placeholder="Enter your registered email" type='text' name='user_name' id='user_name' autofocus />

                                    </div>



                                    <button  class="btn btn-primary btn-lg btn-block" type='submit' name="btn_forget" id="btn_forget">

                                         Submit <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    </button>

                                </fieldset>

                                <br>

                                <p class="">

                                    <a href="<?php echo base_url().'cc' ?>">Back to Login</a>
                                   <?php /*?> <p> <?php if ($emailss == null) {

									echo 'empty';}else{  echo $emailss[0]['login_id'];
									}; ?></p><?php */?>

                                </p>

                            </form>

                        </div>




                    </div>

					<div class="col-md-6">
                        <p>
                            CrisisFlo is committed to protecting your privacy. <a href="#" class="privacy_btn">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>

                        </p>
                    </div>

                </div>
            </div>	

		</div>
    </div>
    

    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $(".btn").click(function() {
        var $btn = $(this);
        $btn.button('loading');
        // simulating a timeout
        setTimeout(function () {
            $btn.button('reset');
        }, 6000);
    });



    $('.privacy_btn').on('click', function(){
        $( "#privacyModal" ).modal('show');
        $( "#privacyModal" ).find('.text_content').html('<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p>');
        $( "#privacyModal" ).find('.modal-footer').hide();

        $( "#privacyModal" ).find('.text_content').load( base_url+'frontpage/anewskin/privacy', function() {
            console.log('yeah');
            $( "#privacyModal" ).find('.modal-footer').show();
        });

        return false;
    });


});

window.onload = function() {
  var input = document.getElementById("user_name").focus();
}
</script>


</body>

</html>




<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller ng-cloak hidden" ng-controller="groupMessageController as $ctrl">

    <div class="row">

        <div class="col-lg-12 col-headers" ng-if="!currentPage.edit">

          <div class="scroll-contentx">

            <div class="message_section">
              <div class="rowx">
                  <div class="chat_area">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-10 col-lg-offset-1 bg-white">


                          <p class="lead text-center text-muted" ng-if="!messages.length" style="margin-top: 15px;">
                            No messages yet.
                          </p>
                          <ul class="list-unstyled" ng-if="messages.length">
                              <li class="left clearfix" ng-repeat="message in messages track by $index" ng-class="{'admin_chat' : message.owner == 'me'}">
                                  <span class="chat-img1" ng-class="{'pull-right' : message.owner == 'me', 'pull-left' : message.owner != 'me'}">
                                    <img ng-src="{{message.author.avatar}}" alt="User" class="img-circle">
                                  </span>
                                  <div class="chat-body1 clearfix">
                                    <div class="chat-holder" ng-class="{'chat-holder-right' : message.owner == 'me'}">
                                      <div class="chat-content" ng-class="{'chat-content-right' : message.owner == 'me'}">
                                        <div ng-bind-html="htmlSafe(message.message)"></div>

                                        <ul class="list-unstyled" ng-if="message.files.length">
                                          <li ng-repeat="file in message.files track by $index">
                                            <a ng-href="{{file.link}}" download target="_self">
                                              <i class="fa fa-file" aria-hidden="true"></i> {{file.file_name}}
                                            </a>
                                          </li>
                                        </ul>
                                      
                                      </div>
                                      <div class="chat_time text-muted" ng-class="{'pull-right' : message.owner != 'me', 'pull-left' : message.owner == 'me'}"><small>{{message.time_sent}}</small></div>
                                    </div>
                                  </div>
                              </li>
                              
                          </ul>

                          <div class="is-typing" ng-if="isTyping.first_name">
                            <p class="text-center text-muted">{{isTyping.first_name}} is typing...</p>
                          </div>

                        </div>                      
                      </div>
                    </div>


                  </div>
                  <!--chat_area-->
                  <div class="message_write">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-10 col-lg-offset-1 bg-white">

                          <summernote airMode ng-model="theMessage.message" config="optionsGroupMessage" on-change="change(evt)"></summernote>
                          <!-- <textarea class="form-control" placeholder="type a message"></textarea> -->
                          <div class="clearfix"></div>

                          <div class="progress" ng-if="uploader.isUploading || uploader.isUploaded" style="margin-bottom: 0px; margin-top: 5px; height: 2px;">
                              <div class="progress-bar" style="height: 2px;" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                          </div>

                          <div class="chat_bottom">
                            <div class="pull-left">

                              <div class="hidden">
                                <input type="file" id="docShare" nv-file-select="" uploader="uploader" />
                              </div>
                            </div>
                            <a class="btn btn-default" ng-click="deleteFile($index)" title="click to remove" ng-repeat="file in files track by $index">
                                {{file.file_name}}
                            </a>

                            <div class="pull-right text-right">
                              <a class="btn upload_btn" ng-click="uploadShare()">
                                  <i class="fa fa-cloud-upload" aria-hidden="true"></i> Add File
                              </a>
                              <a class="btn btn-success" ng-click="sendMessage()" ng-class="{'disabled': loading || (!theMessage.message && !files.length)}">
                                <i class="fa fa-paper-plane-o" aria-hidden="true"></i> <span ng-if="!loading">Send</span><span ng-if="loading">Sending..</span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>


                  </div>
              </div>
            </div>
            <!--message_section-->

          </div>

        </div>
    </div><!--.row -->


  </div>
</div>
  
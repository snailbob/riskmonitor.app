  
	<?php if($this->uri->segment(2) == 'incidentform') { ?> 
	<!-- Plugin JavaScript -->
	<script src="<?php echo base_url() ?>assets/plugins/formbuilder/dist/js/vendor.js"></script>
	<script src="<?php echo base_url() ?>assets/plugins/formbuilder/dist/js/form-builder.min.js"></script>
	<script src="<?php echo base_url() ?>assets/plugins/formbuilder/dist/js/form-render.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.js"></script>
	<script src="<?php echo base_url() ?>assets/plugins/formbuilder/dist/js/demo.js"></script>
	<?php } ?>

    <!-- JavaScript -->
	<script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
	
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap-tour.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/jquery-ui.min.js"></script>

    <!-- JavaScript angularjs -->
	<script src="<?php echo base_url() ?>assets/plugins/angularjs/angular.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.4.8/angular-sanitize.js"></script>

	<script src="<?php echo base_url() ?>assets/plugins/angularjs/app.js"></script>


	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>
    <!--<script src="<?php echo base_url()?>assets/2/js/plugins/popover-extra-placements/popover-extra-placements.js"></script>-->


    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>
    
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/wysiwyg-demo.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/moment.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.form/jquery.form.js"></script>
    
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.geocomplete/jquery.geocomplete.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/oms/oms.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/flipclock/flipclock.min.js"></script>
  
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/momentjs/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/momentjs/moment-timezone-with-data.js"></script>
	
    <script src="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.min.js"></script>


    <script src="<?php echo base_url()?>assets/2/js/chat-im.js"></script>
    <script src="<?php echo base_url().'assets/2/js/plugins/validate/jquery.validate.min.js' ?>"></script>
   
    <?php
	$cc_id = $this->session->userdata('logged_parent_cc');
	$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));

	if($this->uri->segment(2) == 'settings' || $this->uri->segment(3) == 'payments' || $cc_info[0]['stripe_customer_id'] == '0' || ($this->uri->segment(2) == 'communication' && $this->uri->segment(3) == 'manage')) { ?>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
    <?php } ?>
  
    <?php if($cc_info[0]['stripe_customer_id'] == '0' || ($this->uri->segment(2) == 'communication' && $this->uri->segment(3) == 'manage')) { ?>
    

	<script>
		// This identifies your website in the createToken call below
		var pubkey = 'pk_test_2g76COgjgp2DPnHPHdxrRCZc'; // (base_url != 'https://www.crisisflo.com/x') ? 'pk_test_qLvqgzEetIhJZnK3oSl9HrTT' : 'pk_test_2g76COgjgp2DPnHPHdxrRCZc';
		Stripe.setPublishableKey(pubkey);
		// ...
			
			
		function stripeSignUpResponseHandler(status, response) {
		  var $form = $('#signup');
		
		  if (response.error) {
			// Show the errors on the form
			$form.find('.payment-errors').text(response.error.message).closest('div').removeClass('hidden');
			$form.find('button').html('Submit').prop('disabled', false);
			return false;
		  } else {
			// response contains id and card, which contains additional card details
			var token = response.id;
			$form.find('.payment-errors').closest('div').addClass('hidden');
			// Insert the token into the form so it gets submitted to the server
			$form.append($('<input type="hidden" name="stripeToken" />').val(token));
			// and submit
			

			var data = {
				stripeToken: token,
//				amount: $('[name="amount"]').val(),
//				nexmo_balance: $('[name="nexmo_balance"]').val()
			};
			
			data = $form.serialize() +'&'+ $.param(data);
			console.log(data);
			
			$('.step-confirm-btn').click();
			$('.step-confirm-btn').addClass('active').siblings().removeClass('active');
			$('#progressbar').children('.active').removeClass('active').addClass('done').next().addClass('active');
		
			
			
			return false;
			
			//$form.get(0).submit();
		  }
		};	
		//end of sign up subscription
						
		function stripeResponseHandler(status, response) {
		  var $form = $('#top_up_form');
		
		  if (response.error) {
			// Show the errors on the form
			$form.find('.payment-errors').text(response.error.message).closest('div').removeClass('hidden');
			$form.find('button').html('Purchase').prop('disabled', false);
		  } else {
			// response contains id and card, which contains additional card details
			var token = response.id;
			$form.find('.payment-errors').closest('div').addClass('hidden');
			// Insert the token into the form so it gets submitted to the server
			//$form.append($('<input type="hidden" name="stripeToken" />').val(token));
			// and submit

			var data = {
				stripeToken: token,
				amount: $('[name="amount"]').val(),
				nexmo_balance: $('[name="nexmo_balance"]').val()
			};
			
			data = $.param(data);
			$.post(
				base_url+'cc/communication/addcust_purchase',
				data,
				function(res){
					console.log(res);
					window.location.reload(true);
				},
				'json'
			).error(function(err){
				console.log(err);
			});
			
			return false;
			
			//$form.get(0).submit();
		  }
		};	

	</script>
   
    <?php } //end stripe for demo and top up?>
    
    
    
    
    <?php if($this->uri->segment(2) == 'settings' || $this->uri->segment(3) == 'verifications') { ?>
    <script>
			
		// Handle the successful return from the API call
		function onSuccess(data) {
			console.log(data);
			
			$('#MyLinkedInButton').addClass('disabled');
			var dataa = {
				"email": data.emailAddress,
				"id": data.id,
				"type": 'linkedin'
			};
			
			dataa = $.param(dataa); // $(this).serialize() + "&" + 
			
			$.post(
				base_url+'cc/settings/add_connect',
				dataa,
				function(res){
					console.log(res);
					$('#MyLinkedInButton').removeClass('disabled');
					$('#MyLinkedInButton').addClass('hidden').siblings().removeClass('hidden');

				},
				'json'
			);
			
		}
		
		// Handle an error response from the API call
		function onError(error) {
			console.log(error);
		}

		
		$(document).ready(function(e) {
			
			
			$("#MyLinkedInButton").bind('click',function () {IN.User.authorize(); return false;});
			
			IN.Event.on(IN, 'auth', function(){
				/* your code here */
				//for instance

				console.log('auth success');
				IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);


			});
			
			$(document).on('click', '.gplus_connected_btn', function(){
				var $self = $(this);
				var type = 'Google+';
				var connection_type = 'gplus';
				
				bootbox.confirm('<p class="lead">Confirm you wish to disconnect '+type+'.</p>', function(e){
					if(e){
	
						var dataa = {
							"type": connection_type //'linkedin'
						};
						
						dataa = $.param(dataa); // $(this).serialize() + "&" + 
	
						$.post(
							base_url+'cc/settings/remove_connect',
							dataa,
							function(res){
								console.log(res);
								$self.addClass('hidden').siblings().removeClass('hidden');
							},
							'json'
						).error(function(err){
							console.log(err);
						});
					}
				});
			});
			
			
			$(document).on('click', '.linkedin_connected_btn', function(){
				var $self = $(this);
				var type = 'LinkedIn';
				var connection_type = 'linkedin';

				
				bootbox.confirm('<p class="lead">Confirm you wish to disconnect '+type+'.</p>', function(e){
					if(e){
	
						var dataa = {
							"type": connection_type //'linkedin'
						};
						
						dataa = $.param(dataa); // $(this).serialize() + "&" + 
	
						$.post(
							base_url+'cc/settings/remove_connect',
							dataa,
							function(res){
								console.log(res);
								$self.addClass('hidden').siblings().removeClass('hidden');
							},
							'json'
						).error(function(err){
							console.log(err);
						});
					}
				});
			});
			
		
		});
		
	
	</script>
    <?php } //end for linkedin?>
    
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.0/croppie.js"></script> 
    
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/consultant.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/admin-validation.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/cc-validation.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/cc-script.js"></script>
	<?php
	//run chart js if open incident > 0, recall module, and for particular pages to avoid js error

	$cc_id = $this->session->userdata('logged_cc_login_id');
	$org_id = $this->session->userdata('cc_selected_orgnaization');
	
	$main_active_module = substr($this->session->userdata('org_module'), 0,1);
	$active_feature_modules = $this->common_model->active_module($org_id);
	
	$active_module =  $main_active_module.$active_feature_modules;
	
	$the_active_module = array('0'=>array('active_module'=>$active_module));
	$active_module = substr($this->session->userdata('org_module'), 0, 1);

	if($this->uri->segment(2) == 'log'){
		$inci_stat = '1';
	}
	else{
		$inci_stat = '0';
	}

	$whr_inci = array(
		'cc_id'=>$cc_id,
		'org_id'=>$org_id,
		'closed'=>$inci_stat
	);
	
	
	if($active_module == '5'){
		$incidents = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

	}
	else{ //continuity = 8
		$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));

	}
	
	if (strpos($the_active_module[0]['active_module'], '0') !== false && count($incidents) > 0){ //kpi is active
	
		if((($this->uri->segment(2) == 'dashboard') && ($this->uri->segment(3) == 'index' || $this->uri->segment(3) == '')) || ($this->uri->segment(2) == 'kpi' && $this->uri->segment(3) == 'manage') || $this->uri->segment(2) == 'log') {?>
        
    <!-- Flot Charts -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/cc-flot-chart-kpi.js"></script>
    
    
    <?php
	}//dash and kpi
	}//kpi is active
	?>
    
	
	<!-- Scripts -->
	<!-- <script type="text/javascript" src="//code.angularjs.org/1.5.8/angular.min.js"></script> -->
	<script type="text/javascript" src="//cdn.quilljs.com/1.1.5/quill.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/quill/'; ?>ng-quill.js"></script>

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/bootstrap/'; ?>ui-bootstrap-tpls-2.5.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/bootstrap/'; ?>ui-bootstrap-dialogs.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/bootstrap/'; ?>dialogs.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/ng-sortable/'; ?>sortable.js"></script> 
	
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-vertilize/'; ?>angular-vertilize.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-drag-and-drop-lists/'; ?>angular-drag-and-drop-lists.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/draganddrop/'; ?>draganddrop.min.js"></script> 

	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>moment-precise-range.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>countdown.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/moment/'; ?>moment-countdown.js"></script>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-moment/1.0.1/angular-moment.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'; ?>summernote.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'; ?>angular-summernote.js"></script>
	
	<script src="<?php echo base_url().'assets/plugins/angularjs/angular-file-upload/'; ?>dist/angular-file-upload.min.js"></script>

	<script src="<?php echo base_url().'assets/plugins/angularjs/ngToast/'; ?>angular-animate.min.js"></script>
	<script src="<?php echo base_url().'assets/plugins/angularjs/ngToast/'; ?>ngToast.min.js"></script>	
</body>

</html>

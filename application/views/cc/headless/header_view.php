<?php


$cc_id = $this->session->userdata('logged_cc_login_id');

$org_id = $this->session->userdata('cc_selected_orgnaization');


if($cc_id == "" || $org_id == ""){
    redirect(base_url().'signin');
}

else if($this->session->userdata('org_module')==""){
	redirect('cc/dashboard/selectmodule');
}
else{
    $recall_main_status = $this->common_model->setup_status_completed(); //recall_main_status();

}

 ?>

<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $nav_title; ?></title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?>
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/2/css/bootstrap-tour.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet"> -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <?php /*?><!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css"><?php */?>


    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">

	<link href="<?php echo base_url()?>assets/2/css/plugins/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

	<link href="<?php echo base_url()?>assets/2/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">


    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote.css" rel="stylesheet">
    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">-->
    <link href="<?php echo base_url()?>assets/2/css/plugins/flipclock/flipclock.css" rel="stylesheet">

    <!-- Popup css-->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">


    <!-- full calendar css-->
    <link href="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">


    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Style quilljs -->
    <link rel="stylesheet" href="//cdn.quilljs.com/1.1.5/quill.snow.css">
    <link rel="stylesheet" href="//cdn.quilljs.com/1.1.5/quill.bubble.css">

    
    <!-- Style croppie -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.0/croppie.css
"> 

    <!-- Style angular-summernote -->
     <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/angularjs/angular-summernote/'.'summernote.css'; ?>"> 


    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">


    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->

	<script type="text/javascript" src="https://platform.linkedin.com/in.js">
	  api_key: 75eekrdxppqx8g
    </script>

<?php
	/*GET THE DATA FROM THE SESSION**************************/
	$uri_2 = $this->uri->segment(2);
	$uri_3 = $this->uri->segment(3);
	$uri_4 = $this->uri->segment(4);
	$uri_5 = $this->uri->segment(5);
	$uri_6 = $this->uri->segment(6);

	//admin help
	$adminhelp = $this->common_model->get_admin_help($uri_2);

	//form validation global error style
	$this->form_validation->set_error_delimiters('<div class="text-red">', '</div>');

	//cc standby tasks reviewdate
	$revdate = $this->session->userdata('task_review_date');
	$therevdate = date_format(date_create($revdate), 'jS F Y, g:ia');


	$session_userfname = $this->session->userdata('logged_display_name');
	if($this->session->userdata('location') !=''){
		$mylocation = $this->session->userdata('location');
		$myloc_class = 'background: #fff;';
	}
	else{
		$mylocation = 'Enter your location';
		$myloc_class = 'background: #e4b9b9; color: #e74c3c;';
	}

	//get awaiting crt count
	$cc_id = $this->session->userdata('logged_cc_login_id');
	$user_id = $this->session->userdata('user_id');
	$org_id = $this->session->userdata('cc_selected_orgnaization');
	$waiting_crtmem = $this->common_model->get_waiting_crt($cc_id,$org_id);

	$where_recall = array(
		'cc_id'=>$cc_id,
		'org_id'=>$org_id,
		'closed'=>'0'
	);

	$open_recall = $this->master_model->getRecordCount('cf_recall',$where_recall);

	$awaitingcount = 0;
	$awaitingclass = 'background: #fff;';
	$awaitingdisplay = '';

	if(substr($this->session->userdata('org_module'), 0,1) != '5' && $open_recall > 0){
		$awaitingdisplay = 'hidden';
	}

	if(count($waiting_crtmem) > 0){
		$awaitingcount = count($waiting_crtmem);
		$awaitingclass = 'background: #e4b9b9; color: #e74c3c;';
	}
	else{
		$awaitingdisplay = 'hidden';
	}


	$mytimezone = $this->master_model->getRecords('time_zone',array('ci_timezone'=>$this->session->userdata('timezone')));

	$nowtime = $this->common_model->userdatetime();

	$date_zone = date_format(date_create($nowtime), "d F Y D"); //gmdate("d F Y D", $thetimeyeah); //g:i:sa d/m/Y
	$time_zone = date_format(date_create($nowtime), "Y-m-d g:i:s a"); // gmdate("Y-m-d g:i:s a", $thetimeyeah);

	//active case report count
	$num_report = $this->master_model->getRecordCount('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0'));


	//unread forum post count
	$forum_vstr_date = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_cc_login_id')));
	$forum_post = $this->master_model->getRecords('forum_post_master',array('cc_id'=>$this->session->userdata('team_cc'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'date_created >'=>$forum_vstr_date[0]['forum_last_visit'],'post_parent'=>'0'));


	//active modules
	$main_active_module = substr($this->session->userdata('org_module'), 0,1);
	$active_feature_modules = $this->common_model->active_module($org_id);

	$active_module =  $main_active_module.$active_feature_modules;

	$the_active_module = array('0'=>array('active_module'=>$active_module));
	//select org
	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$org_id));



	if($main_active_module == '5'){
		$module_name = 'Recall Manager';
	}
	else if($main_active_module == '1'){
		$module_name = 'Crisis Manager';
	}

	else if($main_active_module == '2'){
		$module_name = 'Case Manager';
	}

	else if($main_active_module == '4'){
		$module_name = 'Member Safety';
	}

	else if($main_active_module == '8'){
		$module_name = 'Continuity Manager';
	}

	//check if status is demo
	$user_status = $this->session->userdata('logged_user_type');
	$remaining = 0;
	$cc_info = $this->master_model->getRecords('cf_login_master', array('login_id'=>$cc_id));
	$user_info = ($user_id) ? $this->master_model->getRecords('cf_login_master', array('login_id'=>$user_id)) : array();

	$mobile_code = $this->master_model->getRecords('country_t');

	if(($user_status != 'live' && $cc_info[0]['stripe_customer_id'] == '0') || ($cc_info[0]['single_user'] == 'y' && $cc_info[0]['stripe_customer_id'] == '0')){


		if($this->session->userdata('stripe_plans') != ''){
			$stripe_plans = $this->session->userdata('stripe_plans');
		}
		else{
			$stripe_plans = $this->common_model->all_plans();
			$this->session->set_userdata('stripe_plans', $stripe_plans);
		}


		$price = 0;
		$the_gst = 0;
		$total_price = 0;
		$plan = '';

		$trialtext = ''; // - <a href="#" class="demo_btn">Upgrade Now</a>';
		if(count($cc_info) > 0){
			foreach($cc_info as $r=>$value){
				if($value['trial_end'] != '0000-00-00 00:00:00'){

					$date1= time();
					$date2= strtotime($value['trial_end']);
					//Calculate the difference.
					$difference = $date2 - $date1;

					//Convert seconds into days.
					$remaining = floor($difference / (60*60*24) );
					$remaining = max($remaining,0);
					
					// $date1 = new DateTime(date('Y-m-d H:i:s'));
					// $date2 = new DateTime($value['trial_end']);
					// $remaining = $date1->diff($date2)->format("%d");
					if($remaining > 0){
						$trialtext = ' &middot; '.$remaining.' days remaining &middot;  <a href="#" class="demo_btn btn btn-xs btn-warning">Activate Now</a>';

						if($cc_info[0]['single_user'] != 'y'){

							//$trialtext = ' ('.$remaining.' days remaining -  <a href="#" class="demo_btn">Buy Now</a>)';
						}
						else if($cc_info[0]['single_user'] == 'y' && $cc_info[0]['stripe_customer_id'] == '0'){
							//$trialtext = ' ('.$remaining.' days remaining -  <a href="'.base_url().'cc/settings/payments'.'">Activate Now</a>)';
						}
						else{
							$the_date = date_format(date_create($value['trial_end']), 'M d, Y');
						}
					}

				}

			}
		}

		$module_name .= $trialtext;
	}


	/*********************************************************/
?>


	<script type="text/javascript" language="javascript">
		//declare variables
		var base_url='<?php  echo base_url(); ?>';
		var recall_time;
		var mydatetime = '<?php echo $time_zone ?>';
		var reviewdate = '<?php echo $revdate; ?>';

		var uri_2 = "<?php echo $this->uri->segment(2) ?>";
		var uri_3 = "<?php echo $this->uri->segment(3) ?>";
		var uri_4 = "<?php echo $this->uri->segment(4) ?>";
		var uri_5 = "<?php echo $this->uri->segment(5) ?>";
		var uri_6 = "<?php echo $this->uri->segment(6) ?>";
		var cc_id = "<?php echo $this->session->userdata('logged_parent_cc') ?>";
		var org_id = "<?php echo $this->session->userdata('cc_selected_orgnaization') ?>";
		var user_full_name = "<?php echo $this->session->userdata('logged_display_name') ?>";
		var now_time = "<?php echo date("F j, Y, g:i a") ?>";
		var my_location = "<?php echo $this->session->userdata('location'); ?>";
		var endtrial = "<?php echo ($cc_info[0]['stripe_customer_id'] == '0' && $remaining == 0) ? 'expired' : 'ok'; ?>";
		var endtourforever = '<?php echo (isset($user_info[0]['endtourforever'])) ? $user_info[0]['endtourforever'] : 'Y'; ?>';
    </script>


</head>

<body class="<?php echo (($this->uri->segment('2') == 'standbytasks' && $this->uri->segment('3') == '') || $this->uri->segment('2') == 'wiki') ? 'no-scroll-body' : ''; ?>">


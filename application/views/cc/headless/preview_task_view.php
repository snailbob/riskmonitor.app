<nav class="navbar navbar-default navbar-blue">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" style="padding: 0 0;">
        <img alt="Brand" src="<?php echo base_url().'assets/2/img/crisisflo-logo-medium.png'?>" style="max-height: 50px">
      </a>
    </div>
  </div>
</nav>


<div class="all-task-content container" ng-app="crisisApp" style="margin-top: 55px;">

    <div class="div-controller hidden ng-cloak" ng-controller="previewController">

        <div class="row" >
            <div class="col-lg-12  col-contentsx">


                <div class="scroll-contentx">
                    <div class="paddingx">
                        <h2>
                            {{activeTaskInfo.question}}
                        </h2>
                        <hr>
                        <div class="content-actions">
                            <a name="" id="" class="btn btn-default btn-sm disabled">
                                <i class="fa fa-user" aria-hidden="true"></i> <span ng-if="activeTaskInfo.assigned_id == '0'">Assign</span>  <span ng-if="activeTaskInfo.assigned_id != '0'">Assigned - {{activeTaskInfo.assigned_info.crt_email}}</span>
                            </a>
                            <a name="" id="" class="btn btn-default btn-sm disabled">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> Due after <span ng-if="activeTaskInfo.months">{{activeTaskInfo.months}} months</span> <span ng-if="activeTaskInfo.days">{{activeTaskInfo.days}} days</span> <span ng-if="activeTaskInfo.hours">{{activeTaskInfo.hours}} hours</span> <span ng-if="activeTaskInfo.mins">{{activeTaskInfo.mins}} mins</span>
                            </a>

                        </div>
                    </div>

                    <div class="checklist-content">
                        <div class="editor-holder-preview" ng-repeat="content in contents track by $index">

                            <div class="list-margin-holderx" ng-class="{'hidden' : content.module == 'image'}"
                                ng-if="content.module == 'text' || content.module == 'image'">

                            <!-- <ng-quill-editor ng-model="content.content" placeholder="" modules="getQuillModule(content.module)"></ng-quill-editor> -->
                            <!-- <summernote ng-model="content.content"></summernote> -->

                                <div ng-bind-html="htmlSafe(content.module)"></div>

                            </div>


                            <div ng-if="content.module == 'video'" class="contents-holder list-margin-holderx">
                                <!-- <div class="add-video">
                                    <div class="form-group">
                                    <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Paste YouTube / Vimeo / Wistia URL or embed here">
                                    </div>
                                </div> -->

                                <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
                            </div>


                            <div class="contents-holder list-margin-holderx" ng-if="content.module == 'image'">
                            <div class="text-center" ng-hide="content.content">
                                <!-- <button type="button" class="btn btn-default" role="button" ng-click="showImagePop(content, $event)">
                                <i class="fa fa-plus" aria-hidden="true"></i> Upload image...
                                </button> -->
                            </div>
                            <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
                            </div>


                            <div class="contents-holder list-margin-holderx" ng-if="content.module == 'select'">

                            <div class="field-single">
                                <div class="options-view">

                                    <div class="form-groupx">
                                        <label for="">{{content.content}}</label>
                                        <!-- <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Type label here"> -->
                                    </div>

                                    <select name="" id="" class="form-control" ng-options="option as option.text for option in content.options track by option.id" ng-model="answer.selected">
                                    </select>
                                    <!-- <div class="card card-select">
                                        <div class="card-block">
                                        <div class="form-check form-check-custom" ng-repeat="option in content.options track by $index">
                                            <label class="custom-control custom-checkbox custom-checkbox2">
                                            <span class="custom-number">{{$index + 1}}</span>
                                            <span class="custom-control-description">
                                                <input type="text" class="form-control form-invisible" id="" placeholder="Type option name here" ng-model="option.text" ng-keydown="keyDownSelect($event, option , $index, $parent.$index)">
                                            </span>
                                            </label>
                                        </div>
                                        </div>
                                    </div> -->
                                </div>
                                <!-- options-view -->
                            </div>
                            </div>

                            <div class="contents-holder list-margin-holderx" ng-if="content.module == 'list'">

                            <div class="form-checkx form-check-customx" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">
                                
                                <div class="checkboxx">
                                    <label style="font-weight: normal">
                                        <input type="checkbox" value="">
                                        {{sublist.text}}
                                    </label>
                                </div>

                                
                                <!-- <label class="custom-control custom-checkbox">
                                <input type="checkbox" disabled class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">
                                    <input type="text" class="form-control form-invisible" id="inlineFormInput" placeholder="Type here.. hit enter to add sub-task" ng-model="sublist.text" ng-keydown="keyDownSublist($event, sublist , $index, $parent.$index)">
                                </span>
                                </label> -->
                            </div>



                            <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
                                <div class="form-groupx" ng-hide="field.type == 'hidden' || field.type == 'select'">
                                    <!-- <input type="text" class="form-control" ng-model="field.text" aria-describedby="helpId" placeholder="Type label here"> -->
                                    <label for="">{{field.text}}</label>
                                </div>


                                <input type="text" ng-if="field.type == 'single'"  class="form-control" name="" id="" aria-describedby="helpId" placeholder="">
                                
                                <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2"  placeholder=""></textarea>
                            
                                <div ng-if="field.type == 'email'"  class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" >
                                </div>

                                <div ng-if="field.type == 'url'"  class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">
                                        <i class="fa fa-globe" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" >
                                </div>

                                <div ng-if="field.type == 'hidden'"  class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">
                                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                    </span>
                                    <!-- <input type="text" class="form-control" ng-model="field.text" placeholder="Type label here" aria-describedby="basic-addon1"> -->
                                    <label for="">{{field.text}}</label>
                                </div>


                                <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" onclick="$('#uploadFileInput').click()">
                                <i class="fa fa-upload" aria-hidden="true"></i> Upload File
                                </button>
                                <div class="hidden">
                                    <input type="file" id="uploadFileInput">
                                </div>

                                <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" popover-append-to-body="true" uib-popover-template="'datePickerPopover.html'" popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                                <i class="fa fa-calendar" aria-hidden="true"></i> Add Date
                                </button>

                            </div>

                            </div>

                            <div class="emailer-holderx" ng-if="content.module == 'email'">
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label">To</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label">Cc</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label">Bcc</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label">Subject</label>
                                    <div class="col-sm-9">
                                    <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label">Body</label>
                                    <div class="col-sm-10">
                                    <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                                    <div class="col-sm-8">
                                    <button class="btn btn-default">
                                        <i class="fa fa-paper-plane" aria-hidden="true"></i> Send
                                    </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- editor-holder -->
                    </div>


                </div>

                <div class="padding text-center" style="padding: 30px; border-top: 1px solid #ccc">
                    <button type="button" onclick="window.close()" class="btn btn-primary btn-lg">
                        Done
                    </button>
                </div>
                


            </div>
        </div>


        <script type="text/ng-template" id="datePickerPopover.html">
            <div style="max-width:315px;">
                <div class="row gutter-md">
                
                    <!-- <div class="col-xs-6">
                        <div class="form-group">
                        <label for="">Date</label>
                        <input type="text" ng-model="activeTaskInfo.due_date | date: 'shortDate'" class="form-control">

                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                        <label for="">Time</label>
                        <input type="text" ng-model="activeTaskInfo.due_time" class="form-control"> 
                        </div>
                    </div> -->
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div uib-datepicker ng-model="activeTaskInfo.due_date" datepicker-options="optionsPicker"></div> 
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <a name="" id="" class="btn btn-success btn-block" ng-click="saveDate()" role="button">Save</a>
                    </div>

                    <div class="col-xs-6">
                        <a name="" id="" class="btn btn-danger btn-block" ng-click="removeDate()" role="button">Remove</a>
                    </div>

                </div>
            </div>

        </script>


        

    </div>
</div>
    
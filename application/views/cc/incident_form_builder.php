


<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller hidden ng-cloak" ng-controller="headerTasksController" ng-click="closeAllPopUps()">
    <toast></toast>

    <div class="hidden">
        <input type="file" id="uploadFileInput">
    </div>

    <div class="row">
  
        <div class="col-lg-12 col-contents">
          <div class="scroll-content">
            <div class="container-fluid">
              <div class="row row-incident-form" ng-if="!incidentFormEditing">
                <div class="col-md-8 col-md-offset-2">
    
                  <div class="alert alert-info">
                    <div class="row gutter-10">
                      <div class="col-sm-11">
                        <div class="input-group">
                          <a ng-href="{{formLink}}" class="input-group-addon" id="basic-addon1">
                          <i class="fa fa-link"></i>
                          </a>
                          <input type="text" class="form-control" name="input-formlink" id="input-formlink" ng-value="formLink">
                        </div>
                      </div>
                      <div class="col-sm-1">

                      <!-- Single button -->
                      <div class="btn-group pull-right" uib-dropdown is-open="status.isopen">
                        <button id="single-button" type="button" class="btn btn-default btn-block btn-lg" uib-dropdown-toggle>
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                          <li role="menuitem"><a ng-click="editIncidentMode()">
                              <i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit
                            </a></li>
                          <li role="menuitem"><a ng-click="copyLink()">
                              <i class="fa fa-fw fa-copy"></i> Copy
                            </a></li>
                          <li role="menuitem"><a href="{{formLink}}" target="_blank">
                              <i class="fa fa-fw fa-eye"></i> Preview
                            </a></li>
                        </ul>
                      </div>


                        <!-- <div class="pull-right text-right">
                          <button class="btn btn-sm btn-info" ng-click="editIncidentMode()">
                            <i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit
                          </button>
                          <button class="btn btn-sm btn-info" ng-click="copyLink()">
                            <i class="fa fa-fw fa-copy"></i> Copy
                          </button>
                          <a href="{{formLink}}" target="_blank" class="btn btn-sm btn-info">
                            <i class="fa fa-fw fa-eye"></i> Preview
                          </a>
                        </div> -->
                      </div>
                    </div>
                  </div>            
                </div>
              </div>
            </div>

            <div class="row" ng-if="incidentFormEditing">
              <div class="col-sm-12">
                <div style="padding: 15px 40px">
                  <div class="form-group">
                    <label for="">Form Title</label>
                    <input type="text" ng-model="incidentTitle" class="form-control">
                  </div>
                </div>
              </div>
            </div>

            <div class="checklist-content" ng-if="incidentFormEditing" ui-sortable="sortableContentOptions" ng-model="contents" drag-channel="A" drop-channel="A" ui-on-Drop="onDrop($event,$data,formElements)">
              <div class="editor-holder" ng-repeat="content in contents track by $index"  as-sortable-item>
                <div class="move-handle">
                  <button class="btn btn-thirdy btn-times btn-sm" role="button" ng-click="removeContent($index, content)">
                    <i aria-hidden="true" class="fa fa-times fa-fw"></i>
                  </button>

                  <button ng-if="$index != 0" class="btn btn-thirdy btn-up btn-sm" role="button" ng-click="moveContent($index, 'up')">
                    <i aria-hidden="true" class="fa fa-chevron-up"></i>
                  </button>
                </div>



                <div class="list-margin-holder" ng-class="{'hidden' : content.module == 'image'}"
                    ng-if="content.module == 'text' || content.module == 'image'">

                  <ng-quill-editor ng-model="content.content" placeholder="" ng-click="focusQuill($index)" modules="getQuillModule(content.module)"></ng-quill-editor>
                  <!-- <summernote ng-model="content.content"></summernote> -->

                </div>


                <div ng-if="content.module == 'video'" class="contents-holder list-margin-holder">
                  <div class="form-group">
                    <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Type label here">
                  </div>
                  <div class="add-video">
                    <div class="form-group">
                      <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Paste YouTube / Vimeo / Wistia URL or embed here">
                    </div>
                  </div>

                  <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
                </div>


                <div class="contents-holder list-margin-holder" ng-if="content.module == 'image'">
                  <div class="text-center" ng-hide="content.content">
                    <button type="button" class="btn btn-default margin-top-15 margin-bottom-15" role="button" onclick="var i2 = $('.ql-image').length - 1; $('.ql-image:eq('+i2+')').click();">
                    <!-- ng-click="showImagePop(content, $event)" -->
                      <i class="fa fa-plus" aria-hidden="true"></i> Upload image...
                    </button>
                  </div>
                  <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
                </div>


                <div class="contents-holder list-margin-holder" ng-if="content.module == 'select'">

                  <div class="field-single">
                    <div class="options-view">

                      <div class="form-group">
                        <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Type label here">
                      </div>

                      <select name="" id="" class="form-control" disabled>
                        <option>An option will be selected from this list:</option>
                      </select>
                      <div class="card card-select">
                        <div class="card-block">
                          <div class="form-check form-check-custom" ng-repeat="option in content.options track by $index">
                            <label class="custom-control custom-checkbox custom-checkbox2">
                              <span class="custom-number">{{$index + 1}}</span>
                              <span class="custom-control-description">
                                  <input type="text" class="form-control form-invisible" id="" placeholder="Type option name here" ng-model="option.text" ng-keydown="keyDownSelect($event, option , $index, $parent.$index)">
                              </span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- options-view -->
                  </div>
                </div>

                <div class="contents-holder list-margin-holder" ng-if="content.module == 'list'">


                  <div class="form-check form-check-custom" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">
                    
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" disabled class="custom-control-input">
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">
                          <input type="text" class="form-control form-invisible" id="inlineFormInput" placeholder="Type here.. hit enter to add sub-task" ng-model="sublist.text" ng-keydown="keyDownSublist($event, sublist , $index, $parent.$index)">
                      </span>
                    </label>
                  </div>



                  <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">

                    <div class="form-group" ng-hide="field.type == 'hidden' || field.type == 'select'">
                      <input type="text" class="form-control" ng-model="field.text" aria-describedby="helpId" placeholder="Type label here">
                    </div>


                    <input type="text" ng-if="field.type == 'single'" disabled class="form-control" name="" id="" aria-describedby="helpId" placeholder="Something will be typed here...">
                    
                    <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2" disabled placeholder="Something will be typed here..."></textarea>
                  
                    <div ng-if="field.type == 'email'"  class="input-group">
                      <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" disabled>
                    </div>

                    <div ng-if="field.type == 'url'"  class="input-group">
                      <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" disabled>
                    </div>

                    <div ng-if="field.type == 'hidden'"  class="input-group">
                      <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" ng-model="field.text" placeholder="Type label here" aria-describedby="basic-addon1">
                    </div>


                    <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" onclick="$('#uploadFileInput').click()">
                      <i class="fa fa-upload" aria-hidden="true"></i> Add File
                    </button>

                    <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" disabled>
                      <i class="fa fa-calendar" aria-hidden="true"></i> Date will be set here
                    </button>

                  </div>

                </div>

                <div class="emailer-holder" ng-if="content.module == 'email'">
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">To</label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Cc</label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Bcc</label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Subject</label>
                    <div class="col-sm-9">
                      <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Body</label>
                    <div class="col-sm-10">
                      <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                    <div class="col-sm-8">
                      <button class="btn btn-default" ng-click="sendTaskContentEmail(content.email)">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i> Test Send
                      </button>
                    </div>
                  </div>
                </div>

                <div class="move-handle">
                  <button class="btn btn-thirdy btn-copy btn-sm" role="button" ng-click="copyContent(content, $index)" >
                    <i aria-hidden="true" class="fa fa-copy fa-fw"></i>
                  </button>

                  <button ng-if="contents.length > 1; $index != (contents.length - 1)" class="btn btn-thirdy btn-down btn-sm" role="button" ng-click="moveContent($index, 'down')">
                    <i aria-hidden="true" class="fa fa-chevron-down"></i>
                  </button>
                </div>

              </div>
              <!-- editor-holder -->
            </div>

            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12">
                  <!-- <div class="preview" ng-if="!incidentFormEditing">
                    <div class="form-group" ng-repeat="content in contents track by $index">
                   
      
                      <div class="contents-holder list-margin-holder" ng-if="content.module == 'list'">
      
                        <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
      
                          <label for="">{{field.text}}</label>
      
      
                          <input type="text" ng-if="field.type == 'single'" disabled class="form-control" name="" id="" aria-describedby="helpId" placeholder="Something will be typed here...">
                          
                          <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2" disabled placeholder="Something will be typed here..."></textarea>
                        
      
                        </div>
      
                      </div>
      
                    </div>
                  </div> -->
                </div>
              </div>
            </div>




            <p class="text-center text-muted lead" style="padding: 140px 0;" ng-if="!contents.length">
              No content added.
            </p>



          </div>
        
        </div>
    </div><!--.row -->


    <div class="sticky-rightbar" ng-if="viewListMode && incidentFormEditing">
      <button class="btn btn-success btn-savechanges" ng-click="saveChanges()">
        <span ng-if="loading">
          <i class="fa fa-spinner fa-spin" aria-hidden="true" uib-tooltip="Save Form"></i> Saving..
        </span>
        <span ng-if="!loading">
          <i class="fa fa-save"></i> Publish
        </span>
      </button>
      <div class="scroll-content">
        <div ui-on-Drop="onDrop($event,$data,formElements)" class="list-group list-group-form-element">
            <a class="list-group-item" ui-draggable="true" drag-channel="A" drop-channel="A" drag="formEl" drop-validate="dropValidate(1, $data)" ng-click="clickFormElement(formEl)" on-drop-success="dropSuccessHandler($event,$index,formElements)"
            ng-repeat="formEl in formElements2 track by $index">
                <i class="fa fa-fw" ng-class="formEl.icon" aria-hidden="true"></i> {{formEl.name}}
            </a>
        </div>
      </div>
    </div>
    

  </div>
</div>
  
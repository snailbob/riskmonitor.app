
<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



	<title><?php echo $this->common_model->appName(); ?> | <?php echo 'Cloud-based product recall management software'; ?> </title>
	


    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">


<!-- Popup css-->

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->


	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/2/js/respond.min.js"></script>

    <![endif]-->

    <!-- JavaScript - linkedin api key spacelli -75tpfodcr9xyfr -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url().'assets/2/js/plugins/validate/jquery.validate.min.js' ?>"></script>

	<script type="text/javascript" src="https://platform.linkedin.com/in.js">
	  api_key: 75eekrdxppqx8g
    </script>

	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}

		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}

		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}

		.or{
			text-align: center;
			position: relative;
			top: -20px;
			margin-bottom: 0px;
		}
		.or span{
			background-color: #fff;
		}
	</style>


</head>

<body class="hidden" style="background: #fff;">

    <script type="text/javascript">
      // Enter a client ID for a web application from the Google Developer Console.
      // The provided clientId will only work if the sample is run directly from
      // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
      // In your Developer Console project, add a JavaScript origin that corresponds to the domain
      // where you will be running the script.
      var clientId = '1041052586833-p5gn0f5828unp0ve0t8i0girpp8mgba8.apps.googleusercontent.com';

      // Enter the API key from the Google Develoepr Console - to handle any unauthenticated
      // requests in the code.
      // The provided key works for this sample only when run from
      // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
      // To use in your own application, replace this API key with your own.
      var apiKey = 'AIzaSyALyiCnNODtJToaFtuZHESkc_ZHdfx_GRc'; //AIzaSyCxXrGNIjpnWIf6FlqbbwlSp-oYYgfTADU'; //AIzaSyAVK5bSoKOZh4tjiy3NpJPFXzc8qCktm4w';

      // To enter one or more authentication scopes, refer to the documentation for the API.
      var scopes = 'https://www.googleapis.com/auth/plus.profile.emails.read'; //plus.me';

      // Use a button to handle authentication the first time.
      function handleClientLoad() {
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth,1);
      }

      function checkAuth() {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true} , handleAuthResult);
      }


      function handleAuthResult(authResult) {
        var authorizeButton = document.getElementById('authorize-button');

         //authorizeButton.style.visibility = '';
         // authorizeButton.onclick = handleAuthClick;
		  console.log(JSON.stringify(authResult));

			if (authResult && !authResult.error) {
			//authorizeButton.style.visibility = 'hidden';
			//	makeApiCall();
				//console.log(JSON.stringify(authResult));

			}

			else {
				//bootbox.alert('<p class="lead"><i class="fa fa-info-circle"></i> Something went wrong. Please contact admin.</p>');
			}
      }

      function handleAuthClick(event) {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, makeApiCall);
		//handleAuthResult);
		//makeApiCall();

        return false;
      }

      // Load the API and make an API call.  Display the results on the screen.
      function makeApiCall() {
        gapi.client.load('plus', 'v1', function() {
          var request = gapi.client.plus.people.get({
            'userId': 'me'
          });
          request.execute(function(resp) {
				//console.log(JSON.stringify(resp));
				console.log(resp.emails[0].value);
				//console.log(resp.displayName);
				console.log(resp.id);
				console.log(resp.name.givenName);
				console.log(resp.name.familyName);
				console.log(resp.image.url);
				//console.log(resp.placesLived[0].value);


				$('.gplus_btn').addClass('disabled');
				var dataa = {
					"btn_cc_login": '',
					"user_name": 'a',
					"pass_word": 'a',
					"email": resp.emails[0].value,
					"id": resp.id,
					"type": 'gplus'
				};

				dataa = $.param(dataa); // $(this).serialize() + "&" +

				$.post(
					base_url+'signin',
					dataa,
					function(res){
						console.log(res);
						if(res.result == 'not_exist'){
							bootbox.alert('<p class="lead">No user found.</p>');
							$('.gplus_btn').removeClass('disabled');
						}
						else{

							$('.panel-body').slideUp('fast', function(){
								$(this).html('<div class="alert alert-success">'+res.success+'</div>').show();
							});

							setTimeout(function(){
								window.location.reload(true);
							}, 1500);

							//window.location.reload(true);
							//window.location.href = base_url + res.rdr;
						}
					},
					'json'
				);


//            var heading = document.createElement('h4');
//            var image = document.createElement('img');
//            image.src = resp.image.url;
//            heading.appendChild(image);
//            heading.appendChild(document.createTextNode(resp.displayName));
//            document.getElementById('content').appendChild(heading);


          });
        });
      }
    </script>
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>




	<nav class="navbar" style="padding-top: 0px">
		<div class="container-fluid">
			<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>">
				<img src="<?php echo base_url()?>assets/2/img/logo.png" alt="">
			</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row" style="margin-bottom: 50px;">
			<div class="col-md-8 col-md-offset-2 new-modal-section" style="margin-top: 50px">
				<span class="pull-right" style="margin-top: 25px;">
					<small>
						Don't have an account?<br>
						<a href="<?php echo base_url().'signup'?>" class="pull-right">Sign Up</a>
					</small>
				</span>
				<h1 class="text-left">Sign into your account</h1>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<div class="row  bg-grey">


					<div class="col-md-6" style="border-right: 1px solid #fff">


						<div id="login_form">

							<div class="alert alert-danger hidden login_err_message" style="margin-bottom: 15px;"></div>

							<form id="userlogin_form">

								<?php if($success!="") { ?>

							<div class="alert alert-success alert-dismissable">

							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

							<strong>Success!</strong> <?php echo $success;   ?>

							</div>

							<?php } if($error!="")  { ?>
							<div class="alert alert-danger alert-dismissable">

							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?php echo $error;?>
							</div>

							<?php } ?>



								<fieldset>
									<div class="form-group">
										<label for="">Enter your email</label>
										<input class="form-control" placeholder="Email Address" type='text' name='user_name' id='user_name' autofocus value="<?php if(isset($_COOKIE['email'])) { echo $_COOKIE['email'];}?>"/>
									</div>

									<div class="form-group">
										<label for="">Enter your password</label>
										<input class="form-control" placeholder="Password" type='password' name='pass_word' id='pass_word' value="<?php if(isset($_COOKIE['password'])) { echo $_COOKIE['password'];}?>"/>
									</div>

									<div class="form-group">
										<div class="checkbox">
											<label>
												<input type="checkbox" value="Y" name="remember" <?php if(isset($_COOKIE['remember'])) { echo 'checked="checked"';} ?>> Remember me
											</label>
										</div>
									</div>

									<p class="">
										<a href="<?php echo base_url().'cc/dashboard/forgotpassword' ?>">Forgot your password?</a>
									</p>
									<div class="form-group">
										<button class="btn btn-primary btn-lg btn-block" name="btn_cc_login" id="btn_cc_login">
											Sign In <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
										</button>
									</div>

								</fieldset>


							</form>

						</div>

					</div>

					<div class="col-md-6">

						<h4 style="font-size: 16px">
							Or sign in with your LinkedIn or Google Account
						</h4>
						<p>
							It's quick, easy, and secure - your CrisisFlo data will be completely private.


						</p>

						<a href="javascript:;" class="btn btn-default btn-block gplus_btn social-sign-up-btn google-btn" onClick="handleAuthClick();">
							Login with Google
						</a>

						<a href="javascript: void(0);" id="MyLinkedInButton" class="btn btn-default btn-block linkedin_btn social-sign-up-btn linkedin-btn">
							Login with LinkedIn
						</a>



					</div>

				</div>
			</div>


		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="myOrgsModal" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close refresh_page" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Select Organization</h4>
			</div>
			<form id="org_select_form">
				<div class="modal-body">
						<div class="form-group">
						<!-- <label for=""></label> -->
						<select class="form-control" name="org_id" id="">
							
						</select>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default refresh_page" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">OK</button>
				</div>
			</form>

		</div>
	</div>
	</div>

<script>

$(document).ready(function(){
	$('.refresh_page').on('click', function(){
		window.location.reload(true);
	});

	$('#org_select_form').validate({
		submitHandler: function(form) {
			$(form).find('[type="submit"]').button('loading');

			$.post(
				base_url+'signin/org_select_form',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);
				},
				'json'
			).error(function(err){
					console.log(err);
			});

		}
	});

	$('#userlogin_form').validate({

		rules: {
			user_name: {
				required: true,
				email: true
			},
			pass_word: {
				required: true
			},
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		submitHandler: function(form) {
			$(form).find('button').button('loading');
			$(form).find('.login_err_message').addClass('hidden');

			var dataa = {
				"email": 'a@a.com',
				"id": '0',
				"type": 'email'
			};

			dataa = $(form).serialize() + "&" + $.param(dataa);
			console.log(dataa);

			$.post(
				base_url+'signin',
				dataa,
				function(res){
					console.log(res);

					if(res.result == 'ok'){
						$(form).find('button').html('Success! <i class="fa fa-check-circle"></i>');


						if(res.user_info.user_orgs.length > 1){
							res.user_info.user_orgs.forEach(function(element) {
								console.log(element);
								$('[name="org_id"]').append('<option value="'+element.org_id+'">'+element.name+'</option>');
								
							});

							$('#myOrgsModal').modal('show');
						}
						else{
							setTimeout(function(){
								window.location.reload(true);
							}, 1700);

							//check payment info
							$.get(base_url+"apiv2/get_billing_info", function (res_billing) {
								localStorage.setItem('crisisflo_billing', JSON.stringify(res_billing));
							}, 'json');
						}

						// $('.panel-body').slideUp('fast', function(){
						// 	$(this).html('<div class="alert alert-success">'+res.success+'</div>').show();
						// });

					}
					else{
						$('.login_err_message').html(res.error).removeClass('hidden');
						$(form).find('button').button('reset');
					}

				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button').button('reset');

			});

		}
	});

	$(".user_name").focus();
});


// Handle the successful return from the API call
function onSuccess(data) {
	console.log(data);

	$('#MyLinkedInButton').addClass('disabled');
	var dataa = {
		"btn_cc_login": '',
		"user_name": 'a',
		"pass_word": 'a',
		"email": data.emailAddress,
		"id": data.id,
		"type": 'linkedin'
	};

	dataa = $.param(dataa); // $(this).serialize() + "&" +

	$.post(
		base_url+'signin',
		dataa,
		function(res){
			console.log(res);
			if(res.result == 'not_exist'){
				bootbox.alert('<p class="lead">No user found.</p>');
				$('#MyLinkedInButton').removeClass('disabled');
			}
			else{
				$('.panel-body').slideUp('fast', function(){
					$(this).html('<div class="alert alert-success">'+res.success+'</div>').show();
				});

				setTimeout(function(){
					window.location.reload(true);
				}, 1500);
				//window.location.href = base_url + res.rdr;
			}
		},
		'json'
	);


}

// Handle an error response from the API call
function onError(error) {
	console.log(error);
}

$(document).ready(function(e) {

	$("#MyLinkedInButton").bind('click',function () {IN.User.authorize(); return false;});

	IN.Event.on(IN, 'auth', function(){
		/* your code here */
		//for instance
		console.log('auth success');
		IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);

//		IN.API.Profile("me").result(function(res){
//			var linuser = res.values[0];
//			//console.log(linuser);
//			console.log(JSON.stringify(res));
//
//		});
	});


	setTimeout(function() {
		$('body').removeClass('hidden');
	}, 600);
});


</script>
</body>

</html>


<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



	<title><?php echo $this->common_model->appName(); ?> | <?php echo 'Cloud-based product recall management software'; ?> </title>
	


    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">


<!-- Popup css-->

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->


	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/2/js/respond.min.js"></script>

    <![endif]-->

    <!-- JavaScript - linkedin api key spacelli -75tpfodcr9xyfr -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url().'assets/2/js/plugins/validate/jquery.validate.min.js' ?>"></script>

	<script type="text/javascript" src="https://platform.linkedin.com/in.js">
	  api_key: 75eekrdxppqx8g
    </script>

	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}

		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}

		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}

		.or{
			text-align: center;
			position: relative;
			top: -20px;
			margin-bottom: 0px;
		}
		.or span{
			background-color: #fff;
		}
	</style>


</head>

<body class="hidden" style="background: #fff;">

    <script type="text/javascript">
      // Enter a client ID for a web application from the Google Developer Console.
      // The provided clientId will only work if the sample is run directly from
      // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
      // In your Developer Console project, add a JavaScript origin that corresponds to the domain
      // where you will be running the script.
      var clientId = '1041052586833-p5gn0f5828unp0ve0t8i0girpp8mgba8.apps.googleusercontent.com';

      // Enter the API key from the Google Develoepr Console - to handle any unauthenticated
      // requests in the code.
      // The provided key works for this sample only when run from
      // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
      // To use in your own application, replace this API key with your own.
      var apiKey = 'AIzaSyALyiCnNODtJToaFtuZHESkc_ZHdfx_GRc'; //AIzaSyCxXrGNIjpnWIf6FlqbbwlSp-oYYgfTADU'; //AIzaSyAVK5bSoKOZh4tjiy3NpJPFXzc8qCktm4w';

      // To enter one or more authentication scopes, refer to the documentation for the API.
      var scopes = 'https://www.googleapis.com/auth/plus.profile.emails.read'; //plus.me';

      // Use a button to handle authentication the first time.
      function handleClientLoad() {
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth,1);
      }

      function checkAuth() {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true} , handleAuthResult);
      }


      function handleAuthResult(authResult) {
        var authorizeButton = document.getElementById('authorize-button');

         //authorizeButton.style.visibility = '';
         // authorizeButton.onclick = handleAuthClick;
		  console.log(JSON.stringify(authResult));

			if (authResult && !authResult.error) {
			//authorizeButton.style.visibility = 'hidden';
			//	makeApiCall();
				//console.log(JSON.stringify(authResult));

			}

			else {
				//bootbox.alert('<p class="lead"><i class="fa fa-info-circle"></i> Something went wrong. Please contact admin.</p>');
			}
      }

      function handleAuthClick(event) {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, makeApiCall);
		//handleAuthResult);
		//makeApiCall();

        return false;
      }

      // Load the API and make an API call.  Display the results on the screen.
      function makeApiCall() {
        gapi.client.load('plus', 'v1', function() {
          var request = gapi.client.plus.people.get({
            'userId': 'me'
          });
          request.execute(function(resp) {
				//console.log(JSON.stringify(resp));
				console.log(resp.emails[0].value);
				//console.log(resp.displayName);
				console.log(resp.id);
				console.log(resp.name.givenName);
				console.log(resp.name.familyName);
				console.log(resp.image.url);
				//console.log(resp.placesLived[0].value);

				$('[name="first_name"]').val(resp.name.givenName);
				$('[name="last_name"]').val(resp.name.familyName);
				$('[name="email"]').val(resp.emails[0].value);
				$('[name="gplus_id"]').val(resp.id);
				

				$('.gplus_btn').addClass('disabled');
				var dataa = {
					"btn_cc_login": '',
					"user_name": 'a',
					"pass_word": 'a',
					"email": resp.emails[0].value,
					"id": resp.id,
					"type": 'gplus'
				};

				dataa = $.param(dataa); // $(this).serialize() + "&" +

				// $.post(
				// 	base_url+'signin',
				// 	dataa,
				// 	function(res){
				// 		console.log(res);
				// 		if(res.result == 'not_exist'){
				// 			bootbox.alert('<p class="lead">No user found.</p>');
				// 			$('.gplus_btn').removeClass('disabled');
				// 		}
				// 		else{

				// 			$('.panel-body').slideUp('fast', function(){
				// 				$(this).html('<div class="alert alert-success">'+res.success+'</div>').show();
				// 			});

				// 			setTimeout(function(){
				// 				window.location.reload(true);
				// 			}, 1500);

				// 			//window.location.reload(true);
				// 			//window.location.href = base_url + res.rdr;
				// 		}
				// 	},
				// 	'json'
				// );


//            var heading = document.createElement('h4');
//            var image = document.createElement('img');
//            image.src = resp.image.url;
//            heading.appendChild(image);
//            heading.appendChild(document.createTextNode(resp.displayName));
//            document.getElementById('content').appendChild(heading);


          });
        });
      }
    </script>
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>




	<nav class="navbar" style="padding-top: 0px">
		<div class="container-fluid">
			<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>">
				<img src="<?php echo base_url()?>assets/2/img/logo.png" alt="">
			</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row" style="margin-bottom: 50px;">
			<div class="col-md-8 col-md-offset-2 new-modal-section" style="margin-top: 50px">
				<span class="pull-right" style="margin-top: 25px;">
					<small>
						Already have an account?<br>
						<a href="<?php echo base_url().'signin'?>" class="pull-right">Sign In</a>
					</small>
				</span>
				<h1 class="text-left">Try now for free</h1>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<div class="row  bg-grey">


					<div class="col-md-6" style="border-right: 1px solid #fff">




						<form id="trial_form" class="text-left">
							<input type="hidden" name="gplus_id">
							<input type="hidden" name="linkedin_id">
							<div class="alert trial_message alert-dismissable text-left" style="display: none;">
								Form successfully submitted.
							</div>

							<fieldset>


								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">First Name</label>
											<input type="text" class="form-control" name="first_name"/>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">Last Name</label>
											<input type="text" class="form-control" name="last_name"/>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Business Email</label>
									<input type="text" class="form-control the_email_field" name="email" value="<?php echo (isset($_GET['email'])) ? $_GET['email'] : '' ?>"/>
								</div>

								<div class="form-group">
									<label class="control-label">Unique Team Domain <i class="fa fa-info-circle text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-title="This is where you will access your new account. Make sure to bookmark it once you’re inside."></i></label>
									<div class="input-group">
										<input type="text" name="organization" class="form-control the_domain_field" placeholder="" aria-describedby="basic-addon2">
										<span class="input-group-addon the_domain_text" style="border-radius: 0" id="basic-addon2">.riskmonitor.app</span>
									</div>
									<p class="text-muted the_domain_text2 hidden">
	  									<i class="fa fa-info-circle" aria-hidden="true"></i> That domain is taken.
									</p>

									<!-- <input type="text" class="form-control organization_field" name="organization"/> -->
								</div>

								<div class="hidden">
									
									<div class="form-group">
										<label class="control-label">Domain <span class="the_domain_text"></span></label>
										<div class="input-group">
										<input type="text" class="form-control domain_field" name="domain"/>
										<span class="input-group-addon" id="basic-addon2">.riskmonitor.app</span>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label">Address</label>
										<input type="text" class="form-control geocomplete" placeholder="Enter location" name="address"/>
										<input type="hidden" class="form-control" name="lat"/>
										<input type="hidden" class="form-control" name="lng"/>
									</div>




									<div class="row">
										<div class="col-sm-5">
											<div class="form-group">
												<label class="control-label">Country Calling Code</label>
												<select class="form-control" name="country_code">
													<option value="" data-code="">Select..</option>
													<?php
														foreach($mobile_code as $r=>$mc){
															echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
														}
													?>
												</select>
											</div>
										</div>

										<div class="col-sm-7">
											<div class="form-group mobile_input">
												<label class="control-label">Mobile Number</label>
												<div class="input-group">
												<span class="input-group-addon addon-shortcode">+</span>
												<input type="text" class="form-control" placeholder="Mobile Number" name="mobile">
												</div>
												<input type="hidden" name="country_short" value=""/>
											</div>
										</div>
									</div>

								</div> 



								<div class="form-group hidden">
									<label class="control-label">Subscription Plan</label>
									<input type="hidden" name="plan" value="5"/>
								</div>


								<p class="text-right"><small>By signing up, you agree to CrisisFlo's <a href="#" class="terms_btn">Terms of Use</a> and <a href="#" class="privacy_btn">Privacy Policy</a></small></p>


								<div class="form-group">
									<button type="submit" class="btn btn-primary btn-lg btn-radius btn-block">
										Sign Up <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
									</button>
								</div>

							</fieldset>

						</form>



					</div>

					<div class="col-md-6">

						<h4 style="font-size: 16px">
						Or sign up with Google or LinkedIn
						</h4>
						<p>
							Now you can link your accounts and sign in to CrisisFlo using your Google or LinkedIn account. It's super quick, easy, and secure - your CrisisFlo data will be completely private.
						</p>

						<a href="javascript:;" class="btn btn-default btn-block gplus_btn social-sign-up-btn google-btn" onClick="handleAuthClick();">
							Signup with Google
						</a>

						<a href="javascript: void(0);" id="MyLinkedInButton" class="btn btn-default btn-block linkedin_btn social-sign-up-btn linkedin-btn">
							Signup with LinkedIn
						</a>



					</div>

				</div>
			</div>


		</div>
	</div>




    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>CrisisFlo - Terms of Use</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->

	
    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


<script>

$(document).ready(function(){

	$('.the_email_field').on('change', function(){
		var $self = $(this);
		var dmn = $self.val();

		var email = dmn;
		var domain = email.replace(/.*@/, "");
		domain = domain.split('.');

		dmn = domain[0].replace(/\s/g, '');
		dmn = dmn.toLowerCase();
		var $dmn_fld = $('.the_domain_field');
		var $the_domain_text = $('.the_domain_text');
		var thdmn = '.riskmonitor.app';
		$dmn_fld.val(dmn);
		$dmn_fld.change();
		$the_domain_text.removeClass('hidden').html(thdmn);

	});

	$('.the_domain_field').on('focus', function(){
		$('.the_email_field').change();
	});

	$('.the_domain_field').on('keyup change', function(){
		var $self = $(this);
		var dmn = $self.val();
		dmn = dmn.replace(/[^\w\s]/gi, ''); //replace(/\s/g, '');
		dmn = dmn.toLowerCase();
		var $the_domain_text2 = $('.the_domain_text2');
		var $the_domain_text = $('.the_domain_text');
		var thdmn = '.riskmonitor.app'; //dmn +

		if(dmn != ''){
			$the_domain_text.removeClass('hidden').html(' <i class="fa fa-spinner fa-spin"></i> ' + thdmn);
			$self.val(dmn);

			$.post(
				base_url+'signup/validate_domain',
				{domain: dmn},
				function(res){
					console.log(res);

					if(res.count > 0){
						$the_domain_text.html(' <i class="fa fa-times-circle text-danger"></i> ' + thdmn);
						$the_domain_text2.removeClass('hidden'); //.html('<a href="#" class="im_broker" data-id="'+res.org[0].id+'">Broker of this company?</a>');

						$the_domain_text.closest('form').find('button[type="submit"]').attr('disabled', true);
					}
					else{
						$the_domain_text2.addClass('hidden');
						$the_domain_text.html(' <i class="fa fa-check-circle text-success"></i> ' + thdmn);
						$the_domain_text.closest('form').find('button[type="submit"]').attr('disabled', false);
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});

		}

	});


	$('[data-toggle="tooltip"]').tooltip();
	$('.terms_btn').on('click', function(){
		$( "#termsModal" ).modal('show');
		$( "#termsModal" ).find('.text_content').html('<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p>');
		$( "#termsModal" ).find('.modal-footer').hide();

		$( "#termsModal" ).find('.text_content').load( base_url+'frontpage/anewskin/terms', function() {
		  console.log('yeah');
	  	  $( "#termsModal" ).find('.modal-footer').show();
		});

		return false;
	});


	$('.privacy_btn').on('click', function(){
		$( "#privacyModal" ).modal('show');
		$( "#privacyModal" ).find('.text_content').html('<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p>');
		$( "#privacyModal" ).find('.modal-footer').hide();

		$( "#privacyModal" ).find('.text_content').load( base_url+'frontpage/anewskin/privacy', function() {
		  console.log('yeah');
	  	  $( "#privacyModal" ).find('.modal-footer').show();
		});

		return false;
	});



	//trial_form
	$('#trial_form').validate({
		rules: {
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			// address: {
			// 	required: true
			// },
			organization: {
				required: true
			},
			plan: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			$(element).closest('.form-group').append(error);
		},

		submitHandler: function(form) {

			var lat = $(form).find('.geocomplete').siblings('input').val();

			// if(lat == ''){
			// 	bootbox.alert('Please select Business Address from suggestion dropdown.');
			// 	return false;
			// }

			$(form).find('button').html('Processing..').prop('disabled', true);
			var data = {
				form_data: $(form).serializeArray(),
				calling_code: $(form).find('[name="country_code"]').find(':selected').data('code')
			};

			data = $(form).serialize() +'&'+ $.param(data);
			console.log(data);


			$.post(
				base_url+'frontpage/anewskin/trial_customer',
				data, //$(form).serialize(),
				function(res){
					console.log(res);
					if(res.result == 'ok'){
						$(form).find('.trial_message').hide().removeClass('alert-danger').addClass('alert-success').html('Yeah');
						$('.modal').modal('hide');
						bootbox.alert('<p class="lead">'+res.message+'</p>', function(){
							window.location.href = base_url+'cc/dashboard';
						});
						$(form).find('button').html('<i class="fa fa-check-circle"></i> Success').prop('disabled', true);
					}
					else{
						$(form).find('button').html('Signup').prop('disabled', false);
						$(form).find('.trial_message').show().removeClass('alert-success').addClass('alert-danger').html('User email already exist.');
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});


		}
	});




	$('[name="first_name"]').focus();
});


// Handle the successful return from the API call
function onSuccess(data) {
	console.log(data);

	$('#MyLinkedInButton').addClass('disabled');
	var dataa = {
		"btn_cc_login": '',
		"user_name": 'a',
		"pass_word": 'a',
		"email": data.emailAddress,
		"id": data.id,
		"type": 'linkedin'
	};

	dataa = $.param(dataa); // $(this).serialize() + "&" +

	$('[name="first_name"]').val(data.firstName);
	$('[name="last_name"]').val(data.lastName);
	$('[name="email"]').val(data.emailAddress);
	$('[name="linkedin_id"]').val(data.id);
	// $.post(
	// 	base_url+'signin',
	// 	dataa,
	// 	function(res){
	// 		console.log(res);
	// 		if(res.result == 'not_exist'){
	// 			bootbox.alert('<p class="lead">No user found.</p>');
	// 			$('#MyLinkedInButton').removeClass('disabled');
	// 		}
	// 		else{
	// 			$('.panel-body').slideUp('fast', function(){
	// 				$(this).html('<div class="alert alert-success">'+res.success+'</div>').show();
	// 			});

	// 			setTimeout(function(){
	// 				window.location.reload(true);
	// 			}, 1500);
	// 			//window.location.href = base_url + res.rdr;
	// 		}
	// 	},
	// 	'json'
	// );


}

// Handle an error response from the API call
function onError(error) {
	console.log(error);
}

$(document).ready(function(e) {

	$("#MyLinkedInButton").bind('click',function () {IN.User.authorize(); return false;});

	IN.Event.on(IN, 'auth', function(){
		/* your code here */
		//for instance
		console.log('auth success');
		IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);

//		IN.API.Profile("me").result(function(res){
//			var linuser = res.values[0];
//			//console.log(linuser);
//			console.log(JSON.stringify(res));
//
//		});
	});


	$('body').removeClass('hidden');
});


</script>
</body>

</html>

<script type="text/javascript" language="javascript">
function chkmessagetype(val)
{
	if(val=='1')
	{
		document.getElementById('new_msg').style.display='none';
		document.getElementById('st_msg').style.display='block';	
		document.getElementById('btn_edit_stmes').style.display='';	
	}
	else
	{
		document.getElementById('st_msg').style.display='none';
		document.getElementById('new_msg').style.display='block';
		document.getElementById('btn_edit_stmes').style.display='none';	
			
	}

}

</script>
<style>
.dataTables_length, .dataTables_filter{
	display: none;
}

</style>


                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1><?php echo 'Initiate Response'; ?>
                               <!-- <small><?php echo 'Initiate Response'; ?></small>-->
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>cc/">Dashboard</a></li>
                                <li class="active"><?php echo 'Initiate Response'; ?></li>

                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->


                

                <!-- Form AREA -->
				<div class="row">
                
                    <div class="col-lg-12" style="height: 50px; padding-left: 30px;">
                        <p class="lead">
							<?php if(count($response) > 0) {
                                foreach($response as $response){
								}
								
								$smsbal = $response['sms_credit'];
								//echo ' US$ '.$smsbal <b>SMS Balance: </b>;
							} ?>
                            
                            <a href="#" data-toggle="modal" data-target="#mySmsReport" class="btn btn-primary pull-right">View SMS Report</a>
<!-- Modal -->
<div class="modal fade" id="mySmsReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">SMS Usage Report</h4>
      </div>
      <div class="modal-body">
        <?php
			$sms_list=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0'));
			
			$failed_sms_list=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status !='=>'0'));
			?>
            
            <!-- Nav tabs category -->
            <ul class="nav nav-tabs faq-cat-tabs" style="margin-bottom: 10px;">
                <li class="active"><a href="#faq-cat-1" data-toggle="tab">Sent</a></li>
                <li><a href="#faq-cat-2" data-toggle="tab">Failed</a></li>
            </ul>


<!-- Tab panes -->
            <div class="tab-content faq-cat-content">
                <div class="tab-pane active in fade" id="faq-cat-1">

					<?php
                    if (count($sms_list) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="example-table"> <thead>
             <tr> <th width="40%">Recipient</th> <th width="29%">Time Sent</th> <th width="30%">Price</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($sms_list as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. date('d M Y - H:i:s',strtotime($value['date_sent'])) .'</td>';
                            echo '<td>US$ '. $value['price'] .'</td></tr>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>

                </div>
                
                
                <!--second tab-->
                <div class="tab-pane fade" id="faq-cat-2">

					<?php
                    if (count($failed_sms_list) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="stake-table"> <thead>
             <tr> <th width="50%">Recipient</th> <th width="50%">Status</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($failed_sms_list as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. $value['status'] .'</td>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>
    


                </div>
            </div>


		
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal for individual sms log -->
<div class="modal fade" id="mySmsReportLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">SMS Usage Report</h4>
      </div>
      <div class="modal-body">
        <?php
			$sms_log=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0','log_scenario_id'=>$this->uri->segment(4)));
			
			$failed_sms_log=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status !='=>'0','log_scenario_id'=>$this->uri->segment(4)));
			?>
            
            <!-- Nav tabs category -->
            <ul class="nav nav-tabs faq-cat-tabs" style="margin-bottom: 10px;">
                <li class="active"><a href="#faq-cat-11" data-toggle="tab">Sent</a></li>
                <li><a href="#faq-cat-22" data-toggle="tab">Failed</a></li>
            </ul>


<!-- Tab panes -->
            <div class="tab-content faq-cat-content">
                <div class="tab-pane active in fade" id="faq-cat-11">

					<?php
                    if (count($sms_log) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="example-table"> <thead>
             <tr> <th width="40%">Recipient</th> <th width="29%">Time Sent</th> <th width="30%">Price</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($sms_log as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. date('d M Y - H:i:s',strtotime($value['date_sent'])) .'</td>';
                            echo '<td>US$ '. $value['price'] .'</td></tr>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>

                </div>
                
                
                <!--second tab-->
                <div class="tab-pane fade" id="faq-cat-22">

					<?php
                    if (count($failed_sms_log) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="stake-table"> <thead>
             <tr> <th width="50%">Recipient</th> <th width="50%">Status</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($failed_sms_log as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. $value['status'] .'</td>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>
    


                </div>
            </div>


		
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

                            
						</p>
                    </div>
                        
                
                    <div class="col-lg-12">
                    	<?php 
						if($this->session->flashdata('success')!="")
						{
						?>
                        <div class="alert alert-success alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>
                        <?php    
						} 
						if($this->session->flashdata('error')!="")
						{
						?>
                        <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>
                        <?php
						} 
						?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4><?php echo 'Initiate Response'; ?></h4>
                                </div>
                                <div class="panel-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="panel-body">
									
                                    <form class="form-horizontal" role="form" action='' method='post' name="initiate_repose" >
                                       <div class="form-group">
                                            <label class="col-sm-2 control-label">Response Type</label>
                                            <div class="col-sm-10">
                                                <label class="radio-inline">
                                                    <input type="radio" name="radiovalit" value="0">Mock
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="radiovalit" value="1">Live
                                                </label>
                                                <div class="text-red" id="err_it"></div>
                                            </div>
                                        </div>
                                      
                                       <div class="form-group">
                                            <label class="col-sm-2 control-label">Message Type</label>
                                            <div class="col-sm-10">

                                                <label class="radio-inline">
                                                    <input type="radio" name="radioval" value="1" onclick="return chkmessagetype(this.value);" <?php   echo ' checked="checked"';?>>Standby Message
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="radioval" value="2" onclick="return chkmessagetype(this.value);">New Message
                                                </label>

                                            </div>
                                        </div>
                                      
                                        <div class="form-group">
                                            <label for="standmess_scene" class="col-sm-2 control-label">Crisis Scenario</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="initiate_scenario" name="scenario">
                                                    <option value="">Select One</option>
													<?php if(count($result_scene) > 0) {
														foreach($result_scene as $sc){
														?>
                                                        <option value="<?php echo $sc['scenario_id']?>">
														<?php echo $sc['scenario']?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>     
                                        
                                        <!--<div class="form-group" id="st_msg" >
                                            <label for="standmess_msg" class="col-sm-2 control-label">Standby Message</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="initiate_standbymsg" name="standbymsg">
                                                    <option value="">Select</option>
                                                    <?php if(count($stby_msg) > 0) {
														foreach($stby_msg as $sc){
														?>
                                                        <option value="<?php echo $sc['stn_msg_id']?>">
														<?php echo $sc['standby_message']?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>--> 
                                         
                                        <div class="form-group" id="st_msg" >
                                            <label for="standmess_msg" class="col-sm-2 control-label">Standby Message
                                            
                                            </label>
                                            <div class="col-sm-10">
                                            	<textarea class="form-control" id="initiate_standbymsg" name="standbymsg" rows="7" disabled="disabled"></textarea><div style="clear:both; height:5px;"></div>
                                                <button type="button" class="btn btn-primary btn-xs"  name="btn_edit_stmes" id="btn_edit_stmes" data-toggle="button">Edit</button>
                                                
                                                <input type="hidden" name="stnd_by_id" id="stnd_by_id" />
                                            </div>
                                        </div>                                      

                                        <div class="form-group" id="new_msg" style="display:none;">
                                            <label for="textArea" class="col-sm-2 control-label">Enter a new message</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="newmsg" name="newmsg" placeholder="" rows="7"></textarea>
                                                <div class="text-red" id="err_newmsg"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Notify via Email</label>
                                            <div class="col-sm-10">

                                                <label class="radio-inline">
                                                    <input type="radio" name="inform" value="crtonly">CRTs only
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="inform" value="crtstk">CRTs and Stakeholders
                                                </label>
												<div class="text-red" id="err_inform"></div>
                                            </div>
                                        </div>

                                        <div class="form-group hidden">
                                            <label class="col-sm-2 control-label">Notify via SMS</label>
                                            <div class="col-sm-10">
                                                <label class="radio-inline <?php if ($smsbal=='0'){echo 'text-muted';} ?>">
                                                    <input type="radio" name="informsms" value="crtonly" <?php if ($smsbal<='0'){echo 'disabled';} ?>>CRTs only
                                                </label>
                                                <label class="radio-inline  <?php if ($smsbal=='0'){echo 'text-muted';} ?>">
                                                    <input type="radio" name="informsms" value="crtstk" <?php if ($smsbal<='0'){echo 'disabled';} ?>>CRTs and Stakeholders
                                                </label>
												<div class="text-red" id="err_inform"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-primary"  name="btn_initiate_responce" id="btn_initiate_responce">Submit</button>
                                                
                                            </div>

                                        </div>
                                    </form>     
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!--.row-->


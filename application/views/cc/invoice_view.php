<div class="container all-task-content" ng-app="crisisApp">

  <div class="div-controller ng-cloak hidden" ng-controller="accountController as $ctrl">

    <div class="row">


      <div class="col-lg-12">
        <div class="pull-right text-right" style="padding-top: 20px;">
          <a name="" id="" class="btn btn-default" role="button" onclick="window.close()">
            CLOSE
          </a>
          <a name="" id="" class="btn btn-default" role="button" onclick="window.print()">
            PRINT
          </a>
        </div>
        <h1 class="text-muted">Invoice #INV-0000{{invoice.id}}</h1>

        <div style="padding: 15px 0;">
          <p class="pull-right">
            <strong>Pending</strong>
          </p>
          <p class="">
            <strong>{{invoice.next_month_format}}</strong>
          </p>
        </div>

        <table class="table">
          <thead class="" style="background: #ddd">
            <tr>

              <th>Description</th>
              <th>Subscription Period</th>
              <th>Price</th>
              <!-- <th>Quantity</th> -->
              <th>Amount</th>
            </tr>
          </thead>
          <tbody class="bg-white">
            <tr>
              <td>
                <span>CrisisFlo subscription</span>
                <!-- <span ng-if="invoice.orgs[0].org_crts.length">New Crisisflo user (x{{invoice.orgs[0].org_crts.length + 1}})</span> -->


              </td>
              <td>{{invoice.orgs[0].last_payment_format}} - {{invoice.next_month_format}}</td>
              <td>${{invoice.orgs[0].total_bill}}AUD</td>
              <!-- <td>{{invoice.orgs[0].org_crts.length + 1}}</td> -->
              <td>${{invoice.orgs[0].total_bill}}AUD</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <!-- <td></td> -->
              <td>
                <strong>${{invoice.orgs[0].total_bill}}AUD</strong>
              </td>
            </tr>
          </tbody>
        </table>

        <div class="row" style="margin-top: 15px;">
          <div class="col-sm-6">
            <div class="panel panel-default">
              <div class="panel-body">
                <p>
                  <strong>Billing Details</strong>
                </p>
                <p>
                  <strong>{{paymentInfo.name}}</strong>
                  <br> {{billingContact.company}}
                  <br> {{paymentInfo.address_line1}}
                  <br> {{paymentInfo.address_city}} {{paymentInfo.address_state}} {{paymentInfo.address_country}}, {{paymentInfo.address_zip}}
                  <br> {{paymentInfo.phone}}
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default" style="min-height: 150px;">
              <div class="panel-body">
                <p>
                  <strong>Note</strong>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--.row -->


  </div>
</div>
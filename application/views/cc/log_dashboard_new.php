
<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller ng-cloak hidden" ng-controller="dashboardController as $ctrl">

    <div class="row row-account-header">
        <div class="col-sm-12">
            <h3>Post-Incident Review</h3>
        </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="padding" style="padding-top: 15px;">

          <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
              <div class="row">

                <div class="col-sm-12">
                  <p class="lead text-muted text-center" style="margin-top: 150px" ng-if="!incidents.length && incidentsLoaded">
                    Nothing to show you
                  </p>

                  <div ng-repeat="incident in incidents track by $index">
                  
                    <a ng-click="deleteIncident($index, incident)" class="btn btn-danger pull-right" style="margin-right: -55px; margin-top: 30px;">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                    <div class="panel panel-default">
                      <div class="panel-body">
                      
                        <div class="row">
                          <div class="col-sm-6">
                            <span class="label label-success">{{incident.incident_status_text}}</span> {{incident.closed_date_format}}
                            <h4>
                              <a ng-href="<?php echo base_url().'cc/log/closed/'?>{{incident.id}}/{{incident.template_id}}" class="link-no-decor">
                                <small>{{incident.incident_no}} - {{incident.description}}</small>
                              </a>
                            </h4>
                          </div>
                          <div class="col-sm-6">
                            <div class="row gutter-0">
                              <div class="col-sm-5 text-center" style="border-right: 1px solid #999">
                                <h4 class="text-muted">
                                  {{incident.completion_rate.percentage}}% <br>
                                  <small>Completion rate</small>
                                </h4>

                              </div>  
                              <div class="col-sm-7 text-center">
                                <h4 class="text-muted">
                                  {{incident.total_time}} <br>
                                  <small>Elapsed time</small>
                                </h4>
                              </div>  
                            </div>  
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>





                </div>

                <!-- <div class="col-sm-3" ng-repeat="incident in incidents track by $index">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <h4>
                        <a ng-href="<?php echo base_url().'cc/log/closed/'?>{{incident.id}}">
                          <span class="text-muted">Closed Incident - {{incident.incident_no}}</span>
                        </a>
                      </h4>
                      <p class="">
                        {{incident.description}} <br>
                        <small>{{incident.incident_status_text}}</small>                          
                      </p>
                    </div>
                  </div>


                </div> -->


              </div>
            </div>
          </div>

        </div>

      </div>
    </div>


    <script type="text/ng-template" id="raiseModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Open Incident
            </h4>
        </div>
        <div class="modal-body">
            <ng-form name="nameDialog" novalidate role="form">
                <div class="form-group">
                    <label class="control-label" for="course">Short incident description</label>
                    <input type="text" class="form-control" name="description" id="description" ng-model="data.description" required>
                </div>
            </ng-form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Submit</button>
        </div>

    </script>

  </div>
</div>
  



<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller hidden ng-cloak" ng-controller="headerTasksController">

    <div class="kanban-view row col-headers" ng-if="!viewListMode">
        <div class="scroll-content" style="padding-top: 15px;">


          <div class="container">
          
            <div class="row">
              <div class="col-sm-12">
                <h2 ng-if="!incident_id">Preview Mode</h2>
                <h2>
                  <small class="pull-right">
                    <a class="btn btn-default btn-sm" target="_blank" href="<?php echo base_url().'pdfcreator/pdf/' ?>{{incident_id}}">
                      <i class="fa fa-file-text-o"></i> Generate Report
                    </a>
                  </small>

                  {{incident.incident_no}} 

                  <!-- Single button -->
                  <div class="btn-group" uib-dropdown>
                    <button id="single-button" type="button" class="btn btn-sm btn-success" uib-dropdown-toggle  ng-if="incident_id" ng-disabled="loading">
                      <span ng-if="!loading">
                        <span ng-if="incident.incident_status == '0'">
                          Open</span>
                        <span ng-if="incident.incident_status == '1'">
                          {{incident.incident_status_text}}</span>
                      </span>
                      <span ng-if="loading">
                          <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Saving changes..
                      </span>
                      
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                      <li role="menuitem">
                        <a ng-click="completeIncident()">
                          <span ng-if="incident.incident_status == '0'">
                            Close Incident</span>
                          <span ng-if="incident.incident_status == '1'">
                            Re-activate</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </h2>
                <p>{{incident.description}}</p>

              </div>
              <div class="col-sm-4">
                <h5><strong>To Do</strong></h5>
                <hr>
              </div>
              <div class="col-sm-4">
                <h5><strong>In Progress</strong></h5>
                <hr>
              </div>
              <div class="col-sm-4">
                <h5><strong>Done</strong></h5>
                <hr>
              </div>
            </div>

            <div class="row kanban-parent" vertilize-container ng-repeat="hTask in headerKanban track by $index" ng-init="headerIndex = $index" style="margin-bottom: 15px;">
              
              <div class="col-sm-12">
                <p class="kanban-header">
                  <i class="fa fa-fw"  title="toggle tasks" ng-class="{'fa-caret-down' : !hTask.isCollapsed, 'fa-caret-up' : hTask.isCollapsed}" aria-hidden="true" ng-click="hTask.isCollapsed = !hTask.isCollapsed"></i> 
                  <span>{{hTask.name}} </span>
                  <a class="btn btn-xs btn-default" title="click to update" ng-click="updateHeaderKan($index, hTask)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                  <!-- ng-click="deleteHeaderKan($index, hTask)" -->
                </p>
              </div>    
              <div class="col-sm-4" uib-collapse="hTask.isCollapsed" ng-repeat="disected_task in hTask.disected_tasks track by $index"ng-init="disectedIndex = $index" >
                <div class="panel-kanban-holder dndPlaceholder" vertilize ng-class="{'kanban-empty' : !disected_task.length}"
                >
                <!-- dnd-list="disected_task"
                dnd-inserted="logEvent($index, event, item)"
                dnd-allowed-types="hTask.allowed_type" -->
                  <div class="panel panel-default panel-kanban" ng-repeat="task in disected_task track by $index"
                  >
                  <!-- dnd-draggable="task"
                  dnd-type="task.task_type"
                  dnd-moved="taskMoved($index, $parent.$index, headerIndex)"
                  dnd-effect-allowed="move"
                  dnd-callback="task.assigned_info.full_name" -->

                    <div class="panel-body">
                      <img src="{{task.assigned_info.avatar}}" class="pull-right img-circle" width="30" alt="" uib-tooltip="{{task.assigned_info.full_name}}" >
                      <p>
                        <!-- <a ng-click="clickTaskBanModal(task, $index, disectedIndex, headerIndex)">{{task.format_task_id}}</a> -->
                        <!-- <span ng-if="disectedIndex == 1 || (disectedIndex != 2 && task.months)"> &middot; </span> -->
                        <small class="text-muted">
                          <!-- <span ng-if="disectedIndex == 1" am-time-ago="task.task_answer.created_at | amParse:'YYYY.MM.DD HH:mm:ss'"></span> -->
                          <span ng-if="disectedIndex == 2" ng-bind-html="formatDoneMoment(task)"></span>
                          <span ng-if="disectedIndex == 1" ng-bind-html="formatInProgressMoment(task.task_answer.created_at)"></span>
                          <span ng-if="disectedIndex != 2 && task.months" ng-bind-html="getDueDiff(task, 'text')" ng-class="getDueDiff(task, 'class')"></span>
                          </small>
                        <br>
                        <a ng-click="clickTaskBanModal(task, $index, disectedIndex, headerIndex)">{{task.question}}</a>
                        <!-- ng-click="editTaskModal($index, disectedIndex, headerIndex, task)"                       -->
                      </p>
                      <!-- <span am-time-ago="message.time"></span> -->

                    </div>
                  </div>

                </div>

              </div>
            </div>


          </div>


          <div class="header-kanban-actions">
            <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <!-- <div class="pull-right text-right">
                    <a name="" id="" class="btn btn-default btn-sm" role="button" ng-click="addHeaderKan()">
                      <i class="fa fa-plus" aria-hidden="true"></i> Heading
                    </a>
                    <a name="" id="" class="btn btn-default btn-sm" role="button" ng-click="addTaskBan()">
                      <i class="fa fa-plus" aria-hidden="true"></i> Task
                    </a>
                  </div> -->
                  <a class="btn btn-link btn-sm" ng-click="toggleListView()">
                    <i class="fa fa-exchange" aria-hidden="true"></i> List View
                  </a>

                </div>
              </div>
            </div>

          </div>


        </div>

      </div>
      <div class="row" ng-if="viewListMode">


        <div class="col-lg-5 col-headers">

          <div class="scroll-content">
            <h2 ng-if="!incident_id">Preview Mode</h2>

            <h2>

              {{incident.incident_no}} 
              <small>
                <span class="label label-success" ng-if="incident.incident_status == '1'">
                  <i class="fa fa-check-circle"></i> {{incident.incident_status_text}}
                </span>
              </small>
            </h2>
            <p>{{incident.description}}</p>
            
            <table class="table table-hover table-bordered table-rtask">
              <tbody id="steporder">

                  <tr ng-class="{'info': activeHeader == $index }" ng-repeat="hTask in headerTasks track by $index">
                    <td class="num" ng-click="focusHeader($index)">
                      <div class="no-drag">
                        {{$index + 1}}
                      </div>
                    </td>
                    <td class="text">
                      <div class="input-group">
                        <p class="form-control-static" ng-click="focusHeader($index)">
                          <strong>{{hTask.name}}</strong>
                        </p>
                        <!-- <input type="text" class="form-control form-control-header" placeholder="" ng-model="hTask.name"> -->
                        <span class="input-group-btn">
                          <button class="btn btn-link" ng-click=" hTask.show = !hTask.show">
                            <i class="fa" ng-class="{'fa-plus': !hTask.show ||  $index != activeHeader, 'fa-minus' :  $index == activeHeader && hTask.show, 'visibility-hidden' : $index != activeHeader }" aria-hidden="true"></i>
                          </button>
                        </span>
                      </div><!-- /input-group -->


                      <div  class="collapse" ng-class="{'in' : $index == activeHeader && hTask.show}" ng-attr-id="collapse{{$index}}">

                        <div class="list-group" style="margin-top: 15px; margin-right: 37px;">
                          <a href="#" class="list-group-item list-group-task" ng-class="{'active': activeTaskIndex == $index && $parent.$index == activeHeader}" ng-repeat="task in hTask.tasks track by $index" ng-click="clickTask(task.id, $index)">
                            
                            <span class="fa-stack" ng-click="completeTask(task, $index)" ng-if="incident.incident_status == '0'">
                              <i class="fa fa-square-o fa-stack-2x"></i>
                              <i class="fa fa-check fa-stack-1x" ng-if="task.checked && !task.loading"></i>
                              <i class="fa fa-minus fa-stack-1x" ng-if="!task.checked && task.in_progress && !task.loading"></i>
                              <i class="fa fa-spinner fa-spin fa-stack-1x" ng-if="task.loading"></i>
                            </span>
                            
                            <span class="fa-stack text-muted" ng-if="!incident.id || incident.incident_status != '0'">
                              <i class="fa fa-square-o fa-stack-2x"></i>
                              <i class="fa fa-check fa-stack-1x" ng-if="task.checked && !task.loading"></i>
                              <i class="fa fa-minus fa-stack-1x" ng-if="!task.checked && task.in_progress && !task.loading"></i>
                              <i class="fa fa-spinner fa-spin fa-stack-1x" ng-if="task.loading"></i>
                            </span>
                          
                            {{task.question}}
                              

                          </a>    

                        </div>

                      </div>
                    </td>
                  </tr>


              </tbody>
            </table>

            <div class="header-actions">
              <a ng-click="showLog(incident)" class="btn btn-primary btn-sm pull-right">View Incident Log</a>

              <a name="" id="" class="btn btn-link btn-sm" href="#" role="button" ng-click="toggleListView()">
                <i class="fa fa-exchange" aria-hidden="true"></i> Kanban View
              </a>
            </div>
          </div>

        </div>
        <div class="col-lg-7 col-contents">


          <div class="scroll-content no-margin-right">
              <div class="padding">
                  <h2>
                      {{activeTaskInfo.question}}
                  </h2>
                  <hr>
                  <div class="content-actions">
                    <!-- Simple dropdown -->
                    <a class="btn btn-primary disabled btn-sm pull-right" id="simple-dropdown" uib-dropdown-toggle>
                      <span ng-if="!activeTaskInfo.checked && !activeTaskInfo.in_progress">To Do</span>
                      <span ng-if="!activeTaskInfo.checked && activeTaskInfo.in_progress">In Progress</span>
                      <span ng-if="activeTaskInfo.checked">Done</span>
                    </a>

                    <a class="btn btn-default btn-sm disabled pull-right" style="margin-right: 5px" ng-hide="!activeTaskInfo.checked && !activeTaskInfo.in_progress">
                      <span ng-if="activeTaskInfo.checked" ng-bind-html="formatDoneMoment(activeTaskInfo)"></span>
                      <span ng-if="activeTaskInfo.in_progress" ng-bind-html="formatInProgressMoment(activeTaskInfo.task_answer.created_at)"></span>
                      <span ng-if="!activeTaskInfo.checked && activeTaskInfo.months && activeTaskInfo.in_progress">&middot;</span>
                      <span ng-if="!activeTaskInfo.checked && activeTaskInfo.months" ng-bind-html="getDueDiff(activeTaskInfo, 'text')" ng-class="getDueDiff(activeTaskInfo, 'class')"></span>
                    </a>


                      <a name="" id="" class="btn btn-default btn-sm disabled">
                          <i class="fa fa-user" aria-hidden="true"></i> <span ng-if="activeTaskInfo.assigned_id == '0'">Assign</span>  <span ng-if="activeTaskInfo.assigned_id != '0'">Assigned - {{activeTaskInfo.assigned_info.crt_email}}</span>
                      </a>
                      <a name="" id="" class="btn btn-default btn-sm disabled">
                          <i class="fa fa-clock-o" aria-hidden="true"></i> Due after <span ng-if="activeTaskInfo.months">{{activeTaskInfo.months}} months</span> <span ng-if="activeTaskInfo.days">{{activeTaskInfo.days}} days</span> <span ng-if="activeTaskInfo.hours">{{activeTaskInfo.hours}} hours</span> <span ng-if="activeTaskInfo.mins">{{activeTaskInfo.mins}} mins</span>
                      </a>

                  </div>
              </div>
              <div class="padding">
                
                <div class="checklist-content">
                    <div class="editor-holder-preview" ng-repeat="content in contents track by $index">

                        <div class="list-margin-holderx" ng-class="{'hidden' : content.module == 'image'}"
                            ng-if="content.module == 'text' || content.module == 'image'">

                        <!-- <ng-quill-editor ng-model="content.content" placeholder="" modules="getQuillModule(content.module)"></ng-quill-editor> -->
                        <!-- <summernote ng-model="content.content"></summernote> -->

                            <div ng-bind-html="htmlSafe(content.module)"></div>

                        </div>


                        <div ng-if="content.module == 'video'" class="contents-holder list-margin-holderx">
                            <!-- <div class="add-video">
                                <div class="form-group">
                                <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Paste YouTube / Vimeo / Wistia URL or embed here">
                                </div>
                            </div> -->

                            <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
                        </div>


                        <div class="contents-holder list-margin-holderx" ng-if="content.module == 'image'">
                        <div class="text-center" ng-hide="content.content">
                            <!-- <button type="button" class="btn btn-default" role="button" ng-click="showImagePop(content, $event)">
                            <i class="fa fa-plus" aria-hidden="true"></i> Upload image...
                            </button> -->
                        </div>
                        <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
                        </div>


                        <div class="contents-holder list-margin-holderx" ng-if="content.module == 'select'">

                        <div class="field-single">
                            <div class="options-view">

                                <div class="form-groupx">
                                    <label for="">{{content.content}}</label>
                                    <!-- <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Type label here"> -->
                                </div>

                                <select name="" id="" class="form-control" ng-options="option as option.text for option in content.options track by option.id" ng-model="answer.selected">
                                </select>
                                <!-- <div class="card card-select">
                                    <div class="card-block">
                                    <div class="form-check form-check-custom" ng-repeat="option in content.options track by $index">
                                        <label class="custom-control custom-checkbox custom-checkbox2">
                                        <span class="custom-number">{{$index + 1}}</span>
                                        <span class="custom-control-description">
                                            <input type="text" class="form-control form-invisible" id="" placeholder="Type option name here" ng-model="option.text" ng-keydown="keyDownSelect($event, option , $index, $parent.$index)">
                                        </span>
                                        </label>
                                    </div>
                                    </div>
                                </div> -->
                            </div>
                            <!-- options-view -->
                        </div>
                        </div>

                        <div class="contents-holder list-margin-holderx" ng-if="content.module == 'list'">

                        <div class="form-checkx form-check-customx" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">
                            
                            <div class="checkboxx">
                                <label style="font-weight: normal">
                                    <input type="checkbox" value="">
                                    {{sublist.text}}
                                </label>
                            </div>

                            
                            <!-- <label class="custom-control custom-checkbox">
                            <input type="checkbox" disabled class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">
                                <input type="text" class="form-control form-invisible" id="inlineFormInput" placeholder="Type here.. hit enter to add sub-task" ng-model="sublist.text" ng-keydown="keyDownSublist($event, sublist , $index, $parent.$index)">
                            </span>
                            </label> -->
                        </div>



                        <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
                            <div class="form-groupx" ng-hide="field.type == 'hidden' || field.type == 'select'">
                                <!-- <input type="text" class="form-control" ng-model="field.text" aria-describedby="helpId" placeholder="Type label here"> -->
                                <label for="">{{field.text}}</label>
                            </div>


                            <input type="text" ng-if="field.type == 'single'"  class="form-control" name="" id="" aria-describedby="helpId" placeholder="">
                            
                            <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2"  placeholder=""></textarea>
                        
                            <div ng-if="field.type == 'email'"  class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" >
                            </div>

                            <div ng-if="field.type == 'url'"  class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <i class="fa fa-globe" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" >
                            </div>

                            <div ng-if="field.type == 'hidden'"  class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                </span>
                                <!-- <input type="text" class="form-control" ng-model="field.text" placeholder="Type label here" aria-describedby="basic-addon1"> -->
                                <label for="">{{field.text}}</label>
                            </div>


                            <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" onclick="$('#uploadFileInput').click()">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload File
                            </button>
                            <div class="hidden">
                                <input type="file" id="uploadFileInput">
                            </div>

                            <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" popover-append-to-body="true" uib-popover-template="'datePickerPopover.html'" popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                            <i class="fa fa-calendar" aria-hidden="true"></i> Add Date
                            </button>

                        </div>

                        </div>

                        <div class="emailer-holderx" ng-if="content.module == 'email'">
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">To</label>
                                <div class="col-sm-8">
                                <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Cc</label>
                                <div class="col-sm-8">
                                <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Bcc</label>
                                <div class="col-sm-8">
                                <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Subject</label>
                                <div class="col-sm-9">
                                <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Body</label>
                                <div class="col-sm-10">
                                <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                                <div class="col-sm-8">
                                <button class="btn btn-default">
                                    <i class="fa fa-paper-plane" aria-hidden="true"></i> Send
                                </button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- editor-holder -->
                </div>
              
              </div>


          </div>



        </div>
    </div><!--.row -->


    <script type="text/ng-template" id="taskContentsModal.html">
      <div class="modal-header">
          <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

          <h4 class="modal-title">
              Task Contents
          </h4>
      </div>
      <div class="modal-body modal-body-kbcontent">

        <div class="checklist-contents" ng-if="!editMode">
            <div class="editor-holder-preview" ng-repeat="content in contents track by $index">

                <div class="list-margin-holderx" ng-class="{'hidden' : content.module == 'image'}"
                    ng-if="content.module == 'text' || content.module == 'image'">
                    <div ng-bind-html="htmlSafe(content.module)"></div>

                </div>


                <div ng-if="content.module == 'video'" class="contents-holder list-margin-holderx">
                    <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
                </div>


                <div class="contents-holder list-margin-holderx" ng-if="content.module == 'image'">
                <div class="text-center" ng-hide="content.content">

                </div>
                <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
                </div>


                <div class="contents-holder list-margin-holderx" ng-if="content.module == 'select'">

                <div class="field-single">
                    <div class="options-view">
                        <div class="form-groupx">
                            <label for="">{{content.content}}</label>
                        </div>

                        <select name="" id="" class="form-control" ng-options="option as option.text for option in content.options track by option.id" ng-model="answer.selected">
                        </select>
                    </div>
                </div>
                </div>

                <div class="contents-holder list-margin-holderx" ng-if="content.module == 'list'">

                <div class="form-checkx form-check-customx" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">
                    
                    <div class="checkboxx">
                        <label style="font-weight: normal">
                            <input type="checkbox" value="">
                            {{sublist.text}}
                        </label>
                    </div>
                </div>



                <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
                    <div class="form-groupx" ng-hide="field.type == 'hidden' || field.type == 'select'">
                        <label for="">{{field.text}}</label>
                    </div>


                    <input type="text" ng-if="field.type == 'single'"  class="form-control" name="" id="" aria-describedby="helpId" placeholder="">
                    
                    <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2"  placeholder=""></textarea>
                
                    <div ng-if="field.type == 'email'"  class="input-group">
                    <span class="input-group-addon" id="basic-addon1">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" >
                    </div>

                    <div ng-if="field.type == 'url'"  class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" >
                    </div>

                    <div ng-if="field.type == 'hidden'"  class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-eye-slash" aria-hidden="true"></i>
                        </span>
                        <label for="">{{field.text}}</label>
                    </div>


                    <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" onclick="$('#uploadFileInput').click()">
                    <i class="fa fa-upload" aria-hidden="true"></i> Upload File
                    </button>
                    <div class="hidden">
                        <input type="file" id="uploadFileInput">
                    </div>

                    <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" popover-append-to-body="true" uib-popover-template="'datePicker.html'" popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                    <i class="fa fa-calendar" aria-hidden="true"></i> Add Date
                    </button>

                </div>

                </div>

                <div class="emailer-holderx" ng-if="content.module == 'email'">
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">To</label>
                        <div class="col-sm-8">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Cc</label>
                        <div class="col-sm-8">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Bcc</label>
                        <div class="col-sm-8">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Subject</label>
                        <div class="col-sm-9">
                        <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Body</label>
                        <div class="col-sm-10">
                        <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                        <div class="col-sm-8">
                        <button class="btn btn-default">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i> Send
                        </button>
                        </div>
                    </div>
                </div>

            </div>
            <!-- editor-holder -->
        </div>

        <div class="edit-mode" ng-if="editMode">
        
          <div class="padding">
            <h2>
              <textarea name="" id="" class="form-control form-invisible form-invisible-lg" ng-model="activeTaskInfo.question" rows="auto"></textarea>
            </h2>
            <hr>
            <div class="content-actions">
              <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'assignPopover.html'" popover-placement="bottom" popover-is-open="assignPopSettings.isOpen" >
                <i class="fa fa-user" aria-hidden="true"></i> <span ng-if="activeTaskInfo.assigned_id == '0'">Assign</span>  <span ng-if="activeTaskInfo.assigned_id != '0'">Assigned - {{activeTaskInfo.assigned_info.full_name}}</span>
              </a>
              <a name="" id="" class="btn btn-default btn-sm" href="#" role="button" popover-append-to-body="true" uib-popover-template="'datePickerPopover.html'" popover-placement="bottom" popover-is-open="datePopSettings.isOpen">
                <i class="fa fa-clock-o" aria-hidden="true"></i> Due after <span ng-if="activeTaskInfo.months">{{activeTaskInfo.months}} months</span> <span ng-if="activeTaskInfo.days">{{activeTaskInfo.days}} days</span> <span ng-if="activeTaskInfo.hours">{{activeTaskInfo.hours}} hours</span> <span ng-if="activeTaskInfo.mins">{{activeTaskInfo.mins}} mins</span>
              </a>

            </div>
          </div>
          <!--  -->
          <div class="checklist-content" ui-sortable="sortableContentOptions" ng-model="contents">
            <div class="editor-holder" ng-repeat="content in contents track by $index"  as-sortable-item>
              <button class="btn btn-thirdy btn-times btn-sm" role="button" ng-click="removeContent($index, content)">
                <i aria-hidden="true" class="fa fa-times fa-fw"></i>
              </button>

              <button ng-if="$index != 0" class="btn btn-thirdy btn-up btn-sm" role="button" ng-click="moveContent($index, 'up')">
                <i aria-hidden="true" class="fa fa-chevron-up"></i>
              </button>


              <div class="list-margin-holder" ng-class="{'hidden' : content.module == 'image'}"
                  ng-if="content.module == 'text' || content.module == 'image'">

                <ng-quill-editor ng-model="content.content" placeholder="" ng-click="focusQuill($index)" modules="getQuillModule(content.module)"></ng-quill-editor>
                <!-- <summernote ng-model="content.content"></summernote> -->

              </div>


              <div ng-if="content.module == 'video'" class="contents-holder list-margin-holder">
                <div class="add-video">
                  <div class="form-group">
                    <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Paste YouTube / Vimeo / Wistia URL or embed here">
                  </div>
                </div>

                <iframe width="560" height="315" ng-src="{{videoUrl(content.content)}}" ng-if="content.content" frameborder="0" allowfullscreen></iframe>
              </div>


              <div class="contents-holder list-margin-holder" ng-if="content.module == 'image'">
                <div class="text-center" ng-hide="content.content">
                  <button type="button" class="btn btn-default" role="button" onclick="var i2 = $('.ql-image').length - 1; $('.ql-image:eq('+i2+')').click();">
                    <i class="fa fa-plus" aria-hidden="true"></i> Upload image...
                  </button>
                </div>
                <div class="text-center img-holder" ng-hide="!content.content" ng-bind-html="htmlSafe(content.content)"></div>
              </div>


              <div class="contents-holder list-margin-holder" ng-if="content.module == 'select'">

                <div class="field-single">
                  <div class="options-view">

                    <div class="form-group">
                      <input type="text" class="form-control" ng-model="content.content" aria-describedby="helpId" placeholder="Type label here">
                    </div>

                    <select name="" id="" class="form-control" disabled>
                      <option>An option will be selected from this list:</option>
                    </select>
                    <div class="card card-select">
                      <div class="card-block">
                        <div class="form-check form-check-custom" ng-repeat="option in content.options track by $index">
                          <label class="custom-control custom-checkbox custom-checkbox2">
                            <span class="custom-number">{{$index + 1}}</span>
                            <span class="custom-control-description">
                                <input type="text" class="form-control form-invisible" id="" placeholder="Type option name here" ng-model="option.text" ng-keydown="keyDownSelect($event, option , $index, $parent.$index)">
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- options-view -->
                </div>
              </div>

              <div class="contents-holder list-margin-holder" ng-if="content.module == 'list'">

                <div class="form-check form-check-custom" ng-repeat="sublist in content.field track by $index" ng-hide="sublist.type != 'sublist'">
                  
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" disabled class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">
                        <input type="text" class="form-control form-invisible" id="inlineFormInput" placeholder="Type here.. hit enter to add sub-task" ng-model="sublist.text" ng-keydown="keyDownSublist($event, sublist , $index, $parent.$index)">
                    </span>
                  </label>
                </div>



                <div class="field-single" ng-repeat="field in content.field track by $index" ng-hide="field.type == 'sublist'">
                  <div class="form-group" ng-hide="field.type == 'hidden' || field.type == 'select'">
                    <input type="text" class="form-control" ng-model="field.text" aria-describedby="helpId" placeholder="Type label here">
                  </div>


                  <input type="text" ng-if="field.type == 'single'" disabled class="form-control" name="" id="" aria-describedby="helpId" placeholder="Something will be typed here...">
                  
                  <textarea ng-if="field.type == 'paragraph'" class="form-control" rows="2" disabled placeholder="Something will be typed here..."></textarea>
                
                  <div ng-if="field.type == 'email'"  class="input-group">
                    <span class="input-group-addon" id="basic-addon1">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" placeholder="e.g., ned@stark.com" aria-describedby="basic-addon1" disabled>
                  </div>

                  <div ng-if="field.type == 'url'"  class="input-group">
                    <span class="input-group-addon" id="basic-addon1">
                      <i class="fa fa-globe" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" placeholder="e.g., https://process.st" aria-describedby="basic-addon1" disabled>
                  </div>

                  <div ng-if="field.type == 'hidden'"  class="input-group">
                    <span class="input-group-addon" id="basic-addon1">
                      <i class="fa fa-eye-slash" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" ng-model="field.text" placeholder="Type label here" aria-describedby="basic-addon1">
                  </div>


                  <button ng-if="field.type == 'file'" type="button" name="" id="" class="btn btn-default" disabled>
                    <i class="fa fa-upload" aria-hidden="true"></i> File will be uploaded here
                  </button>

                  <button ng-if="field.type == 'date'" type="button" name="" id="" class="btn btn-default" disabled>
                    <i class="fa fa-calendar" aria-hidden="true"></i> Date will be set here
                  </button>

                </div>

              </div>

              <div class="emailer-holder" ng-if="content.module == 'email'">
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">To</label>
                  <div class="col-sm-8">
                    <input type="text" name="" class="form-control" id="" ng-model="content.email.email" placeholder="Email, e.g. bruce@wayne-enterprises.com">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Cc</label>
                  <div class="col-sm-8">
                    <input type="text" name="" class="form-control" id="" ng-model="content.email.cc" placeholder="Email, e.g. alfred@wayne-enterprises.com">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Bcc</label>
                  <div class="col-sm-8">
                    <input type="text" name="" class="form-control" id="" ng-model="content.email.bcc" placeholder="Email, e.g. batman@batman.com">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Subject</label>
                  <div class="col-sm-9">
                    <input type="text" name="" class="form-control" id="" ng-model="content.email.subject" placeholder="Subject">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Body</label>
                  <div class="col-sm-10">
                    <textarea name="" class="form-control" rows="2" ng-model="content.email.body" placeholder="Body"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                  <div class="col-sm-8">
                    <button class="btn btn-default">
                      <i class="fa fa-paper-plane" aria-hidden="true"></i> Test Send
                    </button>
                  </div>
                </div>
              </div>


              <button class="btn btn-thirdy btn-copy btn-sm" role="button" ng-click="copyContent(content, $index)" >
                <i aria-hidden="true" class="fa fa-copy fa-fw"></i>
              </button>

              <button ng-if="contents.length > 1; $index != (contents.length - 1)" class="btn btn-thirdy btn-down btn-sm" role="button" ng-click="moveContent($index, 'down')">
                <i aria-hidden="true" class="fa fa-chevron-down"></i>
              </button>
            </div>
            <!-- editor-holder -->
          </div>

                          
        </div>

        <div class="text-center text-muted" style="padding: 50px 0;" ng-if="!contents.length">
          No content added.
        </div>
        
        <div class="sticky-rightbar sticky-rightbar-modal">
          <div class="scroll-content">
            <div class="form-group">
              <button class="btn btn-default btn-block btn-lg" data-toggle="tooltip" data-title="Text" data-placement="left" type="button" ng-click="addText()">
                <i class="fa fa-text-width"></i>
              </button>
            </div>
            <div class="form-group">
              <button class="btn btn-default btn-block btn-lg" container="body" popover-append-to-body="true" uib-popover-template="'mediaPopover.html'" popover-trigger="'focus'" popover-placement="left" type="button" >
                <i class="fa fa-file" data-toggle="tooltip" data-title="Media" data-placement="left"></i>
              </button>
            </div>


            <div class="form-group">
              <button class="btn btn-default btn-block btn-lg" container="body" popover-append-to-body="true" uib-popover-template="'formPopover.html'" popover-trigger="'focus'" popover-placement="left" type="button">
                <i class="fa fa-clipboard" data-toggle="tooltip" data-title="Form field" data-placement="left"></i>
              </button>
            </div>

            
            <div class="form-group">
              <button class="btn btn-default btn-block btn-lg" type="button" ng-click="addSubchecklist()">
                <i class="fa fa-list" data-toggle="tooltip" data-title="Sub checklist" data-placement="left" ></i>
              </button>
            </div>
            <div class="form-group">
              <button class="btn btn-default btn-block btn-lg" data-toggle="tooltip" data-title="Email" data-placement="left" type="button" ng-click="addEmail()">
                <i class="fa fa-envelope"></i>
              </button>
            </div>


          </div>
        </div>
              

      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
          <button type="button" class="btn btn-primary" ng-disabled="loading" ng-click="save()">Save</button>
      </div>
    </script>
    
    

    <script type="text/ng-template" id="datePickerPopover.html">
      <div style="max-width:315px;">
        <div class="row gutter-md">
          <div class="col-xs-12">
            <p class="text-center">Due after</p>
            <hr>
            <p>
              <small>This task will be due:</small>
            </p>
          </div>
          
          <div class="col-xs-6">
            <div class="form-group">

              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.months" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">months</span>
              </div>

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.days" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">days</span>
              </div>
            </div>
          </div>
          
          <div class="col-xs-6">
            <div class="form-group">

              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.hours" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">hours</span>
              </div>

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <div class="input-group input-group-trans">
                <input type="text" class="form-control" ng-model="activeTaskInfo.mins" placeholder="" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">mins</span>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <p>
              <small>After the checklist start date.</small>
            </p>
          </div>
          <!-- <div class="col-xs-6">
            <div class="form-group">
              <label for="">Date</label>
              <input type="text" ng-model="activeTaskInfo.due_date | date: 'shortDate'" class="form-control">

            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group">
              <label for="">Time</label>
               <input type="text" ng-model="activeTaskInfo.due_time" class="form-control"> 
            </div>
          </div> -->
          <!-- <div class="col-xs-12">
            <div class="form-group">
              <div uib-datepicker ng-model="activeTaskInfo.due_date" datepicker-options="optionsPicker"></div> 
            </div>
          </div> -->

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-success btn-block" ng-click="saveDate()" role="button">Save</a>
          </div>

          <div class="col-xs-6">
            <a name="" id="" class="btn btn-danger btn-block" ng-click="removeDate()" role="button">Remove</a>
          </div>

        </div>
      </div>

    </script>

    <script type="text/ng-template" id="customTemplate.html">
      <a>
          <!-- <img ng-src="http://upload.wikimedia.org/wikipedia/commons/thumb/{{match.model.flag}}" width="16"> -->
          <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
      </a>
    </script>

    <script type="text/ng-template" id="assignPopover.html">
      <div style="width:240px;">
        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <input type="text" ng-model="activeTaskInfo.assigned" placeholder="" uib-typeahead="crt as crt.full_name for crt in crts | filter:{full_name:$viewValue}" typeahead-template-url="customTemplate.html" class="form-control" typeahead-show-hint="true" typeahead-min-length="0">
              <span class="input-group-btn">
                <button class="btn btn-link" type="button" ng-click="saveAssigned()">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </button>
              </span>
            </div><!-- /input-group -->

            <small>Search for a person in your organization by name.</small>


          </div>

        </div>
      </div>
    </script>

    <script type="text/ng-template" id="confirmPopover.html">
      <div style="width:140px;">
        <div class="row">
          <div class="col-sm-6">
            <a name="" id="" class="btn btn-block btn-danger" href="#" role="button">
              Delete
            </a>
          </div>
          <div class="col-sm-6">
            <a name="" id="" class="btn btn-block btn-default" href="#" role="button">
              Cancel
            </a>
          </div>

        </div>
      </div>
    </script>


    <script type="text/ng-template" id="mediaPopover.html">
      <div style="width: 200px;">
          <button class="btn btn-default btn-block" type="button" ng-click="addImage()"> <i class="fa fa-picture-o"></i> Image </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addVideo()"> <i class="fa fa-video-camera"></i> Video </button>
      </div>

    </script>


    <script type="text/ng-template" id="formPopover.html">

      <div style="width: 200px;">
          <button class="btn btn-default btn-block" type="button" ng-click="addSingleLine()"> <i class="fa fa-edit fa-fw"></i> Single Line Text </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addParagraph()"> <i class="fa fa-paragraph fa-fw"></i> Paragraph Text </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addEmailForm()"> <i class="fa fa-envelope-o fa-fw"></i> Email Address </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addSiteForm()"> <i class="fa fa-globe fa-fw"></i> Web Address </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addFileForm()"> <i class="fa fa-file-o fa-fw"></i> File Upload </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addDateForm()"> <i class="fa fa-calendar fa-fw"></i> Date </button>
          <button class="btn btn-default btn-block" type="button" ng-click="addDropdownForm()"> <i class="fa fa-arrow-circle-down fa-fw"></i> Dropdown </button>
      </div>
    </script>



    <script type="text/ng-template" id="showLogModal.html">
        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Incident Log - {{data.incident_no}} <br>
                <small>{{data.description}}</small>
            </h4>
        </div>
        <div class="modal-body">
            <p>
                <strong>Incident Started: </strong> {{data.init_date_format}}
            </p>
            <p>
                <strong>Incident Ended: </strong> {{data.closed_date_format}}
            </p>
            <p class="text-success">
                <strong>Total Time: </strong> {{data.total_time}}
            </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="cancel()">OK</button>
        </div>

    </script>


  </div>
</div>
  
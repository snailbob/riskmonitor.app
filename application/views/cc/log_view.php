<style>
.dataTables_filter, .dataTables_info ,.dataTables_length, .dataTables_paginate {display: none;}
</style>

<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->


<?php
	//select org
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

?>


    <!-- Modal for viewing task respond-->
    <div class="modal fade" id="recallViewRespond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<div id="respondtask_holder">
            </div>
			

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Post-Incident Review</h1>

      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i> <a href="<?php base_url() ?>">Dashboard</a></li>
        <li class="active">Post-Incident Review</li>

      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 

<!-- end PAGE TITLE AREA -->



<div class="row">

    <ul class="nav nav-tabs"style="margin-bottom: 20px; margin-left: 15px; margin-right: 15px;">
    
		<?php if (strpos($the_active_module[0]['active_module'], '1') !== false){ $active_scene_tab = 'in active'; ?>
      <li class="active preloadThis1x"><a href="#closedscene" data-toggle="tab">Crises</a></li>
      
		<?php } else { $active_scene_tab = ''; }?>       

		<?php if (strpos($the_active_module[0]['active_module'], '2') !== false){
			if (strpos($the_active_module[0]['active_module'], '1') === false){
				
				$active_case_tab = 'in active';
			?>
      <li class="active preloadThis1x"><a href="#closedcase" data-toggle="tab">Cases</a></li>
		<?php }else{ $active_case_tab = ''; ?>
      <li class="preloadThis1x"><a href="#closedcase" data-toggle="tab">Cases</a></li>
		
		<?php }
		} else {$active_case_tab = '';} ?>  
             
        <?php if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		
		if($active_mod == '5'){
			$tabname = 'Recall';
		}
		else{
			$tabname = 'Continuity';
		}
		
		
			if (strpos($the_active_module[0]['active_module'], '1') === false && strpos($the_active_module[0]['active_module'], '2') === false){
				$active_recall_tab = 'in active';
			?>
      <li class="active preloadThis1x"><a href="javascript:;"><?php echo $tabname; //<a href="#closedrecall" data-toggle="tab">?></a></li>
		<?php }else{ $active_recall_tab = '';?>
      <li class="preloadThis1x"><a href="javascript:;"><?php echo $tabname; ?></a></li>
		<?php }
		} else { $active_recall_tab = '';} ?>  
             
        
        
    </ul>
    
    <div id="myTabContent" class="tab-content">










     	<div class=" tab-pane fade <?php echo $active_scene_tab; ?>" id="closedscene">

<?php

	/* each initiated scenarion will be display seperatly*/	

	if(count($scenarios)>0)

	{

		foreach($scenarios as $scenario)

		{

?>

            <div class="col-lg-12 <?php if ($scenario['completed_date'] == '0000-00-00 00:00:00'){echo 'hidden'; } ?>">
            
              <div class="panel panel-default">

                <div class="panel-heading">

                  <div class="panel-title">

                    <h4><span class="label label-success">Completed</span> <?php if ($scenario['initiation_type']==0){echo '<span class="label label-default">Mock</span> - '.$scenario['scenario']; } else{ echo '<span class="label label-info">Live</span> - '.$scenario['scenario']; }?></h4>

                  </div>

                  <div class="panel-widgets">
					<a class="" data-toggle="collapse" data-parent="#accordion_<?php echo $scenario['scenario_id']; ?>" href="#validationExamples_<?php echo $scenario['log_scene_id']; ?>"><i class="fa fa-chevron-down"></i></a>

                  </div>

                  <div class="clearfix"></div>

                </div>

                <div id="validationExamples_<?php echo $scenario['log_scene_id']; ?>" class="panel-collapse collapse in">

                <div class="panel-body">

                    	<p><b>Date Initiated:</b> <span class="text-muted">
						<?php
						
							$initiation_date = date_create($scenario['initiation_date']);
							echo date_format($initiation_date, 'l jS F Y - g:i:sa');
						 
						 ?>
                         </span></p>
                         
                    	<p><b>Date Closed:</b> <span class="text-muted">
						<?php
							$completed_date = date_create($scenario['completed_date']);
							echo date_format($completed_date, 'l jS F Y - g:i:sa');
						 ?>
                         </span></p>
                    	<p><b>Total Time:</b> <span class="text-muted">
						
						<?php
						 $date_pieces = explode(" ",$scenario['lapse_time']);
						 /*if ($date_pieces[0] !=0 ){echo $date_pieces[0].' years '; }
						 if ($date_pieces[1] !=0 ){echo $date_pieces[1].' months '; }
						 if ($date_pieces[2] !=0 ){echo $date_pieces[2].' days '; }
						 if ($date_pieces[3] !=0 ){echo $date_pieces[3].' hours '; }
						 if ($date_pieces[4] !=0 ){echo $date_pieces[4].' minutes '; }
						 if ($date_pieces[5] !=0 ){echo $date_pieces[5].' seconds'; }*/
						 
						 if ($date_pieces[0] ==0 ){}
						 else if ($date_pieces[0] ==1 ){
							 echo $date_pieces[0].' year '; }
						 else{
							 echo $date_pieces[0].' years '; }
							 
						 if ($date_pieces[1] ==0 ){}
						 else if ($date_pieces[1] ==1 ){
							 echo $date_pieces[1].' month '; }
						 else{
							 echo $date_pieces[1].' months '; }
							 
						 if ($date_pieces[2] ==0 ){}
						 else if ($date_pieces[2] ==1 ){
							 echo $date_pieces[2].' day '; }
						 else{
							 echo $date_pieces[2].' days '; }
	
						 if ($date_pieces[3] ==0 ){}
						 else if ($date_pieces[3] ==1 ){
							 echo $date_pieces[3].' hour '; }
						 else{
							 echo $date_pieces[3].' hours '; }
	
						 if ($date_pieces[4] ==0 ){}
						 else if ($date_pieces[4] ==1 ){
							 echo $date_pieces[4].' minute '; }
						 else{
							 echo $date_pieces[4].' minutes '; }
	
						 if ($date_pieces[5] ==0 ){}
						 else if ($date_pieces[5] ==1 ){
							 echo $date_pieces[5].' second '; }
						 else{
							 echo $date_pieces[5].' seconds '; }
						 
						 
						  
						 ?>
                         </span></p>
                         
                         
                   	<?php 

					/*display task for each scenario */

					$task_list=$this->common_model->get_completed_scenario_task($scenario['scenario_id'],$scenario['initiation_date']);
					
//					$owner=$this->master_model->getRecords('cf_crisis_response_team',array('stn_msg_id'=>$standbymsgid),'*');

					
					if(count($task_list)>0){ ?>
                    	<p><b>Completed Tasks:</b></p>
						<?php } ?>
                    <div style="clear: both"></div>
                   	<?php 

					if(count($task_list)>0)

					{ ?>


                      <div class="table-responsive">

                        <table  class="table table-striped table-bordered table-hover table-green myexample-tablexx"><!--id="example-table"-->

                          <thead>

                            <tr>

                              <th width="50%">Task Name</th>

                              <th width="20%">Owner</th>

                              <th width="20%">Date Completed</th>

                             <?php /*?> <th width="10%">Action</th><?php */?>

                            </tr>

                          </thead>

                          <tbody>
					
                            <?php 
							
						

                           foreach($task_list as $task)

                            {


								 	/*	if($task['task_status'] == 1 ){

                                    $status = ' <span class="text-red">In Progress</span>';

                                    } if($task['task_status'] == 0 ) {	

                                    $status = '<span class="text-red">Pre-Incident</span>';

                                    }

									 if($task['task_status'] == 2 ){

                                    $status = '<span class="text-green">Completed</span>';

                                    } */

                            ?>

                                <tr>

                                    <td><?php echo $task['task']; ?></td>

                                    <td><?php 
									
									$ownerr=$this->master_model->getRecords('cf_crisis_response_team',array('crt_id'=>$task['crt_id']));
									echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']); ?></td>

                                    <td><?php echo date('d M Y - g:i:sa',strtotime($task['completed_date']));?></td>


                                </tr>	

                            <?php 

							}
							
                            ?>
                            
                            

                            

                          </tbody>

                        </table>

                      </div> 

                      <!-- /.table-responsive -->

                      <?php };?>

                   

                    

                    

                  

                </div>

                </div>

                <!-- /.panel-body --> 

              </div>

              <!-- /.panel --> 

              

            </div>

<?php

		}

	}
	
	else{
		?>
        
		<div class="col-md-12">
            <p class="text-center" style="color: #F3F3F3; margin-top: 60px;"><i class="fa fa-edit" style="font-size: 90px"></i></p>
        	<p class="text-center" style="color: #ccc; margin-top: 20px;">No Completed Scenario.</p>
		</div>
<?php
	}

	/* Display scenario and there messages */

?>
        </div><!--./closedscene-->
        
        <div class="tab-pane fade <?php echo $active_case_tab; ?>" id="closedcase">
        

		<?php
			if(count($case_list) > 0){
				
				foreach ($case_list as $ccases){
		?>

                <div class="col-lg-12">
                
                  <div class="panel panel-default">
    
                    <div class="panel-heading">
    
                      <div class="panel-title">
    
                        <h4><span class="label label-success"><?php if($ccases['status'] == '1') { echo 'Closed'; } else{ echo 'Remediated'; } ?></span> <?php echo $ccases['gen_id'].' - '.$ccases['case_title']; ?></h4>
    
                      </div>
    
                      <div class="panel-widgets">
                        <a class="" data-toggle="collapse" data-parent="#accordioncase_<?php echo $ccases['id']; ?>" href="#cases_<?php echo $ccases['id']; ?>"><i class="fa fa-chevron-down"></i></a>
    
                      </div>
    
                      <div class="clearfix"></div>
    
                    </div>
    
                    <div id="cases_<?php echo $ccases['id']; ?>" class="panel-collapse collapse in">
    
                        <div class="panel-body">
        

                            <p><b>Date Initiated:</b> <span class="text-muted">
							<?php
								$date_initiated = date_create($ccases['date_initiated']);
								echo date_format($date_initiated, 'l jS F Y - g:i:sa');
							?>
                            </span></p>
                            <p><b>Date Closed:</b> <span class="text-muted">
							<?php
								$date_closed = date_create($ccases['date_closed']);
								echo date_format($date_closed, 'l jS F Y - g:i:sa');
							?>
                            </span></p>

                            <p><b>Total Time:</b> <span class="text-muted">
                            
                            <?php
                             $date_pieces = explode(" ",$ccases['time_lapsed']);
                             /*if ($date_pieces[0] !=0 ){echo $date_pieces[0].' years '; }
                             if ($date_pieces[1] !=0 ){echo $date_pieces[1].' months '; }
                             if ($date_pieces[2] !=0 ){echo $date_pieces[2].' days '; }
                             if ($date_pieces[3] !=0 ){echo $date_pieces[3].' hours '; }
                             if ($date_pieces[4] !=0 ){echo $date_pieces[4].' minutes '; }
                             if ($date_pieces[5] !=0 ){echo $date_pieces[5].' seconds'; }*/
                             
                             if ($date_pieces[0] ==0 ){}
                             else if ($date_pieces[0] ==1 ){
                                 echo $date_pieces[0].' year '; }
                             else{
                                 echo $date_pieces[0].' years '; }
                                 
                             if ($date_pieces[1] ==0 ){}
                             else if ($date_pieces[1] ==1 ){
                                 echo $date_pieces[1].' month '; }
                             else{
                                 echo $date_pieces[1].' months '; }
                                 
                             if ($date_pieces[2] ==0 ){}
                             else if ($date_pieces[2] ==1 ){
                                 echo $date_pieces[2].' day '; }
                             else{
                                 echo $date_pieces[2].' days '; }
        
                             if ($date_pieces[3] ==0 ){}
                             else if ($date_pieces[3] ==1 ){
                                 echo $date_pieces[3].' hour '; }
                             else{
                                 echo $date_pieces[3].' hours '; }
        
                             if ($date_pieces[4] ==0 ){}
                             else if ($date_pieces[4] ==1 ){
                                 echo $date_pieces[4].' minute '; }
                             else{
                                 echo $date_pieces[4].' minutes '; }
        
                             if ($date_pieces[5] ==0 ){}
                             else if ($date_pieces[5] ==1 ){
                                 echo $date_pieces[5].' second '; }
                             else{
                                 echo $date_pieces[5].' seconds '; }
                             
                             
                              
                             ?>
                             </span></p>
                             
                             
                            <p><a class="btn btn-default btn-sm" href="#"  data-toggle="modal" data-target="#myModal<?php echo  $ccases['id'];?>">View Report</a></p>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal<?php echo  $ccases['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-book"></i>  <?php echo  $ccases['gen_id']; ?> Report</h4>
                                  </div>
                                  <div class="modal-body">


                                        <!-- panel for reported cases-->
                                                <?php 
													$reported_cases=$this->master_model->getRecords('case_report_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'1','cc_decision !='=>'3','case_id'=>$ccases['id']));
                                                if(count($reported_cases)>0){  
                                                    foreach($reported_cases as $rr => $rcases) {
                
                                                ?>
                                                
                                        <div class="panel panel-default">
                
                
                
                
                
                                            <div class="panel-heading" style="display: none;">
                
                                                <div class="panel-title">
                
                                                    <h4><i class="fa fa-book"></i> 
                                                    
                                                    <?php   
                                                        $caseid = $rcases['case_id'];
                                                        $thecase = $this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'id'=>$caseid));
                                                    
                                                        echo 'Case Report for: '.$thecase[0]['gen_id'].' - '.$thecase[0]['case_title'];
                                                    ?>
                                                     </h4>
                
                
                                                </div>
                                                <div class="panel-widgets">
                
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#panelexamples<?php echo $caseid; ?>"><i class="fa fa-chevron-down"></i></a>
                
                                                </div>
                
                
                                            <div class="clearfix"></div>
                
                                            </div>
                
                                            
                                         <div id="panelexamples<?php echo $caseid; ?>" class="panel-collapse collapse in">
                                            <div class="panel-body">

                                                <blockquote class="blockquote">
                                                  <p>CC Note</p>
                                                  <footer><?php echo $rcases['cc_note']; ?></footer>
                                                </blockquote>
                
                                                <blockquote class="blockquote">
                                                  <p>Hazard Analysis</p>
                                                  <footer><?php echo $rcases['hazard']; ?></footer>
                                                </blockquote>
                
                                                <blockquote class="blockquote">
                                                  <p>Root-Cause Analysis</p>
                                                  <footer><?php echo $rcases['root_cause']; ?></footer>
                                                </blockquote>
                
                                                <blockquote class="blockquote">
                                                  <p>Impact Analysis</p>
                                                  <footer>
                                                      <dt><em>Impact to Business operations</em></dt>
                                                      <dd><?php echo $rcases['impact_to_bsn']; ?></dd>
                                                      <dt><em>Impact to Stakeholders</em></dt>
                                                      <dd><?php echo $rcases['impact_to_stk']; ?></dd>
                                                  
                                                  </footer>
                                                </blockquote>
                
                                                <blockquote class="blockquote">
                                                  <p>Control</p>
                                                  <footer><?php echo $rcases['control']; ?></footer>
                                                </blockquote>
                
                                                <blockquote class="blockquote">
                                                  <p>Recommendation</p>
                                                  <footer><?php echo $rcases['recommendation']; ?></footer>
                                                </blockquote>
                
                                                <?php 
                                                
                                                    $casedocss = $this->master_model->getRecords('case_document_report',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'case_id'=>$caseid));
                                                
                                                
                                                    if(count($casedocss) > 0){ ?>
                                                <p style="font-size: 16px; margin-left: 10px;">Evidence and Documents</p>
                                                <div class="table-responsive">
                    
                                                <table class="table table-hover table-bordered table-green" id="example-tablexx">
                    
                                                    <thead>
                    
                                                        <tr>
                    
                                                            <th width="50%">Document Name</th>
                    
                                                            <th width="50%">File Description</th>
                    
                                                            <th width="50px"> </th>
                    
                                                        </tr>
                    
                                                    </thead>
                    
                                                    <tbody>
                
                                                    
                                                                    
                                                    <?php
                                                    
                                                        foreach($casedocss as $casedocss){
                                                    ?>
                                                
                                                
                                                    <tr>
                
                                                        <td>
                                                        
                                                        <?php
                                                        $fileext = explode ('.',$casedocss['document']);
                                                        if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
                                                        
                                                        echo '<i class="fa fa-file-pdf-o text-red"></i> ';
                                                        
                                                        }
                                                        
                                                        else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
                                                        
                                                        echo '<i class="fa fa-file-word-o text-blue"></i> ';
                                                        
                                                        }
                                                        
                                                        else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
                                                        
                                                        echo '<i class="fa fa-file-text-o text-muted"></i> ';
                                                        
                                                        }
                
                                                        echo substr($casedocss['document'],13,50); ?>
                                                        </td>
                
                                                        <td>
                                                        <?php echo $casedocss['file_desc']; ?>
                                                        </td>
                                                        <td>
                                                        <a href="<?php echo base_url().'cc/document/download/'. $casedocss['document']; ?>" class="btn btn-sm btn-default">Download</a>
                                                        </td>
                                                    </tr>
                                                
                
                                                    <?php 
                    
                                                        }
                                                    ?>
                    
                                                                    </tbody>
                    
                                                                </table>
                    
                                                            </div>
                    
                                                            <!-- /.table-responsive -->
                    
                                                    <?php 
                                                    }
                                                    ?>
                
                
                
                
                                            </div>
                                          </div>
                                        </div>
                                                    <?php }
                                                    
                                                    }
                                                  
                                                ?>
                
                                        <!-- /.panel -->
                                        
                

                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                                  </div>
                                </div>
                              </div>
                            </div>




                        </div>
    
                    </div>
    
                    <!-- /.panel-body --> 
        
                  </div>
    
                  <!-- /.panel --> 
    
                  
    
                </div>

		<?php
				}	
			}
			
			  else{ ?>
		<p class="text-center" style="color: #F3F3F3; margin-top: 60px;"><i class="fa fa-life-ring" style="font-size: 90px"></i></p>
		<p class="text-center" style="margin-top: 20px; color: #ccc">No Completed Cases.</p>
		<?php }?>

        </div><!--./closedcase-->



     	
     	
		<?php if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>
        <div class=" tab-pane fade <?php echo $active_recall_tab; ?>" id="closedrecall">






<?php

	if (count($recall) > 0){

		//count task present
		if (strpos($the_active_module[0]['active_module'], '5') !== false){
			$recall_task_count = $this->master_model->getRecordCount('cf_recall_steps',array('recall_id'=>$recall[0]['id']));
		}
		else{
			$recall_task_count = $this->master_model->getRecordCount('cf_continuity_steps',array('recall_id'=>$recall[0]['id']));
		}
?>
        
                 
<!-- viewIssueModal-->
<div class="modal fade" id="viewIssueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">
            Task Issue
        </h4>
        
      </div>
      
      <div class="modal-body">
        
        
      </div>
    </div>
  </div>
</div>   
         


<!-- Modal -->
<div class="modal fade" id="viewCategorySummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category Summary</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="viewAccumulative" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Accumulative Cost</h4>
      </div>
      <div class="modal-body">
        <div class="well" style="border-radius: 0px">
            <?php if(count($recall)> 0){
                echo $recall_accumulate;
            } ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="viewCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Item Cost Details</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
               <!--end of cost monitor modals-->
                
        

<!-- Modal for blockerNote-->
<div class="modal fade" id="blockerNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
    
      <div class="modal-header">
        <h4>Notes
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </h4>
      </div>
      <div class="modal-body">
            <h2 class="text-center text-muted block_load_holder" style="display: none;"><i class="fa fa-spinner fa-spin"></i></h2>
            <div class="well blocker_note_holder" style="color: #333; border-radius: 0; display: none;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>       

        
      
<!-- Modal -->
<div class="modal fade" id="incidentRecallLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
       

        <div class=" ">
          <table class="table table-hover">
            <thead>
            	<tr>
                    <th colspan="2"> <h4 class="modal-title" id="myModalLabel"><?php echo  $recall[0]['incident_no'] .' - '. $recall[0]['incident_name']?> Log</h4></th>
                
                </tr>
            </thead>   
            
            <tbody>
            	<tr>
                	<td><b>Incident Started </b></td>
                	<td><?php echo date_format(date_create($recall[0]['initiation_date']), 'jS F Y - g:ia'); ?></td>
                </tr>
                
            	<tr>
                	<td><b>Incident Ended </b></td>
                	<td><?php echo date_format(date_create($recall[0]['closed_date']), 'jS F Y - g:ia'); ?></td>
                </tr>
                
            	<tr class="success">
                	<td><b>Total Time</b></td>
                	<td><?php

						$inci_tot = explode(' ',$recall[0]['incident_total_time']);
						$unit_time = array(
							'year',
							'month',
							'day',
							'hour',
							'minute',
							'second'
						);
						$u = 0; 
						foreach($inci_tot as $tot){
							if ($tot == 0 ){
							}
							else if ($tot == 1 ){
								echo $tot.' '.$unit_time[$u].' ';
							}
							else{
								echo $tot.' '.$unit_time[$u].'s ';
							}
							$u++;
						}
					
					?></td>
                </tr>

            	<tr class="hidden">
                	<td><b>First Task Completed</b></td>
                	<td><?php echo date_format(date_create($recall[0]['tasks_started']), 'jS F Y - g:ia');?></td>
                </tr>
                
            	<tr class="hidden">
                	<td><b>Last Task Completed</b></td>
                	<td><?php echo date_format(date_create($recall[0]['tasks_ended']), 'jS F Y - g:ia');?></td>
                </tr>
                
            	<tr class="success hidden">
                	<td><b>Total Time</b></td>
                	<td><?php
					
						$tasks_tot = explode(' ',$recall[0]['total_time_tasks']);
						$unit_time = array(
							'year',
							'month',
							'day',
							'hour',
							'minute',
							'second'
						);
						$u = 0; 
						foreach($tasks_tot as $tot){
							if ($tot == 0 ){
							}
							else if ($tot == 1 ){
								echo $tot.' '.$unit_time[$u].' ';
							}
							else{
								echo $tot.' '.$unit_time[$u].'s ';
							}
							$u++;
						}
					
					?></td>
                </tr>
                
                
             </tbody> 
          </table>
          <?php
		  	if($recall_task_count > 0){
				// onclick="save_inci_csv('download');" data-dismiss="modal"
		  ?>
          
          <p class=""><button class="btn btn-default btn-block btn-lg" onclick="$(this).closest('p').hide();$('#extract_form').show();"><i class="fa fa-file-excel-o"></i> Extract Incident into CSV File</button></p>
          <form id="extract_form" style="display: none;">
              <input type="hidden" id="close_inci_form" data-id="<?php echo $recall[0]['id']; ?>" />
              <div class="form-group">
              	<label>Please enter EXTRACT in the field below.</label>
              	<input type="text" class="form-control extract_field" />
                <span class="text-red"></span>
              </div>
              <div class="form-group">
              	<button class="btn btn-primary" disabled="disabled">Download CSV</button>
              </div>
          </form>
          
          
          
          
          <?php } else{ ?>
          
          <p class="hidden"><a href="<?php echo base_url().'uploads/recall-csv/'.$recall[0]['csv_file_name']?>" class="btn btn-default btn-block btn-lg"><i class="fa fa-download"></i> Download Incident CSV File</a></p>
          
          <?php } ?>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


	<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Incident</h4>
        <ul class="list-group list-default">
            <?php if(count($open_recall) > 0){
                $count_open = 0;
                $uri_5 = $this->uri->segment(5);
                foreach($open_recall as $open=>$opeen){
            ?>
            
            <a href="<?php echo base_url().'cc/log/index/r/'.$count_open ?>" class="list-group-item <?php if ($uri_5 == $count_open) { echo 'active'; }?>"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
            
            <?php
                $count_open++;	
                } 
            } ?>
        </ul>


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>

<?php


	if (count($recall) > 0){
		

		foreach($recall as $r=>$rc){//start loop each recall
			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			if($active_mod == '5'){
				$incident_type = '5';
				$pack_id = $rc['input_pack_id']; 
				$recall_field_name = 'recall_id'; 
				$db_name = 'cf_recall_steps'; 
			}
			else{
				$incident_type = '8';
				$pack_id = 17; 
				$recall_field_name = 'continuity_id'; 
				$db_name = 'cf_continuity_steps'; 
			}
		
			$continuity_id = $pack_id; //17;

			if($rc['input_pack_id'] != 0){
				$active_mod  = substr($this->session->userdata('org_module'),0,1);

				$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('id'=>$continuity_id)); ?>

                
                
    <div class="col-lg-12">


        <p>
        
            <?php
                $more_inci = count($open_recall) - 1;
                $my_inci_label ='';
                if (count($open_recall) == 1){
                    $my_inci_label .= '1 Closed Incident '; ?>
                   <button class="btn btn-default disabled"><?php echo $my_inci_label; ?></button> 
            
            <?php
                }
                if (count($open_recall) > 1){
                    $my_inci_label .= count($open_recall).' Closed Incidents '; ?>
                   <button class="btn btn-primary" data-toggle="modal" data-target="#myRecalls"><?php echo $my_inci_label; ?></button> 
            <?php
                }
                //echo $this->pagination->create_links(); 
            ?>
        
        </p>    


        <div class="panel panel-default" style="radius: 0px;">
        


            <div class="panel-heading">
            

                <div class="panel-title">
                    <h4>
                        <?php echo $rc['incident_no'].': '.$rc['incident_name'] ?>
                            
						<?php
                        
                        if ($rc['initiation_type'] == 0){
                            
                            echo '<span class="label label-default">Mock</span>';
                        
                        }
                        
                        else{
                        
                            echo '<span class="label label-info">Live</span>';
                            
                        }
                        
                        ?>
                                
                                
                        

                    </h4>
    
                </div>
    
                <div class="panel-widgets hidden">
                    <a data-toggle="collapse" data-parent="#accordion" href="#open_incident" style="text-decoration: none;">
                    <i class="fa fa-chevron-up tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php // echo 'View '.$more_inci.' more incidents.';?>"></i></a>

                </div>
                <div class="clearfix"></div>
        
                </div>
        
                
        
                <div class="panel-body">
    				<?php
						if($recall_task_count == 0){
					?>	
                    <h5 class="well text-muted"><a href="#" data-toggle="modal" data-target="#incidentRecallLog">View Incident Log</a></h5>
                    
                    <?php
						} else{
					?>

              		<div class="row">  
                        <div class="col-md-3 col-lg-3 steps_holder">
                     
                     
                            <ul class="nav nav-pills nav-stacked navigation" id="myTab" style="margin-bottom: 10px;">
                                
                                <?php
								
								//active module is recall
								if($active_mod == '5'){
									$dbcell = 'recall_id';
									$thedb = 'cf_recall_steps';
								}else{
									$dbcell = 'continuity_id';
									$thedb = 'cf_continuity_steps';
								}
								
									//get blockers 
									$bb = array(
										'cc_id'=>$this->session->userdata('logged_cc_login_id'),
										$recall_field_name=>$rc['id']
									);
									$bb_unresolved = array(
										'cc_id'=>$this->session->userdata('logged_cc_login_id'),
										$recall_field_name=>$rc['id'],
										'status'=>'0'
									);
									$blockers = $this->master_model->getRecords('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));
									$unresolved_blockers = $this->master_model->getRecords('cf_recall_blockers',$bb_unresolved,'*',array('id'=>'DESC'));
									if(count($unresolved_blockers) > 0){
										$blocker_stat = 'text-red bg-danger';
										$blocker_count = '<span class="label label-danger" style="font-size: 65%; border-radius:50%; padding-left: 7px;padding-right: 7px;">'.count($unresolved_blockers).'</span>';
									}else{
										$blocker_stat = 'text-green bg-success';
										$blocker_count = '<i class="fa fa-check-circle fa-fw"></i>';
									}
								?>   
                                
                                <?php //KPIs module
								if (strpos($the_active_module[0]['active_module'], '0') !== false){ ?>
                                <li class="active" id="recall_cost_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#recall_kpi<?php echo $rc['id'] ?>" class="text-info bg-info">
                                    <i class="fa fa-calculator fa-fw"></i> Recall KPIs <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                <?php } ?>
                                
                                
                                <?php //cost monitor module
								if (strpos($the_active_module[0]['active_module'], '6') !== false){ ?>
                                <li class="" id="recall_cost_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#recall_cost<?php echo $rc['id'] ?>" class="text-info bg-info">
                                    <i class="fa fa-usd fa-fw"></i> Cost Monitor <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                <?php } ?>

                                <li class="" id="blockers_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#blockers-<?php echo $rc['id'] ?>" class="<?php echo $blocker_stat ?>">
                                    <?php echo $blocker_count ?> Issues Board <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                      
<?php

if (count($myrecallpack) > 0){ //check if recall input pack
    $recallsteps = $this->master_model->getRecords('cf_recall_packs_steps',
		array('recall_pack_id'=>$myrecallpack[0]['id'], 'date <'=>$rc['initiation_date']),'*',
		array('order'=>'ASC'));
		
    $rpack_step_no = 2;
    foreach($recallsteps as $rst=>$rstep){ //loop step category
		
		//set step no via steps
		$rpack_step_no = $rstep['step_no'];
		
		//disect each step
		${'questions' . $rpack_step_no} = $this->master_model->getRecords($thedb,
			array('recall_id'=>$rc['id'],'step_no'=>$rpack_step_no),'*',
			array('category_id'=>'ASC'));
		
		
		//total question
		${'totq2' . $rpack_step_no} = count(${'questions' . $rpack_step_no});
		
		
		
		//check if each step is completed starts in step 3
		${'step' . $rpack_step_no . '_label'} = 0;
		
		foreach(${'questions' . $rpack_step_no} as $qu=>$qus){
			if($qus['status'] == '0'){
				${'step' . $rpack_step_no . '_label'}++;
			}
		}
		
		//steps label variables
		 if (${'step' . $rpack_step_no . '_label'} == '0'){
			 ${'step' . $rpack_step_no . 'color'} = 'text-green bg-success';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-check-circle fa-fw"></i>';
		} 
		 else if (count(${'questions' . $rpack_step_no}) == ${'step' . $rpack_step_no . '_label'} ){
			 ${'step' . $rpack_step_no . 'color'} = 'text-red bg-danger';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-circle-o fa-fw"></i>';
		} 
		 else{
			 ${'step' . $rpack_step_no . 'color'} = 'text-orange bg-warning';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-minus-circle fa-fw"></i>';
		 }
		
    
        
?>
                    
                                <li class="<?php if($rpack_step_no == 2 && strpos($the_active_module[0]['active_module'], '0') === false) { echo 'active'; }?>" id="step_<?php echo $rpack_step_no; ?>btn">
                                    <a data-toggle="tab" href="#step<?php echo $rpack_step_no.'-'.$rc['id'] ?>" class="<?php echo ${'step' . $rpack_step_no . 'color'}; ?>">
                                    <?php echo ${'step' . $rpack_step_no . 'icon'}; ?> <?php echo ucfirst($rstep['name']); ?> <i class="fa fa-angle-double-right"></i></a>
                                </li>



 <?php

        $rpack_step_no++;
    }
?>
                
                
    
                            </ul>
							<h5 class="well text-muted"><a href="#" data-toggle="modal" data-target="#incidentRecallLog">View Incident Log</a></h5>
							<h5 class="well text-muted hidden">Date Closed: <?php echo date_format(date_create($rc['closed_date']), 'jS F Y - g:ia'); ?></h5>

                        </div>
                        <div class="col-md-9 col-lg-9">
                            
                            
                          <p class="lead text-muted text-center tasktab_loader" style="margin-top: 65px;">
                          	<i class="fa fa-spinner fa-spin"></i><br />
                            Loading..

                          </p>  
                        
                        <div class="user-body">
                            <div class="tab-content">
                            
                                <div class="tab-pane fade" id="blockers-<?php echo $rc['id'] ?>">
                                
    <h3>Issues Board


        <!-- Single button -->
        <div class="btn-group pull-right" style="margin-right: 15px;">
          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 60%; min-width: 82px; right: 0; left: auto;">
          
            <li class="active all_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','','.all_blocker_btn'); ">All</a></li>
          
            <li class="open_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','open','.open_blocker_btn');">Open</a></li>
          
            <li class="closed_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','resolved','.closed_blocker_btn'); ">Closed</a></li>
  
            <li class="my_issues"><a href="javascript:;" onclick="filterTasks('-blockers','<?php echo $this->session->userdata('logged_display_name')?>','.my_issues');">My Issues</a></li>
            
            
          </ul>
        </div> 
                
    </h3>
    
      <table class="table forum table-striped" id="recall-step-filter-table-blockers">
        <thead>
          <tr>
            <th class="cell-stat text-center"></th>
            <th>
                <div class="col-sm-12 hidden">
                    <input type="text" class="form-control" id="recall-step-filter-blockers" data-action="filter" data-filters="#recall-step-filter-table-blockers" placeholder="Filter Tasks"/>
            
                </div>
            
            </th>
            <th class="cell-stat-1-point-5x text-right hidden"></th>
          </tr>
        </thead>
        <tbody>
        
        	<?php
			
			if(count($blockers) > 0){
				foreach( $blockers as $b=>$blck){
			?>
 			<tr>
            	<td class="text-center" >
                    <button class="btn btn-default disabled" id="comp_blck<?php echo $blck['id']?>" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if($blck['status'] == '0') { echo 'display: none'; }?>" onclick="checkTheBlocker('0','<?php echo $blck['id']?>','<?php echo count($unresolved_blockers)?>','<?php echo $rc['id']?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                    
                    
                    <button class="btn btn-default disabled" id="inc_blck<?php echo $blck['id']?>" style="padding:3px; <?php if($blck['status'] == '1') { echo 'display: none'; }?>" onclick="checkTheBlocker('1','<?php echo $blck['id']?>','<?php echo count($unresolved_blockers)?>','<?php echo $rc['id']?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>


                </td>
            	<td>
                  <h4><?php echo $blck['blocker']?><br>
                  
                  	<small>
                    
                        <span class="blocker_note_empty" style=" <?php if ($blck['note'] != ''){ echo 'display: none'; } ?>">No notes added</span>
                        
                        <a href="#" class="blocker_note_btn" data-toggle="modal" data-target="#blockerNote" onclick="loadBlockerNote('<?php echo $blck['id']?>');" style=" <?php if ($blck['note'] == ''){ echo 'display: none'; } ?>">View notes</a><br />
                        
                        
                        Issue: <?php echo $blck['issue']?><br />
                        Impact: <?php echo $blck['impact']?><br />
                        Raised by: <span class="label label-warning" style="border-radius: 15px;"><?php echo $this->common_model->getcrtname($blck['crt_id']);?></span><br />
                        Dependent Task(s): <?php echo $this->common_model->taskname($blck['task_id'], $db_name);?><br />
                        Assigned to: <span class="label label-warning" style="border-radius: 15px;"><?php if($blck['assigned'] != '0') { echo $this->common_model->getcrtname($blck['assigned']); } else { echo 'Not assigned'; }?></span>
                    </small>
                  </h4>
                  <p>
                	<span class="small">Date Raised: <span class="text-muted"><?php echo date_format(date_create($blck['date_created']), 'm.d.Y, g:ia'); ?></span></span><br />
                	<span class="small date_resolved_holder">
						<?php if ($blck['status'] == '1'){?>
                            Date Resolved: <span class="text-muted"><?php echo date_format(date_create($blck['date_resolved']), 'm.d.Y, g:ia'); ?></span>
						<?php }else { echo '<span style="visibility: hidden">open</span>';} ?>
                    </span>
                  </p>
                
                </td>
            	<td class="text-right hidden">
                    <a href="#" class="btn btn-sm btn-default" id="add_note_blck<?php echo $blck['id']?>" style="background:#e0e0e0; <?php if ($blck['status'] == '1'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#resolveBlocker" onclick="openResolveBlocker('<?php echo $rc['id']?>','<?php echo $blck['id']?>');loadBlockerNote('<?php echo $blck['id']?>');"><i class="fa fa-gavel tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolve"></i></a>
                
                </td>
            </tr>     
        
        
        <?php
				}
			} else { ?>
        
        
        	<tr>
            	<td colspan="3" class="text-muted">No blockers</td>
            </tr>
        	
        <?php } ?>
		</tbody>
      </table>    
    
    
    
                                </div>
                                <!-- end of blockers tab-->
                                
                                
                                
                                
                                
                                
                               
								<?php //Recall feature KPIs module //tab-pane fade in 
                                    if (strpos($the_active_module[0]['active_module'], '0') !== false){ ?>
                                
                                    
                                <div class="tab-pane steptab_active" id="recall_kpi<?php echo $rc['id'] ?>">
                   
                   
        <?php
			$cc_id = $this->session->userdata('logged_cc_login_id');
			$org_id = $this->session->userdata('cc_selected_orgnaization');
			
			$incident_type = substr($this->session->userdata('org_module'), 0, 1);
			$whr_inci = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'closed'=>'0'
			);
			
			if($incident_type == '5'){
				$inci_type = 'recall';
				$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
			}
			else{ //continuity = 8
				$inci_type = 'continuity';
				$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
			}
			
			
			$whr_noti = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'type'=>$inci_type,
				'incident_id'=>$rc['id']
			);
			
			$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
			
			$email_sent_count = 0;
			$email_confirmed_count = 0;
			$sms_sent_count = 0;
			if(count($notices_sent) > 0){                
				foreach($notices_sent as $r=>$ns){
					//email sent count
					$whr_email_sent = array(
						'bulk_id'=>$ns['id'],
						'email_sent'=>'1'
					);
					$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

					//email confirmed count
					$whr_email_confirmed = array(
						'bulk_id'=>$ns['id'],
						'email_confirmed'=>'1'
					);
					$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


					//sms sent count
					$whr_sms_sent = array(
						'bulk_id'=>$ns['id'],
						'sms_sent'=>'1'
					);
					$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
					
					$email_sent_count += $bulk_data_email_sent;
					$email_confirmed_count += $bulk_data_email_confirmed;
					$sms_sent_count += $bulk_data_sms_sent;
				
				}
				
			}
	
			//get notification kpi
			$noti_kpi = 0;
			if($email_sent_count != 0){
				$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
			}
			
			
			//$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);

			$whr_kpis = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'incident_id'=>$rc['id'],
				'incident_type'=>$incident_type
			);
			
			$kpis = $this->master_model->getRecords('bulk_kpi', $whr_kpis, '*', array('date_added'=>'DESC'));

	
			//set kpi variables
			$all_kpi = array();
			
			$all_kpi[] = array(
				'type'=>'comp_kpi',
				'dbcell'=>'comp_data',
				'bgcolor'=>'orange',
				'name'=>'Units under Company control'
			);
		
			$all_kpi[] = array(
				'type'=>'dist_kpi',
				'dbcell'=>'dist_data',
				'bgcolor'=>'blue',
				'name'=>'Units in distribution'
			);
		
			$all_kpi[] = array(
				'type'=>'market_kpi',
				'dbcell'=>'market_data',
				'bgcolor'=>'red',
				'name'=>'Units in consumer market'
			);
	


		?>
        
                   
                   
                                
                                
    <h3>Recall KPIs
    
        <small class="hidden">
			<?php if(count($kpis) > 0){ 
			
			if($kpis[0]['date_updated'] == '0000-00-00 00:00:00'){ ?>
            <span class="text-muted" title="<?php echo $kpis[0]['date_added'];?>"><?php echo 'added '. $this->common_model->ago($kpis[0]['date_added']); ?></span></td>
            
            <?php } else{ ?>
            <span class="text-muted" title="<?php echo $kpis[0]['date_updated'];?>"><?php echo 'updated '. $this->common_model->ago($kpis[0]['date_updated']); ?></span></td>
            
            <?php } }?>
        </small>
    
    </h3>
    <div class="col-xs-12" style="padding-top: 20px; padding-left: 0px;">
       <div class="row">
    
    
    		<?php
			$count_all_kpi = 1;
			foreach($all_kpi as $r=>$value){
				$kpi_cell = $value['dbcell'];
				if($value['type'] != 'comp_kpi') {
					$akpiclass = 'text-primary';
					$akpistyle = 'cursor: pointer;';
					
					if($value['type'] == 'dist_kpi'){
						$akpiclass .=" dist_toggle_btn";
					}
				}
				else{
					$akpiclass= '';
					$akpistyle= '';
				}
				
				//check count of kpi
				if(count($kpis) > 0) {
					$kpi_id = $kpis[0]['id'];
					$dbcell = unserialize($kpis[0][$value['dbcell']]);
					if($dbcell != ''){ //get each kpi db cell data
						$total_unit = number_format($dbcell['total_unit']);
						$unit_return = number_format($dbcell['unit_return']);
						$u_disposed = number_format($dbcell['u_disposed']);
						$u_transformed = number_format($dbcell['u_transformed']);
						$u_corrections = number_format($dbcell['u_corrections']);
						$the_kpi = round($dbcell['the_kpi'], 0).'%';
						$u_date_update = $dbcell['date_updated'];
					
					}else{
						$total_unit = '';
						$unit_return = '';
						$u_disposed = '';
						$u_transformed = '';
						$u_corrections = 'NA';
						$the_kpi = '';
						$u_date_update = '';
					
					}
				}
				else{
					$kpi_id = '';
					$total_unit = '';
					$unit_return = '';
					$u_disposed = '';
					$u_transformed = '';
					$u_corrections = 'NA';
					$the_kpi = '';
					$u_date_update = '';
				}
			?>
            
            <div class="col-sm-6">
                <div class="tile <?php echo $value['bgcolor']; ?> dash-demo-tile">
                    <h4><?php echo $value['name']; ?></h4>
                    <div class="row">
                    	<div class="col-lg-5 text-center">
                            <div id="easy-pie-1" class="easy-pie-chart" data-percent="<?php echo $the_kpi;?>">
                                <span class="percent"></span>
                            </div>
                        </div>
                    
                    	<div class="col-lg-7">
                        	<ul class="list-unstyled easy-pie-data" style="font-size: 90%;">
                            	<?php
								$nokpidata = '<li>No data</li>';
								if(count($kpis) > 0) {
									if($dbcell != '') {
										
										?>
                                
                                
                            	<li>Total Units: <?php echo $total_unit; ?></li>
                            	<li>Units Disposed: <?php echo $u_disposed ?></li>
                            	<li>Units Transformed: <?php echo $u_transformed; ?> </li>
                            	<li>Total Corrections: <?php echo $u_corrections; ?></li>
                            	<li style="border-top: 1px solid; border-color: rgba(255,255,255, .2); color: rgba(255,255,255, .5);"><small> 
                                	<?php echo date_format(date_create($u_date_update), 'M d, Y, g:i A'); ?>
                                 </small></li>
                                
                                <?php
									}
									else {
										echo $nokpidata;
									}
								}
								else{
									echo $nokpidata;
								}
								
								?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
			
			if(($count_all_kpi%2) == 0){
				echo '<div class="clearfix"></div>';
			}
			
			$count_all_kpi++;
			}//end loop kpis ?>




            <div class="col-sm-6">
                <div class="tile purple dash-demo-tile">
                    <h4>Outbound notifications</h4>
                    <div class="row">
                    	<div class="col-lg-5 text-center">
                            <div id="easy-pie-4" class="easy-pie-chart" data-percent="<?php echo $noti_kpi; ?>">
                                <span class="percent"></span>
                            </div>
                        </div>
                    
                    	<div class="col-lg-7">
                        	<ul class="list-unstyled easy-pie-data" style="font-size: 90%;">
                            	<li>Notices Sent: <?php echo $email_sent_count; ?></li>
                            	<li>Notices Received: <?php echo $email_confirmed_count; ?></li>

                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    
       
       </div><!--end of row--> 		
       

        <div class="table-responsive">
        
        
            <div class="form-group hidden">
                <input class="form-control" type="text" name="contact_cntr" id="contact_cntr" value="<?php if(count($kpis) > 0) { echo $kpis[0]['contact_cntr']; } ?>"/>
                <input class="form-control" type="text" name="kpi_email" id="kpi_email" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_email']; } ?>"/>
                <input class="form-control" type="text" name="kpi_website" id="kpi_website" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_website']; } ?>"/>
                <input class="form-control" type="text" name="kpi_trading" id="kpi_trading" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_trading']; } ?>"/>
            </div>
        

        <table class="table customcontactstable" id="example-tablexx">

            <thead>

                <tr>
                    
                    <th>Customer Contacts</th>
                    
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Contact Centre</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Email</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Website</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Trading Partner</th>



                </tr>

            </thead>

            <tbody>
            
				<?php
                    if(count($kpis) > 0) {
                        if($kpis[0]['contact_cntr'] !=''){
                            $contact_cntr = number_format($kpis[0]['contact_cntr']);
                            $kpi_email = number_format($kpis[0]['kpi_email']);
                            $kpi_website = number_format($kpis[0]['kpi_website']);
                            $kpi_trading = number_format($kpis[0]['kpi_trading']);
                        
                        }else{
                            $contact_cntr = 'No data';
                            $kpi_email = 'No data';
                            $kpi_website = 'No data';
                            $kpi_trading = 'No data';
                            
                        }
                    }
                    else{
                        $contact_cntr = 'No data';
                        $kpi_email = 'No data';
                        $kpi_website = 'No data';
                        $kpi_trading = 'No data';
                        
                    }
                ?>
            
                <tr class="tr_main">
                    <td><i>Inbound customer contacts</i><br />
                        <button class="btn btn-primary submit_kpis btn-xs hidden disabled" type="submit">Save</button>
                        <a class="btn btn-default btn-xs hidden cancel_kpi_btn">Cancel</a>
                    
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <a href="javascript:void(0);"><?php echo $contact_cntr; ?></a>
                    

                        <input type="text" class="form-control hidden" name="contact_cntr" value="<?php echo $contact_cntr; ?>"/>
                        
                        <input type="hidden" class="form-control hidden" name="id" value="<?php if(count($kpis) > 0) { echo $kpis[0]['id']; }?>"/>
                        
                        <input type="hidden" class="form-control hidden" name="incident_id" value="<?php echo $rc['id']; ?>"/>
                        <input type="hidden" class="form-control hidden" name="incident_type" value="<?php echo substr($this->session->userdata('org_module'), 0, 1); ?>"/>
                        
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        
                        <a href="javascript:void(0);"><?php echo $kpi_email; ?></a>

                        <input type="text" class="form-control hidden" name="kpi_email" value="<?php echo $kpi_email; ?>"/>
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                       
                        
                        <a href="javascript:void(0);"><?php echo $kpi_website?></a>

                        <input type="text" class="form-control hidden" name="kpi_website" value="<?php echo $kpi_website; ?>"/>
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                      
                        <a href="javascript:void(0);"><?php echo $kpi_trading; ?></a>

                        <input type="text" class="form-control hidden" name="kpi_trading" value="<?php echo $kpi_trading;?>"/>
                       
            
                    </td>


                </tr><!--consumer market-->
                
                <?php if(count($kpis) > 0) {
                    if($kpis[0]['kpi_trading'] !=''){
                    ?>


                <tr>
                    <td>&nbsp;</td>
                    <td colspan="4" style="border: 1px solid #ddd;">
                    
                    
                                            
    
                        <!-- Pie Chart Example -->
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-chart-pie"></div>
                        </div>
                        
                        <p class="hidden">&nbsp;<a href="#" data-toggle="modal" data-target="#chartDataModal" style="display: none;"><i class="fa fa-edit"></i> Edit</a></p>
                    
                    
                    </td>
                </tr>

                <?php } } ?>

           
                                

            </tbody>


        </table>

    </div>

    <!-- /.table-responsive -->




    
    </div><!--/col-xs-12-->

    
    
    
                                </div>
                                <!-- end of KPIs module tab-->
                                <?php } ?>
                                         
                                
                                
                                
                                
                                
                                
                                
								<?php //Recall feature cost monitor module activated
                                    if (strpos($the_active_module[0]['active_module'], '6') !== false){ ?>
                                    
                                <div class="tab-pane fade" id="recall_cost<?php echo $rc['id'] ?>">
                                
                                
    <h3>Cost Monitor
    
    

                        <?php if(count($recall)> 0){ ?>
                        <a class="btn btn-primary btn-sm pull-right hidden-xs" href="#" data-toggle="modal" data-target="#viewAccumulative" style="width: 150px;">Running Cost</a> 
                        
                        <a class="btn btn-primary btn-sm pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#viewAccumulative">Running Cost</a> 
                        <?php } ?>
    
    </h3>
    
             
<?php 
						
	if (count($recall) > 0){
	?>
    
							<div class="col-xs-12" style="padding-top: 20px; padding-left: 0px;">
    
								<?php 

								if(count($cost_category)>0)

								{ ?>

							<?php /*?><div class="" style="padding-top: 20px;"><?php */?>

							<table class="table" id="example-tablexx">

								<tbody>

								<?php
								
									foreach($cost_category as $r => $value)

									{
										
								?>

                                                        

                                    <tr class="bg-info">

                                    	<td width="100%">
                                        <h4 style="text-transform: uppercase; font-weight: bold"><?php echo  $value['name']; ?></h4>
             
                                        
                                        </td>

                                        <td class="pull-right">
                                    
       <!-- Single button -->
        <div class="btn-group" style="margin-top: 5px;">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm hidden" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; left: auto; right: 0;">
          
            <li>
                <a href="#" data-toggle="modal" data-target="#addCostItem" onclick="resetItemFields('<?php echo $value['cost_id']?>','<?php echo $recall[0]['default_currency_id']?>');">Add Cost Item</a>
            </li>
          
            <li>
                <a href="javascript:;" onclick="updateCostCat('<?php echo $value['cost_id']?>','<?php echo $value['name']?>')">Edit Category Name</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return delete_cost_cat('<?php echo $value['cost_id'] ?>');">Delete Category</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#viewCategorySummary" onclick="showCostSummary('<?php echo $value['cost_id'] ?>')">View Category Sub-Total</a>
            </li>
          </ul>
        </div>    

										
                                    </td>

                                    </tr>

									<tr>
                                    	<td colspan="2">
                                        
                                        
                           
								<?php
                                
                                $items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$value['cost_id']));
                                ?>
                                        
            

								<?php 

								if(count($items)>0)

								{ ?>

							<?php /*?><div class="table-responsive"><?php */?>

							<table class="table table-hover table-bordered table-datatable">

                                <thead>

                                    <tr>

                                        <th width="10%">Date</th>
                                        
                                        <th width="60%">Item</th>

                                        <th width="10%">CUR</th>

                                        <th width="14%">Cost</th>

                                        <th width="6%"></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($items as $r => $value)

									{
										
								?>

                                                        

                                    <tr>

                                    	<td width="10%"><?php echo $value['item_date_invoice']?></td>
                                    	<td width="60%">
										
										<?php
										
										$files = $this->master_model->getRecords('private_message_file',array('cost_item_id'=>$value['item_id']));
										if(count($files) > 0){
											echo '<i class="fa fa-paperclip text-info fa-fw"></i> ';
										}else{
											echo '<i class="fa fa-hand-o-right text-info fa-fw" style="visibility:hidden"></i> ';
										}
										echo  $value['item_name']; ?>
                                        
                                        </td>

                                    	<td width="10%"><?php echo $my_class->currencycode($value['currency']) ?></td>
                                    	<td width="14%" class="text-right"><?php echo number_format($value['item_cost'],2); ?></td>

                                        <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; right: 0; left: auto;">
            <li>
                <a href="javascript:;" onclick="updateItemCost('<?php echo $value['item_id']?>','<?php echo $value['item_name']?>','<?php echo $value['item_cost']?>','<?php echo $value['currency']?>','<?php echo $value['item_date_invoice']?>')">Edit Cost Item</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return delete_cost_item('<?php echo $value['item_id'] ?>');">Delete Cost Item</a>
            </li>
            <li class="hidden">
                <a href="#" data-toggle="modal" data-target="#viewCostItem" onclick="viewCostItemDetails('<?php echo $value['item_id']?>')">View Cost Detail</a>
            </li>
            
		<?php if(count($files) > 0){ ?>
        
            <li>
                <a href="<?php echo base_url().'cc/costmonitor/download/'.$files[0]['file_upload_name']?>" target="_blank"> Download Document</a>
            </li>
            
		<?php } ?>
          </ul>
        </div>    


                                    </td>

                                    </tr>



								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        <?php /*?></div><?php */?>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-list-ul" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost items for <?php echo $value['name']?></p>
								<?php }

								?>

                                                                                
                                        
                                        
                                        
                                        
                                        </td>
                                    </tr>

								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        <?php /*?></div><?php */?>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>

                                            <p class="text-center hidden" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-usd" style="font-size: 90px"></i></p>
                                            <p class="text-center" style="color: #ccc; margin-top: 60px;">No cost category</p>

								<?php }

								?>







    
	<?php
    } else{
	?>
        	
        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-history" style="font-size: 90px"></i></p>
        <p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall</p>
	<?php	
	}
?>
    
    
               </div>
    
    
    
                                </div>
                                <!-- end of recall_cost tab-->
                                
								<?php } ?>
                                
                                
                                
                                
    <?php
	//variables, start of continuity questions/tasks
	
    $rpack_step_no = 2;
    foreach($recallsteps as $rst=>$rstep){ //loop step category
	
		//set step no via steps
		$rpack_step_no = $rstep['order'] + 1;
		
		//disect each step
		${'questions' . $rpack_step_no} = $this->master_model->getRecords($thedb,
			array('recall_id'=>$rc['id'],'step_no'=>$rpack_step_no),'*',
			array('category_id'=>'ASC', 'id'=>'DESC'));
		
		
		//total question
		${'totq' . $rpack_step_no} = count(${'questions' . $rpack_step_no});
		
		
		
		//check if each step is completed starts in step 3
		${'step' . $rpack_step_no . '_label'} = 0;
		
		foreach(${'questions' . $rpack_step_no} as $qu=>$qus){
			if($qus['status'] == '0'){
				${'step' . $rpack_step_no . '_label'}++;
			}
		}
		
		//steps label variables
		 if (${'step' . $rpack_step_no . '_label'} == '0'){
			 ${'step' . $rpack_step_no . 'color'} = 'text-green bg-success';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-check-circle fa-fw"></i>';
		} 
		 else if (count(${'questions' . $rpack_step_no}) == ${'step' . $rpack_step_no . '_label'} ){
			 ${'step' . $rpack_step_no . 'color'} = 'text-red bg-danger';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-circle-o fa-fw"></i>';
		} 
		 else{
			 ${'step' . $rpack_step_no . 'color'} = 'text-orange bg-warning';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-minus-circle fa-fw"></i>';
		 }
		

?>                                
                                
                                
                                <div class="tab-pane <?php if($rpack_step_no  == 2 && strpos($the_active_module[0]['active_module'], '0') === false) { echo 'steptab_active'; }?>" id="step<?php echo $rpack_step_no; ?>-<?php echo $rc['id'] ?>">
                        
                        


    <h3><?php echo $rstep['name']; ?>
    
    
    <span id="proc<?php echo $rpack_step_no; ?>" <?php if (${'step' . $rpack_step_no . '_label'} != 0){ echo 'style="display: none;"'; }?>>
        <i class="fa fa-check-circle text-green tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Completed"></i> 
       <span class="<?php if (($rpack_step_no-1) == count($recallsteps)) { echo 'hidden'; } ?>"> <a data-toggle="tab" href="#step<?php echo $rpack_step_no + 1; ?>-<?php echo $rc['id'] ?>" class="small" style="text-decoration: none;" onclick="$('#step_<?php echo $rpack_step_no + 1; ?>btn').click();$('#step_<?php echo $rpack_step_no + 1; ?>btn').addClass('active');$('#step_<?php echo $rpack_step_no + 1; ?>btn').siblings('li').removeClass('active');" >Proceed to Step <?php echo $rpack_step_no; ?> <i class="fa fa-arrow-right"></i></a></span>
        </span>
        
        


        <!-- Single button -->
        <div class="btn-group pull-right" style="margin-right: 15px;">
          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 60%; min-width: 82px; right: 0; left: auto;">
            <li class="active"><a href="javascript:;" class="all_tasks_btn<?php echo $rpack_step_no; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','','.all_tasks_btn<?php echo $rpack_step_no; ?>');">All Tasks</a></li>
            <li><a href="javascript:;" class="not_assigned_btn<?php echo $rpack_step_no; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','Not Assigned','.not_assigned_btn<?php echo $rpack_step_no; ?>');">Not Assigned</a></li>
            
            
            
            <?php 
			
                    if(count($all_crt)>0){
            			$crt_cawnt = 1;
                        foreach($all_crt as $r => $value){

							$mycrtname = $value['full_name'];
							?>
							
							
							<li><a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?><?php echo $crt_cawnt; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','<?php echo $mycrtname ?>','.crt_tasks_btn<?php echo $rpack_step_no; ?><?php echo $crt_cawnt ?>');">
							
							<?php echo $mycrtname; ?>
							
							
							</a></li>
							
							<?php
							$crt_cawnt++;

						}
					
					}
			
			?>
            
            <li class="divider"></li>
            <li>
            	<a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?>inprog" onclick="filterTasks('<?php echo $rpack_step_no; ?>','In Progress','.crt_tasks_btn<?php echo $rpack_step_no; ?>inprog');">
                	In Progress
                </a>
            </li>
            <li>
            	<a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?>awaiting" onclick="filterTasks('<?php echo $rpack_step_no; ?>','Awaiting Approval','.crt_tasks_btn<?php echo $rpack_step_no; ?>awaiting');">
                	Awaiting Approval
                </a>
            </li>
            <li>
            	<a href="javascript:;" class="crt_tasks_btn<?php echo $rpack_step_no; ?>comp" onclick="filterTasks('<?php echo $rpack_step_no; ?>','Completed','.crt_tasks_btn<?php echo $rpack_step_no; ?>comp');">
                	Completed
                </a>
            </li>
          </ul>
        </div>    
                
        
    </h3>
                        
                             
                             
      <table class="table forum table-striped table-list-search" id="recall-step-table<?php echo $rpack_step_no; ?>">
        <thead>
          <tr>
            <th>
                <div class="col-sm-12 hidden">
                    <input type="text" class="form-control" id="recall-step-filter<?php echo $rpack_step_no; ?>" data-action="filter" data-filters="#recall-step-table<?php echo $rpack_step_no; ?>" placeholder="Filter Tasks" />
                </div>
			</th>
            <th class="cell-stat-1-point-5x text-right hidden-xs hidden-sm hidden-md"></th>
            <th class="cell-stat-1-n-half text-center hidden-xs"></th>
            
          </tr>
        </thead>
        <tbody>
                                 
            <tr <?php if ($rpack_step_no != '2') { echo 'class="hidden"'; }?>>
                <td>
                  <h4 class="text-normal">
                    Date of incident<br><small><?php echo $recall[0]['incident_date']; ?></small></h4>
                </td>
                <td class="text-right"></td>
                <td class="text-center">
                    <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                </td>
          </tr>
          <tr <?php if ($rpack_step_no != '2') { echo 'class="hidden"'; }?>>
            <td>
              <h4 class="text-normal">
                Affected sites/locations<br><small>
				<?php 
				
				 $locs = json_decode($recall[0]['affected_location']);
					$loc_c = 1;
				if(count($locs) > 0){
					 foreach($locs[0] as $loc){
						 
						 if($loc !=''){
							 echo $loc_c.'. '.$loc.'<br />';
							 $loc_c++;
						 }
					 }
				}else{
					echo 'No affected location.';
				}

				 ?>
                
                </small></h4>
            </td>
            <td class="text-right"></td>
            <td class="text-center">
                <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
            </td>
            
          </tr>
          
                        
<?php

	$countqq = 0;
	$curr_sub_c = '';
	foreach(${'questions' . $rpack_step_no} as $q=>$kyu){
?>	


<?php
	$the_approvers = unserialize($kyu['approvers']);
	$the_approvers_status = unserialize($kyu['approvers_status']);
	$approvers_count = 0;
	$approvers_status = 0;
	
	$theapprovers = array();
	
	$crt_assigned = $kyu['assigned'];
	$crt_date_assigned = ' ('.date_format(date_create($kyu['date_assigned']), 'jS F Y, g:ia').')';
	$crt_approve_btn = '';
	
	$crt_assigned_match = 'yes';
	
//	if($crt_assigned == $this->session->userdata('logged_cc_login_id')){
//		$crt_assigned_match = 'yes';
//	}
	
	$crt_view_approvers_btn = '<li class="crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'" style="cursor: pointer;"><a>Task Approvers</a></li>';
	
	if(is_array($the_approvers)){
		$approvers_count = count($the_approvers);
	}
	else{
		$the_approvers = array();
	}
	
	if(is_array($the_approvers_status)){
		$approvers_status = count($the_approvers_status);
	}
	else{
		$the_approvers_status = array();
	}
	
	
	//get approvers list

	$approvers_name = array();
	$unapproved_count = count($the_approvers);
	
	if(count($the_approvers) > 0){
		foreach($the_approvers as $tapp){
			$user_approved = '';
			if(in_array($tapp, $the_approvers_status)){
				$user_approved = ' - Approved';
				$unapproved_count--;
			}
			
			$approvers_name[] = array(
				'name'=>$this->common_model->getname($tapp),
				'status'=>$user_approved
			); 

		}
	}
	
	$theapprovers = $approvers_name;
	
	
	
	
	if ($kyu['status'] == '0'){
		$status_text = 'In Progress';
		$status_class = 'text-danger';
	}
	else if($unapproved_count > 0){ //$approvers_count != $approvers_status){
		$status_text = 'Awaiting Approval';
		$status_class = 'text-warning';
	
		
	
		//array_diff — Computes the difference of arrays
		$result = array_diff($the_approvers, $the_approvers_status);
		
		if(count($result) > 0){
			$new_arr = array();
			foreach($result as $rs){
				$new_arr[] = $rs;
			}

			$crt_assigned = $new_arr[0];
			$crt_date_assigned = '';
			
			if($crt_assigned == $this->session->userdata('logged_cc_login_id')){
				$crt_approve_btn = '<button class="btn btn-success btn-xs crt_approve_btn" data-id="'.$kyu['id'].'"><i class="fa fa-thumbs-o-up"></i> Approve</button>';
			}
			//$crt_view_approvers_btn = '<button class="btn btn-primary btn-xs crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'"><i class="fa fa-eye"></i> View Approvers</button>';
		}
		
	}
	else{
		$status_text = 'Completed';
		$status_class = 'text-success';
	}
		
?>     

			
            
            <?php
			//if step 1 = $rpack_step_no = 2
			if ($rpack_step_no == '2') {?>
            <tr>
                <td>
                  <h4 class="text-normal">
                    <?php echo $kyu['question']; ?><br><small><?php echo $kyu['answer']; ?></small></h4>
                </td>
                <td class="text-right"></td>
                <td class="text-center">
                    <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                </td>
                
          </tr>
                
            
            <?php }  ?>
    
          <?php if ($kyu['category_id'] != 0){

			$tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$kyu['category_id']));
			if ($curr_sub_c != $tsk_cat[0]['id']){
			?>
            
          <tr class="info hidden">
			<td colspan="3"></td>
          </tr>
          
          <tr class="info hidden-xs">
			<td colspan="3">
            	<h4 style="text-transform:uppercase; font-weight:bold;">
                	<?php echo $tsk_cat[0]['category_name']?>
                </h4>
            </td>
          </tr>
            
          <tr class="info visible-xs">
			<td>
            	<h4 style="text-transform:uppercase; font-weight:bold;">
                	<?php echo $tsk_cat[0]['category_name']?>
                </h4>
            </td>
          </tr>
            
		  <?php } } ?>

          <tr <?php if ($rpack_step_no == '2') { echo 'class="hidden"';}?>>

            <td>
            
            

				<h4>
                
			  <?php
			if($kyu['question'] !='Hazard Analysis'){ //$countqq != 0){
			  echo $kyu['question']; } ?>
              
				  <?php if ($kyu['question'] =='Hazard Analysis'){ ?>
                  
                  <small><button class="btn btn-primary btn-sm save_stp3_table <?php echo 'hidden'; // if ($kyu['status'] != '0') {echo 'hidden'; } else if($kyu['status'] != '0'){echo 'disabled'; }?>" style="display: none;">Save</button><span class="stp3_remaining_items text-red" style="padding-left: 5px;"></span></small>
                  
				  <?php } ?>
              
              
              <br><small>
              
              
              
              
              	<?php if ($kyu['question'] != 'Hazard Analysis'){ //$countqq != 0){ ?>


					<?php if ($kyu['answer'] != ''){
						$note_view = '';
					?>

                    <?php } else if($kyu['answer'] =='' && $kyu['status'] == '0'){
						 $note_view = 'style="display: none;"';
					?>
                        <span id="tsk_not_strted_holder">Task not yet started</span>
                    <?php } else {
						 $note_view = 'style="display: none;"';
					?>
                        <span id="tsk_not_strted_holder">No notes added</span>
                    <?php } ?>
                        <a class="btn btn-default btn-xs" href="#" id="show_rc_respond_btn" <?php echo $note_view; ?> data-toggle="modal" data-target="#recallViewRespond" onclick="getRCTaskRespond('<?php echo $kyu['id']; ?>');">View task notes</a><br>
                    
                
                	<?php
						//get task issues
						$issues = $this->master_model->getRecords('cf_recall_blockers', array($recall_field_name=>$recall[0]['id'], 'task_id'=>$kyu['id']));
						
						$issue_btn = 'style="display: none;"';
						$issue_text = 'Issue';
						if(count($issues) > 0 && $kyu['status'] == '0'){
							$issue_btn = '';
						}
						if(count($issues) > 1){
							$issue_text = 'Issues';
						}
						
					?>
              		<span class="span_issue hidden" <?php echo $issue_btn; ?>><a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-default btn-xs view_issue_btn">View <?php echo count($issues).' '.$issue_text ?></a><br /></span>
                
              
                    
                <?php } //check if question 1 
				
				else{
					
					//get value of the table asnwers
					if ($kyu['answer'] != ''){
						$s3_assessment = explode(' ',$kyu['answer']);
					}
					else{
						$s3_assessment = array('','','','','','');
					}
					//show on top assigned in step 3 table
                    if ($crt_assigned != ''){
						   echo  'Assigned to: <span class="label orange" style="border-radius: 15px;">'.$this->common_model->getname($crt_assigned).'</span>'.$crt_date_assigned;
					   }
					   else{
							echo 'Assigned to: <span class="label danger" style="border-radius: 15px;">Not Assigned</span>';
					   }
					   
					if(count($theapprovers) > 0){

						echo '<p class="text-muted" style="margin-top: 10px;">Assigned Approver(s)</p>';
						echo '<ol class="text-muted">';
						foreach($theapprovers as $r=>$thapp){
							echo '<li>'.$thapp['name'].''.$thapp['status'].'</li>';
						}
						
						echo '</ol>';
					}
					
					
					 ?>
                
                  		</small></h4>
                        <div class="visible-md visible-lg">
                            <div id="task_guidance_h<?php echo $kyu['id'] ?>-lg" style="display: none;">
                                <span  class="thumbnail" >
                                <?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide']); ?>
                                </span>
                            </div>
                        </div>
                    
                



                        <div>
                        
                
                
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Severity</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[0] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="hidden" name="step_3_table_id" value="<?php echo $kyu['id']?>"/>
                            	<input type="radio" name="severity" class="input_hidden" value="Minor" <?php if ($s3_assessment[0] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[0] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Moderate" <?php if ($s3_assessment[0] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[0] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Major" <?php if ($s3_assessment[0] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[0] == 'Serious') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Serious" <?php if ($s3_assessment[0] == 'Serious') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Serious</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Presence</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[1] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Rare" <?php if ($s3_assessment[1] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[1] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Some" <?php if ($s3_assessment[1] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[1] == 'Most') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Most" <?php if ($s3_assessment[1] == 'Most') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Most</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[1] == 'All') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="All" <?php if ($s3_assessment[1] == 'All') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">All</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">
                        
                        

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Likely Injury</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[2] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Rare" <?php if ($s3_assessment[2] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[2] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Some" <?php if ($s3_assessment[2] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[2] == 'Many') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Many" <?php if ($s3_assessment[2] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[2] == 'Certain') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Certain" <?php if ($s3_assessment[2] == 'Certain') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Certain</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Distribution</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[3] == 'Small') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Small" <?php if ($s3_assessment[3] == 'Small') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Small</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[3] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Some" <?php if ($s3_assessment[3] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[3] == 'Many') { echo 'selected'; }?>">

                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Many" <?php if ($s3_assessment[3] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[3] == 'National') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="National" <?php if ($s3_assessment[3] == 'National') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">National</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Identification</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[4] == 'Easy') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Easy" <?php if ($s3_assessment[4] == 'Easy') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Easy</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[4] == 'Possible') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Possible" <?php if ($s3_assessment[4] == 'Possible') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Possible</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[4] == 'Difficult') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Difficult" <?php if ($s3_assessment[4] == 'Difficult') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Difficult</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[4] == 'Undetectable') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Undetectable" <?php if ($s3_assessment[4] == 'Undetectable') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Undetectable</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Hazard</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[5] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Minor" <?php if ($s3_assessment[5] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[5] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Moderate" <?php if ($s3_assessment[5] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[5] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Major" <?php if ($s3_assessment[5] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[5] == 'Severe') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Severe" <?php if ($s3_assessment[5] == 'Severe') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Severe</span></p>
                            </label>
                          </div>
                        </div>
                        </div>
                        
                
                
                
                
                
                
                
                
                
                <?php } //end of step 3 table
				
					if ($kyu['question'] != 'Hazard Analysis'){
					
						if ($crt_assigned != ''){
						   echo  'Assigned to: <span class="label orange" style="border-radius: 15px;">'.$this->common_model->getname($crt_assigned).'</span>'.$crt_date_assigned;
					   }
					   else{
							echo 'Assigned to: <span class="label danger" style="border-radius: 15px;">Not Assigned</span>';
					   }
						   
						if(count($theapprovers) > 0){
	
							echo '<p class="text-muted" style="margin-top: 10px;">Assigned Approver(s)</p>';
							echo '<ol class="text-muted">';
							foreach($theapprovers as $r=>$thapp){
								echo '<li>'.$thapp['name'].''.$thapp['status'].'</li>';
							}
							
							echo '</ol>';
						}
						
					   
					   
					}
                    ?>        
              

              </small></h4>
              
                <div class="<?php if($kyu['id'] == 'Hazard Analysis') { echo 'visible-sm visible-xs'; } ?>">
					<div id="task_guidance_h<?php echo $kyu['id'] ?>" style="display: none;">
                    	<span  class="thumbnail" >
                    	<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide']); ?>
                        </span>
                    </div>
                </div>
            
            
            
              <p class="visible-sm visible-md">                    <a href="#" class="btn btn-sm btn-default" id="assign_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php echo 'display: none'; //if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>');">
                        <i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign"></i>
                    </a>
<a href="#" class="btn btn-sm btn-default complete-task-btn <?php echo 'hidden'; //if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id') && $kyu['assigned'] != 0) {echo 'disabled'; }?> <?php if($kyu['id'] == 'Hazard Analysis') { echo ' hidden'; } ?>" id="comptask_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');">
                        <i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i>
                    </a>
                
                    <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>
                
                    <?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                    
                    <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    	Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                    </span>
                    
              </p>

              <p class="visible-xs">
              
              		<?php /*?>
                    <button class="btn btn-default disabled" id="inc_tskm<?php echo $kyu['id']; ?>" style="padding:2px; <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','0','<?php echo ${'step'.$rpack_step_no .'_label'}; ?>','step_<?php echo $rpack_step_no?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>
                    
                    <button class="btn btn-default disabled" id="comp_tskm<?php echo $kyu['id']; ?>" style="padding:2px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','1','<?php echo ${'step'.$rpack_step_no .'_label'}; ?>','step_2btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                       <?php */?> 
                       
                    <button class="btn bt-link disabled <?php echo $status_class; ?> task_text_status" style="background-color: transparent;">
                    	<?php echo $status_text; ?>
                    </button>
                    <br />
                    <br />
                        

                    <a href="#" class="btn btn-sm btn-default" id="assign_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php echo 'display: none'; // if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>');">
                        <i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign"></i>
                    </a>
                    
<a href="#" class="btn btn-sm btn-default complete-task-btn <?php echo 'hidden'; // if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id') && $kyu['assigned'] != 0) {echo 'disabled'; }?> <?php if($kyu['id'] == 'Hazard Analysis') { echo ' hidden'; } ?>" id="comptask_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');">
                        <i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i>
                    </a>
                
                
                    <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>
                
                    <?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                    
                    <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    	Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                    </span>
                    
                    
              </p>
              
              
            </td>

            
            <td class="text-right hidden-xs hidden-sm hidden-md">
            
            		<a href="#" class="btn btn-sm btn-default" id="assign_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php echo 'display: none'; // if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>');">
                        <i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign"></i>
                    </a>
                    
<a href="#" class="btn btn-sm btn-default complete-task-btn <?php echo 'hidden'; // if($kyu['assigned'] != $this->session->userdata('logged_cc_login_id') && $kyu['assigned'] != 0) {echo 'disabled'; }?> <?php if($kyu['id'] == 'Hazard Analysis') { echo ' hidden'; } ?>" id="comptask_btn<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');">
                        <i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i>
                    </a>
                
                    <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>
                
                    <?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
            		
                    
                    <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    	Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                    </span>
            </td>
            
            <td class="text-center hidden-xs">
                <?php /*?>    
                <button class="btn btn-default disabled" id="inc_tsk<?php echo $kyu['id']; ?>" style="padding:3px; <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','0','<?php echo ${'step' . $rpack_step_no . '_label'} ?>','step_<?php echo $rpack_step_no?>btn','<?php echo $totq2; ?>','#proc<?php echo $rpack_step_no?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>
                
                <button class="btn btn-default disabled" id="comp_tsk<?php echo $kyu['id']; ?>" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','1','<?php echo ${'step' . $rpack_step_no . '_label'} ?>','step_<?php echo $rpack_step_no?>btn','<?php echo ${'totq' . $rpack_step_no}; ?>','#proc<?php echo $rpack_step_no?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                <?php */?>
                <p class="small text-right <?php echo $status_class; ?> task_text_status"><?php echo $status_text; ?></p>
            </td>
            

          </tr>
                                   
	   <?php 
		$countqq++;
		$curr_sub_c = $kyu['category_id'];
		}
         ?>
         
         
        </tbody>
      </table>        
          
    <div class="well  <?php if ($rpack_step_no == '2') { echo 'hidden';}?>" style="margin:5px; margin-top: 15px;">
        <h3>Forum <small>Most Recent</small>
        </h3>
        <hr />
        <?php
        
            $cc_id=$this->session->userdata('logged_cc_login_id');
            $org_id=$this->session->userdata('cc_selected_orgnaization');
            $scen_idee = 'r'.$rc['id'];

            $forum_step_posts = $this->master_model->getRecords('forum_post_master',array('cc_id'=>$cc_id,'org_id'=>$org_id,'scenario_id'=>$scen_idee,'fp_category_id'=>'5'),'*',array('date_created'=>'DESC'),'','2');

            if (count($forum_step_posts) > 0){
                
            foreach($forum_step_posts as $sp=>$fsp){		
            ?>

            <p style="padding-bottom: 15px; border-bottom: 1px solid #eee;">
                <b>Posted by <?php echo $this->common_model->getcrtname($fsp['fp_author_id']).'';?> </b><br /><?php echo $fsp['fp_title'] ?> <a href="<?php echo base_url().'forum/post/'.$scen_idee.'/'.$fsp['fp_id']; ?>">Read more..</a></p>

            <?php }  } else{ echo '<p class="text-muted">Nothing found..</p><hr>'; } ?>

            <p><a class="btn btn-primary hidden" href="<?php echo base_url().'forum/topics/'.$scen_idee.'/5'; ?>">Add New Post</a></p>
    </div>                           


                    
                                    
                                    
                                </div><!--./step 2-->                                
   <?php                             
		$rpack_step_no++;
	} //end loop of step

	?>                            
                                
                                </div>
                                

                            </div>

                        </div>

                    
                    <?php
						} //end of no tasks
					?>
    
    
                    </div>
                </div>
                                  
    

        </div>
        <!-- /.panel -->
     </div>
                                    
   </div>                             
                                                
                <?php		
					
					//end recall pack
					//return false;
				}
				
				
				
			}//end loop recall
		}
	}//end of check recall
}//end of check recall2

	else{
		echo '<div class="col-md-12">
            <p class="text-center" style="color: #F3F3F3; margin-top: 60px;"><i class="fa fa-edit" style="font-size: 90px"></i></p>
        	<p class="text-center" style="color: #ccc; margin-top: 20px;">No Completed Incident.</p>
		</div>';
	}

?>


        </div>
		<?php } ?>       









	</div><!--./content tab-->

</div><!--.row-->



</div>

</div>
</div>
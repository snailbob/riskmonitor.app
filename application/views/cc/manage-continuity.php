                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Recall Input Pack</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>

                                </li>

                                <li class="active">Manage <?php echo $packname[0]['name'] ?> Recall Input Pack</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12">
                    
                          <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addRPackStep" style="margin-left: 5px;">Add Step Categories</button>
                          <button type="button" class="btn btn-primary pull-right <?php if (count($packsteps)== 0){ echo 'disabled'; }?>" data-toggle="modal" data-target="#myModal">Add Task</button>
     
     
                          
<!-- Modal -->
<div class="modal fade" id="addRPackStep" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- Text input-->
        <div class="form-group">
          <label class="control-label" for="step_name">Step Category</label>  
          <input id="step_name" name="step_name" type="text" placeholder="eg. Step 1: Notify" class="form-control input-md">
          <span class="help-block task_err text-red" style="display: none;"></span>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveNewRPackCat('<?php echo $pack_id?>');">Save changes</button>
      </div>
    </div>
  </div>
</div>      
               
<!-- Modal updatestep-->
<div class="modal fade" id="updateStep" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- Text input-->
        <div class="form-group">
          <label class="control-label" for="step_name">Step Category</label>  
          <input id="pack_id" name="pack_id" type="hidden" value="<?php echo $pack_id?>">
          <input id="step_id" name="step_id" type="hidden" value="">
          <input id="step_name" name="step_name" type="text" placeholder="eg. Step 1: Notification" class="form-control input-md">
          <span class="help-block task_err text-red" style="display: none;"></span>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveUpdateStep();">Save changes</button>
      </div>
    </div>
  </div>
</div>      
               
               


<!-- Modal -->
<div class="modal fade" id="myAddSubCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Sub-Category</h4>
        <p><br />
        
<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="add_cat">Sub-Category</label>  
  <input id="add_cat" name="add_cat" type="text" placeholder="Sub-Category" class="form-control input-md">
  <span class="help-block text-red" id="category_err"></span>  
</div>


<!-- Select Basic -->
<div class="form-group steps-drop">
  <label class="control-label" for="select_step">Step Category</label>
    <select id="select_step" name="select_step" class="form-control">
      <option value="">Select</option>
      <?php
	  	if(count($packsteps)>0){
			$cat_count = 2;
			foreach($packsteps as $c=>$cat){
				echo '<option value="'.$cat_count.'">'.$cat['name'].'</option>';
				$cat_count++;
			}
		}
	  ?>
    </select>
  <span class="help-block step_err text-red"></span>  
</div>

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="savePackSubCat();">Save changes</button>
      </div>
    </div>
  </div>
</div>
                  


     
                          
<!-- Modal newRecallPack-->
<div class="modal fade" id="newRecallPack" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- Text input-->
        <div class="form-group">
          <label class="control-label" for="pack_name">Recall Input Pack Name</label>  
          <input id="pack_name" name="pack_name" type="text" placeholder="Recall Input Pack" class="form-control input-md">
          <span class="help-block task_err text-red" style="display: none;"></span>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="saveNewPack();">Save changes</button>
      </div>
    </div>
  </div>
</div>      
                          
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Task</h4>
        <p><br />

<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="text_task">Task Name</label>  
  <input id="text_task" name="text_task" type="text" placeholder="Task" class="form-control input-md">
  <span class="help-block task_err text-red" style="display: none;"></span>  
</div>


<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="text_task_guidance">Task Guidance</label>  
  <textarea id="text_task_guidance" name="text_task_guidance" type="text" placeholder="Task Guidance" class="form-control input-md" rows="5"></textarea>
  <span class="help-block task_guidance_err text-red" style="display: none;"></span>  
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="control-label" for="select_pack_cat">Step Category</label>
    <select id="select_pack_cat" name="select_pack_cat" class="form-control">
      <option value="">Select</option>
      <?php
	  	if(count($packsteps)>0){
			$cat_count = 2;
			foreach($packsteps as $c=>$cat){
				echo '<option value="'.$cat_count.'">'.$cat['name'].'</option>';
				$cat_count++;
			}
		}
	  ?>
    </select>
  <span class="help-block category_err text-red" style="display: none;"></span>  
</div>

<!-- Select Basic -->
<div class="form-group" id="sub_cat">
</div>






        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="saveNewPackTask();">Save changes</button>
      </div>
    </div>
  </div>
</div>






                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>






                    <div class="panel panel-default">

                        <div class="panel-heading">

                            <div class="panel-title">

                                <h4><i class="fa fa-play-circle-o fa-fw"></i><?php echo $packname[0]['name'] ?> Steps Category</h4>

                            </div>

                            <div class="panel-widgets">

                                <a data-toggle="collapse" data-parent="#accordion" href="#packs"><i class="fa fa-chevron-down"></i></a>

                            </div>

                            <div class="clearfix"></div>

                        </div>

                        <div id="packs" class="panel-collapse collapse in">

                            <div class="panel-body">



                            <?php 

                            if(count($packsteps)>0)

                            {
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<thead>
                 
                                    <tr>
                                        <th width="100%" style="background-color:#fff;">Step Category</th>
                                        <th style="background-color:#fff;"></th>

                                    </tr>
								</thead>
								<tbody>
                                
                                <?php		
									$i = 0;

									foreach($packsteps as $r => $value) {

								?>
         			
                        
                                    <tr>

                                    	<td><?php echo $value['name']; ?></td>
                                    	<td>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" style="right: 0; left: auto;font-size: 90%; min-width: 82px;">
    <li><a href="javascript:;" id="<?php echo 'del-rc-pack-cat-'.$value['id']?>" onclick="delRPackCat('<?php echo $value['id']?>','<?php echo 'del-rc-pack-cat-'.$value['id']?>');">Delete</a></li>
    <li><a href="#" data-toggle="modal" data-target="#updateStep" onclick="addStepName('<?php echo $value['name']?>','<?php echo $value['id']?>');">Update</a></li>

  </ul>
</div>                                        
                                        
                                        </td>

          
          
                                    </tr>

								<?php 
									$i++;
									}

								?>

                                                    

                                </tbody>

                                

                                

                            </table>

                        </div>

                        <!-- /.table-responsive -->
    
                        <?php
                        }
                        else{
                        
                            echo '<p class="text-center text-muted" style="margin-top: 20px;">No Step Categories.</p>';
                        }
                        ?>









                            </div>

                          </div>

                        </div>

                        <!-- /.panel -->






                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-exchange fa-fw"></i>Manage Sub-Categories</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#subCat"><i class="fa fa-chevron-up tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="left" title="" data-original-title="Expand"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="subCat" class="panel-collapse collapse">

                                <div class="panel-body">

                          <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myAddSubCat" style="margin-bottom: 15px;">Add Sub-Category</button>
                            <?php 

                            if(count($category)>0)

                            {
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<thead>
                 
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Sub-Category</th>
                                        <th width="40%" style="background-color:#fff;" class="text-center">Category</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center"></th>
                                    </tr>
                                
								</thead>
								<tbody>
                                <?php		
									$i = 0;

									foreach($category as $r => $value) {

								?>
         			
                        
                                    <tr>

                                    	<td>
                                        
                                        <?php if($value['id'] =="38"){ ?>
                                            <a href="javascript:;" class="text-muted" style="text-decoration: none; cursor: pointer" title="editing disabled"><?php echo $value['category_name']; ?></a>
                                        <?php } else{ ?>
                                            <a class="text-muted" id="empty_scat_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_stx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['category_name']; ?></a>
                                     
                                           <p id="toggle_stx<?php echo $i; ?>" style="display: none;"><textarea id="my_stext<?php echo $i; ?>" name="my_stext<?php echo $i; ?>" class="form-control" autofocus><?php echo $value['category_name']; ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitCategoryUpdate('toggle_stx<?php echo $i; ?>','empty_scat_label<?php echo $i; ?>','#my_stext<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_stx<?php echo $i; ?>').hide();$('#empty_scat_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                        <?php } ?>
                                            
                                        </td>


                                    	<td class="text-center" style="vertical-align: middle">
                                        	Step <?php echo $value['step_no'] - 1; ?>
                                        </td>
                                    	<td class="text-right" style="vertical-align: middle">
                                        
                                            <button id="del_btn_cat_<?php echo $i; ?>" class="btn btn-danger btn-sm tooltip-test <?php if($value['id'] =="38"){ echo 'disabled'; }?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="confirmDelete('<?php echo $value['id']?>','cf_recall_steps_category','del_btn_cat_<?php echo $i; ?>');"><i class="fa fa-trash-o"></i></button>
                                        </td>


                                    </tr>

								<?php 
									$i++;
									}

								?>

                                                    

                                </tbody>

                                

                                

                            </table>

                        </div>

                        <!-- /.table-responsive -->
    
                        <?php
                        }
                        else{
                        
                            echo '<p class="text-center text-muted" style="margin-top: 20px;">No Sub-Category.</p>';
                        }
                        ?>


                            </div>

                          </div>

                        </div>
                        <!-- /.panel -->
                        
                        
                        
                        
                        
                        
                        


                        <div class="panel panel-default">


                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4><i class="fa fa-play-circle-o fa-fw"></i>Continuity Task</h4>
                                </div>
                                <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">


                            <?php 

                            if($packsteps>0)

                            {
								$step_no = 2;
								$i = 0;
								foreach($packsteps as $ps=>$pcs){
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<thead>
                                
                                
                                    <tr>
                                    	<td colspan="6" style="background-color:#fff;"><h4><?php echo $pcs['name']?> <span id="load_save_step"></span></h4></td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#fff; width: 15px;"><i class="fa fa-arrows" style="visibility: hidden;"></i></th>
                                        <th width="40%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="20%" style="background-color:#fff;" class="text-center">Sub-Category</th>
                                        <th style="background-color:#fff;"></th>
                                        <th style="background-color:#fff;"></th>
                                    </tr>
                                </thead>   
								<tbody id="draggablePanelList<?php if ($step_no != 2){ echo $step_no; }?>">
                                    
                                    <?php
									
									${"step" . $step_no} = $this->master_model->getRecords('cf_recall_guidance',array('step_no'=>$step_no,'recall_pack_id'=>$pack_id),'',array('category'=>'ASC','arrangement'=>'ASC'));
									
									foreach (${"step" . $step_no} as $r=>$value){?>
                                    <tr id="ID_<?php echo $value['id']?>">



                                    	<td class="text-left">
                                        	<span id="arrange_arrows" style="display: none" title="move">
                                                <i class="fa fa-arrows"></i>
                                            </span>
                                        </td>

                                    	<td>
                                        
                                        <?php if($value['id'] =="81"){ ?>
                                            <a href="javascript:;" class="text-muted" style="text-decoration: none; cursor: pointer" title="editing disabled"><?php echo $value['question']; ?></a>
                                        <?php } else{ ?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['question'] ?></a>
                                        <?php }?>
                                        
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['question'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitTaskUpdate('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                           
                                        </td>
                                        

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="task_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tg_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tg_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_tg_text<?php echo $i; ?>" name="my_tg_text<?php echo $i; ?>" class="form-control" rows="7"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tg_tx<?php echo $i; ?>','task_guide_label<?php echo $i; ?>','#my_tg_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tg_tx<?php echo $i; ?>').hide();$('#task_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="task_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tg_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tg_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_tg_text<?php echo $i; ?>" name="my_tg_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tg_tx<?php echo $i; ?>','task_guide_label<?php echo $i; ?>','#my_tg_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tg_tx<?php echo $i; ?>').hide();$('#task_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>
                                        
                                    	<td class="text-center"><?php
											//get category name
											$the_cat = $this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$value['step_no'],'id'=>$value['category'],'recall_pack_id'=>$value['recall_pack_id']));
											$step_cats = $this->master_model->getRecords('cf_recall_steps_category',array('step_no'=>$value['step_no'],'deleted'=>'0','recall_pack_id'=>$value['recall_pack_id'])); //,'recall_pack_id'=>$value['recall_pack_id']
											
											if (count($the_cat) > 0){ ?>
                                                <a class="text-muted" id="category_holder_<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_cat<?php echo $i; ?>').show();" title="click to edit"><?php echo $the_cat[0]['category_name']; ?></a>
												
                                                <!-- Select Basic -->
                                                <div class="form-group" id="toggle_cat<?php echo $i; ?>" style="display: none;">
                                                    <select id="my_sel_cat<?php echo $i; ?>" name="my_sel_cat<?php echo $i; ?>" class="form-control">
                                                      <option value="0">Select</option>
                                                      
                                                      <?php foreach($step_cats as $sc=>$scats){ ?>
                                                      <option value="<?php echo $scats['id'] ?>" <?php if ($scats['id'] == $value['category']) { echo 'selected="selected"'; } ?>><?php echo $scats['category_name'] ?></option>
                                                      <?php } ?>
                                                      
                                                    </select>
                                                   <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="selectTaskCat('toggle_cat<?php echo $i; ?>','category_holder_<?php echo $i; ?>','#my_sel_cat<?php echo $i; ?>','<?php echo $value['id']; ?>');">Submit</button>
                                                   <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_cat<?php echo $i; ?>').hide();$('#category_holder_<?php echo $i; ?>').show();">Cancel</button>
                                                    
                                                </div>
                                                
                                                
                                                												
											<?php
                                            }
											else{ ?>
                                                <a class="text-muted" id="category_holder_<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_cat<?php echo $i; ?>').show();" title="click to edit">No Sub-Category</a>
                                                <!-- Select Basic -->
                                                <div class="form-group" id="toggle_cat<?php echo $i; ?>" style="display: none;">
                                                    <select id="my_sel_cat<?php echo $i; ?>" name="my_sel_cat<?php echo $i; ?>" class="form-control">
                                                      <option value="0">Select</option>
                                                      
                                                      <?php foreach($step_cats as $sc=>$scats){ ?>
                                                      <option value="<?php echo $scats['id'] ?>"><?php echo $scats['category_name'] ?></option>
                                                      <?php } ?>
                                                      
                                                    </select>
                                                   <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="selectTaskCat('toggle_cat<?php echo $i; ?>','category_holder_<?php echo $i; ?>','#my_sel_cat<?php echo $i; ?>','<?php echo $value['id']; ?>');">Submit</button>
                                                   <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_cat<?php echo $i; ?>').hide();$('#category_holder_<?php echo $i; ?>').show();">Cancel</button>
                                                    
                                                </div>
                                           
											<?php
                                            }
										?></td>
                                        
                                        
                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="enabled_btn_<?php echo $i; ?>" class="btn btn-success btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enabled" onclick="toggleDisable('<?php echo $value['id']?>','0','enabled_btn_<?php echo $i; ?>');" <?php if ($value['disabled']== '1') { echo 'style="display: none;"'; } ?>><i class="fa fa-check fa-fw"></i></button>
                                            
                                            <button id="disabled_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Disabled" onclick="toggleDisable('<?php echo $value['id']?>','1','disabled_btn_<?php echo $i; ?>');" <?php if ($value['disabled']== '0') { echo 'style="display: none;"'; } ?>><i class="fa fa-times fa-fw"></i></button>
                                        </td>  
                                    	<td class="text-center" style="vertical-align: middle">
                                            <button id="del_btn_<?php echo $i; ?>" class="btn btn-danger btn-sm tooltip-test <?php if($value['id'] =="81"){ echo 'disabled'; } ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="confirmDelete('<?php echo $value['id']?>','cf_recall_guidance','del_btn_<?php echo $i; ?>');"><i class="fa fa-trash-o fa-fw"></i></button>
                                        </td>


                                    </tr>

									<?php
									$i++;
									} //echo $i; ?>
                                    
                                    
                                </tbody>
                            </table>

                        </div> <!-- /.table-responsive -->
						<?php  /******************step 2 end ***************************/ ?>

        
        
                        <?php 
									$step_no++;
								}//end foreach steps
                        }
                        else{
                        
                            echo '<p class="text-center text-muted" style="margin-top: 20px;">No task available.</p>';
                        }
                        ?>



                                    
                                    
                                    


                            </div>

                        </div>

                        <!-- /.panel -->



                    </div>



                </div><!--.row -->

					




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<style>
.dataTables_filter/*, .dataTables_info */,.dataTables_length{display: none;}
</style>


<!-- Modal -->
<div class="modal fade" id="updateDefaultCurrency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Default Currency</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="default_currency_btn">Save changes</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="viewCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Item Cost Details</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="addCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="upload_sect col-lg-12" style="display:none; position: absolute; top: 220px; padding-left: 5px; z-index: 1500;">
        	<div class="col-xs-6">
            <form id="myForm" action="<?php echo base_url().'cc/costmonitor/upload' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                 <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>    
             </div>
        	<div class="col-xs-6">
                <div id="progress" style="display: none;">
                    <div id="bar"></div>
                    <div id="percent">0%</div >
                </div>
             </div>
		</div>
                                                        
    <form>
            
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Item</h4>

            
            <fieldset>
        	<br />
            <!-- Text input-->
            <div class="form-group">
              <input id="item_name" name="item_name" type="text" placeholder="Item Name" class="form-control input-md">
              <input id="cost_id" name="cost_id" type="hidden" value="">
              <input id="cost_item_id" name="cost_item_id" type="hidden" value="">
             <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
              <span class="help-block text-red item_name_error"></span>  

            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <input id="item_cost" name="item_cost" type="text" placeholder="Item Cost" class="form-control input-md">
              <span class="help-block text-red item_cost_error"></span>  

            </div>
            
            <!-- select input-->
            <div class="form-group">


                <select name="currency_code" id="currency_code" class="form-control">
                  <option value="">Select Currency</option>
                  <?php
				  	if(count($currency_code) > 0){
						foreach($currency_code as $cc=>$curcode){
							echo '<option value="'.$curcode['idCountry'].'"';
							if($my_recall[0]['default_currency_id'] == $curcode['idCountry']){
								echo ' selected="selected"';
							}
							echo '>'.$curcode['countryName'].' ('.$curcode['currencyCode'].')</option>';
						}
					
					
					}
				  
				  ?>
                </select>
               <span class="help-block text-red currency_code_error"></span>  

            </div>

            <div class="form-group">

                <a href="javascript:;" onclick="$('.upload_sect').show(); $(this).after('&nbsp;').hide();" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>

  
            </div>

            <div class="form-group">

                <div class="col-xs-12">

                    <div id="file_up_text" style="display: none;">File Attached: <div id="message"><a href=""></a></div></div>

                </div>

            </div>


 
            
            </fieldset>
            


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="cost_item_btn" name="cost_item_btn" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>


	<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Recall Incident</h4>
        <table class="table table-hover">
        <?php
			if(count($recall)> 0){
				foreach($recall as $r=>$call){ ?>
                
                  <tr>
                  	<td>
                    	<a href="<?php echo base_url().'cc/costmonitor/index/'.$call['id']; ?>"><?php echo $call['incident_no'] ?> : <?php echo $call['incident_name'] ?></a>
                    </td>
                  </tr>
			<?php }
			}else{
				echo '<tr><td class="text-center text-muted">No recall incidents.</td> </tr>';
			}
			
		?>
                </table>					


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewCategorySummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category Summary</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="viewAccumulative" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Accumulative Cost</h4>
      </div>
      <div class="modal-body">
        <div class="well" style="border-radius: 0px">
			<?php if(count($my_recall)> 0){
				echo $recall_accumulate;
			} ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="addCostCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form>
            
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category</h4>

            
            <fieldset>
        	<br />
            <!-- Text input-->
            <div class="form-group">
              <input id="name_cat" name="name_cat" type="text" placeholder="Category Name" class="form-control input-md">
              <input id="cat_id" name="cat_id" type="hidden" value="">
              <input id="recall_id" name="recall_id" type="hidden" value="<?php echo $this->uri->segment(4)?>">
              <span class="help-block text-red name_cat_error"></span>  

            </div>

            
            </fieldset>
            


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="name_cat_btn" name="name_cat_btn" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>





<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Cost Monitor</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Cost Monitor</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                    <div class="col-lg-12">

                        <?php if(count($my_recall)> 0){ ?>
                        <a class="btn btn-primary pull-right hidden-xs" href="#" data-toggle="modal" data-target="#addCostCat" style="width: 150px; margin-left: 5px;" onclick="$('#name_cat').val('');$('#cat_id').val('');"><b>+</b> Cost Category</a> 
                        <a class="btn btn-primary pull-right hidden-xs" href="#" data-toggle="modal" data-target="#updateDefaultCurrency" style="width: 150px; margin-left: 5px;" onclick="addCurrencyDropdown('<?php echo $my_recall[0]['default_currency_id']?>','<?php echo $my_recall[0]['id']?>')">Default Currency</a> 
                        <a class="btn btn-primary pull-right hidden-xs" href="#" data-toggle="modal" data-target="#viewAccumulative" style="width: 150px;">Running Cost</a> 
                        
                        <a class="btn btn-primary pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#addCostCat" onclick="$('#name_cat').val('');$('#cat_id').val('');"><b>+</b> Cost Category</a> 
                        <a class="btn btn-primary pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#updateDefaultCurrency"  onclick="addCurrencyDropdown('<?php echo $my_recall[0]['default_currency_id']?>','<?php echo $my_recall[0]['id']?>')">Default Currency</a> 
                        <a class="btn btn-primary pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#viewAccumulative">Running Cost</a> 
                        
                        
                        
                        <?php } ?>

                    </div>
					<div style="clear:both;"></div>
                    
 					<div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>
                        
                        
<?php 
						
	if (count($recall) > 0){
	?>
    
    
    
                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Cost Category
                                    
										<?php if($this->uri->segment(4) != ''){
											
											echo 'for '.$my_recall[0]['incident_no'].': '.$my_recall[0]['incident_name'];
											
												if ($my_recall[0]['initiation_type'] == 0){
													
													echo ' <span class="label label-default">Mock</span>';
												
												}
												
												else{
												
													echo ' <span class="label label-info">Live</span>';
													
												}
											
											}?>
                                        
                                        
                                    </h4>

                                </div>
                                
                                <div class="panel-widgets">
                                    <span class="text-info">
                                    <?php
                                        $more_inci = count($recall) - 1;
                                        if (count($recall) == 1){
                                            echo '1 current incident ';
                                        }
                                        if (count($recall) > 1){
                                            echo count($recall).' current incidents ';
                                        }
                                        //echo $this->pagination->create_links(); 
                                    ?></span>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#open_incident" style="text-decoration: none;">
                                    <i class="fa fa-chevron-up tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php // echo 'View '.$more_inci.' more incidents.';?>"></i></a>
                
                                </div>
                                
                                <div class="clearfix"></div>

                            </div>


                            <div class="panel-body">

                                <div id="open_incident" class="panel-collapse collapse">
                
                                    <ul class="list-group list-default">
                                        <?php if(count($recall) > 0){
                                            $count_open = 0;
                                            $uri_4 = $this->uri->segment(4);
                                            foreach($recall as $open=>$opeen){
                                                
                                                if ($uri_4 != $opeen['id']) {
                                        ?>
                                        
                                        <a href="<?php echo base_url().'cc/costmonitor/index/'.$opeen['id']?>" class="list-group-item"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
                                        
                                        <?php
                                                }
                                            $count_open++;	
                                            } 
                                        } ?>
                                    </ul>
                                </div>
            

								<?php 

								if(count($cost_category)>0)

								{ ?>

							<div class="">

							<table class="table" id="example-tablexx">

								<tbody>

								<?php
								
									foreach($cost_category as $r => $value)

									{
										
								?>

                                                        

                                    <tr class="bg-info">

                                    	<td width="100%">
                                        <h4 style="text-transform: uppercase; font-weight: bold"><?php echo  $value['name']; ?></h4>
             
                                        
                                        </td>

                                        <td class="pull-right">
                                    
       <!-- Single button -->
        <div class="btn-group" style="margin-top: 5px;">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; left: auto; right: 0;">
          
            <li>
                <a href="#" data-toggle="modal" data-target="#addCostItem" onclick="resetItemFields('<?php echo $value['cost_id']?>','<?php echo $my_recall[0]['default_currency_id']?>');">Add Cost Item</a>
            </li>
          
            <li>
                <a href="javascript:;" onclick="updateCostCat('<?php echo $value['cost_id']?>','<?php echo $value['name']?>')">Edit Category Name</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/costmonitor/delete/'.$value['cost_id'].'/'.$this->uri->segment(4); ?>');">Delete Category</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#viewCategorySummary" onclick="showCostSummary('<?php echo $value['cost_id'] ?>')">View Category Sub-Total</a>
            </li>
          </ul>
        </div>    

										
                                    </td>

                                    </tr>

									<tr>
                                    	<td colspan="2">
                                        
                                        
                           
								<?php
                                
                                $items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$value['cost_id']));
                                ?>
                                        
            

								<?php 

								if(count($items)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover table-bordered table-datatable">

                                <thead>

                                    <tr>

                                        <th width="50%">Item Name</th>

                                        <th width="50%">Item Cost</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($items as $r => $value)

									{
										
								?>

                                                        

                                    <tr>

                                    	<td>
										
										<?php
										
										$files = $this->master_model->getRecords('private_message_file',array('cost_item_id'=>$value['item_id']));
										if(count($files) > 0){
											echo '<i class="fa fa-paperclip text-info fa-fw"></i> ';
										}else{
											echo '<i class="fa fa-hand-o-right text-info fa-fw" style="visibility:hidden"></i> ';
										}
										echo  $value['item_name']; ?>
                                        
                                        </td>

                                    	<td><?php echo  $my_class->currencycode($value['currency']).' '.number_format($value['item_cost'],2); ?></td>

                                        <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; right: 0; left: auto;">
            <li>
                <a href="javascript:;" onclick="updateItemCost('<?php echo $value['item_id']?>','<?php echo $value['item_name']?>','<?php echo $value['item_cost']?>','<?php echo $value['currency']?>')">Edit Cost Item</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/costmonitor/deleteitem/'.$value['item_id'].'/'.$value['cost_id'].'/'.$this->uri->segment(4); ?>');">Delete Cost Item</a>
            </li>
            <li class="hidden">
                <a href="#" data-toggle="modal" data-target="#viewCostItem" onclick="viewCostItemDetails('<?php echo $value['item_id']?>')">View Cost Detail</a>
            </li>
            
		<?php if(count($files) > 0){ ?>
        
            <li>
                <a href="<?php echo base_url().'cc/costmonitor/download/'.$files[0]['file_upload_name']?>" target="_blank"> Download Document</a>
            </li>
            
		<?php } ?>
          </ul>
        </div>    


                                    </td>

                                    </tr>



								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-list-ul" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost items for <?php echo $value['name']?></p>
								<?php }

								?>

                                                                                
                                        
                                        
                                        
                                        
                                        </td>
                                    </tr>

								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-usd" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost category</p>
								<?php }

								?>



                            </div>

                        </div>

                        <!-- /.panel -->




    
	<?php
    } else{
	?>
    
        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-history" style="font-size: 90px"></i></p>
        <p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall</p>
	<?php	
	}
?>
    





                    </div>



                </div><!--.row -->

					




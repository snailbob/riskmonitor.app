                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->


<!-- Modal -->
<div class="modal fade" id="viewCategorySummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category Summary</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="viewCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Item Cost Details</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addCostCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="upload_sect col-lg-12" style="display:none; position: absolute; top: 220px; padding-left: 5px; z-index: 1500;">
        	<div class="col-xs-6">
            <form id="myForm" action="<?php echo base_url().'cc/costmonitor/upload' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                 <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>    
             </div>
        	<div class="col-xs-6">
                <div id="progress" style="display: none;">
                    <div id="bar"></div>
                    <div id="percent">0%</div >
                </div>
             </div>
		</div>
                                                        
    <form>
            
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Item</h4>

            
            <fieldset>
        	<br />
            <!-- Text input-->
            <div class="form-group">
              <input id="item_name" name="item_name" type="text" placeholder="Item Name" class="form-control input-md">
              <input id="cat_id" name="cat_id" type="hidden" value="<?php echo $this->uri->segment(4) ?>">
              <input id="cost_item_id" name="cost_item_id" type="hidden" value="">
             <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
              <span class="help-block text-red item_name_error"></span>  

            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <input id="item_cost" name="item_cost" type="text" placeholder="Item Cost" class="form-control input-md">
              <span class="help-block text-red item_cost_error"></span>  

            </div>
            
            <!-- select input-->
            <div class="form-group">


                <select name="currency_code" id="currency_code" class="form-control">
                  <option value="">Select Currency</option>
                  <option value="AUD" selected="selected">Australian Dollar (AUD)</option>
                  <option value="BRL">Brazilian Real (BRL)</option>
                  <option value="CAD">Canadian Dollar (CAD)</option>
                  <option value="CZK">Czech Koruna (CZK)</option>
                  <option value="DKK">Danish Krone (DKK)</option>
                  <option value="EUR">Euro (EUR)</option>
                  <option value="HKD">Hong Kong Dollar (HKD)</option>
                  <option value="HUF">Hungarian Forint (HUF)</option>
                  <option value="ILS">Israeli New Sheqel (ILS)</option>
                  <option value="JPY">Japanese Yen (JPY)</option>
                  <option value="MYR">Malaysian Ringgit (MYR)</option>
                  <option value="MXN">Mexican Peso (MXN)</option>
                  <option value="NOK">Norwegian Krone (NOK)</option>
                  <option value="NZD">New Zealand Dollar (NZD)</option>
                  <option value="PHP">Philippine Peso (PHP)</option>
                  <option value="PLN">Polish Zloty (PLN)</option>
                  <option value="GBP">Pound Sterling (GBP)</option>
                  <option value="SGD">Singapore Dollar (SGD)</option>
                  <option value="SEK">Swedish Krona (SEK)</option>
                  <option value="CHF">Swiss Franc (CHF)</option>
                  <option value="TWD">Taiwan New Dollar (TWD)</option>
                  <option value="THB">Thai Baht (THB)</option>
                  <option value="TRY">Turkish Lira (TRY)</option>
                  <option value="USD">U.S. Dollar (USD)</option>
                </select>
               <span class="help-block text-red currency_code_error"></span>  

            </div>

            <div class="form-group">

                <a href="javascript:;" onclick="$('.upload_sect').show(); $(this).after('&nbsp;').hide();" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>

  
            </div>

            <div class="form-group">

                <div class="col-xs-12">

                    <div id="file_up_text" style="display: none;">File Attached: <div id="message"><a href=""></a></div></div>

                </div>

            </div>


 
            
            </fieldset>
            


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="cost_item_btn" name="cost_item_btn" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>



<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Cost Monitor</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Cost Monitor</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url().'cc/costmonitor/manage'; ?>"><i class="fa fa-angle-double-left"></i> Back to Cost Category List</a> 
                        <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#addCostCat" onclick="resetItemFields();"><b>+</b> Cost Item</a> 

                    </div>

 					<div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><?php echo $cost_category[0]['name']?> Category Items
                                    
                                    <a href="#" class="pull-right btn btn-default btn-xs" data-toggle="modal" data-target="#viewCategorySummary" onclick="showCostSummary('<?php echo $this->uri->segment(4) ?>')">View Summary</a>
                                    
                                    </h4>
                                    
                                </div>
                                <div class="clearfix"></div>
                                

                            </div>


                            <div class="panel-bodyx">



								<?php 

								if(count($items)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="50%">Item Name</th>

                                        <th width="50%">Item Cost</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($items as $r => $value)

									{
										
								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['item_name']; ?></td>

                                    	<td><?php echo  $value['currency'].' '.$value['item_cost']; ?></td>

                                        <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="javascript:;" onclick="updateItemCost('<?php echo $value['item_id']?>','<?php echo $value['item_name']?>','<?php echo $value['item_cost']?>','<?php echo $value['currency']?>')">Edit</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/costmonitor/deleteitem/'.$value['item_id'].'/'.$value['cost_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#viewCostItem" onclick="viewCostItemDetails('<?php echo $value['item_id']?>')">View</a>
            </li>
          </ul>
        </div>    


                                    </td>

                                    </tr>



								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-list-ul" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost items</p>
								<?php }

								?>







                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




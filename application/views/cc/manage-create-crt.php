<style>
	.popover {
		z-index: 9999;
		position: fixed;
	}
	
	.fc-agendaDay-button, .fc-month-button{
		visibility: hidden;
		display: none;
	}

</style>

<?php
	//select org
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

?>       
       
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->



<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Response Team Members</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Manage Response Team</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                    <!-- <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                         <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/crisisteam/add">Add Response Team Member</a>  

                    </div> -->

                    <div class="container">
                        
                        <h2 class="text-center" style="margin-bottom: 50px;">
                            Add your team members
                        </h2>

                        <div class="row">
                            <div class="col-sm-5 col-sm-offset-1">
                                <p class="text-center">
                                    <img src="<?php echo base_url().'assets/img/createteam.png'?>" alt="">
                                </p>
                                <p>
                                    Successfully containing a crisis requires collaboration between cross-functional teams across areas such as quality assurance, marketing, communications, finance, legal and operations.
                                </p>
                                <h3>
                                    $15 AUD <small>/ month / additional user</small>
                                </h3>
                                <p>
                                    billed $<?php echo 15*12; ?> AUD annually*
                                </p>
                            </div>
                            <div class="col-sm-5">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 style="margin-top: 0">
                                            Create a team to start collaborating!
                                        </h3>

                                        <form id="crt_create_form" no-validate>
                                            <div class="form-group">
                                                <label for="">Invite team members: <i>(at least 1 required)</i></label>
                                                <input type="hidden" name="utype[]">

                                                <div class="input-group">
                                                    <input type="text" name="emails[]" class="form-control" placeholder="email@address.com" data-email="true">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-link btn-lg add-crt-email" type="button">
                                                            <i class="fa fa-plus-circle text-muted" aria-hidden="true"></i>
                                                        </button>
                                                    </span>
                                                </div><!-- /input-group -->
                                                <p class="text-helper">
                                                    Please enter an email address here.
                                                </p>
                                            </div>

                                            <div class="form-group group-email-list">

                                            </div>
                                            <div class="form-group">
                                                <a class="btn btn-link" data-toggle="collapse" href="#collapseMessage">
                                                    Add a Personal Message
                                                </a>
                                            </div>

                                            <div class="collapse" id="collapseMessage">
                                                <div class="form-group">
                                                    <textarea name="message" id="" rows="2" class="form-control" placeholder="Add your message here.."></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <input type="checkbox" required name="understood" value="yes">
                                                    <label for="checkbox1">
                                                        I understand that I'll be billed for each invitation I send for the remaining term of my subscription.
                                                    </label>
                                                </div>
                                            
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-block btn-primary">
                                                    Create Team
                                                </button>
                                            </div>
                                            <p>
                                                <small>
                                                Auto-renew will be enabled and you'll be billed for your team plan on the same day each year until you cancel your subscription. By clicking Create Team, you authorise all future charges for adding users and agree to our Terms of Use and Privacy Policy.
                                                </small>
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div><!--.row -->

					




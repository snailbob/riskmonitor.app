<style>
	.popover {
		z-index: 9999;
		position: fixed;
	}
	
	.fc-agendaDay-button, .fc-month-button{
		visibility: hidden;
		display: none;
	}

</style>

<?php
	//select org
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

?>       
       
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->


<!-- Modal for individual sms log -->
<div class="modal fade" id="mySmsReportLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
        <?php
		$sms_cred =$this->master_model->getRecords('organization_master',array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
?>
        <h4 class="modal-title" id="myModalLabel">Avaiable SMS Credit - $<?php echo $sms_cred[0]['sms_credit']?></h4>
      </div>
      <div class="modal-body">
        <?php
			$sms_log=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0','id'=>$this->uri->segment(4)));
			
			$failed_sms_log=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status !='=>'0','id'=>$this->uri->segment(4)));
			?>
            
            <!-- Nav tabs category -->
            <ul class="nav nav-tabs faq-cat-tabs" style="margin-bottom: 10px;">
                <li class="active"><a href="#faq-cat-11" data-toggle="tab">Sent</a></li>
                <li><a href="#faq-cat-22" data-toggle="tab">Failed</a></li>
            </ul>


<!-- Tab panes -->
            <div class="tab-content faq-cat-content">
                <div class="tab-pane active in fade" id="faq-cat-11">

					<?php
                    if (count($sms_log) > 0){
                    ?>
                        <div class="table-responsive">
                        
                        <table class="table table-hover" id="example-tablexx"> <thead>
             <tr> <th width="40%">Recipient</th> <th width="29%">Time Sent</th> <th width="30%">Price</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($sms_log as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. date('d M Y - H:i:s',strtotime($value['date_sent'])) .'</td>';
                            echo '<td>US$ '. $value['price'] .'</td></tr>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>

                </div>
                
                
                <!--second tab-->
                <div class="tab-pane fade" id="faq-cat-22">

					<?php
                    if (count($failed_sms_log) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="stake-tablexx"> <thead>
             <tr> <th width="50%">Recipient</th> <th width="50%">Status</th> </tr> </thead> <tbody>
                        
                        
                    <?php					
                                            
                        foreach($failed_sms_log as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. $value['status'] .'</td>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->
    
                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>
    


                </div>
            </div>


		
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
<!--./Modal for individual sms log -->


<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Response Team Members</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Manage Response Team</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



<!-- end PAGE TITLE AREA -->







<div class="container">


    <div class="row">
        <?php if($my_info['user_level'] == '0') { ?>
            <div class="col-lg-10 col-lg-offset-1 text-right" style="height:40px;">
                
                <a class="btn btn-default disabled">
                    <?php  echo number_format($total_crt_count).'/'.number_format($plan_max_user); ?> members assigned under your current plan 
                </a>
                <a class="btn btn-primary <?php echo ($remaining_user) ? '' : 'disabled'; ?>" href="<?php echo base_url();?>cc/crisisteam/createteam">
                    <i class="fa fa-plus" aria-hidden="true"></i> Team Member
                </a> 

            </div>
        <?php } ?>

        <div class="col-lg-10 col-lg-offset-1" style="padding-top:10px;">

            <?php 

            if($this->session->flashdata('success')!="")

            {

            ?>

            <div class="alert alert-success alert-dismissable">

            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

            <?php echo $this->session->flashdata('success'); ?></div>

            <?php    

            } 

            if($this->session->flashdata('error')!="" && $this->session->flashdata('success')=="")

            {

            ?>

            <div class="alert alert-danger alert-dismissable">

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            <?php echo $this->session->flashdata('error'); ?></div>

            <?php

            } 

            ?>

            <div class="panelx panel-defaultx">

                <!-- <div class="panel-heading">
                    <div class="panel-title">
                        <h4><i class="fa fa-user fa-fw"></i>Response Team Members</h4>
                    </div>
                    <div class="clearfix"></div>
                </div> -->


                <div class="panel-bodyx">
                
                
                <?php if($total_crt_count > 0){ ?>




                <div class="table-responsive">

                <table class="table table-hover table-survey table-dt-sort" id="example-tablex">

                    <thead>

                        <tr style="">

                            <th width="25%">Name</th>
                            <th width="25%">Position</th>
                            <?php if($my_info['user_level'] == '0') { ?>
                                <th width="25%">User Type</th>
                                <th width="10%"> </th>
                            <?php } ?>

                        </tr>

                    </thead>

                    <tbody>

                <?php 

                if(count($all_crt)>0)

                { ?>


                    <?php
                        $crt_emails = array();
                        foreach($all_crt as $r => $value) {
                            if (true){ //$value['user_level'] == '0' || strpos($value['module'], substr($active_module, 0, 1)) !== false

                            $crt_emails[] = $value['crt_email'];
                    ?>

                                            

                        <tr>

                            <td>
                                <a href="#" class="<?php echo ($value['crt_email']  == '') ? 'hidden' : ''; ?>" data-toggle="modal" data-target="#myModal<?php echo  $value['login_id'];?>">
                                    <?php
                                        $nm = $value['first_name'].' '.$value['last_name'];
                                        echo ($nm != ' ') ? $nm : 'Awaiting registration';
                                    ?>
                                </a>
                                <a href="<?php echo base_url().'cc/crisisteam/update/'.$value['login_id']; ?>" class="<?php echo ($value['crt_email']  != '') ? 'hidden' : ''; ?>">
                                    Reassign
                                </a>
                            </td>
                            <td><?php echo $value['crt_position']; ?></td>


                            <?php if($my_info['user_level'] == '0') { ?>

                            <td><?php echo ($value['user_level'] == '1') ? 'User' : 'Administrator'; ?></td>



                            <td>
                            
                                <!-- Single button -->
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                        <li class=" <?php echo ($value['crt_email']  == '') ? 'hidden' : ''; ?>">
                                            <a href="<?php echo base_url(); ?>cc/crisisteam/update/<?php echo  $value['login_id']; ?>">Edit</a> 

                                        </li>
                                        <li class="<?php echo ($value['crt_email']  == '') ? 'hidden' : ''; ?>">
                                            <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/crisisteam/delete/'.$value['login_id']; ?>');">Remove</a>
                                        </li>
                                        <li class="<?php echo ($value['crt_email']  != '') ? 'hidden' : ''; ?>">
                                            <a class="delete_user" data-id="<?php echo $value['login_id']?>">Delete</a>
                                        </li>

                                        <?php if($value['user_status'] == '0'){?>
                                        <li class=" <?php echo ($value['crt_email']  == '') ? 'hidden' : ''; ?>">
                                            <a href="javascript: resendCRT('<?php echo $value['login_id']?>');">Resend invite</a>
                                        </li>
                                        <?php } ?>
                                        
                                    </ul>
                                </div>    

                            </td>
                            <?php } ?>

                        </tr>



                    <?php 

                        }
                    }//end foreach loop
                        
            

                } //end check if empty

                ?>
            
                <?php		
                        
                //display crt from other org
                if(count($all_notmy_crt) > 0){
                    foreach($all_notmy_crt as $c=>$rt){
                        $usertype = $this->master_model->getRecords('cf_login_master', array('login_id'=>$rt['login_id']));
                        
                        if($usertype[0]['user_level'] == '1'){ //check if a crt
                            $crt_orgs = unserialize($rt['org_list']);
                            
                            if(count($crt_orgs) > 0){
                                //echo $active_module;

                                $count_module_exist = 0;
                                foreach($crt_orgs as $crt_row=>$crt_val){
                                    //echo $crt_val['active_module'];
                                    if(substr($active_module, 0, 1) == $crt_val['active_module'] && $org_id == $crt_val['org_id']){ ?>
                                    
                            <tr>          
                            <td><?php echo  $this->master_model->decryptIt($rt['crt_first_name']); ?></td>
                            <td><?php echo  $this->master_model->decryptIt($rt['crt_last_name']); ?></td>
                            <td class="<?php echo $position_show; ?>"><?php echo $rt['crt_position'];?></td>
                            <td class="<?php echo $access_show; ?>">
                                <?php
                                
                                $access = unserialize($rt['access']);
                                
                                if(!is_array($access)){
                                    $access = array();
                                }
                                
                                $kpi = '<i class="fa fa-times text-danger fa-fw"></i>';
                                $cost = '<i class="fa fa-times text-danger fa-fw"></i>';
                                $issues = '<i class="fa fa-times text-danger fa-fw"></i>';
                                
                                if(in_array('kpi', $access)){
                                    $kpi = '<i class="fa fa-check text-success fa-fw"></i>';
                                }

                                if(in_array('cost', $access)){
                                    $cost = '<i class="fa fa-check text-success fa-fw"></i>';
                                }

                                if(in_array('issues', $access)){
                                    $issues = '<i class="fa fa-check text-success fa-fw"></i>';
                                }
                                
                                echo '<p>'.$kpi.' KPI Dashboard<br/>';
                                echo $cost.' Cost Monitor<br/>';
                                echo $issues.' Issues Board</p>';

                                ?>
                            
                            </td>
                            
                            

                            <?php 
                            $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting registration</span>';
                            if($usertype[0]['user_status'] == 1){

                                $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Accepted</span>';
                            }
                            ?>

                            <td><?php echo  $status ?></td>
                            <td>
                <!-- Single button -->
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                            <!-- <li>
                                <a href="#" data-toggle="modal" data-target="#myModal<?php echo  $rt['login_id'];?>">View Details</a>
                            </li> -->
                            <li class="hidden">
                                <a href="#">Remove</a>
                            </li>
                            
                            <?php
                            if (strpos($the_active_module[0]['active_module'], 'b') !== false){
                            ?>
                            
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="view_calendar_btn" data-id="<?php echo $rt['login_id']?>" data-name="<?php echo $this->master_model->decryptIt($rt['crt_first_name']).' '.$this->master_model->decryptIt($rt['crt_last_name']); ?>">Availability</a>
                            </li>
                            <?php } ?>
                            
                            
                        </ul>
                    </div>    
                            
                            
                            
                            
                            </td>
                            
                            
                            
                            
                            
                            </tr>         
                                    
                                    <?php
                                        
                                        
                                        $count_module_exist++;
                                    }
                                }//foreach crt_orgs
                                                
                                
                                
                            }//end check if have the org
                        }//end check if crt
                        
                        
                        
                        

                    }
                }//end count all_notmy_crt
                
                                    
                    
                    ?>
                    
                    
                                        

                                    </tbody>

                                    

                                    

                                </table>

                            </div>

                            <!-- /.table-responsive -->


                <?php
                } //end not empty crts
                else {?>
                    
                <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-user" style="font-size: 90px"></i></p>
                <p class="text-center" style="color: #ccc; margin-top: 20px;">No response team members</p>

                <?php }?>	


                </div>

            </div>

            <!-- /.panel -->

        </div>



    </div><!--.row -->

    
</div>




<?php
	$crt_emails = array();
	if(count($all_crt)>0){
	foreach($all_crt as $r => $value) {
		if (true){ //$value['user_level'] == '0' || strpos($value['module'], substr($active_module, 0, 1)) !== false){

		$crt_emails[] = $value['crt_email'];
?>


<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CRT Member Details</h4>
      </div> -->
      <div class="modal-body" style="background-color: #fff !important">

            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td class="text-right" width="30%">Name:</td>
                        <td><span class="text-muted"><?php echo  $value['first_name']; ?> <?php echo  $value['last_name']; ?></span></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="30%">Position:</td>
                        <td><span class="text-muted"><?php echo $value['crt_position']; ?></span></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="30%">Location:</td>
                        <td><span class="text-muted"><?php echo $value['location']; ?></span></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="30%">Email Address:</td>
                        <td><span class="text-muted"><?php echo $value['crt_email']; ?></span></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="30%">Mobile Number:</td>
                        <td><span class="text-muted"><?php echo '+'.$value['countrycode'].' '.$value['crt_digits']; ?></span></td>
                    </tr>


                </tbody>
            </table>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<?php } } } //org crts

if(count($all_notmy_crt) > 0){
	foreach($all_notmy_crt as $c=>$rt){
		$usertype = $this->master_model->getRecords('cf_login_master', array('login_id'=>$rt['login_id']));
		
		if($usertype[0]['user_level'] == '1'){ //check if a crt
			$crt_orgs = unserialize($rt['org_list']);
			
			if(count($crt_orgs) > 0){
				//echo $active_module;

				$count_module_exist = 0;
				foreach($crt_orgs as $crt_row=>$crt_val){
					//echo $crt_val['active_module'];
					if(substr($active_module, 0, 1) == $crt_val['active_module'] && $org_id == $crt_val['org_id']){ ?>




<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $rt['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CRT Member Details</h4>
      </div> -->
      <div class="modal-body" style="background-color: #fff !important">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($rt['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($rt['crt_last_name']); ?></span></p>
			<p>Position: <span class="text-muted"><?php echo  $rt['crt_position']; ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($rt['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($rt['crt_mobile']); ?></span></p>
			<p>Crisis Function: <span class="text-muted"><?php echo  $rt['crisis_function']; ?></span></p>
			<p>Alternate Member: <span class="text-muted"><?php echo  $rt['alt_member']; ?></span></p>
			<p>Organization Origin: <span class="text-muted"><?php echo  $rt['crt_organisation']; ?></span></p>
            
            <?php
				if($this->common_model->stkstatus($rt['login_id']) == 1){
					echo '<p>Received SMS: <span class="text-muted">';
					
					if ($value['received_sms'] == 0){
						echo 'No';
					}
					else{
						echo 'Yes';
					}
					echo '</p>';
				}
			?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<?php
							
							
						$count_module_exist++;
					}
				}//foreach crt_orgs
								
				
				
			}//end check if have the org
		}//end check if crt
		
		
		
		

	}
}//end count all_notmy_crt
?>



<!-- Modal -->
<div class="modal fade" id="calendarViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
    
    
            <div class="alert alert-info no-sched-alert fade in hidden">
                <button type="button" class="close hidden" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <i class="fa fa-info-circle"></i> <span class="calendar_crt_name"></span> has not yet added a schedule.
            </div>
    
            <div class="the_cal">
    
                <div id="script-warning">
                    <code>php/get-events.php</code> must be running.
                </div>
            
                <div class="" id="loading">loading...</div>
            
                <div id="cc_calendar"></div>
            
            </div>
    

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

    <h1>Manage Crisis Documents</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Manage Crisis Documents</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

                <div class="row">


                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>
                        
                        
                    <div>
                        <a class="hidden-xs btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        
                        <a class="hidden-xs btn btn-primary pull-right" style="margin-left: 5px;" href="<?php echo base_url();?>cc/document/upload">Upload Document</a> 
                        <a class="hidden-xs btn btn-primary pull-right add_doc_cat_btn" href="<?php echo base_url();?>cc/document/upload">+ Document Category</a> 
                        
                        
                        <a class="visible-xs btn btn-default btn-block" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        
                        <a class="visible-xs btn btn-primary btn-block" href="<?php echo base_url();?>cc/document/upload">Upload Document</a> 
                        <a href="#" class="visible-xs btn btn-primary btn-block add_doc_cat_btn">+ Document Category</a> 
               		</div>
                    
                    <div class="clearfix" style="margin-bottom: 15px;"></div>




                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">
                                    <h4><i class="fa fa-file-o"></i> Uncategorized Documents</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                             <?php
								if(! is_null($error)) {
									echo $error;
									if(! is_null($success)) 
									echo $success;
								}
							 ?>	

                            <div class="panel-bodyx">

								<?php 

								if(count($all_doc)>0){
									
								?>

							<div class="table-responsive">

							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="33%">Document Name</th>

                                        <th width="33%">Shared to</th>

                                        <th width="33%">Category</th>

                                        <th width=""></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($all_doc as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td>
										
										

										<?php
										
										 if ($value['current_file_name'] != ''){
										 
											$fileext = explode ('.',$value['current_file_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
											echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
											echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
											echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											 echo substr($value['current_file_name'],13,50);
											 if ($value['rights_to_group'] == $this->session->userdata('group_id') || $value['rights_to_indi'] == $this->session->userdata('logged_crt_login_id')){
												 
											$updated_doc = $this->master_model->getRecords('cf_file_history',array('cf_file_id'=>$value['file_upload_id']));
											
												 echo '<a href="#"  data-toggle="modal" data-target="#myModal'.$value['file_upload_id'].'" style="text-decoration: none;"> <i class="fa fa-info-circle tooltip-test text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="This file was edited '.count($updated_doc).' time(s)"></i></a>';
										 ?>
                                         
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['file_upload_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">File Update History</h4>
      </div>
      <div class="modal-body">
			....
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

                                         <?php
											 } 										 
										 
										 }
										 else{
	
											$fileext = explode ('.',$value['file_upload_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
												echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
												echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
												echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											echo substr($value['file_upload_name'],13,50);

										 }
										 ?>
                                         										
                                         </td>

                                    	<td>
											<?php
											
												echo $this->common_model->getgroup_name($value['group_id']);
                                            ?>
                                        
                                        </td>
                                        
                                    	<td>
                                        	
                                        	<?php 
												echo $this->common_model->getdoc_cat($value['category_id']);
											?>
                                        </td>
                                    	<td>

                                            
       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
            <a href="<?php echo base_url();?>cc/document/update/<?php echo $value['file_upload_id'];?>">Edit</a>
            </li>
            <li>
            <a href="#" class="move_doc_btn" data-cat="" data-id="<?php echo $value['file_upload_id'];?>"> Move</a>
            </li>
            <li>
            <a href="javascript: void(0);" title="Delete" onclick="return del_confirm('<?php echo base_url();?>cc/document/delete/<?php echo $value['file_upload_id'];?>/<?php echo $value['file_upload_name'];?>');">Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cc/document/download/<?php echo $value['file_upload_name'];?>">Download</a>
            </li>
          </ul>
        </div>    
                                            

                                    	</td>

                                    </tr>

								<?php 

									}
									
								?>


                                                </tbody>

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-file-o" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No crisis documents</p>
								<?php }

								?>






                            </div>

                        </div>

                        <!-- /.panel -->



                    
                        <?php
							if(count($categories) > 0){
								foreach($categories as $r=>$category){
									
									$org_id = $this->session->userdata['cc_selected_orgnaization'];
									
									$arr2 = array(
										'org_id'=>$org_id,
										'category_id'=>$category['id']
									);
									
									$all_doc = $this->master_model->getRecords('cf_file_upload', $arr2);
						 ?>
                                
                                    
                                
                                
                                
                                
                                
                                
                                
                                


                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">
                                    <h4><i class="fa fa-file-o"></i> <?php echo $category['name']; ?> <a href="#" class="pull-right update_doccat_btn" data-id="<?php echo $category['id']; ?>" data-name="<?php echo $category['name']; ?>" data-toggle="tooltip" title="Edit Category"><i class="fa fa-edit"></i></a></h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                             <?php
								if(! is_null($error)) {
									echo $error;
									if(! is_null($success)) 
									echo $success;
								}
							 ?>	

                            <div class="panel-bodyx">

								<?php 

								if(count($all_doc)>0){
									
								?>

							<div class="table-responsive">

							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="33%">Document Name</th>

                                        <th width="33%">Shared to</th>

                                        <th width="33%">Category</th>

                                        <th width=""></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($all_doc as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td>
										
										

										<?php
										
										 if ($value['current_file_name'] != ''){
										 
											$fileext = explode ('.',$value['current_file_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
											echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
											echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
											echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											 echo substr($value['current_file_name'],13,50);
											 if ($value['rights_to_group'] == $this->session->userdata('group_id') || $value['rights_to_indi'] == $this->session->userdata('logged_crt_login_id')){
												 
											$updated_doc = $this->master_model->getRecords('cf_file_history',array('cf_file_id'=>$value['file_upload_id']));
											
												 echo '<a href="#"  data-toggle="modal" data-target="#myModal'.$value['file_upload_id'].'" style="text-decoration: none;"> <i class="fa fa-info-circle tooltip-test text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="This file was edited '.count($updated_doc).' time(s)"></i></a>';
										 ?>
                                         
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['file_upload_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">File Update History</h4>
      </div>
      <div class="modal-body">
			....
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

                                         <?php
											 } 										 
										 
										 }
										 else{
	
											$fileext = explode ('.',$value['file_upload_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
												echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
												echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
												echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											echo substr($value['file_upload_name'],13,50);

										 }
										 ?>
                                         										
                                         </td>

                                    	<td>
											<?php
											
												echo $this->common_model->getgroup_name($value['group_id']);
                                            ?>
                                        
                                        </td>
                                        
                                    	<td>
                                        	
                                        	<?php 
												echo $this->common_model->getdoc_cat($value['category_id']);
											?>
                                        </td>
                                    	<td>

                                            
       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
            <a href="<?php echo base_url();?>cc/document/update/<?php echo $value['file_upload_id'];?>"> Edit</a>
            </li>
            <li>
            <a href="#" class="move_doc_btn" data-cat="<?php echo $category['id']; ?>" data-id="<?php echo $value['file_upload_id'];?>"> Move</a>
            </li>
            <li>
            <a href="javascript: void(0);" title="Delete" onclick="return del_confirm('<?php echo base_url();?>cc/document/delete/<?php echo $value['file_upload_id'];?>/<?php echo $value['file_upload_name'];?>');"> Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cc/document/download/<?php echo $value['file_upload_name'];?>">Download</a>
            </li>
          </ul>
        </div>    
                                            

                                    	</td>

                                    </tr>

								<?php 

									}
									
								?>


                                                </tbody>

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-file-o" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No crisis documents</p>
								<?php }

								?>






                            </div>

                        </div>

                        <!-- /.panel -->

                                
                                
                                
                                
                                
                                    
                                    
                                    
						<?php
								}
							}
							else{
								echo '<p class="text-center text-muted thumbnail" style="margin-top: 50px; padding: 30px;">No document category</p>';
							}
						?>





                    </div>
                </div><!--.row -->

					

<!-- Modal -->
<div class="modal fade" id="docCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Document Category</h4>
      </div>
      <div class="modal-body">
        	<form id="doc_cat_form">
            
            	<div class="form-group">
                	<label>Name</label>
                    <input type="text" class="form-control" name="name" />
                    <input type="hidden" name="id" />
                </div>
            
            	<div class="form-group">
                	<label>Assign Group Access Privileges</label>
                    
                      <div class="checkbox">
                        <input type="checkbox" name="group[]" id="group_all" value="all">
                        <label for="group_all">
                            All
                        </label>
                      </div>

                    <?php
						if(count($user_group) > 0){
							foreach($user_group as $r=>$group){ ?>
                            
                    
                      <div class="checkbox">
                        <input type="checkbox" name="group[]" id="group<?php echo $group['id']?>" value="<?php echo $group['id']?>">
                        <label for="group<?php echo $group['id']?>">
                            <?php echo $group['group_name']; ?>
                        </label>
                      </div>
                            
                            <?php
								
							}
						}
					?>
                </div>
            
            	<div class="form-group">
                	<button class="btn btn-primary">Submit</button>
                </div>
            </form>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="moveDocModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Move Document</h4>
      </div>
      <div class="modal-body">
        	<form id="move_doc_form">
				<div class="form-group">
                	<label>Categories</label>
                    <input type="hidden" name="doc_id" />
                    <select name="categories" class="form-control">
                    	<option value="">Uncategorized</option>
                        <?php

						if(count($categories) > 0){
							foreach($categories as $r=>$category){
								echo '<option value="'.$category['id'].'">'.$category['name'].'</option>' ; 
							}
						}
								

						
						?>
                        
                        
                    </select>
                
                </div>
				<div class="form-group">
                	<button class="btn btn-primary">Submit</button>
                </div>
            </form>
      </div>
    </div>
  </div>
</div>


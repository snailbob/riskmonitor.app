                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Manage User Groups</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Manage User Groups</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                    <div class="col-lg-12" style="height:40px;">

                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/usergroup/add">Add User Group</a> 

                    </div>

 					<div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-male fa-fw"></i>User Groups

</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                             <?php if(! is_null($error)) 

										echo $error;

										if(! is_null($success)) 

										$success;

								?>	

                                

                                 <?php if($success!="") { ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $success;   ?>

                            </div>

                            <?php } if($error!="") { ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $error; ?></div>

                            <?php } ?>

                            <div class="panel-bodyx">



								<?php 

								if(count($all_group)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th>Group Name</th>

										<?php
										
										$active_mod = substr($this->session->userdata('org_module'), 0, 1);
										$col_name = 'Access';
										$position_show = 'hidden';
										$access_show = '';
										if($active_mod != '5' && $active_mod != '8'){
											$col_name = 'Position';
											$position_show = '';
											$access_show = 'hidden';
										}
										
										?>

                                        <th class="<?php echo $access_show; ?>">Access</th>

                                        <th> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($all_group as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['group_name']; ?></td>

                                    	<td class="<?php echo $access_show; ?>">
											<?php
											
											$access = unserialize($value['access']);
											
											if(!is_array($access)){
												$access = array();
											}
											
											$kpi = '<i class="fa fa-times text-danger fa-fw"></i>';
											$cost = '<i class="fa fa-times text-danger fa-fw"></i>';
											$issues = '<i class="fa fa-times text-danger fa-fw"></i>';
											
											if(in_array('kpi', $access)){
												$kpi = '<i class="fa fa-check text-success fa-fw"></i>';
											}

											if(in_array('cost', $access)){
												$cost = '<i class="fa fa-check text-success fa-fw"></i>';
											}

											if(in_array('issues', $access)){
												$issues = '<i class="fa fa-check text-success fa-fw"></i>';
											}
											
											echo '<p>'.$kpi.' KPI Dashboard<br/>';
											echo $cost.' Cost Monitor<br/>';
											echo $issues.' Issues Board</p>';

											?>
                                        
                                        </td>

                                    	<td>
       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>cc/usergroup/update/<?php echo  $value['id']; ?>">Edit</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/usergroup/delete/'.$value['id']; ?>');">Delete</a>
            </li>
            <li>
                <a href="javascript:;<?php /*echo base_url(); ?>cc/crisisteam/details/<?php echo  $value['login_id'];*/ ?>"  data-toggle="modal" data-target="#myModal<?php echo  $value['id'];?>">View</a>
            
            </li>
          </ul>
        </div>    


                                    </td>

                                </tr>

<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User Groups Members</h4>
      </div>
      <div class="modal-body">
        <?php 
			 $crts = $this->master_model->getRecords('cf_crisis_response_team',array('group_id'=>$value['id']));
				
			 if (count($crts) > '0'){
				 echo '<ol>';
				 foreach ( $crts as $crt=>$thecrt){
				 
					 echo '<li>'.$this->master_model->decryptIt($thecrt['crt_first_name']).' '.$this->master_model->decryptIt($thecrt['crt_last_name']).'</li>';
				 }
				 echo '</ol>';

			 }
			 else{
				 echo '<p class="text-muted text-center">No members.</p>';
			 }
				 
		  ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


								<?php 

									}
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-male" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No user group</p>
								<?php }

								?>







                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




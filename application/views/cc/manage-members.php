                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Manage Members</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Manage Members</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/members/add">Add Member</a> 

                    </div>

 					<div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Members

</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                             <?php if(! is_null($error)) 

										echo $error;

										if(! is_null($success)) 

										$success;

								?>	

                                

                                 <?php if($success!="") { ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $success;   ?>

                            </div>

                            <?php } if($error!="") { ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $error; ?></div>

                            <?php } ?>

                            <div class="panel-bodyx">



								<?php 

								if(count($all_mem)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="35%">First Name</th>

                                        <th width="35%">Last Name</th>

                                        <th width="30%">Status</th>

                                        <th width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($all_mem as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $this->master_model->decryptIt($value['first_name']); ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($value['last_name']); ?></td>


                                    	<?php 

                                    if($value['user_status'] == 1 ){

                                    $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Accepted</span>';

                                    } else {	

                                    $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting Acceptance</span>';

                                    }

                                    ?>

                                    <td><?php echo  $status ?></td>

                                    <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>cc/members/update/<?php echo  $value['login_id']; ?>">Edit</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/members/delete/'.$value['login_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href="#"  data-toggle="modal" data-target="#myModal<?php echo  $value['login_id'];?>">View</a>
            
            </li>
          </ul>
        </div>    


                                    </td>

                                    </tr>

<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Member Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['first_name']); ?> <?php echo  $this->master_model->decryptIt($value['last_name']); ?></span></p>

			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['email_id']); ?></span></p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


								<?php 

									}
									
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-user" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No members.</p>
								<?php }

								?>







                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




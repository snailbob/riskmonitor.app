                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

        <h1>Response Flow</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Response Flow</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/scenario/managescene"><i class="fa fa-angle-double-left"></i> Scenarios</a> 
                        
                        <?php if ($scenario_info[0]['scenario_status'] != 0) { ?>
                            <a class="btn btn-primary pull-right" href="<?php echo base_url().'forum/index/'.$scenario_info[0]['scenario_id'];?>">Open Forum</a> 
                        <?php } ?>
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        
                        




                    </div>
                    
                    <div class="col-lg-12">
                        <div class="col-lg-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                              <div class="list-group">

                                <a href="" class="list-group-item active text-center scene_bg">
                                 
                                 	<h4 class="fa fa-lightbulb-o fa-fw fa-3x"></h4><br/><?php echo substr($scenario_info[0]['scenario'],0,15); ?>
                                </a>
                                <?php
									if (count($task_list) > 0 ){
										foreach ($task_list as $t=>$tsk){
								?>
                                <a href="" class="list-group-item text-center">
                                  <h4 class="fa fa-tasks fa-fw"></h4><br/><?php echo $tsk['task'] ?>
                                </a>
                                <?php
										}
									}
									else{
								?>
                                <a href="" class="list-group-item text-center">
                                  <h4 class="fa fa-tasks fa-fw"></h4><br/>No Tasks
                                </a>
                                <?php
									}
								?>
                         
                              </div>
                            </div>
                            
                            
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                                <!-- flight section -->
                                <div class="bhoechie-tab-content active">
                                    <center>
                                      <h3 style="margin-top: 50px;color:#55518a"><?php echo $scenario_info[0]['scenario']; ?><br />
                                      	<span class="small"><?php if ($scenario_info[0]['scenario_status'] == 0) { echo 'Pre-Incident'; } else { echo 'Response Initiated'; } ?></span>
                                      </h3>
                                    </center>
                                </div>
                                
                                
                                <?php
									if (count($task_list) > 0 ){
										foreach ($task_list as $t=>$tsk){
								?>
                                <div class="bhoechie-tab-content">
                                    <center>
                                      <h4 style="margin-top: 5px;color:#55518a"><span class="small">Task Description</span><br />
                                      	<?php echo $tsk['task']; ?>
                                      </h4>
                                     
                                      <h4 style="margin-top: 5px;color:#55518a"><span class="small">Task Guide</span><br />
                                      	<?php echo $tsk['task_guide']; ?>
                                      </h4>
                                     
                                      <h4 style="margin-top: 5px;color:#55518a"><span class="small">Task Owner</span><br />
                                      	
										
										<?php
										if ($tsk['crt_id'] == 0){
											
											echo 'Not Assigned';
										}
										else{
												
											$crt_info = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$tsk['crt_id']));
											echo $this->master_model->decryptIt($crt_info[0]['crt_first_name']).' '.$this->master_model->decryptIt($crt_info[0]['crt_last_name']);
										}
										
										?>
                                        
                                      </h4>
                                     
                                    </center>
                                </div>
                                <?php
										}
									}
									else{
								?>


                                <!-- hotel search -->
                                <div class="bhoechie-tab-content">
                                    <center>
                                      <a href="<?php echo base_url().'cc/scenario/addtask/'.$this->uri->segment(4); if ($scenario_info[0]['scenario_status'] != 0) { echo '/res'; } ?> " style="margin-top: 50px;" class="btn btn-default btn-lg">Add Task</a>
                                    </center>
                                </div>
                                
                                <?php
									}
								?>


                            </div>
                        </div>
                    </div>
    

                </div><!--.row -->




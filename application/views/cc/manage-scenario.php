                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

        <h1>Manage Scenario</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Response Scenario</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/scenario/add">Add Scenario</a> 
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        
                        




                    </div>

                        
                        <?php if (count($scenarios) > 0){
								$colours = array ('orange','red','maroon','green','green2','grey','purple','blue');
								foreach($scenarios as $sc=>$scene){
						 ?>
                        
                               
                            <div class="col-lg-<?php if (count($scenarios) == 1) { echo '6 col-lg-offset-3'; } else if (count($scenarios) == 2){
							echo '6'; } else { echo '4'; }?> col-sm-6" id="assigned_task">
                                <div class="circle-tile <?php echo $colours[array_rand($colours)] ?>">
                        
                                    <div class="circle-tile-number text-faded">
                                       <?php
											$whr_arr=array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'scenario_id'=>$scene['scenario_id']); 

											$task_list=$this->master_model->getRecords('cf_task as task',$whr_arr);

									   ?>
                                        <span class="lead"><?php echo count($task_list); if (count($task_list) > 1) { echo ' Tasks'; } else { echo ' Task'; }?> </span>
                                    </div>
                                    <a href="<?php echo base_url().'cc/scenario/responseflow/'.$scene['scenario_id'] ?>" class="circle-tile-footer" title="click to view response flow"> <?php echo substr($scene['scenario'],0,30); ?> <i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                            
                        <?php 
								}
							}
						?>
                        </div>   


                </div><!--.row -->




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Stakeholders</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Manage Stakeholders</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">
                    <div class="col-lg-12">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        <a class="btn btn-primary pull-right hidden" style="margin-left: 5px;" href="#importstake-modal" data-toggle="modal">Add Existing Stakeholder</a> 
                        <a class="btn btn-primary pull-right" style="margin-left: 5px;" href="<?php echo base_url();?>cc/stakeholder/add">Add New Stakeholder</a> 
                        
                        

<!-- Modal -->
<div class="modal fade" id="importstake-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Existing Stakeholder</h4>
      </div>
      <div class="modal-body">
        
        <form id="importstk-form" method="post" action="<?php echo base_url().'cc/stakeholder/import_stk'?>">

          <div class="form-group">
          
          	<?php
			
			
			foreach($allstk as $r=>$stk){
				
				if(!in_array($stk['login_id'], $stks)){ ?>
				
                    
            <div class="checkboxx">
              <label>
                <input type="checkbox" name="stkcheck[]" value="<?php echo $stk['login_id']; ?>">
                <?php echo $this->common_model->getstkname($stk['login_id']);?>
              </label>
            </div>
                   
                    
			<?php
                }
            }
			?>
          <button type="submit" class="btn btn-primary disabled">Import</button>
          </div>
        </form>        
        
      </div>
    </div>
  </div>
</div>                        
                        
                        
                        
                        
                        
               		</div>
                    
                    <div class="clearfix"></div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-users fa-fw"></i> Stakeholders</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                             <?php if(! is_null($error)) 

										echo $error;

										if(! is_null($success)) 

										echo $success;

								?>	

                            <div class="panel-bodyx">

								<?php 

								if(count($stks)>0)

								{
								?>

							<div class="table-responsive">
                            
							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="25%">First Name</th>

                                        <th width="25%">Last Name</th>

                                        <th width="25%">Organization</th>

                                        <th width="25%">Status</th>

                                        <th width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($stks as $value) {
										$thestk = $this->master_model->getRecords('cf_stakeholder', array('login_id'=>$value));
										if(count($thestk) > 0){
											
								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $this->master_model->decryptIt($thestk[0]['stk_first_name']); ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($thestk[0]['stk_last_name']); ?></td>

                                    	<td><?php echo  $thestk[0]['stk_position']; ?></td>

                                    	<?php 

                                    if($this->common_model->stkstatus($value) == 1){

                                    $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Accepted</span>';

                                    } else {	

                                    $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting Acceptance</span>';

                                    }

                                    ?>

                                    <td><?php echo  $status ?></td>

                                    <td>
                                    
       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="" data-toggle="modal" data-target="#myModal<?php echo  $thestk[0]['login_id'];?>">View</a>
            
            </li>
			
			<?php if($thestk[0]['cc_id'] == $this->session->userdata('logged_cc_login_id')){ ?>
            <li>
                <a href="<?php echo base_url(); ?>cc/stakeholder/update/<?php echo  $thestk[0]['login_id']; ?>">Edit</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/stakeholder/remove/'.$thestk[0]['login_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/stakeholder/delete/'.$thestk[0]['login_id']; ?>');">Delete Permanently</a>
            </li>
            <?php } else{ ?>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/stakeholder/remove/'.$thestk[0]['login_id']; ?>');">Delete</a>
            </li>
            <?php }?>
          </ul>
        </div>    
                                    
                                    

                                    </td>

                                    </tr>
                                    

								<?php 
										}//end each stks info
									}//end foreach stks
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-users" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No stakeholders</p>
								<?php }

								?>









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					


<?php 
	foreach($stks as $value) {
		$thestk = $this->master_model->getRecords('cf_stakeholder', array('login_id'=>$value));
		
		if(count($thestk) > 0){
?>
                            
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $thestk[0]['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Stakeholder Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($thestk[0]['stk_first_name']); ?> <?php echo $this->master_model->decryptIt($thestk[0]['stk_last_name']); ?></span></p>
			<p>Organization: <span class="text-muted"><?php echo  $thestk[0]['stk_position']; ?></span></p><!--changed position to org...-->
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($thestk[0]['stk_email_address']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($thestk[0]['stk_mobile_num']); ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<?php } } ?>

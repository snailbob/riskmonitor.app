                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

        <h1>Manage Response Plan</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Response Action Plan</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/scenario/add">Add Scenario</a> 
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-lightbulb-o"></i> Response Action Plan </h4>

                                </div>

                                <div class="panel-widgets">
                                        <a href="<?php echo base_url().'cc/scenario/managescene'; ?>" class="btn btn-default btn-xs">
                                        	View Response Flow
                                        </a>
                                </div>

                            <div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-bodyx">

								<?php 

								if(count($task_list)>0)

								{ ?>

							<div class="table-responsive">
							<table class="table table-hover" id="example-tablex">

                                <thead>

                                    <tr>

                                        <th width="37%">Scenario</th>

                                        <th width="37%">Task</th>

                                        <th width="13%">Owner</th>

                                        <th width="13%">Status</th>

                                        <th width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
									foreach($task_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $value['scenario']; ?></td>

                                    	<td><?php echo  substr($value['task'],0,50); ?></td>

                                    	<td>
										<?php
											 if ($value['crt_first_name']=='' && $value['crt_last_name'] ==''){
											 	echo '<span class="text-muted small">Not Assigned</span>';
											 }
											 else{
												 echo  $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']);
											 }
										 ?>
                                        </td>

                                    	<?php 

                                    if($value['task_status'] == 1 ){

                                    $status = ' <span class="text-red">In Progress</span>';

                                    } if($value['task_status'] == 0 ) {	

                                    $status = '<span class="text-red">Pre-Incident</span>';

                                    }

									 if($value['task_status'] == 2 ){

                                    $status = '<span class="text-green">Completed</span>';

                                    } 

                                    ?>

                                    <td><?php echo  $status ?></td>

                                    <td>
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>cc/scenario/update/<?php echo  $value['task_id']; ?>">Edit</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/scenario/delete/'.$value['task_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href=""  data-toggle="modal" data-target="#myModal<?php echo  $value['task_id']; ?>">View</a>
            
            </li>
          </ul>
        </div>    
                                    
                                    

                                   <?php /*?> <a class="btn btn-green btn-xs" href="<?php echo base_url(); ?>cc/scenario/update/<?php echo  $value['task_id']; ?>">Edit</a> 

								<a class="btn btn-orange btn-xs" onclick="return del_confirm('<?php echo base_url().'cc/scenario/delete/'.$value['task_id']; ?>');" href="javascript:void(0);">Delete</a>

                                

                                <a class="btn btn-orange btn-xs" href="javascript:;<?php echo base_url(); ?>cc/scenario/details/<?php echo  $value['task_id']; ?>" data-toggle="modal" data-target="#myModal<?php echo  $value['task_id']; ?>">View</a><?php */?>

                                    </td>

                                    </tr>
                                    
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $value['task_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Response Action Plan Details</h4>
      </div>
      <div class="modal-body">
			<p>Scenario: <span class="text-muted"><?php echo  $value['scenario']; ?></span></p>
			<p>Task: <span class="text-muted"><?php echo  $value['task']; ?></span></p>
			<p>Task Guidance: <span class="text-muted"><?php echo  $value['task_guide']; ?></span></p>
			<p>Assigned CRT: <span class="text-muted"><?php if ($value['crt_first_name'] != '' || $value['crt_last_name'] != ''){echo  $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); } else { echo 'Not Assigned'; }?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                    

								<?php } ?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->


								<?php }
								  else{ ?>
                                        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
										<p class="text-center" style="margin-top: 20px; color: #ccc">No tasks assigned</p>
								  <?php }?>







                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




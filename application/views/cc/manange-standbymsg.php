                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Standby Messages</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>

                                </li>

                                <li class="active">Standby Messages</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->

				

                <div class="row">

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/preincident"><i class="fa fa-angle-double-left"></i> Pre-Incident Menu</a> 
                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/standbymessage/add">Add Message</a> 
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} if($this->session->flashdata('error')!="") {

						?>

                        <div class="alert alert-error alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-folder-open"></i> Standby Messages </h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-body">


								<?php 

								if(count($msg_list)>0)

								{ ?>
							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="50%">Scenario</th>

                                        <th  width="50%">Message</th>

                                        <th  width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($msg_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo $value['scenario']; ?></td>

                                    	<td><?php echo substr(stripslashes($value['standby_message']),0,45).'..';?><?php /*?>&nbsp;&nbsp;<a href="<?php echo base_url();?>cc/standbymessage/details/<?php echo $value['stn_msg_id'];?>" >Read More</a><?php */?></td>

                                    	<td>
                                        
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>cc/standbymessage/update/<?php echo  $value["stn_msg_id"]; ?>">Edit</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url();?>cc/standbymessage/delete/<?php echo $value['stn_msg_id'];?>');">Delete</a>
            </li>
            <li>
                <a href=""   data-toggle="modal" data-target="#myModal<?php echo $value["stn_msg_id"];?>">View</a>
            
            </li>
          </ul>
        </div>    
        

                                    </td>

                                    </tr>

<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $value['stn_msg_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Standby Message Details</h4>
      </div>
      <div class="modal-body">
			<p>Scenario: <span class="text-muted"><?php echo $value['scenario']; ?></span></p>
			<p>Standby Message: <span class="text-muted"><?php echo htmlspecialchars_decode(nl2br(stripslashes($value['standby_message'])),true); ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
								<?php 

									}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->


								<?php 
								}
								else{ ?>
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-folder-open-o" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No standby messages</p>
								<?php }

								?>







                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




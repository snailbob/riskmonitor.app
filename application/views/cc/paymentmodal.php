<!-- Modal for new task -->
<div class="portfolio-modal modal fade" id="newPaymentModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container-fluid text-left container-new-payment">
            <div class="row row-subscriptionx">
                <div class="col-md-3 col-lg-2">

                    <nav>
                        <div class="lifecycle">
                            <ul class="list life-list">

                                <li class="item start current subscription">
                                    <a onclick="activateSubscriptionTab('subscription')">
                                        <div class="indicator">
                                            <div class="semiline"></div>
                                            <div class="semiline"></div>
                                            <div class="circle wow animated zoomIn"></div>
                                        </div>
                                        <span>Subscription</span>
                                    </a>
                                </li>

                                <!-- <li class="item billing_details">
                                    <a onclick="activateSubscriptionTab('billing_details')">
                                        <div class="indicator">
                                            <div class="semiline"></div>
                                            <div class="semiline"></div>
                                            <div class="circle wow animated zoomIn"></div>
                                        </div>
                                        <span>Billing Details</span>
                                    </a>
                                </li>

                                <li class="item last billing_contacts">
                                    <a onclick="activateSubscriptionTab('billing_contacts')">
                                        <div class="indicator">
                                            <div class="semiline"></div>
                                            <div class="semiline"></div>
                                            <div class="circle wow animated zoomIn"></div>
                                        </div>
                                        <span>Billing Contacts</span>
                                    </a>
                                </li> -->

                                <li class="item end payment_method">
                                    <a onclick="activateSubscriptionTab('payment_method')">
                                        <div class="indicator">
                                            <div class="semiline"></div>
                                            <div class="semiline"></div>
                                            <div class="circle wow animated zoomIn"></div>
                                        </div>
                                        <span>Payment Method</span>
                                    </a>
                                </li>

                            </ul>
                        </div>

                    </nav>


                    <!-- Nav tabs -->
                    <div class="list-group list-group-subscription hidden" role="tablist">
                        <a class="list-group-item active" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab" onclick="activateSubscriptionTab('subscription')">Subscription</a>
                        <a class="list-group-item" href="#billing_details" aria-controls="billing_details" role="tab" data-toggle="tab" onclick="activateSubscriptionTab('billing_details')">Billing Details</a>
                        <a class="list-group-item" href="#billing_contacts" aria-controls="billing_contacts" role="tab" data-toggle="tab" onclick="activateSubscriptionTab('billing_contacts')">Billing Contacts</a>
                        <a class="list-group-item" href="#payment_method" aria-controls="payment_method" role="tab" data-toggle="tab" onclick="activateSubscriptionTab('payment_method')">Payment Method</a>
                    </div>

                </div>
                <div class="col-md-6 col-lg-8">

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="subscription">
                            <?php $this->load->view('cc/subscription_option')?>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="billing_details">

                            <?php $this->load->view('cc/subscription_details')?>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="billing_contacts">

                            <?php $this->load->view('cc/subscription_contacts')?>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="payment_method">
                            <?php $this->load->view('cc/subscription_payment')?>

                        </div>
                    </div>


                </div>

                <div class="col-md-3 col-lg-2 row-subscription hidden">
                    <div class="alert alert-info">
                            
                        Our billing cycle starts from the first day of every month. You’ll be charged a prorated amount of $<strong class="prorated_payment_label"></strong>,
                        followed by an ongoing monthly subscription fee of $<strong class="monthly_payment_label"></strong>.
        
                    </div>
                </div>
            </div>
            
        </div>



    </div>
</div>
<!--portfolio-modal-->
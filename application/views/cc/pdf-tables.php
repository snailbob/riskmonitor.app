<!DOCTYPE html>
<html lang="en">
<body>

    <?php
	if(in_array("kpis", $tobegen)){
		
		$incident_id = $recallid;
		$active_module = substr($this->session->userdata('org_module'), 0, 1);


		$whr_inci = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			'closed'=>'0'
		);
	
	
		if($active_module == '5'){
			$incident_type = '5';
			$inci_type = 'recall';
			$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
			
			$whr_inci['id'] = $incident_id;
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

		}
		else{ //continuity = 8
			$incident_type = '8';
			$inci_type = 'continuity';
			$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
			
			$whr_inci['id'] = $incident_id;
			$the_incident = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));
		}
	
	
		$the_kpis = $this->common_model->loop_kpi($cc_id, $org_id, $incident_id, $inci_type, $incident_type);

		//print_r($the_kpis);
		
		
	?>
    
    
            <div></div>
    
    
            <table cellpadding="4">
    
                <tr>
                    
                    <th style="border-bottom: 1px solid #ddd; width:25%">
                    <h4>Product Reconcilliation</h4></th>
                    <th width="10%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h4>Total Units</h4></th>
                    <th width="10%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h4>Units Returned</h4></th>
                    <th width="36%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h4>Corrective Actions</h4>                    
                        <table class="table" style="margin-left: -4px; margin-bottom: -4px !important; ">
                            <tr class="bg-primary">
                                <td style="background-color:#337ab7; color: #fff; border-right: 1px solid #ddd;  border-top: 1px solid #ddd;" width="33%">Units Disposed</td>
                                <td style="background-color:#337ab7; color: #fff; border-right: 1px solid #ddd;  border-top: 1px solid #ddd;" width="33%">Units Transformed</td>
                                <td style="background-color:#337ab7; color: #fff; border-top: 1px solid #ddd;" width="33%">Total Corrections</td>
                            </tr>
                        </table>
                    
                    </th>
                    <th width="8%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h4>KPIs</h4></th>
                    <th width="11%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h4>Threshold</h4></th>
    
    
                </tr>
    
    			<?php 
					foreach($the_kpis['all_kpi'] as $r=>$value){
				?>
                
                <tr class="tr_main">
    
                    <td><i><?php echo $value['name'] ?></i>
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;" class="total_control">
                        <?php echo $value['total_unit']; ?>
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;" class="total_control">
                        <?php echo $value['unit_return']; ?>
                    </td>
                    <td class="text-center" style="padding: 0 0 0 0 !important; border: 1px solid #ddd;">
                        <table class="table" style="margin-top: -1px; margin-left: -1px; margin-bottom: -1px !important; ">
    
                            <tr>
                                <td width="33%">
                                    <?php echo $value['u_disposed']; ?>
                                </td>
                                <td width="33%">
                                    <?php echo $value['u_transformed']; ?>
                                </td>
                                <td width="33%">
                                    <?php echo $value['u_corrections']; ?>
                                </td>
                            </tr>
    
                        </table>
                    
                    
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd; background-color: #dff0d8;">
                        <?php echo $value['the_kpi']; ?>%
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <?php echo $value['u_threshold']; ?>%
                    </td>
                    
                </tr>
    			<?php 
					}
				?>

    
    
        </table><!--kpis table-->
        

        <div></div>
        <table cellpadding="4">

                <tr>
                    
                    <th style="border-bottom: 1px solid #ddd; width:28%"><h3>Notifications</h3></th>
                    
                    <th width="24%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>Notices Sent</h3></th>
                    <th width="24%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>Notices Received</h3></th>
                    <th width="24%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>KPIs</h3></th>



                </tr>

            
                <tr>
                    <td><i>Outbound notifications</i></td>
                    <td class="text-center" style="border: 1px solid #ddd;">
                        <?php echo $the_kpis['outbound_notification']['email_sent_count']; ?>
                    </td>
                    <td class="text-center" style="border: 1px solid #ddd;">
                        <?php echo $the_kpis['outbound_notification']['email_confirmed_count']; ?>
                    </td>
                    <td class="text-center bg-success" style=" border: 1px solid #ddd;">
                        <?php echo $the_kpis['outbound_notification']['noti_kpi']; ?>
                    </td>

                    

                </tr><!--consumer market-->



        </table><!--notifications-->
        
        
        
        
        
        <div></div>
        <table cellpadding="4">

                <tr>
                    
                    <th style="border-bottom: 1px solid #ddd; width:28%"><h3>Customer Contacts</h3></th>
                    
                    <th width="18%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>Contact Centre</h3></th>
                    <th width="18%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>Email</h3></th>
                    <th width="18%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>Website</h3></th>
                    <th width="18%" style="background-color:#337ab7; color: #fff; vertical-align: middle; border: 1px solid #ddd;"><h3>Trading Partner</h3></th>



                </tr>

            
                <tr class="tr_main">
                    <td><i>Inbound customer contacts</i>
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <?php echo $the_kpis['inbound_notification']['contact_cntr'] ?>                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <?php echo $the_kpis['inbound_notification']['kpi_email'] ?>                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <?php echo $the_kpis['inbound_notification']['kpi_website'] ?>                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        <?php echo $the_kpis['inbound_notification']['kpi_trading'] ?>                        
                    </td>


                </tr><!--consumer market-->



        </table>
        
        
        <div></div>
        
        
        <table cellpadding="4" style="border: 1px solid #d9edf7;">
          <tbody>
            <tr style="background-color:#337ab7; color: #fff;">
              <td><h3> Cost Monitor Category</h3></td>
            </tr>
            <tr style="background-color:#f5f5f5;">
                <th width="15%">Date</th>
                <th width="60%">Item</th>
                <th width="10%">CUR</th>
                <th width="15%">Cost</th>
            </tr>
            <tr>
              <td colspan="4">No items added.</td>
            </tr>
            
                
          </tbody>
        </table> 
            
            
            
		
	<?php } ?>

    <?php
	if(in_array("cost", $tobegen)){
	
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$dbcell = 'recall_id';
		}
		else{
			$dbcell = 'continuity_id';
		}
	
		$wher_cost_cat = array(
			'cc_id'=>$cc_id,
			'org_id'=>$org_id,
			$dbcell=>$recallid
		);
		
		$cost_category = $this->master_model->getRecords('cf_cost_category',$wher_cost_cat,'*',array('date_created'=>'DESC'));


			if(count($cost_category) > 0){
	
				foreach($cost_category as $r=>$value){
            ?>
            
        <div></div>
        <table cellpadding="4" style="border: 1px solid #d9edf7;">
          <tbody>
            <tr style="background-color:#337ab7; color: #fff;">
              <td><h3><?php echo $value['name']?> Cost Monitor Category</h3></td>
            </tr>
                
             <?php
			 $items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$value['cost_id']));
			if(count($items)>0){ ?>
                <tr style="background-color:#f5f5f5;">
                    <th width="15%">Date</th>
                    <th width="60%">Item</th>
                    <th width="10%">CUR</th>
                    <th width="15%">Cost</th>
                </tr>

            <?php
				$modular = 0;
				foreach($items as $r => $value){
					if($modular % 2){
						$style = 'style="background-color: #d9edf7;"';
					}else{
						$style = '';
					}
					
					?>
                
                <tr <?php echo $style; ?>>
                  <td><?php echo $value['item_date_invoice']?></td>
                  <td><?php echo $value['item_name']; ?></td>
                  <td><?php echo $this->common_model->currencycode($value['currency']) ?></td>
                  <td><?php echo number_format($value['item_cost'],2); ?></td>
                </tr>
                
                
                
			<?php $modular++;  }
			
			} //end of count items
			
			else{
			?>
                <tr>
                  <td colspan="4">No items added.</td>
                </tr>
                
            
			<?php }
			
			?>
                
          </tbody>
        </table> 
        
            
            <?php } //end foreach category?>
            
        <?php }else{ ?>
        <div></div>
        <table cellpadding="4" style="border: 1px solid #d9edf7;">
          <tbody>
            <tr style="background-color:#337ab7; color: #fff;">
              <td><h3>Cost Monitor Category</h3></td>
            </tr>
                
            <tr>
              <td>No Cost Monitor Category</td>
            </tr>
                
          </tbody>
        </table> 
            
        <?php }?>
	
                   
                
	<?php		
	}//end of check cost monitor
	
	if(in_array("blockers", $tobegen)){
	
		$active_mod  = substr($this->session->userdata('org_module'),0,1);
		//active module is recall
		if($active_mod == '5'){
			$dbcell = 'recall_id';
			$db_name = 'cf_recall_steps';
		}
		else{
			$dbcell = 'continuity_id';
			$db_name = 'cf_continuity_steps';
		}
	
	
		//get blockers 
		$bb = array(
			$dbcell=>$recallid
		);
		$blockers = $this->master_model->getRecords('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));
		

	?>
    
            
        <div></div>
        <table cellpadding="4" style="border: 1px solid #d9edf7;">
          <tbody>
            <tr style="background-color:#337ab7; color: #fff;">
              <td><h3>Issues Board</h3></td>
            </tr>
            <?php
            $modular = 0;
			if(count($blockers) > 0){
	
				foreach($blockers as $r=>$value){
					if($modular % 2){
						$style = 'style="background-color: #d9edf7;"';
					}else{
						$style = '';
					}
					
					$issue_note = 'No notes added';
					if($value['note'] != ''){
						$issue_note = $value['note'];
					}
					
					$issue_status = 'Unresolved';
					if($value['status'] != '0'){
						$issue_status = 'Closed';
					}
            ?>
                
                <tr <?php echo $style; ?>>
                  <td>Blocker: <?php echo $value['blocker']?><br />
                        Issue: <?php echo $value['issue']?><br />
                        Impact: <?php echo $value['impact']?><br />
                        Raised by: <?php echo $this->common_model->getcrtname($value['crt_id']);?><br />
                        Dependent Task(s): <?php echo $this->common_model->taskname($value['task_id'], $db_name);?><br />
                        Date Raised: <?php echo date_format(date_create($value['date_created']), 'M d Y, g:ia'); ?><br />
                        Assigned to: <?php if($value['assigned'] != '0') { echo $this->common_model->getcrtname($value['assigned']); } else { echo 'Not assigned'; }?><br />
                        Notes: <?php echo $issue_note ?><br />
                        Status: <?php echo $issue_status; ?>
                        
                  </td>
                </tr>
                
                
                <?php $modular++; } ?>
                
            <?php }else{ ?>
                
                <tr>
                  <td>No blockers.</td>
                </tr>
				
			<?php }?>
	
          </tbody>
        </table>            
                
	<?php
	}//end of check blockers

	if(in_array("steps", $tobegen)){
		// echo $recallid. '$recallid';
		$pdf_report_data = $this->common_model->pdf_report_data($recallid);

		
		foreach($pdf_report_data as $r=>$value){ ?>


			<table cellpadding="4" style="border: 1px solid #d9edf7;">
			<tbody>
				<tr style="background-color:#337ab7; color: #fff;">
					<td colspan="2"><h3><?php echo ucfirst($value['name']); ?></h3></td>
				</tr>

				<?php foreach($value['tasks'] as $tr=>$tvalue){ ?>

                <tr style="border-bottom: 1px solid #d9edf7">
					<td width="15%">
						<span style="<?php echo ($tvalue['status_text'] == 'Done') ? 'color: #3c763d' : ''; echo ($tvalue['status_text'] == 'In Progress') ? 'color: #f39c12' : '';?>"><?php echo $tvalue['status_text']; ?></span>
					</td>
					<td width="85%">
						<?php echo $tvalue['question']; ?>
						<span style="color: #666"><br>Assigned to: <?php echo (isset($tvalue['assigned']['full_name'])) ? $tvalue['assigned']['full_name'] : 'N/A'; ?></span>
						<span style="color: #666"><br>Timer: <?php echo $tvalue['timer']; ?></span>
						
					</td>
				</tr>

				<?php } ?>

				</tbody>
			</table>
			

		<?php
		}

		foreach($recall as $r=>$rc){//start loop each recall
			$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('id'=>$rc['input_pack_id'])); 
	
			if (count($myrecallpack) > 0){ //check if recall input pack
				$recallsteps = $this->master_model->getRecords('cf_recall_packs_steps',
					array('recall_pack_id'=>$myrecallpack[0]['id'], 'date <'=>$rc['initiation_date'], 'date_deleted'=>'0000-00-00 00:00:00'),'*',
					array('id'=>'ASC'));

				$rpack_step_no = 2;
				foreach($recallsteps as $rst=>$rstep){ //loop step category

				?>
			<div></div>
			
	<?php /*
		<table cellpadding="4" style="border: 1px solid #d9edf7;">
              <tbody>
                <tr style="background-color:#337ab7; color: #fff;">
                  <td colspan="2"><h3><?php echo ucfirst($rstep['name']); ?></h3></td>
                </tr>
                
                
                
				<?php
					//disect each step
					$active_mod  = substr($this->session->userdata('org_module'),0,1);
					//active module is recall
					// if($active_mod == '5'){
					// 	$dbtable = 'cf_recall_steps';
					// }else{
					// 	$dbtable = 'cf_continuity_steps';
					// }

					$dbtable="cf_recall_guidance";
					
					${'questions' . $rpack_step_no} = $this->master_model->getRecords($dbtable,
						array('recall_pack_id'=>'18','step_no'=>$rpack_step_no, 'date_deleted'=>'0000-00-00 00:00:00'),'*',
						array('arrangement'=>'ASC'));
		
                
                    $countqq = 0;
                    $curr_sub_c = '';
                    foreach(${'questions' . $rpack_step_no} as $q=>$kyu){
						
						if ($rpack_step_no == '2') {?>
                        
                        

                <tr style="border-bottom: 1px solid #d9edf7">
                    <td width="15%"><span style="color: #090">Completed</span></td>
                    <td width="85%"><?php echo $kyu['question']; ?><br><span style="color: #666"><?php echo $kyu['answer']; ?></span></td>
                </tr>
                        
                        
						<?php } //end step 1 tasks ?>
                        
						<?php if ($kyu['category_id'] != 0){
							
							$tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$kyu['category_id']));
							if ($curr_sub_c != $tsk_cat[0]['id']){ ?>
                            
          
                  <tr>
                    <td colspan="2" style="background-color: #d9edf7;"><h4 style="text-transform:uppercase; font-weight:bold;"><?php echo $tsk_cat[0]['category_name']?></h4></td>
                  </tr>
                            
							
                        <?php
							$countqq++;
							$curr_sub_c = $kyu['category_id'];
						
							}
						}//end sub category ?>
                

						<?php
						 if ($rpack_step_no != '2') {
							
							if ($kyu['answer'] != '' && $kyu['question'] == 'Hazard Analysis'){
								$s3_assessment = explode(' ',$kyu['answer']);
								$severity = $s3_assessment[0];
								$presence = $s3_assessment[1];
								$likely = $s3_assessment[2];
								$distribution = $s3_assessment[3];
								$identification = $s3_assessment[4];
								$hazard = $s3_assessment[5];
								
								$checkmark = '<img src="'.base_url().'assets/2/img/checkmark.png" alt="test alt attribute" width="15" height="15" border="0" /><br>';
								
								//severity
								$sev1 = '';
								$sev2 = '';
								$sev3 = '';
								$sev4 = '';
								
								if ($severity == 'Minor') {
									$sev1 = $checkmark;
								}
								else if ($severity == 'Moderate') {
									$sev2 = $checkmark;
								}
								else if ($severity == 'Major') {
									$sev3 = $checkmark;
								}
								else if ($severity == 'Serious') {
									$sev4 = $checkmark;
								}
								
								
								//presence
								$psev1 = '';
								$psev2 = '';
								$psev3 = '';
								$psev4 = '';
								
								if ($presence == 'Rare') {
									$psev1 = $checkmark;
								}
								else if ($presence == 'Some') {
									$psev2 = $checkmark;
								}
								else if ($presence == 'Most') {
									$psev3 = $checkmark;
								}
								else if ($presence == 'All') {
									$psev4 = $checkmark;
								}
								
								//likely
								$like1 = '';
								$like2 = '';
								$like3 = '';
								$like4 = '';
								
								if ($likely == 'Rare') {
									$like1 = $checkmark;
								}
								else if ($likely == 'Some') {
									$like2 = $checkmark;
								}
								else if ($likely == 'Many') {
									$like3 = $checkmark;
								}
								else if ($likely == 'Certain') {
									$like4 = $checkmark;
								}
												
								//distribution
								$d1 = '';
								$d2 = '';
								$d3 = '';
								$d4 = '';
								
								if ($distribution == 'Small') {
									$d1 = $checkmark;
								}
								else if ($distribution == 'Some') {
									$d2 = $checkmark;
								}
								else if ($distribution == 'Many') {
									$d3 = $checkmark;
								}
								else if ($distribution == 'National') {
									$d4 = $checkmark;
								}
									
											
								//identification
								$id1 = '';
								$id2 = '';
								$id3 = '';
								$id4 = '';
								
								if ($identification == 'Easy') {
									$id1 = $checkmark;
								}
								else if ($identification == 'Possible') {
									$id2 = $checkmark;
								}
								else if ($identification == 'Difficult') {
									$id3 = $checkmark;
								}
								else if ($identification == 'Undetectable') {
									$d4 = $checkmark;
								}	
								
									
								//hazard
								$ha1 = '';
								$ha2 = '';
								$ha3 = '';
								$ha4 = '';
								
								if ($hazard == 'Minor') {
									$ha1 = $checkmark;
								}
								else if ($hazard == 'Moderate') {
									$ha2 = $checkmark;
								}
								else if ($hazard == 'Major') {
									$ha3 = $checkmark;
								}
								else if ($hazard == 'Severe') {
									$ha4 = $checkmark;
								}
								
							}
							else{
								//severity
								$sev1 = '';
								$sev2 = '';
								$sev3 = '';
								$sev4 = '';

								//presence
								$psev1 = '';
								$psev2 = '';
								$psev3 = '';
								$psev4 = '';
								
								
								//likely
								$like1 = '';
								$like2 = '';
								$like3 = '';
								$like4 = '';
								
								//distribution
								$d1 = '';
								$d2 = '';
								$d3 = '';
								$d4 = '';
								
								//identification
								$id1 = '';
								$id2 = '';
								$id3 = '';
								$id4 = '';
								
								//hazard
								$ha1 = '';
								$ha2 = '';
								$ha3 = '';
								$ha4 = '';
								
							}
							
							
							//create table for hazard analysis
							// $subtable = '<table border="1" cellpadding="8" style="border: 1px solid #666;">
							// 	<tr>
							// 		<td bgcolor="#c3d69b" align="left">Severity</td>
							// 		<td bgcolor="#FFFF00" align="center">'.$sev1.' Minor</td>
							// 		<td bgcolor="#ffcc00" align="center">'.$sev2.' Moderate</td>
							// 		<td bgcolor="#ff6600" align="center">'.$sev3.' Major</td>
							// 		<td bgcolor="#c00000" align="center">'.$sev4.' Serious</td>
							// 	</tr>

							// 	<tr>
							// 		<td bgcolor="#c3d69b" align="left">Presence</td>
							// 		<td bgcolor="#FFFF00" align="center">'.$psev1.' Rare</td>
							// 		<td bgcolor="#ffcc00" align="center">'.$psev2.' Some</td>
							// 		<td bgcolor="#ff6600" align="center">'.$psev3.' Most</td>
							// 		<td bgcolor="#c00000" align="center">'.$psev4.' All</td>
							// 	</tr>

							// 	<tr>
							// 		<td bgcolor="#c3d69b" align="left">Likely Injury</td>
							// 		<td bgcolor="#FFFF00" align="center">'.$like1.' Rare</td>
							// 		<td bgcolor="#ffcc00" align="center">'.$like2.' Some</td>
							// 		<td bgcolor="#ff6600" align="center">'.$like3.' Many</td>
							// 		<td bgcolor="#c00000" align="center">'.$like4.' Certain</td>
							// 	</tr>

							// 	<tr>
							// 		<td bgcolor="#c3d69b" align="left">Distribution</td>
							// 		<td bgcolor="#FFFF00" align="center">'.$d1.' Small</td>
							// 		<td bgcolor="#ffcc00" align="center">'.$d2.' Some</td>
							// 		<td bgcolor="#ff6600" align="center">'.$d3.' Many</td>
							// 		<td bgcolor="#c00000" align="center">'.$d4.' National</td>
							// 	</tr>

							// 	<tr>
							// 		<td bgcolor="#c3d69b" align="left">Identification</td>
							// 		<td bgcolor="#FFFF00" align="center">'.$id1.' Easy</td>
							// 		<td bgcolor="#ffcc00" align="center">'.$id2.' Possible</td>
							// 		<td bgcolor="#ff6600" align="center">'.$id3.' Difficult</td>
							// 		<td bgcolor="#c00000" align="center">'.$id4.' Undetectable</td>
							// 	</tr>

							// 	<tr>
							// 		<td bgcolor="#c3d69b" align="left">Hazard</td>
							// 		<td bgcolor="#FFFF00" align="center">'.$ha1.' Minor</td>
							// 		<td bgcolor="#ffcc00" align="center">'.$ha2.' Moderate</td>
							// 		<td bgcolor="#ff6600" align="center">'.$ha3.' Major</td>
							// 		<td bgcolor="#c00000" align="center">'.$ha4.' Severe</td>
							// 	</tr>

							// </table>';
							
							
							//what to display
							if($kyu['question'] == 'Hazard Analysis'){
								$note = $subtable;
								$question = '';
								
							}
							else if($kyu['answer'] != '' && $kyu['question'] != 'Hazard Analysis'){
								$note = $kyu['answer'];
								$question = $kyu['question'];
							}
							else{
								$note = 'Assigned to: '; //No Notes Added';
								$question = $kyu['question'];
							}
							
							
							
												
							//check if there's approvers
							$the_approvers = unserialize($kyu['approvers']);
							$the_approvers_status = unserialize($kyu['approvers_status']);
							$approvers_count = 0;
							$approvers_status = 0;
							
							$crt_assigned = '';
							$crt_date_assigned = '';
							
							if($kyu['assigned'] != ''){
								$crt_assigned = '<br><span style="color: #666">Assigned to: '.$this->common_model->getname($kyu['assigned']).'</span>';
								$crt_date_assigned = '<span style="color: #666"> ('.date_format(date_create($kyu['date_assigned']), 'jS F Y, g:ia').')</span>';
								
							}
							
							if(is_array($the_approvers)){
								$approvers_count = count($the_approvers);
							}
							else{
								$the_approvers = array();
							}
							
							if(is_array($the_approvers_status)){
								$approvers_status = count($the_approvers_status);
							}
							else{
								$the_approvers_status = array();
							}
							
							
							//check status
							if ($kyu['status'] == '0'){
							 	$status = '<br><span style="color: #e74c3c">In Progress</span>';
							 	$timelapse = '';
								
								if($kyu['question'] != 'Hazard Analysis'){
									$note = '';
								}
							 	$answer = $note;

							}
							else if($approvers_count != $approvers_status){
							 	$status = '<br><span style="color: #F93">Awaiting Approval</span>';
							 	$timelapse = '';
								$answer = '<br><span style="color: #666">'.$note.'</span>';
							
								//array_diff — Computes the difference of arrays
								$result = array_diff($the_approvers, $the_approvers_status);
								
								if(count($result) > 0){
									$new_arr = array();
									foreach($result as $rs){
										$new_arr[] = $rs;
									}
									$crt_assigned = '<br><span style="color: #666">Assigned to: '.$this->common_model->getname($new_arr[0]).'</span>';
									$crt_date_assigned = '';
								}
								
							}
							else{
							 	$status = '<br><span style="color: #090">Completed</span>';
							 	$timelapse = '<br><span style="color: #666">Time to Complete: '.$kyu['time_lapse'].'</span>';
								$answer = '<br><span style="color: #666">'.$note.'</span>';
								
							}
						?>

                <tr style="border-bottom: 1px solid #d9edf7">

                    <td width="15%"><?php echo $status; ?></td>
                    <td width="85%"><?php echo $question; ?><?php echo $answer ?><?php echo $timelapse; ?><?php echo $crt_assigned.$crt_date_assigned; ?></td>
                </tr>
						<?php } //end not step 1?>
                
                
                        
			 <?php 		
					} //end foreach each questions on each steps
                ?>	
                
                
                
          </tbody>
		</table>  
		
	<?php */ ?>
                
                
                <?php
				$rpack_step_no++;

				} //end of foreach steps
				?>
                
			<?php
			}//
	
	
		}//end foreach recall

	}//end of check steps
	
    ?>
    
    
    
    
</body>
</html>

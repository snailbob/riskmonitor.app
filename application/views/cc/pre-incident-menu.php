                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                
<?php 
//select org
	//$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
	$theorg = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

	/*********************************************************/
?>                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Pre-Incident

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Pre-Incident</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>




                    <div class="col-lg-12">
                    
                    <?php if(isset($action_message['success'])){ ?>
                    
						<div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                			<?php echo strip_tags($action_message['success']); ?>
                            
                        </div>
                    <?php } ?>
	
    
    
    				<?php //case management module activated
						if (strpos($theorg[0]['active_module'], '2') !== false){ ?>

                        <div class="row">
                            <div class="col-sm-1 hidden-xs"></div>
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-male fa-fw"></i><br /><h4>User Groups<br />
<small>asf</small></h4>
                                 </a>
                            </div>
    
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-user fa-fw"></i><br /><h4>Response Team</h4>
                                 </a>
                            </div>
                            
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/settings/timezone" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-clock-o fa-fw"></i><br /><h4>Set Timezone</h4>
                                 </a>
                            </div>
    
                        </div>
					<?php } ?>                                   

					<?php //standard crisisflo activated
						if (strpos($theorg[0]['active_module'], '1') !== false){ ?>



                        <div class="row">
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-male fa-fw"></i><br /><h4>User Groups</h4>
                                 </a>
                            </div>
    
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-user fa-fw"></i><br /><h4>Response Team</h4>
                                 </a>
                            </div>
    
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/stakeholder/managestk" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-users fa-fw"></i><br /><h4>Stakeholders</h4>
                                 </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/document/manage" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-paperclip fa-fw"></i><br /><h4>Crisis Documents</h4>
                                 </a>
                            </div>
                            
                        </div>
                        
                    
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/scenario/managetask" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-lightbulb-o fa-fw"></i><br /><h4>Response Plan</h4>
                                 </a>
                            </div>
                            
                            
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/standbymessage/manage" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-folder-open fa-fw"></i><br /><h4>Standby Messages</h4>
                                 </a>
                            </div>
                            

                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/reminder" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-bell-o fa-fw"></i><br /><h4>Set Review Date</h4>
                                 </a>
                            </div>
                            
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/settings/timezone" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-clock-o fa-fw"></i><br /><h4>Set Timezone</h4>
                                 </a>
                            </div>


                        </div> 
                        
                                 
                                 
					<?php } ?>                                   
                        
                        
                        
            

					<?php //recall module activated
						if (strpos($theorg[0]['active_module'], '5') !== false || strpos($theorg[0]['active_module'], '8') !== false){ ?>



                        <div class="row">
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-male fa-fw"></i><br />
                                    <h4>User Groups<br />
                                    	<?php if(count($user_group_updated) > 0){ ?>
                                        <small class="ne">Last Updated: <?php echo date_format(date_create($user_group_updated[0]['date_updated']), ' d M Y'); ?></small>
                                        <?php } else { ?>
                                        <small class="">&nbsp;</small> 
                                        <?php } ?>
                                    </h4>
                                 </a>
                            </div>
    
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-user fa-fw"></i><br />
                                    
                                    <h4>Response Team<br />
                                    	<?php if(count($crt_updated) > 0){ ?>
                                        <small class="ne">Last Updated: <?php echo date_format(date_create($crt_updated[0]['date_updated']), ' d M Y'); ?></small>
                                        <?php } else { ?>
                                        <small class="">&nbsp;</small> 
                                        <?php } ?>
                                    </h4>
                                 </a>
                            </div>
    
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/stakeholder/managestk" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-users fa-fw"></i><br />
                                    
                                    <h4>Stakeholders<br />
                                    	<?php if(count($stk_updated) > 0){ ?>
                                        <small class="ne">Last Updated: <?php echo date_format(date_create($stk_updated[0]['date_updated']), ' d M Y'); ?></small>
                                        <?php } else { ?>
                                        <small class="">&nbsp;</small> 
                                        <?php } ?>
                                    </h4>
                                 </a>
                            </div>

                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/document/manage" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-paperclip fa-fw"></i><br />
                                    <h4>Recall Documents<br />
                                    	<?php if(count($docs_updated) > 0){ ?>
                                        <small class="ne">Last Updated: <?php echo date_format(date_create($docs_updated[0]['date_updated']), ' d M Y'); ?></small>
                                        <?php } else { ?>
                                        <small class="">&nbsp;</small> 
                                        <?php } ?>
                                    </h4>
                                    
                                 </a>
                            </div>
                            
                        </div>
                        
                    
                        <div class="row">

                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/standbytasks" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-list-alt fa-fw"></i><br />
                                    <h4>Standby Tasks<br />
                                    	<?php if($cc_info[0]['tasks_review_date'] != '0000-00-00 00:00:00'){ ?>
                                        <small class="ne">Last Updated: <?php echo date_format(date_create($cc_info[0]['tasks_review_date']), ' d M Y'); ?></small>
                                        <?php } else { ?>
                                        <small class="">&nbsp;</small> 
                                        <?php } ?>
                                    </h4>
                                    
                                 </a>
                            </div>
                            
                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/reminder" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-bell-o fa-fw"></i><br />
                                    
                                    <h4>Set Review Date<br />
                                    	<?php if(count($reminder_updated) > 0){
											$time = strtotime($review_date[0]['reminder_date']);
											$curtime = time();	
										?>
                                        <small class="ne <?php if($curtime > $time) { echo 'text-danger bg-danger';}?>">
											Next review date: <?php echo date('d M Y',strtotime($review_date[0]['reminder_date']));?>
										<?php /*?>Last Updated: <?php echo date_format(date_create($reminder_updated[0]['date_updated']), ' d M Y'); ?><?php */?>
                                        </small>
                                        <?php } else { ?>
                                        <small class="">&nbsp;</small> 
                                        <?php } ?>
                                    </h4>
                                    
                                 </a>
                            </div>

                            <div class="col-sm-3">
                                <a href="<?php echo base_url()?>cc/settings/timezone" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-clock-o fa-fw"></i><br />
                                    <h4>Set Timezone<br />
                                    	<?php //if($cc_info[0]['timezone_updated'] != '0000-00-00 00:00:00'){ ?>
                                        <small class="ne"><?php echo $timezone[0]['utc_name']; ?>
                                        </small>
                                        <?php // } ?>
                                    
                                    </h4>
                                
                                
                                 </a>
                            </div>
                        </div>
                        
					<?php } ?>                                   
                        
                        
                        

                    </div><!----.col-lg-12-->



                </div><!--.row-->




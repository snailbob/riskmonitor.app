                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                
<?php 
//select org
	//$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
	$theorg = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

	/*********************************************************/
?>                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Pre-Incident

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Pre-Incident</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>
                            <div class="alert alert-danger alert-dismissable">

                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                <?php echo $this->session->flashdata('error');   ?>

                            </div>

                            <?php } ?>

                    </div>




                    <div class="col-lg-12">
                    
                    <?php if(isset($action_message['success'])){ ?>
                    
						<div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                			<?php echo strip_tags($action_message['success']); ?>
                            
                        </div>
                    <?php } ?>




                    <div class="row row-setup">
                        
                        <div class="col-md-6 col-md-offset-3">
                            <div class="setup-line"></div>
                        </div>
                        <div class="col-md-2 col-md-offset-3">
                            <a href="<?php echo base_url().'cc/settings/timezone' ?>" title="<?php echo $timezone[0]['utc_name']; ?>">
                                <div class="setup-circle <?php echo ($cc_info[0]['timezone_updated'] != '0000-00-00 00:00:00') ? '' : 'setup-pending' ?>">
                                    <?php echo ($cc_info[0]['timezone_updated'] != '0000-00-00 00:00:00') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-clock-o" aria-hidden="true"></i>' ?>
                                    
                                </div>
                                <p>
                                    Set timezone
                                </p>
                            </a>
                        </div>

                        <div class="col-md-2">

                        <?php
                            $crt_date = (count($crt_updated) > 1) ? $crt_updated[0]['date_updated'] : '0000-00-00 00:00:00';
                            $crt_date = ($crt_updated != '0000-00-00 00:00:00') ? $crt_date : $crt_updated[0]['date_added'];
                            
                            $crt_count = (count($crt_updated) > 1) ? 'Last Updated: '.date_format(date_create($crt_date), ' d M Y') : '';
                            ?>

                            <a href="<?php echo base_url()?>cc/crisisteam/createteam" title="<?php echo $crt_count; ?>">
                                <div class="setup-circle <?php echo ($crt_count) ? '' : 'setup-pending'; ?>">
                                     <?php echo ($crt_count) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-user" aria-hidden="true"></i>'; ?>
                                    
                                </div>
                                <p>
                                    Create team
                                </p>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <a href="<?php echo base_url()?>cc/standbytasks" title="<?php echo 'Last updated: '.date_format(date_create($cc_info[0]['tasks_review_date']), ' d M Y'); ?>">
                                <div class="setup-circle <?php echo ($cc_info[0]['tasks_review_date'] != '0000-00-00 00:00:00') ? '' : 'setup-pending'; ?>">
                                     <?php echo ($cc_info[0]['tasks_review_date'] != '0000-00-00 00:00:00') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-list-alt" aria-hidden="true"></i>'; ?>
                                    
                                </div>
                                <p>
                                    Review standby tasks
                                </p>
                            </a>
                        </div>


                    </div><!--.col-lg-12-->



                </div><!--.row-->




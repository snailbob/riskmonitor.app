<style>
.dataTables_length, .dataTables_filter{
	display: none;
}

</style>


<?php
	//select org
	//$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

?>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1><?php echo 'Continuity Incident'; ?>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>cc/">Dashboard</a></li>
                                <li class="active"><?php echo 'Continuity Incident'; ?></li>

                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->




                <!-- Form AREA -->
				<div class="row">

                    <?php
						if(count($response) > 0) {

							$smsbal = $response[0]['sms_credit'];

							//echo ' US$ <b>SMS Balance: </b>'.$smsbal;
						}
					?>


                    <?php /*?><div class="col-lg-12 hidden" style="height: 50px; padding-left: 30px;">
                        <p class="lead">
							<?php if(count($response) > 0) {
                                foreach($response as $response){
								}

								$smsbal = $response['sms_credit'];
								//echo ' US$ <b>SMS Balance: </b>'.$smsbal;
							} ?>

                            <a href="#"  data-toggle="modal" data-target="#mySmsReport" class="btn btn-primary pull-right">View SMS Report</a>
<!-- Modal -->
<div class="modal fade" id="mySmsReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">SMS Usage Report</h4>
      </div>
      <div class="modal-body">
        <?php
			$sms_list=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0'));

			$failed_sms_list=$this->master_model->getRecords('sms',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status !='=>'0'));
			?>

            <!-- Nav tabs category -->
            <ul class="nav nav-tabs faq-cat-tabs" style="margin-bottom: 10px;">
                <li class="active"><a href="#faq-cat-1" data-toggle="tab">Sent</a></li>
                <li><a href="#faq-cat-2" data-toggle="tab">Failed</a></li>
            </ul>


<!-- Tab panes -->
            <div class="tab-content faq-cat-content">
                <div class="tab-pane active in fade" id="faq-cat-1">

					<?php
                    if (count($sms_list) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="example-table"> <thead>
             <tr> <th width="40%">Recipient</th> <th width="29%">Time Sent</th> <th width="30%">Price</th> </tr> </thead> <tbody>


                    <?php

                        foreach($sms_list as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. date('d M Y - H:i:s',strtotime($value['date_sent'])) .'</td>';
                            echo '<td>US$ '. $value['price'] .'</td></tr>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->

                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>

                </div>


                <!--second tab-->
                <div class="tab-pane fade" id="faq-cat-2">

					<?php
                    if (count($failed_sms_list) > 0){
                    ?>
                        <div class="table-responsive">  <table class="table table-hover table-bordered table-green" id="stake-table"> <thead>
             <tr> <th width="50%">Recipient</th> <th width="50%">Status</th> </tr> </thead> <tbody>


                    <?php

                        foreach($failed_sms_list as $row=>$value){
                            echo '<tr><td>'. $value['recipient'] .'</td>';
                            //echo 'Message ID: '. $value['message_id'] .'<br>';
                            echo '<td>'. $value['status'] .'</td>';
                        }
                    ?>
                        </tbody> </table> </div><!---responsive-table-->

                    <?php
                    }
                    else{
                        echo '<p class="text-center text-muted" style="margin-top: 40px;">No records found.</p>';
                    }
                    ?>



                </div>
            </div>




      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

						</p>
                    </div><!--.sms report-->

                      <?php */?>


                    <div class="col-lg-12">
                    	<?php
						if($this->session->flashdata('success')!="")
						{
						?>
                        <div class="alert alert-success alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>
                        <?php
						}
						if($this->session->flashdata('error')!="")
						{
						?>
                        <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>
                        <?php
						}
						?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4><?php echo 'Recall Incident'; ?></h4>
                                </div>
                                <div class="panel-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="panel-body">

                                    <form class="col-lg-6 col-lg-offset-3 col-md-10 col-md-offset-1" role="form" action='' method='post' name="initiate_repose" id="initiate_recall_form">
                                       <div class="form-group">
                                            <label>Response Type</label>

                                            	<div class="clr"></div>
                                                <div class="btn-group" data-toggle="buttons">
                                                  <label class="btn btn-default">
                                                    <input type="radio" name="radiovalit" value="0">Mock
                                                  </label>
                                                  <label class="btn btn-default">
                                                    <input type="radio" name="radiovalit" value="1">Live
                                                  </label>
                                                </div>

                                                <?php echo form_error('radiovalit'); ?>
                                        </div>

                                        <div class="form-group">
                                          <label for="">Description</label>
                                          <input type="text" class="form-control" name="description" id="" aria-describedby="helpId" placeholder="" value="<?php echo (isset($_GET['description'])) ? $_GET['description'] : '' ?>">
                                        </div>

																				<div class="row">
																					<div class="col-sm-6">
																						<div class="form-group">
		                                            <label for="inci_no">Incident number</label>
		                                                <input type="text" class="form-control" id="inci_no" name="inci_no" value="<?php echo sprintf("R%03s", $cc_incident_id); ?>" readonly="readonly"><?php echo form_error('inci_no'); ?>
		                                                <input type="hidden" class="form-control" id="inci_base_no" name="inci_base_no" value="<?php echo $cc_incident_id; ?>">
		                                        </div>

																				  </div>
																					<div class="col-sm-6">

		                                        <?php /*?><div class="form-group">
		                                            <label for="inci_date">Date of incident</label>
		                                                <input type="text" class="form-control" id="inci_date" name="inci_date" placeholder=""><?php echo form_error('inci_date'); ?>
		                                        </div><?php */?>


		                                        <div class="form-group">
		                                            <label for="inci_date">Date of incident</label>
		                                            <div class='input-group date' id='datetimepicker5'>
		                                                <span class="input-group-addon">
		                                                    <span class="glyphicon glyphicon-calendar"></span>
		                                                </span>
		                                                <input type='text' class="form-control" id="inci_date" name="inci_date" data-date-format="DD-MMM-YYYY" value="<?php echo set_value('inci_date'); ?>"/>

		                                            </div><?php echo form_error('inci_date'); ?>

		                                        </div>
																				  </div>
																				</div>






                                        <div class="form-group">
                                            <label for="inci_name">Incident name</label>
                                                <input type="text" class="form-control" id="inci_name" name="inci_name" placeholder="" value="<?php echo set_value('inci_name'); ?>"><?php echo form_error('inci_name'); ?>
                                        </div>


      <?php
	  		if(count($questions) > 0){
				$the_q = 1;
				foreach($questions as $q=>$question){ ?>


                                        <div class="form-group">
                                            <label for="step1q<?php echo $the_q?>"><?php echo $question['question']?></label>
                                                <input type="text" class="form-control" id="step1q<?php echo $the_q?>" name="step1q<?php echo $the_q?>" placeholder="" value="<?php echo set_value('step1q'.$the_q); ?>" data-rule-required="true"><?php echo form_error('step1q'.$the_q); ?>
                                        </div>


      <?php			$the_q++;
				}
			}
	  ?>

                                        <div class="form-group">
                                        	<span class="pull-right hidden-xs"><a href="javascript:;" class="addScnt"><i class="fa fa-plus"></i> Add More Location..</a></span>
                                            <label for="investigation">Affected sites/locations</label>
                                        	<p class="visible-xs"><a href="javascript:;" class="addScnt"><i class="fa fa-plus"></i> Add More Location..</a></p>
                                            <div id="p_scents">
                                                <p>
                                                    <input class="form-control aff_location" type="text" id="p_scnt1" name="p_scnt[]" value=""/>
                                                    <span class="small hidden"><a href="#" class="removeInp">Remove</a></span>
                                                    <input type="hidden" name="lat[]" value=""  />
                                                    <input type="hidden" name="lng[]" value=""  />
                                                </p>
                                                <?php /*?><p class="hidden">
                                                    <input class="form-control aff_location" type="text" id="p_scnt2" name="p_scnt[]" value=""  />
                                                    <span class="small"><a href="#" class="removeInp">Remove</a></span>
                                                    <input type="hidden" name="lat[]" value=""  />
                                                    <input type="hidden" name="lng[]" value=""  />
                                                </p>
                                                <p class="hidden">
                                                    <input class="form-control aff_location" type="text" id="p_scnt3" name="p_scnt[]" value=""  />
                                                    <span class="small"><a href="#" class="removeInp">Remove</a></span>
                                                    <input type="hidden" name="lat[]" value=""  />
                                                    <input type="hidden" name="lng[]" value=""  />
                                                </p>
                                                <p class="hidden">
                                                    <input class="form-control aff_location" type="text" id="p_scnt4" name="p_scnt[]" value=""  />
                                                    <span class="small"><a href="#" class="removeInp">Remove</a></span>
                                                    <input type="hidden" name="lat[]" value=""  />
                                                    <input type="hidden" name="lng[]" value=""  />
                                                </p>
                                                <p class="hidden">
                                                    <input class="form-control aff_location" type="text" id="p_scnt5" name="p_scnt[]" value=""  />
                                                    <span class="small"><a href="#" class="removeInp">Remove</a></span>
                                                    <input type="hidden" name="lat[]" value=""  />
                                                    <input type="hidden" name="lng[]" value=""  />
                                                </p>
                                                <p class="hidden">
                                                    <input class="form-control aff_location" type="text" id="p_scnt6" name="p_scnt[]" value=""  />
                                                    <span class="small"><a href="#" class="removeInp">Remove</a></span>
                                                    <input type="hidden" name="lat[]" value=""  />
                                                    <input type="hidden" name="lng[]" value=""  />
                                                </p><?php */?>
                                            </div>

                                            <div class="aff_loc_clones">

                                            </div>

                                        </div>



                                        <div class="form-group">
                                            <label>Email notification to: </label>
                                            	<div class="clr"></div>
                                                <div class="btn-group" data-toggle="buttons">
                                                  <label class="btn btn-default">
                                                    <input type="radio" name="inform" value="crtonly">Recall Team only
                                                  </label>
                                                  <label class="btn btn-default">
                                                    <input type="radio" name="inform" value="crtstk">Recall Team & Stakeholders
                                                  </label>
                                                </div>

												<?php echo form_error('inform'); ?>
                                        </div>


                                        <div class="form-group hidden">
                                            <label>Notify via SMS</label>
                                            	<div class="clr"></div>
                                                <div class="btn-group" data-toggle="buttons">
                                                  <label class="btn btn-default <?php if ($smsbal<='0'){echo 'disabled';} ?>">
                                                    <input type="radio" name="informsms" value="crtonly" <?php if ($smsbal<='0'){echo 'disabled';} ?>>CRTs only
                                                  </label>
                                                  <label class="btn btn-default <?php if ($smsbal<='0'){echo 'disabled';} ?>">
                                                    <input type="radio" name="informsms" value="crtstk" <?php if ($smsbal<='0'){echo 'disabled';} ?>>CRTs and Stakeholders
                                                  </label>
                                                </div>


												<?php echo form_error('informsms'); ?>
                                        </div>

                                        <div class="form-group">
                                                <button class="btn btn-primary"  name="btn_notify_incident" id="btn_notify_incident">Submit</button>


                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>

                </div><!--.row-->

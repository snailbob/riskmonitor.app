<style>

.dataTables_length, .dataTables_filter{
	display: none;
	visibility: hidden;
}
</style>    
    
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Reporting
                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>
                        
                                <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                        
                                </li>
                        
                                <li class="active"> Reporting</li>




                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                    

                <div class="row">

                    <div class="col-lg-12">

							<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                        </div>

                    </div>


                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Reporting</h4>

                                </div>

                                <div class="panel-widgets hidden">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#MailStakeholder"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="MailStakeholder" class="panel-collapse collapse in">

                                <div class="panel-body">
                                
                                
                                
                                


								<?php 

								if(count($open_recall)>0)

								{ ?>

							<div class="table-responsivexx">

							<table class="table" id="example-table">

                                <thead class="hidden">

                                    <tr>

                                        <th class="hidden">sort</th>
                                        
                                        <th width="100%">Event</th>
                                        <th></th>
                                        


                                    </tr>

                                </thead>

								<tbody>

								<?php
									$logsort = 1;
									foreach($open_recall as $r => $value)

									{

								?>

                                                        

                                    <tr>
										<td class="hidden"><?php echo $logsort; ?></td>
                                    	<td>
										<?php echo $value['incident_no'].': '. $value['incident_name']; ?>                      
                                        </td>
										<td class="">
                                        
                                           <!-- Single button -->
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                                </button>
                                              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
                                                <li><a href="#genreport" data-toggle="modal" onclick="$('#genform').find('#recall_id').val('<?php echo $value['id']; ?>')">Generate Report</a></li>
                                                <li><a href="#viewhistory" data-toggle="modal" onclick="getHistory('<?php echo $value['id']; ?>');">View History</a></li>
                                              </ul>
                                            </div>      
                                                    
                                                                            
                                        </td>

                                    </tr>


								<?php 
									$logsort++;
									}
									
									
								?>

                                                    

                                                </tbody>


                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-file-text-o" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall Incident.</p>
								<?php }

								?>


                                </div><!---end of panel-body-->

                            </div>

                        </div>

                        <!-- /.panel -->


                </div><!--.row -->

	
<!-- Modal -->
<div class="modal fade" id="genreport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Generate Report</h4>
      </div>
      <div class="modal-body">
        <form target="_blank" method="post" id="genform" action="<?php echo base_url().'pdfcreator/pdf/report'?>">
        	<div class="form-group">
                <input type="hidden" name="recall_id" id="recall_id" value="">
                <div class="checkboxx">
                  <label>
                    <input type="checkbox" name="tobegen[]" value="kpis" checked="checked">
                    Recall KPIs
                  </label>
                </div>
                <div class="checkboxx">
                  <label>
                    <input type="checkbox" name="tobegen[]" value="cost" checked="checked">
                    Cost Monitor
                  </label>
                </div>
                <div class="checkboxx">
                  <label>
                    <input type="checkbox" name="tobegen[]" value="blockers" checked="checked">
                    Blockers
                  </label>
                </div>
                <div class="checkboxx">
                  <label>
                    <input type="checkbox" name="tobegen[]" value="steps" checked="checked">
                    Steps
                  </label>
                </div>
            </div>

            <button class="btn btn-primary">Generate</button>
        </form>


      </div>

    </div>
  </div>
</div>

	
<!-- Modal -->
<div class="modal fade" id="viewhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">History</h4>
      </div>
      <div class="modal-body report-history">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


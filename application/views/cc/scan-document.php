                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

    <h1>Scan Crisis Documents</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Scan Crisis Documents</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

                <div class="row">

                    <div class="col-lg-12" style="height:40px;">

                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/document/upload">Upload Document</a> 

               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error:</strong> <?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">



                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-paperclip"></i> Crisis Documents</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                             <?php if(! is_null($error)) 

										echo $error;

										if(! is_null($success)) 

										echo $success;

								?>	

                            <div class="panel-body">


								<?php
								echo $response['data_id'];
								
								/*foreach ($response['items'] as $items)
								{
									echo "data_id:". $items['data_id'] ."<br>";
									echo "rest_ip:". $items['data_id'] ."<br>";
								};*/
								?>


                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




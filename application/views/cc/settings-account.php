<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller hidden ng-cloak" ng-controller="accountController">
    <div class="row row-account-header">
      <div class="col-sm-12">
        <h3>My Account</h3>
      </div>
    </div>
    <div class="row bg-white">
      <div class="col-sm-12x">

        <div class="tabbable-panel">
          <div class="tabbable-line">

            <uib-tabset active="activeTab">
              <uib-tab index="0" heading="Account Summary">
                <div class="padding">
                  <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">

                      <h4>Account Details</h4>
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-3 text-center">
                              <a onclick="$('#upload').click()" title="click to update" class="img-displayer">
                                <img ng-src="{{myInfo.avatar}}" class="img-circle img-thumbnail" alt="" width="100" height="100">
                              </a>
                            </div>
                            <div class="col-sm-9">

                              <div class="row">
                                <div class="col-sm-6">
                                  <p>
                                    <small class="text-muted">Name</small>
                                    <br> {{myInfo.full_name}}
                                    <a ng-click="launch('name')">Edit</a>
                                  </p>
                                </div>
                                <div class="col-sm-6">
                                  <p>
                                    <small class="text-muted">Position</small>
                                    <br> {{myInfo.crt_position || 'Nothing added'}}
                                    <a ng-click="launch('uinfo', 'Position')">Edit</a>
                                  </p>
                                </div>
                                <div class="col-sm-12">
                                  <p>
                                    <small class="text-muted">Level/Suite/Apt</small>
                                    <br> {{myInfo.crisis_function || 'Nothing added'}}
                                    <a ng-click="launch('uinfo', 'Level/Suite/Apt')">Edit</a>
                                  </p>
                                </div>
                                <div class="col-sm-12">
                                  <p>
                                    <small class="text-muted">Location</small>
                                    <br> {{myInfo.location || 'Nothing added'}}
                                    <a ng-click="launch('uinfo', 'Location')">Edit</a>
                                  </p>
                                </div>
                                <div class="col-sm-6">
                                  <p>
                                    <small class="text-muted">Mobile Number</small>
                                    <br> +{{myInfo.countrycode +' '+myInfo.crt_digits || 'Nothing added'}}
                                    <a ng-click="launch('uinfo', 'Mobile Number')">Edit</a>
                                  </p>
                                </div>

                              </div>
                            </div>
                          </div>

                        </div>
                      </div>


                      <h4>Login Details</h4>
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="row">
                            <!-- <div class="col-sm-3 text-center">
                                                        <a onclick="$('#upload').click()" title="click to update" class="img-displayer">
                                                            <img ng-src="{{myInfo.avatar}}" class="img-circle img-thumbnail" alt="" width="100" height="100">
                                                        </a>
                                                    </div> -->
                            <div class="col-sm-12">

                              <div class="row">

                                <div class="col-sm-6">
                                  <p>
                                    <small class="text-muted">Email</small>
                                    <br> {{myInfo.crt_email}}
                                    <a ng-click="launch('email')">Edit</a>
                                  </p>
                                </div>
                                <div class="col-sm-6">
                                  <p>
                                    <small class="text-muted">Password</small>
                                    <br> *********
                                    <a ng-click="launch('password')">Edit</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>


                      <h4>Linked Account</h4>
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-6">
                              <p>
                                <small class="text-muted">LinkedIn</small>
                                <br>
                                <a id="MyLinkedInButton" ng-if="!myInfo.linkedin_id" ng-class="{'linkedin_btn' : !myInfo.linkedin_id}">Link Account</a>
                                <a ng-if="myInfo.linkedin_id" ng-class="{'linkedin_connected_btn' : myInfo.linkedin_id}">Connected</a>
                              </p>
                            </div>
                            <div class="col-sm-6">
                              <p>
                                <small class="text-muted">Google</small>
                                <br>
                                <a id="MyGooglePlusButton" ng-if="!myInfo.gplus_id" ng-class="{'gplus_btn' : !myInfo.gplus_id}" onClick="handleAuthClick();">Link Account</a>
                                <a ng-if="myInfo.gplus_id" ng-class="{'gplus_connected_btn' : myInfo.gplus_id}">Connected</a>

                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <h4>Time Zone</h4>
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-12">
                              <p>
                                <small class="text-muted">Timezone</small>
                                <br> {{myInfo.timezone_details.name}}
                                <a ng-click="launch('timezone')">Edit</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </uib-tab>
              <uib-tab index="1" heading="Payment Method" ng-if="myInfo.user_level == '0'">
                <div class="padding">
                  <div class="row">


                    <div class="col-md-8 col-md-offset-2">

                      <div class="panel panel-default hidden">
                        <div class="panel-body">
                          <div ng-if="!billingContact || !paymentInfo.card_type">
                            <div class="row">
                              <div class="col-sm-2">
                                <strong>Trial</strong>
                                <br>Account
                              </div>
                              <div class="col-sm-5">
                                <span>
                                  <span class="pull-right">
                                    {{transaction.users_count.users}}/5
                                  </span>
                                  Team Members
                                </span>
                                <div class="progress">
                                  <div class="progress-bar" role="progressbar" ng-attr-aria-valuenow="transaction.users_count.percent" aria-valuemin="0" aria-valuemax="100"
                                    ng-style="{'width': transaction.users_count.percent+'%'}">
                                    <span class="sr-only">{{transaction.users_count.percent}}% Complete</span>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-5">
                                <span>{{transaction.trial_days.remaining}} trail days remaining</span>

                                <div class="progress">
                                  <div class="progress-bar" role="progressbar" ng-attr-aria-valuenow="ransaction.trial_days.percent" aria-valuemin="0" aria-valuemax="100"
                                    ng-style="{'width': transaction.trial_days.percent+'%'}">
                                    <span class="sr-only">{{transaction.trial_days.percent}}% Complete</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <p>
                              To upgrade your account, please enter your Payment Information and Billing Contact Details.
                            </p>
                          </div>


                          <div ng-if="billingContact && paymentInfo.card_type">
                            <a name="" id="" class="btn btn-default pull-right" ng-click="cancelSubscription()" role="button" ng-if="myInfo.subscription_cancelled == '0'">
                              Cancel
                            </a>
                            <a name="" id="" class="btn btn-default pull-right" ng-click="reactivateSubscription()" role="button" ng-if="myInfo.subscription_cancelled == '1'">
                              Stop Cancellation
                            </a>

                            <p ng-if="myInfo.subscription_cancelled == '1'">
                              <strong>Pending Cancellation</strong>
                              <br> Subscription will be cancelled on - {{transaction.billing.next_month_format}}.
                            </p>
                            <p ng-if="myInfo.subscription_cancelled == '0'">
                              <strong>Paid Subscription</strong>
                              <br> Your subscription renewal date is {{transaction.billing.next_month_format}}. Based on current
                              number of users assigned to your subscription, you will be charged ${{transaction.billing.orgs[0].total_bill}}AUD
                              at the next renewal date.
                            </p>
                          </div>

                        </div>
                      </div>

                      <!-- <h4>Payment Information</h4> -->
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="pi-add" ng-if="!paymentInfo.card_type">
                            <a name="" id="" class="btn btn-primary pull-right" ng-click="launch('payment')" role="button">
                              Update
                            </a>
                            <p>
                              <small class="text-muted">Payment Method</small>
                              <br> Not added
                            </p>
                          </div>
                          <div class="pi-edit" ng-if="paymentInfo.card_type">
                            <strong>Payment Method</strong>
                            <table class="table table-borderless">
                              <tr>
                                <td width="30%">
                                  <span class="text-muted">Card Type:</span>
                                </td>
                                <td>{{paymentInfo.card_type}}</td>
                              </tr>
                              <tr>
                                <td width="30%">
                                  <span class="text-muted">Card Number:</span>
                                </td>
                                <td>{{paymentInfo.number}}</td>
                              </tr>
                              <tr>
                                <td width="30%">
                                  <span class="text-muted">Cardholder Name:</span>
                                </td>
                                <td>{{paymentInfo.name}}</td>
                              </tr>

                            </table>

                            <p class="text-right">
                              <a name="" id="" class="btn btn-default" ng-click="launch('payment')" role="button">
                                Edit
                              </a>
                            </p>

                          </div>

                        </div>
                      </div>

                    </div>
                  </div>
                </div>


              </uib-tab>
              <uib-tab disable="!paymentInfo.card_type" index="2" heading="Transaction History" ng-if="myInfo.user_level == '0'">

                <div class="padding">

                  <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                      <h4>Purchase Activity</h4>
                      <table class="table">
                        <thead class="" style="background: #eee">
                          <tr>
                            <th>Invoice</th>
                            <th>Billing Date</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="bg-white">
                          <tr>
                            <td scope="row">
                              <a href="<?php echo base_url().'cc/invoice'?>" target="_blank">INV-00000</a>
                            </td>
                            <td>{{transaction.billing.next_month_format}}</td>
                            <td>
                              <span>CrisisFlo subscription</span>
                              <!-- <span ng-if="transaction.billing.orgs[0].org_crts.length">New Crisisflo user (x{{transaction.billing.orgs[0].org_crts.length + 1}})</span> -->

                            </td>
                            <td>Pending</td>
                            <td>${{transaction.billing.orgs[0].total_bill}}AUD</td>
                          </tr>
                          <tr ng-repeat="history in transaction.history track by $index">
                            <td scope="row">
                              <a ng-href="<?php echo base_url().'cc/invoice/index/'?>{{$index}}" target="_blank">INV-0000{{history.id}}</a>
                            </td>
                            <td>{{history.content.next_month_format}}</td>
                            <td>
                              <span ng-if="!history.content.orgs[0].org_crts.length">CrisisFlo subscription</span>
                              <!-- <span ng-if="history.content.orgs[0].org_crts.length">New Crisisflo user (x{{history.content.orgs[0].org_crts.length + 1}})</span> -->

                            </td>
                            <td>Paid</td>
                            <td>${{history.content.orgs[0].total_bill}}AUD</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>


              </uib-tab>

            </uib-tabset>

          </div>
        </div>

      </div>
    </div>

    <div class="row">


      <div class="col-lg-12">




      </div>
    </div>


    <script type="text/ng-template" id="nameModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Change your name
          <br>
          <small>We respect your privacy.</small>
        </h4>
      </div>
      <div class="modal-body">
        <ng-form name="nameDialog" novalidate role="form">
          <div class="form-group">
            <label class="control-label" for="course">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" ng-model="data.first_name" required>
          </div>
          <div class="form-group">
            <label class="control-label" for="course">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name" ng-model="data.last_name" required>
          </div>
        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Change Name</button>
      </div>

    </script>

    <script type="text/ng-template" id="passwordModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Change your password
          <br>
          <small>We respect your privacy.</small>
        </h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" ng-if="data.error">
          Password didn't match.
        </div>
        <ng-form name="nameDialog" novalidate role="form">
          <div class="form-group">
            <label class="control-label" for="course">Current password</label>
            <input type="password" class="form-control" name="current" id="current" ng-model="data.current" required>
          </div>
          <div class="form-group">
            <label class="control-label" for="course">New password</label>
            <input type="password" class="form-control" name="password" id="password" ng-model="data.password" required>
          </div>
          <div class="form-group">
            <label class="control-label" for="course">Confirm new password</label>
            <input type="password" class="form-control" name="password2" id="password2" ng-model="data.password2" required>
          </div>
        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Change Password</button>
      </div>

    </script>


    <script type="text/ng-template" id="uinfoModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Update info
        </h4>
      </div>
      <div class="modal-body">

        <ng-form name="nameDialog" novalidate role="form">
          <div class="form-group">
            <label class="control-label" for="course">{{type}}</label>

            <div ng-if="type == 'Location'">
              <input type="text" class="form-control uinfo_geolocation" name="location" id="location" ng-model="data.location" required>
              <input type="hidden" name="lat" ng-model="data.location_lat">
              <input type="hidden" name="lng" ng-model="data.location_lng">

            </div>

            <input type="text" class="form-control" ng-if="type == 'Level/Suite/Apt'" name="crisis_function" id="crisis_function" ng-model="data.crisis_function">

            <input type="text" class="form-control" ng-if="type == 'Position'" name="crt_position" id="crt_position" ng-model="data.crt_position"
              required>

            <div class="row" ng-if="type == 'Mobile Number'">
              <div class="col-sm-4">
                <select name="countrycode" id="countrycode" class="form-control" ng-options="tz as tz.short_calling for tz in countries track by tz.country_id"
                  ng-model="data.countrycode_details" ng-change="changeCc(data.countrycode_details)">
                </select>
              </div>
              <div class="col-sm-8">
                <input type="tel" class="form-control" name="crt_digits" id="crt_digits" ng-model="data.crt_digits" required>

              </div>
            </div>


            <!-- <textarea rows="3" class="form-control" ng-if="type == 'Crisis Function'" name="crisis_function" id="crisis_function" ng-model="data.crisis_function"
                            required></textarea> -->

          </div>

        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Update</button>
      </div>

    </script>
    <script type="text/ng-template" id="emailModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Change your email
          <br>
          <small>We respect your privacy.</small>
        </h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" ng-if="data.error">
          Password didn't match.
        </div>
        <ng-form name="nameDialog" novalidate role="form">
          <div class="form-group">
            <label class="control-label" for="course">Email</label>
            <input type="text" class="form-control" name="current" id="current" ng-model="data.crt_email" required>
          </div>
          <div class="form-group">
            <label class="control-label" for="course">Current password</label>
            <input type="password" class="form-control" name="current" id="current" ng-model="data.current" required>
          </div>
        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Change Email</button>
      </div>

    </script>

    <script type="text/ng-template" id="timezoneModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Change your time zone
          <br>
          <small>We respect your privacy.</small>
        </h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" ng-if="data.error">
          Password didn't match.
        </div>
        <ng-form name="nameDialog" novalidate role="form">
          <div class="form-group">
            <label class="control-label" for="course">Time zone</label>
            <select name="timezone" id="timezone" class="form-control" ng-options="tz as tz.name for tz in timeZone track by tz.id" ng-model="data.timezone_details"
              ng-change="changeTz(data.timezone_details)">
            </select>
          </div>
        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Change Time zone</button>
      </div>

    </script>

    <script type="text/ng-template" id="paymentModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Edit Payment Method
        </h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" ng-if="error.message">
          {{error.message}}
        </div>
        <ng-form name="nameDialog" class="form-sm" novalidate role="form">
          <div class="hidden">
            <p>
              <span class="pull-right">
                * Required
              </span>
              <strong>
                Subscription Plan
              </strong>
            </p>


            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*Subscription Type:</p>
              </div>
              <div class="col-sm-7">
                <select name="card_type" ng-model="payment.selected_plan" id="" class="form-control input-sm" required>
                  <option value="0">Free</option>
                  <option value="1">10 Users for $49 / month</option>
                  <option value="2">30 Users for $79 / month</option>
                </select>
              </div>
            </div>
          
          </div>

          <p>
            <strong>
              Payment Method
            </strong>
          </p>


          <div class="form-group row gutter-10">
            <div class="col-sm-3 col-sm-offset-1 text-right">
              <p class="form-control-staticx">*Card Type:</p>
            </div>
            <div class="col-sm-7">
              <select name="card_type" ng-model="payment.card_type" id="" class="form-control input-sm" required>
                <option value="MasterCard">MasterCard</option>
                <option value="Visa">Visa</option>
                <option value="Discover">Discover</option>
                <option value="American Express">American Express</option>
              </select>
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-3 col-sm-offset-1 text-right">
              <p class="form-control-staticx">*Card Number:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="number" id="number" ng-model="payment.number" required>
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-3 col-sm-offset-1 text-right">
              <p class="form-control-staticx">*Name on Card:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="name" id="name" ng-model="payment.name" required>
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-3 col-sm-offset-1 text-right">
              <p class="form-control-staticx">*Expiration Date:</p>
            </div>
            <div class="col-sm-2">
              <select name="exp_month" ng-model="payment.exp_month" id="" class="form-control input-sm" required>
                <option value="01" selected>01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
            <div class="col-sm-2">
              <select name="exp_year" ng-model="payment.exp_year" id="" class="form-control input-sm" required>
                <option value="2017" selected>2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
              </select>
            </div>
            <div class="col-sm-1">
              <p class="form-control-staticx">*Cvv:</p>
            </div>

            <div class="col-sm-2">
              <div class="input-group input-group-trans">
                <input type="text" class="form-control input-sm" name="cvc" ng-model="payment.cvc" placeholder="" aria-describedby="basic-addon2"
                  required>
                <span class="input-group-addon" id="basic-addon2">
                  <a uib-popover-template="'cvvPopoverTemplate.html'" popover-append-to-body="false" popover-placement="left">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                  </a>
                </span>
              </div>
            </div>
          </div>

          <div class="hidden">
            <strong>
              Billing Details
            </strong>

            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*Country:</p>
              </div>
              <div class="col-sm-7">
                <select name="country" id="country" class="form-control input-sm" ng-options="country as country.short_name for country in countries track by country.country_id"
                  ng-model="data.country_details" ng-change="changeCountry(data.country_details)">
                </select>
              </div>
            </div>

            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*Postal Code:</p>
              </div>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name="address_zip" id="address_zip" ng-model="payment.address_zip" required>
              </div>
            </div>
            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*State:</p>
              </div>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name="address_state" id="address_state" ng-model="payment.address_state"
                  required>

              </div>
            </div>

            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*Address:</p>
              </div>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name="address_line1" id="address_line1" ng-model="payment.address_line1"
                  required>
              </div>
            </div>

            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*City:</p>
              </div>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name="address_city" id="address_city" ng-model="payment.address_city" required>
              </div>
            </div>
            <div class="form-group row gutter-10">
              <div class="col-sm-3 col-sm-offset-1 text-right">
                <p class="form-control-staticx">*Phone Number:</p>
              </div>
              <div class="col-sm-7">
                <input type="text" class="form-control input-sm" name="phone" id="phone" ng-model="payment.phone" required>
              </div>
            </div>
          
          </div>
        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading">Confirm</button>
      </div>

    </script>

    <script type="text/ng-template" id="billingInfoModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h4 class="modal-title">
          Edit Contact Information
        </h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" ng-if="data.error">
          Password didn't match.
        </div>
        <ng-form name="nameDialog" class="form-sm" novalidate role="form">

          <div class="form-group row gutter-10" style="margin-top: 25px;">
            <div class="col-sm-4 text-right">
              <p class="form-control-staticx">*Billing Email:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="card_number" id="card_number" ng-model="data.crt_email" required>
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-4 text-right">
              <p class="form-control-staticx">First Name:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="name" id="name" ng-model="data.first_name">
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-4 text-right">
              <p class="form-control-staticx">Last Name:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="name" id="name" ng-model="data.last_name">
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-4 text-right">
              <p class="form-control-staticx">Alternate Billing Email:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="name" id="name" ng-model="data.alt_email">
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-4 text-right">
              <p class="form-control-staticx">Company:</p>
            </div>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" name="name" id="name" ng-model="data.company">
            </div>
          </div>
          <div class="form-group row gutter-10">
            <div class="col-sm-4 text-right">
              <p class="form-control-staticx">CrisisFlo Billing Receipt:</p>
            </div>
            <div class="col-sm-7">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" name="receipt" id="receipt" ng-model="data.receipt">
              </label>
            </div>
          </div>
        </ng-form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine || data.loading === true">Save</button>
      </div>

    </script>

    <script type="text/ng-template" id="cvvPopoverTemplate.html">
      <div>
        <h4>FINDING YOUR CARD VERIFICATION VALUE</h4>
        <p>The card verification value (CVV) is used as a security precaution, insuring that you have possession of your card.</p>
        <p>
          <img ng-src="{{base_url}}assets/img/cards/card1.jpg" class="img-responsive">
        </p>
        <p>For
          <b>MasterCard, Visa &amp; Discover</b> cards, the CVV is the last three digits printed on the black of your card in
          the signature panel.</p>
        <p>
          <img ng-src="{{base_url}}assets/img/cards/card2.jpg" class="img-responsive">
        </p>
        <p>The CVV for
          <b>American Express</b> cards is the four small number printed the front of the card, above the last few embossed
          digits.</p>

      </div>

    </script>

  </div>
</div>
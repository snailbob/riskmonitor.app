                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Coordinators

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Manage Coordinators</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>
                    
                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/settings"><i class="fa fa-angle-double-left"></i> Settings Menu</a> 
                        <a class="btn btn-primary pull-right" href="<?php echo base_url();?>cc/settings/coordinator">Add Coordinator</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">
                    
                    
                    	<div class="panel panel-default">
                        	<div class="panel-heading">
                            	<h4 class="heading-title">Manage Coordinators</h4>
                            
                            </div>
                            
                            <div class="panel-body">
                            	
                                <?php if(count($all_cc) > 0){ ?>
									
                                    <div class="table-responsive">
                                    
                                    <table class="table table-hover" id="example-tablexx">
                                    	<thead>
                                        	<tr>
                                                <th width="50%">Name</th>
        
                                                <th width="50%">Status</th>
        
                                                <th> </th>
                                     
                                            </tr>
                                        </thead>
                                    <tbody>
                                    	
                                    <?php foreach($all_cc as $r=>$value){ ?>
                                    <tr>
                                    	<td><?php echo $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); if($org_info[0]['cc_id'] == $value['login_id']) { echo '<span class="text-muted"> (Original Coordinator)</span>';}?></td>
                                        
										<?php 
                                            
                                            $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting Acceptance</span>';
                                            if($value['user_status'] == 1){
        
                                                $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Accepted</span>';
        
                                            }
    
                                        ?>

                                        <td><?php echo $status; ?></td>
                                        
                                        
                                        <td>
                                        
           <!-- Single button -->
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                Action <span class="caret"></span>
                </button>
              <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
              	<?php if($org_info[0]['cc_id'] != $value['login_id'] && $value['login_id'] != $this->session->userdata('user_id') ) { ?>
                    <li>
                        <a href="<?php echo base_url(); ?>cc/settings/coordinator/<?php echo  $value['login_id']; ?>">Edit</a> 
        
                    </li>
                    <li>
                        <a href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'cc/crisisteam/delete/'.$value['login_id'].'/'.$this->uri->segment(2); ?>');">Delete</a>
                    </li>
                <?php } ?>
                <li>
                    <a href="#"  data-toggle="modal" data-target="#myModal<?php echo  $value['login_id'];?>">View Details</a>
                </li>
                <?php if($value['user_status'] == '0'){?>
                <li>
                    <a href="javascript: resendCC('<?php echo $value['login_id']?>');">Re-send</a>
                </li>
                <?php } ?>
                
                
              </ul>
            </div>    
    
                                        </td>                                        
                                        
                                        
                                    </tr>    
									<?php } ?>
                                    </tbody>
                                    </table>
                                    </div>
                                    
                                    
								<?php }
									else{
										echo '<p class="text-muted text-center">Nothing to show you.</p>';
									}
								?>
                            </div>
                        </div>
                    
                    
                    </div><!----.col-lg-12-->



                </div><!--.row-->





<?php
	$crt_emails = array();
	if(count($all_cc)>0){
	foreach($all_cc as $r => $value) {
		//if (strpos($value['module'], substr($active_module, 0, 1)) !== false){

		$crt_emails[] = $value['crt_email'];
?>


<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Coordinator's Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></span></p>
			<p class="hidden">Position: <span class="text-muted"><?php echo  $value['crt_position']; ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_mobile']); ?></span></p>
			<p class="hidden">Crisis Function: <span class="text-muted"><?php echo  $value['crisis_function']; ?></span></p>
			<p class="hidden">Alternate Member: <span class="text-muted"><?php echo  $value['alt_member']; ?></span></p>
            
            <?php
				if($value['user_status'] == 1){
					echo '<p>Received SMS: <span class="text-muted">';
					
					if ($value['received_sms'] == 0){
						echo 'No';
					}
					else{
						echo 'Yes';
					}
					echo '</p>';
				}
			?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<?php } } //} //org crts ?>
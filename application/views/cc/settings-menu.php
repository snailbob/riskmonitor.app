                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Settings

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Settings</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <a href="<?php echo base_url()?>cc/settings/changePassword" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-key fa-fw"></i><br /><h4>Change Password</h4>
                                 </a>
                            </div>

                        
                            <div class="col-md-4 col-sm-6">
                                <a href="<?php echo base_url()?>cc/settings/updateinfo" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-info fa-fw"></i><br /><h4>Update Details</h4>
                                 </a>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <a href="<?php echo base_url()?>cc/dashboard/selectmodule" class="thumbnail text-center preloadThis sidebar_submenu">
                                   <i class="fa fa-eraser fa-fw"></i><br /><h4>Change Module</h4>
                                 </a>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4 col-sm-6">
                                <a href="<?php echo base_url()?>cc/settings/verifications" class="thumbnail text-center preloadThis sidebar_submenu">
                                   <i class="fa fa-google-plus fa-fw"></i><br /><h4>Social Sign On</h4>
                                 </a>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <a href="<?php echo base_url()?>cc/settings/payments" class="thumbnail text-center preloadThis sidebar_submenu">
                                   <i class="fa fa-cc-stripe fa-fw"></i><br /><h4>Billing Details</h4>
                                 </a>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <a href="<?php echo base_url()?>cc/settings/coordinators" class="thumbnail text-center preloadThis sidebar_submenu">
                                   <i class="fa fa-plus fa-fw"></i><br /><h4>Manage Coordinators</h4>
                                 </a>
                            </div>
                            
                        </div>
                        
                        
                        
                        

                    </div><!----.col-lg-12-->



                </div><!--.row-->




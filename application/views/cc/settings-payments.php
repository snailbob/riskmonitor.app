                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Settings

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Settings</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/settings"><i class="fa fa-angle-double-left"></i> Settings Menu</a> 
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">
                    
                    
                    	<div class="panel panel-default">
                        	<div class="panel-heading">
                            	<h4 class="heading-title">Manage Billing
                                	<?php if($user_info[0]['single_user'] == 'y' && $user_info[0]['stripe_customer_id'] != '0') {
										$bill_info = $billing['orgs'][0]; 
										$prv_txt = ($bill_info['org_info'][0]['last_payment'] == '0000-00-00 00:00:00') ? 'Trial end Date: ' : 'Last Payment Date:';
										$prv_txt .= date_format(date_create($bill_info['last_payment']), 'd M Y');
										$nxt_date = date('Y-m-1 H:i:s', strtotime("+1 month"));
										$nxt_date = date_format(date_create($nxt_date), 'd M Y');
										
										
										$total_u = count($bill_info['org_crts']) + 1; //org_members + 1;
										$total = $total_u * $user_info[0]['single_price']; ?>
                                	<a href="#" data-toggle="modal" data-target="#paymentDetailsModal" class="hidden pull-right" title="<?php echo $total_u.' total users at $'.$user_info[0]['single_price']?>/month">Next billing amount: $<?php echo $billing['orgs'][0]['total_bill']; ?></a>
                                    <?php } ?>
                                </h4>
                            
                            </div>
                            
                            <div class="panel-body">
                            
                            
                            	<div class="row">
                                	<div class="col-md-6 col-md-offset-3">
                                      <div class="card_form <?php if($user_info[0]['stripe_customer_id'] != '0') { echo 'hidden'; } ?>">
                                        <div class="alert alert-info text-center <?php if($user_info[0]['stripe_customer_id'] != '0') { echo 'hidden'; } ?>">
                                            Add your billing details to activate your account.
                                        </div>
                                        
                                        <div class="well">
                                            <ul class="list-inline pull-right">
                                                <li><img src="<?php echo base_url().'assets/img/cards/visa.png' ?>" alt="" title="" /></li>
                                                <li><img src="<?php echo base_url().'assets/img/cards/amex.png' ?>" alt="" title="" /></li>	
                                                <li><img src="<?php echo base_url().'assets/img/cards/mastercard.png' ?>" alt="" title="" /></li>
                                            
                                            </ul>
                                            
											<?php if($user_info[0]['stripe_customer_id'] != '0'){ ?>
                                          		<p><a href="#" onclick="$('.card_added_div').removeClass('hidden'); $(this).closest('.card_form').addClass('hidden');">Cancel</a></p>
										    <?php } ?>
                                    
                                        <form role="form" id="payment-form">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label for="cardNumber">CARD NUMBER</label>
                                                        <div class="input-group">
                                                            <input 
                                                                type="tel"
                                                                class="form-control"
                                                                name="cardNumber"
                                                                placeholder="Valid Card Number"
                                                                autocomplete="cc-number"
                                                                required autofocus 
                                                            />
                                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                        </div>
                                                    </div>                            
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-7 col-md-7">
                                                    <div class="form-group">
                                                        <label for="cardExpiry"><span class="">EXPIRATION</span> DATE</label>
                                                        <input 
                                                            type="tel" 
                                                            class="form-control" 
                                                            name="cardExpiry"
                                                            placeholder="MM / YY"
                                                            autocomplete="cc-exp"
                                                            required 
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-xs-5 col-md-5 pull-right">
                                                    <div class="form-group">
                                                        <label for="cardCVC">CV CODE</label>
                                                        <input 
                                                            type="tel" 
                                                            class="form-control"
                                                            name="cardCVC"
                                                            placeholder="CVC"
                                                            autocomplete="cc-csc"
                                                            required
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-success btn-lg btn-block" type="submit">Submit</button>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;">
                                                <div class="col-xs-12">
                                                    <p class="payment-errors"></p>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                      </div>

                                      <div class="card_added_div <?php if($user_info[0]['stripe_customer_id'] == '0') { echo 'hidden'; } ?>">
                                      	<div class="row">
                                        
                                        	<div class="col-sm-6">
                                                <div class="well">
                                                    <?php
                                                        if(isset($customer['description'])){
                                                            echo 'Organization: '.$customer['description'].'<br>';
                                                            echo 'Email: '.$customer['email'].'<br>';
                                                            echo 'Card: ***********'.$customer['sources']['data'][0]['last4'].'<br>';
                                                        }
                                                    ?>
                                                    <a href="#" class="btn btn-primary btn-sm" onclick="$('.card_form').removeClass('hidden'); $(this).closest('.card_added_div').addClass('hidden');">Update Card</a>
                                                </div>                                            
                                            </div>

                                        	<div class="col-sm-6">
                                                <div class="well">
    
                                                    Next billing amount: $<?php echo $bill_info['total_bill']; ?><br />
                                                    Next billing date: <?php echo $nxt_date; ?><br />
                                                    No. of active users: <?php echo $total_u; ?><br />
                                                    <a href="#" data-toggle="modal" data-target="#paymentDetailsModal" class="btn btn-primary btn-sm">More Details</a>
                                                </div>
                                            </div>
                                            
                                      </div>
                                      
                                      

                                        <p class="small text-muted text-center" style="margin: 0 auto;">
                                        <img class="pull-left" src="<?php echo base_url().'assets/img/lock-icon.png'?>" alt="" title="" style="margin-left: 10px;"/>
                                       <span class="text-left"> For your security, CrisisFlo does not store credit card details.<br /> Stripe is our secure payment provider. <a href="https://stripe.com" target="_blank">Find out more</a></span></p>


                                    </div>
                                </div>


                            </div>
                        </div>
                    
                    
                    </div><!----.col-lg-12-->



                </div><!--.row-->




    
    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="paymentDetailsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1>Next Billing Details</h1>
                            <hr class="star-primary">
                            <div class="alert alert-info">
                                <p class="text-left">

          
                                	<span class="pull-right">Rate/user: $<?php echo $bill_info['cc_info']['single_price'] ?> per month<br />
                                    <?php if($bill_info['gst'] > 0) { echo '10% GST: $'.$bill_info['gst']; }?>

</span>
									<?php echo $prv_txt; ?><br />
									Next Payment Date: <?php echo $nxt_date; ?>

                                
                                </p>
                            </div>
                            
                            <table class="table table-striped text-left">
                            	<thead>
                                	<tr>
                                    	<th>User</th>
                                    	<th>Date Activated</th>
                                    	<th>Prorate Fee</th>
                                    </tr>
                                </thead>
                            	<tbody>
                                	<tr>
                                    	<?php $cc_info = $billing['orgs'][0]['cc_info']; ?>
                                    	<td><?php echo $this->common_model->getname($cc_info['login_id'])?></td>
                                    	<td><?php echo date_format(date_create($cc_info['date_profile_activated']), 'd M Y'); ?></td>
                                    	<td>$<?php echo $cc_info['user_bill']; ?></td>
                                    </tr>
                                    
                                    <?php
										if($bill_info['org_members_count'] > 0){
											foreach($bill_info['org_crts'] as $r=>$value){ ?>
                                        
                                            <tr>
                                                <td><?php echo $this->common_model->getname($value['login_id'])?></td>
                                                <td><?php echo date_format(date_create($value['date_profile_activated']), 'd M Y'); ?></td>
                                                <td>$<?php echo $value['user_bill']; ?></td>
                                            </tr>
                                            
                                            <?php									
											
											}
										}
									?>
                                	<tr>
                                    	<td colspan="3" class="text-right"><strong>Grand Total: $<?php echo $bill_info['total_bill']; ?></strong></td>
                                    </tr>
                                    
                                    
                                </tbody>
                            </table>
                            
              
                        </div><!--modal-body-->
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div><!--portfolio-modal-->
    

                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Coordinator Details  </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Update Details</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12" style="height:40px;">
                        <a class="btn btn-default pull-left" href="<?php echo base_url();?>cc/settings/coordinators"><i class="fa fa-angle-double-left"></i> Manage Coordinators</a> 
               		</div>

                    <div class="col-lg-12" style="padding-top:10px;">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>
                            
                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Coordinator Details</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="admin_change_password" id="admin_change_password" method='post' class="form-horizontal" role="form" validate>

						

                     

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="f_name" name="f_name" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_first_name'])?>" required data-msg-required="Please enter last name"><?php echo form_error('f_name');  ?>

                            </div>

                        </div>

                    

                          

                        <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="l_name" name="l_name" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_last_name'])?>" required data-msg-required="Please enter first name"><?php echo form_error('l_name'); ?>

                            </div>

                        </div>

                        

                         <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Email Address</label>

                            <div class="col-sm-10">

                            <input type="email" class="form-control" id="emailid" name="emailid" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_email'])?>" required data-msg-required="Please enter first name"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>


                        <div class="form-group hidden">

                            <label class="col-sm-2 control-label">Location</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control geocomplete" id="crt_location" name="crt_location" value="<?php echo $personal_info[0]['location']?>"/><?php echo form_error('crt_location'); ?>
                                <input name="country_short" id="country_short" type="hidden" value="">
                                <input name="lat" id="lat" type="hidden" value="<?php echo $personal_info[0]['location_lat']?>">
                                <input name="lng" id="lng" type="hidden" value="<?php echo $personal_info[0]['location_lng']?>">
                            </div>

                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <input type="text" class="form-control hidden" id="countrycodex" name="countrycodex" placeholder="Country Code" value="<?php echo $this->master_model->decryptIt($personal_info[0]['countrycode']); ?>"/>
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['country_id'].' '.$countries['calling_code'].'"';
                                        
                                        if ($personal_info[0]['country_id'] == $countries['country_id']){
                                            echo ' selected="selected"';
                                        }
                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select><?php echo form_error('countrycode'); ?>

                            </div>
                            <div class="col-sm-6">

                            <input type="number" class="form-control" id="ph_no" name="ph_no" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_digits'])?>" required data-msg-required="Please enter first name"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>


        
                           <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Address</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_address'])?>" required data-msg-required="Please enter first name"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>

                        

                           <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">City</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_city'])?>" required data-msg-required="Please enter first name"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>

                        

                        

                        

                           <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">State</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="state" name="state" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_state'])?>" required data-msg-required="Please enter first name"><?php echo form_error('state'); ?>

                            </div>

                        </div>

                        

                         <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Zip/Post Code</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="zip" name="zip" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_zip_code'])?>" required data-msg-required="Please enter first name"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_coordinator" id="add_coordinator">Submit</button>

                            </div>

                        </div>

										

									</form>									

                                </div>

                            </div>

                        </div>



                    </div><!--./col-lg-12-->



                </div><!--.row-->




<?php
    $logged_info = $this->common_model->logged_info();
?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
    
            <div class="col-md-12">
    
                <form name="nameDialog" id="billing_contacts_form" class="form-sm" novalidate role="form">
    
                    <div class="form-group row gutter-10" style="margin-top: 25px;">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*Billing Email:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="email" class="form-control input-sm" name="crt_email" id="crt_email" required value="<?php echo ($logged_info['billing_contact']) ? $logged_info['billing_contact']['crt_email'] : ''?>">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">First Name:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="first_name" value="<?php echo ($logged_info['billing_contact']) ? $logged_info['billing_contact']['first_name'] : ''?>" id="first_name">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">Last Name:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="last_name" value="<?php echo ($logged_info['billing_contact']) ? $logged_info['billing_contact']['last_name'] : ''?>" id="last_name">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">Alternate Billing Email:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="alt_email" value="<?php echo ($logged_info['billing_contact']) ? $logged_info['billing_contact']['alt_email'] : ''?>" id="alt_email">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">Company:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="company" value="<?php echo ($logged_info['billing_contact']) ? $logged_info['billing_contact']['company'] : ''?>" id="company">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">CrisisFlo Billing Receipt:</p>
                        </div>
                        <div class="col-sm-12">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="receipt" value="<?php echo ($logged_info['billing_contact']) ? $logged_info['billing_contact']['receipt'] : ''?>" id="receipt">
                            </label>
                        </div>
                    </div>
    
                    
                    <div class="clearfix text-right">
                        <button type="button" onclick="activateSubscriptionTab('billing_details')" class="btn btn-default pull-left">
                            <i class="fa fa-angle-double-left"></i> Back
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Next
                        </button>
                    </div>
                </form>
                
    
    
            </div>
    
        </div>
    </div>
</div>
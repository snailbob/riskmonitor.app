<?php
    $countries = $this->common_model->countryformat();
    $billing_info = $this->common_model->get_billing_info();
?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
    
            <div class="col-md-12">
                <form id="billing_details_form" novalidate>
                    
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*Country:</p>
                        </div>
                        <div class="col-sm-12">
                            <select name="country" id="country" class="form-control input-sm" required>
                                <?php 
                                    foreach($countries as $r=>$value){
                                        echo '<option value="'.$value['country_id'].'"';
                                        echo ($this->session->userdata('country_id') == $value['country_id']) ? ' selected ' : '';
                                        echo " data-info='".json_encode($value)."' ";
                                        echo '>'.$value['short_name'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
    
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*Postal Code:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="address_zip" id="address_zip" required value="<?php echo ($billing_info) ? $billing_info['address_zip'] : ''?>">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*State:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="address_state" id="address_state"
                                required value="<?php echo ($billing_info) ? $billing_info['address_state'] : ''?>">
    
                        </div>
                    </div>
    
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*Address:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="address_line1" id="address_line1"
                                required value="<?php echo ($billing_info) ? $billing_info['address_line1'] : ''?>">
                        </div>
                    </div>
    
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*City:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="address_city" id="address_city" required value="<?php echo ($billing_info) ? $billing_info['address_city'] : ''?>">
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*Phone Number:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="phone" id="phone" required value="<?php echo ($billing_info) ? $billing_info['phone'] : ''?>">
                        </div>
                    </div>
                    
                    <div class="clearfix text-right">
                        <button type="button" onclick="activateSubscriptionTab('subscription')" class="btn btn-default pull-left">
                            <i class="fa fa-angle-double-left"></i> Back
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Next
                        </button>
                    </div>
                    
                </form>
            </div>
    
    
        </div>
    </div>
</div>
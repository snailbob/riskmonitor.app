<?php
    $billing_info = $this->common_model->get_billing_info();
    $selected_plan = ($billing_info) ? $billing_info['selected_plan'] : '0';
?>
<div class="paddingx">
    <div class="row">

        <div class="col-md-12">
            <div id="generic_price_table" style="margin-top: 0">
                <div class="container-fluidx">

                    <!--BLOCK ROW START-->
                    <div class="row sm-gutter">
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content clearfix">

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <h2>
                                        FREE
                                        <br>
                                        <small style="font-size: 45%">&nbsp;</small>
                                    </h2>

                                    <?php if($selected_plan == '0') {?>
                                        <div class="generic_price_btn clearfix">
                                            <a class="btn btn-primary btn-block disabled">CURRENT PLAN</a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="generic_price_btn clearfix">
                                            <a class="btn btn-primary btn-block" onclick="selectSubscription('0')">SIGN UP</a>
                                        </div>
                                    <?php } ?>

                                    <ul>
                                        <li>
                                            <span>Up to 3</span> Users
                                        </li>
                                        <li>
                                            <span>1</span> Open Case
                                        </li>
                                        <li>
                                            Group Messaging
                                        </li>
                                        <li>
                                            Wiki Space
                                        </li>
                                        <li>
                                            File Sharing up to 20 MB
                                        </li>
                                    </ul>
                                </div>
                                <!--//FEATURE LIST END-->


                            </div>
                            <!--//PRICE CONTENT END-->

                        </div>
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content generic_content_middle clearfix">

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <h2>
                                        AUD$49 / MONTH
                                        <br>
                                        <small style="font-size: 45%">plus GST (if applicable)</small>
                                    </h2>

                                    <?php if($selected_plan == '1') {?>
                                        <div class="generic_price_btn clearfix">
                                            <a class="btn btn-primary btn-block disabled">CURRENT PLAN</a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="generic_price_btn clearfix">
                                            <a class="btn btn-primary btn-block" onclick="selectSubscription('1')">SIGN UP</a>
                                        </div>
                                    <?php } ?>

                                    <ul>
                                        <li>
                                            <span>Up to 10</span> Users
                                        </li>
                                        <li>
                                            <span>Unlimited</span> Open Cases
                                        </li>
                                        <li>
                                            Group Messaging
                                        </li>
                                        <li>
                                            Wiki Space
                                        </li>
                                        <li>
                                            Video Conferencing
                                        </li>
                                        <li>
                                            Document Sharing
                                        </li>
                                        <li>
                                            Add Standby Templates
                                        </li>
                                        <li>
                                            Post Incident Review
                                        </li>
                                        <li>
                                            File Sharing up to 200 MB
                                        </li>
                                    </ul>
                                </div>
                                <!--//FEATURE LIST END-->

                            </div>
                            <!--//PRICE CONTENT END-->

                        </div>
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content clearfix">

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <h2>
                                        AUD$79 / MONTH
                                        <br>
                                        <small style="font-size: 45%">plus GST (if applicable)</small>
                                    </h2>

                                    <?php if($selected_plan == '2') {?>
                                        <div class="generic_price_btn clearfix">
                                            <a class="btn btn-primary btn-block disabled">CURRENT PLAN</a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="generic_price_btn clearfix">
                                            <a class="btn btn-primary btn-block" onclick="selectSubscription('2')">SIGN UP</a>
                                        </div>
                                    <?php } ?>

                                    <ul>
                                        <li>
                                            <span>Up to 30</span> Users
                                        </li>
                                        <li>
                                            <span>Unlimited</span> Open Cases
                                        </li>
                                        <li>
                                            Group Messaging
                                        </li>
                                        <li>
                                            Wiki Space
                                        </li>
                                        <li>
                                            Video Conferencing
                                        </li>
                                        <li>
                                            Document Sharing
                                        </li>
                                        <li>
                                            Add Standby Templates
                                        </li>
                                        <li>
                                            Post Incident Review
                                        </li>
                                        <li>
                                            File Sharing up to 500 MB
                                        </li>
                                    </ul>
                                </div>
                                <!--//FEATURE LIST END-->

                            </div>
                            <!--//PRICE CONTENT END-->

                        </div>

                    </div>
                    <!--//BLOCK ROW END-->

                </div>
            </div>

        </div>

    </div>
</div>
<?php
    $billing_info = $this->common_model->get_billing_info();
?>
<div class="panel panel-default">

    <div class="panel-body">
        <div class="row row-country">
            <div class="col-sm-12">
            <p class="lead">Is your organisation registered in Australia?</p>

            </div>
            <div class="col-md-4 col-md-offset-2">
                <button data-country="au" class="btn btn-default btn-block">
                    Yes
                </button>
            </div>
            <div class="col-md-4">
                <button data-country="os" class="btn btn-default btn-block">
                    No
                </button>
            </div>
        </div>


    
        <div class="row row-subscription hidden">
    
            <div class="col-md-12" style="margin-top: 15px;">
                <hr>
    
                <form id="subscription_payment_form" novalidate>
                    <input type="hidden" name="subscribe_country">
                    <div class="row">
                        <div class="col-sm-12">
                            <img style="margin-bottom: 10px" src="<?php echo base_url().'assets/img/cards/cards.png'?>"     class="pull-right" alt="">
                        </div>
                    </div>

                
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row gutter-10">
                                <div class="col-sm-12 text-left">
                                    <p class="form-control-staticx">*Card Type:</p>
                                </div>
                                <div class="col-sm-12">
                                    <select name="card_type" id="" class="form-control input-sm" required>
                                        <option value="MasterCard">MasterCard</option>
                                        <option value="Visa">Visa</option>
                                        <option value="Discover">Discover</option>
                                        <option value="American Express">American Express</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row gutter-10">

                                <div class="col-sm-12 text-left">
                                    <p class="form-control-staticx">*Card Number:</p>
                                </div>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" autocomplete="cc-number" name="number" id="number" required value="<?php echo ($billing_info) ? $billing_info['number'] : ''?>"> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row gutter-10">
                        <div class="col-sm-12 text-left">
                            <p class="form-control-staticx">*Name on Card:</p>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="name" id="name" required value="<?php echo ($billing_info) ? $billing_info['name'] : ''?>"> 
                        </div>
                    </div>
                    <div class="form-group row gutter-10">
                        <div class="row">
                            
                            <div class="col-xs-8 col-sm-4 text-left">
                                <p class="form-control-staticx">*Expiration Date:</p>
                            </div>
                            <div class="col-xs-4 text-left">
                                <p class="form-control-staticx">
                                    CCV* 
                                    <i class="fa fa-info-circle fa-fw text-info popover-template" data-container="body" data-placement="top" type="button" id="cvv" data-html="true"></i>
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-2">
                            <select name="exp_month" id="" class="form-control input-sm" required> 

                                <?php
                                    for($i = 1; $i <= 12; $i++){
                                        echo '<option value="'.sprintf("%08d", $i).'"';
                                        echo ($billing_info && $billing_info['exp_year']) ? 'selected' : '';
                                        echo '>'.sprintf("%02d", $i).'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-4 col-sm-2">
                            <select name="exp_year" id="" class="form-control input-sm" required> 
                                <?php
                                    for($i = 2018; $i < 2099; $i++){
                                        echo '<option value="'.$i.'"';
                                        echo ($billing_info && $billing_info['exp_year'] == $i) ? 'selected' : '';
                                        echo '>'.$i.'</option>';

                                    }
                                ?>
                            </select>
                        </div>
                        
                        <div class="col-xs-4 col-sm-2">
                            <div class="input-group input-group-trans">
                                <input type="text" class="form-control input-sm" name="cvc" placeholder="" aria-describedby="basic-addon2"
                                    required value="<?php echo ($billing_info) ? $billing_info['cvc'] : ''?>"> 
                            </div>
                        </div>
                    </div>
    
    
                    <div class="text-right">
                        <button type="button" onclick="activateSubscriptionTab('billing_contacts')" class="btn btn-default pull-left">
                            <i class="fa fa-angle-double-left"></i> Back
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
    
            </div>
    
        </div>
    </div>
</div>



<div id="popover-content-cvv" class="hide">

    <div>
        <h5>FINDING YOUR CARD VERIFICATION VALUE</h5>
        <p class="small">The card verification value (CVV) is used as a security precaution, insuring that you have possession of your card.</p>


        <p class="small">
        <img src="<?php echo base_url() ?>assets/img/cards/card1.jpg"  class="pull-left">

        For
            <b>MasterCard, Visa &amp; Discover</b> cards, the CVV is the last three digits printed on the black of your card in
            the signature panel.</p>

        <p class="small">
            <img src="<?php echo base_url() ?>assets/img/cards/card2.jpg" class="pull-left">

        The CVV for
            <b>American Express</b> cards is the four small number printed the front of the card, above the last few embossed digits.
        </p>

    </div>


</div>
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Edit Case

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Edit Case</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



				 <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>
                            
                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Edit Case</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-scenario" id="frm-add-scenario" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">
                            <label for="cida" class="col-sm-2 control-label">Case ID </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="gen_id" name="gen_id"
                                
                                	value="<?php
										echo $caseinfo[0]['gen_id'];
										
										 ?>" readonly="readonly" /><?php echo form_error('scenario_desc'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="case_title" class="col-sm-2 control-label">Case Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="case_title" name="case_title" placeholder="Enter case title" value="<?php echo $caseinfo[0]['case_title']; ?>"  /><?php echo form_error('case_title'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="case_det" class="col-sm-2 control-label">Case Details</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="case_det" rows="6" name="case_det" placeholder="Enter case details"><?php echo $caseinfo[0]['case_details']; ?></textarea><?php echo form_error('case_det'); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="crt_mem" class="col-sm-2 control-label">Assign To:</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="crt_mem" name="crt_mem" rows="6">
                                	<option value="">Select</option>
                                    
									<?php
                                    foreach($crt_list as $crts){ 
									?>
                                   
                                    <option value="<?php echo $crts['login_id']; ?>" <?php if ($caseinfo[0]['crt_id'] == $crts['login_id']) { echo 'selected="selected"'; }?>><?php echo $this->master_model->decryptIt($crts['crt_first_name']).' '.$this->master_model->decryptIt($crts['crt_last_name']); ?></option>
                                    
                                    <?php }
                                    
                                    ?>
                                
                                </select><?php echo form_error('crt_mem'); ?>
                            </div>
                        </div>

                       

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>cc/managecase">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_case" id="add_case">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




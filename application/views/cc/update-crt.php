<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





<div class="row">

    <div class="col-lg-12">

        <div class="page-title">

            <h1>Update Crisis Team Member Information



            </h1>

            <ol class="breadcrumb">

                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo base_url().'cc'?>">Dashboard</a>
                </li>

                <li class="active">Update Crisis Team Member Information</li>



            </ol>

        </div>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

<!-- end PAGE TITLE AREA -->









<!-- Form AREA -->

<div class="row">

    <div class="col-lg-12">

        <?php if($this->session->flashdata('success')!=""){ ?>

        <div class="alert alert-success alert-dismissable">

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            <?php echo $this->session->flashdata('success');   ?>

        </div>

        <?php } if($this->session->flashdata('error')!=""){ ?>
        <div class="alert alert-danger alert-dismissable">

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('error');   ?>
        </div>'

        <?php } ?>

    </div>

    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">

                <div class="panel-title">

                    <h4>Account Details</h4>

                </div>

                <div class="panel-widgets">

                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                </div>

                <div class="clearfix"></div>

            </div>

            <div id="validationExamples" class="panel-collapse collapse in">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">



                            <form action='' name="frm-update-crt" id="frm-update-crt" method='post' class="form-horizontal" role="form" validate>



                                <div class="form-group">

                                    <label for="firstname" class="col-sm-2 control-label">First Name</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="cct_firstname" name="cct_firstname" value="<?php echo $crt_record['first_name'];?>">
                                        <?php echo form_error('cct_firstname'); ?>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="cct_lastname" name="cct_lastname" value="<?php echo $crt_record['last_name'];?>">
                                        <?php echo form_error('cct_lastname'); ?>

                                    </div>

                                </div>



                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Position</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="crt_position" name="crt_position" placeholder="" required data-msg-required="Please enter phone number"
                                            value="<?php echo $crt_record['crt_position'];?>">
                                        <?php echo form_error('crt_position'); ?>

                                    </div>

                                </div>


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Floor/Suite/Apt</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="crisis_function" name="crisis_function" placeholder="" value="<?php echo $crt_record['crisis_function'];?>">
                                        <?php echo form_error('crisis_function'); ?>

                                    </div>

                                </div>



                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Location</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control geocomplete" id="crt_location" name="crt_location" value="<?php echo $crt_record['location'];?>"
                                        />
                                        <?php echo form_error('crt_location'); ?>
                                        <input name="country_short" id="country_short" type="hidden" value="">
                                        <input name="lat" id="lat" type="hidden" value="<?php echo $crt_record['location_lat'];?>">
                                        <input name="lng" id="lng" type="hidden" value="<?php echo $crt_record['location_lng'];?>">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Email Address</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="crt_email" name="crt_email" placeholder="" required data-msg-required="Please enter address"
                                            value="<?php echo $crt_record['crt_email'];?>" <?php echo ($crt_record[
                                            'crt_email']) ? 'readonly="readonly"' : ''; ?>>
                                        <?php echo form_error('cc_address'); ?>

                                    </div>

                                </div>


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Mobile Number</label>

                                    <div class="col-sm-4" style="margin-bottom: 6px;">
                                        <input type="text" class="form-control" id="countrycode" name="countrycode" placeholder="Country Code" readonly="readonly"
                                            value="<?php echo $crt_record['countrycode']; ?>" />
                                        <?php echo form_error('countrycode'); ?>

                                        <select class="form-control hidden" id="countrycodex" name="countrycodex">
                                            <option value="">Select country code..</option>
                                            <?php if(count($countriescode)!="0"){

                                    foreach($countriescode as $countries){


                                        echo '<option value="'.$countries['calling_code'].'"';

                                        if ($crt_record['countrycode'] == $countries['calling_code']){
                                            echo ' selected="selected"';
                                        }

                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';

                                    }
                                }

                                ?>
                                        </select>

                                    </div>
                                    <div class="col-sm-6">

                                        <input type="text" class="form-control" id="crt_no" name="crt_no" placeholder="Exclude initial 0" value="<?php echo $crt_record['crt_digits'];?>">
                                        <?php echo form_error('crt_digits'); ?>

                                    </div>

                                </div>



                                <div class="form-group hidden">

                                    <label class="col-sm-2 control-label">User Group</label>

                                    <div class="col-sm-10" style="margin-bottom: 6px;">
                                        <select class="form-control" id="group" name="group">
                                            <option value="" data-access="[]">Select..</option>
                                            <?php if(count($all_group)!="0"){

                                    foreach($all_group as $groups){


                                        echo '<option value="'.$groups['id'].'"';

                                        if ($groups['id'] == $crt_record['group_id']){
                                            echo ' selected="selected"';
                                        }

										$access = ($groups['access'] != '') ? unserialize($groups['access']) : array();
										echo ' data-access=\''.json_encode($access).'\'';


										echo '>'.$groups['group_name'].'</option>';

                                    }
                                }

                                ?>
                                        </select>
                                        <?php echo form_error('group'); ?>

                                    </div>


                                </div>


                                <?php
                                    $active_mod = substr($this->session->userdata('org_module'), 0, 1);

                                    $access = array(); //unserialize($crt_record['access']);

                                    if(!is_array($access)){
                                        $access = array();
                                    }

                                ?>
                                
                                <div class="form-group hidden <?php if($active_mod != '5' && $active_mod != '8') { echo 'hidden'; } ?>">
                                    <label class="col-sm-2 control-label">Access</label>

                                    <div class="col-sm-10" style="margin-bottom: 6px;">

                                        <div class="checkbox">
                                            <input type="checkbox" name="access[]" value="kpi" id="checkbox1" <?php if(in_array( 'kpi', $access)) { echo
                                                'checked="checked"';}?>>
                                            <label for="checkbox1">
                                                KPI Dashboard
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <input type="checkbox" name="access[]" value="cost" id="checkbox2" <?php if(in_array( 'cost', $access)) { echo
                                                'checked="checked"';}?>>
                                            <label for="checkbox2">
                                                Cost Monitor
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <input type="checkbox" name="access[]" value="issues" id="checkbox3" <?php if(in_array( 'issues', $access)) { echo
                                                'checked="checked"';}?>>
                                            <label for="checkbox3">
                                                Issues Board
                                            </label>
                                        </div>
                                    </div>

                                </div>





                                <div class="form-group hidden">

                                    <label class="col-sm-2 control-label">Crisis Function</label>

                                    <div class="col-sm-10">

                                        <textarea class="form-control" id="crt_func" rows="3" name="crt_func" placeholder="Enter member’s function during a crisis">
                                            <?php echo $crt_record['crisis_function'];?>
                                        </textarea>
                                        <?php echo form_error('crt_func'); ?>

                                    </div>

                                </div>

                                <div class="form-group hidden">

                                    <label class="col-sm-2 control-label">Alternate Member</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="alter_member" name="alter_member" placeholder="Enter name, title and contact details (mobile and email)."
                                            value="<?php echo $crt_record['alt_member'];?>">
                                        <?php echo form_error('alter_member'); ?>

                                    </div>

                                </div>




                                <div class="form-group">

                                    <label class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10 text-right">
                                        <a class="btn btn-default" href="<?php echo base_url()?>cc/crisisteam/managecrt">Back</a>
                                        <button type="submit" class="btn btn-primary" name="btn_add_crt_member" id="btn_add_crt_member">Submit</button>

                                    </div>

                                </div>





                            </form>

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>



</div>
<!--.row-->

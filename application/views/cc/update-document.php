                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Pre-Incident Phase



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Update Document User Group</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php

						}

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						}

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update Document User Group</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">


						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>




                        <div class="form-group">

                            <label class="col-sm-2 control-label">File Uploaded</label>

                            <div class="col-sm-10"  style="margin-top: 6px;">
								<span>

								<?php

                                $fileext = explode ('.',$the_doc[0]['file_upload_name']);
                                if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){

                                echo '<i class="fa fa-file-pdf-o text-red"></i> ';

                                }

                                else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){

                                echo '<i class="fa fa-file-word-o text-blue"></i> ';

                                }

                                else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){

                                echo '<i class="fa fa-file-text-o text-muted"></i> ';

                                }


                                 echo substr($the_doc[0]['file_upload_name'],13,50);?>

                            	</span>
                            </div>

                        </div>



                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="radios">Crisis Pack?</label>
                          <div class="col-sm-10">


                            <div class="btn-group" data-toggle="buttons">
                              <label class="btn btn-default <?php if ($the_doc[0]['c_pack'] == 'yes'){ echo 'active'; } ?>">
                                <input type="radio" name="c_pack" id="radios-0" value="yes" <?php if ($the_doc[0]['c_pack'] == 'yes'){ echo ' checked="checked"'; } ?>> Yes
                              </label>
                              <label class="btn btn-default <?php if ($the_doc[0]['c_pack'] == 'no'){ echo 'active'; } ?>">
                                <input type="radio" name="c_pack" id="radios-1" value="no" <?php if ($the_doc[0]['c_pack'] == 'no'){ echo ' checked="checked"'; } ?>> No
                              </label>

                            </div>

                              <?php echo form_error('c_pack'); ?>
                              <div class="hidden text-red" id="cpack_error"></div>
                          </div>
                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">User Group</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="group" name="group">
                                    <option value="">Select..</option>
                                    <option value="0"
                                        <?php if ($the_doc[0]['group_id'] == '0'){
                                            echo ' selected="selected"';
                                        } ?>
                                    >All</option>
                                <?php if(count($all_group) > 0){
                                    foreach($all_group as $groups){


                                        echo '<option value="'.$groups['id'].'"';

                                        if ($groups['id'] == $the_doc[0]['group_id']){
                                            echo ' selected="selected"';
                                        }

										echo '>'.$groups['group_name'].'</option>';

                                    }
                                }

                                ?>
                                </select><?php echo form_error('group'); ?>
                                <div class="hidden text-red" id="group_error"></div>
                            </div>


                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="category" name="category">
                                    <option value="">Select</option>
                                <?php

								if(count($categories) > 0){
                                    foreach($categories as $r=>$cat){

                                        echo '<option value="'.$cat['id'].'"';

										if($the_doc[0]['category_id'] == $cat['id']){
											echo 'selected="selected"';
										}
										echo '>'.$cat['name'].'</option>';

                                    }
                                }

                                ?>
                                </select><?php echo form_error('category'); ?>
                                <div class="hidden text-red" id="category_error"></div>

                            </div>


                        </div>




                        <!-- Multiple Radios (inline) -->
                        <div class="form-group hidden">
                          <label class="col-sm-2 control-label" for="radios">Right to edit</label>
                          <div class="col-sm-10">


                                <div class="btn-group">
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default <?php if ($the_doc[0]['rights_to_indi'] != '') { echo 'active';}?>" id="btn1_btn" onclick="$('#selectgroup').val('')">Individual</button>
                                  </div>
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default <?php if ($the_doc[0]['rights_to_group'] != '') { echo 'active';}?>" id="btn2_btn" onclick="$('#selectcrt').val('')">Group</button>
                                  </div>
                                </div>

                          </div>
                        </div>


                   <div class="hidden btn1_show" <?php if ($the_doc[0]['rights_to_indi'] == '') { echo 'style="display: none"';}?>>

                        <!-- Select Basic -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="selectcrt"> </label>
                          <div class="col-sm-10">
                            <select id="selectcrt" name="selectcrt" class="form-control">
                                <option value="">Select</option>
                                <?php if (count($crts)>0){

                                    foreach($crts as $r=>$value){

                                        echo '<option value="'.$value['login_id'].'"';

										 if($value['login_id'] == $the_doc[0]['rights_to_indi']){

										 	echo 'selected="selected"';
										 }
										echo '>'.$this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']).'</option>';
                                    }

                                }
                                ?>
                            </select><span class="selectcrt_error text-red"><?php echo form_error('selectcrt'); ?></span>

                          </div>
                        </div>

                    </div>

                   <div class="hidden btn2_show" <?php if ($the_doc[0]['rights_to_group'] == '') { echo 'style="display: none"';}?>>

                        <!-- Select Basic -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="selectgroup"> </label>
                          <div class="col-sm-10">
                            <select id="selectgroup" name="selectgroup" class="form-control">
                                <option value="">Select</option>
                                <?php if (count($all_group)>0){

                                    foreach($all_group as $r=>$value){

                                        echo '<option value="'.$value['id'].'" ';

										 if($value['id'] == $the_doc[0]['rights_to_group']){

										 	echo 'selected="selected"';
										 }
										echo ' >'.$value['group_name'].'</option>';
                                    }

                                }
                                ?>
                            </select><span class="selectcrt_error text-red"><?php echo form_error('selectgroup'); ?></span>

                          </div>
                        </div>

                    </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>cc/document/manage">Back</a>
                            <button type="submit" class="btn btn-primary" name="upload_doc" id="upload_doc">Submit</button>

                            </div>

						</div>





                                    </form>

                                    </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->

                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                           <h1><?php echo 'Edit User Group'; ?>



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>cc/">Dashboard</a></li>

                                <li class="active">Edit User Group</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Edit User Group</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">
                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">



						<form action='' name="frm-add-cc" id="" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="group_name" class="col-sm-2 control-label">User Group Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="group_name" name="group_name" placeholder="" data-msg-required="Please enter first name" value="<?php echo $group_record[0]['group_name']; ?>"><?php echo form_error('group_name'); ?>

                            </div>

                        </div>

						<?php
						$active_mod = substr($this->session->userdata('org_module'), 0, 1);
						$access = ($group_record[0]['access'] != '') ? unserialize($group_record[0]['access']) : array();
						 ?>
                        <div class="form-group <?php if($active_mod != '5' && $active_mod != '8') { echo 'hidden'; } ?>">
                            <label class="col-sm-2 control-label">Access</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">

                                  <div class="checkbox">
                                    <input type="checkbox" name="access[]" value="kpi" id="checkbox1" <?php echo (in_array('kpi', $access)) ? 'checked="checked"' : ''; ?>>
                                    <label for="checkbox1">
                                        KPI Dashboard
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <input type="checkbox" name="access[]" value="cost" id="checkbox2" <?php echo (in_array('cost', $access)) ? 'checked="checked"' : ''; ?>>
                                    <label for="checkbox2">
                                        Cost Monitor
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <input type="checkbox" name="access[]" value="issues" id="checkbox3" <?php echo (in_array('issues', $access)) ? 'checked="checked"' : ''; ?>>
                                    <label for="checkbox3">
                                        Issues Board
                                    </label>
                                  </div>
                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                                <a class="btn btn-default" href="<?php echo base_url()?>cc/usergroup/managegroup">Back</a>
                                <button type="submit" class="btn btn-primary" name="btn_add_group" id="btn_add_group">Submit</button>

                            </div>

                        </div>





                                    </form>

                                    </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->

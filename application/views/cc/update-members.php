                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Crisis Team Member Information



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Update Crisis Team Member Information</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update Member Information</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">


						<form action='' name="frm-update-crt" id="frm-update-crt" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cct_firstname" name="cct_firstname" value="<?php echo $this->master_model->decryptIt($crt_record[0]['first_name']);?>"><?php echo form_error('cct_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cct_lastname" name="cct_lastname"

							value="<?php echo $this->master_model->decryptIt($crt_record[0]['last_name']);?>"><?php echo form_error('cct_lastname'); ?>

                            </div>

                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label">Email Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="crt_email" name="crt_email" placeholder="" required data-msg-required="Please enter address" value="<?php echo $this->master_model->decryptIt($crt_record[0]['email_id']);?>" readonly="readonly"><?php echo form_error('cc_address'); ?>

                            </div>

                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>
                                <?php if(count($countriescode)!="0"){

                                    foreach($countriescode as $countries){


                                        echo '<option value="'.$countries['calling_code'].'"';

                                        if ($this->master_model->decryptIt($crt_record[0]['countrycode']) == $countries['calling_code']){
                                            echo ' selected="selected"';
                                        }

                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';

                                    }
                                }

                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="crt_no" name="crt_no" placeholder="" required  data-msg-required="Please enter state" value="<?php if ($crt_record[0]['digits'] != ""){echo $this->master_model->decryptIt($crt_record[0]['digits']);} ?>"><?php echo form_error('crt_digits'); ?>

                            </div>

                        </div>






                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                                <a class="btn btn-default" href="<?php echo base_url()?>cc/members/manage">Back</a>
                                <button type="submit" class="btn btn-primary" name="btn_add_crt_member" id="btn_add_crt_member">Submit</button>

                            </div>

                        </div>





                    </form>

                  </div>
                </div>

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->

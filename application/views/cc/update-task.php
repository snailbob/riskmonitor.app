                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Scenario Task

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Update Response Action Plan Task</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



			  <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                             <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update Response Action Plan Task</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-task" id="frm-add-task" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Scenario Description</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="scenario_desc" name="scenario_desc" placeholder="Enter scenario description" disabled="disabled"><?php echo $task_info[0]['scenario']; ?></textarea><?php echo form_error('scenario_desc'); ?>							

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Task</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="task_desc" name="task_desc" placeholder="Enter task description"><?php echo $task_info[0]['task']; ?></textarea><?php echo form_error('task_desc'); ?>							

                           	</div>

                        </div>
                        
                        <div class="form-group">

                            <label class="col-sm-2 control-label">Task Guidance</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="task_guide" name="task_guide" rows="7" placeholder="Enter any guidance or business rules that apply to this task (Optional)"><?php echo $task_info[0]['task_guide']; ?></textarea><?php echo form_error('task_guide'); ?>							

                           	</div>

                        </div>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Assign to</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="assign_to" id="assign_to">

                               	<option value=""> Select </option>

                                <?php

									if(count($crt_list) > 0)

									{

										foreach($crt_list as $crt)

										{

								?>

                                		<option value="<?php echo $crt['login_id'] ?>" <?php if($crt['login_id']==$task_info[0]['crt_id']) echo 'selected="selected"'; ?>>

                                        <?php echo $this->master_model->decryptIt($crt['crt_first_name']).' '.$this->master_model->decryptIt($crt['crt_last_name']); ?>

                                        </option>

                                <?php

										}

									}

                                ?>

                               </select><?php echo form_error('assign_to'); ?>	

                           	</div>

                        </div>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>cc/scenario/managetask">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_task" id="add_task">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




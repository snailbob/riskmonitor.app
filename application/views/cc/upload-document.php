                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->





                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Pre-Incident Phase



                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Upload Document</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->









                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <?php echo $this->session->flashdata('success'); ?></div>

                        <?php

						}

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						}

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Upload Document</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

                                  <div class="row">
                                    <div class="col-md-6 col-md-offset-3">


						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">File Input</label>

                            <div class="col-sm-10">

                                <input type="file" name="file_upload_name" id="file_upload_name" value=""/>

                                <p class="small text-muted">(.doc, .docx, .pdf, .txt )</p>

                                <?php echo form_error('file_upload_name'); ?>

                                <div class="hidden text-red" id="file_error"></div>

                            </div>

                        </div>






                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="radios">Email Pack? <i class="fa fa-question-circle text-info  tooltip-test" rel="tooltip" data-placement="right" title="Do you want this document to be automatically emailed to the recall team in the event an incident has been initiated? "></i></label>
                          <div class="col-sm-10">

                            <div class="btn-group" data-toggle="buttons">
                              <label class="btn btn-default">
                                <input type="radio" name="c_pack" id="radios-0" value="yes"> Yes
                              </label>
                              <label class="btn btn-default">
                                <input type="radio" name="c_pack" id="radios-1" value="no"> No
                              </label>

                            </div>

                              <?php echo form_error('c_pack'); ?>
                              <div class="hidden text-red" id="cpack_error"></div>
                          </div>
                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label">User Group <i class="fa fa-question-circle text-info  tooltip-test" rel="tooltip" data-placement="right" title="Select user group you give permission to access this document."></i>
</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="group" name="group">
                                    <option value="">Select</option>
                                    <option value="0">All</option>
                                <?php

								if(count($all_group) > 0){

                                    foreach($all_group as $groups){
                                        echo '<option value="'.$groups['id'].'">'.$groups['group_name'].'</option>';
                                    }
                                }

                                ?>
                                </select><?php echo form_error('group'); ?>
                                <div class="hidden text-red" id="group_error"></div>

                            </div>


                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="category" name="category">
                                    <option value="">Select</option>
                                <?php

								if(count($categories) > 0){
                                    foreach($categories as $r=>$cat){

                                        echo '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';

                                    }
                                }

                                ?>
                                </select><?php echo form_error('category'); ?>
                                <div class="hidden text-red" id="category_error"></div>

                            </div>


                        </div>



                        <!-- Multiple Radios (inline) -->
                        <div class="form-group hidden">
                          <label class="col-sm-2 control-label" for="radios">Edit Rights <i class="fa fa-question-circle text-info  tooltip-test" rel="tooltip" data-placement="right" title="Who do you want to give permission to edit this document? "></i></label>
                          <div class="col-sm-10">



                                <div class="btn-group">
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default" id="btn1_btn" onclick="$('#selectgroup').val('')">Individual</button>
                                  </div>
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default" id="btn2_btn" onclick="$('#selectcrt').val('')">Group</button>
                                  </div>
                                </div>

                          </div>
                        </div>


                   <div class="btn1_show" style="display: none">

                        <!-- Select Basic -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="selectcrt"> </label>
                          <div class="col-sm-10">
                            <select id="selectcrt" name="selectcrt" class="form-control">
                                <option value="">Select Response Team Member</option>
                                <?php if (count($crts)>0){

                                    foreach($crts as $r=>$value){

                                        echo '<option value="'.$value['login_id'].'">'.$this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']).'</option>';
                                    }

                                }
                                ?>
                            </select><span class="selectcrt_error text-red"><?php echo form_error('selectcrt'); ?></span>

                          </div>
                        </div>

                    </div>

                   <div class="btn2_show" style="display: none">

                        <!-- Select Basic -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="selectgroup"> </label>
                          <div class="col-sm-10">
                            <select id="selectgroup" name="selectgroup" class="form-control">
                                <option value="">Select Group</option>
                                <?php if (count($all_group)>0){

                                    foreach($all_group as $r=>$value){

                                        echo '<option value="'.$value['id'].'">'.$value['group_name'].'</option>';
                                    }

                                }
                                ?>
                            </select><span class="selectcrt_error text-red"><?php echo form_error('selectgroup'); ?></span>

                          </div>
                        </div>

                    </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>cc/document/manage">Back</a>
                            <button type="submit" class="btn btn-primary" name="upload_doc" id="upload_doc">Submit</button>

                            </div>

						</div>





                                    </form>

                                    </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->

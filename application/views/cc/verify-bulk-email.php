<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>CrisisFlo | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    
<script language="javascript" type="text/javascript">

// validation of verify stk password validation
$(document).ready(function(e) {
		$("#verify_crt").click(function()

		{ 	

			var crt_password=$("#crt_password");

			var confirm_crt_password=$("#confirm_crt_password");

			if ($('.password-verdict').html()!="Medium" && $('.password-verdict').html()!='Strong' && $('.password-verdict').html()!='Very Strong' ){
				
				$('.mediumerror').html('For security reasons, your password needs to be ‘Medium’ strength or above.');

				$('#crt_password').focus();
				return false;
			}
			
			else{
				$('.mediumerror').html('');
			}
			



			if(crt_password.val()=="")

            { 

			    crt_password.val('');

				$('#crt_password').attr('placeholder','Please Enter Password');

				$('#crt_password').removeClass('form-control');

				$('#crt_password').addClass('error_border');

				$('#crt_password').focus();

				return false;

			}

/*			if(crt_password.val().length<6)

            { 

			    crt_password.val('');

				$('#crt_password').attr('placeholder','Password Must Be Atleast 6 Character');

				$('#crt_password').removeClass('form-control');

				$('#crt_password').addClass('error_border');

				$('#crt_password').focus();

				return false;

			}*/

			else if(confirm_crt_password.val()=="")

            { 

			    confirm_crt_password.val('');

				$('#confirm_crt_password').attr('placeholder','Please Enter Confirm Password');

				$('#confirm_crt_password').removeClass('form-control');

				$('#confirm_crt_password').addClass('error_border');

				$('#confirm_crt_password').focus();

				return false;

			}

			else if(crt_password.val()!=confirm_crt_password.val())

            { 

			    confirm_crt_password.val('');

				$('#confirm_crt_password').attr('placeholder','Password does not match');

				$('#confirm_crt_password').removeClass('form-control');

				$('#confirm_crt_password').addClass('error_border');

				$('#confirm_crt_password').focus();

				return false;

			 }			

		});
});


</script>    
 
     <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->


</head>

<body style="background: #efefef;">
<?php
	
	//form validation global error style
	$this->form_validation->set_error_delimiters('<div class="text-red">', '</div>');


?>

    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">




                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Receipt Confirmation</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

						<?php if ($status == 'verified'){
						?>

                        <div class="alert alert-success">
                            <p class=""><i class="fa fa-check-circle"></i> Congratulations! You have successfully confirmed receipt. You may now close this window.</p>
                        </div>
                        
                        
                        <?php } else { ?>

                        <div class="alert alert-info">
                            <p class=""><i class="fa fa-info-circle"></i> Invalid link.</p>
                        </div>
                        <?php } ?>
						


                    </div><!--/.end of div panel-body-->

                </div>

            </div>

        </div>

    </div>



    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="text-muted">© 2015 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                viewports: {
                    progress: ".pwstrength_viewport_progress",
                    verdict: ".pwstrength_viewport_verdict"
                }
            };
            options.common = {
                onLoad: function () {
                    $('#messages').text('Start typing password');
                },
                zxcvbn: true
            };
            $(':password').pwstrength(options);
        });
    </script>

</body>

</html>
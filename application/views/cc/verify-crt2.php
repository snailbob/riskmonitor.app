<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>CrisisFlo | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    
     <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->


</head>

<body style="background: #efefef;">


    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">




                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>New Response Team Member Confirmation</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

						

						<?php 

						if($success!="" || $this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $success.$this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($error!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $error; ?></div>

                        <?php

						} 

						?>

						

						

						<?php 

						if(count($crt_info)>0 && $this->uri->segment(3)!="")

						{ 

						?>

						<?php if($crt_info[0]['user_status']==0){ ?>

                        <h4>Do you want to accept request to become Response Team member of <strong><?php echo $this->common_model->getorgname($crt_info[0]['org_id']);?></strong>?</h4><br/>

                        <?php } if($crt_info[0]['user_status']==1 && $this->session->flashdata('success')==""){ ?>

                        <h4>You have successfully confirmed your details. You can <a class="btn btn-primary" href="<?php echo base_url()?>signin">Sign in here</a></h4><br/>
</h4><br/>

                        <?php } ?>


                            <?php $cc_login_id = $crt_info[0]['login_id'];  ?>

                           

                        	<?php if($crt_info[0]['user_status']==0){ ?>

                      	<form id="confirm_form">


                            <div class="form-group">
                            
                            	<input type="hidden" name="crt_id" value="<?php echo $cc_login_id; ?>">
                                
                                <div class="btn-group" data-toggle="buttons">
                                  <label class="btn btn-default">
                                    <input type="radio" name="options" id="option2" value="1"> Yes
                                  </label>
                                  <label class="btn btn-default">
                                    <input type="radio" name="options" id="option3" value="0"> No
                                  </label>
                                </div>
                            </div>

                            <div class="form-group">

                                <button class="btn btn-primary">Confirm</button>

                            </div>

                        </form>

                        <?php 

							}

						}

						else

						{

						?>

                         <h4>Please check link, invalid link</h4>

                        <?php	

						}

						?>





                    </div>

                </div>

            </div>

        </div>

    </div>



    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="text-muted">© 2015 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>

    <script type="text/javascript">

	// validation of verify stk password validation
	$(document).ready(function(e) {
			$('#confirm_form').on('submit', function(){
				if($('[name="options"]:checked').length == 0){
					bootbox.alert('Please select your decision.');
				}
				
				else{
					$('#confirm_form').find('button').addClass('disabled').html('Saving..');
					$.post(
						base_url+'verification/crt2_confirm',
						$('#confirm_form').serialize(),
						function(res){
							console.log(res);
							$('.panel-body').html('<p class="text-center lead">Decision has been saved.</p>');
						}
					);
				}
				return false;
			});
	});
	

    </script>    
 

</body>

</html>
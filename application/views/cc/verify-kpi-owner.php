<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>CrisisFlo | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
	<link href="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/spinner/bootstrap-spinner.css" rel="stylesheet">
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">
    

<!-- END -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    
    
     <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->


</head>

<body style="background: #efefef;">
<?php
	
	//form validation global error style
	$this->form_validation->set_error_delimiters('<div class="text-red">', '</div>');


?>

    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">




                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>KPI Owner<?php if($status == 'allowed'){ echo ' - '. $bulk_data[0]['name']; } ?> </strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">
                    
                    
    <div class="row">
    
    	<?php
		if($status == 'allowed'){
			$kpi_type = $bulk_data[0]['kpi_type'];
			
			if($kpi_type != 'all'){
				$cols = '12';
			}
			else{
				$cols = '6';
			}
		}
		?>
        
        
        

<?php //filter the display 
if($status == 'allowed'){
    if($kpi_type == 'all' || $kpi_type == 'dist'){
?>
        <div class="col-md-<?php echo $cols; ?>">
			<?php

                    $kpi_cell = 'dist_data';
                    

                $whr_kpi = array(
                    'incident_id'=>$bulk_data[0]['incident_id'],
                    'incident_type'=>$bulk_data[0]['incident_type']
                );
                $kpi = $this->master_model->getRecords('bulk_kpi', $whr_kpi);

				//set values
				$total_unit = '';
				$unit_return = '';
				$u_disposed = '';
				$u_transformed = '';
				$u_corrections = 'NA';
				$the_kpi = 'NA';

                //check count of kpi
                if(count($kpi) > 0) {
                    $kpi_id = $kpi[0]['id'];
                    $dbcell = unserialize($bulk_data[0][$kpi_cell]);
                    if($dbcell != ''){ //get each kpi db cell data
                        $total_unit = $dbcell['total_unit'];
                        $unit_return = $dbcell['unit_return'];
                        $u_disposed = $dbcell['u_disposed'];
                        $u_transformed = $dbcell['u_transformed'];
                        $u_corrections = $dbcell['u_corrections'];
                        $the_kpi = round($dbcell['the_kpi'], 0).'%';
                    
                    }
				}
					

                ?>

                

                <div class="alert alert-info">
                    <p class=""><?php /*?><i class="fa fa-info-circle"></i> You, <?php echo $bulk_data[0]['name']; ?> are allowed to update <?php */?><i>
                    
                    <?php  echo 'Units in distribution'; ?></i> KPI details.</p>
                </div>

                <form id="owner_form" class="owner_form well">

                
                    <div class="form-group">
                    
                        <label>Total Units</label>
                        
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="total_unit" id="total_unit" value="<?php echo $total_unit; ?>" placeholder="Total Units"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                        
                        
                        <input class="form-control" type="hidden" name="owner_type" id="owner_type" value="<?php echo $kpi_cell; ?>"/>
                        <input class="form-control" type="hidden" name="owner_id" id="owner_id" value="<?php echo $bulk_data[0]['id']; ?>"/>
                        <input class="form-control" type="hidden" name="kpi_id" id="kpi_id" value="<?php if(count($kpi) > 0) { echo $kpi[0]['id']; }?>"/>
                        <input class="form-control" type="hidden" name="incident_id" id="incident_id" value="<?php echo $bulk_data[0]['incident_id']; ?>"/>
                        <input class="form-control" type="hidden" name="incident_type" id="incident_type" value="<?php echo $bulk_data[0]['incident_type']; ?>"/>
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group">
                        <label>Units Returned</label>
                        
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="unit_return" id="unit_return" value="<?php echo $unit_return;?>" placeholder="Units Returned"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                            
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group">
                        <label>Units Disposed</label>
                            
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="u_disposed" id="u_disposed" placeholder="Units Disposed" value="<?php echo $u_disposed;?>"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                        
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group">
                        <label>Units Transformed</label>
                        
                            
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="u_transformed" id="u_transformed"  placeholder="Units Transformed" value="<?php echo $u_transformed; ?>"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group <?php if($cols == '6') { echo 'hidden'; } ?>">
                        <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                    </div>
                </form>						
                            
                        
            
            
        
        
        
        
        </div><!--/col-md-6-->
    
    
    <?php } ?>

<?php } else { ?>
    <div class="col-md-12">
        <div class="alert alert-info">
            <p class=""><i class="fa fa-info-circle"></i> Invalid link.</p>
        </div>
    </div>
<?php } ?>
        
        
        
        
<?php //filter the display 
if($status == 'allowed'){
	if($kpi_type == 'all' || $kpi_type == 'mkt'){
?>
        <div class="col-md-<?php echo $cols; ?>">
			<?php 

                    $kpi_cell = 'market_data';
                    

                $whr_kpi = array(
                    'incident_id'=>$bulk_data[0]['incident_id'],
                    'incident_type'=>$bulk_data[0]['incident_type']
                );
                $kpi = $this->master_model->getRecords('bulk_kpi', $whr_kpi);


				//set values
				$total_unit = '';
				$unit_return = '';
				$u_disposed = '';
				$u_transformed = '';
				$u_corrections = 'NA';
				$the_kpi = 'NA';

                //check count of kpi
                if(count($kpi) > 0) {
                    $kpi_id = $kpi[0]['id'];
                    $dbcell = unserialize($bulk_data[0][$kpi_cell]);
                    if($dbcell != ''){ //get each kpi db cell data
                        $total_unit = $dbcell['total_unit'];
                        $unit_return = $dbcell['unit_return'];
                        $u_disposed = $dbcell['u_disposed'];
                        $u_transformed = $dbcell['u_transformed'];
                        $u_corrections = $dbcell['u_corrections'];
                        $the_kpi = round($dbcell['the_kpi'], 0).'%';
                    
                    }
                
                }											
                ?>
                
                <div class="alert alert-info">
                    <p class=""><?php /*?><i class="fa fa-info-circle"></i> You, <?php echo $bulk_data[0]['name']; ?> are allowed to update <?php */?><i>
                    
                    <?php  echo 'Units in consumer market'; ?></i> KPI details.</p>
                </div>

                <form id="owner_form" class="owner_form well">

                
                    <div class="form-group">

                        <label>Total Units</label>
                        
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="total_unit" id="total_unit" value="<?php echo $total_unit; ?>" placeholder="Total Units"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                        
                        
                        <input class="form-control" type="hidden" name="owner_type" id="owner_type" value="<?php echo $kpi_cell; ?>"/>
                        <input class="form-control" type="hidden" name="owner_id" id="owner_id" value="<?php echo $bulk_data[0]['id']; ?>"/>
                        <input class="form-control" type="hidden" name="kpi_id" id="kpi_id" value="<?php if(count($kpi) > 0) { echo $kpi[0]['id']; }?>"/>
                        <input class="form-control" type="hidden" name="incident_id" id="incident_id" value="<?php echo $bulk_data[0]['incident_id']; ?>"/>
                        <input class="form-control" type="hidden" name="incident_type" id="incident_type" value="<?php echo $bulk_data[0]['incident_type']; ?>"/>
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group">
                        <label>Units Returned</label>
                        
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="unit_return" id="unit_return" value="<?php echo $unit_return;?>" placeholder="Units Returned"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                            
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group">
                        <label>Units Disposed</label>
                            
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="u_disposed" id="u_disposed" placeholder="Units Disposed" value="<?php echo $u_disposed;?>"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                        
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group">
                        <label>Units Transformed</label>
                        
                            
                        <div class="input-group" data-trigger="spinner">
                          <a href="javascript:;" data-spin="down" class="input-group-addon"><i class="fa fa-minus"></i></a>
                            <input class="form-control input-lg" type="text" data-max="null" data-min="0" data-rule="quantity" name="u_transformed" id="u_transformed"  placeholder="Units Transformed" value="<?php echo $u_transformed; ?>"/>
                          <a href="javascript:;" data-spin="up" class="input-group-addon"><i class="fa fa-plus"></i></a>
                        </div>          
                        <span class="form-helper text-danger"></span>
                    </div>
                
                    <div class="form-group <?php if($cols == '6') { echo 'hidden'; } ?>">
                        <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                    </div>
                </form>						
                            
                            
                        

        
        
        </div><!--/col-md-6-->
        
		<?php } ?>
    
    <?php } else { ?>
        <div class="col-md-12">
            <div class="alert alert-info">
                <p class=""><i class="fa fa-info-circle"></i> Invalid link.</p>
            </div>
        </div>
    <?php } ?>
        
        

        <?php if($status == 'allowed'){ ?>
        
        <div class="col-md-12 text-center <?php if($cols == '12') { echo 'hidden'; } ?>">
            <button class="btn btn-primary btn-lg submit_all_form">Submit</button>
        </div>
		<?php } ?>
        
    </div><!--/row-->

                    


                    </div><!--/.end of div panel-body-->

                </div>

            </div>

        </div>

    </div>



    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="text-muted">© 2015 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/spinner/jquery.spinner.js"></script>
    
    
    <script type="text/javascript">
		$(document).ready(function(e) {
			$('input').keyup(function(event) {
		
			  // skip for arrow keys
			  if(event.which >= 37 && event.which <= 40){
				event.preventDefault();
			  }
			
			  $(this).val(function(index, value) {
				return value
				  .replace(/\D/g, "")
				  //.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
				  .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
				;
			  });
			});
			
//			$('a[data-spin="down"]').on('click', function(){
//				if(/^[,.]*$/.test($(this).siblings('input').val())){
//					console.log(this);
//					return false;
//				}
//			});
			

			
			//bind the the `keydown` event for the `document` object which will catch all `keydown` events that bubble up the DOM


			/*$('.owner_form').find('input').on('keydown', function (event) {
				var $self = $(this);
				//up-arrow (regular and num-pad)
				if (event.which == 38 || event.which == 104) {
			
					//make sure to use `parseInt()` so you can numerically add to the value rather than concocting a longer string
					$self.val((parseInt($self.val()) + 1));
			
				//down-arrow (regular and num-pad)
				} else if (event.which == 40 || event.which == 98) {
					$self.val((parseInt($self.val()) - 1));
				}
			});	*/		
			

//			('.owner_form').find('input').spinner('enable');
			
			//validate owner form
			$('.owner_form').find('input[type="text"]').on('keyup', function(){
				var len = [];
				var $self = $(this);
				$self.find('input[type="text"]').each(function(index, obj){
					if($(this).val() == ''){
						len[index] = 'aw';
					}
				});
				console.log(len.length);
				if(len.length > 0){
					$self.find('button').attr('disabled','disabled');
				}else{
					$self.find('button').removeAttr('disabled');
				}
			});
			
			
			$('.submit_all_form').on('click', function(){
				$('.owner_form').each(function(index, element) {
                    $(element).submit();
					console.log('form submitted');
                });
			});
			
			
			$('.owner_form').on('submit', function(){
				var $self = $(this);
				$self.find('span.form-helper').hide();
				console.log($(this).serialize());
				//return false;
				$.post(
					base_url+'verification/kpiupdate',
					$self.serialize(),
					function(response){
						console.log(response);
						//window.location.reload(true);
						if(response.type == 'greater'){
							bootbox.alert('<p class="lead">'+response.message+'</p>');
						}
						else if(response.type == 'invalid_total'){
							bootbox.alert('<p class="lead">'+response.message+'</p>');
						}
						else{
							//$self.find('button').attr('disabled','disabled');
							bootbox.alert('<p class="lead"><i class="fa fa-check-circle text-success"></i> '+response.kpi_type+' Saved!<br><small>Total Corrections: '+response.u_corrections+'<br>KPIs: '+response.the_kpi+'%</small></p>');
							
							$('.submit_all_form').attr('disabled','disabled').html('Changes Saved!');
							
							setTimeout( function(){
								$('.submit_all_form').removeAttr('disabled').html('Update');
							},
							2000);
							
							<?php /*?><?php if($cols == '12') { //both ?>
							<?php } else {?>
							<?php } ?><?php */?>
							
						}
								
						
					},
					"json"
				
				).error( function(){
					bootbox.alert('Something went wrong. Please try again later.');
				});
				

				return false;
			});
		
        });


    </script>

</body>

</html>
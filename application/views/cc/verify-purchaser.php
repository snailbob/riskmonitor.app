<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>CrisisFlo | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->
    
	<style>
		.panel .login-heading {
			padding: 15px;
		}
		.login-banner {
			margin: 50px 0;
			color: #fff;
		}

		.panel-default {
			border: none;
		}
		.panel-default>.panel-heading {
			color: #4d5055;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
			border: none;
		}
		.panel-default, .panel-default>.panel-heading {
			border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
			border-color: #3374b2;
			color: #fff;
			background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
			border-color: #3374b2;
			color: #fff;
			background-color: #3374b2;
		}
	</style>
    
     <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->


</head>

<body style="background: #efefef;">




    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">




                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Create Recall Coordinator's Account</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

						<?php 

						if(count($purchase_data) > 0 && $this->uri->segment(3) != "") { 
							$purchaser = unserialize($purchase_data['content']);
							
//							print_r($purchaser);
//							foreach($purchaser as $r){
//								echo $r['value'];
//								echo '<br>';
//							}

							if($purchase_data['status'] == 'N'){
							?>
                            


                            <form id="trial_form" class="text-left">
                                <div class="alert trial_message alert-dismissable" style="display: none;">
                                    Form successfully submitted.
                                </div>   
                        
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input type="text" class="form-control" name="first_name"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" class="form-control" name="last_name"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Business Email</label>
                                    <input type="text" class="form-control" name="email"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Business Address</label>
                                    <input type="text" class="form-control user_geolocation" placeholder="Enter location" name="address" value="<?php echo $purchaser[2]['value'] ?>"/>
                                    <input type="hidden" class="form-control" name="lat" value="<?php echo $purchaser[4]['value'] ?>"/>
                                    <input type="hidden" class="form-control" name="lng" value="<?php echo $purchaser[5]['value'] ?>"/>
                                    
                                    <input type="hidden" class="form-control" name="subs_plan_id" value="<?php echo $purchaser[12]['value']?>"/>

                                    <input type="hidden" class="form-control" name="stripe_cust_id" value="<?php echo $purchase_data['stripe_cust_id']; ?>"/>
                                    <input type="hidden" class="form-control" name="purchaser_id" value="<?php echo $purchase_data['id']; ?>"/>
                                    <input type="hidden" class="form-control" name="gplus_id" value=""/>
                                    <input type="hidden" class="form-control" name="linkedin_id" value=""/>

                                </div> 
                                     
                                <div class="form-group">
                                    <label class="control-label">Organization/Business Name</label>
                                    <input type="text" class="form-control" name="organization" value="<?php echo $purchaser[2]['value']; ?>"/>
                                </div> 
                                


                                <div class="row">
                                
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label>
                                            <select class="form-control" id="signup_ccode" name="country_code">
                                                <option value="" data-code="">Select..</option>
                                                <?php
                                                    foreach($mobile_code as $r=>$mc){
                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
														if($purchaser[7]['value'] == $mc['country_id']){
															echo ' selected="selected"';
														}
														echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-7">
                                        <div class="form-group mobile_input">
                                            <label class="control-label">Mobile Number</label>
                                            <div class="input-group">
                                              <span class="input-group-addon" id="addon-shortcode">+</span>
                                              <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="<?php echo $purchaser[8]['value']; ?>">
                                            </div>
                                            <input type="hidden" name="country_short" value=""/>
                                        </div>
                                    </div>
                                </div>
                                


                                
                                             
                                <div class="form-group hidden">
                                    <label class="control-label">Subscription Plan</label>
                                    <input type="hidden" name="plan" value="<?php echo $purchaser[13]['value']?>"/>
                                </div> 
                                
                                          
                                             
                                <div class="form-group">
                                    <button class="btn btn-primary btn-rounded">Signup</button>
                                </div> 

                                             
                            </form>
                            
                            
                            
                            <?php
							
							}//not created cc
							
							else{ ?>
                            
                                 <div class="alert alert-info"> You have successfully created Coordinator's account.</div>
                            
                            
                            <?php
								
							
							}//have created cc

						}

						else {

						?>

                         <div class="alert alert-info"> Your link is invalid. Please check your email or contact admin.</div>

                        <?php	

						}

						?>





                    </div>

                </div>

            </div>

        </div>

    </div>



    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="text-muted">© 2015 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.geocomplete/jquery.geocomplete.js"></script>
    
    <script src="<?php echo base_url().'assets/2/js/plugins/validate/jquery.validate.min.js' ?>"></script>
    <script src="<?php echo base_url().'assets/2/js/single_page_validations/script.js' ?>"></script>
    
    

</body>

</html>
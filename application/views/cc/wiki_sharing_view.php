


<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller ng-cloak hidden" ng-controller="wikiController as $ctrl">

    <div class="row">

        <div class=" col-contents2" ng-class="{'col-xs-12' : currentPage.edit, 'col-lg-12' : !currentPage.edit }">
        
          <div ng-if="!showDocSharing">
            <div class="scroll-content">

              <div class="hidden">
                <input type="file" id="docShare" nv-file-select="" uploader="uploader" />
              </div>
              <h2>
                <div class="pull-right text-right">
                  <a name="" id="" class="btn btn-lg" role="button">
                    <i class="fa fa-upload fa-2x" ng-click="uploadShare()" aria-hidden="true"></i>
                  </a>
                </div>
                Document Sharing
              </h2>

              <div class="file-upload-table" ng-if="uploadTable">
              
                <table class="table">
                    <thead>
                        <tr>
                            <th width="75%">Name</th>
                            <th ng-show="uploader.isHTML5">Size</th>
                            <th ng-show="uploader.isHTML5">Progress</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in uploader.queue">
                            <td><strong>{{ item.file.name }}</strong></td>
                            <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                            <td ng-show="uploader.isHTML5">
                                <div class="progress" style="margin-bottom: 0;">
                                    <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                </div>
                            </td>

                            <td nowrap>
                                <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                    <span class="glyphicon glyphicon-upload"></span> Upload
                                </button>
                                <!-- <button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
                                    <span class="glyphicon glyphicon-ban-circle"></span> Cancel
                                </button> -->
                                <button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                    <span class="glyphicon glyphicon-trash"></span> Remove
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div style="padding-top: 15px;">
                    <a name="" id="" class="btn btn-default"  role="button" ng-click="showUploadTable(false)">
                      <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to List
                    </a>
                </div>


              </div>

              <div class="files-list" ng-if="!uploadTable">


                <div class="row">
                  <div class="col-md-4 col-lg-3">
                    <div class="panel panel-default">
                      <div class="panel-body">
                        <h5>File Space</h5>
                        <ul class="list-unstyled">
                          <li>This Project uses: <strong class="pull-right">{{totalFileSize/1024/1024|number:2}}MB ({{fileAverage | number: 2}}%)</strong></li>
                          <li>Free space: <strong class="pull-right">{{remainingSpace/1024/1024|number:2}}MB</strong> </li>
                        </ul>

                        <uib-progressbar class="progress-striped" value="fileAverage" type="warning">{{fileAverage | number: 2}}%</uib-progressbar>

                      </div>
                    </div>
                  </div>

                  <div class="col-md-8 col-lg-9">
                    <p class="text-center text-muted" ng-if="!files.length" style="margin-top: 140px;">
                        Nothing to show you.
                    </p>

                    <table class="table table-hover" ng-if="files.length">
                      <thead>
                        <tr>
                          <th width="70%">File Name</th>
                          <th width="15%">Uploaded by</th>
                          <th width="15%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="file in files track by $index">
                          <td>
                            <a ng-href="{{file.link}}" target="_blank">
                              {{file.file_name}}
                            </a>
                          </td>
                          <td>
                            {{file.author.full_name}}
                          </td>
                          <td class="text-right">
                              <!-- Single button -->
                              <div class="btn-group  pull-right" uib-dropdown is-open="status.isopen">
                                <button id="single-button" type="button" class="btn btn-default" uib-dropdown-toggle>
                                  <i class="fa fa-ellipsis-v"></i>
                                </button>
                                <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                                  <li role="menuitem"><a ng-href="{{file.link}}" download target="_self">Download</a></li>
                                  <li role="menuitem" ng-if="myInfo.login_id == file.author.login_id"><a ng-click="deleteFile(file, $index)">Delete</a></li>
                                </ul>
                              </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>


              </div>
            </div>
          </div>
          <!-- showDocSharing = true -->

        </div>
    </div><!--.row -->


    <script type="text/ng-template" id="myModalContent.html">

        <div class="modal-body" id="modal-body">
            {{$ctrl.items[0]}}
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">OK</button>
            <button class="btn btn-default" type="button" ng-click="$ctrl.cancel()">Cancel</button>
        </div>
    </script>

    <script type="text/ng-template" id="modalConfirm.html">

      <div class="modal-header">
        <h3>{{modalOptions.headerText}}</h3>
      </div>
      <div class="modal-body">
        <p>{{modalOptions.bodyText}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" 
                data-ng-click="modalOptions.close()">{{modalOptions.closeButtonText}}</button>
        <button class="btn btn-primary" 
                data-ng-click="modalOptions.ok();">{{modalOptions.actionButtonText}}</button>
      </div>

    </script>

  </div>
</div>
  
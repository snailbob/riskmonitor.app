


<div class="all-task-content" ng-app="crisisApp">

  <div class="div-controller ng-cloak hidden" ng-controller="wikiController as $ctrl">

    <div class="row">


        <div class="col-lg-2 col-md-3 col-headers" ng-if="!currentPage.edit">

          <div class="scroll-content">
            <h4>
              <strong>Wiki Space</strong>

              <!-- <span class="fa-stack fa-lg">
                <i class="fa fa-square fa-stack-2x text-primary"></i>
                <i class="fa fa-folder fa-stack-1x fa-inverse"></i>
              </span> -->
            </h4>
            <hr>

            <!-- <div class="list-group">
              <a class="list-group-item list-group-item-action" ng-class="{'active' : showDocSharing}" ng-click="toggleDocSharing(true)">
                <i class="fa fa-file" aria-hidden="true"></i> Document Sharing
              </a>
            </div> -->

            <h5>PAGE TREE</h5>

            <p class="text-muted" ng-if="!pages.length">empty</p>

            <ul class="list-unstyled list-pages" ng-click="toggleDocSharing(false)" ui-sortable="sortOptions" ng-model="pages">
              <li as-sortable-item ng-class="{ 'active': currIndex == $index }" ng-repeat="page in pages track by $index" ng-init="PageIndex = $index">
                <p ng-click="activePage($index, page)">
                  <img src="<?php echo base_url().'assets/img/drag_handleRectArrows.png'?>">
                  <a ng-href="#{{page.id}}"> {{page.title}}</a>
                </p>
                <!-- <ul class="list-unstyled" uib-collapse="!page.isCollapsed" style="margin-left: 15px;">
                  <li ng-repeat="subpage in page.subpages track by $index" ng-click="activePage(PageIndex, subpage, $index)">
                    <p>
                      <a ng-class="{'text-muted': currSubIndex == $index && PageIndex == currIndex}">{{subpage.title}}</a>
                    </p>
                  </li>
                </ul> -->
              </li>
            </ul>

            <!-- <div class="list-group" ng-click="toggleDocSharing(false)">
              <a class="list-group-item list-group-item-action" ng-class="{ 'active': currIndex == $index }" ng-repeat="page in pages track by $index" ng-click="activePage($index, page)">
                <input type="text" class="form-control form-invisible" ng-model="page.title" name="" id="" aria-describedby="helpId" placeholder="Add Title">
              </a>
            </div> -->



             <div class="header-actions" ng-if="!showDocSharing">
              <a name="" id="" class="btn btn-default btn-sm" role="button" ng-click="addPage()">
                <i class="fa fa-plus" aria-hidden="true"></i> Page
              </a>
              <!-- <a name="" id="" class="btn btn-default btn-sm" role="button" ng-click="addSubPage()">
                <i class="fa fa-plusf" aria-hidden="true"></i> Sub-page
              </a> -->
              <!-- <button type="button" name="" id="" class="btn btn-default btn-sm" role="button" ng-click="deletePage()" ng-class="{'disabled' : !pages.length}" ng-if="currentPage.author.login_id == myInfo.login_id">
                <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
              </button> -->
            </div> 
          </div>

        </div>
        <div class=" col-contents2" ng-class="{'col-xs-12' : currentPage.edit, 'col-lg-10 col-md-9' : !currentPage.edit }">
        
          <div ng-if="showDocSharing">
            <div class="scroll-content">

              <div class="hidden">
                <input type="file" id="docShare" nv-file-select="" uploader="uploader" />
              </div>
              <h1>
                <div class="pull-right text-right">
                  <a name="" id="" class="btn" role="button">
                    <i class="fa fa-upload fa-2x" ng-click="uploadShare()" aria-hidden="true"></i>
                  </a>
                </div>
                Document Sharing
              </h1>


              <div class="file-upload-table" ng-if="uploadTable">
              
                <table class="table">
                    <thead>
                        <tr>
                            <th width="75%">Name</th>
                            <th ng-show="uploader.isHTML5">Size</th>
                            <th ng-show="uploader.isHTML5">Progress</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in uploader.queue">
                            <td><strong>{{ item.file.name }}</strong></td>
                            <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                            <td ng-show="uploader.isHTML5">
                                <div class="progress" style="margin-bottom: 0;">
                                    <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                </div>
                            </td>
                            <td class="text-center">
                                <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                                <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                            </td>
                            <td nowrap>
                                <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                    <span class="glyphicon glyphicon-upload"></span> Upload
                                </button>
                                <!-- <button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
                                    <span class="glyphicon glyphicon-ban-circle"></span> Cancel
                                </button> -->
                                <button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                    <span class="glyphicon glyphicon-trash"></span> Remove
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div style="padding-top: 15px;">
                    <!-- <div>
                        Queue progress:
                        <div class="progress" style="">
                            <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                        </div>
                    </div> -->

                    <a name="" id="" class="btn btn-default"  role="button" ng-click="showUploadTable(false)">
                      <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to List
                    </a>
                    <!-- <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                        <span class="glyphicon glyphicon-upload"></span> Upload all
                    </button>
                    <button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
                        <span class="glyphicon glyphicon-ban-circle"></span> Cancel all
                    </button>
                    <button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
                        <span class="glyphicon glyphicon-trash"></span> Remove all
                    </button> -->
                </div>


              </div>

              <div class="files-list" ng-if="!uploadTable">
                <p class="text-center text-muted" ng-if="!files.length" style="margin-top: 140px;">
                    Nothing to show you.
                </p>

                <table class="table table-hover" ng-if="files.length">
                  <thead>
                    <tr>
                      <th width="70%">File Name</th>
                      <th width="15%">Uploaded by</th>
                      <th width="15%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="file in files track by $index">
                      <td>
                        <a ng-href="{{file.link}}" target="_blank">
                          {{file.file_name}}
                        </a>
                      </td>
                      <td>
                          {{file.author.full_name}}
                      </td>
                      <td>
                        <a ng-href="{{file.link}}" download target="_self">Download</a>
                        <span ng-if="myInfo.login_id == file.author.login_id">&middot; <a ng-click="deleteFile(file, $index)">Delete</a></span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- showDocSharing = true -->



          <div ng-if="!showDocSharing">
            <div class="scroll-content" ng-if="currentPage.showHistory">


              <h1>
                <div class="pull-right text-right">
                  <a name="" id="" ng-click="currentPage.showHistory = false" class="btn btn-lg" role="button">
                    <i class="fa fa-caret-left" aria-hidden="true"></i> View Page
                  </a>
                </div>
                Page History
              </h1>

              <table class="table" style="margin-top: 30px">
                <thead>
                  <tr>
                    <th>Version</th>
                    <th>Publish</th>
                    <th>Change By</th>
                    <th>Comment</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td scope="row"> <a ng-click="showCurrent()">CURRENT</a> (v{{currentPage.histories.length + 1}})</td>
                    <td> {{currentPage.created_at_time | date: 'mediumDate'}} </td>
                    <td> {{currentPage.author.full_name}} </td>
                    <td> {{currentPage.comments.length}}</td>
                    <td></td>
                  </tr>
                  <tr ng-repeat="history in currentPage.histories track by $index">
                    <td scope="row"> <a ng-click="viewHistory(history)">v. {{currentPage.histories.length - $index}}</a> </td>
                    <td> {{history.created_at_time | date: 'mediumDate'}} </td>
                    <td> {{history.author.full_name}} </td>
                    <td> {{history.comments.length}}</td>
                    <td> <a ng-click="restoreHistory(history)">Restore</a> &middot; <a ng-click="deleteHistory(history, $index)">Delete</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- showHistory -->

            <div class="scroll-content" ng-if="!currentPage.showHistory">
                <div ng-if="currentPage && !currentPage.edit">
                  <div class="pull-right text-right" ng-if="!currentPage.isHistory">

                      <a name="" id="" ng-click="currentPage.edit = true" class="btn" role="button" ng-if="currentPage.author.login_id == myInfo.login_id">
                        <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                      </a>

                      <a name="" id="" ng-click="currentPage.showHistory = true" class="btn" role="button" ng-if="currentPage.author.login_id == myInfo.login_id">
                        <i class="fa fa-history fa-2x" aria-hidden="true"></i>
                      </a>
                      <a class="btn" role="button" ng-click="deletePage()" ng-if="currentPage.author.login_id == myInfo.login_id">
                        <i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>
                      </a>
                  </div>
                  <h1>
                    {{currentPage.title}}
                  </h1>


                  <!-- <div class="media">
                    <div class="media-left">
                      <a>
                        <img class="media-object img-circle" ng-src="{{currentPage.author.avatar}}" alt="..." width="45">
                      </a>
                    </div>
                    <div class="media-body">
                      <p>
                        <strong>{{currentPage.author.full_name}}</strong> <br>
                        <small>{{currentPage.created_at | date: 'mediumDate'}}</small>
                      </p>
                    </div>
                  </div> -->

                  <div style="padding: 30px; padding-bottom: 0" ng-if="currentPage.isHistory">
                    <div class="alert alert-warning" role="alert">
                      <p class="lead">
                        <i class="fa fa-warning" aria-hidden="true"></i> You are viewing an old version of this page.

                      </p>
                      <p>
                        <a ng-click="restoreHistory()">Restore</a> &middot;
                        <a ng-click="viewPageHistory()">View Page History</a>
                      </p>
                    </div>
                  </div>



                  <div ng-bind-html="htmlSafe(currentPage.content)" style="padding: 30px;"></div>

                  <table class="table table-hover" ng-if="currentPage.comments.length">
                    <thead>
                      <tr>
                        <th>
                          {{currentPage.comments.length}} comment(s)
                        </th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr ng-repeat="comment in currentPage.comments track by $index">
                        <td>
                            <div class="media">
                              <div class="media-left">
                                <a>
                                  <img class="media-object img-circle" ng-src="{{comment.author.avatar}}" alt="..." width="45">
                                </a>
                              </div>
                              <div class="media-body">
                                <p>
                                  <b>{{comment.author.full_name}}</b><br>
                                  <small>{{comment.created_at | date: 'mediumDate'}}</small></p>
                                <p>{{comment.message}}</p>
                              </div>
                            </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="media" ng-if="currentPage.id">
                    <div class="media-left">
                      <a>
                        <img class="media-object img-circle" ng-src="{{currentPage.author.avatar}}" alt="..." width="45">
                      </a>
                    </div>
                    <div class="media-body">

                      <div class="form-group">
                        <textarea class="form-control" ng-model="currentPage.theComment" placeholder="Write a comment" name="theComment" rows="3"></textarea>
                      </div>
                      <div class="form-group text-right">
                        <button type="button" class="btn btn-success" role="button" ng-class="{'disabled' : !currentPage.theComment || currentPage.commentSubmitting }" ng-click="saveComment(currentPage.theComment)">
                          <span ng-if="!currentPage.commentSubmitting">Send</span>
                          <span ng-if="currentPage.commentSubmitting">
                            <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Sending..
                          </span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>


                <div ng-if="currentPage.edit">
                  <div class="form-group" style="margin: 30px 0;">
                      <input type="text" class="form-control form-invisible-lg input-limit-char" data-length="100" ng-model="currentPage.title" placeholder="Add Title">
                      <span class="small pull-right">100 characters remaining.</span>

                  </div>

                  <!-- <div class="form-group">
                    <label for="">Parent Page <a ng-if="currentPage.subparent_id" ng-click="resetParent()">Reset</a></label>
                    <select name="" id="" class="form-control" ng-options="option as option.title for option in parentPages" ng-model="currentPage.subparent" ng-change="changeParent(currentPage.subparent)">
                    </select>
                  </div> -->
                  <summernote ng-model="currentPage.content" config="optionsSummer"></summernote>


                  <div class="row gutter-10">
                    <div class="col-md-4 col-md-offset-6">
                      <input type="text" name="" class="form-control input-sm" ng-model="currentPage.comment" ng-if="currentPage.id" placeholder="What's changed?" id="">

                    </div>
                    <div class="col-md-1">
                    
                      <div class="form-group">
                        <button type="button" class="btn btn-success btn-block" role="button" ng-class="{'disabled' : currentPage.publishSubmitting }" ng-click="publishPage(currentPage)">
                          <span ng-if="!currentPage.publishSubmitting">Publish</span>
                          <span ng-if="currentPage.publishSubmitting">
                            <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Publishing..
                          </span>
                        </button>
                      </div>
                    </div>

                    <div class="col-md-1">

                      <div class="form-group">
                        <button type="button" class="btn btn-default btn-block" role="button" ng-click="currentPage.edit = false">
                          Close
                        </button>
                      </div>
                    </div>
                  </div>  
                </div>

            </div>
          </div>

        </div>
    </div><!--.row -->
    

    <script type="text/ng-template" id="selectParentModal.html">

        <div class="modal-header">
            <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">
                Select Parent Page
            </h4>
        </div>
        <div class="modal-body" id="modal-body">
          <div class="form-group">
            <label for="">Parent Page <a ng-if="data.subparent_id" ng-click="resetParent()">Reset</a></label>
            <select name="" id="" class="form-control" ng-options="option as option.title for option in pages" ng-model="data.subparent" ng-change="changeParent(data.subparent)">
            </select>
          </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
          <button type="button" class="btn btn-primary" ng-click="save()">Select</button>
        </div>
    </script>

    <script type="text/ng-template" id="myModalContent.html">

        <div class="modal-body" id="modal-body">
            {{$ctrl.items[0]}}
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">OK</button>
            <button class="btn btn-default" type="button" ng-click="$ctrl.cancel()">Cancel</button>
        </div>
    </script>

    <script type="text/ng-template" id="modalConfirm.html">

      <div class="modal-header">
        <h3>{{modalOptions.headerText}}</h3>
      </div>
      <div class="modal-body">
        <p>{{modalOptions.bodyText}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" 
                data-ng-click="modalOptions.close()">{{modalOptions.closeButtonText}}</button>
        <button class="btn btn-primary" 
                data-ng-click="modalOptions.ok();">{{modalOptions.actionButtonText}}</button>
      </div>

    </script>

  </div>
</div>
  
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CrisisFlo<?php // |  echo $page_title; ?></title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?> 
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->


        <style>
            audio,video {
				width: 100%;
				vertical-align:top;
			}
		
			table {
				width: 100%;
			}
			.experiment {
				border-radius: .2em;
			}



									
        </style>
        <!-- Meeting.js library -->
        <script src="<?php echo base_url()?>assets/2/js/plugins/webrtc/meeting.js"> </script>
        
        <!-- scripts used for screen-sharing -->
        <script src='<?php echo base_url()?>assets/2/js/plugins/webrtc/firebase.js'> </script>
        <script src="<?php echo base_url()?>assets/2/js/plugins/webrtc/screen.js"></script>
                
		<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    
    
    
</head>

<body>
	<?php //form validation global error style
		 $this->form_validation->set_error_delimiters('<div class="text-red">', '</div>');
	 ?>
    <nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-xs preloadThis" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="202" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="161" height="48" style="margin-top: -15px;"></a>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 hidden-xs" >
                <h3 class="big text-primary" style="text-align: center">Team Video Conferencing</h3>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 visible-xs" >
                <h4 class="big text-primary" style="text-align: center">Team Video Conferencing</h4>
            </div>
            
        </div>
        <!-- /.container -->
    </nav>
    
    <div class="container main_container" style="margin-top: 0">

 
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1> Video Conferencing <span class="small" id="number-of-participants"></span>
                            </h1>



                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->




				<hr style="margin-top: 5px; margin-bottom: 15px;">
                    

                <div class="row">

                    <div class="col-md-2 col-sm-3 col-xs-12" style="border-right: 1px solid #eee;">
                    	<h4>Peers Online</h4>
                        <ul class="list-unstyled" id="results">
                        	<span class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading..</span>
                        	
                        </ul>
                        
                        
                    	<h4 style="padding-top: 10px;">Peers Joined</h4>
                        
                        <div id="joinedpeers">
                            <span class="text-muted"><i class="fa fa-refresh fa-spin"></i> Loading..</span>
                        </div>
                        
                        
						<!--screen share-->
                    	<h4 style="padding-top: 10px;" id="show_sscreen">
                        	<span id="span_unhide_sscreen">
                            	<button class="btn btn-sm btn-primary btn-block hidden" id="unhide_sscreen">View Shared Screen</button>
                            </span>
                            <a class="btn btn-sm btn-primary btn-block hidden" id="modal_ss_btn" href="#" data-toggle="modal" data-target="#mySharedvid">View Shared Screen</a>
                            </h4>
                        
<!-- Modal for shared vid -->
<div class="modal fade" id="mySharedvid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Prerequisite</h4>
      </div>
      <div class="modal-body" id="sharedModalBody">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                            
                        
                        
                        <div id="shr_btn" style="border-bottom: 1px solid #eee;">
                            <button class="btn btn-primary btn-sm btn-block setup" id="share-screen">Share Your Screen</button>
                            <p class="small text-center"><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-info-circle"></i> Prerequisite</a></p>
                            
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Prerequisite</h4>
      </div>
      <div class="modal-body">
      
        <ol>
          <li>Type this in Chrome URL: <strong class="text-info"> chrome://flags/ </strong></li>
          <li>Find 'Enable Screen Capture support in getUserMedia() and enable it. (refer to the image below)</li>
          <li>Relaunch browser and start using web screen sharing.</li>
        </ol>
        <p class="text-center"><img src="<?php echo base_url().'assets/2/img/google-chrome-enable-screen-capture-support.png'; ?>" class="img-responsive thumbnail center-block" /></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                            
                            
                        </div>

                        <div id="rooms-list">
                        </div>

                    
                    </div>
    					
                    <div class="col-md-10 col-sm-9 col-xs-12">
    					




            
                        <!-- just copy this <section> and next script -->
                        <section class="experiment">
                        
                            
                            <div class="row <?php if ($this->session->userdata('logged_cc_login_id')==''){ echo 'hidden'; } ?>" style="margin-bottom: 15px;">
                                <div class="col-sm-7 col-md-8">
                                    <input type="text" class="form-control" id="meeting-name" placeholder="Add title to this conference" autofocus>
                                </div>					
                                <div class="col-sm-5 col-md-4">
                                    <button class="form-control btn btn-primary" id="setup-meeting">Setup New Conference</button>
            
                                    <button class="form-control btn btn-danger hidden" id="stop-meeting">Stop Conference</button>
                                </div>					
                            </div>
                            
                            
                            
                            <?php if ($this->session->userdata('logged_crt_login_id')!=''){ ?>
                            <div class="row" style="margin-bottom: 15px;">
                                <div class="col-sm-12" id="meetings-list"></div>
                            </div>
                            <?php } ?>
                            
                            
                            
                            <div class="row" style="margin-bottom: 15px;">
                                <div class="col-sm-6" style="padding-left: 15px">
                                    <div class="thumbnail">
                                        <p class="text-center">You!</p>
                                        <div id="local-streams-container"></div>
                                        <div id="conf_log">
            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="background: transparent;">
                                    <div class="thumbnail">
                                        <p class="text-center">Remote Peers</p>
                                        <div id="remote-streams-container"></div>
                                    </div>
                                </div>
                            </div>
                        </section>
            
            
                        <!-- just copy this <section> and next script -->
                        <section class="experiment">                
                            <section>
                                <span class="hidden">
                                    Private ?? <a href="/screen-sharing/" target="_blank" title="Open this link for private screen sharing!"><code><strong id="unique-token">#123456789</strong></code></a>
                                </span>
                                <input type="text" id="user-name" placeholder="Your Name" hidden="" value="<?php echo $this->session->userdata('logged_display_name'); ?>">
                                <button id="share-screen" class="setup hidden">Share Your Screen</button>
                            </section>
                            
                            <!-- list of all available broadcasting rooms -->
                            <table style="width: 100%;" id="rooms-list"></table>
                            
                            <!-- local/remote videos container -->
                            <div class="col-md-12 thumbnail">
                            	<p class="text-center">Shared Screen</p>
                                <div id="videos-container">
                                	<div id="shared_scr_label"></div>
                                </div>
                            </div>
                        </section>
        
        
                    </div>
    

                </div><!--.row -->

					



            </div>

        </div>

    </div>
    <!-- /.container -->


    <!-- <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-muted">© 2014 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div> -->
    <!-- /.container -->

        <div class="chat-collapse pull-left" id="show_chat" style="display: none;" onClick="$('#page-wrap').show();$('#show_chat').hide();$('#sendie').focus();">
           <b>Show Conversation<button type="button" class="close pull-right" title="Expand Chat"> <i class="fa fa-plus-square-o"></i></button></b>
        </div>
        <div id="page-wrap">
			<p style="padding-right:5px;">
            <span class="pull-left small text-muted" id="count_online" style="margin-left: 5px;"></span>
            <button type="button" class="close" title="Collapse Chat" onClick="$('#page-wrap').hide();$('#show_chat').show();"> <i class="fa fa-minus-square-o"></i></button></p><div style="clear: both;"></div>
            <p id="name-area"></p>
            
            <div id="chat-wrap"><div id="chat-area">
            	<h4 class="text-muted text-center lead hidden">You're disconnected.</h4>
            </div></div>
            
            <form id="send-message-area" class="form-horizontal">
                <textarea rows="1" id="sendie" maxlength = "600"></textarea>
                <!--<span style="position: absolute; bottom: 1px; right: 1px; background:#eee; padding:10px;"><i class="fa fa-send"></i></span>-->
            </form>
        
        </div>

    

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/chat.js"></script>

           <script>
		   
		   		/***********************vidcon***************************/
		   
		   		$('.navbar-toggle').hide();
                var meeting = new Meeting();

                var meetingsList = document.getElementById('meetings-list');
                var meetingRooms = {};
				
				
				
				// display no open conf
				function noConf () {
					if ($('#meetings-list').is(':empty')){
						var meetingsList = document.getElementById('meetings-list');
						var tr = document.createElement('h4');
						tr.innerHTML = '<span class="lead text-muted">No open conference.</span>';
	
						meetingsList.insertBefore(tr, meetingsList.firstChild);
					  
					}
				
				}

	
				
                meeting.onmeeting = function (room) {
                    if (meetingRooms[room.roomid]) return;
                    meetingRooms[room.roomid] = room;

                    var tr = document.createElement('p');
                    tr.innerHTML = '<span class="lead text-muted" style="margin-top: 5px;">' + room.roomid + 
                        ' - <button id="join_btn" class="join btn btn-primary btn-md">Join</button></span>';

                    meetingsList.insertBefore(tr, meetingsList.firstChild);
					
					$('#meetings-list h4').addClass("hidden");
					
                    // when someone clicks table-row; joining the relevant meeting room
                    document.getElementById('join_btn').onclick = function () {
                        room = meetingRooms[room.roomid];

                        // manually joining a meeting room
                        if (room) meeting.meet(room);
						
							meetingsList.style.display = 'none';
							//var tr = document.createElement('tr');
							//tr.innerHTML = '<td> No Open Conference</td>';
							
                    };
					
					
                };
					// call no open conf
					noConf();


                var remoteMediaStreams = document.getElementById('remote-streams-container');
                var localMediaStream = document.getElementById('local-streams-container');

                // on getting media stream
                meeting.onaddstream = function (e) {
                    if (e.type == 'local') {
						
						localMediaStream.appendChild(e.video);
						
						<?php if ($this->session->userdata('crt_selected_orgnaization') != "") { ?>
							var time_joined = new Date().toISOString().slice(0, 19).replace('T', ' ');
							//store data of peers joined
							$.ajax({
							  type: "POST",
							  url: "<?php echo base_url().'conference/peersdata'; ?>",
							  data: { crt_id: "<?php echo $this->session->userdata('logged_crt_login_id'); ?>", cc_id: "<?php echo $this->session->userdata('team_cc'); ?>", org_id: "<?php echo $this->session->userdata('crt_selected_orgnaization'); ?>", name: "<?php echo $this->session->userdata('logged_display_name'); ?>", time_joined: time_joined}
							});
						<?php } ?>

					}
                    if (e.type == 'remote'){
						
						remoteMediaStreams.insertBefore(e.video, remoteMediaStreams.firstChild);
						
							var log_container = document.getElementById('conf_log');
					
						<?php if ($this->session->userdata('cc_selected_orgnaization') != "") { ?>
							var numvids = $('#remote-streams-container').children().length;
		
							//store number of peers joined
							$.ajax({
							  type: "POST",
							  url: "<?php echo base_url().'conference/add_peers'; ?>",
							  data: { peer: numvids, org_id: "<?php echo $this->session->userdata('cc_selected_orgnaization'); ?>"}
							});
						
						<?php } ?>
						
	
						var currentdate = new Date(); 
						var datetime = //	 currentdate.getDate() + "/"
										//+ (currentdate.getMonth()+1)  + "/" 
										//+ currentdate.getFullYear() + " @ "  
										+ currentdate.getHours() + ":"  
										+ currentdate.getMinutes() + ":" 
										+ currentdate.getSeconds();
	
	
						var loggg = document.createElement('p');
						//loggg.innerHTML = '<span class="text-muted">A peer joined the conference.</span><span class="pull-right text-muted">'+ datetime + '</span>';
	
						log_container.insertBefore(loggg, log_container.firstChild);
					
					 }
					
				};

				meeting.openSignalingChannel = function(onmessage) {
					var channel = location.href.replace(/\/|:|#|%|\.|\[|\]/g, '');
					var websocket = new WebSocket('wss://wsnodejs.nodejitsu.com:443');
					websocket.onopen = function () {
						websocket.push(JSON.stringify({
							open: true,
							channel: channel
						}));
					};
					websocket.push = websocket.send;
					websocket.send = function (data) {
						if(websocket.readyState != 1) {
							return setTimeout(function() {
								websocket.send(data);
							}, 300);
						}

						websocket.push(JSON.stringify({
							data: data,
							channel: channel
						}));
					};
					websocket.onmessage = function(e) {
						onmessage(JSON.parse(e.data));
					};
					return websocket;
				};

                // using firebase for signaling
                // meeting.firebase = 'muazkh';

                // if someone leaves; just remove his video
                meeting.onuserleft = function (userid) {
                    var video = document.getElementById(userid);
					
                    if (video) video.parentNode.removeChild(video);

					var log_container = document.getElementById('conf_log');

					var currentdate = new Date(); 
					var datetime = //	 currentdate.getDate() + "/"
								//	+ (currentdate.getMonth()+1)  + "/" 
								// +	 currentdate.getFullYear() + " @ "  
									+ currentdate.getHours() + ":"  
									+ currentdate.getMinutes() + ":" 
									+ currentdate.getSeconds();

                    var loggg = document.createElement('p');
                   // loggg.innerHTML = '<span class="text-muted"> A peer leaves the conference.</span><span class="pull-right text-muted">'+ datetime +'</span>';

                    log_container.insertBefore(loggg, log_container.firstChild);

					
                };

                // check pre-created meeting rooms
                meeting.check();

                document.getElementById('setup-meeting').onclick = function () {
                    // setup new meeting room
                    var meetingRoomName = document.getElementById('meeting-name').value || 'Simple Meeting';
                    meeting.setup(meetingRoomName);
					
					var setup_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
					
					
					//store data about the pre-created meeting
					$.ajax({
					  type: "POST",
					  
					  url: "<?php echo base_url().'conference/store_meeting'; ?>",
					  
					  data: { cc_id: "<?php echo $this->session->userdata('logged_cc_login_id'); ?>", org_id: "<?php echo $this->session->userdata('cc_selected_orgnaization'); ?>", meeting_name: meetingRoomName, setup_date: setup_date,peer: "0"}
					  
					});

					$('#setup-meeting').addClass('hidden');
					$('#stop-meeting').removeClass('hidden');
                  //  this.disabled = true;
                   // this.parentNode.innerHTML = '<h2><a href="' + location.href + '" target="_blank">Share this link</a></h2>';
                };
				
				//when user click stop meeting button
				$('#stop-meeting').click(function() {

					var close_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
				
					//store end of session of cc
					$.ajax({
					  type: "POST",
					  
					  url: "<?php echo base_url().'conference/stop_meeting'; ?>",
					  
					  data: { cc_id: "<?php echo $this->session->userdata('logged_cc_login_id'); ?>", org_id: "<?php echo $this->session->userdata('cc_selected_orgnaization'); ?>", end_date: close_date},
					  
					  success: function() {
							window.location.reload(true);
							$('#joinedpeers').html('<span class="text-muted">Conferencing stopped.</span>');
						}
					  
					});
					
					console.log('time lapse saved..');
					
					//location.reload();
				});
				
		
		
				//when user close window
				window.onunload = function(){
					
					var close_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
				
					//store end of session of cc
					$.ajax({
					  type: "POST",
					  
					  url: "<?php echo base_url().'conference/stop_meeting'; ?>",
					  
					  data: { cc_id: "<?php echo $this->session->userdata('logged_cc_login_id'); ?>", org_id: "<?php echo $this->session->userdata('cc_selected_orgnaization'); ?>", end_date: close_date},
					  
					  success: function() {
							window.location.reload(true);
						}
					  
					});
				};
						
						
				$(document).ready(function(e) {
					
					//refresh list of online
					setInterval(function(){
						
						$.ajax({
							url: "<?php echo base_url().'conference/onlines'; ?>",
							cache: false,
							success: function(data){
								if (data != ''){
									$("#results").html(data);
								}
								else{
									$('#results').html('<span class="text-muted">No online users.</span>');
								}
								
							}
						});
						
						
						//number of online
						var count = $("#results").children('li').length;

						if (count > 1){
							$('#count_online').html(count + ' peers online');
						}
						else{
							$('#count_online').html(count + ' peers online');
						}
						
						
					}, 5000);
					
					
					
					//refresh list of joined peers
					setInterval(function(){
						
						$.ajax({
						  url: "<?php echo base_url().'conference/joined'; ?>",
						  cache: false,
						  success: function(peers){
							$("#joinedpeers").html(peers);
							
							if (peers == '<span class="text-muted"><i class="fa fa-warning"></i> Something went wrong. Please check your session.</span>'){
								$('#sendie').attr('disabled','disabled');
								$('#chat-area h4').removeClass('hidden');
								$('#chat-area p').hide();
								$('#count_online').hide();
								$('#join_btn').addClass('disabled');
								$('#setup-meeting').addClass('disabled');
								$('#share-screen').addClass('disabled');
								
							}
							else{
								$('#sendie').removeAttr('disabled','disabled');
								$('#join_btn').removeClass('disabled');
								$('#setup-meeting').removeClass('disabled');
								$('#count_online').show();
								$('#chat-area h4').addClass('hidden');
								$('#chat-area p').show();
								$('#share-screen').removeClass('disabled');
										
							}
							
						  }
						});
						
						
					}, 5000);
					
					
					
						//count online
					setInterval(function(){
						
						//chat label
						if ($('#chat-area').children().length < 2){
							$('#chat-area').html('<h3 class="text-center text-muted lead">Chat with your online peers.</h3>');
						}
						else{
							$('#chat-area h3').addClass('hidden');
						}
								
						if ($('#videos-container video#self').length == '1'){
							
							$('#videos-container video#self').hide();
							$('#videos-container div#shared_scr_label').html('<p class="small text-center">Your screen is being shared.</p>');
							$('#unhide_sscreen').removeClass('hidden');

						}
				
						
					}, 2000);
					
					
					
					//unhide vid
					$('#unhide_sscreen').on( "click", function(){
						
						$('#mySharedvid').modal('show');
						//clone vid for popup
						$('#videos-container video#self').clone().appendTo( "#sharedModalBody" );
						$('#sharedModalBody').find('video').show();
						
						
						$('#span_unhide_sscreen').hide();	

						$('#modal_ss_btn').removeClass('hidden');

						
					});
						
                });
				
				
				
			/*chat*/

				var name = "<?php echo $this->session->userdata('logged_display_name'); ?>";
				var org = "<?php echo $this->uri->segment(4); ?>";
				var time_sent = "<?php echo date("F j, Y, g:i a"); ?>";
			// default name is 'Guest'
			if (!name || name === ' ') {
			   name = "Guest";	
			}
			
			// strip tags
			name = name.replace(/(<([^>]+)>)/ig,"");
			
			// display name on page
			//$("#name-area").html("You are: <span>" + name + "</span>");
			
			// kick off chat
			var chat =  new Chat();
			$(function() {
			
				 chat.getState(); 
				 
				 // watch textarea for key presses
				 $("#sendie").keydown(function(event) {  
				 
					 var key = event.which;  
			   
					 //all keys including return.  
					 if (key >= 33) {
					   
						 var maxLength = $(this).attr("maxlength");  
						 var length = this.value.length;  
						 
						 // don't allow new content if length is maxed out
						 if (length >= maxLength) {  
							 event.preventDefault();  
						 }  
					  }  
																																																				});
				 // watch textarea for release of key press
				 $('#sendie').keyup(function(e) {	
									 
					  if (e.keyCode == 13) { 
					  
						var text = $(this).val();
						var maxLength = $(this).attr("maxlength");  
						var length = text.length; 
						 
						// send 
						if (length <= maxLength + 1) { 
						 
							chat.send(text, name, time_sent, org);	
							$(this).val("");
							
						} else {
						
							$(this).val(text.substring(0, maxLength));
							
						}	
						
						
					  }
				 });
				
			});
				//reload chat every 1 second
				$(document).ready(function(e) {
					setInterval('chat.update()', 700);
                });	
				//var x = document.cookie;
				//console.log(x);
            </script>




            <!-----------------------------------screen share----------------------------------->
            <script>
                // Muaz Khan     - https://github.com/muaz-khan
                // MIT License   - https://www.webrtc-experiment.com/licence/
                // Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/screen-sharing
                
                var videosContainer = document.getElementById("videos-container") || document.body;
                var roomsList = document.getElementById('rooms-list');
                
                var screen = new Screen();
                
                screen.onscreen = function(_screen) {
                    var alreadyExist = document.getElementById(_screen.userid);
                    if (alreadyExist) return;

                    if (typeof roomsList === 'undefined') roomsList = document.body;

                    var tr = document.createElement('span');
                    
                    tr.id = _screen.userid;
                    tr.innerHTML = '<button class="join btn btn-primary btn-sm btn-block text-center" style="margin-top: 5px;" title=" Preview ' + _screen.userid+ ' Screen">' + _screen.userid+ '</button>';
                    roomsList.insertBefore(tr, roomsList.firstChild);

                    var button = tr.querySelector('.join');
                    button.setAttribute('data-userid', _screen.userid);
                    button.setAttribute('data-roomid', _screen.roomid);
                    button.onclick = function() {
                        var button = this;
                        button.disabled = true;
                        
                        var _screen = {
                            userid: button.getAttribute('data-userid'),
                            roomid: button.getAttribute('data-roomid')
                        };
                        screen.view(_screen);
                    };
                };

                // on getting each new screen
                screen.onaddstream = function(media) {
                    media.video.id = media.userid;
                    
                    var video = media.video;
                    video.setAttribute('controls', true);
                    videosContainer.insertBefore(video, videosContainer.firstChild);
                    video.play();
                    rotateVideo(video);
                };

                // using firebase for signaling
                // screen.firebase = 'signaling';

                // if someone leaves; just remove his screen
                screen.onuserleft = function(userid) {
                    var video = document.getElementById(userid);
					
					//$('#number-of-participants').hide();
                    if (video && video.parentNode){
						 video.parentNode.removeChild(video);
					
						//$('#videos-container video').hide();
						$('#videos-container div#shared_scr_label').html('<p class="small text-center">Screen sharing stopped.</p>');
						
						$('#span_unhide_sscreen').show();						
						$('#unhide_sscreen').addClass('hidden');
						$('#modal_ss_btn').addClass('hidden');
						
						$('#videos-container').find('video').remove();

						document.getElementById('share-screen').disabled = false;
					}
                };

                // check pre-shared screens
                screen.check();

                document.getElementById('share-screen').onclick = function() {
                    screen.share();
                    this.disabled = true;
					
                };
                
                document.getElementById('share-screen').onclick = function() {
                    var username = document.getElementById('user-name');
                    username.disabled = this.disabled = true;
                    
                    screen.isModerator = true;
                    screen.userid = username.value;
                    
                    screen.share();
					
                };
                
				
				
				
                function rotateVideo(video) {
                    video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
                    setTimeout(function() {
                        video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
                    }, 1000);
                }

                (function() {
                    var uniqueToken = document.getElementById('unique-token');
                    if (uniqueToken)
                        if (location.hash.length > 2) uniqueToken.parentNode.parentNode.parentNode.innerHTML = '<h2 style="text-align:center;"><a href="' + location.href + '" target="_blank">Share this link</a></h2>';
                        else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-');
                })();
                
                screen.onNumberOfParticipantsChnaged = function(numberOfParticipants) {
                    if(!screen.isModerator) return;
                    
                    //document.title = numberOfParticipants + ' users are viewing your screen!';
                    var element = document.getElementById('number-of-participants');
                    if (element) {
                        element.innerHTML = numberOfParticipants + ' users are viewing your screen!';	
					}
                };
            </script><!--./screen share-->
	
</body>

</html>

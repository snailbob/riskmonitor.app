<?php if(!isset($_SESSION['logged_reseller'])){redirect(base_url().'consultant');} ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CrisisFlo | Consultant's Portal</title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?> 

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
   
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">


	<link href="<?php echo base_url()?>assets/2/css/plugins/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">

    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->


	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    
    
</head>

<body>

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="padding-bottom: 20px;">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-xs preloadThis" href="<?php echo base_url()?>consultant/portal"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="202" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url()?>consultant/portal"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="161" height="48" style="margin-top: -15px;"></a>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 col-sm-6 col-sm-offset-3 hidden-xs" >
                <h3 class="big text-primary" style="text-align: center">Consultant's Portal</h3>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 visible-xs" >
                <h4 class="big text-primary" style="text-align: center">Consultant's Portal</h4>
            </div>

			<?php /*GET THE DATA FROM THE SESSION**************************/

				//active case report count
				$num_report=$this->master_model->getRecordCount('case_master',array('crt_id'=>$this->session->userdata('logged_crt_login_id'),'cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'0'));
    			
				$reseller_info = $this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));
				$tabs = $reseller_info[0]['deactivated_tab'];
				
				$single_tab = explode(' ',$tabs);

				$res_fname = $_SESSION['rfirst_name'];
				$res_lname = $_SESSION['rlast_name'];
	
	
				$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>'); 

            /*********************************************************/?>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#"></a>
                    </li>
		<?php if (in_array("portal", $single_tab)){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url() ?>reseller" class="visible-xs"><i class="fa fa-bullseye text-muted"></i> Portal</a></li>
		<?php } ?>
		<?php if (in_array("activated", $single_tab)){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>consultant/customer/activated" class="visible-xs"><i class="fa fa-star"></i> Activated Customer</a></li>
                   <li class="page-scroll hidden"> <a href="<?php echo base_url()?>consultant/customer/manage" class="visible-xs"><i class="fa fa-user"></i> Manage CC</a></li>

                   <li class="page-scroll hidden"> <a href="<?php echo base_url()?>consultant/organization/manage" class="visible-xs"><i class="fa fa-building-o"></i> Manage Organization</a></li>
		<?php } ?>
		<?php if (in_array("request", $single_tab)){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>consultant/customer/pending" class="visible-xs"><i class="fa fa-star-o"></i> Request Activation</a></li>
		<?php } ?>
		<?php if (in_array("recall", $single_tab)){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>consultant/recall" class="visible-xs"><i class="fa fa-history"></i> Recall Tasks</a></li>
		<?php } ?>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>consultant/simulation" class="visible-xs"><i class="fa fa-chain"></i> Simulation</a></li>
                   
                    <li class="dropdown hidden-sm hidden-xs">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>consultant/settings/changepassword"><i class="fa fa-key"></i> Change Password</a></li>
                        <li><a href="<?php echo base_url() ?>consultant/settings/updateinfo"><i class="fa fa-info"></i> Update Info</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>consultant/portal/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                        
                        
                      </ul>
                    </li>  
                    
                    <li class="visible-xs"><a href="<?php echo base_url(); ?>consultant/settings/changepassword"><i class="fa fa-key"></i> Change Password</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>consultant/settings/updateinfo"><i class="fa fa-info"></i> Update Info</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url();?>consultant/portal/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                    
                    
                    

                    
                    <li class="dropdown dropdown-large visible-sm"><!--menu for small devices-->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
                        
                        <ul class="dropdown-menu dropdown-menu-large row">
                            <li class="col-sm-6">
                                <ul>
		<?php if (in_array("portal", $single_tab)){ ?>
                                       <li> <a href="<?php echo base_url(); ?>consultant" class="dropdown-header preloadThis"><i class="fa fa-bullseye text-muted"></i> Portal</a></li>

                                    <li class="divider"></li>
        <?php } ?>  
                                    <li style="visibility: hidden">Portal Deactivated</li>
		<?php if (in_array("activated", $single_tab)){ ?>
                                       <li> <a href="<?php echo base_url(); ?>consultant/customer/activated" class="dropdown-header preloadThis"><i class="fa fa-star"></i> Activated Customer</a></li>
                                        <li><a href="<?php echo base_url(); ?>consultant/customer/manage" class="preloadThis hidden">Manage CC</a></li>
                                        <li><a href="<?php echo base_url(); ?>consultant/organization/manage" class="preloadThis hidden">Manage Organization</a></li>
        <?php } ?>    
                                   
                                </ul>
                            </li>
                            
                            
                            
                            <li class="col-sm-6">
                                <ul>
                                    <li style="visibility: hidden">Portal Deactivated</li>
		<?php if (in_array("request", $single_tab)){ ?>
                                   <li> <a href="<?php echo base_url(); ?>consultant/customer/pending" class="dropdown-header preloadThis"><i class="fa fa-star-o"></i> Request Activation</a></li>
                                    <li class="divider"></li>
        <?php } ?>    
		<?php if (in_array("recall", $single_tab)){ ?>
                                   <li> <a href="<?php echo base_url(); ?>consultant/recall" class="dropdown-header preloadThis"><i class="fa fa-history"></i> Recall Tasks</a></li>
                                    <li class="divider"></li>
        <?php } ?>    
                                   <li> <a href="<?php echo base_url(); ?>consultant/simulation" class="dropdown-header preloadThis"><i class="fa fa-chain"></i> Simulation</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header"><i class="fa fa-gear"></i> Settings</li>

                                    <li><a href="<?php echo base_url(); ?>consultant/settings/changePassword" class="preloadThis">Change Password</a></li>
                                    <li><a href="<?php echo base_url() ?>consultant/settings/updateinfo" class="preloadThis">Update Info</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="<?php echo base_url();?>consultant/portal/logout" class="dropdown-header"><i class="fa fa-sign-out"></i> Logout</a></li>
                                </ul>
                            </li>
                            

                            

                        </ul>
                        
                    </li><!---.end of large dropdown-->
                       
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <div class="container-fluid" style="background: #fff;"><?php //main_container ?>

        <div class="row">
            <div class="col-lg-2 col-md-2 hidden-sm hidden-xs sidebar_nav scrollable">
            	<div class="text-center" style="padding-top: 20px;">
            	<!--<img  class="img-rounded img-thumbnail" src="<?php echo base_url()?>assets/2/img/icon-user-default.png" width="130" height="130">-->
                    <p class="text-muted">
                    	<em><i class="fa fa-key"></i> Logged in as</em>
                        </p>
                    <p class="lead"><?php echo $res_fname.' '.$res_lname; ?></p>
                </div>
                <div class="main_nav">
                
		<?php if (in_array("portal", $single_tab)){ ?>
                    <a href="<?php echo base_url() ?>consultant/portal" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="portal"){echo "active";}?>"><i class="fa fa-bullseye fa-fw"></i> Portal</a>
    	<?php } ?>
		<?php if (in_array("activated", $single_tab)){ ?>

                    <a href="<?php echo base_url()?>consultant/customer/activated" class="list-group-item preloadThis <?php if($this->uri->segment(3)=="activated"){echo "active";}?>"><i class="fa fa-star fa-fw"></i> Activated Customers</a>
    	<?php } ?>
    
                <div class="preincidentItems" <?php if(($this->uri->segment(3)=="manage" && $this->uri->segment(2)=="customer" ) || $this->uri->segment(2)=="organization"  || $this->uri->segment(3)=="activated" || $this->uri->segment(3)=="update"){echo ' style="display:block;"';} else {echo ' style="display:none;"'; }?>>
                  
                    <a href="<?php echo base_url()?>consultant/customer/manage" class="list-group-item preloadThis hidden preincidentChild  <?php if(($this->uri->segment(3)=="manage" && $this->uri->segment(2)=="customer") || ($this->uri->segment(3)=="update" && $this->uri->segment(2)=="customer" ) ){echo "active";}?>"><i class="fa fa-user fa-fw"></i> Manage CC</a>
                  
                    <a href="<?php echo base_url()?>consultant/organization/manage" class="list-group-item preloadThis hidden preincidentChild <?php if($this->uri->segment(2)=="organization"){echo "active";}?>"><i class="fa fa-building-o fa-fw"></i> Manage Organization</a>
                </div>
                
		<?php if (in_array("request", $single_tab)){ ?>

                    <a href="<?php echo base_url()?>consultant/customer/pending" class="list-group-item preloadThis <?php if($this->uri->segment(3)=="pending" || $this->uri->segment(3)=="request" ){echo "active";}?>"><i class="fa fa-star-o fa-fw"></i> Request Activation</a>
    	<?php } ?>
		<?php if (in_array("recall", $single_tab)){ ?>
        
                    <a href="<?php echo base_url()?>consultant/recall" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="recall"  && ($this->uri->segment(3)=="" || $this->uri->segment(3)=="index")){echo "active";}?>"><i class="fa fa-history fa-fw"></i> Recall Tasks</a>
    	<?php } ?>
                <div class="preincidentItems hidden" <?php if($this->uri->segment(2)=="recall"){echo ' style="display:block;"';} else {echo ' style="display:none;"'; }?>>
                  
                    <a href="<?php echo base_url()?>consultant/recall/guidance" class="list-group-item preloadThis preincidentChild  <?php if($this->uri->segment(2)=="recall" && $this->uri->segment(3)=="guidance"){echo "active";}?>"><i class="fa fa-lightbulb-o fa-fw"></i> Task Guidance</a>
                  
                    <a href="<?php echo base_url()?>consultant/recall/subcategory" class="list-group-item preloadThis preincidentChild  <?php if($this->uri->segment(2)=="recall" && $this->uri->segment(3)=="subcategory"){echo "active";}?>"><i class="fa fa-exchange fa-fw"></i> Sub-Category</a>
                  
                </div>




                    <a href="<?php echo base_url()?>consultant/simulation" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="simulation"){echo "active";}?>"><i class="fa fa-chain fa-fw"></i> Simulation</a>
                    <a href="<?php echo base_url()?>consultant/packs/manage" class="hidden list-group-item preloadThis <?php if($this->uri->segment(2)=="packs"){echo "active";}?>"><i class="fa fa-cube fa-fw"></i> Input Packs</a>

                    <a href="javascript:void(0);" class="list-group-item toggleCaretSet" onClick="$('.settings_nav').slideToggle();<?php /*?>$('.preincidentItems').slideUp()<?php */?>"><i class="fa fa-gear fa-fw"></i> Settings <i class="fa fa-caret-up pull-right"></i></a>
                </div>
                <div class="settings_nav" <?php if($this->uri->segment(2)=="settings"){echo ' style="display:block;"';} else {echo ' style="display:none;"'; }?>>
                    <a href="<?php echo base_url()?>consultant/settings/changepassword" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="changepassword"){echo "active";}?>"><i class="fa fa-key fa-fw"></i> Change Password</a>
                    <a href="<?php echo base_url()?>consultant/settings/updateinfo" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="updateinfo"){echo "active";}?>"><i class="fa fa-info fa-fw"></i> Update Info</a>
                </div>
                
                
            </div>
			
            <!--main container-->
            <div class="col-md-10 col-xs-12 col-sm-12 scrollable main_content" style="border-left: 1px solid #e7e7e7">


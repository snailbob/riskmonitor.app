<style>
	.dataTables_filter, .dataTables_length{
		display: none;
		visibility: hidden;
	}
</style>
                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Input Pack

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 
                                	<a href="<?php echo base_url().'webmanager'?>">Portal</a></li>
                                <li class="active">Update Input Pack</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



<!-- Form AREA -->

<div class="row">

    <div class="col-lg-12">

        <?php 

        if($this->session->flashdata('success')!="")

        {

        ?>

        <div class="alert alert-success alert-dismissable">

        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

        <?php    

        } 

        if($this->session->flashdata('error')!="")

        {

        ?>

        <div class="alert alert-danger alert-dismissable">

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

        <?php

        } 

        ?>

        <div>
        	<a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addSimulationModal">Add Simulation Module</a>
		</div>

        <div class="clearfix" style="margin-bottom: 15px;">
		</div>
        
        
        <div class="panel panel-default">

            <div class="panel-heading">

                <div class="panel-title">

                    <h4>Simulation Modules</h4>

                </div>

                <div class="panel-widgets">

                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                </div>

                <div class="clearfix"></div>

            </div>

            <div id="validationExamples" class="panel-collapse collapse in">

                <div class="panel-body">

                    
                    

					<?php if(count($simulation_modules) > 0){ ?>
                    
                    <div class="table-responsive">

                    <table class="table table-hover" id="example-table">

                        <thead>
                            <tr>
                                <th width="100%">Name</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>  
                        
                        	<?php foreach($simulation_modules as $r=>$value){ ?> 
                            <tr>
                                <td><?php echo $value['name']; ?></td>
                                <td>
                                
<div class="dropdown pull-right">
  <a class="btn btn-default btn-xs" id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown">
    Action
    <span class="caret"></span>
  </a>

  <ul class="dropdown-menu text-right" style="min-width: 100px;">
    <li><a href="<?php echo base_url().'consultant/simulation/module/'.$value['id'] ?>">Manage</a></li>
    <li><a href="#" class="update_simu_btn" data-name="<?php echo $value['name']; ?>" data-id="<?php echo $value['id']; ?>">Update</a></li>
    <li><a href="#" class="delete_simu_module_id" data-id="<?php echo $value['id']; ?>">Delete</a></li>
  </ul>
</div>                                
                                
                                
                                
                                </td>
                            </tr>
                            
                            <?php } ?>
                        
                             
                        </tbody>
                    </table>
                    </div>     
                           
					<?php } else{
						echo '<p class="text-center text-muted">No simulation modules.</p>';
					}?>
                                

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->


<!-- Modal -->
<div class="modal fade" id="addSimulationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="lead">Simulation Module</p>

		<form id="simu_module_form">
        	<div class="form-group">
            	<input type="text" class="form-control" placeholder="Name" name="name" />
            	<input type="hidden" class="form-control" name="id" />
            </div>
        
        	<div class="form-group">
            	<button type="submit" class="btn btn-primary" disabled="disabled">Save changes</button>
            </div>
        
        </form>


      </div>
    </div>
  </div>
</div>


                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Case Report

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Case Report</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



				 <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!="" || $error != "" || form_error('r_control') !=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error'); 
							
							echo form_error('r_control');
							
							echo $error;  ?>
                            
                            
                            </div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    
                    
                        <ul class="nav nav-tabs" style="margin-bottom: 35px;">
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportdoc/<?php echo $this->uri->segment(4); ?>">Evidence and Documents</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reporthazard/<?php echo $this->uri->segment(4); ?>">Hazard Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcause/<?php echo $this->uri->segment(4); ?>">Root-Cause Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportimpact/<?php echo $this->uri->segment(4); ?>">Impact Analysis</a></li>
                          <li class="active"><a href="<?php echo base_url() ?>crt/managecase/reportcontrol/<?php echo $this->uri->segment(4); ?>">
                          <?php if(count($casedocs) > 0){ if ($casedocs[0]['control']!=''){ echo '<i class="fa fa-check text-success"></i> ';  } } ?>
                          Control</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportrecommend/<?php echo $this->uri->segment(4); ?>">
                          Recommendations </a></li>
                        </ul>                    


									

						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">
                            <label for="r_control" class="col-sm-12"><em>Can the case be controlled? And if so, how? </em></label>
                            <div class="col-sm-12">
                                <textarea rows="6" class="form-control" id="r_control" name="r_control" placeholder="Can the case be controlled?"><?php
										foreach($casedocs as $rr => $value){ 
										
										echo $value['control'];
										
										}
									?></textarea>
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-sm-12">
                            <a class="btn btn-default" href="<?php echo base_url()?>crt/managecase">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_reportcontrol" id="add_reportcontrol">Submit</button>

                            </div>

						</div>

										

										

                        </form>									





                    </div>
                </div><!--.row-->




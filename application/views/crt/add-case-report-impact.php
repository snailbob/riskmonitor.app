                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Case Report

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Case Report</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



				 <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!="" || $error != "" || (form_error('impact_to_bsn') !="" || form_error('impact_to_stk') !="")){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error'); 
							
							echo form_error('impact_to_bsn');
							echo form_error('impact_to_stk');
							
							echo $error;  ?>
                            
                            
                            </div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    
                    
                        <ul class="nav nav-tabs" style="margin-bottom: 35px;">
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportdoc/<?php echo $this->uri->segment(4); ?>">Evidence and Documents</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reporthazard/<?php echo $this->uri->segment(4); ?>">Hazard Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcause/<?php echo $this->uri->segment(4); ?>">Root-Cause Analysis</a></li>
                          <li class="active"><a href="<?php echo base_url() ?>crt/managecase/reportimpact/<?php echo $this->uri->segment(4); ?>">
                          <?php if(count($casedocs) > 0){ if ($casedocs[0]['impact_to_bsn']!=''&& $casedocs[0]['impact_to_stk']!=''){ echo '<i class="fa fa-check text-success"></i> ';  } } ?>
                          Impact Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcontrol/<?php echo $this->uri->segment(4); ?>">
                          Control</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportrecommend/<?php echo $this->uri->segment(4); ?>">
                          Recommendations </a></li>
                        </ul>                    


									

						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">
                            <label for="impact_to_bsn" class="col-sm-12"><em>What is the impact to business operations? </em></label>
                            <div class="col-sm-12">
                                <textarea rows="6" class="form-control" id="impact_to_bsn" name="impact_to_bsn" placeholder="Enter impact to business operation"><?php
										foreach($casedocs as $rr => $value){ 
										
											if($value['impact_to_bsn'] != ''){
												 echo $value['impact_to_bsn'];
											}else{
												 echo set_value('impact_to_bsn');
											}
										
										}
									?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="impact_to_stk" class="col-sm-12"><em>What is the impact to stakeholders? </em></label>
                            <div class="col-sm-12">
                                <textarea rows="6" class="form-control" id="impact_to_stk" name="impact_to_stk" placeholder="Enter impact to stakeholders"><?php
										foreach($casedocs as $rr => $value){ 
										
											if($value['impact_to_stk'] != ''){
												 echo $value['impact_to_stk'];
											}else{
												 echo set_value('impact_to_stk');
											}
										
										}
									?></textarea>
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-sm-12">
                            <a class="btn btn-default" href="<?php echo base_url()?>crt/managecase">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_reportcause" id="add_reportcause">Submit</button>

                            </div>

						</div>

										

										

                        </form>									





                    </div>
                </div><!--.row-->




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Case Report

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Case Report</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



				 <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!="" || $error != ""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error'); 
							
							echo $error;  ?>
                            
                            
                            </div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    
                    
                        <ul class="nav nav-tabs" style="margin-bottom: 35px;">
                          <li class="active"><a href="<?php echo base_url() ?>crt/managecase/reportdoc/<?php echo $this->uri->segment(4); ?>">
                          
                          <?php if(count($casedocs) > 0){ echo '<i class="fa fa-check text-success"></i> '; } ?>
                          Evidence and Documents</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reporthazard/<?php echo $this->uri->segment(4); ?>">Hazard Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcause/<?php echo $this->uri->segment(4); ?>">Root-Cause Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportimpact/<?php echo $this->uri->segment(4); ?>">Impact Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcontrol/<?php echo $this->uri->segment(4); ?>">
                          Control</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportrecommend/<?php echo $this->uri->segment(4); ?>">
                          Recommendations </a></li>
                        </ul>                    


									

						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">File Input</label>

                            <div class="col-sm-10">

                                <input type="file" name="file_upload_name" id="file_upload_name" value=""/>

                                <p class="small text-muted">(.doc, .docx, .pdf, .txt )</p>

                                <?php echo form_error('file_upload_name'); ?>
                                
                                <div class="hidden text-red" id="file_error"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="case_title" class="col-sm-2 control-label">File Description</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="file_desc" name="file_desc" placeholder="Enter file description"  /><?php echo form_error('case_title'); ?>
                                <div class="hidden text-red" id="desc_error"></div>
                                
                            </div>
                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>crt/managecase">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_casedoc" id="add_casedoc">Upload</button>

                            </div>

						</div>

										

										

                        </form>									




                        <hr class="divider" />
                        
                        
                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Evidence and  Documents List</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									<?php 
									
										if(count($casedocs) > 0){ ?>
                                        
                                    <div class="table-responsive">
        
                                    <table class="table table-hover table-bordered table-green" id="example-table">
        
                                        <thead>
        
                                            <tr>
        
                                                <th width="50%">Document Name</th>
        
                                                <th width="50%">File Description</th>
        
                                                <th width="50px"> </th>
        
                                            </tr>
        
                                        </thead>
        
                                        <tbody>

                                        
                                        
                                        <?php
										
											foreach($casedocs as $casedocs){
										?>
                                    
                                    
                                        <tr>
    
                                            <td>
                                            
											<?php
                                            $fileext = explode ('.',$casedocs['document']);
                                            if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
                                            
                                            echo '<i class="fa fa-file-pdf-o text-red"></i> ';
                                            
                                            }
                                            
                                            else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
                                            
                                            echo '<i class="fa fa-file-word-o text-blue"></i> ';
                                            
                                            }
                                            
                                            else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
                                            
                                            echo '<i class="fa fa-file-text-o text-muted"></i> ';
                                            
                                            }

											echo substr($casedocs['document'],13,50); ?>
                                            </td>
                                            <td>
                                            <?php echo $casedocs['file_desc']; ?>
                                            </td>
                                            <td>
                                            <a href="javascript:return void(0);" class="btn btn-sm btn-default"  onclick="return del_confirm('<?php echo base_url();?>crt/managecase/delete/<?php echo $casedocs['id'];?>/<?php echo $casedocs['document'].'/'.$this->uri->segment(4);?>');">Delete</a>
                                            </td>
                                        </tr>
                                    

										<?php 
        
                                            }
                                        ?>
        
                                                        </tbody>
        
                                                    </table>
        
                                                </div>
        
                                                <!-- /.table-responsive -->
        
                                        <?php 
                                        }
                                        else{ ?>
                                        
                                        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-file-o" style="font-size: 90px"></i></p>
                                        <p class="text-center" style="color: #ccc; margin-top: 20px;">No evidence and documents</p>
                                        <?php }
        
                                        ?>
                                    
                                    						

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




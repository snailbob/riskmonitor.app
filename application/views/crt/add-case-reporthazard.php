                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Case Report

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> 

                                	<a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Case Report</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->



				 <!-- Form AREA -->

				<div class="row" style="padding-bottom: 40px;">

                	<div class="col-lg-12">

                       <?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!="" || form_error('hazard') !=""){ ?>

                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');
							
							echo form_error('hazard');  ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    	<div style="display:none">
                            <button class="btn btn-primary btn-sm save_stp3_table" style="display: none;">Save</button>

                        </div>
                    
                    
                        <ul class="nav nav-tabs" style="margin-bottom: 35px;">
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportdoc/<?php echo $this->uri->segment(4); ?>">Evidence and Documents</a></li>
                          <li class="active"><a href="<?php echo base_url() ?>crt/managecase/reporthazard/<?php echo $this->uri->segment(4); ?>">
                          <?php if(count($casedocs) > 0){  if ($casedocs[0]['hazard']!=''){ echo '<i class="fa fa-check text-success"></i> '; } }?>
                          Hazard Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcause/<?php echo $this->uri->segment(4); ?>">Root-Cause Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportimpact/<?php echo $this->uri->segment(4); ?>">Impact Analysis</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportcontrol/<?php echo $this->uri->segment(4); ?>">
                          Control</a></li>
                          <li><a href="<?php echo base_url() ?>crt/managecase/reportrecommend/<?php echo $this->uri->segment(4); ?>">
                          Recommendations </a></li>
                        </ul>                    


						<?php 
                        
							if(count($casedocs) > 0){ 
								if ($casedocs[0]['hazard'] !=''){
									
									foreach($casedocs as $rr => $casedocs){
									?>
									<div class="col-xs-12 bg-info" style="padding-top: 20px; margin-bottom: 25px;">
										<p class="lead">You selected: <?php echo $casedocs['hazard']; ?></p>
									</div>
									<?php
									}
								}
							
								?>
							
							<?php 
							}												
                        ?>

						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="row" style="padding-left: 15px; padding-right: 15px;">
                          <div class="col-sm-4 hidden-xs no-margin-btm">&nbsp;</div>
                          <div class="col-sm-8 col-xs-12 no-margin-btm"><p class="text-center"><strong>SEVERITY</strong></p></div>
                        </div>

                        <div class="row hidden-xs" style="padding-left: 15px; padding-right: 15px;">
                          <div class="col-xs-4 no-margin-btm">&nbsp;</div>
                          <div class="col-xs-2 no-margin-btm">
                              <p class="text-center">
                                  I<br />
                                  Catastrophic
                              </p>
                          </div>
                          <div class="col-xs-2 no-margin-btm">
                              <p class="text-center">
                                  II<br />
                                  Critical
                              </p>
                          </div>
                          <div class="col-xs-2 no-margin-btm">
                              <p class="text-center">
                                  III<br />
                                  Marginal
                              </p>
                          </div>
                          <div class="col-xs-2 no-margin-btm">
                              <p class="text-center">
                                  IV<br />
                                  Negligible
                              </p>
                          </div>
                       
                        </div>
                        <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        
                        
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">&nbsp;</div>
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">A. Frequently</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="I-A" />
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Catastrophic, y-Frequently" style="margin-top: 20px;">I-A</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="II-A"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Critical, y-Frequently" style="margin-top: 20px;">II-A</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="III-A"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Marginal, y-Frequently" style="margin-top: 20px;">III-A</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="IV-A"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Negligible, y-Frequently" style="margin-top: 20px;">IV-A</p>
                            </label>
                          </div>
                          
                        <div style="clear: both;"> </div>

                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">&nbsp;</div>
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">B. Moderate</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="I-B"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Catastrophic, y-Moderate" style="margin-top: 20px;">I-B</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="II-B"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Critical, y-Moderate" style="margin-top: 20px;">II-B</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="III-B"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Marginal, y-Moderate" style="margin-top: 20px;">III-B</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="IV-B"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Negligible, y-Moderate" style="margin-top: 20px;">IV-B</p>
                            </label>
                          </div>
                          
                        <div style="clear: both;"> </div>

                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs"><b>LIKELIHOOD</b></div>
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">C. Occasional</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="I-C"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Catastrophic, y-Occasional" style="margin-top: 20px;">I-C</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="II-C"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Critical, y-Occasional" style="margin-top: 20px;">II-C</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="III-C"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Marginal, y-Occasional" style="margin-top: 20px;">III-C</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="IV-C"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Negligible, y-Occasional" style="margin-top: 20px;">IV-C</p>
                            </label>
                          </div>
                          
                        <div style="clear: both;"> </div>

                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">&nbsp;</div>
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">D. Remote</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="I-D"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Catastrophic, y-Remote" style="margin-top: 20px;">I-D</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="II-D"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Critical, y-Remote" style="margin-top: 20px;">II-D</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="III-D"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Marginal, y-Remote" style="margin-top: 20px;">III-D</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="IV-D"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Negligible, y-Remote" style="margin-top: 20px;">IV-D</p>
                            </label>
                          </div>
                          
                        <div style="clear: both;"> </div>

                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">&nbsp;</div>
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">E. Unlikely</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="I-E"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Catastrophic, y-Unlikely" style="margin-top: 20px;">I-E</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="II-E"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Critical, y-Unlikely" style="margin-top: 20px;">II-E</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="III-E"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Marginal, y-Unlikely" style="margin-top: 20px;">III-E</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="IV-E"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Negligible, y-Unlikely" style="margin-top: 20px;">IV-E</p>
                            </label>
                          </div>
                          
                        <div style="clear: both;"> </div>

                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">&nbsp;</div>
                          <div class="col-sm-2 col-xs-3 no-margin-btm hidden-xs">F. Impossible</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="I-F"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Catastrophic, y-Impossible" style="margin-top: 20px;">I-F</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="II-F"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Critical, y-Impossible" style="margin-top: 20px;">II-F</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="III-F"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Marginal, y-Impossible" style="margin-top: 20px;">III-F</p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="IV-F"/>
                            	 <p class="lead tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="x-Negligible, y-Impossible" style="margin-top: 20px;">IV-F</p>
                            </label>
                          </div>
                        </div>
                        






                        <div class="form-group" style="margin-top: 20px;">

                            <div class="col-sm-4">
                            </div>
                            
                            <div class="col-sm-8 col-xs-12">
                            <a class="btn btn-default" href="<?php echo base_url()?>crt/managecase">Back</a> 
                            <button type="submit" class="btn btn-primary" name="add_hazard" id="add_hazard">Submit</button>

                            </div>

						</div>

										

										

                        </form>									



					</div>

                </div><!--.row-->




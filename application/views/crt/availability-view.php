                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                
<?php 
	//select org
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));

	/*********************************************************/
?>                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Pre-Incident

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Pre-Incident</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>




                    <div class="col-lg-12">
	


<div class="panel panel-default hidden">
  <div class="panel-heading">
    <div class="panel-title"><h4>Availability</h4></div>
  </div>
  <div class="panel-body">

        
        <form class="form-vertical">
          <div class="form-group">
            <label>Status <i class="fa fa-check-circle text-success hidden"></i></label><br />
            <div class="btn-group" data-toggle="buttons">
              <label class="btn <?php if($available != 'N') { echo 'btn-success active'; } else { echo 'btn-default'; } ?>">
                <input type="radio" name="availability_btn" value="Y" autocomplete="off" <?php if($available != 'N') { echo 'checked'; } ?>> Available
              </label>
              <label class="btn  <?php if($available == 'N') { echo 'btn-success active'; } else { echo 'btn-default'; } ?>">
                <input type="radio" name="availability_btn" value="N" autocomplete="off" <?php if($available == 'N') { echo 'checked'; } ?>> Unavailable
              </label>
            </div>

          </div>

        </form>

  </div>
</div>    
    


<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-primary pull-right add_recur_btn" style="margin-top: 3px;">Add recurring schedule</button>
    <div class="panel-title"><h4>Availability Calendar</h4></div>
  </div>
  <div class="panel-body">
        <div class="alert alert-info no-sched-alert fade in hidden">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        	<i class="fa fa-info-circle"></i> To add your schedule, highlight your time/days of availability by clicking and dragging from the calendar.
        </div>
        
        
		<div class="row hidden">
        	<div class="col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-4">
            
                <div id="external-events" class="well well-sm">
                    <h5>Drag to add new schedule.</h5>
                    <div class="fc-event" data-id="<?php echo $last_id; ?>">Available</div>
                    <p class="hidden">
                        <input type="checkbox" id="drop-remove" />
                        <label for="drop-remove">remove after drop</label>
                    </p>
                </div>
            </div>
        
        </div>
        
   

        <div class="the_cal">

            <div id="script-warning">
                <code>php/get-events.php</code> must be running.
            </div>
        
            <div class="hidden" id="hidden">loading...</div>
        
            <div id="calendar"></div>
        
        </div>


  </div>
</div>    
    
    

                    </div><!----.col-lg-12-->



                </div><!--.row-->




<!-- Modal -->
<div class="modal fade" id="myDaySchedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">


<form id="dayscheduler_form">
<fieldset>

    <!-- Form Name -->
    <legend>
        <small>Add Available time
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </small>
    
    </legend>


    <!-- Text input-->
    <div class="form-group">
        <label>Start</label>  
        <div class="input-group date" id="datetimepicker3">
            <span class="input-group-addon">
                <span class="fa fa-clock-o"></span>
            </span>
            <input name="daystart_time" type="text" placeholder="Start" class="form-control timepicker date">
        </div>
        <input name="today_day" type="hidden">
    </div>
    
    <!-- Text input-->
    <div class="form-group">

        <label>End</label>
        <div class="input-group date">
            <span class="input-group-addon">
                <span class="fa fa-clock-o"></span>
            </span>
            <input name="dayend_time" type="text" placeholder="End" class="form-control timepicker date">
        </div>
       

    </div>
    
    <div class="form-group">
        <button class="btn btn-primary submit_form_btn">Add schedule</button>
    </div>		

</fieldset>
</form>

      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="mySchedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        


<form id="scheduler_form">
<fieldset>

    <!-- Form Name -->
    <legend><small>Add Available time
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </small></legend>
    
    <div class="row">
    	<div class="col-lg-12">
            <!-- Text input-->
            <div class="form-group">
                <label>Start</label>  
    
                <div class="input-group date" id="datetimepicker3">
                    <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
                    <input name="start_time" type="text" placeholder="Start" class="form-control timepicker date">
                </div>
    
            </div>
            
            <!-- Text input-->
            <div class="form-group">
    
                <label>End</label>
                <div class="input-group date">
                    <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
                    <input name="end_time" type="text" placeholder="End" class="form-control timepicker date">
                </div>
               
    
            </div>
            
            <!-- Text input-->
            <div class="form-group">
    
                <label>Duration</label>
                <select name="duration" class="form-control">
                    <option value="0">Select</option>
                    <option value="0">0 Minutes</option>
                    <option value="5">5 Minutes</option>
                    <option value="10">10 Minutes</option>
                    <option value="15">15 Minutes</option>
                    <option value="30" selected="selected">30 Minutes</option>
                    <option value="60">1 Hour</option>
                    <option value="120">2 Hours</option>
                    <option value="180">3 Hours</option>
                    <option value="240">4 Hours</option>
                    <option value="300">5 Hours</option>
                    <option value="360">6 Hours</option>
                    <option value="420">7 Hours</option>
                    <option value="480">8 Hours</option>
                    <option value="540">9 Hours</option>
                    <option value="600">10 Hours</option>
                    <option value="660">11 Hours</option>
                    <option value="720">0.5 Days</option>
                    <option value="1440">1 Day</option>
                    <option value="2880">2 Days</option>
                    <option value="4320">3 Days</option>
                    <option value="5760">4 Days</option>
                    <option value="10080">1 Week</option>
                    <option value="20160">2 Weeks</option>
                </select>
            </div>
    
        </div>
    </div>
    
    <!-- Form Name -->
    <legend><small>Recurrence pattern</small></legend>
    
    <div class="row">
    	<div class="col-lg-12">
            <div class="row">
            
                    
            
                <div class="col-sm-4 col-md-4 col-lg-3" style="border-right: 1px solid #eee;">
                    <!-- Multiple Radios -->
                    <div class="form-group">

                          <div class="radio radio-info">
                              <input type="radio" name="recur_radios" id="radios-0" value="daily" checked="checked">
                            <label for="radios-0">
                              Daily
                            </label>
                          </div>
                          <div class="radio radio-info">
                              <input type="radio" name="recur_radios" id="radios-1" value="weekly">
                            <label for="radios-1">
                              Weekly
                            </label>
                          </div>
                          <div class="radio radio-info">
                              <input type="radio" name="recur_radios" id="radios-3" value="monthly">
                            <label for="radios-3">
                              Monthly
                            </label>
                          </div>
                          <div class="radio radio-info">
                              <input type="radio" name="recur_radios" id="radios-4" value="yearly">
                            <label for="radios-4">
                              Yearly
                            </label>
                          </div>

                    </div>
                    
                </div>
                
                
                <div class="col-sm-8 col-md-8 col-lg-9">
                	<div class="daily_settings">
                            
                    
                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="daily_set_radios" id="daily_set_radios-1" value="1" checked="checked" value="1">
                                <label for="daily_set_radios-1">
                                    Every
                                </label>
                            </div>
                            <input type="number" name="daily_every_day" class="form-control slim_text_input" value="1" /> day(s)
                        </div>
                    
                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="daily_set_radios" id="daily_set_radios-2" value="2">
                                <label for="daily_set_radios-2">
                                    Every weekday
                                </label>
                            </div>
                        </div>
                              
                        
                        
                    </div>   
                	<div class="monthly_settings hidden">
                    	
                    
                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="monthly_set_radios" id="monthly_set_radios-1" value="1" checked="checked">
                                <label for="monthly_set_radios-1">
                                    Day
                                </label>
                            </div>
                            <input type="number" name="monthly_every_day" class="form-control slim_text_input"/> of every 
                            <input type="number" name="monthly_day_months" class="form-control slim_text_input" value="1"/> month(s)
                        </div>
                    
                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="monthly_set_radios" id="monthly_set_radios-2" value="2">
                                <label for="monthly_set_radios-2">
                                    The
                                </label>
                            </div>
                            <select class="form-control" style="max-width: 70px;" name="monthly_week_no">
                            	<option value="1">first</option>
                            	<option value="2">second</option>
                            	<option value="3">third</option>
                            	<option value="4">forth</option>
                            	<option value="5">last</option>
                            
                            </select>  
                            <select class="form-control slim_text_input" name="monthly_days_settings">
                            	<option value="everyday">day</option>
                            	<option value="weekday">weekday</option>
                            	<option value="weekend">weekend day</option>
                            	<option value="0">Sunday</option>
                            	<option value="1">Monday</option>
                            	<option value="2">Tuesday</option>
                            	<option value="3">Wednesday</option>
                            	<option value="4">Thursday</option>
                            	<option value="5">Friday</option>
                            	<option value="6">Saturday</option>
                            
                            </select> of every 
                            <input type="number" name="monthly_the_months" class="form-control" style="max-width: 70px;" value="1"/> month(s)
                        </div>
                    
                        
                        
                    </div>    
                	<div class="yearly_settings hidden">

                    	
                    
                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                        	Recure every 
                            <input type="number" name="yearly_every_day" class="form-control slim_text_input" value="1"/> 
                            year(s)
                        </div>
                    
                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="yearly_set_radios" id="yearly_set_radios-1" value="1" checked="checked">
                                <label for="yearly_set_radios-1">
                                    On:
                                </label>
                            </div>
                            <select class="form-control slim_text_input" name="yearly_on_month">
                            	<option value="1">January</option>
                            	<option value="2">February</option>
                            	<option value="3">March</option>
                            	<option value="4">April</option>
                            	<option value="5">May</option>
                            	<option value="6">June</option>
                            	<option value="7">July</option>
                            	<option value="8">August</option>
                            	<option value="9">September</option>
                            	<option value="10">October</option>
                            	<option value="11">Novermber</option>
                            	<option value="12">December</option>
                            </select>                             
                            <input type="number" name="yearly_day" class="form-control slim_text_input" />
                        </div>

                        <!-- Multiple Radios -->
                        <div class="form-group form-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="yearly_set_radios" id="yearly_set_radios-2" value="2">
                                <label for="yearly_set_radios-2">
                                    On the:
                                </label>
                            </div>
                            <select class="form-control" name="yearly_week_no" style="max-width: 70px;">
                            	<option value="first">first</option>
                            	<option value="second">second</option>
                            	<option value="third">third</option>
                            	<option value="forth">forth</option>
                            	<option value="last">last</option>
                            
                            </select>  
                            <select class="form-control slim_text_input" name="yearly_days_settings">
                            	<option value="everyday">day</option>
                            	<option value="weekday">weekday</option>
                            	<option value="weekend">weekend day</option>
                            	<option value="0">Sunday</option>
                            	<option value="1">Monday</option>
                            	<option value="2">Tuesday</option>
                            	<option value="3">Wednesday</option>
                            	<option value="4">Thursday</option>
                            	<option value="5">Friday</option>
                            	<option value="6">Saturday</option>
                            
                            </select> of
                            <select class="form-control slim_text_input" name="yearly_onthe_month">
                            	<option value="1">January</option>
                            	<option value="2">February</option>
                            	<option value="3">March</option>
                            	<option value="4">April</option>
                            	<option value="5">May</option>
                            	<option value="6">June</option>
                            	<option value="7">July</option>
                            	<option value="8">August</option>
                            	<option value="9">September</option>
                            	<option value="10">October</option>
                            	<option value="11">Novermber</option>
                            	<option value="12">December</option>
                            </select>                             
                        </div>


                    </div>    
                	<div class="weekly_settings hidden">
                        
                          <div class="form-group form-inline">
                              Recur every
                              <input type="number" name="weekly_recure" class="form-control slim_text_input" value="1">
                              week(s) on:
                          </div>
                          
                          <div class="form-group">
                          
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox1" value="0">
                                <label for="checkbox1">
                                    Sunday
                                </label>
                              </div>
                         
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox2" value="1">
                                <label for="checkbox2">
                                    Monday
                                </label>
                              </div>
                         
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox3" value="2">
                                <label for="checkbox3">
                                    Tuesday
                                </label>
                              </div>
                         
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox4" value="3">
                                <label for="checkbox4">
                                    Wednesday
                                </label>
                              </div>
                         
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox5" value="4">
                                <label for="checkbox5">
                                    Thursday
                                </label>
                              </div>
                         
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox6" value="5">
                                <label for="checkbox6">
                                    Friday
                                </label>
                              </div>
                         
                              <div class="checkbox checkbox-inline">
                                <input type="checkbox" name="days_in_week[]" id="checkbox7" value="6">
                                <label for="checkbox7">
                                    Saturday
                                </label>
                              </div>
                         
    
                          </div><!--form-group-->
                    
                    
                    
                    </div><!--weekly settings-->
        
        
        
        
        
                </div><!--col-md-8 col-lg-9-->
            
            
            
            </div><!--row-->
        </div><!--col-md-8 col-md-offset-2-->
    </div><!--row-->
    
    
    <!-- Form Name -->
    <legend><small>Range of recurrence</small></legend>
    <div class="row">
    	<div class="col-lg-12">
            <div class="row">
            
                <div class="col-md-5">
                	<div class="form-group form-inline">
                    	<label>Start</label>
                        <input type="text" class="form-control avail_date_picker" name="start_date">                   
                    </div>
                
                </div>
           
                <div class="col-md-7">
                
                    <!-- Multiple Radios -->
                    <div class="form-group">
                        <div class="radio radio-info">
                            <input type="radio" name="end_radios" id="end_radios-0" value="1" checked="checked" value="1">
                            <label for="end_radios-0">
                                Until the end of current year
                            </label>
                        </div>
                    </div>
                
                    <!-- Multiple Radios -->
                    <div class="form-group form-inline">
                        <div class="radio radio-info">
                            <input type="radio" name="end_radios" id="end_radios-1" value="2">
                            <label for="end_radios-1">
                                End after
                            </label>
                        </div>
                        <input type="number" name="number_of_occurence" class="form-control slim_text_input" value="1"/> occurences
                    </div>
                
                    <!-- Multiple Radios -->
                    <div class="form-group form-inline">
                        <div class="radio radio-info">
                            <input type="radio" name="end_radios" id="end_radios-2" value="3">
                            <label for="end_radios-2">
                                End by
                            </label>
                        </div>
                        <input type="text" class="form-control avail_date_picker" name="end_date" />
                    </div>
                          

                    </div>
                    
                </div>
                
            
                <div class="form-group">
                    <button class="btn btn-primary submit_form_btn">Add schedule</button>
                </div>		
            
            </div>
        </div>
    </div>



</fieldset>

</form>
        
        

      </div>
    </div>
  </div>
</div>

<!-- begin PAGE TITLE AREA -->
<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
    <div class="row">
        <div class="col-lg-12">
            <div class="page-title">
                <h1>Pre-Incident Phase CRT Members</h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i>
                    <a href="<?php echo base_url().'crt'?>">Dashboard</a>
                    </li>
                    <li class="active">Messages</li>
                </ol>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- end PAGE TITLE AREA -->
<div class="row">
	<div class="col-lg-12">
		<?php 
        if($this->session->flashdata('success')!="")
        {
        ?>
        <div class="alert alert-success alert-dismissable">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>
        <?php    
        } 
        if($this->session->flashdata('error')!="")
        {
        ?>
        <div class="text-red">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>
        <?php
        } 
        ?>
		<div class="portlet portlet-default">
			<div class="portlet-heading">
				<div class="portlet-title">
					<h4><i class="fa fa-inbox"></i>Detail Message</h4>
				</div>
                <div class="clearfix"></div>
			</div>
			<div class="portlet-body">
               
                            
                <nav class="navbar mailbox-topnav" role="navigation">
					<div class="mailbox-nav">
                    	<ul class="nav navbar-nav button-tooltips">
                        	<li class="message-actions">
                            	<div class="btn-group navbar-btn">
                                	Date : <?php echo date('d M Y',strtotime($msg_details[0]['send_date'])); ?>
                            	</div>
                        	</li>
                        </ul>
                    </div>
                </nav>
                <div align="right">
					<a href="<?php echo base_url() ?>crt/dashboard/" style="text-decoration: none;"><b> Back </b></a>
				</div>
                <div id="mailbox">
                    <form action='' name="message-details" id="message-details" method='post' class="form-horizontal" >
						<?php //print_r($msg_details) ;?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Posted by</label>
                            <div class="col-sm-10">
                                 <div class="details_content">
                                 <?php echo $msg_details[0]['crt_first_name'].' '.$msg_details[0]['crt_last_name']; ?>
                            </div>							
                           	</div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Scenario</label>
                            <div class="col-sm-10">
                                 <div class="details_content">
                                 <?php echo $msg_details[0]['scenario']; ?>
                            </div>							
                           	</div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Message</label>
                            <div class="col-sm-10">
                                <div class="details_content">
                                 <?php echo $msg_details[0]['common_message']; ?>
                            </div>					
                           	</div>
                        </div>
					</form>
                </div>
                
			</div>
		</div>
	
	</div>

</div><!--.row -->
					


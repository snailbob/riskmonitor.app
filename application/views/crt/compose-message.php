<div class="row">

    <div class="col-lg-12">

        <div class="page-title">

        	<h1>Compose Message</h1>

            <ol class="breadcrumb">

                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'crt/message/inbox'?>">Message</a></li>

                <li class="active">Compose Message</li>

            </ol>

        </div>

    </div>

</div>



<!-- Form AREA -->

<div class="row" >
<?php /*?>
	<div align="right">

		<div class="form-group">

			<label class="col-sm-2 control-label"></label>

            <div class="col-sm-10">

                <a class="btn btn-default" href="<?php echo base_url()?>crt/message/inbox">Back</a> 

            </div>

		</div>	

	</div>

	

    <div class="col-lg-12" style="padding-top:20px;">

		<?php if($this->session->flashdata('success')!=""){ ?>

        <div class="alert alert-success alert-dismissable">

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

        </div>

        <?php } if($this->session->flashdata('error')!=""){ ?>

        <div class="text-red">

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>'

        <?php } ?>

	</div>

                    

    <div class="col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">

				<div class="panel-title">

					<h4>Compose Message</h4>

				</div>

				<div class="panel-widgets">

					<a data-toggle="collapse" data-parent="#accordion" href="#validationExamples">

                    <i class="fa fa-chevron-down"></i>

                    </a>

				</div>

				<div class="clearfix"></div>

			</div>

			

            <div id="validationExamples" class="panel-collapse collapse in">

				<div class="panel-body">

					<form action='' name="frm-compose-message" id="frm-compose-message" method='post' class="form-horizontal"

                     role="form" validate>

						<div class="form-group">

							<label class="col-sm-2 control-label">To</label>

                            <div class="col-sm-10">

                                <select class="form-control" name="message_receiver" id="message_receiver">

                               	<option value=""> Select </option>

                                <?php

									$my_team=$my_class->fetch_team_members();

									if(count($my_team) > 0)

									{

										foreach($my_team as $team)

										{

								?>

                                		<option value="<?php echo $team['login_id'].' '.$this->master_model->decryptIt($team['crt_email']).' '.$this->master_model->decryptIt($team['crt_first_name']).' '.$this->master_model->decryptIt($team['crt_last_name']) ?>" <?php if($this->uri->segment(4)!="" && base64_decode($this->uri->segment(4))==$team['login_id']){echo 'selected="selected"';} ?>>

                                        <?php echo $this->master_model->decryptIt($team['crt_first_name']).' '.$this->master_model->decryptIt($team['crt_last_name']); ?>

                                        </option>

                                <?php

										}

									}

                                ?>

                               </select>							

                           	</div>

                        </div>

                        

                          <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Subject</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="subject" name="subject" value="" required data-msg-required="Please enter last name" ><?php echo form_error('subject');  ?>

                            </div>

                        </div>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Message</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="txt_message" name="txt_message" placeholder="Enter message"></textarea><?php echo form_error('txt_message'); ?>							

                           	</div>

                        </div>

                        

                        <div class="form-group">

                        <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-10">

                        <button type="submit" class="btn btn-primary"   name="send_msg" id="send_msg" value='0' >Send</button>

                        </div>

            			</div>	

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>

					<?php */?>




    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <div class="btn-group hidden">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Mail <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Mail</a></li>
                        <li><a href="#">Contacts</a></li>
                        <li><a href="#">Tasks</a></li>
                    </ul>
                </div>
            </div>
            
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <a href="<?php echo base_url().'crt/message/compose' ?>" class="btn btn-danger btn-sm btn-block active" role="button">COMPOSE</a>
                <hr />
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo base_url().'crt/message/inbox' ?>">
                    
                    
                     <?php 
						$unread=$this->common_model->get_unread_count($this->session->userdata('logged_cc_login_id'));
						
						if($unread > 0){echo '<span class="badge danger pull-right">'.$unread.'</span>';} ?>
                    
                    
                     Inbox </a>
                    </li>
                    <?php /*?><li><a href="#">Starred</a></li>
                    <li><a href="#">Important</a></li><?php */?>
                    <li><a href="<?php echo base_url().'crt/message/sentmail' ?>">Sent Mail</a></li>
                    <li><a href="<?php echo base_url().'crt/message/trash' ?>">Trash</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
               
               <div class="row upload_sec">
               
               
                 <div class="col-xs-6">
                    <form id="myForm" action="<?php echo base_url().'crt/message/upload' ?>" method="post" enctype="multipart/form-data">
                         <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                         <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                         <input type="submit" class="hidden" value="Ajax File Upload">
                     </form>                                                   
                </div>
                 
                 <div class="col-xs-6">
                    <div id="progress" style="display: none;">
                        <div id="bar"></div>
                        <div id="percent">0%</div >
                    </div>
                </div>
                
                
            </div>
               
               

                <form action='' name="frm-compose-message" id="frm-compose-message" method='post' class="form-horizontal"

                 role="form" validate>

                    <div class="form-group">

                     <div class="col-sm-12">

                            <select class="form-control" name="message_receiver" id="message_receiver">

                            <option value=""> Select </option>

                            <?php

                                $my_team=$my_class->fetch_team_members();

                                if(count($my_team) > 0)

                                {

                                    foreach($my_team as $team)

                                    {

                            ?>

                                    <option value="<?php echo $team['login_id'].' '.$this->master_model->decryptIt($team['crt_email']).' '.$this->master_model->decryptIt($team['crt_first_name']).' '.$this->master_model->decryptIt($team['crt_last_name']) ?>" <?php if($this->uri->segment(4)!="" && base64_decode($this->uri->segment(4))==$team['login_id']){echo 'selected="selected"';} ?>>

                                    <?php echo $this->master_model->decryptIt($team['crt_first_name']).' '.$this->master_model->decryptIt($team['crt_last_name']); ?>

                                    </option>

                            <?php

                                    }

                                }

                            ?>

                           </select>							

                        </div>

                    </div>

                    

                    <div class="form-group">
    
                        <div class="col-sm-12 upload_place">
    
                        <input type="text" class="form-control" id="subject" name="subject" value="" placeholder="Subject" data-msg-required="Please enter last name" ><?php echo form_error('subject');  ?>
                         <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                        </div>

                    </div>

                    <div class="form-group">
    
                        <div class="col-sm-12">
    
                        	<a href="javascript:;" onclick="$('.upload_sec').show(); $(this).after('&nbsp;').hide();" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>
    
                        </div>

                    </div>

                    
                    <div class="form-group">
    
                        <div class="col-xs-12">
    
                        	<div id="file_up_text" style="display: none;">File Attached: <div id="message"><a href=""></a></div></div>
    
                        </div>

                    </div>

                    

                    <div class="form-group hidden">

                        <div class="col-sm-12">

                            <textarea class="form-control" id="txt_message" name="txt_message" placeholder="Enter message"></textarea><?php echo form_error('txt_message'); ?>							

                        </div>

                    </div>

                    
                    <div class="form-group">

                        <div class="col-sm-12">

                            <!-- Summernote CMS Example -->
                            <div id="summernote"></div>
            

                        </div>

                    </div>

                    

                    <div class="form-group">

                        <div class="col-sm-12">

                        <button type="submit" class="btn btn-primary"   name="send_msg" id="send_msg" value='0' >Send</button>

                        </div>

                    </div>	

                                    

                </form>		               
           
               
            </div>
        </div>
        
    </div>




</div><!--.row-->




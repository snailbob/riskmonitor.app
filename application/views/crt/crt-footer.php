            </div>

        </div>

    </div>
    <!-- /.container -->


    <hr />
    <div class="container-fluid">

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-muted" style="padding-bottom: 20px;">
		<?php
            //select org
            $theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));

        if ($theorg[0]['cust_type'] == '0'){
        ?>	
						
                        <span class="text-muted pull-right">Powered	by <a href="<?php echo base_url() ?>" target="_blank">Crisisflo</a></span>
        <?php }
		else{ ?>
        
        
                    	<span class="pull-left">© <span class="curr_year"></span> CrisisFlo</span>
		<?php } ?>	
                        
                        
                    </p>
                    
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
    

    
		<!--chat pesonal message list-->	
        <div class="chat-collapse pull-left" id="show_chat" onClick="$('#page-wrap').show();$('#show_chat').hide();" style="height: 43px;">
            <button type="button" class="close" title="Expand Chat" onClick="$('#page-wrap').hide();$('#show_chat').show();" style="margin-left: 5px;"><i class="fa fa-plus-square-o small"></i></button>
           <b class="text-muted"><i class="fa fa-circle text-green"></i> Online (<span id="number_online">0</span>) 
           <span class="badge red pull-right" id="coll_count_unread" style="display: none;"><i class="fa fa-bell fa-fw"></i> <span id="unread_number"></span></span>
           </b>
           <audio id="audiotag1" src="<?php echo base_url() ?>assets/2/audio/flute_c_long_01.wav" preload="auto"></audio>   
            </p><div style="clear: both;"></div>
        </div>
        <div id="page-wrap" style="display: none;">
			<p style="padding-right:5px;">
            <span class="pull-left small text-muted" id="count_online" style="margin-left: 5px;">
            
            </span>
            <button type="button" class="close" title="Collapse Chat" onClick="$('#page-wrap').hide();$('#show_chat').show();"> <i class="fa fa-minus-square-o small"></i></button></p><div style="clear: both;"></div>
            <p id="name-area"></p>
            
            <div id="chat-wrap"><div id="chat-user-list">

                <ul class="list-unstyled" id="results">
                    <li class="text-muted text-center hidden"><i class="fa fa-refresh fa-spin"></i> Loading..</li>
                </ul>

            </div></div>

        
        </div>
        
        
        
		<!--chat pesonal message view-->	
        <div class="chat-collapse-pm pull-left" id="show_chat_pm" style="display: none;" onClick="$('#page-wrap-pm').show();$('#show_chat_pm').hide();$('#sendie').focus();">
            <button type="button" class="close pull-left" title="Close Chat" style="z-index: 15;padding-left:2px; padding-right: 2px;" onClick="event.stopPropagation();$('#show_chat_pm').hide();removeChatPm();">&times;</button>
           <b id="chat_username_bottom"></b>
            <button type="button" class="close" title="Expand Chat" style="z-index: 15;"><i class="fa fa-plus-square-o small"></i></button>
        </div>
        
        <div id="page-wrap-pm" style="display: none;">
			<p style="padding-right:5px;">
            <span class="pull-left small text-muted" id="chat_username_top" style="margin-left: 5px; font-weight: bold;">
            </span>
            <button type="button" class="close" title="Collapse Chat" onClick="$('#page-wrap-pm').hide();$('#show_chat_pm').show();"><i class="fa fa-minus-square-o small"></i></button></p><div style="clear: both;"></div>
            <p id="name-area"></p>
            
            <div id="chat-wrap"><div id="chat-area">
            	<h4 class="text-muted text-center lead hidden">You're disconnected.</h4>
            </div></div>
            
            <form id="send-message-area" class="form-horizontal">
                <textarea rows="1" id="sendie" maxlength = "600"></textarea>
                <!--<span style="position: absolute; bottom: 1px; right: 1px; background:#eee; padding:10px;"><i class="fa fa-send"></i></span>-->
            </form>
        
        </div>
        

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>
    
    <script src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>
   
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/wysiwyg-demo.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/plugins/jquery.form/jquery.form.js"></script>
    
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
	<script src="https://www.google.com/jsapi"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/jquery.geocomplete/jquery.geocomplete.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/oms/oms.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flipclock/flipclock.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/momentjs/moment.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/momentjs/moment-timezone-with-data.js"></script>
    
    
    <script src="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/jquery-ui.min.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/bootstrap-datetimepicker.min.js"></script>


    <script src="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.min.js"></script>


    <script src="<?php echo base_url()?>assets/2/js/chat-im-crt.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/cc-validation.js"></script>

    
    <script src="<?php echo base_url()?>assets/2/js/crt-script.js"></script>
    
    
    
    <?php if($this->uri->segment(2) == 'settings' && $this->uri->segment(3) == 'verifications') { ?>
    <script>
			
		// Handle the successful return from the API call
		function onSuccess(data) {
			console.log(data);
			
			$('#MyLinkedInButton').addClass('disabled');
			var dataa = {
				"email": data.emailAddress,
				"id": data.id,
				"type": 'linkedin'
			};
			
			dataa = $.param(dataa); // $(this).serialize() + "&" + 
			
			$.post(
				base_url+'crt/settings/add_connect',
				dataa,
				function(res){
					console.log(res);
					$('#MyLinkedInButton').removeClass('disabled');
					$('#MyLinkedInButton').addClass('hidden').siblings().removeClass('hidden');

				},
				'json'
			);
			
		}
		
		// Handle an error response from the API call
		function onError(error) {
			console.log(error);
		}

		
		$(document).ready(function(e) {
			
			
			$("#MyLinkedInButton").bind('click',function () {IN.User.authorize(); return false;});
			
			IN.Event.on(IN, 'auth', function(){
				/* your code here */
				//for instance

				console.log('auth success');
				IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);


			});
			
			$('.gplus_connected_btn').on('click', function(){
				var $self = $(this);
				var type = 'Google+';
				var connection_type = 'gplus';
				
				bootbox.confirm('<p class="lead">Confirm you wish to disconnect '+type+'.</p>', function(e){
					if(e){
	
						var dataa = {
							"type": connection_type //'linkedin'
						};
						
						dataa = $.param(dataa); // $(this).serialize() + "&" + 
	
						$.post(
							base_url+'crt/settings/remove_connect',
							dataa,
							function(res){
								console.log(res);
								$self.addClass('hidden').siblings().removeClass('hidden');
							},
							'json'
						).error(function(err){
							console.log(err);
						});
					}
				});
			});
			
			
			$('.linkedin_connected_btn').on('click', function(){
				var $self = $(this);
				var type = 'LinkedIn';
				var connection_type = 'linkedin';

				
				bootbox.confirm('<p class="lead">Confirm you wish to disconnect '+type+'.</p>', function(e){
					if(e){
	
						var dataa = {
							"type": connection_type //'linkedin'
						};
						
						dataa = $.param(dataa); // $(this).serialize() + "&" + 
	
						$.post(
							base_url+'crt/settings/remove_connect',
							dataa,
							function(res){
								console.log(res);
								$self.addClass('hidden').siblings().removeClass('hidden');
							},
							'json'
						).error(function(err){
							console.log(err);
						});
					}
				});
			});
			
		
		});
		
	
	</script>
    <?php } //end for linkedin?>
        
    
    
    
	<?php
	//run chart js if open incident > 0, recall module, and for particular pages to avoid js error
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );
	$cc_id = $this->session->userdata('team_cc');
	$user_access = $this->common_model->user_access($this->session->userdata('logged_crt_login_id'));
	$org_id = $this->session->userdata('cc_selected_orgnaization');
	$active_module = substr($this->session->userdata('org_module'), 0, 1);
	$the_uri_5 = 0;
	
	if($this->uri->segment(5) != ''){
		$the_uri_5 = $this->uri->segment(5);
	}
	

	if($this->uri->segment(2) == 'log'){
		$inci_stat = '1';
	}
	else{
		$inci_stat = '0';
	}

	$whr_inci = array(
		'cc_id'=>$cc_id,
		'org_id'=>$org_id,
		'closed'=>$inci_stat
	);
	
	
	if($active_module == '5'){
		$incidents = $this->master_model->getRecords('cf_recall', $whr_inci, '*', array('id'=>'DESC'));

	}
	else{ //continuity = 8
		$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci, '*', array('id'=>'DESC'));

	}
	
	
	if (strpos($the_active_module[0]['active_module'], '0') !== false && count($incidents) > 0){ //kpi is active
	
	
		if($active_module == '5'){
			$recall_tasks_count = $this->master_model->getRecordCount('cf_recall_steps', array('recall_id'=>$incidents[$the_uri_5]['id']));
	
		}
		else{ //continuity = 8
			$recall_tasks_count = $this->master_model->getRecordCount('cf_continuity_steps', array('recall_id'=>$incidents[$the_uri_5]['id']));

		}
	
	
		if($recall_tasks_count > 0){
	
	
			if(((in_array('kpi', $user_access) && $this->uri->segment(2) == 'dashboard') && ($this->uri->segment(3) == 'index' || $this->uri->segment(3) == '')) || $this->uri->segment(2) == 'log') {?>
            
            
    <!-- Flot Charts -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/flot/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/cc-flot-chart-kpi.js"></script>
 
 
    <?php
			}//dash and kpi
		}//if task is there
	}//kpi is active
	?>
    
    
    
</body>

</html>

<?php

if($this->session->userdata('logged_crt_login_id')==""){
	redirect(base_url().'crt');

}
else if($this->session->userdata('org_module')==""){
	redirect('crt/dashboard/selectmodule');
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CrisisFlo</title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->

    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?>
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
		<link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">

    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">

	<link href="<?php echo base_url()?>assets/2/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote.css" rel="stylesheet">
    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">-->
    <link href="<?php echo base_url()?>assets/2/css/plugins/flipclock/flipclock.css" rel="stylesheet">

    <!-- Popup css-->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

    <!-- full calendar css-->
    <link href="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/js/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">


    <!-- datetimepicker css-->
	<link href="<?php echo base_url()?>assets/2/css/plugins/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">


    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">


    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->

	<script type="text/javascript" src="https://platform.linkedin.com/in.js">
	  api_key: 75eekrdxppqx8g
    </script>



	<?php
    /*GET THE DATA FROM THE SESSION**************************/

		//admin help
		$adminhelp = $this->master_model->getRecords('admin_login', array('id'=>'1'));

		$org_id = $this->session->userdata('crt_selected_orgnaization');
		//active modules
		$main_active_module = substr($this->session->userdata('org_module'), 0,1);
		$active_feature_modules = $this->common_model->active_module($org_id);

		$active_module =  $main_active_module.$active_feature_modules;

		$the_active_module = array('0'=>array('active_module'=>$active_module));

		//select org

		$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));

	    $session_userfname = $this->session->userdata('logged_display_name');

		$crt_id = $this->session->userdata('logged_crt_login_id');
		$availability_status = $this->common_model->availability($crt_id);

		if($availability_status == 'Available'){
			$availability_class = 'background: #d6e9c6; color: #3c763d';
		}
		else{
			$availability_class = 'background: #e4b9b9; color: #e74c3c;';
		}


		if (strpos($active_module, 'b') !== false){
			$availability_visible = '';
		}
		else{
			$availability_visible = 'hidden';
		}

//		print_r($availability_status);
//		return false;

		if($this->session->userdata('location') !=''){
			$mylocation = $this->session->userdata('location');
			$myloc_class = 'background: #fff;';
		}
		else{
			$mylocation = 'Enter your location';
			$myloc_class = 'background: #e4b9b9; color: #e74c3c;';
		}

		$mytimezone = $this->master_model->getRecords('time_zone',array('ci_timezone'=>$this->session->userdata('timezone')));


		$nowtime = $this->common_model->userdatetime();

		$date_zone = date_format(date_create($nowtime), "d F Y D"); //gmdate("d F Y D", $thetimeyeah); //g:i:sa d/m/Y
		$time_zone = date_format(date_create($nowtime), "Y-m-d g:i:s a"); // gmdate("Y-m-d g:i:s a", $thetimeyeah);

        //active case report count
        $num_report = $this->master_model->getRecordCount('case_master',array('crt_id'=>$this->session->userdata('logged_crt_login_id'),'cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'0'));

	//unread forum post count
	$forum_vstr_date = $this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$this->session->userdata('logged_crt_login_id')));
	$forum_post=$this->master_model->getRecords('forum_post_master',array('cc_id'=>$this->session->userdata('team_cc'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'date_created >'=>$forum_vstr_date[0]['forum_last_visit'],'post_parent'=>'0'));




	if(substr($this->session->userdata('org_module'), 0,1) == '5'){
		$module_name = 'Recall Manager';
	}
	else if(substr($this->session->userdata('org_module'), 0,1) == '1'){
		$module_name = 'Crisis Manager';
	}

	else if(substr($this->session->userdata('org_module'), 0,1) == '2'){
		$module_name = 'Case Manager';
	}

	else if(substr($this->session->userdata('org_module'), 0,1) == '4'){
		$module_name = 'Member Safety';
	}

	else if(substr($this->session->userdata('org_module'), 0,1) == '8'){
		$module_name = 'Continuity Manager';
	}
    /*********************************************************/?>


	<script type="text/javascript" language="javascript">
		//declare variables
		var base_url='<?php  echo base_url(); ?>';
		var recall_time;
		var mydatetime = '<?php echo $time_zone ?>';

		var uri_2 = "<?php echo $this->uri->segment(2) ?>";
		var uri_3 = "<?php echo $this->uri->segment(3) ?>";
		var uri_4 = "<?php echo $this->uri->segment(4) ?>";
		var uri_5 = "<?php echo $this->uri->segment(5) ?>";
		var uri_6 = "<?php echo $this->uri->segment(6) ?>";
		var cc_id = "<?php echo $this->session->userdata('team_cc') ?>";
		var org_id = "<?php echo $this->session->userdata('cc_selected_orgnaization') ?>";
		var user_full_name = "<?php echo $this->session->userdata('logged_display_name') ?>";
		var now_time = "<?php echo date("F j, Y, g:i a") ?>";
		var my_location = "<?php echo $this->session->userdata('location'); ?>";
    </script>

</head>

<body>

    <script type="text/javascript">
		if(uri_2 == 'settings' && uri_3 == 'verifications'){

			  //GOOGLE PLUS
			  // Enter a client ID for a web application from the Google Developer Console.
			  // The provided clientId will only work if the sample is run directly from
			  // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
			  // In your Developer Console project, add a JavaScript origin that corresponds to the domain
			  // where you will be running the script.
			  var clientId = '1041052586833-p5gn0f5828unp0ve0t8i0girpp8mgba8.apps.googleusercontent.com';

			  // Enter the API key from the Google Develoepr Console - to handle any unauthenticated
			  // requests in the code.
			  // The provided key works for this sample only when run from
			  // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
			  // To use in your own application, replace this API key with your own.
			  var apiKey = 'AIzaSyCxXrGNIjpnWIf6FlqbbwlSp-oYYgfTADU'; //AIzaSyAVK5bSoKOZh4tjiy3NpJPFXzc8qCktm4w';

			  // To enter one or more authentication scopes, refer to the documentation for the API.
			  var scopes = 'https://www.googleapis.com/auth/plus.profile.emails.read'; //plus.me';

			  // Use a button to handle authentication the first time.
			  function handleClientLoad() {
				gapi.client.setApiKey(apiKey);
				window.setTimeout(checkAuth,1);
			  }

			  function checkAuth() {
				gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true} , handleAuthResult);
			  }


			  function handleAuthResult(authResult) {
				var authorizeButton = document.getElementById('authorize-button');

				 //authorizeButton.style.visibility = '';
				 // authorizeButton.onclick = handleAuthClick;
				  console.log(JSON.stringify(authResult));

					if (authResult && !authResult.error) {
					//authorizeButton.style.visibility = 'hidden';
					//	makeApiCall();
						//console.log(JSON.stringify(authResult));

					}

					else {
						//bootbox.alert('<p class="lead"><i class="fa fa-info-circle"></i> Something went wrong. Please contact admin.</p>');
					}
			  }

			  function handleAuthClick(event) {
				gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, makeApiCall);
				//handleAuthResult);
				//makeApiCall();

				return false;
			  }

			  // Load the API and make an API call.  Display the results on the screen.
			  function makeApiCall() {
				gapi.client.load('plus', 'v1', function() {
				  var request = gapi.client.plus.people.get({
					'userId': 'me'
				  });
				  request.execute(function(resp) {
						//console.log(JSON.stringify(resp));
						console.log(resp.emails[0].value);
						//console.log(resp.displayName);
						console.log(resp.id);
						console.log(resp.name.givenName);
						console.log(resp.name.familyName);
						console.log(resp.image.url);
						//console.log(resp.placesLived[0].value);

						$('.gplus_btn').addClass('disabled');
						var dataa = {
							"email": resp.emails[0].value,
							"id": resp.id,
							"type": 'gplus'
						};

						dataa = $.param(dataa); // $(this).serialize() + "&" +

						$.post(
							base_url+'crt/settings/add_connect',
							dataa,
							function(res){
								console.log(res);
								$('.gplus_btn').removeClass('disabled');
								$('#MyGooglePlusButton').addClass('hidden').siblings().removeClass('hidden');

							},
							'json'
						);



				  });
				});
			  }

		}


    </script>
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>



<!-- Modal for popup incidents list -->
<div class="modal fade" id="myLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Location</h4>
      </div>
      <div class="modal-body">
      	<form id="up_loc_form">
        	<div class="form-group">
            	<label></label>
                <input type="text" class="form-control update_location" name="up_location" id="up_location" placeholder="<?php echo $mylocation; ?>" />
                <input type="hidden" name="lat" />
                <input type="hidden" name="lng" />
                <span class="control-helper text-red" style="display: none;">Mandatory field.</span>
            </div>
        	<div class="form-group">
                <button class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
	   <?php
        if ($theorg[0]['cust_type'] == '0'){
        ?>

                <a class="navbar-brand hidden-xs preloadThis" id="site_logo" href="<?php echo base_url().'crt/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/AIG_rebrand_logo.gif" width="114" height="62" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url().'crt/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/AIG_rebrand_logo.gif" width="88" height="48" style="margin-top: -10px;"></a>
        </div>

            <div class="module-name hidden-xs">
                <p class="text-primary" style="text-align: center"><span style="font-size: 130%; font-weight: bold;">Crisis Command Centre</span><br><?php echo $module_name; ?></p>
            </div>

            <div class="navbar-header col-lg-1 col-lg-offset-1 col-md-10 col-lg-md-1 visible-xs" >
                <h5 class="big text-primary" style="text-align: center">Crisis Command Centre - <?php echo $module_name; ?></h5>
            </div>
	   <?php }
       else{
        ?>
                <a class="navbar-brand hidden-xs preloadThis" id="site_logo" href="<?php echo base_url().'crt/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="202" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url().'crt/dashboard'?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="161" height="48" style="margin-top: -15px;"></a>
        </div>

            <div class="module-name hidden-xs">
                <p class="text-primary" style="text-align: center"><span style="font-size: 130%; font-weight: bold;">Crisis Command Centre</span><br><?php echo $module_name; ?></p>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 visible-xs" >
                <h5 class="big text-primary" style="text-align: center">Crisis Command Centre - <?php echo $module_name; ?></h5>
            </div>



		<?php } ?>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#"></a>
                    </li>
                   <li class="page-scroll"> <a href="<?php echo base_url().'crt/dashboard' ?>" class="visible-xs  active"><i class="fa fa-fw fa-home"></i> Home</a></li>
                    <?php

						$unread=$this->common_model->get_unread_count($this->session->userdata('logged_crt_login_id'));

					?>
                   <li class="page-scroll"> <a href="<?php echo base_url().'crt/message/inbox' ?>" class="visible-xs <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-fw fa-envelope"></i> Messages <strong><?php if($unread > 0){echo '<span class="badge danger pull-right">'.$unread.'</span>';} ?></strong></a></li>




					<?php //Recall feature cost monitor module activated
						if (strpos($active_module, '6') !== false && strpos($active_module, '5') !== false ){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url().'crt/costmonitor' ?>" class="hidden <?php if($this->uri->segment(2)=="costmonitor"){echo "active";}?>"><i class="fa fa-fw fa-usd"></i> Cost Monitor</a></li>


					<?php
						}
					?>





					<?php //display this module if activated
						if (strpos($active_module, '1') !== false){
							?>


                   <li class="page-scroll"> <a href="javascript:void(0);" class="hidden"><i class="fa fa-fw fa-fire"></i> Pre-Incident Planning</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-fw fa-user"></i> Response Team</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>crt/document/manage" class="visible-xs "><i class="fa fa-fw fa-paperclip"></i> Recall Documents</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>crt/scenario/managetask" class="visible-xs "><i class="fa fa-fw fa-lightbulb-o"></i> Response Plan</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>crt/standbymessage/manage" class="visible-xs "><i class="fa fa-fw fa-folder-open"></i> Standby Message</a></li>
                    <li class="page-scroll"><a href="<?php echo base_url()?>crt/forum" class="visible-xs"><i class="fa fa-flag fa-fw"></i> Forums <?php if(count($forum_post) > 0){echo '<strong><span class="badge danger pull-right">'.count($forum_post).'</span></strong>';} ?>
                    </a></li>
                   <?php } ?>


					<?php //display this module if activated
						if (strpos($active_module, '5') !== false && (strpos($active_module, '1') === false) ){ ?>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-fw fa-user"></i> Response Team</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>crt/document/manage" class="visible-xs "><i class="fa fa-fw fa-paperclip"></i> Recall Documents</a></li>
                    <li class="page-scroll"><a href="<?php echo base_url()?>crt/forum" class="visible-xs"><i class="fa fa-flag fa-fw"></i> Forums <?php if(count($forum_post) > 0){echo '<strong><span class="badge danger pull-right">'.count($forum_post).'</span></strong>';} ?>
                    </a></li>
                   <?php } ?>


					<?php //display this module if activated
						if (strpos($active_module, '2') !== false  && strpos($active_module, '5') === false && (strpos($active_module, '1') === false) ){ ?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="visible-xs"><i class="fa fa-fw fa-user fa-fw"></i> Response Team</a></li>

                   <?php } ?>


					<?php //display this module if activated
						if (strpos($active_module, '2') !== false){
							?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>crt/managecase" class="visible-xs"><i class="fa fa-fw fa-life-ring"></i> Case Management
                    <?php
						if($num_report > 0){echo '<strong><span class="badge danger pull-right">'.$num_report.'</span></strong>';}
					?>
                   </a></li>

                   <?php } ?>

					<?php //display this module if activated
						if (strpos($active_module, 'b') !== false){
							?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>crt/availability" class="visible-xs"><i class="fa fa-fw fa-calendar"></i> Availability
                   </a></li>
                   <?php } ?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>crt/log" class="visible-xs"><i class="fa fa-fw fa-edit"></i> Post-Incident Review</a></li>


                   <li class="dropdown hidden-xs">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down fa-fw"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>crt/settings/changePassword"><i class="fa fa-fw fa-key"></i> Change Password</a></li>
                        <li><a href="<?php echo base_url() ?>crt/settings/updateinfo"><i class="fa fa-fw fa-info"></i> Update Details</a></li>
                        <?php if ($this->session->userdata('crt_consultant_id') !='') {?>
                        <li><a href="<?php echo base_url() ?>crt/dashboard/selectorganization"><i class="fa fa-fw fa-building-o"></i> Change Organization</a></li>
                        <?php } ?>
                        <!-- <li><a href="<?php echo base_url() ?>crt/dashboard/selectmodule"><i class="fa fa-fw fa-eraser"></i> Change Module</a></li> -->
                        <li><a href="<?php echo base_url() ?>crt/settings/verifications"><i class="fa fa-google-plus fa-fw"></i> Verifications</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>crt/dashboard/logout/"><i class="fa fa-fw fa-sign-out"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>


                      </ul>
                    </li>

                    <li class="visible-xs"><a href="<?php echo base_url(); ?>crt/settings/changePassword"><i class="fa fa-fw fa-key"></i> Change Password</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>crt/settings/updateinfo"><i class="fa fa-fw fa-info"></i> Update Details</a></li>
					<?php if ($this->session->userdata('crt_consultant_id') !='') {?>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>crt/dashboard/selectorganization"><i class="fa fa-fw fa-building-o"></i> Change Organization</a></li>
                    <?php } ?>
                    <!-- <li class="visible-xs"><a href="<?php echo base_url() ?>crt/dashboard/selectmodule"><i class="fa fa-fw fa-eraser"></i> Change Module</a></li> -->
                    <li class="visible-xs"><a href="<?php echo base_url() ?>crt/settings/verifications"><i class="fa fa-google-plus fa-fw"></i> Verifications</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url();?>crt/dashboard/logout/"><i class="fa fa-fw fa-sign-out"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>


                    <?php /*?><li class="dropdown dropdown-large visible-sm visible-md"><!--menu for small devices-->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-bars"></i></a>

                        <ul class="dropdown-menu dropdown-menu-large row">
                            <li class="col-sm-6">
                                <ul>
                                       <li> <a href="<?php echo base_url().'crt/dashboard' ?>" class="dropdown-header preloadThis <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-fw fa-home"></i> Home</a></li>

                                    <li class="divider"></li>

                                           <?php
                                            $unread=$this->common_model->get_unread_count($this->session->userdata('logged_crt_login_id'));
                                        ?>
                                       <li> <a href="<?php echo base_url().'crt/message/inbox' ?>" class="dropdown-header preloadThis <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-fw fa-envelope"></i>  Messages <strong><?php if($unread > 0){echo '<span class="badge danger pull-right" style="margin-top: 3px;">'.$unread.'</span>';} ?></strong></a></li>


					<?php //Recall feature cost monitor module activated
						if (strpos($active_module, '6') !== false && strpos($active_module, '5') !== false ){ ?>
                                    <li class="hidden divider"></li>

                                   <li> <a href="<?php echo base_url().'crt/costmonitor' ?>" class="hidden dropdown-header preloadThis <?php if($this->uri->segment(2)=="costmonitor"){echo "active";}?>"><i class="fa fa-fw fa-usd"></i>  Cost Monitor</a></li>


					<?php
						}
					?>

					<?php //display this module if activated
						if (strpos($active_module, '1') !== false){
							?>
                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-fw fa-fire"></i> Pre-Incident Planning</li>

                                   <li> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="preloadThis">Response Team</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/document/manage" class="preloadThis">Recall Documents</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/scenario/managetask" class="preloadThis">Response Plan</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/standbymessage/manage" class="preloadThis">Standby Message</a></li>


                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url()?>crt/forum" class="dropdown-header preloadThis"><i class="fa fa-flag fa-fw"></i> Forums <?php if(count($forum_post) > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px">'.count($forum_post).'</span></strong>';} ?>
                                    </a></li>

					<?php } ?>

					<?php //display this module if activated
						if (strpos($active_module, '5') !== false && (strpos($active_module, '1') === false)){ ?>
                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-fw fa-fire"></i> Pre-Incident Planning</li>

                                   <li> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="preloadThis">Response Team</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/document/manage" class="preloadThis">Recall Documents</a></li>

                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url()?>crt/forum" class="dropdown-header preloadThis"><i class="fa fa-flag fa-fw"></i> Forums <?php if(count($forum_post) > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px">'.count($forum_post).'</span></strong>';} ?>
                                    </a></li>
					<?php } ?>

					<?php //display this module if activated
						if (strpos($active_module, '2') !== false && strpos($active_module, '5') === false && (strpos($active_module, '1') === false)){
							?>


                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-fw fa-fire fa-fw"></i> Pre-Incident Planning</li>

                                   <li> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="preloadThis">Response Team</a></li>

					<?php } ?>



                                </ul>
                            </li>



                            <li class="col-sm-6">
                                <ul>

					<?php //display this module if activated
						if (strpos($active_module, '2') !== false){
							?>


                                   <li> <a href="<?php echo base_url()?>crt/managecase" class="preloadThis dropdown-header"><i class="fa fa-fw fa-life-ring"></i> Case Management
                    <?php
						if($num_report > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px">'.$num_report.'</span></strong>';}
					?>
                                   </a></li>
                                   <li class="divider"></li>

                     <?php } ?>


					<?php //display this module if activated
						if (strpos($active_module, 'b') !== false){
							?>

                                   <li> <a href="<?php echo base_url()?>crt/availability" class="preloadThis dropdown-header"><i class="fa fa-fw fa-calendar"></i> Availability
                                   </a></li>

                                   <li class="divider"></li>
                    <?php } ?>



					<?php //display this module if activated
						if (strpos($active_module, '3') !== false){
							?>

                                   <li> <a href="<?php echo base_url()?>crt/conference" class="preloadThis dropdown-header"><i class="fa fa-fw fa-video-camera fa-fw"></i> Video Conferencing</a></li>

                                   <li class="divider"></li>

                     <?php } ?>

                                   <li> <a href="<?php echo base_url()?>crt/log" class="preloadThis dropdown-header"><i class="fa fa-fw fa-edit"></i>  Post-Incident Review</a></li>

                                    <li class="divider"></li>

                                                                        <li class="dropdown-header"><i class="fa fa-fw fa-gear"></i> Settings</li>

                                    <li><a href="<?php echo base_url(); ?>crt/settings/changePassword" class="preloadThis">Change Password</a></li>
                                    <li><a href="<?php echo base_url() ?>crt/settings/updateinfo" class="preloadThis">Update Details</a></li>

								<?php if ($this->session->userdata('crt_consultant_id') !='') {?>
                                    <li><a href="<?php echo base_url() ?>crt/dashboard/selectorganization" class="preloadThis">Change Organization</a></li>
								<?php } ?>
                                    <li><a href="<?php echo base_url() ?>crt/dashboard/selectmodule" class="preloadThis">Change Module</a></li>

                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url();?>crt/dashboard/logout/" class="dropdown-header"><i class="fa fa-fw fa-sign-out"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>
                                </ul>
                            </li>



                            <li class="col-sm-6">
                                <ul>

                                </ul>
                            </li>


                        </ul>

                    </li><!---.end of large dropdown-->
                     <?php */?>
                </ul>

                <div class="pull-right dropdown digital_clock hidden-xs">
                    <a href="javascript:;" style="text-decoration: none; color: #333;">
                        <span class="small">Basecamp time/date:</span>
                        <span class=""><p class="user-clock-holder"><span class="user-clock"></span></p></span>
                        <span class="hidden"><div class="clock my_clock"></div></span>
                        <div class="" style="position: absolute; top: 47px;"><?php echo $date_zone; ?></div>
                    </a>

                </div>


            </div>
            <!-- /.navbar-collapse -->


				<?php //status and org
                $main_status=$this->common_model->get_main_status($this->session->userdata('logged_parent_crt'),$this->session->userdata('crt_selected_orgnaization')); ?>

                <div class="topLabel bg-info">

                    <div class="pull-left megamenu_clone hidden-xs hidden-lg hidden-md" style="margin-left: 10px; margin-right: 10px;">

                    </div>

                	<div class="hidden-md hidden-lg">
                      <div class="scroller scroller-left" style="display: none;"><i class="glyphicon glyphicon-chevron-left"></i></div>
                      <div class="scroller scroller-right" style="display: block;"><i class="glyphicon glyphicon-chevron-right"></i></div>
                    </div>
                  <div class="wrapper visible-sm visible-xs">
                    <ul class="nav list" id="myTab" style="left: 0px;">


                        <li class="">
                            <a href="<?php echo base_url().'crt/dashboard'; ?>" class="btn btn-default btn-sm">
                                <i class="fa fa-home fa-fw"></i>
                            </a>
                        </li>

                        <li class="">
                            <a href="<?php echo base_url().'crt/message/inbox'; ?>" class="btn btn-default btn-sm" <?php if ($unread > 0) { echo 'style="opacity: 1; background: #e4b9b9; color: #e74c3c;"';} ?>>
                                <?php if ($unread > 0) { echo $unread; }  ?><i class="fa fa-envelope fa-fw"></i>
                            </a>
                        </li>

                        <li class="">
                            <a href="<?php echo base_url().'crt/forum'; ?>" class="btn btn-default btn-sm" <?php if (count($forum_post) > 0) { echo 'style="opacity: 1; background: #e4b9b9; color: #e74c3c;"';} ?>>
                                <?php if (count($forum_post) > 0) { echo count($forum_post); }  ?><i class="fa fa-flag fa-fw"></i>
                            </a>
                        </li>



                      <li class="">
                        	<a class="btn btn-default btn-sm disabled" style="opacity: 1; background: #fff; cursor: default;">
                            	<i class="fa fa-user"></i> Logged in as : <?php echo $session_userfname; ?>
                            </a>

                      </li>
                      <li class="">
                        	<a class="btn btn-default btn-sm disabled" style="opacity: 1; background: #fff; cursor: default;">
                                <i class="fa fa-building-o"></i> Organization : <?php echo $this->session->userdata('crt_selected_organization_name'); ?>
                            </a>
                      </li>
                      <li class="<?php echo $availability_visible ?>">
                        	<a href="<?php echo base_url().'crt/availability' ?>" style="opacity: 1; <?php echo $availability_class ?>">
                            	<?php echo $availability_status; ?>
                            </a>
                      </li>
                      <li class="">
                        	<a href="javascript:;" title="<?php echo $mylocation; ?>" class="btn btn-default btn-sm" style="opacity: 1; <?php echo $myloc_class; ?>" onClick="$('#myLocationModal').modal('show');">
                            	<i class="fa fa-map-marker fa-fw"></i> Your Location: <?php echo $mylocation; ?>
                            </a>
                      </li>

                      <li class="">
                        	 <img src="<?php echo base_url().'assets/2/img/help-icon.png' ?>" alt="" title="" class="admin_help_btn hidden"/>
                      </li>

                  </ul>
                  </div>


            		<div class="col-sm-12 hidden-xs hidden-sm">


                        <div class="statLabel hidden-xs hidden-sm">


                            <ul class="list-inline blue_btns_list">
                                <li>
                                    <div class="dropdown mega-dropdown">
                                        <a class="btn btn-default btn-sm sidemenu_btn">
                                            <i class="fa fa-bars fa-fw"></i>
                                        </a>


                                        <ul class="dropdown-menu mega-dropdown-menu row">
                                            <li class="col-sm-6">
                                            	<ul>
                                       <li> <a href="<?php echo base_url().'crt/dashboard' ?>" class="dropdown-header preloadThis <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-fw fa-home"></i> Home</a></li>

                                    <li class="divider"></li>

                                           <?php
                                            $unread=$this->common_model->get_unread_count($this->session->userdata('logged_crt_login_id'));
                                        ?>
                                       <li> <a href="<?php echo base_url().'crt/message/inbox' ?>" class="dropdown-header preloadThis <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-fw fa-envelope"></i>  Messages <strong><?php if($unread > 0){echo '<span class="badge danger pull-right" style="margin-top: 3px;">'.$unread.'</span>';} ?></strong></a></li>


					<?php //Recall feature cost monitor module activated
						if (strpos($active_module, '6') !== false && strpos($active_module, '5') !== false ){ ?>
                                    <li class="hidden divider"></li>

                                   <li> <a href="<?php echo base_url().'crt/costmonitor' ?>" class="hidden dropdown-header preloadThis <?php if($this->uri->segment(2)=="costmonitor"){echo "active";}?>"><i class="fa fa-fw fa-usd"></i>  Cost Monitor</a></li>


					<?php
						}
					?>

					<?php //display this module if activated
						if (strpos($active_module, '1') !== false){
							?>
                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-fw fa-fire"></i> Pre-Incident Planning</li>

                                   <li> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="preloadThis">Response Team</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/document/manage" class="preloadThis">Recall Documents</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/scenario/managetask" class="preloadThis">Response Plan</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/standbymessage/manage" class="preloadThis">Standby Message</a></li>


                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url()?>crt/forum" class="dropdown-header preloadThis"><i class="fa fa-flag fa-fw"></i> Forums <?php if(count($forum_post) > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px">'.count($forum_post).'</span></strong>';} ?>
                                    </a></li>

					<?php } ?>

					<?php //display this module if activated
						if (strpos($active_module, '5') !== false && (strpos($active_module, '1') === false)){ ?>
                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-fw fa-fire"></i> Pre-Incident Planning</li>

                                   <li> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="preloadThis">Response Team</a></li>
                                   <li> <a href="<?php echo base_url(); ?>crt/document/manage" class="preloadThis">Recall Documents</a></li>

                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url()?>crt/forum" class="dropdown-header preloadThis"><i class="fa fa-flag fa-fw"></i> Forums <?php if(count($forum_post) > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px">'.count($forum_post).'</span></strong>';} ?>
                                    </a></li>
					<?php } ?>

					<?php //display this module if activated
						if (strpos($active_module, '2') !== false && strpos($active_module, '5') === false && (strpos($active_module, '1') === false)){
							?>


                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-fw fa-fire fa-fw"></i> Pre-Incident Planning</li>

                                   <li> <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="preloadThis">Response Team</a></li>

					<?php } ?>


                                                </ul>
                                            </li><!--end first column menu-->



                                            <li class="col-sm-6">
                                            	<ul>

					<?php //display this module if activated
						if (strpos($active_module, '2') !== false){
							?>


                                   <li> <a href="<?php echo base_url()?>crt/managecase" class="preloadThis dropdown-header"><i class="fa fa-fw fa-life-ring"></i> Case Management
                    <?php
						if($num_report > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px">'.$num_report.'</span></strong>';}
					?>
                                   </a></li>
                                   <li class="divider"></li>

                     <?php } ?>


					<?php //display this module if activated
						if (strpos($active_module, 'b') !== false){
							?>

                                   <li> <a href="<?php echo base_url()?>crt/availability" class="preloadThis dropdown-header"><i class="fa fa-fw fa-calendar"></i> Availability
                                   </a></li>

                                   <li class="divider"></li>
                    <?php } ?>



					<?php //display this module if activated
						if (strpos($active_module, '3') !== false){
							?>

                                   <li> <a href="<?php echo base_url()?>crt/conference" class="preloadThis dropdown-header"><i class="fa fa-fw fa-video-camera fa-fw"></i> Video Conferencing</a></li>

                                   <li class="divider"></li>

                     <?php } ?>

                                   <li> <a href="<?php echo base_url()?>crt/log" class="preloadThis dropdown-header"><i class="fa fa-fw fa-edit"></i>  Post-Incident Review</a></li>

                                    <li class="divider"></li>

                                                                        <li class="dropdown-header"><i class="fa fa-fw fa-gear"></i> Settings</li>

                                    <li><a href="<?php echo base_url(); ?>crt/settings/changePassword" class="preloadThis">Change Password</a></li>
                                    <li><a href="<?php echo base_url() ?>crt/settings/updateinfo" class="preloadThis">Update Details</a></li>

								<?php if ($this->session->userdata('crt_consultant_id') !='') {?>
                                    <li><a href="<?php echo base_url() ?>crt/dashboard/selectorganization" class="preloadThis">Change Organization</a></li>
								<?php } ?>
                                    <!-- <li><a href="<?php echo base_url() ?>crt/dashboard/selectmodule" class="preloadThis">Change Module</a></li> -->

                                    <li><a href="<?php echo base_url() ?>crt/settings/verifications" class="preloadThis">Verifications</a></li>

                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url();?>crt/dashboard/logout/" class="dropdown-header"><i class="fa fa-fw fa-sign-out"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>


                                                </ul>
                                            </li><!--end second column menu-->
                                        </ul>

                                    </div>

                                </li>

                                <li class="">
                                    <a href="<?php echo base_url().'crt/dashboard'; ?>" class="btn btn-default btn-sm">
                                        <i class="fa fa-home fa-fw"></i>
                                    </a>
                                </li>

                                <li class="">
                                    <a href="<?php echo base_url().'crt/message/inbox'; ?>" class="btn btn-default btn-sm" <?php if ($unread > 0) { echo 'style="opacity: 1; background: #e4b9b9; color: #e74c3c;"';} ?>>
                                        <?php if ($unread > 0) { echo $unread; }  ?><i class="fa fa-envelope fa-fw"></i>
                                    </a>
                                </li>

                                <li class="">
                                    <a href="<?php echo base_url().'crt/forum'; ?>" class="btn btn-default btn-sm" <?php if (count($forum_post) > 0) { echo 'style="opacity: 1; background: #e4b9b9; color: #e74c3c;"';} ?>>
                                        <?php if (count($forum_post) > 0) { echo count($forum_post); }  ?><i class="fa fa-flag fa-fw"></i>
                                    </a>
                                </li>


                                <li>
                                    <a class="btn btn-default btn-sm disabled" style="opacity: 1; background: #fff; cursor: default;">
                                        <i class="fa fa-user"></i> Logged in as : <?php echo $session_userfname; ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-default btn-sm disabled" style="opacity: 1; background: #fff; cursor: default;">
                                        <i class="fa fa-building-o"></i> Organization : <?php echo $this->session->userdata('crt_selected_organization_name'); ?>
                                    </a>
                                </li>
                            </ul>


                        </div>

                        <div class="organisationLabel hidden-xs">


                        	<a href="<?php echo base_url().'crt/availability' ?>" class="btn btn-default btn-sm <?php echo $availability_visible ?>" style="opacity: 1; <?php echo $availability_class ?>">
                            	<?php echo $availability_status; ?>
                            </a>

                        	<a href="javascript:;" title="<?php echo $mylocation; ?>" class="btn btn-default btn-sm" style="opacity: 1; <?php echo $myloc_class; ?>" onClick="$('#myLocationModal').modal('show');">
                            	<i class="fa fa-map-marker fa-fw"></i> Your Location: <?php echo $mylocation; ?>
                            </a>

                            <a href="#" class="admin_help_btn hidden">
                                 <img src="<?php echo base_url().'assets/2/img/help-icon.png' ?>" alt="" title=""/>
                            </a>

                        </div>
                    </div>


                </div>

        </div>
        <!-- /.container -->
    </nav>


    <!-- Modal for adminHelpModal-->
    <div class="portfolio-modal modal fade" id="adminHelpModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Help</h1>
                            <hr class="star-primary">

                            <div class="well text-left">
                            	<?php echo $adminhelp[0]['help_content']; ?>

                            </div>

                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



    <div class="container-fluid main_container">

        <div class="row">


            <!--main container-->
            <!--<div class="col-lg-10 col-md-12 col-xs-12 col-sm-12 scrollable main_content" style="border-left: 1px solid #e7e7e7; width: 96%;">-->
            <div class="col-xs-12 scrollable main_content">

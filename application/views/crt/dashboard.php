<style>
.dataTables_filter/*, .dataTables_info */,.dataTables_length{display: none;}
</style>
<script>
	
	var recall_time = <?php echo $the_sec; ?>;
	console.log('<?php echo $the_sec; ?>');
	
</script>

<!-- END -->

<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<?php
	//select org
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('crt_selected_orgnaization')));
	
$main_status=$this->common_model->get_main_status($this->session->userdata('logged_parent_crt'),$this->session->userdata('crt_selected_orgnaization'));

	$user_access = $this->common_model->user_access($this->session->userdata('logged_crt_login_id'));

	$active_module = substr($this->session->userdata('org_module'), 0, 1);

?>



<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Dashboard
      
      </h1>

      <ol class="breadcrumb">

        <li class="active"><i class="fa fa-dashboard"></i> KPI Dashboard</li>
        
              <?php if((strpos($the_active_module[0]['active_module'], '1') === false) || $main_status!="Pre-Incident Phase" || count($recall) > 0 ){ ?>
      <span class="pull-right hidden-xs active">Your Crisis Coordinator is <b><?php echo $this->master_model->decryptIt($ccinfo[0]['crt_first_name']).' '.$this->master_model->decryptIt($ccinfo[0]['crt_last_name']); ?></b> - Mobile No. <b><?php echo $this->master_model->decryptIt($ccinfo[0]['crt_mobile']);?></b></span>
      <?php } ?>
      


      </ol>



    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 

<!-- end PAGE TITLE AREA -->



<div class="row">

  <div class="col-lg-12">
	<?php 

	if($this->session->flashdata('success')!="")

	{

	?>

	<div class="alert alert-success alert-dismissable">

	<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

	<strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

	<?php    

	} 

	if($this->session->flashdata('error')!="")

	{

	?>

	<div class="alert alert-danger alert-dismissable">

	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

	<strong>Error:</strong><?php echo $this->session->flashdata('error'); ?></div>

	<?php

	} 

	?>
    
    

  <?php if((strpos($the_active_module[0]['active_module'], '1') === false) || $main_status!="Pre-Incident Phase" || count($recall) > 0 ){ ?>
    <div class="well visible-xs" style=" margin-top:10px;">Your Crisis Coordinator is <b><?php echo $this->master_model->decryptIt($ccinfo[0]['crt_first_name']).' '.$this->master_model->decryptIt($ccinfo[0]['crt_last_name']); ?></b> - Mobile No. <b><?php echo $this->master_model->decryptIt($ccinfo[0]['crt_mobile']);?></b></div>
              
<?php } ?>
    
  </div>


    <!---/.response team contact list modal--->
<?php

if(($main_status=="Pre-Incident Phase" && count($recall) > 0) || $main_status!="Pre-Incident Phase" ){ ?>


<div class="col-sm-12" style=" margin-bottom: 0px;">
		<?php //Recall module activated
            if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false ){ ?>
        
        <div class="pull-left">
                    <p class="hidden-xs">
						<?php
                            $more_inci = count($open_recall) - 1;
							$my_inci_label ='';
                            if (count($open_recall) == 1){
                                $my_inci_label .= '1 Open Incident '; ?>
                               <button class="btn btn-default disabled"><?php echo $my_inci_label; ?></button> 
						
						<?php
                            }
                            if (count($open_recall) > 1){
                                $my_inci_label .= count($open_recall).' Open Incidents '; ?>
                               <button class="btn btn-primary" data-toggle="modal" data-target="#myRecalls"><?php echo $my_inci_label; ?></button> 
                        <?php
                            }
                            //echo $this->pagination->create_links(); 
                        ?>
                    
                    <span class="hidden-xs hidden-sm hidden-md"><span style="margin-left: 15px;">Incident Clock :</span></span>
                    <a class="btn btn-default pull-right visible-sm visible-md disabled inci_clock_btn" style="opacity: 1; margin-left: 5px;">
                        <i class="fa fa-clock-o fa-fw"></i> Incident Clock: <span id="ct" class="mobile_clock"></span>
                    </a>
                    
                    </p>
					<div class="hidden-xs hidden-sm hidden-md">
                    	<div class="clock my_recall_timer"></div>      
                    </div>  
        
        </div>
        <div class="visible-sm visible-xs">
            <div  style="clear: both;"></div>
        </div>
        <?php } //close recall module activated ?>
    
        <a class="btn btn-default btn-block disabled visible-xs inci_clock_btn" style="opacity: 1">
            <i class="fa fa-clock-o fa-fw"></i> Incident Clock: <span id="ct" class="mobile_clock"></span>
        </a>

        <div class="visible-xs" style="clear:both;padding-top: 5px;"></div>

      <span class="visible-xs" style="margin-bottom: 15px;">
      	<a class="btn btn-primary btn-block" href="javascript:;" onclick="teamLocalTime();">Team Contact List</a>
        <?php if ((strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false) && (count($recall) > 0)){ ?>
       <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#myRecalls"><?php echo $my_inci_label; ?></button> 
        <a class="btn btn-primary btn-block" href="javascript:;" onclick="recallMap('<?php echo $recall[0]['id']?>');">Situation Map</a>
        <?php } ?>

	  </span>
      
      <span class="hidden-xs hidden-sm">
      	<a class="btn btn-primary pull-right" href="javascript:;" style="min-width: 150px; margin-bottom: 15px;" onclick="teamLocalTime();">Team Contact List</a>
        <?php if ((strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false) && (count($recall) > 0)){ ?>
        <a class="btn btn-primary pull-right" href="javascript:;" onclick="recallMap('<?php echo $recall[0]['id']?>');" style="margin-right: 5px;min-width: 150px;">Situation Map</a>
        <?php } ?>
      </span>
      
      	<a class="btn btn-primary pull-left hidden-xs hidden-lg hidden-md" href="javascript:;" style="min-width: 150px; margin-bottom: 15px;margin-right: 5px;" onclick="teamLocalTime();">Team Contact List</a>
        <?php if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false ){ ?>
        <a class="btn btn-primary pull-left hidden-xs hidden-lg hidden-md" href="javascript:;" onclick="recallMap('<?php echo $recall[0]['id']?>');" style="margin-right: 5px;min-width: 150px;">Situation Map</a>
        <?php } ?>

</div><div style="clear: both;"></div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Response Team Contacts</h4>
      </div>
      <div class="modal-body">
          <div class="table-responsive">

            <table class="table table-hover table-bordered table-green" id="example-table">
    
                <thead>
    
                    <tr>
    
                        <th width="20%">First Name</th>
    
                        <th width="20%">Last Name</th>
    
                        <th width="20%">Position</th>
    
                        <th width="20%">Location</th>
    
                        <th width="20%">Location Time</th>

                        <th width="20%">Mobile No.</th>
    
                    </tr>
        
                    </thead>
        
                    <tbody>
              
                    <?php 
            
                    if(count($all_crt)>0)
            
                    {
            
                        foreach($all_crt as $r => $value)
            
                        {

                    ?>
        
                    <tr>
        
                        <td><?php echo $value['first_name']; ?></td>
        
                        <td><?php echo $value['last_name']; ?></td>
        
                        <td><?php echo $value['crt_position']; ?></td>
                        
                        <td><?php echo $value['location']; ?><br /><span class="crts_timex"></span></td>
                        
                        <td class="my_local_td<?php echo $value['login_id']; ?>"><?php // echo  $tz_data->timeZoneId; ?></td>

                        <td><?php echo $value['crt_mobile']; ?></td>
        
                    </tr>
                    <?php 

                        }
        
                    }
        
                    ?>
        
                    
    
                </tbody>
    
                
    
                
    
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>



<?php } ?>




<!------START CONTINUITY MANAGER OR RECALL MANAGER------>
<?php if (strpos($the_active_module[0]['active_module'], '8') !== false || strpos($the_active_module[0]['active_module'], '5') !== false){ ?>




<?php

	if (count($recall) > 0){
		

		foreach($recall as $r=>$rc){//start loop each recall
			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			if($active_mod == '5'){
				$pack_id = $rc['input_pack_id']; 
				$recall_field_name = 'recall_id'; 
				$db_name = 'cf_recall_steps'; 
			}
			else{
				$pack_id = 17; 
				$recall_field_name = 'continuity_id'; 
				$db_name = 'cf_continuity_steps'; 
			}
		
			$continuity_id = $pack_id; //17;
			if($rc['input_pack_id'] != 0){
				$myrecallpack = $this->master_model->getRecords('cf_recall_packs',array('id'=>$continuity_id)); ?>
                
                
                
                
    <div class="col-lg-12">
        
        <div class="panel panel-default" style="radius: 0px;">
        


            <div class="panel-heading">
            

                <div class="panel-title">
                    <h4>
                        <?php echo $rc['incident_no'].': '.$rc['incident_name'] ?>
                            
						<?php
                        
                        if ($rc['initiation_type'] == 0){
                            
                            echo '<span class="label label-default">Mock</span>';
                        
                        }
                        
                        else{
                        
                            echo '<span class="label label-info">Live</span>';
                            
                        }
                        
                        ?>
                                
                                
                        

                    </h4>
    
                </div>
    
                <div class="panel-widgets hidden">
                    <a data-toggle="collapse" data-parent="#accordion" href="#open_incident" style="text-decoration: none;">
                    <i class="fa fa-chevron-up tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php // echo 'View '.$more_inci.' more incidents.';?>"></i></a>

                </div>
                <div class="clearfix"></div>
        
                </div>
        
                
        
                <div class="panel-body">
    
                    <div id="open_incident" class="panel-collapse collapse">
    
                        <ul class="list-group list-default">
                        	<?php if(count($open_recall) > 0){
								$count_open = 0;
								$uri_5 = $this->uri->segment(5);
								foreach($open_recall as $open=>$opeen){
									
									if ($uri_5 != $count_open) {
							?>
							
                            <a href="<?php echo base_url().'cc/dashboard/index/r/'.$count_open ?>" class="list-group-item"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
                            
							<?php
									}
								$count_open++;	
								} 
							} ?>
                        </ul>
                    </div>
            
    
    
              		<div class="row">  
                        <div class="col-md-3 col-lg-3 steps_holder">
                     
                     
                            <ul class="nav nav-pills nav-stacked navigation" id="myTab" style="margin-bottom: 10px;">
                                
                                <?php
								
									//get blockers 
									$bb = array(
										'cc_id'=>$this->session->userdata('team_cc'),
										$recall_field_name=>$rc['id']
									);
									$bb_unresolved = array(
										'cc_id'=>$this->session->userdata('team_cc'),
										$recall_field_name=>$rc['id'],
										'status'=>'0'
									);
									$blockers = $this->master_model->getRecords('cf_recall_blockers',$bb,'*',array('id'=>'DESC'));
									$unresolved_blockers = $this->master_model->getRecords('cf_recall_blockers',$bb_unresolved,'*',array('id'=>'DESC'));
									if(count($unresolved_blockers) > 0){
										$blocker_stat = 'text-red bg-danger';
										$blocker_count = '<span class="label label-danger" style="font-size: 65%; border-radius:50%; padding-left: 7px;padding-right: 7px;">'.count($unresolved_blockers).'</span>';
									}else{
										$blocker_stat = 'text-green bg-success';
										$blocker_count = '<i class="fa fa-check-circle fa-fw"></i>';
									}
								?> 
                                
                                <?php //KPIs module
								if (strpos($the_active_module[0]['active_module'], '0') !== false && in_array('kpi', $user_access)){ ?>
                                <li class="kpi_menu_tab" id="recall_cost_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#recall_kpi<?php echo $rc['id'] ?>" class="text-info bg-info">
                                    <i class="fa fa-calculator fa-fw"></i> KPI Dashboard <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                <?php } ?>

                                
                                
                                  
                                <?php //cost monitor module
								if (strpos($the_active_module[0]['active_module'], '6') !== false && in_array('cost', $user_access)){ ?>
                                <li class="costmon_menu_tab" id="recall_cost_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#recall_cost<?php echo $rc['id'] ?>" class="text-info bg-info">
                                    <i class="fa fa-usd fa-fw"></i> Cost Monitor <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                <?php } ?>

                                <li class="blockers_menu_tab <?php if(!in_array('issues', $user_access)) { echo 'hidden'; }?>" id="blockers_li<?php echo $rc['id']?>">
                                    <a data-toggle="tab" href="#blockers-<?php echo $rc['id'] ?>" class="<?php echo $blocker_stat ?>">
                                    <?php echo $blocker_count ?> Issues Board <i class="fa fa-angle-double-right"></i></a>
                                </li>
                                      

<?php

if (count($myrecallpack) > 0){ //check if recall input pack
    $recallsteps = $this->master_model->getRecords('cf_recall_packs_steps',
		array('recall_pack_id'=>$myrecallpack[0]['id'], 'date <'=>$rc['initiation_date']),'*',
		array('order'=>'ASC'));
		
    $rpack_step_no = 2;
    foreach($recallsteps as $rst=>$rstep){ //loop step category
		//set step no via steps
		$rpack_step_no = $rstep['step_no'];
		$rpack_sort_no = $rstep['order'];
		
		//disect each step
		${'questions' . $rpack_step_no} = $this->master_model->getRecords($db_name,
			array('recall_id'=>$rc['id'],'step_no'=>$rpack_step_no),'*',
			array('category_id'=>'ASC'));
		
		
		//total question
		${'totq2' . $rpack_step_no} = count(${'questions' . $rpack_step_no});
		
		
		
		//check if each step is completed starts in step 3
		${'step' . $rpack_step_no . '_label'} = 0;
		
		foreach(${'questions' . $rpack_step_no} as $qu=>$qus){
			if($qus['status'] == '0'){
				${'step' . $rpack_step_no . '_label'}++;
			}
		}
		
		//steps label variables
		 if (${'step' . $rpack_step_no . '_label'} == '0'){
			 ${'step' . $rpack_step_no . 'color'} = 'text-green bg-success';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-check-circle fa-fw"></i>';
		} 
		 else if (count(${'questions' . $rpack_step_no}) == ${'step' . $rpack_step_no . '_label'} ){
			 ${'step' . $rpack_step_no . 'color'} = 'text-red bg-danger';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-circle-o fa-fw"></i>';
		} 
		 else{
			 ${'step' . $rpack_step_no . 'color'} = 'text-orange bg-warning';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-minus-circle fa-fw"></i>';
		 }
		
    
        
?>
                    
                                <li class="<?php if(($rpack_step_no == 2 && strpos($the_active_module[0]['active_module'], '0') === false) || ($rpack_step_no == 2 && !in_array('kpi', $user_access)) ) { echo 'active'; }?>" id="step_<?php echo $rpack_step_no; ?>btn">
                                    <a data-toggle="tab" href="#step<?php echo $rpack_step_no.'-'.$rc['id'] ?>" class="<?php echo ${'step' . $rpack_step_no . 'color'}; ?>">
                                    <?php echo ${'step' . $rpack_step_no . 'icon'}; ?> <?php echo ucfirst($rstep['name']); ?> <i class="fa fa-angle-double-right"></i></a>
                                </li>



 <?php   
        $rpack_step_no++;
    }
?>
                
                
    
                            </ul>
                                

                        </div>
                        <div class="col-md-9 col-lg-9">
                            
                          <p class="lead text-muted text-center tasktab_loader" style="margin-top: 65px;">
                          	<i class="fa fa-spinner fa-spin"></i><br />
                            Loading..

                          </p>  
                        
                        
                        <div class="user-body">
                            <div class="tab-content">
                            
                                <div class="tab-pane" id="blockers-<?php echo $rc['id'] ?>">
                                
    <h3>Issues Board

        
        <!-- Single button -->
        <div class="btn-group pull-right" style="margin-right: 15px;">
          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 60%; min-width: 82px; right: 0; left: auto;">
          
            <li class="active all_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','','.all_blocker_btn'); ">All</a></li>
          
            <li class="open_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','open','.open_blocker_btn');">Open</a></li>
          
            <li class="closed_blocker_btn"><a href="javascript:;" onclick="filterTasks('-blockers','resolved','.closed_blocker_btn'); ">Closed</a></li>
  
            <li class="my_issues"><a href="javascript:;" onclick="filterTasks('-blockers','<?php echo $this->session->userdata('logged_display_name')?>','.my_issues');">My Issues</a></li>
            
            
          </ul>
        </div> 
         
    	<button class="btn btn-primary btn-sm pull-right add_issue_btn" data-id="" style="margin-right: 5px;">Add Issue</button>
    	<a href="#" class="btn btn-primary btn-sm pull-right hidden" data-toggle="modal" data-target="#addBlocker" onclick="recallAssign('<?php echo $rc['id'] ?>');" style="margin-right: 5px;">Raise a Blocker</a>
    </h3>
    
      <table class="table forum table-striped" id="recall-step-filter-table-blockers">
        <thead>
          <tr>
            <th class="cell-stat text-center hidden-xs"></th>
            <th>
                <div class="col-sm-12 hidden">
                    <input type="text" class="form-control" id="recall-step-filter-blockers" data-action="filter" data-filters="#recall-step-filter-table-blockers" placeholder="Filter Tasks"/>
            
                </div>
            
            </th>
            <th class="cell-stat-1-point-5x text-right hidden-xs"></th>
          </tr>
        </thead>
        <tbody>
        
        	<?php
			
			if(count($blockers) > 0){
				foreach( $blockers as $b=>$blck){
			?>
 			<tr>
            	<td class="text-center hidden-xs">
                    <button class="btn btn-default disabled" id="comp_blck<?php echo $blck['id']?>" style="margin-top: 10px; padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if($blck['status'] == '0') { echo 'display: none'; }?>" onclick="checkTheBlocker('0','<?php echo $blck['id']?>','<?php echo count($blockers)?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                    
                    
                    <button class="btn btn-default disabled" id="inc_blck<?php echo $blck['id']?>" style="padding:3px; margin-top: 10px; <?php if($blck['status'] == '1') { echo 'display: none'; }?>" onclick="checkTheBlocker('1','<?php echo $blck['id']?>','<?php echo count($blockers)?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>


                </td>
            	<td>
                  <h4><?php echo $blck['blocker']?><br>
                  
                  	<small>
                    
                        <span class="blocker_note_empty" style=" <?php if ($blck['note'] != ''){ echo 'display: none'; } ?>">No notes added</span>
                        
                        <a href="#" class="blocker_note_btn" data-toggle="modal" data-target="#blockerNote" onclick="loadBlockerNote('<?php echo $blck['id']?>');" style=" <?php if ($blck['note'] == ''){ echo 'display: none'; } ?>">View notes</a><br />
    
                        Issue: <?php echo $blck['issue']?><br />
                        Impact: <?php echo $blck['impact']?><br />
                        Raised by: <span class="label label-warning" style="border-radius: 15px;"><?php echo $my_class->getcrtname($blck['crt_id']);?></span><br />
                        Dependent Task(s): <?php echo $this->common_model->taskname($blck['task_id'], $db_name);?><br />
                        Assigned to: <span class="label label-warning" style="border-radius: 15px;"><?php if($blck['assigned'] != '0') { echo $my_class->getcrtname($blck['assigned']); } else { echo 'Not assigned'; }?></span>
                   
                    </small>
                  </h4>
                  <p>
                	<span class="small">Date Raised: <span class="text-muted"><?php echo date_format(date_create($blck['date_created']), 'm.d.Y, g:ia'); ?></span></span><br />
                	<span class="small date_resolved_holder">
						<?php if ($blck['status'] == '1'){?>
                            Date Resolved: <span class="text-muted"><?php echo date_format(date_create($blck['date_resolved']), 'm.d.Y, g:ia'); ?></span>
						<?php }else { echo '<span style="visibility: hidden">open</span>';} ?>
                    </span>
                  </p>
                
                  <p class="hidden-lg hidden-md hidden-sm">
                        <button class="btn btn-default disabled" id="comp_blck<?php echo $blck['id']?>" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de; <?php if($blck['status'] == '0') { echo 'display: none'; }?>" onclick="checkTheBlocker('0','<?php echo $blck['id']?>','<?php echo count($blockers)?>');"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                        
                        
                        <button class="btn btn-default disabled" id="inc_blck<?php echo $blck['id']?>" style="padding:3px; <?php if($blck['status'] == '1') { echo 'display: none'; }?>" onclick="checkTheBlocker('1','<?php echo $blck['id']?>','<?php echo count($blockers)?>');"><i class="fa fa-check text-muted" style="font-size: 165%;"></i></button>
                  
                        <a href="#" class="btn btn-sm btn-default" id="add_note_blck<?php echo $blck['id']?>" style="background:#e0e0e0; <?php if ($blck['status'] == '1'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#resolveBlocker" onclick="openResolveBlocker('<?php echo $rc['id']?>','<?php echo $blck['id']?>');loadBlockerNote('<?php echo $blck['id']?>');"><i class="fa fa-gavel tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolve"></i></a>
                  
                  </p>
                
                </td>
            	<td class="text-right hidden-xs">
                    <a href="#" class="btn btn-sm btn-default" id="add_note_blck<?php echo $blck['id']?>" style="background:#e0e0e0; <?php if ($blck['status'] == '1'){ echo 'display: none'; }?>" data-toggle="modal" data-target="#resolveBlocker" onclick="openResolveBlocker('<?php echo $rc['id']?>','<?php echo $blck['id']?>');loadBlockerNote('<?php echo $blck['id']?>');"><i class="fa fa-gavel tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolve"></i></a>
                
                </td>

            </tr>     
        
        
        <?php
				}
			} else { ?>
        
        
        	<tr>
            	<td colspan="3" class="text-muted">No blockers</td>
            </tr>
        	
        <?php } ?>
		</tbody>
      </table>    
    
    
    
                                </div>
                                <!-- end of blockers tab-->
                                
                                
                                
                                
  
								<?php //Recall feature KPIs module //tab-pane fade in 
                                    if (strpos($the_active_module[0]['active_module'], '0') !== false && in_array('kpi', $user_access)){ ?>
                                
                                    
                                <div class="tab-pane steptab_active" id="recall_kpi<?php echo $rc['id'] ?>">
                   
                   
        <?php
			$cc_id = $this->session->userdata('logged_parent_crt');
			$org_id = $this->session->userdata('crt_selected_orgnaization');
			
			$active_mod  = substr($this->session->userdata('org_module'),0,1);
			//active module is recall
			$incident_type = '8';
			if($active_mod == '5'){
				$incident_type = '5';
			}

			
			$whr_inci = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'closed'=>'0'
			);
			
			if($incident_type == '5'){
				$inci_type = 'recall';
				$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);
			}
			else{ //continuity = 8
				$inci_type = 'continuity';
				$incidents = $this->master_model->getRecords('cf_continuity', $whr_inci);
			}
			
			
			$whr_noti = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'type'=>$inci_type,
				'incident_id'=>$rc['id']
			);
			
			$notices_sent = $this->master_model->getRecords('bulk_notification', $whr_noti);
			
			$email_sent_count = 0;
			$email_confirmed_count = 0;
			$sms_sent_count = 0;
			if(count($notices_sent) > 0){                
				foreach($notices_sent as $r=>$ns){
					//email sent count
					$whr_email_sent = array(
						'bulk_id'=>$ns['id'],
						'email_sent'=>'1'
					);
					$bulk_data_email_sent = $this->master_model->getRecordCount('bulk_data', $whr_email_sent);

					//email confirmed count
					$whr_email_confirmed = array(
						'bulk_id'=>$ns['id'],
						'email_confirmed'=>'1'
					);
					$bulk_data_email_confirmed = $this->master_model->getRecordCount('bulk_data', $whr_email_confirmed);


					//sms sent count
					$whr_sms_sent = array(
						'bulk_id'=>$ns['id'],
						'sms_sent'=>'1'
					);
					$bulk_data_sms_sent = $this->master_model->getRecordCount('bulk_data', $whr_sms_sent);
					
					$email_sent_count += $bulk_data_email_sent;
					$email_confirmed_count += $bulk_data_email_confirmed;
					$sms_sent_count += $bulk_data_sms_sent;
				
				}
				
			}

	
			//get notification kpi
			$noti_kpi = 0;
			if($email_sent_count != 0){
				$noti_kpi = ($email_confirmed_count/$email_sent_count) * 100;
			}
			
			
			//$incidents = $this->master_model->getRecords('cf_recall', $whr_inci);

			$whr_kpis = array(
				'cc_id'=>$cc_id,
				'org_id'=>$org_id,
				'incident_id'=>$rc['id'],
				'incident_type'=>$incident_type
			);
			
			$kpis = $this->master_model->getRecords('bulk_kpi', $whr_kpis, '*', array('date_added'=>'DESC'));

	
			//set kpi variables
			$all_kpi = array();
			
			$all_kpi[] = array(
				'type'=>'comp_kpi',
				'dbcell'=>'comp_data',
				'bgcolor'=>'orange',
				'kpistatus'=>'comp_stat',
				'name'=>'Units under Company control'
			);
		
			$all_kpi[] = array(
				'type'=>'dist_kpi',
				'dbcell'=>'dist_data',
				'bgcolor'=>'blue',
				'kpistatus'=>'dist_stat',
				'name'=>'Units in distribution'
			);
		
			$all_kpi[] = array(
				'type'=>'market_kpi',
				'dbcell'=>'market_data',
				'bgcolor'=>'green',
				'kpistatus'=>'market_stat',
				'name'=>'Units in consumer market'
			);
	


		?>
        
                   
                   
                                
                                
    <h3>KPI Dashboard
    
        <small class="hidden">
			<?php if(count($kpis) > 0){ 
			
			if($kpis[0]['date_updated'] == '0000-00-00 00:00:00'){ ?>
            <span class="text-muted" title="<?php echo $kpis[0]['date_added'];?>"><?php echo 'added '. $this->common_model->ago($kpis[0]['date_added']); ?></span></td>
            
            <?php } else{ ?>
            <span class="text-muted" title="<?php echo $kpis[0]['date_updated'];?>"><?php echo 'updated '. $this->common_model->ago($kpis[0]['date_updated']); ?></span></td>
            
            <?php } }?>
        </small>
    
    </h3>
    <div class="col-xs-12" style="padding-top: 20px; padding-left: 0px;">
       <div class="row">
    
    
    		<?php
			$count_all_kpi = 1;
			foreach($all_kpi as $r=>$value){
				$kpi_cell = $value['dbcell'];
				if($value['type'] != 'comp_kpi') {
					$akpiclass = 'text-primary';
					$akpistyle = 'cursor: pointer;';
					
					if($value['type'] == 'dist_kpi'){
						$akpiclass .=" dist_toggle_btn";
					}
				}
				else{
					$akpiclass= '';
					$akpistyle= '';
				}

				//set value
				$total_unit = '';
				$unit_return = '';
				$u_disposed = '';
				$u_transformed = '';
				$u_corrections = 'NA';
				$the_kpi = '0';
				$u_date_update = '';
				$u_threshold = '0';
				
				$kpi_status = '';
				
				//check count of kpi
				if(count($kpis) > 0) {
					if($kpis[0][$value['kpistatus']] != '0'){//enabled
						$kpi_status = 'hidden';
					}

				
				
					$kpi_id = $kpis[0]['id'];
					$dbcell = unserialize($kpis[0][$value['dbcell']]);
					if($dbcell != ''){ //get each kpi db cell data
						$total_unit = number_format($dbcell['total_unit']);
						$unit_return = number_format($dbcell['unit_return']);
						$u_disposed = number_format($dbcell['u_disposed']);
						$u_transformed = number_format($dbcell['u_transformed']);
						$u_corrections = number_format($dbcell['u_corrections']);
						$the_kpi = round($dbcell['the_kpi'], 0).'%';
						$u_date_update = $dbcell['date_updated'];
						$u_threshold = round($dbcell['threshold'], 0);
					}
				}
				
				
				if($the_kpi >= $u_threshold){
					$kpi_thresh_status = '';
				}
				else{
					$kpi_thresh_status = '<i class="fa fa-circle text-red pull-right" data-toggle="tooltip" title="Status: Not OK" style="font-size: 160%;"></i>';
				}
				
				
			?>
            
            <div class="col-sm-6 <?php echo $kpi_status; ?>">
                <div class="tile <?php echo $value['bgcolor']; ?> dash-demo-tile">
                    <h4><?php echo $value['name'].' '.$kpi_thresh_status; ?></h4>
                    <div class="row">
                    	<div class="col-lg-5 text-center">
                            <div id="easy-pie-1" class="easy-pie-chart" data-percent="<?php echo $the_kpi;?>">
                                <span class="percent"></span>
                            </div>
                        </div>
                    
                    	<div class="col-lg-7">
                        	<ul class="list-unstyled easy-pie-data" style="font-size: 90%;">
                            	<?php
								$nokpidata = '<li>No data</li>';
								if(count($kpis) > 0) {
									if($dbcell != '') {
										
										?>
                                
                                
                            	<li>Total Units: <?php echo $total_unit; ?></li>
                            	<li>Units Disposed: <?php echo $u_disposed ?></li>
                            	<li>Units Transformed: <?php echo $u_transformed; ?> </li>
                            	<li>Total Corrections: <?php echo $u_corrections; ?></li>
                            	<li style="border-top: 1px solid; border-color: rgba(255,255,255, .2); color: rgba(255,255,255, .5);"><small> 
                                	<?php echo date_format(date_create($u_date_update), 'M d, Y, g:i A'); ?>
                                 </small></li>
                                
                                <?php
									}
									else {
										echo $nokpidata;
									}
								}
								else{
									echo $nokpidata;
								}
								
								?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
			
			if(($count_all_kpi%2) == 0){
				echo '<div class="clearfix"></div>';
			}
			
			$count_all_kpi++;
			}//end loop kpis ?>




            <div class="col-sm-6 <?php if(count($kpis) > 0) { if($kpis[0]['noti_stat'] == 1) { echo 'hidden'; } }?>">
                <div class="tile purple dash-demo-tile">
                    <h4>Outbound notifications</h4>
                    <div class="row">
                    	<div class="col-lg-5 text-center">
                            <div id="easy-pie-4" class="easy-pie-chart" data-percent="<?php echo $noti_kpi; ?>">
                                <span class="percent"></span>
                            </div>
                        </div>
                    
                    	<div class="col-lg-7">
                        	<ul class="list-unstyled easy-pie-data" style="font-size: 90%;">
                            	<li>Notices Sent: <?php echo $email_sent_count; ?></li>
                            	<li>Notices Received: <?php echo $email_confirmed_count; ?></li>

                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    
       
       </div><!--end of row--> 		
       

        <div class="table-responsive <?php if(count($kpis) > 0) { if($kpis[0]['contact_stat'] == '1') { echo 'hidden'; } }?>">
        
        
            <div class="form-group hidden">
                <input class="form-control" type="text" name="contact_cntr" id="contact_cntr" value="<?php if(count($kpis) > 0) { echo $kpis[0]['contact_cntr']; } ?>"/>
                <input class="form-control" type="text" name="kpi_email" id="kpi_email" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_email']; } ?>"/>
                <input class="form-control" type="text" name="kpi_website" id="kpi_website" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_website']; } ?>"/>
                <input class="form-control" type="text" name="kpi_trading" id="kpi_trading" value="<?php if(count($kpis) > 0) { echo $kpis[0]['kpi_trading']; } ?>"/>
            </div>
        

        <table class="table customcontactstable" id="example-tablexx">

            <thead>

                <tr>
                    
                    <th>Customer Contacts</th>
                    
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Contact Centre</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Email</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Website</th>
                    <th width="18%" class="text-center bg-primary" style=" vertical-align: middle; border: 1px solid #ddd;">Trading Partner</th>



                </tr>

            </thead>

            <tbody>
            
				<?php
                    if(count($kpis) > 0) {
                        if($kpis[0]['contact_cntr'] !=''){
                            $contact_cntr = number_format($kpis[0]['contact_cntr']);
                            $kpi_email = number_format($kpis[0]['kpi_email']);
                            $kpi_website = number_format($kpis[0]['kpi_website']);
                            $kpi_trading = number_format($kpis[0]['kpi_trading']);
                        
                        }else{
                            $contact_cntr = 'No data';
                            $kpi_email = 'No data';
                            $kpi_website = 'No data';
                            $kpi_trading = 'No data';
                            
                        }
                    }
                    else{
                        $contact_cntr = 'No data';
                        $kpi_email = 'No data';
                        $kpi_website = 'No data';
                        $kpi_trading = 'No data';
                        
                    }
                ?>
            
                <tr class="tr_main">
                    <td><i>Inbound customer contacts</i><br />
                        <button class="btn btn-primary submit_kpis btn-xs hidden disabled" type="submit">Save</button>
                        <a class="btn btn-default btn-xs hidden cancel_kpi_btn">Cancel</a>
                    
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                         <a href="javascript: void(0);"><?php echo $contact_cntr; ?></a>
                    

                        <input type="text" class="form-control hidden" name="contact_cntr" value="<?php echo $contact_cntr; ?>"/>
                        
                        <input type="hidden" class="form-control hidden" name="id" value="<?php if(count($kpis) > 0) { echo $kpis[0]['id']; }?>"/>
                        
                        <input type="hidden" class="form-control hidden" name="incident_id" value="<?php echo $rc['id']; ?>"/>
                        <input type="hidden" class="form-control hidden" name="incident_type" value="<?php echo $active_module; ?>"/>
                        
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                        
                         <a href="javascript: void(0);"><?php echo $kpi_email; ?></a>

                        <input type="text" class="form-control hidden" name="kpi_email" value="<?php echo $kpi_email; ?>"/>
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                       
                        
                         <a href="javascript: void(0);"><?php echo $kpi_website?></a>

                        <input type="text" class="form-control hidden" name="kpi_website" value="<?php echo $kpi_website; ?>"/>
                        
                    </td>
                    <td class="text-center" style=" border: 1px solid #ddd;">
                      
                        <a href="javascript: void(0);"><?php echo $kpi_trading; ?></a>

                        <input type="text" class="form-control hidden" name="kpi_trading" value="<?php echo $kpi_trading;?>"/>
                       
            
                    </td>


                </tr><!--consumer market-->
                
                <?php if(count($kpis) > 0) {
                    if($kpis[0]['kpi_trading'] !=''){
                    ?>


                <tr>
                    <td>&nbsp;</td>
                    <td colspan="4" style="border: 1px solid #ddd;">
                    
                    
                                            
    
                        <!-- Pie Chart Example -->
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-chart-pie"></div>
                        </div>
                        
                        <p class="hidden">&nbsp;<a href="#" data-toggle="modal" data-target="#chartDataModal" style="display: none;"><i class="fa fa-edit"></i> Edit</a></p>
                    
                    
                    </td>
                </tr>

                <?php } } ?>

           
                                

            </tbody>


        </table>

    </div>

    <!-- /.table-responsive -->








    
    </div><!--/col-xs-12-->

    
    
    
                                </div>
                                <!-- end of KPIs module tab-->
                                <?php } ?>
                                                                
                                
                                
                                
                                
								<?php //Recall feature cost monitor module activated
                                    if (strpos($the_active_module[0]['active_module'], '6') !== false){ ?>
                                    
                                <div class="tab-pane" id="recall_cost<?php echo $rc['id'] ?>">
                                
                                
    <h3>Cost Monitor
    
    

                        <?php if(count($recall)> 0){ ?>
                        <a class="btn btn-primary btn-sm pull-right hidden-xs" href="#" data-toggle="modal" data-target="#viewAccumulative" style="width: 150px;">Running Cost</a> 
                        
                        <a class="btn btn-primary btn-sm pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#viewAccumulative">Running Cost</a> 
                        <?php } ?>
    
    </h3>
    
             
<?php 
						
	if (count($recall) > 0){
	?>
    
							<div class="col-xs-12" style="padding-top: 20px; padding-left: 0px;">
    
								<?php 

								if(count($cost_category)>0)

								{ ?>

							<?php /*?><div class="" style="padding-top: 20px;"><?php */?>

							<table class="table" id="example-tablexx">

								<tbody>

								<?php
								
									foreach($cost_category as $r => $value)

									{
										
								?>

                                                        

                                    <tr class="bg-info">

                                    	<td width="100%">
                                        <h4 style="text-transform: uppercase; font-weight: bold"><?php echo  $value['name']; ?></h4>
             
                                        
                                        </td>

                                        <td class="pull-right">
                                    
       <!-- Single button -->
        <div class="btn-group" style="margin-top: 5px;">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; left: auto; right: 0;">
          
            <li>
                <a href="#" data-toggle="modal" data-target="#viewCategorySummary" onclick="showCostSummary('<?php echo $value['cost_id'] ?>')">View Category Sub-Total</a>
            </li>
          </ul>
        </div>    

										
                                    </td>

                                    </tr>

									<tr>
                                    	<td colspan="2">
                                        
                                        
                           
								<?php
                                
                                $items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$value['cost_id']));
                                ?>
                                        
            

								<?php 

								if(count($items)>0)

								{ ?>

							<?php /*?><div class="table-responsive"><?php */?>

							<table class="table table-hover table-bordered table-datatable">

                                <thead>

                                    <tr>

                                        <th width="10%">Date</th>
                                        
                                        <th width="60%">Item</th>

                                        <th width="10%">CUR</th>

                                        <th width="14%">Cost</th>

                                        <?php /*?><th width="6%"></th><?php */?>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($items as $r => $value)

									{
										
								?>

                                                        

                                    <tr>

                                    	<td width="10%"><?php echo $value['item_date_invoice']?></td>
                                    	<td width="60%">
										
										<?php
										
										$files = $this->master_model->getRecords('private_message_file',array('cost_item_id'=>$value['item_id']));
										if(count($files) > 0){
											echo '<i class="fa fa-paperclip text-info fa-fw"></i> ';
										}else{
											echo '<i class="fa fa-hand-o-right text-info fa-fw" style="visibility:hidden"></i> ';
										}
										echo  $value['item_name']; ?>
                                        
                                        </td>

                                    	<td width="10%"><?php echo $my_class->currencycode($value['currency']) ?></td>
                                    	<td width="14%" class="text-right"><?php echo number_format($value['item_cost'],2); ?></td>

                                        <?php /*?><td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; right: 0; left: auto;">
            <li>
                <a href="javascript:;" onclick="updateItemCost('<?php echo $value['item_id']?>','<?php echo $value['item_name']?>','<?php echo $value['item_cost']?>','<?php echo $value['currency']?>','<?php echo $value['item_date_invoice']?>')">Edit Cost Item</a> 

            </li>
            <li>
                <a href="javascript:void(0);" onclick="return delete_cost_item('<?php echo $value['item_id'] ?>');">Delete Cost Item</a>
            </li>
            <li class="hidden">
                <a href="#" data-toggle="modal" data-target="#viewCostItem" onclick="viewCostItemDetails('<?php echo $value['item_id']?>')">View Cost Detail</a>
            </li>
            
		<?php if(count($files) > 0){ ?>
        
            <li>
                <a href="<?php echo base_url().'cc/costmonitor/download/'.$files[0]['file_upload_name']?>" target="_blank"> Download Document</a>
            </li>
            
		<?php } ?>
          </ul>
        </div>    


                                    </td><?php */?>

                                    </tr>



								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        <?php /*?></div><?php */?>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-list-ul" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost items for <?php echo $value['name']?></p>
								<?php }

								?>

                                                                                
                                        
                                        
                                        
                                        
                                        </td>
                                    </tr>

								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        <?php /*?></div><?php */?>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>

                                            <p class="text-center hidden" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-usd" style="font-size: 90px"></i></p>
                                            <p class="text-center" style="color: #ccc; margin-top: 60px;">No cost category</p>

								<?php }

								?>







    
	<?php
    } else{
	?>
        	
        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-history" style="font-size: 90px"></i></p>
        <p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall</p>
	<?php	
	}
?>
    
    
               </div>
    
    
    
                                </div>
                                <!-- end of recall_cost tab-->
                                
								<?php } ?>
                                
                                
                                
                                
    <?php
	//variables, start of continuity questions/tasks
	
    $rpack_step_no = 2;
	$rpack_sort_no = 0;
    foreach($recallsteps as $rst=>$rstep){ //loop step category
		//set step no via steps
		$rpack_step_no = $rstep['step_no'];
		$rpack_sort_no = $rstep['order'];
		
		//disect each step
		${'questions' . $rpack_step_no} = $this->master_model->getRecords($db_name,
			array('recall_id'=>$rc['id'],'step_no'=>$rpack_step_no),'*',
			array('category_id'=>'ASC', 'id'=>'DESC'));
		
		
		//total question
		${'totq' . $rpack_step_no} = count(${'questions' . $rpack_step_no});
		
		
		
		//check if each step is completed starts in step 3
		${'step' . $rpack_step_no . '_label'} = 0;
		
		foreach(${'questions' . $rpack_step_no} as $qu=>$qus){
			if($qus['status'] == '0'){
				${'step' . $rpack_step_no . '_label'}++;
			}
		}
		
		//steps label variables
		 if (${'step' . $rpack_step_no . '_label'} == '0'){
			 ${'step' . $rpack_step_no . 'color'} = 'text-green bg-success';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-check-circle fa-fw"></i>';
		} 
		 else if (count(${'questions' . $rpack_step_no}) == ${'step' . $rpack_step_no . '_label'} ){
			 ${'step' . $rpack_step_no . 'color'} = 'text-red bg-danger';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-circle-o fa-fw"></i>';
		} 
		 else{
			 ${'step' . $rpack_step_no . 'color'} = 'text-orange bg-warning';
			 ${'step' . $rpack_step_no . 'icon'} = '<i class="fa fa-minus-circle fa-fw"></i>';
		 }
		

?>                                
                                
                                
                                <div class="tab-pane <?php if(($rpack_step_no == 2 && strpos($the_active_module[0]['active_module'], '0') === false) || ($rpack_step_no == 2 && !in_array('kpi', $user_access)) ) { echo 'steptab_active'; }?>" id="step<?php echo $rpack_step_no; ?>-<?php echo $rc['id'] ?>">
                        
                        


    <h3><?php echo $rstep['name']; ?>
    
    
    <span id="proc<?php echo $rpack_step_no; ?>" <?php if (${'step' . $rpack_step_no . '_label'} != 0){ echo 'style="display: none;"'; }?>>
        <i class="fa fa-check-circle text-green tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Completed"></i> 
       <span class="<?php if (($rpack_step_no-1) == count($recallsteps)) { echo 'hidden'; } ?>"> <a data-toggle="tab" href="#step<?php echo $rpack_step_no + 1; ?>-<?php echo $rc['id'] ?>" class="small" style="text-decoration: none;" onclick="$('#step_<?php echo $rpack_step_no + 1; ?>btn').click();$('#step_<?php echo $rpack_step_no + 1; ?>btn').addClass('active');$('#step_<?php echo $rpack_step_no + 1; ?>btn').siblings('li').removeClass('active');" >Proceed to Step <?php echo $rpack_step_no; ?> <i class="fa fa-arrow-right"></i></a></span>
        </span>
        
        
        <div class="btn-group pull-right" data-toggle="buttons" style="margin-right: 15px;">
          <label class="btn btn-primary btn-sm active all_task_btn<?php echo $rpack_step_no; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','','.all_task_btn<?php echo $rpack_step_no; ?>');">
            <input type="radio" name="options" id="option1"> All Tasks
          </label>
          <label class="btn btn-default btn-sm my_task_btn<?php echo $rpack_step_no; ?>" onclick="filterTasks('<?php echo $rpack_step_no; ?>','<?php echo $this->session->userdata('logged_display_name')?>','.my_task_btn<?php echo $rpack_step_no; ?>'); ">
            <input type="radio" name="options" id="option3"> My Tasks
          </label>
        </div>    
         
        <?php 
			if(($recall[0]['initiation_type'] == '0' && strpos($the_active_module[0]['active_module'], 'a') !== false) && ($rpack_step_no > 2)){
		?> 
        <button class="btn btn-primary btn-sm pull-right simulation_btn <?php if(${'step' . $rpack_step_no . '_label'} > 0) { echo 'hidden'; }?>" data-sid="<?php echo $theorg[0]['simulation_id'] ?>" data-id="<?php echo $recall[0]['id'] ?>" data-step="<?php echo $rpack_step_no; ?>" style="margin-right: 5px;">View Step Simulation</button>
        <?php
			}
		?>

        
    </h3>
                        
                             
                             
      <table class="table forum table-striped table-list-search" id="recall-step-table<?php echo $rpack_step_no; ?>">
        <thead>
          <tr>
            <th>
                <div class="col-sm-12 hidden">
                    <input type="text" class="form-control" id="recall-step-filter<?php echo $rpack_step_no; ?>" data-action="filter" data-filters="#recall-step-table<?php echo $rpack_step_no; ?>" placeholder="Filter Tasks" />
                </div>
			</th>
            <th class="cell-stat-1-point-5x text-right hidden-xs hidden-sm hidden-md"></th>
            
            <th class="cell-stat-1-n-half text-center hidden-xs"></th>
            
            
          </tr>
        </thead>
        <tbody>
                                 
            <tr <?php if ($rpack_step_no != '2') { echo 'class="hidden"'; }?>>
                <td>
                  <h4 class="text-normal">
                    Date of incident<br><small><?php echo $recall[0]['incident_date']; ?></small></h4>
                </td>
                <td class="text-right"></td>
                <td class="text-center">
                    <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                </td>
                
          </tr>
          <tr <?php if ($rpack_step_no != '2') { echo 'class="hidden"'; }?>>
            <td>
              <h4 class="text-normal">
                Affected sites/locations<br><small>
				<?php 
				
				 $locs = json_decode($recall[0]['affected_location']);
					$loc_c = 1;
				if(count($locs) > 0){
					 foreach($locs[0] as $loc){
						 
						 if($loc !=''){
							 echo $loc_c.'. '.$loc.'<br />';
							 $loc_c++;
						 }
					 }
				}else{
					echo 'No affected location.';
				}

				 ?>
                
                </small></h4>
            </td>
            <td class="text-right"></td>
            <td class="text-center">
                <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
            </td>
          </tr>
          
                        
<?php

	$countqq = 0;
	$curr_sub_c = '';
	foreach(${'questions' . $rpack_step_no} as $q=>$kyu){
?>	

<?php
	$the_approvers = unserialize($kyu['approvers']);
	$the_approvers_status = unserialize($kyu['approvers_status']);
	$approvers_count = 0;
	$approvers_status = 0;
	
	$theapprovers = array();
	
	$crt_assigned = $kyu['assigned'];
	$crt_date_assigned = ' ('.date_format(date_create($kyu['date_assigned']), 'jS F Y, g:ia').')';
	$crt_approve_btn = '';
	
	$crt_assigned_match = 'no';
	
	if($crt_assigned == $this->session->userdata('logged_crt_login_id')){
		$crt_assigned_match = 'yes';
	}
	
	$crt_view_approvers_btn = '<li class="crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'" style="cursor: pointer;"><a>Assign Approver(s)</a></li>';
	
	$add_issue_btn = '<li class="add_issue_btn" id="raise_btn'.$kyu['id'].'" data-id="'.$kyu['id'].'" style="cursor: pointer;"><a>Raise an Issue</a></li>';
	
	if(is_array($the_approvers)){
		$approvers_count = count($the_approvers);
	}
	else{
		$the_approvers = array();
	}
	
	if(is_array($the_approvers_status)){
		$approvers_status = count($the_approvers_status);
	}
	else{
		$the_approvers_status = array();
	}
	
	
	
	//get approvers list

	$approvers_name = array();
	$unapproved_count = count($the_approvers);
	
	if(count($the_approvers) > 0){
		foreach($the_approvers as $tapp){
			$user_approved = '';
			if(in_array($tapp, $the_approvers_status)){
				$user_approved = ' - Approved';
				$unapproved_count--;
			}
			
			$approvers_name[] = array(
				'name'=>$this->common_model->getname($tapp),
				'status'=>$user_approved
			); 

		}
	}
	
	$theapprovers = $approvers_name;
	$curr_user_is_approver = '';
		
	if($unapproved_count > 0){ //$approvers_count != $approvers_status){
		$status_text = 'Awaiting Approval';
		$status_class = 'text-warning';
	

		//array_diff — Computes the difference of arrays
		$result = array_diff($the_approvers, $the_approvers_status);
		
		if(count($result) > 0){
			$new_arr = array();
			foreach($result as $rs){
				$new_arr[] = $rs;
			}

			$crt_assigned = $new_arr[0];
			$crt_date_assigned = '';
			
			if($crt_assigned == $this->session->userdata('logged_crt_login_id')){
				$crt_approve_btn = '<button class="btn btn-success btn-xs crt_approve_btn" data-id="'.$kyu['id'].'"><i class="fa fa-thumbs-o-up"></i> Approve</button>';
				$curr_user_is_approver = 'curr_user_is_approver';
			}
			//$crt_view_approvers_btn = '<button class="btn btn-primary btn-xs crt_view_approvers_btn" data-id="'.$kyu['id'].'" data-assigned="'.$crt_assigned_match.'"><i class="fa fa-eye"></i> View Approvers</button>';
		}
		
	}
	
	else if ($kyu['status'] == '0'){
		$status_text = 'In Progress';
		$status_class = 'text-danger';
	}
	
	else{
		$status_text = 'Completed';
		$status_class = 'text-success';
	}
		
?>            
            <?php
			//if step 1 = $rpack_step_no = 2
			if ($rpack_step_no == '2') {?>
            <tr>
                <td>
                  <h4 class="text-normal">
                    <?php echo $kyu['question']; ?><br><small><?php echo $kyu['answer']; ?></small></h4>
                </td>
                <td class="text-right"></td>
                <td class="text-center">
                    <button class="btn btn-default disabled" style="padding:3px; background-color: rgba(91,192,222,.3); border: 1px solid #5bc0de;"><i class="fa fa-check" style="font-size: 165%;"></i></button>
                </td>
          </tr>
                
            
            <?php }  ?>
    
          <?php if ($kyu['category_id'] != 0){

			$tsk_cat = $this->master_model->getRecords('cf_recall_steps_category',array('id'=>$kyu['category_id']));
			if ($curr_sub_c != $tsk_cat[0]['id']){
			?>
            
          <tr class="info hidden">
			<td colspan="3"></td>
          </tr>
          
          <tr class="info hidden-xs">
			<td colspan="3">
            	<h4 style="text-transform:uppercase; font-weight:bold;">
                	<?php echo $tsk_cat[0]['category_name']?>
                </h4>
            </td>
          </tr>
            
          <tr class="info visible-xs">
			<td>
            	<h4 style="text-transform:uppercase; font-weight:bold;">
                	<?php echo $tsk_cat[0]['category_name']?>
                </h4>
            </td>
          </tr>
            
		  <?php } } ?>

          <tr <?php if ($rpack_step_no == '2') { echo 'class="hidden"';}?>>
            <td>
            


				<h4>
                
			  <?php
			if($kyu['question'] !='Hazard Analysis'){ //$countqq != 0){
			  echo $kyu['question']; } ?>
              
				  <?php if ($kyu['question'] =='Hazard Analysis'){ ?>
                  
                  <small><button class="btn btn-primary btn-sm save_stp3_table <?php if ($kyu['status'] != '0') {echo 'hidden'; } else if($kyu['status'] != '0'){echo 'disabled'; }?>" style="display: none;">Save</button><span class="stp3_remaining_items text-red" style="padding-left: 5px;"></span></small>
                  
				  <?php } ?>
              
              
              <br><small>
              
              
              
              
              	<?php if ($kyu['question'] !='Hazard Analysis'){ //$countqq != 0){ ?>
                


					<?php if ($kyu['answer'] !=''){
						$note_view = '';
					?>
                    	
               
                    <?php } else if($kyu['answer'] =='' && $kyu['status'] == '0'){
						 $note_view = 'style="display: none;"';
					?>
                        <span id="tsk_not_strted_holder">Task not yet started</span>
                    <?php } else {
						 $note_view = 'style="display: none;"';
					?>
                        <span id="tsk_not_strted_holder">No notes added</span>
                    <?php } ?>
                        <a class="btn btn-default btn-xs" href="#" id="show_rc_respond_btn" <?php echo $note_view; ?> data-toggle="modal" data-target="#recallViewRespond" onclick="getRCTaskRespond('<?php echo $kyu['id']; ?>');">View task notes</a><br>
                    
                
                	<?php
						//get task issues
						$issues = $this->master_model->getRecords('cf_recall_blockers', array($recall_field_name=>$recall[0]['id'], 'task_id'=>$kyu['id']));
						
						$issue_btn = 'style="display: none;"';
						$issue_text = 'Issue';
						if(count($issues) > 0 && $kyu['status'] == '0'){
							$issue_btn = '';
						}
						if(count($issues) > 1){
							$issue_text = 'Issues';
						}
						
					?>
              		<span class="span_issue hidden" <?php echo $issue_btn; ?>><a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-default btn-xs view_issue_btn">View <?php echo count($issues).' '.$issue_text ?></a><br /></span>
                
              
                    
                <?php } //check if question 1 
				
				else{
					
					//get value of the table asnwers
					if ($kyu['answer'] != ''){
						$s3_assessment = explode(' ',$kyu['answer']);
					}
					else{
						$s3_assessment = array('','','','','','');
					}
					//show on top assigned in step 3 table
                    if ($crt_assigned != ''){
						   echo  'Assigned to: <span class="label orange" style="border-radius: 15px;">'.$this->common_model->getname($crt_assigned).'</span>'.$crt_date_assigned.' '.$crt_approve_btn;
					   }
					   else{
							echo 'Assigned to: <span class="label danger na_label" style="border-radius: 15px;">Not Assigned</span> '.$crt_approve_btn;
					   }
						   
						   
					if(count($theapprovers) > 0){
						echo '<p class="text-muted" style="margin-top: 10px;">Assigned Approver(s)</p>';
						echo '<ol class="text-muted">';
						foreach($theapprovers as $r=>$thapp){
							echo '<li>'.$thapp['name'].''.$thapp['status'].'</li>';
						}
						
						echo '</ol>';
					}
					
					 ?>
                
                  		</small></h4>
                        <div class="visible-md visible-lg">
                            <div id="task_guidance_h<?php echo $kyu['id'] ?>-lg" style="display: none;">
                                <span  class="thumbnail" >
                                <?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide']); ?>
                                </span>
                            </div>
                        </div>
                    
                



                        <div>
                        
                
                
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Severity</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[0] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="hidden" name="step_3_table_id" value="<?php echo $kyu['id']?>"/>
                            	<input type="radio" name="severity" class="input_hidden" value="Minor" <?php if ($s3_assessment[0] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[0] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Moderate" <?php if ($s3_assessment[0] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[0] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Major" <?php if ($s3_assessment[0] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[0] == 'Serious') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="severity" class="input_hidden" value="Serious" <?php if ($s3_assessment[0] == 'Serious') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Serious</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Presence</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[1] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Rare" <?php if ($s3_assessment[1] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[1] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Some" <?php if ($s3_assessment[1] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[1] == 'Most') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="Most" <?php if ($s3_assessment[1] == 'Most') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Most</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[1] == 'All') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="presence" class="input_hidden" value="All" <?php if ($s3_assessment[1] == 'All') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">All</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">
                        
                        

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Likely Injury</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[2] == 'Rare') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Rare" <?php if ($s3_assessment[2] == 'Rare') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Rare</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[2] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Some" <?php if ($s3_assessment[2] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[2] == 'Many') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Many" <?php if ($s3_assessment[2] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[2] == 'Certain') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="like_dist" class="input_hidden" value="Certain" <?php if ($s3_assessment[2] == 'Certain') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Certain</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Distribution</div>
                          <div class="col-sm-2 col-xs-3 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[3] == 'Small') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Small" <?php if ($s3_assessment[3] == 'Small') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Small</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[3] == 'Some') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Some" <?php if ($s3_assessment[3] == 'Some') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Some</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[3] == 'Many') { echo 'selected'; }?>">

                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="Many" <?php if ($s3_assessment[3] == 'Many') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Many</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[3] == 'National') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="distribution" class="input_hidden" value="National" <?php if ($s3_assessment[3] == 'National') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">National</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info2" style="padding:25px 0 25px 15px;">Identification</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[4] == 'Easy') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Easy" <?php if ($s3_assessment[4] == 'Easy') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Easy</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[4] == 'Possible') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Possible" <?php if ($s3_assessment[4] == 'Possible') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Possible</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[4] == 'Difficult') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Difficult" <?php if ($s3_assessment[4] == 'Difficult') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Difficult</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[4] == 'Undetectable') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="identification" class="input_hidden" value="Undetectable" <?php if ($s3_assessment[4] == 'Undetectable') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Undetectable</span></p>
                            </label>
                          </div>
                          
                        </div>
                        <div style="clear: both;">

                          <div class="col-sm-3 col-xs-3 no-margin-btm hidden-xs bg-info3" style="padding:25px 0 25px 15px;">Hazard</div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center green3 <?php if ($s3_assessment[5] == 'Minor') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Minor" <?php if ($s3_assessment[5] == 'Minor') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Minor</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center yellow <?php if ($s3_assessment[5] == 'Moderate') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Moderate" <?php if ($s3_assessment[5] == 'Moderate') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Moderate</span></p>
                            </label>
                          </div>
                          <div class="col-sm-2 col-xs-3 thumbnail no-margin-btm text-center orange2 <?php if ($s3_assessment[5] == 'Major') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Major" <?php if ($s3_assessment[5] == 'Major') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Major</span></p>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-3 thumbnail no-margin-btm text-center red2 <?php if ($s3_assessment[5] == 'Severe') { echo 'selected'; }?>">
                            <label>
                            	<input type="radio" name="hazard" class="input_hidden" value="Severe" <?php if ($s3_assessment[5] == 'Severe') { echo 'checked="checked"'; }?>/>
                            	 <p class="lead" style="margin-top: 20px;"><span style="font-size: 15px;">Severe</span></p>
                            </label>
                          </div>
                        </div>
                        </div>
                        
                
                
                
                
                
                
                
                
                
                <?php } //end of step 3 table
				
					if ($kyu['question'] != 'Hazard Analysis'){
					
						if ($crt_assigned != ''){
						   echo  'Assigned to: <span class="label orange" style="border-radius: 15px;">'.$this->common_model->getname($crt_assigned).'</span>'.$crt_date_assigned.' '.$crt_approve_btn;
					   }
					   else{
							echo 'Assigned to: <span class="label danger na_label" style="border-radius: 15px;">Not Assigned</span> '.$crt_approve_btn;
					   }
					   
							   
						if(count($theapprovers) > 0){
							echo '<p class="text-muted" style="margin-top: 10px;">Assigned Approver(s)</p>';
							echo '<ol class="text-muted">';
							foreach($theapprovers as $r=>$thapp){
								echo '<li>'.$thapp['name'].''.$thapp['status'].'</li>';
							}
							
							echo '</ol>';
						}
					   
					}
                    ?>        
              

              </small></h4>
              
                <div class="<?php if($kyu['id'] == 'Hazard Analysis') { echo 'visible-sm visible-xs'; } ?>">
					<div id="task_guidance_h<?php echo $kyu['id'] ?>" style="display: none;">
                    	<span  class="thumbnail" >
                    	<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide']); ?>
                        </span>
                    </div>
                </div>
                        
              
              
				<div class="clearfix"></div>
     			<div class="visible-xs" style="margin-top: 10px;">
                
    
                    <div class="dropdown pull-left">
                      <button class="btn btn-default btn-sm" data-toggle="dropdown">
                        Action
                        <span class="caret"></span>
                      </button>
                      
                      <ul class="dropdown-menu text-left">
                        <?php
                            if($kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){ 
                                $assign_stat = 'not_assigned';
                            }
                            else{
                                $assign_stat = 'assigned';
                            }
                        ?>
                        <li class="<?php if($kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){ echo 'disabled'; }?>">
                            <a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" id="inc_tskm<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','1','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                                Mark as Incomplete
                            </a>
                        </li>
                        <li class="<?php echo $curr_user_is_approver; if($kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){ echo 'disabled'; }?>">
                            <a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" id="comp_tskm<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','0','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                                Mark as Completed
                            </a>
                        </li>
                        
                        <?php echo $crt_view_approvers_btn; ?>
                        <?php echo $add_issue_btn; ?>
                        
                        
                      </ul>
                    </div>       
                    <a class="btn bt-link disabled <?php echo $status_class; ?> task_text_status" style="background-color: transparent;">
                        <?php echo $status_text; ?>
                    </a>
                                      
                                        
                </div>
                
                    

              <p class="visible-xs">

                <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                </span><br />
                

				<?php if ($kyu['assigned'] == ''){ ?>
                    <a href="#" class="btn btn-sm btn-default" id="pickup_btn_xs<?php echo $kyu['id']; ?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','','<?php echo $this->session->userdata('logged_display_name'); ?>','<?php echo $kyu['assigned']?>');"><i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pick Up Task"></i></a>
                    
                    <a href="#" id="comptask_btn<?php echo $kyu['id'];?>" class="btn btn-sm btn-default" style="background:#e0e0e0; <?php if ($kyu['status'] != '0' || $kyu['assigned'] == ''){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','','','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');"><i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i></a>

                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                <?php } ?>    

                <?php
                if ($kyu['assigned'] != '' && $kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){
                    ?>
                    

                    <a href="javascript:;" class="btn btn-sm btn-default tooltip-test" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none;'; }?>" title="Task already assigned"><i class="fa fa-user"></i></a>
                    
                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                    <?php
                     }
                             
                     if ($kyu['assigned'] == $this->session->userdata('logged_crt_login_id')) {?>

                    
                    <a href="#" id="comptask_btn<?php echo $kyu['id'];?>" class="btn btn-sm btn-default" style="background:#e0e0e0; <?php if ($kyu['status'] != '0' || $kyu['assigned'] == ''){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');"><i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i></a>
                    
                    
                                          <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                  
                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>


                <?php } ?>

                
            
              </p>
              
              <p class="visible-sm visible-md">
              
              
                <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                </span><br />
                
                    
              		
				<?php if ($kyu['assigned'] == ''){ ?>
                    <a href="#" class="btn btn-sm btn-default" id="pickup_btn_sm<?php echo $kyu['id']; ?>"  style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','','<?php echo $this->session->userdata('logged_display_name'); ?>','<?php echo $kyu['assigned']?>');"><i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pick Up Task"></i></a>
                    
                    
                    <a href="#" id="comptask_btn<?php echo $kyu['id'];?>" class="btn btn-sm btn-default" style="background:#e0e0e0; <?php if ($kyu['status'] != '0' || $kyu['assigned'] == ''){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');"><i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i></a>

                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>



<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                <?php } ?>    

                <?php
                if ($kyu['assigned'] != '' && $kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){
                    ?>
                    

                    <a href="javascript:;" class="btn btn-sm btn-default tooltip-test" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none;'; }?>" title="Task already assigned"><i class="fa fa-user"></i></a>
                    
                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                    <?php
                     }
                             
                     if ($kyu['assigned'] == $this->session->userdata('logged_crt_login_id')) {?>

                    
                    <a href="#" id="comptask_btn<?php echo $kyu['id'];?>" class="btn btn-sm btn-default" style="background:#e0e0e0; <?php if ($kyu['status'] != '0' || $kyu['assigned'] == ''){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');"><i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i></a>
                    
                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>


                <?php } ?>

                          
              </p>              
              
            </td>

            
            <td class="text-right hidden-xs hidden-sm hidden-md">
            


            
                                            
				<?php if ($kyu['assigned'] == ''){ ?>
                    <a href="#" class="btn btn-sm btn-default" id="pickup_btn_md<?php echo $kyu['id'];?>" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallModal" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','','<?php echo $this->session->userdata('logged_display_name'); ?>','<?php echo $kyu['assigned']?>');"><i class="fa fa-user tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pick Up Task"></i></a>
                    
                    <a href="#" id="comptask_btn<?php echo $kyu['id'];?>" class="btn btn-sm btn-default" style="background:#e0e0e0; <?php if ($kyu['status'] != '0' || $kyu['assigned'] == ''){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');"><i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i></a>
                    
                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                <?php } ?>    

                <?php
                if ($kyu['assigned'] != '' && $kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){
                    ?>
                    

                    <a href="javascript:;" class="btn btn-sm btn-default tooltip-test" style="background:#e0e0e0; <?php if ($kyu['status'] != '0'){ echo 'display: none;'; }?>" title="Task already assigned"><i class="fa fa-user"></i></a>
                    
                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>
                    <?php
                     }
                             
                     if ($kyu['assigned'] == $this->session->userdata('logged_crt_login_id')) {?>

                    
                    <a href="#" id="comptask_btn<?php echo $kyu['id'];?>" class="btn btn-sm btn-default" style="background:#e0e0e0; <?php if ($kyu['status'] != '0' || $kyu['assigned'] == ''){ echo 'display: none;'; }?>" data-toggle="modal" data-target="#recallRespond" onclick="recallAssign('<?php echo $rc['id']?>','<?php echo $kyu['id']?>','<?php echo trim(preg_replace('/\s+/', ' ', $kyu['answer']))?>','','<?php echo $kyu['assigned']?>');addTGuide('<?php echo str_replace(array("\n", "\r"), '<br>', $kyu['task_guide'])?>');getRespond('<?php echo $kyu['id']; ?>');"><i class="fa fa-edit tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Task Notes"></i></a>
                    
                    
                                        <a href="#" data-id="<?php echo $kyu['id'] ?>" class="btn btn-sm btn-danger view_issue_btn" <?php echo $issue_btn; ?>><i class="fa fa-exclamation tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="View <?php echo count($issues).' '.$issue_text ?>"></i></a>

                    
<?php if($kyu['task_guide'] != '') { ?><a href="javascript:;" class="btn btn-sm btn-danger" onclick="$('#task_guidance_h<?php echo $kyu['id'] ?>').slideToggle();" style="background: #fff; <?php if($kyu['status'] != '0'){ echo 'display: none'; } ?>"><i class="fa fa-question text-red"></i></a><?php } ?>


                <?php } ?>

              
              
                <span class="small date_complete<?php echo $kyu['id']?>" <?php if ($kyu['status'] == 0) { echo 'style="display: none; "'; }?>>
                    Time to Complete:<br><span class="text-muted"><?php echo $kyu['time_lapse']; ?></span>
                </span><br />
                    


            </td>
            
            <td class="text-center hidden-xs">
            
                <div class="dropdown pull-right">
                  <button class="btn btn-default btn-sm" data-toggle="dropdown">
                    Action
                    <span class="caret"></span>
                  </button>
                  
                  <ul class="dropdown-menu text-right">
                    <?php
						if($kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){ 
							$assign_stat = 'not_assigned';
						}
						else{
							$assign_stat = 'assigned';
						}
					?>
                    <li class="<?php if($kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){ echo 'disabled'; }?>">
                    	<a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '0'){ echo 'display: none;'; }?>" id="inc_tsk<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','inc_tsk<?php echo $kyu['id'] ?>','inc_tskm<?php echo $kyu['id'] ?>','1','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                        	Mark as Incomplete
                        </a>
                    </li>
                    <li class="<?php echo $curr_user_is_approver; if($kyu['assigned'] != $this->session->userdata('logged_crt_login_id')){ echo 'disabled'; }?>">
                    	<a href="javascript: void(0);" style=" <?php if ($kyu['status'] == '1'){ echo 'display: none;'; }?>" id="comp_tsk<?php echo $kyu['id']; ?>" onclick="checkTheCBox('<?php echo $kyu['id'] ?>','comp_tsk<?php echo $kyu['id'] ?>','comp_tskm<?php echo $kyu['id'] ?>','0','<?php echo ${'step'.$rpack_step_no.'_label'}; ?>','step_<?php echo $rpack_step_no ?>btn','<?php echo ${'totq'.$rpack_step_no}; ?>','#proc<?php echo $rpack_step_no; ?>','<?php echo $assign_stat; ?>');">
                        	Mark as Completed
                        </a>
                    </li>
                    
                    <?php echo $crt_view_approvers_btn; ?>
					<?php echo $add_issue_btn; ?>
                  </ul>
                </div>       
                
                <p class="small pull-right <?php echo $status_class; ?> task_text_status"><?php echo $status_text; ?></p>

            </td>

            
            
          </tr>
                                   
	   <?php 
		$countqq++;
		$curr_sub_c = $kyu['category_id'];
		}
         ?>
         
    
        
             
        </tbody>
      </table>        
          
    <div class="well  <?php if ($rpack_step_no == '2') { echo 'hidden';}?>" style="margin:5px; margin-top: 15px;">
        <h3>Forum <small>Most Recent</small>
        </h3>
        <hr />
        <?php
        
            $cc_id=$this->session->userdata('team_cc');
            $org_id=$this->session->userdata('crt_selected_orgnaization');
            $scen_idee = 'r'.$rc['id'];
			$forumcat = $rpack_step_no + 2;
            $forum_step_posts = $this->master_model->getRecords('forum_post_master',array('cc_id'=>$cc_id,'org_id'=>$org_id,'scenario_id'=>$scen_idee,'fp_category_id'=>$forumcat),'*',array('date_created'=>'DESC'),'','2');

            if (count($forum_step_posts) > 0){
                
            foreach($forum_step_posts as $sp=>$fsp){		
            ?>

            <p style="padding-bottom: 15px; border-bottom: 1px solid #eee;">
                <b>Posted by <?php echo $my_class->getcrtname($fsp['fp_author_id']).'';?> </b><br /><?php echo $fsp['fp_title'] ?> <a href="<?php echo base_url().'forum/post/'.$scen_idee.'/'.$fsp['fp_id']; ?>">Read more..</a></p>

            <?php }  } else{
				echo '<p class="text-muted">Nothing found..</p><hr>'; } ?>

            <p><a class="btn btn-primary" href="<?php echo base_url().'forum/topics/'.$scen_idee.'/'.$forumcat; ?>">Add New Post</a></p>
    </div>                           


                    
                                    
                                    
                                </div><!--./step 2-->                                
   <?php                             
		$rpack_step_no++;
	} //end loop of step

	?>                            
                                

                                </div>
                            </div>


                        </div>



    
    
                    </div>
                </div>
                                  
    
    

        </div>
        <!-- /.panel -->
        
     </div>
                                    
   </div>                             
                                                
                <?php		
					
					//end recall pack
					//return false;
				}
				
				
				
			}//end loop recall
		}
	}//end of check recall
			?>

<?php } //END CONTINUITY ?>




<!------START NOTIFY INCIDENT RECALL AND CONTINUITY MODAL SHARE------>
<?php if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){ ?>


<?php if (count($recall) > 0) { ?>

   
<!-- add issue -->
<div class="modal fade" id="issueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <form id="issue_form">
            	<div class="form-group">
                	<label>Blocker</label>
                    <input type="text" class="form-control" name="blocker" />
                    <input type="hidden" name="task_id" />
                    <input type="hidden" name="issue_id" />
                    <input type="hidden" name="recall_id" value="<?php echo $recall[0]['id'] ?>" />
                    <input type="hidden" name="incident_type" value="<?php echo $active_module; ?>" />
                    
                </div>
            	<div class="form-group">
                	<label>Issue</label>
                    <textarea rows="5" class="form-control" name="issue"></textarea>
                </div>
            	<div class="form-group">
                	<label>Impact</label>
                    <textarea rows="5" class="form-control" name="impact"></textarea>
                </div>
            	<div class="form-group">
                	<label>Assign</label>
                    <select class="form-control" name="issue_assign">
                    	<option value="">Select</option>
					   <?php
					    echo '<option value="'.$cc_id.'">'.$this->common_model->getname($cc_id).'</option>';
						if(count($all_crt) > 0){
							foreach($all_crt as $r=>$value){
								if($value['login_id'] != $cc_id){
									echo '<option value="'.$value['login_id'].'">'.$value['full_name'].'</option>';
								}
								
							}
						}
						?>
                        
                        
                    </select>
                </div>
            	<div class="form-group">
                	<label>Step no.</label>
                    <select class="form-control" name="step_no">
                    	<option value="">Select</option>
					   <?php
						if(count($recallsteps) > 0){
							foreach($recallsteps as $r=>$value){
								if($value['step_no'] != 2){
									echo '<option value="'.$value['step_no'].'">'.$value['name'].'</option>';
								}
							}
						}
						?>
                        
                        
                    </select>
                </div>
            	<div class="form-group hidden">
                	<label>Task <i class="fa fa-spinner fa-spin"></i></label>
                    <select class="form-control" name="step_task">
                    	<option value=""></option>
                    </select>
                </div>
            	<div class="form-group">
                	<button class="btn btn-primary" disabled="disabled">Submit</button>
                </div>
            </form>
        
      </div>
    </div>
  </div>
</div>   
                     
<!-- viewIssueModal-->
<div class="modal fade" id="viewIssueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">
        	Task Issue
        </h4>
      </div>
      <div class="modal-body">
        
        
      </div>
    </div>
  </div>
</div>   

                     
<!-- assign approver -->
<div class="modal fade" id="viewSimulationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        
        <p class="lead step_simu_text">Step Simulation</p>
        <p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p>
		
        <div class="simulist">
                    
            <div class="panel-group" id="accordion" role="tablist">
                     
            </div>        
        
        </div>
        
        
        
      </div>
    </div>
  </div>
</div>   

                     
<!-- assign approver -->
<div class="modal fade" id="assignApproverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
       
       	<p class="loader text-center lead text-muted"><i class="fa fa-spinner fa-spin"></i><br />Loading..</p> 
        
        <div class="approver_list">
              <h4 for="step_name">Assigned Approver(s)</h4>
            <ol>
            </ol>

            <button class="btn btn-default update_approvers_btn">Update Approver(s)</button>
        </div>
        <form id="assign_approver_form_crt">
            <!-- Text input-->
            <div class="form-group">
              <h4 for="step_name">
              	Assign Approver(s) 
                <a href="#" class="btn btn-xs btn-primary clone_approver_btn" data-toggle="tooltip" title="Add Approver"><i class="fa fa-plus"></i></a>
              </h4>  
              <input type="hidden" class="standbytask_id" name="standbytask_id" value="" />
              <input type="hidden" class="approvers_id" name="approvers_id" value="" />
            </div>
            
            
            <div class="form-group clone_this_group">
              <label>Approver <span>1</span></label>
                <select name="approvers[]" class="form-control">
                    <option value="">Select..</option>
                    <option value="<?php echo $this->session->userdata('team_cc'); ?>">
                        <?php echo $this->common_model->getname($this->session->userdata('team_cc')); ?>
                    </option>

				   <?php
                    if(count($all_crt) > 0){
                        foreach($all_crt as $r=>$value){
							if($value['login_id'] != $this->session->userdata('logged_crt_login_id')){
                            ?>
    
                        <option value="<?php echo $value['login_id'] ?>">
							<?php echo $value['full_name'] ?>
                        </option>
    
    
                            
                        <?php
							}
                            
                        }
                    }
                   ?>               
                
                </select>     
            </div>
            
            <div class="clones_holder"></div>
            
            
            <div class="form-group">
            	
                <button type="submit" class="btn btn-primary" disabled="disabled">Save changes</button>
                <a class="btn btn-default" data-dismiss="modal">Cancel</a>
            </div>
            
        </form>
      </div>
    </div>
  </div>
</div>   


   

<!-- Modal -->
<div class="modal fade" id="refreshPageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p class="lead"><i class="fa fa-info-circle fa-fw text-info"></i> A team member has made an update since your last refresh. Please click 'OK' now to refresh your page to view the update before proceeding.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default hidden" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="reloadForUpdate();">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Recall Incident</h4>
        <ul class="list-group list-default">
            <?php if(count($open_recall) > 0){
                $count_open = 0;
                $uri_5 = $this->uri->segment(5);
                foreach($open_recall as $open=>$opeen){
            ?>
            
            <a href="<?php echo base_url().'crt/dashboard/index/r/'.$count_open ?>" class="list-group-item <?php if ($uri_5 == $count_open) { echo 'active'; }?>"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
            
            <?php
                $count_open++;	
                } 
            } ?>
        </ul>


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Situation Map</h4>
      </div>
      <div class="modal-body">
      	<div id="map-canvas" style="min-height: 512px; width: 100%;"></div>
      	<div id="map-legend" style="padding-top: 15px;">
        	<p>
            <span class=""><img src="<?php echo base_url().'assets/img/map-marker-blue.png' ?>" style="height: 25px;"/> <span class="bluemark-count"></span></span>
            <span class="" style="margin-left: 15px;"><img src="<?php echo base_url().'assets/img/map-marker-red.png' ?>" style="height: 25px;"/> <span class="redmark-count"></span></span>
            <span class="" style="margin-left: 15px;"><img src="<?php echo base_url().'assets/img/google_purple_icon_sm.png' ?>" style="height: 25px;"/> <span class="yellow-count"></span></span>
            </p>
        </div>
      </div>
    </div>
  </div>
</div>
    
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Response Team Contacts</h4>
      </div>
      <div class="modal-body">
      
          <div class="table-responsive">
            <table class="table table-hover table-bordered table-green" id="example-table">
    
                <thead>
    
                    <tr>
    
                        <th width="25%">First Name</th>
    
                        <th width="25%">Last Name</th>
    
                        <th width="25%">Position</th>
    
                        <th width="25%">Mobile No.</th>
    
                    </tr>
        
                    </thead>
        
                    <tbody>
              
                    <?php 
            
                    if(count($all_crt)>0)
            
                    {
            
                        foreach($all_crt as $r => $value)
            
                        {
							
							$mymodyul = substr($this->session->userdata('org_module'),0,1);
							if(strpos($value['module'],$mymodyul) !== false){

        
                    ?>
        
                    <tr>
        
                        <td><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?></td>
        
                        <td><?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></td>
        
                        <td><?php echo  $value['crt_position']; ?></td>
                        
                        <td><?php echo  $this->master_model->decryptIt($value['crt_mobile']); ?></td>
        
                    </tr>
                    <?php
							}
        
                        }
        
                    }
        
                    ?>
        
                    
    
                </tbody>
    
                
    
                
    
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div><!---/.response team contact list modal--->

    <!-- Modal -->
    <div class="modal fade" id="viewCategorySummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Cost Category Summary</h4>
          </div>
          <div class="modal-body">
            <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
    
    
    
    <!-- Modal -->
    <div class="modal fade" id="viewAccumulative" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Accumulative Cost</h4>
          </div>
          <div class="modal-body">
            <div class="well" style="border-radius: 0px">
                <?php if(count($recall)> 0){
                    echo $recall_accumulate;
                } ?>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="viewCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Item Cost Details</h4>
          </div>
          <div class="modal-body">
            <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
    
<?php } ?><!--end of cost monitor modals-->
    
    <!-- Modal for blockerNote-->
    <div class="modal fade" id="blockerNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <div class="modal-header">
            <h4>Notes
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </h4>
          </div>
          <div class="modal-body">
                <h2 class="text-center text-muted block_load_holder" style="display: none;"><i class="fa fa-spinner fa-spin"></i></h2>
                <div class="well blocker_note_holder" style="color: #333; border-radius: 0; display: none;"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>                



        <!-- Modal for resolveBlocker-->
        <div class="modal fade" id="resolveBlocker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="form-group">
						<label for="blocker_note">Add notes</label>
                        <h2 class="text-center text-muted block_load_holder" style="display: none;"><i class="fa fa-spinner fa-spin"></i></h2>
                        <textarea class="form-control blocker_note_holder" id="blocker_note" name="blocker_note" rows="5"></textarea>
                        <input type="hidden" id="the_blocker_id" value="" />
                        <span class="text-red blocker_note_error"></span>
                	</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="save_blocker_note">Save changes</button>
              </div>
            </div>
          </div>
        </div>                



        <!-- Modal for addBlocker-->
        <div class="modal fade" id="addBlocker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
         
                    <div class="form-group">
						<label for="blocker_name">Raise a Blocker</label>
                        <input type="text" class="form-control" id="blocker_name" name="blocker_name" placeholder="Blocker" />
                        <input type="hidden" id="step_no" value="" />
                        <span class="text-red blocker_name_error"></span>
                	</div>
         
                    <div class="form-group">
                        <textarea class="form-control" id="blocker_issue" name="blocker_issue" rows="5" placeholder="Issue"></textarea>
                        <span class="text-red blocker_issue_error"></span>
                	</div>
         
                    <div class="form-group">
                        <textarea class="form-control" id="blocker_impact" name="blocker_impact" rows="5" placeholder="Impact"></textarea>
                        <span class="text-red blocker_impact_error"></span>
                	</div>
                    
                    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="save_new_blocker">Save changes</button>
              </div>
            </div>
          </div>
        </div>                


    <!-- Modal for add respond crt-->
    <div class="modal fade" id="recallRespond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <p class="lead">Task Notes <i class="fa fa-question-circle text-info small" style="cursor: pointer;" onclick="$('#modal_guide_holder').slideToggle();" id="modal_guide_btn"></i></p>
			
            <div id="modal_guide_holder" style="display: none;">
                <div class="well" style="#fff;"></div>	
            </div>
            


            <div class="success_holder_mess"></div>
            <textarea class="form-control" id="txt_message" name="txt_message" placeholder="Enter task notes" rows="15"></textarea>
			<div class="text_error text-red"></div>	
            

            <div style="padding-top: 15px;">

                <a id="atta_file_btn" href="javascript:;" onclick="$('.upload_section').show(); $(this).after('&nbsp;').hide();" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>

            </div>
            
           
             <div class="upload_section" style="display: none;">
             	<?php $uniquetime= time().mt_rand(); ?>

                 <div class="col-xs-6" style="padding-top: 0">
                    <form id="myForm" action="<?php echo base_url().'crt/recall/upload' ?>" method="post" enctype="multipart/form-data">
                         <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                         <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                         <input type="hidden" id="the_question_id" name="the_question_id" value="">
                         <input type="submit" class="hidden" value="Ajax File Upload">
                     </form>                                                   
                </div>
                 
                 <div class="col-xs-6" style="padding-top: 0">
                    <div id="progress" style="display: none;">
                        <div id="bar"></div>
                        <div id="percent">0%</div >
                    </div>
                </div>

                <div class="col-xs-12">
    
                    <div id="file_up_text" style="display: none;"><p>File Attached: <div id="message"><a href=""></a></div></p></div>
    
                </div>

                                                                   
            </div>
            
            
          </div><div style="clear: both"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" id="save_message_rc">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    
    

    <!-- Modal for viewing task respond-->
    <div class="modal fade" id="recallViewRespond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<div id="respondtask_holder">
            </div>
			

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>

    

		<?php
        
            $whr_arr=array('cc_id'=>$this->session->userdata('team_cc'));
            $select="login_id,crt_first_name,crt_last_name,crt_email";
            $order=array('crt_first_name'=>'ASC','crt_last_name'=>'ASC');
            $str_whr="(org_id=0 OR org_id=".$this->session->userdata('crt_selected_orgnaization').")";
            
            $this->db->where($str_whr,NULL,FALSE);
            $this->db->where('login_id != ',$this->session->userdata('team_cc'));
            $my_team=$this->master_model->getRecords('cf_crisis_response_team',$whr_arr,$select,$order);
        
        
        ?>

        <!-- Modal for assign crt recall-->
        <div class="modal fade" id="recallModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <?php /*?><form action='' name="frm-compose-message" id="frm-compose-message" method='post' class="form-horizontal" role="form" validate><?php */?>
              <div class="modal-body">
                <p class="lead">Please confirm you wish to pick up task.</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

					<div class="success_holder"></div>

                    <div class="form-group hidden">

                     <div class="col-sm-12">
                         <input type="hidden" id="the_recall_id" name="the_recall_id" value="">
                         <input type="hidden" id="the_current_assigned" name="the_current_assigned" value="">
                         <input type="hidden" id="pickup_crtname" name="pickup_crtname" value="">
                         <input type="hidden" id="message_receiver" name="message_receiver" value="<?php echo $this->session->userdata('logged_crt_login_id'); ?>">
                        <?php /*?> <input type="hidden" id="the_question_id" name="the_question_id" value=""><?php */?>
                            <?php /*?><select class="form-control" name="message_receiver" id="message_receiver">

                            <option value=""> Select </option>

                            <?php

                               if(count($my_team) > 0)

                                {

                                    foreach($my_team as $team)

                                    {

                            ?>

                                    <option value="<?php echo $team['login_id'] ?>" <?php if($this->session->userdata('logged_crt_login_id') == $team['login_id']){echo 'selected="selected"';} ?>>

                                    <?php echo $this->master_model->decryptIt($team['crt_first_name']).' '.$this->master_model->decryptIt($team['crt_last_name']); ?>

                                    </option>

                            <?php

                                    }

                                }

                            ?>

                           </select>	<?php */?>						
							<div class="err_holder text-red"></div>
                        </div>

                    </div>

    
    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="assign_recall">OK</button>
              </div>
            <?php /*?></form>	<?php */?>	               
            </div>
          </div>
        </div>          
              
<?php } //end of modal share ?>

<!------START CASE MANAGEMENT------>
<?php if (strpos($the_active_module[0]['active_module'], '2') !== false){
        //active case report count
        $num_report=$this->master_model->getRecordCount('case_master',array('crt_id'=>$this->session->userdata('logged_crt_login_id'),'cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'0'));
?>

<div class="col-md-12 welcomeScreen">
    <div class="panel panel-default col-md-6 col-md-offset-3 text-center">
      <div class="panel-body">
        <h2>Hi <?php echo $this->session->userdata('logged_display_name'); ?></h2>
        
        <?php if($num_report== 0){?>
        <p class="lead">Currently no open cases assigned to you.</p>
        <?php } else { ?>
        <p class="lead">Currently there are <?php echo $num_report; ?> open cases assigned to you.</p>
        <?php } ?>
        
      </div><!--.panel body-->
    </div>
</div><!---.welcomeScreen-->
<div style="clear: both;"><hr></div>
   
   

<?php
	}
    ?>
<!------END CASE MANAGEMENT------>
   

<!------START RECALL AND CONTINUITY MANAGEMENT------>
<?php if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false){

if(count($recall) == 0){

	?>


<div class="col-md-12 welcomeScreen">
    <div class="panel panel-default col-md-6 col-md-offset-3 text-center">
      <div class="panel-body">
        <h2>Hi <?php echo $this->session->userdata('logged_display_name'); ?></h2>
        
        <p class="lead">Welcome to CrisisFlo. There are currently no active incidents.</p>
        
      </div><!--.panel body-->
    </div>
</div><!---.welcomeScreen-->
<div style="clear: both;"><hr></div>
   
   

<?php
		}
	}
    ?>
<!------END RECALL AND CONTINUITY MANAGEMENT------>
   
<!------START STANDARD CRISISFLO------>
<?php if (strpos($the_active_module[0]['active_module'], '1') !== false){ ?>

   
<?php if($main_status=="Pre-Incident Phase" ){ ?>

<?php /*GET THE DATA FROM THE SESSION**************************/

	$session_userfname = $this->session->userdata('logged_display_name');

/*********************************************************/?>
<div class="col-md-12 welcomeScreen">
    <div class="panel panel-default col-md-6 col-md-offset-3 text-center">
      <div class="panel-body">
        <h2>Hi <?php echo $session_userfname; ?></h2>
        <p class="lead">Current status is Pre-Incident Phase.</p>
        
      </div><!--.panel body-->
    </div>
</div><!---.welcomeScreen-->
<div style="clear: both;"><hr></div>










<?php 

}//end of pre-incident phase

else

{

?>

<div class="col-lg-12">


  <div class="panel panel-default">

    <div class="panel-heading">

      <div class="panel-title">

        <h4>My Assigned Tasks</h4>

      </div>

      	<div class="panel-widgets">

			<a class="" data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

		</div>

      <div class="clearfix"></div>

    </div>

    <div id="validationExamples" class="panel-collapse collapse in">

    <div class="panel-body">

      <div id="myTabContent" class="tab-content">

        <div class="tab-pane fade active in" id="home">

          <?php if(count($my_task)>0){?>

          <div class="table-responsive">

            <table  class="table table-striped table-bordered table-hover table-green myexample-table">

              <thead>

                <tr>

                  <th width="26%">Scenario</th>

                  <th width="26%">Task</th>

                  <th width="18%">Assign To</th>

                  <th width="14%">Status</th>

                  <th width="16%">Action</th>

                </tr>

              </thead>

              <tbody>

                <?php 

				if(count($my_task)>0)

				{

					foreach($my_task as $mtask)

					{

						if($mtask['task_status']=='1')

						{ $status = '<i class="fa  text-red"></i> <span class="text-red">In Progress</span>';

						}

						if($mtask['task_status']=='2')

						{ $status = '<i class="fa text-green"></i> <span class="text-green">Completed</span>';

						}

						if($mtask['task_status']=='0')

						{ $status = '<i class="fa  text-red"></i> <span class="text-red">Not Started</span>';

						}

				?>

               	 	<tr>

                    	<td><?php echo $mtask['scenario']; ?></td>

                        <td><?php echo $mtask['task']; ?></td>

                        <td><?php echo $this->master_model->decryptIt($mtask['crt_first_name']).' '.$this->master_model->decryptIt($mtask['crt_last_name']); ?></td>

                        <td><?php echo $status; ?></td>

                        <td>

                        <?php if($mtask['task_status']=='2'){?>

                        <a class="btn btn-default btn-xs" href="javascript:void(0);">Task Completed</a>

                        <?php }else{ ?>

                        <a class="btn btn-default btn-xs" href="javascript:;" onclick="return complete_confirm('<?php echo base_url().'crt/dashboard/changestatus/'.$mtask['task_id'];?>')">Mark as Completed</a>  

                        <?php } ?>

                        </td>

                    </tr>	

                <?php 

					}

				}

				?>

                

              </tbody>

            </table>

          </div>

          <!-- /.table-responsive -->

          <?php }
		  else{ ?>
                <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
				<p class="text-center" style="margin-top: 20px; color: #ccc">No tasks assigned</p>
		  <?php }?>

        </div>

        

        

      </div>

    </div>

    </div>

    <!-- /.panel-body --> 

  </div>

  <!-- /.panel --> 

  

</div>

<?php 
	/* each initiated scenarion will be display seperatly*/	

	if(count($initiated_scn)>0)

	{

		foreach($initiated_scn as $scenario)

		{

?>

            <div class="col-lg-12">
            
              <div class="panel panel-default">

                <div class="panel-heading">

                  <div class="panel-title">

                    <h4>Group Tasks for Scenario: <?php echo $scenario['scenario']; ?></h4>

                  </div>

                  <div class="panel-widgets">
					<a class="" data-toggle="collapse" data-parent="#accordion_<?php echo $scenario['scenario_id']; ?>" href="#validationExamples_<?php echo $scenario['scenario_id']; ?>"><i class="fa fa-chevron-down"></i></a>

                  </div>

                  <div class="clearfix"></div>

                </div>

                <div id="validationExamples_<?php echo $scenario['scenario_id']; ?>" class="panel-collapse collapse in">

                <div class="panel-body">

                   	<?php 

					/*display task for each scenario */

					$task_list=$this->common_model->get_scenario_task($scenario['scenario_id']);

					if(count($task_list)>0)

					{

						

					?>

                    <div id="myTabContent" class="tab-content">

                    <div class="tab-pane fade active in" id="home">

                      <div class="table-responsive">

                        <table  class="table table-striped table-bordered table-hover table-green myexample-table"><!--id="example-table"-->

                          <thead>

                            <tr>

                              <th width="50%">Task Name</th>

                              <th width="25%">Owner</th>

                              <th width="15%">Status</th>

                             <?php /*?> <th width="10%">Action</th><?php */?>

                            </tr>

                          </thead>

                          <tbody>

                            <?php 

                           	foreach($task_list as $task)

                            {

								 	if($task['task_status'] == 1 ){

                                    $status = ' <span class="text-red">In Progress</span>';

                                    } if($task['task_status'] == 0 ) {	

                                    $status = '<span class="text-red">Pre-Incident</span>';

                                    }

									 if($task['task_status'] == 2 ){

                                    $status = '<span class="text-green">Completed</span>';

                                    } 

                            ?>

                                <tr>

                                    <td><?php echo $task['task']; ?></td>

                                    <td><?php echo $this->master_model->decryptIt($task['crt_first_name']).' '.$this->master_model->decryptIt($task['crt_last_name']); ?></td>

                                    <td><?php echo $status; ?></td>

                                  <?php /*?>  <td><a class="btn btn-default btn-xs" href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'crt/dashboard/delete_task/'.$task['task_id']; ?>');">Delete</a> </td><?php */?>

                                </tr>	

                            <?php 

							}

                            ?>

                            

                          </tbody>

                        </table>

                      </div> 

                    </div>

                    </div>

                      <!-- /.table-responsive -->

                      <?php }
					  else{ ?>
                            <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
							<p class="text-center" style="margin-top: 20px; color: #ccc">No tasks for this scenario</p>
					  <?php }?>
					  
                    

                  

                </div>

                </div>

                <!-- /.panel-body --> 

              </div>

              <!-- /.panel --> 

              

            </div>

<?php

		}

	}

	/* Display scenario and there messages */

?>


<!-- Scenarion And messages -->

<div class="col-lg-12">

<?php 

if(count($scenario)>0){

for($i=0;$i<count($scenario);$i++)

{

?>	

  <div class="panel panel-default" style="display:none;"><!--hide forum-->

    <div class="panel-heading">

      <div class="panel-title">

        <h4><?php echo $scenario[$i]['scenario'];?></h4>

      </div>

      <?php // Fetching Massage for A scenario

	  $this->db->join('cf_crisis_response_team','cf_crisis_response_team.login_id=common_messages.sender_id');

	  $messages=$this->master_model->getRecords('common_messages',array('scenario_id'=>$scenario[$i]['scenario_id']),'*',array('com_message_id'=>'DESC'));

	 ?>

		<div class="panel-widgets">

			<a class="" data-toggle="collapse" data-parent="#accordion" href="#myscenario_<?php echo $i;?>"><i class="fa fa-chevron-down"></i></a>

		</div>

      <div class="clearfix"></div>

    </div>

    	

    <div id="myscenario_<?php echo $i;?>" class="panel-collapse collapse in">

    <div class="col-xs-12">

    <a class="btn btn-default btn-sm common_msg_click pull-right" style="margin:15px; margin-right: 0; " href="#modal_<?php echo $i;?>" id="superscaled">Send Message</a></div>

    <div class="panel-body">

      <div id="myTabContent" class="tab-content">

        <div class="tab-pane fade active in" id="home">

          <?php if(count($messages)>0){?>

          <div class="table-responsive">

            <table  class="table table-striped table-bordered table-hover table-green myexample-table">

              <thead>

                <tr>

                <th>Post by</th>

                <th>Post Date</th>

                  <th>Message</th>

                </tr>

              </thead>

              <tbody>

              		<?php foreach($messages as $msg){ ?>

               	 	

                    <tr>

                    	<td><?php echo $this->master_model->decryptIt($msg['crt_first_name']).' '.$this->master_model->decryptIt($msg['crt_last_name']); ?></td>

                        <td><?php echo date('d-m-Y',strtotime($msg['send_date'])) ; ?></td>

                       	<td><?php echo substr($msg['common_message'],'0','100').'..';?>

							<a href="<?php echo base_url(); ?>crt/dashboard/read/<?php echo $msg['com_message_id']?>">&nbsp;&nbsp;<?php echo "Read More"; ?></a>	

                       <!--<a href="<?php echo base_url(); ?>crt/dashboard/read/<?php echo $msg['com_message_id']?>"><?php echo $msg['common_message']; ?></a>--></td>

                    </tr>

                    <?php } ?>	

              </tbody>

            </table>

          </div>

          <!-- /.table-responsive -->

          <?php }else{?>

            <p class="text-muted text-center">Sorry, No message for this scenario.</p>

          <?php } ?>

        </div>

      </div>

      	

    </div>

    </div>

    <!-- /.panel-body --> 

  </div>

  <!-- /.panel -->

<!-- POPUP VIEW -------------------------------------------------------------------------------------------- -->

	<div id="modal_<?php echo $i;?>" style="display: none;" class="modal-example-content nn">

        <div class="modal-example-header">

            <button type="button" class="close" onclick="$.fn.custombox('close');">&times;</button>

            <h4>Enter Message</h4>

        </div>

        <div class="panel-body" style="padding-right:30px;padding-top:10px;padding-left:10px;">

            <form action='' name="frm-add-task" id="frm-add-task" method='post' class="form-horizontal" role="form" validate>

                <div class="form-group">

                            <label class="col-sm-2 control-label">Message</label>

                            <div class="col-sm-10">

                                <textarea class="form-control" id="common_message" name="common_message" placeholder="Enter Message"></textarea>

                                <input type="hidden" name="scenario_id" id="scenario_id" value="<?php echo $scenario[$i]['scenario_id'];?>"/>							

                           	</div>

                        </div>

                <div class="form-group">

                    <label class="col-sm-2 control-label"></label>

                    <div class="col-sm-10">

                        <button type="submit" class="btn btn-default" name="add_message" id="add_message">Submit</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

<!-- POPUP VIEW _____________________________________________________________________________________ --> 

<?php } }?>  

</div>





<?php

}

?>
<?php } ?>
<!-- -------END STANDARD CRISISFLO--------> 

</div>

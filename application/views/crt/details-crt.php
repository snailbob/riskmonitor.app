                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Crisis Team Member Details

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>cc/">Dashboard</a></li>

                                <li class="active">Crisis Team Member Details</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->

                <!-- Form AREA -->

				<div class="row">

                    <div align="right">

                         <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                            <a class="btn btn-default" href="<?php echo base_url();?>crt/crisisteam/managecrt">Back</a> 

                            </div>

                        </div>	

               		</div>

                

                	  <div class="col-lg-12" style="padding-top:10px;">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Crisis Team Member Details</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-update-crt" id="frm-update-crt" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                

                                 <div class="details_content">

                                <?php echo $crt_record[0]['crt_first_name']; ?>

                            </div>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <div class="details_content">

                                <?php echo $crt_record[0]['crt_last_name']; ?>

                            </div>

                            </div>

                        </div>

                      

                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Position</label>

                                            <div class="col-sm-10">

                                                <div class="details_content">

                                					<?php echo $crt_record[0]['crt_position']; ?>

                           					 </div>

                                            </div>

                                        </div>

                                        

                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Email Address</label>

                                            <div class="col-sm-10">

                                                 <div class="details_content">

                                					<?php echo $crt_record[0]['crt_email']; ?>

                           					 </div>

                                            </div>

                                        </div>

                                     

                                        <div class="form-group">

                                            <label class="col-sm-2 control-label">Mobile Number</label>

                                            <div class="col-sm-10">

                                                 <div class="details_content">

                                					<?php echo $crt_record[0]['crt_mobile']; ?>

                           					 </div>

                                            </div>

                                        </div>

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->





<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Crisis FLO | <?php echo $page_title; ?></title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->
    <link href="<?php echo base_url() ?>assets/css/plugins/pace/pace.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>assets/js/plugins/pace/pace.js"></script>

    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- THEME STYLES - Include these on every page. -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins.css" rel="stylesheet">

    <!-- THEME DEMO STYLES - Use these styles for reference if needed. Otherwise they can be deleted. -->
    <link href="<?php echo base_url() ?>assets/css/demo.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    
    
</head>
<body style="background: #34495e;">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                    <h1><i class="fa fa-gears"></i> Crisis FLO</h1>
                </div>
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h3><strong>Forget Password</strong>
                            </h3>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                    
                        <div id='login_form'>
                            <form action='' method='post' name="admin-login" id="admin-login">
                                
                              <br />
                               <?php if($this->session->flashdata('success')!=""){ ?>
                            <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
                            </div>
                            <?php } if($this->session->flashdata('error')!=""){ ?>
                            <div class="text-red">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>
                            <?php } ?>
                              
                           
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Email" type='text' name='user_name' id='user_name' />
                                    </div>
                                   
                                    <input  class="btn btn-lg btn-green btn-block" type='submit' value='Submit' 
                                    name="btn_forget" id="btn_forget" />	
                                </fieldset>
                                <br>
                                <p class="">
                                    <a href="<?php echo base_url().'cc' ?>">Back to Login</a>
                                </p>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
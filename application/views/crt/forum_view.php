<style>
.dataTables_filter, .dataTables_info ,.dataTables_length, .dataTables_paginate {display: none;}
</style>

<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->



<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Forum News</h1>

      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i> <a href="<?php base_url() ?>">Dashboard</a></li>
        <li class="active">Forum News</li>

      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 

<!-- end PAGE TITLE AREA -->



<div class="row">


	<?php if($this->session->flashdata('success')!="") { ?>
        <div class="col-lg-12">
            <div class="alert alert-success alert-dismissable">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
        </div>

	<?php }  if($this->session->flashdata('error')!="") { ?>
    
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
    
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
        
        </div>
     </div>

	<?php } ?>



      </div>


      <table class="table forum table-striped">
        <thead>
          <tr>
            <th>Topics</th>
            <th class="cell-stat text-center hidden-xs hidden-sm">Replies</th>
            <th class="cell-stat-2x text-center hidden-xs ">Author</th>
          </tr>
        </thead>
        <tbody>
        
        
		<?php if (count($forum_post) > 0){
            foreach ($forum_post as $p=>$psts){?>
				
          <tr>

            <td>
              <h4><a href="<?php echo base_url().'forum/post/'.$psts['scenario_id'].'/'.$psts['fp_id']; ?>"><?php echo $psts['fp_title'] ?></a><br><small><?php echo substr($psts['fp_slogan'],0, 30); ?></small></h4>
            </td>

            <td class="text-center hidden-xs hidden-sm"><span class="text-info">
            	<?php
					$forum_post = $this->master_model->getRecords('forum_post_master', array('post_parent'=>$psts['fp_id']));
					echo count($forum_post);
				?>
            
            
            </span>
            </td>
            
            <td class="hidden-xs text-center">
            	<span class="text-info">
                	<?php
						$auth = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$psts['fp_author_id']));
						echo $this->master_model->decryptIt($auth[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth[0]['crt_last_name']);
					?>
                </span>
            </td>
 
 

          </tr>
		<?php	
            }
        }
		else{?>
			<tr> <td colspan="3" >No recent topics.</td></tr>
		<?php }
		 ?>
                    
                    
        </tbody>
      </table>
      
      
      
    </div>

</div><!--.row-->


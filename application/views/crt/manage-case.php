                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

        <h1>Case Management</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'cc'?>">Dashboard</a>

        </li>

        <li class="active">Case Management</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->

				<div class="row">


                    <div class="col-lg-12" style="padding-top:10px;">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-life-ring"></i> Cases </h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            

                            <div class="panel-body">

								<?php 

								if(count($case_list)>0)

								{  ?>

							<div class="table-responsive">
							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>
                                        <th width="20%">Case ID</th>

                                        <th width="50%">Case Title</th>

                                        <th width="30%">Owner</th>

                                        <th width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
									foreach($case_list as $r => $value)

									{

								?>

                                                        

                                    <tr>
                                        <td><?php echo  $value['gen_id'];   ?></td>

                                    	<td><?php echo  $value['case_title']; ?></td>

                                    	<td><?php 
										
										$ownerr=$this->master_model->getRecords('cf_crisis_response_team',array('login_id'=>$value['crt_id']));
										echo $this->master_model->decryptIt($ownerr[0]['crt_first_name']).' '.$this->master_model->decryptIt($ownerr[0]['crt_last_name']); ?></td>


                                    <td>
       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href=""  data-toggle="modal" data-target="#myModal<?php echo  $value['id']; ?>">View</a>
            
            </li>
            <li>
                <a href="<?php echo base_url(); ?>crt/managecase/reportdoc/<?php echo  $value['id']; ?>">Create Report</a>
            
            </li>
          </ul>
        </div>    
                                    
                                    

                                    </td>

                                    </tr>
                                    
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Case Details</h4>
      </div>
      <div class="modal-body">
			<p>Case ID: <span class="text-muted"><?php echo  $value['gen_id']; ?></span></p>
			<p>Case Title: <span class="text-muted"><?php echo  $value['case_title']; ?></span></p>
			<p>Case Details: <span class="text-muted"><?php echo  $value['case_details']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                    

								<?php } ?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->


								<?php }
								  else{ ?>
                                        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-life-ring" style="font-size: 90px"></i></p>
										<p class="text-center" style="margin-top: 20px; color: #ccc">No cases</p>
								  <?php }?>







                            </div>

                        </div>

                        <!-- /.panel -->



                        
                        <!-- panel for reported cases-->
								<?php 
								if(count($reported_cases)>0){  
									foreach($reported_cases as $rr => $rcases) {
										
										//if shared to a group
										if($rcases['share_to_group'] !=''){
											
											$checkgroup = explode(' ',$rcases['share_to_group']);
											
											if (in_array($this->session->userdata('group_id'), $checkgroup)) {
												
											?>
                                                
                                                

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-book"></i> 
                                    
                                    <?php   
										$caseid = $rcases['case_id'];
										$thecase = $this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'id'=>$caseid));
									
										echo 'Case Report for: '.$thecase[0]['gen_id'].' - '.$thecase[0]['case_title'];
									?>
                                     </h4>


                                </div>
                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#panelexamples<?php echo $caseid; ?>"><i class="fa fa-chevron-down"></i></a>

                                </div>


                            <div class="clearfix"></div>

                            </div>

                            
                         <div id="panelexamples<?php echo $caseid; ?>" class="panel-collapse collapse in">
                            <div class="panel-body">
                            
                            
                            
								<div class="col-xs-12" style="height: 55px; border-bottom: 1px solid #eee; margin-bottom: 20px;">
                                <span class="pull-left" style="font-size: 22px;">
									
									<?php
									
									if( $rcases['cc_decision'] =='0'){
                                        echo '<span class="label label-default tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Coordinator Note: '.$rcases['cc_note'].'">Case Closed</span>';
                                    }
                                    
									else if( $rcases['cc_decision'] =='1'){
                                        echo '<span class="label label-default tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Coordinator Note: '.$rcases['cc_note'].'">Case Remidiated</span>';
                                    }
                                    
									else if( $rcases['cc_decision'] =='2'){
                                        echo '<span class="label label-default tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Coordinator Note: '.$rcases['cc_note'].'">Case Escalated</span>';
                                    }
									else{
                                        echo '<span class="label label-default">Awaiting Decision</span>';
									}
                                    
                                    
                                    
                                    ; ?>
                                    
                                </span>
                                
                                
                                <span class="pull-right" style="font-size: 22px;">
									
									<span class="label label-info">Shared</span>
                                    
                                </span>

                                 
                                </div>


                                <blockquote class="blockquote">
                                  <p>Hazard Analysis</p>
                                  <footer><?php echo $rcases['hazard']; ?></footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Root-Cause Analysis</p>
                                  <footer><?php echo $rcases['root_cause']; ?></footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Impact Analysis</p>
                                  <footer>
                                      <dt><em>Impact to Business operations</em></dt>
                                      <dd><?php echo $rcases['impact_to_bsn']; ?></dd>
                                      <dt><em>Impact to Stakeholders</em></dt>
                                      <dd><?php echo $rcases['impact_to_stk']; ?></dd>
                                  
                                  </footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Control</p>
                                  <footer><?php echo $rcases['control']; ?></footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Recommendation</p>
                                  <footer><?php echo $rcases['recommendation']; ?></footer>
                                </blockquote>

								<?php 
								
                                	$casedocss = $this->master_model->getRecords('case_document_report',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'case_id'=>$caseid));
								
								
                                    if(count($casedocss) > 0){ ?>
                                <p style="font-size: 16px; margin-left: 10px;">Evidence and Documents</p>
                                <div class="table-responsive">
    
                                <table class="table table-hover table-bordered table-green" id="example-table">
    
                                    <thead>
    
                                        <tr>
    
                                            <th width="50%">Document Name</th>
    
                                            <th width="50%">File Description</th>
    
                                            <th width="50px"> </th>
    
                                        </tr>
    
                                    </thead>
    
                                    <tbody>

                                    
                                    
									<?php
                                    
                                        foreach($casedocss as $casedocss){
                                    ?>
                                
                                
                                    <tr>

                                        <td>
                                        
                                        <?php
                                        $fileext = explode ('.',$casedocss['document']);
                                        if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
                                        
                                        echo '<i class="fa fa-file-pdf-o text-red"></i> ';
                                        
                                        }
                                        
                                        else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
                                        
                                        echo '<i class="fa fa-file-word-o text-blue"></i> ';
                                        
                                        }
                                        
                                        else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
                                        
                                        echo '<i class="fa fa-file-text-o text-muted"></i> ';
                                        
                                        }

                                        echo substr($casedocss['document'],13,50); ?>
                                        </td>
                                        <td>
                                        <?php echo $casedocss['file_desc']; ?>
                                        </td>
                                        <td>
                                        <a href="<?php echo base_url().'crt/document/download/'. $casedocss['document']; ?>" class="btn btn-sm btn-default">Download</a>
                                        </td>
                                    </tr>
                                

                                    <?php 
    
                                        }
                                    ?>
    
                                                    </tbody>
    
                                                </table>
    
                                            </div>
    
                                            <!-- /.table-responsive -->
    
                                    <?php 
                                    }
                                    ?>




                            </div>
                          </div>
                        </div>                                                
                                                
                                                
                                                
                                                
                                            <?php
											}
											
										}
										
										
										//if shared to particular crt member
										else if($rcases['share_to'] !=''){
											
											$checkcrts = explode(' ',$rcases['share_to']);
											
											if (in_array($this->session->userdata('logged_crt_login_id'), $checkcrts)) {

								?>
								
                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-book"></i> 
                                    
                                    <?php   
										$caseid = $rcases['case_id'];
										$thecase = $this->master_model->getRecords('case_master',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'id'=>$caseid));
									
										echo 'Case Report for: '.$thecase[0]['gen_id'].' - '.$thecase[0]['case_title'];
									?>
                                     </h4>


                                </div>
                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#panelexamples<?php echo $caseid; ?>"><i class="fa fa-chevron-down"></i></a>

                                </div>


                            <div class="clearfix"></div>

                            </div>

                            
                         <div id="panelexamples<?php echo $caseid; ?>" class="panel-collapse collapse in">
                            <div class="panel-body">
                            
                            
                            
								<div class="col-xs-12" style="height: 55px; border-bottom: 1px solid #eee; margin-bottom: 20px;">
                                <span class="pull-left" style="font-size: 22px;">
									
									<?php
									
									if( $rcases['cc_decision'] =='0'){
                                        echo '<span class="label label-default tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Coordinator Note: '.$rcases['cc_note'].'">Case Closed</span>';
                                    }
                                    
									else if( $rcases['cc_decision'] =='1'){
                                        echo '<span class="label label-default tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Coordinator Note: '.$rcases['cc_note'].'">Case Remidiated</span>';
                                    }
                                    
									else if( $rcases['cc_decision'] =='2'){
                                        echo '<span class="label label-default tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Coordinator Note: '.$rcases['cc_note'].'">Case Escalated</span>';
                                    }
									else{
                                        echo '<span class="label label-default">Awaiting Decision</span>';
									}
                                    
                                    
                                    
                                    ; ?>
                                    
                                </span>
                                
                                
                                <span class="pull-right" style="font-size: 22px;">
									
									<span class="label label-info">Shared</span>
                                    
                                </span>

                                 
                                </div>


                                <blockquote class="blockquote">
                                  <p>Hazard Analysis</p>
                                  <footer><?php echo $rcases['hazard']; ?></footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Root-Cause Analysis</p>
                                  <footer><?php echo $rcases['root_cause']; ?></footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Impact Analysis</p>
                                  <footer>
                                      <dt><em>Impact to Business operations</em></dt>
                                      <dd><?php echo $rcases['impact_to_bsn']; ?></dd>
                                      <dt><em>Impact to Stakeholders</em></dt>
                                      <dd><?php echo $rcases['impact_to_stk']; ?></dd>
                                  
                                  </footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Control</p>
                                  <footer><?php echo $rcases['control']; ?></footer>
                                </blockquote>

                                <blockquote class="blockquote">
                                  <p>Recommendation</p>
                                  <footer><?php echo $rcases['recommendation']; ?></footer>
                                </blockquote>

								<?php 
								
                                	$casedocss = $this->master_model->getRecords('case_document_report',array('cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'case_id'=>$caseid));
								
								
                                    if(count($casedocss) > 0){ ?>
                                <p style="font-size: 16px; margin-left: 10px;">Evidence and Documents</p>
                                <div class="table-responsive">
    
                                <table class="table table-hover table-bordered table-green" id="example-table">
    
                                    <thead>
    
                                        <tr>
    
                                            <th width="50%">Document Name</th>
    
                                            <th width="50%">File Description</th>
    
                                            <th width="50px"> </th>
    
                                        </tr>
    
                                    </thead>
    
                                    <tbody>

                                    
                                    
									<?php
                                    
                                        foreach($casedocss as $casedocss){
                                    ?>
                                
                                
                                    <tr>

                                        <td>
                                        
                                        <?php
                                        $fileext = explode ('.',$casedocss['document']);
                                        if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
                                        
                                        echo '<i class="fa fa-file-pdf-o text-red"></i> ';
                                        
                                        }
                                        
                                        else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
                                        
                                        echo '<i class="fa fa-file-word-o text-blue"></i> ';
                                        
                                        }
                                        
                                        else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
                                        
                                        echo '<i class="fa fa-file-text-o text-muted"></i> ';
                                        
                                        }

                                        echo substr($casedocss['document'],13,50); ?>
                                        </td>
                                        <td>
                                        <?php echo $casedocss['file_desc']; ?>
                                        </td>
                                        <td>
                                        <a href="<?php echo base_url().'crt/document/download/'. $casedocss['document']; ?>" class="btn btn-sm btn-default">Download</a>
                                        </td>
                                    </tr>
                                

                                    <?php 
    
                                        }
                                    ?>
    
                                                    </tbody>
    
                                                </table>
    
                                            </div>
    
                                            <!-- /.table-responsive -->
    
                                    <?php 
                                    }
                                    ?>




                            </div>
                          </div>
                        </div>
                                    <?php }
										}
									  }
                                    }
								  
								?>

                        <!-- /.panel -->



                    </div>



                </div><!--.row -->

					




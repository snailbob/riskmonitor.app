                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<style>
.dataTables_filter/*, .dataTables_info */,.dataTables_length{display: none;}
</style>



<!-- Modal -->
<div class="modal fade" id="viewAccumulative" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Accumulative Cost</h4>
      </div>
      <div class="modal-body">
        <div class="well" style="border-radius: 0px">
			<?php if(count($my_recall)> 0){
				echo $recall_accumulate;
			} ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewCostItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Item Cost Details</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myRecalls" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 id="myModalLabel">Select Recall Incident</h4>
        <table class="table table-hover">
        <?php
			if(count($recall)> 0){
				foreach($recall as $r=>$call){ ?>
                
                  <tr>
                  	<td>
                    	<a href="<?php echo base_url().'crt/costmonitor/index/'.$call['id']; ?>"><?php echo $call['incident_no'] ?> : <?php echo $call['incident_name'] ?></a>
                    </td>
                  </tr>
			<?php }
			}else{
				echo '<tr><td class="text-center text-muted">No recall incidents.</td> </tr>';
			}
			
		?>
                </table>					


      </div>
      <div class="modal-footer hidden">
        <a class="btn btn-default" href="<?php echo base_url(); ?>"><i class="fa fa-angle-double-left"></i> back to dashboard</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewCategorySummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cost Category Summary</h4>
      </div>
      <div class="modal-body">
        <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>



<div class="row">

  <div class="col-lg-12">

    <div class="page-title">

      <h1>Cost Monitor</h1>
      <ol class="breadcrumb">

        <li><i class="fa fa-dashboard"></i>

        <a href="<?php echo base_url().'crt'?>">Dashboard</a>

        </li>

        <li class="active">Cost Monitor</li>


      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 

</div>

<!-- /.row --> 



                <!-- end PAGE TITLE AREA -->





                

                <div class="row">
                    <div class="col-lg-12" style="height:40px;">
                    	
                        <?php if(count($my_recall)> 0){ ?>

                        <a class="btn btn-primary pull-right hidden-xs" href="#" data-toggle="modal" data-target="#viewAccumulative" style="width: 150px;">Running Cost</a> 

                        <a class="btn btn-primary pull-right visible-xs btn-block" href="#" data-toggle="modal" data-target="#viewAccumulative">Running Cost</a> 

                        <?php } ?>

                    </div>

 					<div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>
                        
                        
<?php 
						
	if (count($recall) > 0){
	?>
    
    
    
                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Cost Category
                                    
										<?php if($this->uri->segment(4) != ''){
											
											echo 'for '.$my_recall[0]['incident_no'].': '.$my_recall[0]['incident_name'];
											
												if ($my_recall[0]['initiation_type'] == 0){
													
													echo ' <span class="label label-default">Mock</span>';
												
												}
												
												else{
												
													echo ' <span class="label label-info">Live</span>';
													
												}
											
											}?>
                                        
                                        
                                    </h4>

                                </div>
                                
                                <div class="panel-widgets">
                                    <span class="text-info">
                                    <?php
                                        $more_inci = count($recall) - 1;
                                        if (count($recall) == 1){
                                            echo '1 current incident ';
                                        }
                                        if (count($recall) > 1){
                                            echo count($recall).' current incidents ';
                                        }
                                        //echo $this->pagination->create_links(); 
                                    ?></span>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#open_incident" style="text-decoration: none;">
                                    <i class="fa fa-chevron-up tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php // echo 'View '.$more_inci.' more incidents.';?>"></i></a>
                
                                </div>
                                
                                <div class="clearfix"></div>

                            </div>


                            <div class="panel-body">

                                <div id="open_incident" class="panel-collapse collapse">
                
                                    <ul class="list-group list-default">
                                        <?php if(count($recall) > 0){
                                            $count_open = 0;
                                            $uri_4 = $this->uri->segment(4);
                                            foreach($recall as $open=>$opeen){
                                                
                                                if ($uri_4 != $opeen['id']) {
                                        ?>
                                        
                                        <a href="<?php echo base_url().'crt/costmonitor/index/'.$opeen['id']?>" class="list-group-item"><?php echo $opeen['incident_no'].': '.$opeen['incident_name'] ?></a>
                                        
                                        <?php
                                                }
                                            $count_open++;	
                                            } 
                                        } ?>
                                    </ul>
                                </div>
            

								<?php 

								if(count($cost_category)>0)

								{ ?>

							<div class="">

							<table class="table" id="example-tablexx">

								<tbody>

								<?php
								
									foreach($cost_category as $r => $value)

									{
										
								?>

                                                        

                                    <tr class="bg-info">

                                    	<td width="100%">
                                        <h4 style="text-transform: uppercase; font-weight: bold"><?php echo  $value['name']; ?></h4>
             
                                        
                                        </td>

                                        <td class="pull-right">
                                    
                                    
       <!-- Single button -->
        <div class="btn-group" style="margin-top: 5px;">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; left: auto; right: 0;">
            <li>
                <a href="#" data-toggle="modal" data-target="#viewCategorySummary" onclick="showCostSummary('<?php echo $value['cost_id'] ?>')">View Category Sub-Total</a>
            </li>
          </ul>
        </div>                                        
                                    
                                    
										
                                        </td>

                                    </tr>

									<tr>
                                    	<td colspan="2">
                                        
                                        
                           
								<?php
                                
                                $items = $this->master_model->getRecords('cf_cost_category_item',array('cost_id'=>$value['cost_id']));
                                ?>
                                        
            

								<?php 

								if(count($items)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover table-bordered table-datatable">

                                <thead>

                                    <tr>

                                        <th width="50%">Item Name</th>

                                        <th width="50%">Item Cost</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php
								
									foreach($items as $r => $value)

									{
										
								?>

                                                        

                                    <tr>

                                    	<td>
										
										<?php
										
										$files = $this->master_model->getRecords('private_message_file',array('cost_item_id'=>$value['item_id']));
										if(count($files) > 0){
											echo '<i class="fa fa-paperclip text-info fa-fw"></i> ';
										}else{
											echo '<i class="fa fa-hand-o-right text-info fa-fw" style="visibility:hidden"></i> ';
										}
										echo  $value['item_name']; ?>
                                        
                                        </td>

                                    	<td><?php echo  $my_class->currencycode($value['currency']).' '.number_format($value['item_cost'],2); ?></td>

                                        <td>
                                    
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px; right: 0; left: auto;">

            <li class="">
                <a href="#" data-toggle="modal" data-target="#viewCostItem" onclick="viewCostItemDetails('<?php echo $value['item_id']?>')">View Cost Detail</a>
            </li>
            
		<?php if(count($files) > 0){ ?>
        
            <li>
                <a href="<?php echo base_url().'uploads/costitems-uploads/'.$files[0]['file_upload_name']?>" target="_blank"> Download Document</a>
            </li>
            
		<?php } ?>
          </ul>
        </div>    


                                    </td>

                                    </tr>



								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-list-ul" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost items for <?php echo $value['name']?></p>
								<?php }

								?>

                                                                                
                                        
                                        
                                        
                                        
                                        </td>
                                    </tr>

								<?php 

								}//end foreach loop
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-usd" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No cost category</p>
								<?php }

								?>



                            </div>

                        </div>

                        <!-- /.panel -->




    
	<?php
    } else{
	?>
    
        <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-history" style="font-size: 90px"></i></p>
        <p class="text-center" style="color: #ccc; margin-top: 20px;">No Recall</p>
	<?php	
	}
?>
    





                    </div>



                </div><!--.row -->

					




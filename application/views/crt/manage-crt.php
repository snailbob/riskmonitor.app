                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Crisis Response Team Members</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'crt'?>">Dashboard</a>

                                </li>

                                <li class="active">CRT Members</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->

					<div class="row">

 					<div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-user fa-fw"></i>Crisis Response Team Members

</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                             <?php if(! is_null($error)) 

										echo $error;

										if(! is_null($success)) 

										$success;

								?>	

                                

                                 <?php if($success!="") { ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $success;   ?>

                            </div>

                            <?php } if($error!="") { ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $error; ?></div>

                            <?php } ?>

                            <div class="panel-body">


								<?php 

								if(count($all_crt)>0)

								{ ?>
							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="25%">First Name</th>

                                        <th width="25%">Last Name</th>

                                        <th width="25%">Email</th>

                                        <!--<th>Status</th>-->

                                        <th width="25%">Mobile Number</th>

                                        <th width="50px"> </th>

                                      

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($all_crt as $r => $value) {
										$active_module = $this->session->userdata('org_module');
										if (strpos($value['module'], substr($active_module, 0, 1)) !== false){

								?>

                                                        

                                    <tr>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_email']); ?></td>

                                    	<?php 

                                    if($value['user_status'] == 1 ){

                                    $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">CRT Accepted</span>';

                                    } else {	

                                    $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Awaiting CRT Acceptance</span>';

                                    }

                                    ?>

                                    <!--<td><?php //echo  $status ?></td>-->

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_mobile']);?></td>

                                     	<td>
                                           <!-- Single button -->
                                            <div class="btn-group">
                                         <a class="btn btn-default btn-sm" href="#"  data-toggle="modal" data-target="#myModal<?php echo  $value['login_id'];?>">View</a>
                                            </div>    
                                            
                                        
                                        
                                       <?php /*?>  <a class="btn btn-orange btn-xs" href="#<?php echo base_url(); ?>crt/crisisteam/details/<?php echo  $value['login_id'];  ?>"  data-toggle="modal" data-target="#myModal<?php echo  $value['login_id'];?>">View</a><?php */?>
                                         
                                         </td>

                                    </tr>

<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CRT Member Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></span></p>
			<p>Position: <span class="text-muted"><?php echo  $value['crt_position']; ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_mobile']); ?></span></p>
			<p>Crisis Function: <span class="text-muted"><?php echo  $value['crisis_function']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>


								<?php 
										}
									}
									
								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-user" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No response team members</p>
								<?php }

								?>








                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Crisis Documents</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i>

                                <a href="<?php echo base_url().'crt'?>">Dashboard</a>

                                </li>

                                <li class="active">Crisis Documents</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">

                

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">



                            <div class="panel-heading">

                                <div class="panel-title">
                                    <h4><i class="fa fa-paperclip fa-fw"></i>Crisis Documents</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                             <?php if(! is_null($error)) 

										echo $error;

										if(! is_null($success)) 

										echo $success;

								?>	

                            <div class="panel-body">


								<?php 

								if(count($all_doc)>0)

								{ ?>
                                
							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="50%">Document Name</th>

                                        <th width="50%">Shared to</th>

                                        <th width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($all_doc as $r => $value)

									{
									if ($value['group_id'] == $this->session->userdata['group_id'] || $value['group_id'] == '0' ){
								?>
									
                                                        




                                    <tr>

                                    	<td>
										<?php
										
										 if ($value['current_file_name'] != ''){
										 
											$fileext = explode ('.',$value['current_file_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
											echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
											echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
											echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											 echo substr($value['current_file_name'],13,50);
											 if ($value['rights_to_group'] == $this->session->userdata('group_id') || $value['rights_to_indi'] == $this->session->userdata('logged_crt_login_id')){
												 
											$updated_doc = $this->master_model->getRecords('cf_file_history',array('cf_file_id'=>$value['file_upload_id']));
											
												 echo '<a href="#"  data-toggle="modal" data-target="#myModal'.$value['file_upload_id'].'" style="text-decoration: none;"> <i class="fa fa-info-circle tooltip-test text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="You\'re allowed to edit this file. Edited '.count($updated_doc).' time(s)"></i></a>';
										 ?>
                                         
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $value['file_upload_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">File Update History</h4>
      </div>
      <div class="modal-body">
			....
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

                                         <?php
											 } 										 
										 
										 }
										 else{
	
											$fileext = explode ('.',$value['file_upload_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
											echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
											echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
											echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											 echo substr($value['file_upload_name'],13,50);
											 if ($value['rights_to_group'] == $this->session->userdata('group_id') || $value['rights_to_indi'] == $this->session->userdata('logged_crt_login_id')){
												 echo ' <i class="fa fa-info-circle tooltip-test text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="You\'re allowed to edit this doc"></i>';
												
											 }
										 }
										?>
                                         
                                         
                                         </td>
                                    	<td>
										<?php
												if ($value['group_id'] == '0'){
													echo 'All';
												}
												else{
													$user_group = $this->master_model->getRecords('user_group',array('id'=>$value['group_id']));
													echo $user_group[0]['group_name'];
												}
                                        
                                        ?>
                                         </td>

                                    	<td>

                                    		<!--<a href="javascript:return void(0);" onclick="return del_confirm('<?php echo base_url(); ?>webmanager/employee/delete','<?php echo $emp['login_id']; ?>');" title="Delete"><img src="<?php echo base_url();?>images/delete.png" height="23" width="23" /></a>-->
                                            
                                            
                             
       <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>crt/document/download/<?php echo $value['file_upload_name'];?>">Download</a>
            </li>
          	<?php if ($value['rights_to_group'] == $this->session->userdata('group_id') || $value['rights_to_indi'] == $this->session->userdata('logged_crt_login_id')) {?>
            <li>
            <a href="<?php echo base_url();?>crt/document/update/<?php echo $value['file_upload_id'];?>"> Edit</a>
            </li>
            <?php } ?>
          </ul>
        </div>    
                                            

                                            
                                           <!-- Single button -->
                                            <?php /*?><div class="btn-group">
                                            <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>crt/document/download/<?php echo $value['file_upload_name'];?>">Download</a>
                                            </div> <?php */?>   
                                            

                                           <?php /*?> <a class="btn btn-orange btn-xs" href="<?php echo base_url(); ?>crt/document/download/<?php echo $value['file_upload_name'];?>">Download</a>
<?php */?>
                                    	</td>

                                    </tr>

								<?php 
										}
									}
								?>

                                                </tbody>

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 
								}
								else{ ?>
								
								<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-file-o" style="font-size: 90px"></i></p>
								<p class="text-center" style="color: #ccc; margin-top: 20px;">No crisis documents</p>
								<?php }

								?>








                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




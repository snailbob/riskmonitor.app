<!-- begin PAGE TITLE AREA -->
<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
<div class="row">
  <div class="col-lg-12">
    <div class="page-title">
      <h1>Crisis Response Plan</h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Manage Task</li>
      </ol>
    </div>
  </div>
  <!-- /.col-lg-12 --> 
</div>
<!-- /.row --> 
<!-- end PAGE TITLE AREA -->
<div class="row">
<?php 
$main_status=$this->common_model->get_main_status($this->session->userdata('logged_parent_crt'),$this->session->userdata('crt_selected_orgnaization'));

	
?>
<div class="col-lg-12">

	<?php 

	if($this->session->flashdata('success')!="")

	{

	?>

	<div class="alert alert-success alert-dismissable">

	<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

	<strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

	<?php    

	} 

	if($this->session->flashdata('error')!="")

	{

	?>

	<div class="text-red">

	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

	<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

	<?php

	} 

	?>

  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="panel-title">
        <h4>My Tasks</h4>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      
      <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="home">
          <?php if(count($my_task)>0){?>
          <div class="table-responsive">
            <table  class="table table-striped table-bordered table-hover table-green myexample-table" >
              <thead>
                <tr>
                  <th width="43%">Scenario</th>
                  <th width="43%">Task</th>
                  <!--<th>Assign To</th>-->
                  <th width="16%">Status</th>
                  <th width="50px"> </th>
                </tr>
              </thead>
              <tbody>
                <?php 
				if(count($my_task)>0)
				{
					foreach($my_task as $mtask)
					{
				?>
               	 	<tr>
                    	<td><?php echo $mtask['scenario']; ?></td>
                        <td><?php echo $mtask['task']; ?></td>
                        <!--<td><?php echo $this->master_model->decryptIt($mtask['crt_first_name']).' '.$this->master_model->decryptIt($mtask['crt_last_name']); ?></td>-->
                        <td>
                         <?php if($mtask['task_status']=='2'){?>
                        <!--<a class="btn btn-default btn-xs disabled" href="javascript:void(0);">--><span class="text-green">Completed</span><!--</a>-->
                         <?php }if($mtask['task_status']=='0'){?>
                        <!--<a class="btn btn-default btn-xs disabled" href="javascript:void(0);">--><span class="text-red">Pre-Incident</span><!--</a>-->
                        <?php }if($mtask['task_status']=='1'){ ?>
                       <!--<a class="btn btn-default btn-xs disabled" href="javascript:;" onclick="return complete_confirm('<?php echo base_url().'crt/scenario/changestatus/'.$mtask['task_id'];?>')" >--><span class="text-red">In Progress</span><!--</a>-->  
                        <?php } ?> 
                        </td>
                        <td>
       <!-- Single button -->
        <div class="btn-group">
                        <a class="btn btn-default btn-sm" href="#<?php /*echo base_url().'crt/scenario/details/'.$mtask['task_id'];*/?>" data-toggle="modal" data-target="#myModal<?php echo $mtask['task_id'];?>">View</a>
        </div>    
                        <?php /*?><a class="btn btn-orange btn-xs" href="#<?php echo base_url().'crt/scenario/details/'.$mtask['task_id'];?>" data-toggle="modal" data-target="#myModal<?php echo $mtask['task_id'];?>">View</a></td><?php */?>
                         </td>
                    </tr>
                    	
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo  $mtask['task_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">My Task Details</h4>
      </div>
      <div class="modal-body">
			<p>Scenario: <span class="text-muted"><?php echo $mtask['scenario'];?></span></p>
			<p>Task: <span class="text-muted"><?php echo $mtask['task']; ?></span></p>
			<p>Assigned To: <span class="text-muted"><?php echo $this->master_model->decryptIt($mtask['crt_first_name']).' '.$this->master_model->decryptIt($mtask['crt_last_name']); ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                    
                    
                <?php 
					}
				}
				?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
          <?php }
		  else{ ?>
                <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
				<p class="text-center" style="margin-top: 20px; color: #ccc">No tasks assigned</p>
		  <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- CRT's Team Task -->
<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="panel-title">
        <h4>Team Tasks</h4>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      
      <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="home">
          <?php if(count($team_task)>0){?>
          <div class="table-responsive">
            <table  class="table table-striped table-bordered table-hover table-green myexample-table">
              <thead>
                <tr>
                  <th width="41%">Scenario</th>
                  <th width="41%">Task</th>
                  <th width="18%">Owner</th>
                  <th width="50px"> </th>
                </tr>
              </thead>
              <tbody>
                <?php 
				if(count($team_task)>0)
				{  
					foreach($team_task as $team_task)
					{
				?>
               	 	<tr>
                    	<td><?php echo $team_task['scenario']; ?></td>
                        <td><?php echo $team_task['task']; ?></td>
                        <td><?php echo $this->master_model->decryptIt($team_task['crt_first_name']).' '.$this->master_model->decryptIt($team_task['crt_last_name']); ?></td>
                   		<td>
                       <!-- Single button -->
                        <div class="btn-group">
                        	<a class="btn btn-default btn-sm" href="#<?php /*echo base_url().'crt/scenario/details/'.$team_task['task_id'];*/?>" data-toggle="modal" data-target="#myTeamModal<?php echo $team_task['task_id'];?>">View</a>
                        </div>    
                        
                        	<?php /*?><a class="btn btn-orange btn-xs" href="#<?php echo base_url().'crt/scenario/details/'.$team_task['task_id'];?>" data-toggle="modal" data-target="#myTeamModal<?php echo $team_task['task_id'];?>">View</a><?php */?>
                            </td>
                    </tr>
                    
<!-- Modal -->
<div class="modal fade" id="myTeamModal<?php echo $team_task['task_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">My Team Task Details</h4>
      </div>
      <div class="modal-body">
			<p>Scenario: <span class="text-muted"><?php echo $team_task['scenario'];?></span></p>
			<p>Task: <span class="text-muted"><?php echo $team_task['task']; ?></span></p>
			<p>Assigned To: <span class="text-muted"><?php echo $this->master_model->decryptIt($team_task['crt_first_name']).' '.$this->master_model->decryptIt($team_task['crt_last_name']); ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                    
                <?php 
					}
				}
				?>
                
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
          <?php } 
		  else{ ?>
                <p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-tasks" style="font-size: 90px"></i></p>
				<p class="text-center" style="margin-top: 20px; color: #ccc">No team tasks assigned</p>
		  <?php }?>
        </div>
      </div>
    </div>
    <!-- /.panel-body --> 
  </div>
  <!-- /.panel --> 
  
</div>
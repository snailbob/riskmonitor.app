<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

    <div class="row">

        <div class="col-lg-12">

            <div class="page-title">

                <h1>Subject:  <?php echo $msg_details[0]['subject']; ?></h1>

                <ol class="breadcrumb">

                    <li><i class="fa fa-dashboard"></i>

                    <a href="<?php echo base_url().'cc'?>">Dashboard</a>

                    </li>

                    <li class="active">Messages</li>

            

                </ol>

            </div>

        </div>

        <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

<!-- end PAGE TITLE AREA -->

<div class="row">

	<div class="col-lg-12">

		<?php 

        if($this->session->flashdata('success')!="")

        {

        ?>

        <div class="alert alert-success alert-dismissable">

        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

        <?php    

        } 

        if($this->session->flashdata('error')!="")

        {

        ?>

        <div class="text-red">

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

        <?php

        } 

        ?>
<?php /*?>
		<div class="panel panel-default">

			<div class="panel-heading">

				<div class="panel-title">

					<h4><i class="fa fa-inbox"></i> Inbox</h4>

				</div>

                <div class="clearfix"></div>

			</div>

			<div class="panel-body">

                <ul id="myTab" class="nav nav-tabs">

                    <li <?php if($this->uri->segment(3)=="inbox" || $this->uri->segment(5)=="inbox"){echo 'class="active"';} ?>>

                    	<a href="<?php echo base_url().'crt/message/inbox' ?>">Inbox</a>

					</li>

                    <li <?php if($this->uri->segment(3)=="outbox" || $this->uri->segment(5)=="outbox"){echo 'class="active"';} ?>>

                    	<a href="<?php echo base_url().'crt/message/outbox' ?>">Outbox</a>

					</li>

                    <li <?php if($this->uri->segment(3)=="trash"){echo 'class="active"';} ?>>

                    	<a href="<?php echo base_url().'crt/message/trash' ?>">Trash</a>

					</li>

                </ul>

                            

                            	<div style="padding-top: 25px; margin-bottom: 35px;">

                                	<div class="pull-left col-lg-6">Date : <?php echo date('d M Y',strtotime($msg_details[0]['send_date'])); ?></div>
									<div class="pull-right  col-sm-6"><a href="<?php echo base_url() ?>crt/message/compose/<?php echo base64_encode($msg_details[0]['sender_id']); ?>" style="text-decoration: none;" class="pull-right"><i class="fa fa-edit"></i> Reply</a></div>
                            	</div>



                <?php if($this->uri->segment(5)=="inbox"){ ?>


					


                <?php } ?>

                <div id="mailbox">

                    <form action='' name="message-details" id="message-details" method='post' class="form-horizontal" >

						<?php 

						if($this->session->userdata('logged_cc_login_id')==$msg_details[0]['receiver_id'])

						{

						?>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">From</label>

                            <div class="col-sm-10">

                                 <div class="details_content details_content">

                                 <?php echo $my_class->getcrtname($msg_details[0]['sender_id']); ?>

                            </div>							

                           	</div>

                        </div>

                       	<?php

						}

						else

						{

						?>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">To</label>

                            <div class="col-sm-10">

                                 <div class="details_content details_content">

                                 <?php echo $my_class->getcrtname($msg_details[0]['receiver_id']); ?>

                            </div>							

                           	</div>

                        </div>

                        <?php

						}

                        ?>

                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Subject</label>

                            <div class="col-sm-10">

                                 <div class="details_content details_content">

                                 <?php echo $msg_details[0]['subject']; ?>

                            </div>							

                           	</div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Message</label>

                            <div class="col-sm-10">

                                <div class="details_content details_content">

                                 <?php echo $msg_details[0]['message']; ?>

                            </div>					

                           	</div>

                        </div>

					</form>

                </div>

                

			</div>

		</div><?php */?>


        <div class="row">
            <div class="col-sm-3 col-md-2">
                <div class="btn-group hidden">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Mail <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Mail</a></li>
                        <li><a href="#">Contacts</a></li>
                        <li><a href="#">Tasks</a></li>
                    </ul>
                </div>
            </div>
            
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-3 col-md-2" style="margin-bottom: 15px;">
                <a href="<?php echo base_url().'crt/message/compose' ?>" class="btn btn-danger btn-sm btn-block" role="button">COMPOSE</a>
                <hr />
                <ul class="nav nav-pills nav-stacked">
                    <li <?php if($this->uri->segment(5) == 'inbox'){ echo 'class="active"'; } ?>><a href="<?php echo base_url().'crt/message/inbox' ?>">
                    
                    
                     <?php 
						$unread=$this->common_model->get_unread_count($this->session->userdata('logged_crt_login_id'));
						
						if($unread > 0){echo '<span class="badge danger pull-right">'.$unread.'</span>';} ?>
                    
                    
                     Inbox </a>
                    </li>
                    <?php /*?><li><a href="#">Starred</a></li>
                    <li><a href="#">Important</a></li><?php */?>
                    <li <?php if($this->uri->segment(5) == 'outbox'){ echo 'class="active"'; } ?>><a href="<?php echo base_url().'crt/message/sentmail' ?>">Sent Mail</a></li>
                    <li><a href="<?php echo base_url().'crt/message/trash' ?>">Trash</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
               <div class="col-xs-6">
               		<p><b><?php echo $my_class->getcrtname($msg_details[0]['sender_id']); ?></b><br />
                    	<span class="small text-muted">Sent to: <?php echo $my_class->getcrtname($msg_details[0]['receiver_id']); ?></span>
                    </p>
               </div>
               <div class="col-xs-6">
               		<p class="pull-right">
					  <?php echo date('d M Y',strtotime($msg_details[0]['send_date'])); ?>
                      <a href="<?php echo base_url() ?>crt/message/compose/<?php echo base64_encode($msg_details[0]['sender_id']); ?>" class="btn btn-sm btn-default"><i class="fa fa-reply"></i> Reply</a> 
                    </p>
               </div>
               
               
               <div class="col-xs-12" style="border-top: 1px solid #eee;">
               		<div style="padding: 20px;">
               			<?php echo $msg_details[0]['message']; ?>
                    </div>
               </div>
               
               

               <?php
			   		$message_id = $this->uri->segment(4);
					$files = $this->master_model->getRecords('private_message_file',array('message_id'=>$message_id));
					if (count($files) > 0){ ?>
                    
               <div class="col-xs-12" style="border-top: 1px solid #eee;">
               		<div style="padding: 20px;">
                        <div class="row"><!--row of attachments-->
               <?php
						foreach($files as $r=>$file){
			   ?>


                    
                        
                        
                            <div class="col-md-3 col-sm-4 col-xs-6" style="margin-top: 15px;">
                                <div class="cuadro_intro_hover" style="background-color:#cccccc;">
                                    <p style="text-align:center; margin-top:20px;">
                                       
                                       
                                       	<?php 
										
											if ($my_class->filename_extension($file['file_upload_name']) == 'pdf') { 
											
												echo '<i class="fa fa-file-pdf-o fa-3x text-danger"></i>';
											}
											else if ($my_class->filename_extension($file['file_upload_name']) == 'txt') { 
												echo '<i class="fa fa-file-word-o fa-3x text-info"></i>';
											
											}
											else if ($my_class->filename_extension($file['file_upload_name']) == 'zip' || $my_class->filename_extension($file['file_upload_name']) == 'rar' ) { 
												echo '<i class="fa fa-file-zip-o fa-3x text-muted"></i>';
											
											}
											else if ($my_class->filename_extension($file['file_upload_name']) == 'doc' || $my_class->filename_extension($file['file_upload_name']) == 'docx' ) { 
												echo '<i class="fa fa-file-word-o fa-3x text-info"></i>';
											
											}
											else if ($my_class->filename_extension($file['file_upload_name']) == 'jpg' ||
												$my_class->filename_extension($file['file_upload_name']) == 'gif' ||
												$my_class->filename_extension($file['file_upload_name']) == 'png' ||
												$my_class->filename_extension($file['file_upload_name']) == 'jpeg'
												) { 
												echo '<i class="fa fa-file-image-o fa-3x text-info"></i>';
												//echo '<img src="'.base_url().'uploads/message-uploads/'.$file['file_upload_name'].'" class="img-responsive" style="margin: 0 auto; margin-top: -20px;"/>';
											}
											else { 
												echo '<i class="fa fa-file-o fa-3x text-info"></i>';
											
											}
											
										?>
                                    </p>
                                    <div class="caption">
                                        <div class="blur"></div>
                                        <div class="caption-text">
                                            <p style="margin-top: 20px; border-bottom: 1px solid #eee; padding:10px;" title="<?php echo substr($file['file_upload_name'],13,27) ?>"><?php if (strlen($file['file_upload_name']) > 27){ echo substr($file['file_upload_name'],13,13).'.'.$my_class->filename_extension($file['file_upload_name']);} else { echo substr($file['file_upload_name'],13,27); } ?></p>
                                             
                                            <a class="btn btn-default btn-xs tooltip-test" href="<?php echo base_url().'crt/message/download/'.$file['file_upload_name']; ?>" data-toggle="tooltip" data-placement="top" title="Download"><span class="fa fa-arrow-down"></span></a>
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                            
                            

               
               
               <?php
						} ?>
                        
                        </div><!--.row of attachments-->
                    </div>
               </div>
				<?php
						
					}
				?>
                              
               
               
               
            </div>
        </div>
        
        
        
	</div><!--.col-lg-12 -->



</div><!--.row -->

					



<style>

.cuadro_intro_hover{
    	padding: 0px;
		position: relative;
		overflow: hidden;
		height: 110px;
	}
	.cuadro_intro_hover:hover .caption{
		opacity: 1;
		transform: translateY(-70px);
		-webkit-transform:translateY(-70px);
		-moz-transform:translateY(-70px);
		-ms-transform:translateY(-70px);
		-o-transform:translateY(-70px);
	}
	.cuadro_intro_hover img{
		z-index: 4;
	}
	.cuadro_intro_hover .caption{
		position: absolute;
		top:70px;
		-webkit-transition:all 0.3s ease-in-out;
		-moz-transition:all 0.3s ease-in-out;
		-o-transition:all 0.3s ease-in-out;
		-ms-transition:all 0.3s ease-in-out;
		transition:all 0.3s ease-in-out;
		width: 100%;
	}
	.cuadro_intro_hover .blur{
		background-color: rgba(0,0,0,0.7);
		height: 300px;
		z-index: 5;
		position: absolute;
		width: 100%;
	}
	.cuadro_intro_hover .caption-text{
		z-index: 10;
		color: #fff;
		position: absolute;
		height: 300px;
		text-align: center;
		top:-20px;
		width: 100%;
	}
</style>


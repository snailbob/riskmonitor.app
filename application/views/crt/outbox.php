<?php /*?><script language="javascript" type="text/javascript">  
function check_mult_action(form_id)
{
	var chk=document.getElementsByName("selectedId[]");
	var len=chk.length;
	var chk_flag=0;
	for(var i=0;i<len;i++)
	{
		if(chk[i].checked==true)
		{chk_flag=1;break;}
	}
	if(chk_flag==1)
	{document.getElementById(form_id).submit();}
	else
	{
		document.getElementById("del_error").innerHTML='<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error:</strong> Please select record to delete.</div>';
		return false;
	}
	
	
}
</script><?php */?>
<!-- begin PAGE TITLE AREA -->
<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
<div class="row">
    <div class="col-lg-12">
        <div class="page-title">
            <h1>Outbox</h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i>
                <a href="<?php echo base_url().'crt'?>">Dashboard</a>
                </li>
                <li class="active">Messages</li>
        
            </ol>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- end PAGE TITLE AREA -->
<div class="row">

    <div class="col-lg-12">


        <div id="del_error"></div>

		<?php 
        if($this->session->flashdata('success')!="")
        {
        ?>
        <div class="alert alert-success alert-dismissable">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>
        <?php    
        } 
        if($this->session->flashdata('error')!="")
        {
        ?>
        <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>
        <?php
        } 
        ?>
        </div>
        
        
        

    
    <form name="inbox" id="inbox" method="post" action="<?php echo base_url(); ?>crt/message/multdelete">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <div class="btn-group hidden">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Mail <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Mail</a></li>
                        <li><a href="#">Contacts</a></li>
                        <li><a href="#">Tasks</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 col-md-10">
                <!-- Split button -->
                <button type="button" class="btn btn-default" style="padding-top: 10; padding-bottom: 4px; padding-right: 7px; background: #fff;">
                    <input type="checkbox" id="selectall" data-toggle="tooltip" data-placement="bottom" 
                    title="Select All"> <label for="selectall"></label>
                </button>
                <a type="button" class="btn btn-default refresh_btn" href="">
                       <span class="glyphicon glyphicon-refresh"></span>   </a>
                <!-- Single button -->
                <div class="btn-group hidden">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        More <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Mark all as read</a></li>
                        <li class="divider"></li>
                        <li class="text-center"><small class="text-muted">Select messages to see more actions</small></li>
                    </ul>
                </div>
                
                
                <button type="submit" name="addtotrash" id="addtotrash" class="btn btn-default disabled" title="Trash">
                <i class="fa fa-trash-o" >
                </i>
                </button>
                 
                <div class="pull-right">
                    <span class="text-muted"><b><?php echo $count_inbox; ?></b> total messages</span>
                    <?php /*?><div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </button>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>
                    </div><?php */?>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <a href="<?php echo base_url().'crt/message/compose' ?>" class="btn btn-danger btn-sm btn-block" role="button">COMPOSE</a>
                <hr />
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo base_url().'crt/message/inbox' ?>">
                    
                    
                     <?php 
						$unread=$this->common_model->get_unread_count($this->session->userdata('logged_cc_login_id'));
						
						if($unread > 0){echo '<span class="badge pull-right">'.$unread.'</span>';} ?>
                    
                    
                     Inbox </a>
                    </li>
                    <?php /*?><li><a href="#">Starred</a></li>
                    <li><a href="#">Important</a></li><?php */?>
                    <li class="active"><a href="<?php echo base_url().'crt/message/sentmail' ?>">Sent Mail</a></li>
                    <li><a href="<?php echo base_url().'crt/message/trash' ?>">Trash</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" style="margin-bottom: 10px;">
                    <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox">
                    </span>Primary</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                        <?php if (count($outbox) > 0) { ?>
                        <div id="mailbox">
                            <div class="table-responsive"><!--mailbox-messages-->
                                <div class="table-responsive"><!-- mailbox-messages table-bordered-->
                                    <table class="table table-hover  table-green table-striped" id="example-tablexx">
                                        <thead class="hidden">
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        
                                        foreach($outbox as $out)
                                        {
                                            ?>
                                            <tr class="">
                                                <td class="checkbox-col">
                                                    <input type="checkbox" class="selectedId" name="selectedId[]" id="selectedId[]" value="<?php echo $out['message_id'] ?>">
                                                </td>
                                                <td class="from-col"><?php echo $this->master_model->decryptIt($out['crt_first_name']).' '.$this->master_model->decryptIt($out['crt_last_name']); ?></td>
                                                <td class="msg-col clickableRow">
                                                    <a href="<?php echo base_url().'crt/message/read/'.$out['message_id'].'/outbox'; ?>"><?php echo $out['subject']; ?></a> <span class="text-muted small hidden-xs">- <?php echo substr(strip_tags($out['message']),0,40); ?></span>
                                                </td>
                                                <td class="date-col text-right"><!--<i class="fa fa-paperclip"></i>--> <?php echo date('d M Y',strtotime($out['send_date'])); ?></td>
                                            </tr>
                                        <?php 
                                        } 
                                        ?>
                                        </tbody>
                                    </table>
                                <!-- </div>-->
                            </div>
                        </div>                    
						<?php } else { echo '<div class="list-group"> <div class="list-group-item"> <span class="text-center text-muted">Empty..</span> </div> </div>'; }?>
                    
                    </div>
               
               
                </div>
               
            </div>
        </div>
    </div>
</div>
</form>

    </div>    
    
    
</div><!--.row -->
					

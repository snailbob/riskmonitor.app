                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                
<?php 
	//select org
	$the_active_module = array('0'=>array('active_module'=>$this->session->userdata('org_module')) );

	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));

	/*********************************************************/
?>                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Pre-Incident

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Pre-Incident</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>




                    <div class="col-lg-12">
	
    
    
    				<?php //case management module activated
						if (strpos($the_active_module[0]['active_module'], '2') !== false){ ?>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-user fa-fw"></i><br /><h4>Response Team</h4>
                                 </a>
                            </div>
    
    
                        </div>
					<?php } ?>                                   

					<?php //standard crisisflo activated
						if (strpos($the_active_module[0]['active_module'], '1') !== false){ ?>

                        <div class="row">
                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-user fa-fw"></i><br /><h4>Response Team</h4>
                                 </a>
                            </div>
        
                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/document/manage" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-paperclip fa-fw"></i><br /><h4>Crisis Documents</h4>
                                 </a>
                            </div>
                            
                        </div>
                        
                    
                        <div class="row">

                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/scenario/managetask" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-lightbulb-o fa-fw"></i><br /><h4>Response Plan</h4>
                                 </a>
                            </div>
                            
                            
                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/standbymessage/manage" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-folder-open fa-fw"></i><br /><h4>Standby Messages</h4>
                                 </a>
                            </div>

                        </div> 
                        
                          
                          
					<?php } ?>                                   
                        
                        
                        
            

					<?php //recall module activated
						if (strpos($the_active_module[0]['active_module'], '5') !== false || strpos($the_active_module[0]['active_module'], '8') !== false && strpos($the_active_module[0]['active_module'], '1') === false ){ ?>



                        <div class="row">
                        	
                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/crisisteam/managecrt" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-user fa-fw"></i><br /><h4>Response Team</h4>
                                 </a>
                            </div>
    
                            <div class="col-sm-4 hidden">
                                <a href="<?php echo base_url()?>crt/stakeholder/managestk" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-users fa-fw"></i><br /><h4>Stakeholders</h4>
                                 </a>
                            </div>


                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/document/manage" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-paperclip fa-fw"></i><br /><h4>Crisis Documents</h4>
                                 </a>
                            </div>
                            
                        </div>
                        
					<?php } ?>                                   
                        
                        
                        

                    </div><!----.col-lg-12-->



                </div><!--.row-->




<?php  if($this->session->userdata('logged_crt_login_id')=="")

{redirect(base_url().'crt');} ?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>



    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}

	</style>
    

</head>

<body style="background: #efefef;">


    <div class="container">

        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>


                
				<?php 
                if($this->session->flashdata('success')!="")

                {

                ?>

                <div class="alert alert-success alert-dismissable">

                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                <?php    

                } 

                if($this->session->flashdata('error')!="")

                {

                ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                <?php

                } 

                ?>


                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong><?php echo $this->session->userdata('crt_selected_organization_name'); ?></strong>
                            	<span class="small pull-right hidden">
                                    <?php if (strpos($org_modules[0]['active_module'], '3') !== false){ ?>
                                            <span class="fa-stack fa-xs tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="bottom" title="Video Conferencing feature activated" >
                                              <i class="fa fa-circle fa-stack-2x"></i>
                                              <i class="fa fa-video-camera fa-stack-1x fa-inverse"></i>
                                            </span>                                      
                                    <?php }else { ?>
                                            <span class="fa-stack fa-xs tooltip-test" id="cat_chevron" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Video Conferencing feature not activated">
                                              <i class="fa fa-video-camera fa-stack-1x"></i>
                                              <i class="fa fa-ban fa-stack-2x text-danger"></i>
                                            </span>                                        
                                    <?php } ?>
                                </span>
                            
                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                    

                        <div id='login_form'>

                            <form action='' method='post' name="admin-login" id="admin-login">

                                

                              <br />
	

                                <fieldset>
                                

                                
                                
                                    <div class="form-group">
                                    	<label for="org_module">Select Module</label>
                                        <select class="form-control"  name='org_module' id='org_module' />

                                            <option value="">--Select--</option>
                                            <?php
												if(count($org_modules) > 0){
																	
													if (strpos($org_modules[0]['active_module'], 'm3m') !== false){
														$vid_mod = 3;
													} else{
														$vid_mod = '';
													}
													
													if (strpos($org_modules[0]['active_module'], 'm6m') !== false){
														$cost_mod = 6;
													} else{
														$cost_mod = '';
													}
													
													if (strpos($org_modules[0]['active_module'], 'm7m') !== false){
														$sms_mod = 7;
													} else{
														$sms_mod = '';
													}
													
													if (strpos($org_modules[0]['active_module'], 'm10m') !== false){
														$kpi_mod = 0;
													} else{
														$kpi_mod = '';
													}
													
													if (strpos($org_modules[0]['active_module'], 'm11m') !== false){
														$sim_mod = 'a';
													} else{
														$sim_mod = '';
													}
													
													if (strpos($org_modules[0]['active_module'], 'm12m') !== false){
														$ava_mod = 'b';
													} else{
														$ava_mod = '';
													}
													
													
													
													foreach($main_module as $r){
                                                        echo '<option value="'.$r.$ava_mod.$sim_mod.$kpi_mod.$sms_mod.$cost_mod.$vid_mod.'">'.$this->common_model->getmodulename($r).'</option>';
													}
												}
											?>
                                                
										
                                        </select>
                                    </div>
                                    

                                    
                                    <input  type="submit" class="btn btn-primary btn-block" value="Submit" 

                                    name="btn_select_org" id="btn_select_org" />	

                                </fieldset>

                                <br>

                               

                            </form>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>
    
    <?php /*?><script src="<?php echo base_url()?>assets/2/js/cc-validation.js"></script><?php */?>


    <script>
	$('.common_msg_click').on('click', function ( e ) { 
				$.fn.custombox( this );
				e.preventDefault();
	});
    </script>
</body>

</html>

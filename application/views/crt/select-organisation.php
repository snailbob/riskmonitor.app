<?php  if($this->session->userdata('logged_crt_login_id')=="")

{redirect(base_url().'crt');} ?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>



    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}

	</style>
    

</head>

<body style="background: #efefef;">


    <div class="container">

        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>


                
				<?php 
                if($this->session->flashdata('success')!="")

                {

                ?>

                <div class="alert alert-success alert-dismissable">

                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                <?php    

                } 

                if($this->session->flashdata('error')!="")

                {

                ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                <?php

                } 

                ?>


                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Select Organization
</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                    

                        <div id='login_form'>

                            <form action='' method='post' name="admin-login" id="admin-login">
                                

                                <br />
                                <fieldset>

                                    <div class="form-group">

                                        <select class="form-control"  name='org_name' id='org_name' />

                                            <option value="">--Select--</option>
                                            
                                            <?php
                                                if(count($crt_orgs) > 0){
                                                    foreach($crt_orgs as $r){
                                                        echo '<option value="'.$r.'">'.$this->common_model->getorgname($r).'</option>';
                                                    }
                                                }
                                            ?>



                                        </select>


                                    </div>

                                   

                                    <input  type="submit" class="btn btn-primary btn-block" value="Submit" 

                                    name="btn_select_org" id="btn_select_org" />	

                                </fieldset>

                                <br>

                               

                            </form>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>
    
    <?php /*?><script src="<?php echo base_url()?>assets/2/js/cc-validation.js"></script><?php */?>


    <script>
	$('.common_msg_click').on('click', function ( e ) { 
				$.fn.custombox( this );
				e.preventDefault();
	});
    </script>
</body>

</html>

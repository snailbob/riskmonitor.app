                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Settings

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'crt'?>">Dashboard</a></li>

                                <li class="active">Settings</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    
                        <div class="row">

                            <div class="<?php if ($this->session->userdata('crt_consultant_id') !='') { echo 'col-sm-6';} else{ echo 'col-sm-4';}?>">
                                <a href="<?php echo base_url()?>crt/settings/changePassword" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-key fa-fw"></i><br /><h4>Change Password</h4>
                                 </a>
                            </div>
    
                            <div class="<?php if ($this->session->userdata('crt_consultant_id') !='') { echo 'col-sm-6';} else{ echo 'col-sm-4';}?>">
                                <a href="<?php echo base_url()?>crt/settings/updateinfo" class="thumbnail text-center preloadThis sidebar_submenu">
                                    <i class="fa fa-info fa-fw"></i><br /><h4>Update Details</h4>
                                 </a>
                            </div>
                            
                        
						<?php if ($this->session->userdata('crt_consultant_id') !='') {?>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="<?php echo base_url()?>crt/dashboard/selectorganization" class="thumbnail text-center preloadThis sidebar_submenu">
                                   <i class="fa fa-building-o fa-fw"></i><br /><h4>Change Organization</h4>
                                 </a>
                            </div>
                            
                        <?php } ?>
                            
                            <div class="<?php if ($this->session->userdata('crt_consultant_id') !='') { echo 'col-sm-6';} else{ echo 'col-sm-4';}?>">
                                <a href="<?php echo base_url()?>crt/dashboard/selectmodule" class="thumbnail text-center preloadThis sidebar_submenu">
                                   <i class="fa fa-eraser fa-fw"></i><br /><h4>Change Module</h4>
                                 </a>
                            </div>
                        </div>
                        

                    </div><!----.col-lg-12-->



                </div><!--.row-->




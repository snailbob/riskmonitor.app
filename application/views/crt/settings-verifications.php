                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Settings

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'cc'?>">Dashboard</a></li>

                                <li class="active">Settings</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">
                    
                    
                    	<div class="panel panel-default">
                        	<div class="panel-heading">
                            	<h4 class="heading-title">Verifications</h4>
                            
                            </div>
                            
                            <div class="panel-body">


                                <div class="gplus_btns">
                                
                                    <a href="javascript: void(0);" class="btn btn-default btn-block btn-lg gplus_connected_btn <?php if ($user_info[0]['gplus_id'] == '') { echo 'hidden';} else{ echo 'Connect LinkedIn'; }?>"><i class="fa fa-google-plus fa-fw text-danger"></i> Google+ connected</a>
                                    
                                    <a href="javascript: void(0);" id="MyGooglePlusButton" class="btn btn-default btn-block btn-lg gplus_btn <?php if ($user_info[0]['gplus_id'] != '')  { echo 'hidden'; }?>" onClick="handleAuthClick();"><i class="fa fa-google-plus fa-fw text-danger"></i> Connect Google+</a>
                                    
                                </div>
                                
                                <div class="linkedin_btns" style="margin-top: 5px;">
                                
                                    <a href="javascript: void(0);" class="btn btn-primary btn-block btn-lg linkedin_connected_btn <?php if ($user_info[0]['linkedin_id'] == '') { echo 'hidden';} else{ echo 'Connect LinkedIn'; }?>"><i class="fa fa-linkedin fa-fw"></i> LinkedIn connected</a>
                                    
                                    <a href="javascript: void(0);" id="MyLinkedInButton" class="btn btn-primary btn-block btn-lg linkedin_btn <?php if ($user_info[0]['linkedin_id'] != '')  { echo 'hidden'; }?>"><i class="fa fa-linkedin fa-fw"></i> Connect LinkedIn</a>
                                    
                                </div>
        

                            </div>
                        </div>
                    
                    
                    </div><!----.col-lg-12-->



                </div><!--.row-->




                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Personal Information

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>">Dashboard</a></li>

                                <li class="active">Update Details</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->

			     <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                       	<?php if($this->session->flashdata('success')!=""){ ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                            </div>

                            <?php } if($this->session->flashdata('error')!=""){ ?>

                            <div class="text-red">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>'

                            <?php } ?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update Details</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="admin_change_password" id="admin_change_password" method='post' class="form-horizontal" role="form" validate>

						

                     

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="f_name" name="f_name" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_first_name'])?>" required data-msg-required="Please enter last name"  readonly="readonly"><?php echo form_error('f_name');  ?>

                            </div>

                        </div>

                    

                          

                        <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="l_name" name="l_name" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_last_name'])?>" required data-msg-required="Please enter first name" readonly="readonly"><?php echo form_error('l_name'); ?>

                            </div>

                        </div>

                        

                         <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Email Address</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="emailid" name="emailid" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_email'])?>" required data-msg-required="Please enter first name" readonly="readonly"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>



                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['country_id'].' '.$countries['calling_code'].'"';
                                        
                                        if ($personal_info[0]['country_id'] == $countries['country_id']){
                                            echo ' selected="selected"';
                                        }
                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                            <input type="text" class="form-control" id="ph_no" name="ph_no" value="<?php echo $this->master_model->decryptIt($personal_info[0]['crt_digits'])?>" required data-msg-required="Please enter first name"><?php echo form_error('ph_no'); ?>

                            </div>

                        </div>



                        

                           <div class="form-group">

                        <label for="firstname" class="col-sm-2 control-label">Position</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="position" name="position" value="<?php echo $personal_info[0]['crt_position']?>" required data-msg-required="Please enter first name"><?php echo form_error('emailid'); ?>

                            </div>

                        </div>

                           

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="update_personal_info_crt" id="update_personal_info_crt">Submit</button>

                            </div>

                        </div>

										

									</form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




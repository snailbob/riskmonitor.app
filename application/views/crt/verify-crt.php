<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    
<script language="javascript" type="text/javascript">

// validation of verify stk password validation
$(document).ready(function(e) {
		$("#verify_crt").click(function()

		{ 	

			var crt_password=$("#crt_password");

			var confirm_crt_password=$("#confirm_crt_password");

			if(crt_password.val()=="")

            { 

			    crt_password.val('');

				$('#crt_password').attr('placeholder','Please Enter Password');

				$('#crt_password').removeClass('form-control');

				$('#crt_password').addClass('error_border');

				$('#crt_password').focus();

				return false;

			}

			if(crt_password.val().length<6)

            { 

			    crt_password.val('');

				$('#crt_password').attr('placeholder','Password Must Be Atleast 6 Character');

				$('#crt_password').removeClass('form-control');

				$('#crt_password').addClass('error_border');

				$('#crt_password').focus();

				return false;

			}

			else if(confirm_crt_password.val()=="")

            { 

			    confirm_crt_password.val('');

				$('#confirm_crt_password').attr('placeholder','Please Enter Confirm Password');

				$('#confirm_crt_password').removeClass('form-control');

				$('#confirm_crt_password').addClass('error_border');

				$('#confirm_crt_password').focus();

				return false;

			}

			else if(crt_password.val()!=confirm_crt_password.val())

            { 

			    confirm_crt_password.val('');

				$('#confirm_crt_password').attr('placeholder','Confirm Password Does Not Match');

				$('#confirm_crt_password').removeClass('form-control');

				$('#confirm_crt_password').addClass('error_border');

				$('#confirm_crt_password').focus();

				return false;

			 }			

		});
});


</script>    
 
     <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->


</head>

<body style="background: #efefef;">


    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">




                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>New Crisis Response Team Member Account</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

						

						<?php 

						if($success!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $success; ?></div>

                        <?php    

						} 

						if($error!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $error; ?></div>

                        <?php

						} 

						?>

						

						

						<?php 

						if(count($crt_info)>0 && $this->uri->segment(3)!="")

						{ 

						?>

						<?php if($crt_info[0]['user_status']==0){ ?>

                        <h4>Please choose a password for your CrisisFlo account.</h4><br/>

                        <?php } if($crt_info[0]['user_status']==1){ ?>

                        <h4>Your Account details are already confirmed. You can <a class="btn btn-primary" href="<?php echo base_url()?>signin">Sign in here</a></h4><br/>
</h4><br/>

                        <?php } ?>

                        
<?php /*?>
                        	Name : <?php echo $crt_info[0]['crt_first_name']; ?><br/>

                            Last Name : <?php echo $crt_info[0]['crt_last_name']; ?><br/>

                            Email Address : <?php echo $crt_info[0]['crt_email']; ?><br/>

                            Phone Number : <?php echo $crt_info[0]['crt_mobile']; ?><br/>

                            Position : <?php echo $crt_info[0]['crt_position'] ?><br />

                            Organization : <?php echo $crt_info[0]['crt_organisation'] ?><br /><br /><?php */?>

                        

                            <?php $cc_login_id = $crt_info[0]['login_id'];  ?>

                           

                        	<?php if($crt_info[0]['user_status']==0){ ?>

                      	<form method='post' name='process' class="form-horizontal" role="form" action="">

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                 <h2>Password Selection</h2>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Email address</label>

                                    <input type="email" class="form-control"  name='cc_email' id='cc_email' 

                                    value='<?php echo $crt_info[0]['crt_email']; ?>' disabled='disabled'>

                                    <input type="hidden" name="cc_login_id" id="cc_login_id" value="<?php echo $cc_login_id; ?>" >
                                    <input type="hidden" name="crt_email" id="crt_email" value="<?php echo $crt_info[0]['crt_email'];?>">
                                    <input type="hidden" name="crt_first_name" id="crt_first_name" value="<?php echo $crt_info[0]['crt_first_name'];?>">
                                    <input type="hidden" name="crt_last_name" id="crt_last_name" value="<?php echo $crt_info[0]['crt_last_name'];?>">
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Create Password</label>

                                    <input type="password" class="form-control" name='crt_password' id="crt_password" placeholder="Password">

                                    <div class="col-sm-12 my-help-text" style="margin-top:15px;">
                                        <span class="pwstrength_viewport_progress"></span> <span class="pwstrength_viewport_verdict"></span>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Confirm Password</label>

                                    <input type="password" class="form-control" id="confirm_crt_password" name="confirm_crt_password" placeholder="Confirm Password">

                                </div>

                            </div>



                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                    <button type="submit" name="verify_crt" id="verify_crt" class="btn btn-primary">Submit</button>

                                </div>

                            </div>

                        </form>

                        <?php 

							}

						}

						else

						{

						?>

                         <h4>Please check link, invalid link</h4>

                        <?php	

						}

						?>





                    </div>

                </div>

            </div>

        </div>

    </div>



    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="text-muted">© 2014 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                viewports: {
                    progress: ".pwstrength_viewport_progress",
                    verdict: ".pwstrength_viewport_verdict"
                }
            };
            options.common = {
                onLoad: function () {
                    $('#messages').text('Start typing password');
                },
                zxcvbn: true
            };
            $(':password').pwstrength(options);
        });
    </script>

</body>

</html>
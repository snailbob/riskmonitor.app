<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><!--[if IE]><html xmlns="http://www.w3.org/1999/xhtml" class="ie-browser" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><![endif]--><!--[if !IE]><!--><html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--><head>
    <!--[if gte mso 9]><xml>
     <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <title>Template Base</title>
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
	<!--<![endif]-->

    <style type="text/css" id="media-query">
      body {
  margin: 0;
  padding: 0; }

table {
  border-collapse: collapse;
  table-layout: fixed; }

* {
  line-height: inherit; }

a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important; }

.ie-browser .col, [owa] .block-grid .col {
  display: table-cell;
  float: none !important;
  vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
  width: 500px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%; }

.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
  width: 164px !important; }

.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
  width: 328px !important; }

.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
  width: 250px !important; }

.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
  width: 166px !important; }

.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
  width: 125px !important; }

.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
  width: 100px !important; }

.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
  width: 83px !important; }

.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
  width: 71px !important; }

.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
  width: 62px !important; }

.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
  width: 55px !important; }

.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
  width: 50px !important; }

.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
  width: 45px !important; }

.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
  width: 41px !important; }

@media only screen and (min-width: 520px) {
  .block-grid {
    width: 500px !important; }
  .block-grid .col {
    display: table-cell;
    Float: none !important;
    vertical-align: top; }
    .block-grid .col.num12 {
      width: 500px !important; }
  .block-grid.mixed-two-up .col.num4 {
    width: 164px !important; }
  .block-grid.mixed-two-up .col.num8 {
    width: 328px !important; }
  .block-grid.two-up .col {
    width: 250px !important; }
  .block-grid.three-up .col {
    width: 166px !important; }
  .block-grid.four-up .col {
    width: 125px !important; }
  .block-grid.five-up .col {
    width: 100px !important; }
  .block-grid.six-up .col {
    width: 83px !important; }
  .block-grid.seven-up .col {
    width: 71px !important; }
  .block-grid.eight-up .col {
    width: 62px !important; }
  .block-grid.nine-up .col {
    width: 55px !important; }
  .block-grid.ten-up .col {
    width: 50px !important; }
  .block-grid.eleven-up .col {
    width: 45px !important; }
  .block-grid.twelve-up .col {
    width: 41px !important; } }

@media (max-width: 520px) {
  .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important; }
  .block-grid {
    width: calc(100% - 40px) !important; }
  .col {
    width: 100% !important; }
    .col > div {
      margin: 0 auto; }
  img.fullwidth {
    max-width: 100% !important; } }

    </style>
</head>
<!--[if mso]>
<body class="mso-container" style="background-color:#FFFFFF;">
<![endif]-->
<!--[if !mso]><!-->
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<!--<![endif]-->
  <div class="nl-container" style="min-width: 500px;Margin: 0 auto;background-color: #FFFFFF">

    <div style="background-color:#2db1ff;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 500px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:#2db1ff;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; padding-right: 0px; padding-left: 0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <div rel="col-num-container-box" class="col num12" style="min-width: 500px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;">
                  <div style="line-height: 20px; font-size:1px">&nbsp;</div>


<!--[if !mso]><!--><div align="center" style="Margin-right: 10px;Margin-left: 10px;"><!--<![endif]-->
  <div style="line-height: 10px; font-size:1px">&nbsp;</div>
  <!--[if (mso)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px;padding-left: 10px;"><![endif]-->
  <div style="border-top: 0px solid transparent; width:100%; font-size:1px;">&nbsp;</div>
  <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
  <div style="line-height:10px; font-size:1px">&nbsp;</div>
<!--[if !mso]><!--></div><!--<![endif]-->



<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <img class="center" align="center" border="0" src="<?php echo base_url() ?>assets/frontpage/corporate/images/crisisflo-logo-medium.png" alt="CrisisFlo Logo" title="CrisisFlo Logo" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 100%;max-width: 186px" width="184">
</div>

                                  <div style="line-height: 20px; font-size: 1px">&nbsp;</div>
              </div>
            </div>
          <!--[if (mso)|(IE)]></td></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:#FFFFFF;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 500px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:#FFFFFF;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; padding-right: 0px; padding-left: 0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <div rel="col-num-container-box" class="col num12" style="min-width: 500px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;">
                  <div style="line-height: 30px; font-size:1px">&nbsp;</div>


<div style="Margin-right: 10px; Margin-left: 10px;">
	<div style="font-size:12px;line-height:15px;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif; font-weight: 300; color:#B8B8C0;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 15px; line-height: 27px; color: rgb(0, 0, 0);"><?php if(isset($first_name)) { echo 'Hi '.$first_name; } else { echo 'Hi';} ?>,</span></p><p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 15px; line-height: 27px; color: rgb(0, 0, 0);"><br data-mce-bogus="1"></span></p></div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>


<div style="Margin-right: 10px; Margin-left: 10px;">
	<div style="font-size:12px;line-height:15px;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif; font-weight: 300; color:#B8B8C0;text-align:left;"><div style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 15px; line-height: 27px; color: rgb(0, 0, 0);">

<?php
	$mess_info=$this->master_model->getRecords('email_messages_master',array('id'=>'18'));
	//You have successfully activated your CrisisFlo account. You can now access your user panel by #
	
	$click = '<a href="'.base_url().'consultant'.'" target="_blank">click here</a>';
	$clicking = '<a href="'.base_url().'consultant'.'" target="_blank">clicking here</a>';
	
	$healthy = array("[click_here]", "[clicking_here]");
	$yummy   = array($click, $clicking);

	$newphrase = str_replace($healthy, $yummy, $mess_info[0]['content']);
	
	echo $newphrase;

?></span><br><br>


<a style="font-weight:bold; text-decoration:none;" href="<?php echo base_url().'consultant'; ?>"><div style="display:block; max-width:100% !important; width:95% !important; height:auto !important;background-color:#4dbbfc;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;border-radius:2px;color:#ffffff;font-size:20px;font-family:Arial, Helvetica, sans-serif; text-align:center"><?php echo $mess_info[0]['button_text'];?></div></a>
 

<!--[if !mso]><!--><div align="center" style="Margin-right: 10px;Margin-left: 10px;"><!--<![endif]-->
  <div style="line-height: 10px; font-size:1px">&nbsp;</div>
  <!--[if (mso)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px;padding-left: 10px;"><![endif]-->
  <div style="border-top: 0px solid transparent; width:100%; font-size:1px;">&nbsp;</div>
  <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
  <div style="line-height:10px; font-size:1px">&nbsp;</div>
<!--[if !mso]><!--></div><!--<![endif]-->

                                  <div style="line-height: 30px; font-size: 1px">&nbsp;</div>
              </div>
            </div>
          <!--[if (mso)|(IE)]></td></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:#ffffff;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 500px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:#ffffff;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; padding-right: 0px; padding-left: 0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <div rel="col-num-container-box" class="col num12" style="min-width: 500px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;">
                  <div style="line-height: 30px; font-size:1px">&nbsp;</div>




</body></html>



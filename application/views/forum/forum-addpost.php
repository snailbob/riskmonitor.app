

	<!--main_container-->
    <div class="container main_container" style="padding-bottom: 15px;">
    	<div class="row">
            <div class="col-xs-12">
            
                <h1 class="page-header">
                	<i class="fa fa-pencil"></i> Add Post
                
                    <a class="btn btn-default" href="<?php echo base_url().'forum/topics/'.$this->uri->segment(3).'/'.$fc_category[0]['fc_id']; ?>"><i class="fa fa-angle-double-left"></i> Back to topics</a>
                
                </h1>
                
                
                <div class="form-group">
                    <input type="text" class="form-control" id="post_title" placeholder="Post Title" value=""/>
                </div>
                
                <div class="form-group">
                    <input type="hidden" class="form-control" id="post_slogan" placeholder="Slogan" value=""/>
                    <input type="hidden" class="form-control" id="post_level" value="0"/>
                    <input type="hidden" class="form-control" id="post_parent" value="0"/>
                    <input type="hidden" class="form-control" id="reply_parent" value=""/>
                </div>
                
                <!-- Summernote CMS Example -->
                <div id="summernote"></div>
                
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right" id="submit_post" style="margin-top: 15px;">Post</button>
                </div>

            </div><!--./col-xs-12-->
        </div><!--./row-->
        
        
    </div><!--./main_container-->


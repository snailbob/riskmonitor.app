    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-muted" style="padding-bottom: 20px;">
		<?php
            //select org
            $theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));

        if ($theorg[0]['cust_type'] == '0'){
        ?>	
						
                        <span class="text-muted pull-right">Powered	by <a href="http://crisisflo.com" target="_blank">Crisisflo</a></span>
        <?php }
		else{ ?>
        
        
                    	<span class="pull-left">© <span class="curr_year"></span> CrisisFlo</span>
		<?php } ?>	
                        
                        
                    </p>
                    
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap-tour.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>
    <!--<script src="<?php echo base_url()?>assets/2/js/plugins/popover-extra-placements/popover-extra-placements.js"></script>-->
    
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/wysiwyg-demo.js"></script>


    <?php /*?><script src="<?php echo base_url()?>assets/2/js/cc-validation.js"></script><?php */?>

    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>
    
	<script>
	
	$(document).ready(function(){
		
		$('#submit_post').click(function(){
			//console.log($('.note-editable').html());
			if($('#post_title').val() == ''){
				bootbox.alert('Title is required.');
				return false;
			}
			else if($('.note-editable').html() == ''){
				bootbox.alert('Content is required.');
				return false;
			}
			$.ajax({
				  type: "POST",
				  
				  url: "<?php echo base_url().'forum/addpost_submit/'.$this->uri->segment(3); ?>",
				  
				  data: {
					  scenario_id: "<?php echo $this->uri->segment(3); ?>",
					  topic_id: "<?php echo $this->uri->segment(4); ?>",
					  post_title: $('#post_title').val(),
					  post_slogan: $('#post_slogan').val(),
					  post_parent: $('#post_parent').val(),
					  reply_parent: $('#reply_parent').val(),
					  post_level: $('#post_level').val(),
					  post_content: $('.note-editable').html(),	  		
						
				  },
				  
				  success: function(data) {
					//$('.filter_crt_result').html(data);
					// similar behavior as an HTTP redirect
					if (data != 'failed'){
						window.location.replace("<?php echo base_url().'forum/post/'.$this->uri->segment(3).'/' ?>"+data);
						//  console.log(data);
					}
					else{
						window.location.replace("<?php echo base_url().'forum/addpost/'.$this->uri->segment(3).'/'.$this->uri->segment(4) ?>");
					}
				  }
				  
			});


		});//end add post
		
		
	
		$('#submit_postupdate').click(function(){
			//console.log($('.note-editable').html());
			if($('#post_title').val() == ''){
				bootbox.alert('Title is required.');
				return false;
			}
			else if($('.note-editable').html() == ''){
				bootbox.alert('Content is required.');
				return false;
			}
			
			
			$.ajax({
				  type: "POST",
				  
				  url: "<?php echo base_url().'forum/updatepost_submit/'.$this->uri->segment(3); ?>",
				  
				  data: {
					  scenario_id: "<?php echo $this->uri->segment(3); ?>",
					  topic_id: "<?php echo $this->uri->segment(4); ?>",
					  post_title: $('#post_title').val(),
					  post_slogan: $('#post_slogan').val(),
					  post_parent: $('#post_parent').val(),
					  post_content: $('.note-editable').html(),	  		
					  post_id: $('#post_id').val(),	  		
						
				  },
				  
				  success: function(data) {
					//$('.filter_crt_result').html(data);
					// similar behavior as an HTTP redirect
					if (data != 'failed'){
						window.location.replace("<?php echo base_url().'forum/post/'.$this->uri->segment(3).'/' ?>"+data);
					}
					else{
						window.location.replace("<?php echo base_url().'forum/addpost/'.$this->uri->segment(3).'/'.$this->uri->segment(4) ?>");
					}
				  }
				  
			});


		});//end update post
		
		
		
		
	});
	
	function delete_post(site_path)
	{ 
	
		bootbox.confirm("<p class='lead'>Do you really want to delete this thread?</p>", function(r)
		{;
			if(r)
			{ 
				location.href=site_path;
			}
		});
	} 
	function delete_reply(site_path)
	{ 
	
		bootbox.confirm("<p class='lead'>Do you really want to delete this reply?</p>", function(r)
		{;
			if(r)
			{ 
				location.href=site_path;
			}
		});
	} 
    </script>

    
    <style>
    
    input#post_title, input#post_slogan{
        border: 1px solid #a9a9a9;
        border-radius: 0px;
    }
    
    #submit_post, #submit_postupdate{
        border-radius: 0px;
    }
    </style>
        
</body>
</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CrisisFlo | Forums</title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?> 
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/bootstrap-tour.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    <!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->


	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    
    
    
</head>

<body>

<?php
	/*GET THE DATA FROM THE SESSION**************************/
	
	
	//form validation global error style
	$this->form_validation->set_error_delimiters('<div class="text-red">', '</div>');
	
	
	$session_userfname  =$this->session->userdata('logged_display_name');
	
	
	//active case report count
	$num_report=$this->master_model->getRecordCount('case_master',array('cc_id'=>$this->session->userdata('logged_cc_login_id'),'org_id'=>$this->session->userdata('cc_selected_orgnaization'),'status'=>'0'));
	
	
	//select org
	$theorg = $this->master_model->getRecords('organization_master', array('organization_id'=>$this->session->userdata('cc_selected_orgnaization')));
	
	//usertype
	$user_type = 'crt';
	if($this->session->userdata('logged_cc_login_id') != ''){
		$user_type = 'cc';
	}

	/*********************************************************/
?>



    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
	   <?php
        if ($theorg[0]['cust_type'] == '0'){
        ?>	
       
                <a class="navbar-brand hidden-xs preloadThis" id="site_logo" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/AIG_rebrand_logo.gif" width="114" height="62" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/AIG_rebrand_logo.gif" width="88" height="48" style="margin-top: -10px;"></a>
        </div>
        
            <div class="navbar-header col-lg-10 col-lg-offset-1 col-sm-8 col-sm-offset-2 hidden-xs" >
                <h3 class="big text-primary" style="text-align: center">Crisis Command Centre</h3>
            </div>

            <div class="navbar-header col-lg-10 col-lg-offset-1 visible-xs" >
                <h4 class="big text-primary" style="text-align: center">Crisis Command Centre</h4>
            </div>
	   <?php }
       else{
        ?>
                <a class="navbar-brand hidden-xs preloadThis" id="site_logo" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="202" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="161" height="48" style="margin-top: -15px;"></a>
        </div>
        
            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 hidden-xs" >
                <h3 class="big text-primary" style="text-align: center">Crisis Command Centre</h3>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 visible-xs" >
                <h4 class="big text-primary" style="text-align: center">Crisis Command Centre</h4>
            </div>



		<?php } ?>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse hidden">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <span class="text-info"></span>
                    </li>
                   <li class="page-scroll"> <a href="<?php echo base_url().$user_type.'/dashboard';; ?>" class="visible-xs  active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                     <?php 
						$unread=$this->common_model->get_unread_count($this->session->userdata('logged_cc_login_id'));
					?>
                   <li class="page-scroll"> <a href="<?php echo base_url().'cc/message/inbox' ?>" class="visible-xs <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-envelope fa-fw"></i> Messages <strong><?php if($unread > 0){echo '<span class="badge danger pull-right">'.$unread.'</span>';} ?></strong></a></li>
                                   
					<?php //display this module if activated
						if ($theorg[0]['active_module'] == '123' || $theorg[0]['active_module'] == '13' || $theorg[0]['active_module'] == '12' || $theorg[0]['active_module'] == '1'){ ?>
                        
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/responce/manage" class="visible-xs"><span class=" text-red"><i class="fa fa-road fa-fw"></i> Initiate Response</span></a></li> 
                   <li class="page-scroll"> <a href="javascript:void(0);" class="hidden"><i class="fa fa-fire fa-fw"></i> Pre-Incident Planning</a></li>
                   
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="visible-xs"><i class="fa fa-male fa-fw"></i> User Group</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="visible-xs"><i class="fa fa-user fa-fw"></i> Response Team</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/stakeholder/managestk" class="visible-xs "><i class="fa fa-users fa-fw"></i> Stakeholders</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/document/manage" class="visible-xs "><i class="fa fa-paperclip fa-fw"></i> Crisis Documents</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/scenario/managetask" class="visible-xs "><i class="fa fa-lightbulb-o fa-fw"></i> Response Plan</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/standbymessage/manage" class="visible-xs "><i class="fa fa-folder-open fa-fw"></i> Standby Message</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/reminder" class="visible-xs"><i class="fa fa-bell-o fa-fw"></i> Set Review Date</a></li>
                   <?php } ?>
                   
                                   
					<?php //display this module if activated
						if (strpos($theorg[0]['active_module'], '2') !== false && strpos($theorg[0]['active_module'], '1') === false ){ ?>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="visible-xs"><i class="fa fa-male fa-fw"></i> User Group</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="visible-xs"><i class="fa fa-user fa-fw"></i> Response Team</a></li>
                   
                   <?php } ?>
                   
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/log" class="visible-xs"><i class="fa fa-edit fa-fw"></i> Post-Incident Review</a></li>
                   
					<?php //display this module if activated
						//if ($theorg[0]['active_module'] == '123' || $theorg[0]['active_module'] == '12' || $theorg[0]['active_module'] == '2' || $theorg[0]['active_module'] == '23'){
						if (strpos($theorg[0]['active_module'], '2') !== false){	
							?>
                   
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/managecase" class="visible-xs"><i class="fa fa-life-ring fa-fw"></i> Case Management
                    <?php
						if($num_report > 0){echo '<strong><span class="badge danger pull-right">'.$num_report.'</span></strong>';}
					?>
                   </a></li>
                   
                   <?php } ?>
                   
                    
                   <li class="page-scroll"> <a href="<?php echo base_url()?>cc/reminder/team" class="visible-xs"><i class="fa fa-paper-plane-o fa-fw"></i> Send Update Request</a></li>
                   <li class="page-scroll"> <a href="<?php echo base_url(); ?>cc/dashboard/selectorganization" class="visible-xs"><i class="fa fa-eraser fa-fw"></i> Change Organization</a></li>
                   
                    <li class="visible-xs"><a href="<?php echo base_url(); ?>cc/settings/changePassword"><i class="fa fa-key fa-fw"></i> Change Password</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>cc/settings/updateinfo"><i class="fa fa-info fa-fw"></i> Update Details</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>cc/settings/timezone"><i class="fa fa-clock-o fa-fw"></i> Set Timezone</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url();?>cc/dashboard/logout/"><i class="fa fa-sign-out fa-fw"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>
               
                   
                    <li class="dropdown hidden-xs hidden-sm">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>cc/settings/changePassword"><i class="fa fa-key fa-fw"></i> Change Password</a></li>
                        <li><a href="<?php echo base_url() ?>cc/settings/updateinfo"><i class="fa fa-info fa-fw"></i> Update Details</a></li>
                        <li><a href="<?php echo base_url() ?>cc/settings/timezone"><i class="fa fa-clock-o fa-fw"></i> Set Timezone</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>cc/dashboard/logout/"><i class="fa fa-sign-out fa-fw"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>
                        
                        
                      </ul>
                    </li>  
                    
                    
                    
                    <li class="dropdown dropdown-large visible-sm"><!--menu for small devices-->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
                        
                        <ul class="dropdown-menu dropdown-menu-large row">
                            <li class="col-sm-6">
                                <ul>
                                       <li> <a href="<?php echo base_url().$user_type.'/dashboard';; ?>" class="dropdown-header preloadThis <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>

                                    <li class="divider"></li>
    
                                           <?php 
                                            $unread=$this->common_model->get_unread_count($this->session->userdata('logged_cc_login_id'));
                                        ?>
                                       <li> <a href="<?php echo base_url().'cc/message/inbox' ?>" class="dropdown-header preloadThis <?php if($this->uri->segment(2)=="message"){echo "active";}?>"><i class="fa fa-envelope fa-fw"></i> Messages <strong><?php if($unread > 0){echo '<span class="badge danger pull-right" style="margin-top: 3px;">'.$unread.'</span>';} ?></strong></a></li>
                                       
                                       
					<?php //display this module if activated
						//if ($theorg[0]['active_module'] == '123' || $theorg[0]['active_module'] == '13' || $theorg[0]['active_module'] == '12' || $theorg[0]['active_module'] == '1'){ 
						if (strpos($theorg[0]['active_module'], '1') !== false){	
						
						?>

                                    <li class="divider"></li>

                                   <li> <a href="<?php echo base_url(); ?>cc/responce/manage" class="dropdown-header preloadThis"><span class=" text-red"><i class="fa fa-road fa-fw"></i> Initiate Response</span></a></li>                                   
                                   
                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url()?>cc/preincident" class="preloadThis dropdown-header"><i class="fa fa-fire fa-fw"></i> Pre-Incident Planning</a></li>
                                   
                                   <li> <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="preloadThis">User Group</a></li>
                                   <li> <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="preloadThis">Response Team</a></li>
                                   <li> <a href="<?php echo base_url(); ?>cc/stakeholder/managestk" class="preloadThis">Stakeholders</a></li>
                                   <li> <a href="<?php echo base_url(); ?>cc/document/manage" class="preloadThis">Crisis Documents</a></li>
                                   <li> <a href="<?php echo base_url(); ?>cc/scenario/managetask" class="preloadThis">Response Plan</a></li>
                                   <li> <a href="<?php echo base_url(); ?>cc/standbymessage/manage" class="preloadThis">Standby Message</a></li>
                                   <li> <a href="<?php echo base_url()?>cc/reminder" class="preloadThis">Set Review Date</a></li>
                                   
                                    
                                   
					<?php } ?>       
                                                

					<?php //display this module if activated
						if (strpos($theorg[0]['active_module'], '2') !== false && strpos($theorg[0]['active_module'], '1') === false ){ ?>


                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url()?>cc/preincident" class="preloadThis dropdown-header"><i class="fa fa-fire fa-fw"></i> Pre-Incident Planning</a></li>
                                   
                                   <li> <a href="<?php echo base_url()?>cc/usergroup/managegroup" class="preloadThis">User Group</a></li>
                                   <li> <a href="<?php echo base_url()?>cc/crisisteam/managecrt" class="preloadThis">Response Team</a></li>
                                   
					<?php } ?>                                   

                                   
                                </ul>
                            </li>
                            
                            
                            
                            <li class="col-sm-6">
                                <ul>

                                   
					<?php //display this module if activated
						//if ($theorg[0]['active_module'] == '123' || $theorg[0]['active_module'] == '12' || $theorg[0]['active_module'] == '2' || $theorg[0]['active_module'] == '23'){
						if (strpos($theorg[0]['active_module'], '2') !== false){	
							
							?>
                        
                 
                                   <li> <a href="<?php echo base_url()?>cc/managecase" class="preloadThis dropdown-header"><i class="fa fa-life-ring fa-fw"></i> Case Management
                    <?php
						if($num_report > 0){echo '<strong><span class="badge danger pull-right" style="margin-top: 3px;">'.$num_report.'</span></strong>';} ?>
                    
                     </a></li>
                                    <li class="divider"></li>
                                    
                     <?php } ?>               

					<?php //display this module if activated
						//if ($theorg[0]['active_module'] == '123' || $theorg[0]['active_module'] == '13' || $theorg[0]['active_module'] == '23' || $theorg[0]['active_module'] == '3'){ 
						if (strpos($theorg[0]['active_module'], '3') !== false){	
						?>

                                   <li> <a href="<?php echo base_url()?>cc/conference" class="preloadThis dropdown-header"><i class="fa fa-video-camera fa-fw"></i> Video Conferencing</a></li>
                                    <li class="divider"></li>
                                    
                     <?php } ?>               
                                    
                                   <li> <a href="<?php echo base_url()?>cc/reminder/team" class="preloadThis dropdown-header"><i class="fa fa-paper-plane-o fa-fw"></i> Send Update Request</a></li>
                                    <li class="divider"></li>
                                    
                                   <li> <a href="<?php echo base_url()?>cc/log" class="preloadThis dropdown-header"><i class="fa fa-edit fa-fw"></i> Post-Incident Review</a></li>
                                    <li class="divider"></li>
                                    
                                    
                                    <li><a href="<?php echo base_url()?>cc/settings" class="preloadThis dropdown-header"><i class="fa fa-gear fa-fw"></i> Settings</a></li>

                                   <li> <a href="<?php echo base_url(); ?>cc/dashboard/selectorganization" class="preloadThis">Change Organization</a></li>

                                    <li><a href="<?php echo base_url(); ?>cc/settings/changePassword" class="preloadThis">Change Password</a></li>
                                    <li><a href="<?php echo base_url() ?>cc/settings/updateinfo" class="preloadThis">Update Details</a></li>
                                    <li><a href="<?php echo base_url() ?>cc/settings/timezone" class="preloadThis"> Set Timezone</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="<?php echo base_url();?>cc/dashboard/logout/" class="dropdown-header"><i class="fa fa-sign-out fa-fw"></i> Logout <strong><?php echo $session_userfname; ?></strong></a></li>
                                </ul>
                            </li>
                            
                                   
        
                            <li class="col-sm-6">
                                <ul>
                                   
                                </ul>
                            </li>
                            

                        </ul>
                        
                    </li><!---.end of large dropdown-->
                            
                    
     
     
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            
        </div>
        <!-- /.container -->
    </nav>



	<!--main_container-->
    <div class="container main_container">

        <h1 class="page-header"><i class="fa fa-file-text-o"></i> <?php echo $post[0]['fp_title'] ?> <a class="btn btn-default" href="<?php echo base_url().'forum/topics/'.$this->uri->segment(3).'/'.$post[0]['fp_category_id']; ?>"><i class="fa fa-angle-double-left"></i> Back to topics</a></h1>
        
	<?php if($this->session->flashdata('success')!="") { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissable">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
                </div>
            </div>
        </div>

	<?php }  if($this->session->flashdata('error')!="") { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
                </div>
             </div>
         </div>

	<?php } ?>


          <p class="lead"><?php echo $post[0]['fp_slogan'] ?></p>
          <p class="alert alert-danger hidden">This topic is <strong>2 years</strong> old. It can be missleading. Add warnings just like any other alerts with Bootstrap!</p>
          <ul class="media-list forum">
            <!-- Forum Post -->
            <li class="media well">
            
              <div class="user-info col-sm-2 col-xs-12" href="#">
                <?php /*?><img class="avatar img-circle img-thumbnail hidden" src="http://snipplicious.com/images/guest.png"
                  width="64" alt="Generic placeholder image"><?php */?>
                <strong><span class="text-info">
                	<?php
						$auth1 = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$post[0]['fp_author_id']));
						echo $this->master_model->decryptIt($auth1[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth1[0]['crt_last_name']);
					?>
                
                </span></strong><br />
                <small><?php if ($auth1[0]['org_id'] == '0') { echo 'Coordinator'; } else { echo 'Member'; } ?></small>
                <?php /*?><br>
                <small class="btn-group btn-group-xs">
                <a class="btn btn-default"><i class="fa fa-thumbs-o-up"></i></a>
                <a class="btn btn-default"><i class="fa fa-thumbs-o-down"></i></a>
                <strong class="btn btn-success">+1</strong>
                </small><?php */?>
              </div>
              <div class="media-body col-sm-10 col-xs-12">
                <!-- Post Info Buttons -->
                <div class="forum-post-panel btn-group btn-group-xs">
                  <a href="#" class="btn btn-default disabled"><i class="fa fa-clock-o"></i> Posted <?php echo date_format(date_create($post[0]['date_created']), 'jS F Y, g:ia'); ?></a>
                </div>
                <div class="forum-post-panel btn-group btn-group-xs">
				   <?php if ($this->session->userdata('logged_cc_login_id') != $post[0]['fp_author_id'] && $this->session->userdata('logged_crt_login_id') != $post[0]['fp_author_id']){ ?>
                  
                  	<a href="<?php echo base_url().'forum/reply/'.$this->uri->segment(3).'/'. $post[0]['fp_category_id'].'/'. $post[0]['fp_id'] ?>" class="btn btn-success"><i class="fa fa-mail-reply"></i> Reply</a>
                  	<?php /*?><button class="btn btn-danger"><i class="fa fa-warning"></i> Report post</button><?php */?>
			 	   <?php } ?>
                    
                </div>
                <!-- Post Info Buttons END -->
                
                <!-- Post Text -->
                <p><?php echo $post[0]['fp_content'] ?></p>
                <!-- Post Text EMD -->
                
                <?php if ($this->session->userdata('logged_cc_login_id') == $post[0]['fp_author_id'] || $this->session->userdata('logged_crt_login_id') == $post[0]['fp_author_id']){ ?>
                <div style="clear: both;">
                  <a href="<?php echo base_url().'forum/updatepost/'.$this->uri->segment(3).'/'. $post[0]['fp_category_id'].'/'. $post[0]['fp_id'] ?>" class="small"><i class="fa fa-pencil"></i> Edit</a> <span class="text-info">|</span> 
                  <a href="javascript: delete_post('<?php echo base_url().'forum/delete/'.$this->uri->segment(3).'/'. $post[0]['fp_category_id'].'/'. $post[0]['fp_id'] ?>');" class="small"><i class="fa fa-trash-o"></i> Delete</a>
                </div>
                
                <?php } ?>


              </div>
            </li>
            <!-- Forum Post END -->
            
            <?php
			if (count($post_reply) > 0){
				foreach($post_reply as $p=>$reply){ ?>
                
            <!-- Forum reply -->
            <li class="media well">
            
              <div class="user-info col-sm-2 col-xs-12" href="#">
                <strong><span class="text-info">
                	<?php
						$auth1 = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$reply['fp_author_id']));
						echo $this->master_model->decryptIt($auth1[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth1[0]['crt_last_name']);
					?>
                
                </span></strong><br />
                <small><?php if ($auth1[0]['org_id'] == '0') { echo 'Coordinator'; } else { echo 'Member'; } ?></small>


              </div>
              
              
              <div class="media-body col-sm-10 col-xs-12">
                <!-- Post Info Buttons -->
                <h3 style="margin-top: 0px;" class="hidden"><?php echo $reply['fp_title'] ?></h3>
                <div class="forum-post-panel btn-group btn-group-xs">
                  <button class="btn btn-default disabled"><i class="fa fa-clock-o"></i> Posted <?php echo date_format(date_create($reply['date_created']), 'jS F Y, g:ia'); ?></button>
                </div>
                
                <div class="forum-post-panel btn-group btn-group-xs">
                
				   <?php if ($this->session->userdata('logged_cc_login_id') != $reply['fp_author_id'] && $this->session->userdata('logged_crt_login_id') != $reply['fp_author_id']){ ?>
                   
                  	<a href="<?php echo base_url().'forum/reply/'.$this->uri->segment(3).'/'.$reply['fp_category_id'].'/'.$reply['post_parent'].'/'.$reply['fp_id'] ?>" class="btn btn-success"><i class="fa fa-mail-reply"></i> Reply</a>
                    
                   <?php } ?>
                </div>
                <!-- Post Info Buttons END -->
                
                
				<?php if ($reply['reply_parent'] != '' && $reply['reply_parent'] != '0'){
					
                    
                    $reply_parent = $this->master_model->getRecords('forum_post_master', array('fp_id'=>$reply['reply_parent']));
					$auth1 = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$reply_parent[0]['fp_author_id']));
                ?>
                
                    <!-- Post Text -->
                    <div class="thumbnail text-muted" style="padding: 10px;">
                        
                		<?php 
						echo '<p><b><i class="fa fa-user"></i> '.$this->master_model->decryptIt($auth1[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth1[0]['crt_last_name']).'</b></p>';

						echo  $reply_parent[0]['fp_content']; ?>        
                    </div>
				<?php } ?>
                    
                    
                <p><?php echo $reply['fp_content'] ?></p>
                <!-- Post Text EMD -->
                
                <?php if ($this->session->userdata('logged_cc_login_id') == $reply['fp_author_id'] || $this->session->userdata('logged_crt_login_id') == $reply['fp_author_id']){ ?>
                <div style="clear: both;">
                  <a href="<?php echo base_url().'forum/updatepost/'.$this->uri->segment(3).'/'.$reply['fp_category_id'].'/'.$reply['fp_id'] ?>" class="small"><i class="fa fa-pencil"></i> Edit</a> <span class="text-info">|</span> 
                  <a href="javascript: delete_reply('<?php echo base_url().'forum/delete/'.$this->uri->segment(3).'/'.$reply['fp_category_id'].'/'.$reply['fp_id'] ?>');" class="small"><i class="fa fa-trash-o"></i> Delete</a>
                </div>
                
                <?php } ?>
                
              </div>
            </li>
            <!-- Forum reply END -->
            			
			<?php
				}
			}
			 ?>
            
          </ul>
    </div>
	<!--./main_container-->

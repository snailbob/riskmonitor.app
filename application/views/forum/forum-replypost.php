

	<!--main_container-->
    <div class="container main_container" style="padding-bottom: 15px;">
    	<div class="row">
            <div class="col-xs-12">
            
                <h1 class="page-header">
                	<i class="fa fa-reply"></i> Reply Post
                
                    <a class="btn btn-default" href="<?php echo base_url().'forum/post/'.$this->uri->segment(3).'/'.$post[0]['fp_id']; ?>"><i class="fa fa-angle-double-left"></i> Back to post</a>
                
                </h1>
                
                
                <div class="form-group hidden">
                    <input type="text" class="form-control" id="post_title" placeholder="Post Title" value="x"/>
                    <input type="text" class="form-control" id="post_slogan" placeholder="Slogan" value=""/>
                    <input type="text" class="form-control" id="post_parent" value="<?php echo $post[0]['fp_id'] ?>"/>
                    <input type="text" class="form-control" id="post_level" value="1"/>
                    <input type="text" class="form-control" id="reply_parent" value="<?php echo $this->uri->segment(6); ?>"/>
                </div>
                
                <!-- Summernote CMS Example -->
                <div id="summernote"></div>
                
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right" id="submit_post" style="margin-top: 15px;">Post Reply</button>
                </div>

            </div><!--./col-xs-12-->
        </div><!--./row-->
        
        
    </div><!--./main_container-->


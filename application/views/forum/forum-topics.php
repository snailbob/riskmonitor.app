
	<?php
		$user_type = 'crt';
		if($this->session->userdata('logged_cc_login_id') != ''){
			$user_type = 'cc';
		}
	?>
	<!--main_container-->
    <div class="container main_container">

      <div class="page-header page-heading">
        <h1 ><a href="<?php echo base_url().'forum/index/'.$this->uri->segment(3); ?>" style="color: #000" class="pull-left">
	<?php  if(is_numeric($this->uri->segment(3))) {
			echo '<i>'.$scenario[0]['scenario'].'</i> ';
		} else{ 
			echo '<i>'.$scenario[0]['incident_name'].'</i> ';
   } ?>Forums</a>
      			<a href="<?php echo base_url().$user_type.'/dashboard'; ?>" class="pull-right btn btn-default"><i class="fa fa-angle-double-left"></i> Back to Dashboard</a>

        </h1>
        <ol class="breadcrumb pull-right where-am-i hidden">
          <li><span class="text-info">Forums</span></li>
          <li class="active">List of topics</li>
        </ol>
        <div class="clearfix"></div>
      </div>
      
	<?php if($this->session->flashdata('success')!="") { ?>
        <div class="col-lg-12">
            <div class="alert alert-success alert-dismissable">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
        </div>

	<?php }  if($this->session->flashdata('error')!="") { ?>
    
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
    
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
        
        </div>
     </div>

	<?php } ?>




      <div class="col-xs-12" style="padding-bottom: 20px; border-bottom: 1px solid #eee; margin-top:-20px;">
      	<h2><span class="small text-muted"><?php echo $fc_category[0]['fc_name'] ?></span>
        <a href="<?php echo base_url().'forum/addpost/'.$this->uri->segment(3).'/'.$fc_category[0]['fc_id']; ?>" class="btn btn-primary pull-right btn-lg visible-lg visible-md">Add Post</a><hr class="visible-sm visible-xs" />
        <a href="<?php echo base_url().'forum/addpost/'.$this->uri->segment(3).'/'.$fc_category[0]['fc_id']; ?>" class="btn btn-primary visible-sm visible-xs">Add Post</a>
</h2>
    
      </div>


      <table class="table forum table-striped">
        <thead>
          <tr>
            <th class="cell-stat hidden-xs"></th>
            <th>Topics</th>
            <th class="cell-stat text-center hidden-xs hidden-sm">Replies</th>
            <th class="cell-stat-2x text-center hidden-xs ">Author</th>
            <th class="cell-stat-2x hidden-xs hidden-sm">Last Post</th>
          </tr>
        </thead>
        <tbody>
        
        
		<?php if (count($posts) > 0){
            foreach ($posts as $p=>$psts){?>
				
          <tr>
            <td class="text-center hidden-xs ">
              <i class="fa fa-star text-info"></i>
            </td>

            <td>
              <h4><a href="<?php echo base_url().'forum/post/'.$this->uri->segment(3).'/'.$psts['fp_id']; ?>"><?php echo $psts['fp_title'] ?></a><br><small><?php echo substr($psts['fp_slogan'],0, 30); ?></small></h4>
            </td>

            <td class="text-center hidden-xs hidden-sm"><span class="text-info">
            	<?php
					$forum_post = $this->master_model->getRecords('forum_post_master', array('post_parent'=>$psts['fp_id']));
					echo count($forum_post);
				?>
            
            
            </span>
            </td>
            
            <td class="hidden-xs text-center">
            	<span class="text-info">
                	<?php
						$auth = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$psts['fp_author_id']));
						echo $this->master_model->decryptIt($auth[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth[0]['crt_last_name']);
					?>
                </span>
            </td>
            
            <td class="hidden-xs hidden-sm">
            	<span class="text-info">
                	<?php
						$auth = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$psts['fp_author_id']));
						echo $this->master_model->decryptIt($auth[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth[0]['crt_last_name']);
					?>
                </span>
                <br><small><i class="fa fa-clock-o"></i> <?php echo date_format(date_create($psts['date_created']), 'F d Y, g:ia') ?></small>
            </td>


          </tr>
		<?php	
            }
        }
		else{?>
			<tr><td></td><td colspan="4" class="center">No topics have been added yet.</td></tr>
		<?php }
		 ?>
                    
                    
        </tbody>
      </table>
      
      
      
    </div>
    </div>
	<!--./main_container-->

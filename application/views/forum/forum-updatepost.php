

	<!--main_container-->
    <div class="container main_container" style="padding-bottom: 15px;">
    	<div class="row">
            <div class="col-xs-12">
            
                <h1 class="page-header">
                	<i class="fa fa-pencil"></i> Update Post
                
                    <a class="btn btn-default" href="<?php echo base_url().'forum/post/'.$this->uri->segment(3).'/'.$post[0]['fp_id']; ?>"><i class="fa fa-angle-double-left"></i> Back to post</a>
                
                </h1>
                
                
                <div class="form-group <?php if ($post[0]['post_parent'] != '0') {echo 'hidden'; }?>">
                    <input type="text" class="form-control" id="post_title" placeholder="Post Title" value="<?php echo $post[0]['fp_title']; ?>"/>
                </div>
                
                <div class="form-group <?php if ($post[0]['post_parent'] != '0') {echo 'hidden'; }?>">
                    <input type="hidden" class="form-control" id="post_slogan" placeholder="Slogan" value="<?php echo $post[0]['fp_slogan']; ?>"/>
                    <input type="hidden" class="form-control" id="post_id" value="<?php echo $post[0]['fp_id']; ?>"/>
                    <input type="hidden" class="form-control" id="post_parent" value="<?php echo $post[0]['post_parent']; ?>"/>
                </div>
                
                <!-- Summernote CMS Example -->
                <div id="summernote"><?php echo $post[0]['fp_content']; ?></div>
                
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right" id="submit_postupdate" style="margin-top: 15px;">Update Post</button>
                </div>

            </div><!--./col-xs-12-->
        </div><!--./row-->
        
        
    </div><!--./main_container-->


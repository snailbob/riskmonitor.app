
	<?php
		$user_type = 'crt';
		if($this->session->userdata('logged_cc_login_id') != ''){
			$user_type = 'cc';
		}
	?>

	<!--main_container-->
    <div class="container main_container">

      <div class="page-header page-heading">
        <h1><a href="<?php echo base_url().'forum/index/'.$this->uri->segment(3); ?>" style="color: #000" class="pull-left">
	<?php  if(is_numeric($this->uri->segment(3))) {
			echo '<i>'.$scenario[0]['scenario'].'</i> ';
		} else{ 
			echo '<i>'.$scenario[0]['incident_name'].'</i> ';
   } ?>Forums</a>
   
   			<a href="<?php echo base_url().$user_type.'/dashboard'; ?>" class="pull-right btn btn-default"><i class="fa fa-angle-double-left"></i> Back to Dashboard</a>

        </h1>
        <ol class="breadcrumb pull-right where-am-i hidden">
          <li><span class="text-info">Forums</span></li>
          <li class="active">List of topics</li>
        </ol>
        <div class="clearfix"></div>
      </div>
      
      
      
	<?php if($this->session->flashdata('success')!="") { ?>
        <div class="col-lg-12">
            <div class="alert alert-success alert-dismissable">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
        </div>

	<?php }  if($this->session->flashdata('error')!="") { ?>
    
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
    
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?>
        
        </div>
     </div>

	<?php }  if(is_numeric($this->uri->segment(3))) { ?>
        <p class="lead">This is the place to discuss any ideas regarding the scenario <i><?php echo $scenario[0]['scenario']?></i>. Please follow the forum rules and always check FAQ before posting to prevent duplicate posts.</p>
   <?php } else{ ?>
        <p class="lead">This is the place to discuss any ideas regarding the incident <i><?php echo $scenario[0]['incident_name']?></i>. Please follow the forum rules and always check FAQ before posting to prevent duplicate posts.</p>
   <?php } ?>
      <table class="table forum table-striped">
        <thead>
          <tr>
            <th class="cell-stat"></th>
            <th>
              <h3>Important</h3>
            </th>
            <?php /*?><th class="cell-stat text-center hidden-xs hidden-sm">Topics</th><?php */?>
            <th class="cell-stat text-center hidden-xs hidden-sm">Posts</th>
            <th class="cell-stat-2x hidden-xs hidden-sm">Published</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-center"><i class="fa fa-question fa-2x text-primary"></i></td>
            <td>
              <h4><a href="<?php echo base_url().'forum/topics/'.$this->uri->segment(3).'/'.$fc_category[0]['fc_id']; ?>"><?php echo $fc_category[0]['fc_name'] ?></a><br><small><?php echo $fc_category[0]['fc_description'] ?></small></h4>
            </td>
          
          
            <td class="text-center hidden-xs hidden-sm"><span class="text-info">
            	<?php
					$forum_post1 = $this->master_model->getRecords('forum_post_master', array('fp_category_id'=>$fc_category[0]['fc_id'],'post_parent'=> '0','scenario_id'=>$this->uri->segment(3)));
					echo count($forum_post1);
				?>
            
            
            </span>
            </td>
            <td class="hidden-xs hidden-sm"> by
                <span class="text-info">
                	<?php
						$auth1 = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$this->session->userdata('team_cc')));
						echo $this->master_model->decryptIt($auth1[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth1[0]['crt_last_name']);
					?>
                </span>
                <?php /*?><br><small><i class="fa fa-clock-o"></i> <?php echo date_format(date_create($fc_category[0]['date_created'] ), 'jS F Y');?></small><?php */?>
            </td>
          </tr>
          <tr>
            <td class="text-center"><i class="fa fa-exclamation fa-2x text-danger"></i></td>
            <td>
              <h4><a href="<?php echo base_url().'forum/topics/'.$this->uri->segment(3).'/'.$fc_category[1]['fc_id']; ?>"><?php echo $fc_category[1]['fc_name'] ?></a><br><small><?php echo $fc_category[1]['fc_description'] ?></small></h4>
            </td>

            
            <td class="text-center hidden-xs hidden-sm"><span class="text-info">
            	<?php
					$forum_post2 = $this->master_model->getRecords('forum_post_master', array('fp_category_id'=>$fc_category[1]['fc_id'],'post_parent'=> '0','scenario_id'=>$this->uri->segment(3)));
					echo count($forum_post2);
				?>
            
            
            </span>
            </td>
            <td class="hidden-xs hidden-sm"> by
                <span class="text-info">
                	<?php
						$auth2 = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$this->session->userdata('team_cc')));
						echo $this->master_model->decryptIt($auth2[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth2[0]['crt_last_name']);
					?>
                </span>
                <?php /*?><br><small><i class="fa fa-clock-o"></i> <?php echo  date_format(date_create($fc_category[1]['date_created']), 'jS F Y'); ?></small><?php */?>
            </td>
          </tr>
        </tbody>
      </table>
      
      
      
      
      
      <table class="table forum table-striped">
        <thead>
          <tr>
            <th class="cell-stat"></th>
            <th>
              <h3>Open discussion</h3>
            </th>
            <th class="cell-stat text-center hidden-xs hidden-sm">Posts</th>
            <th class="cell-stat-2x hidden-xs hidden-sm">Published</th>
          </tr>
        </thead>
        <tbody>
        
	<?php if(!is_numeric($this->uri->segment(3))) {
		
		$i = 3;
		for($i; $i < 11; $i++){
		?>	
			
          <tr>
            <td class="text-center"><i class="fa fa-star-o fa-2x text-info"></i></td>
            <td>
              <h4><a href="<?php echo base_url().'forum/topics/'.$this->uri->segment(3).'/'.$fc_category[$i]['fc_id']; ?>"><?php echo $fc_category[$i]['fc_name'] ?></a><br><small><?php echo $fc_category[$i]['fc_description'] ?></small></h4>
            </td>

            
            <td class="text-center hidden-xs hidden-sm"><span class="text-info">
            	<?php
					$forum_post3= $this->master_model->getRecords('forum_post_master', array('fp_category_id'=>$fc_category[$i]['fc_id'],'post_parent'=> '0','scenario_id'=>$this->uri->segment(3)));
					echo count($forum_post3);
				?>
            
            
            </span>
            </td>
            <td class="hidden-xs hidden-sm"> by
                <span class="text-info">
                	<?php
						$auth2 = $this->master_model->getRecords('cf_crisis_response_team', array('login_id'=>$this->session->userdata('team_cc')));
						echo $this->master_model->decryptIt($auth2[0]['crt_first_name']).' '.$this->master_model->decryptIt($auth2[0]['crt_last_name']);
					?>
                </span>
                <?php /*?><br><small><i class="fa fa-clock-o"></i> <?php echo date_format(date_create($fc_category[$i]['date_created']), 'jS F Y') ?></small><?php */?>
            </td>
          </tr>
		<?php	
            }
        }
		else{?>
			<tr><td></td><td colspan="3" class="center">No topics have been added yet.</td></tr>
		<?php }
		 ?>
        </tbody>
      </table>
    </div>
    </div>
	<!--./main_container-->

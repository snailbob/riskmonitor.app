<video autoplay muted loop id="myVideo" poster="<?php echo base_url().'assets/frontpage/video/coordinate.png' ?>">
  <source src="<?php echo base_url().'assets/frontpage/video/crisisflo_background.mp4' ?>" type="video/mp4">
  <source src="<?php echo base_url().'assets/frontpage/video/crisisflo_background.webm' ?>" type="video/webm">
  <source src="<?php echo base_url().'assets/frontpage/video/crisisflo_background.ogv' ?>" type="video/ogg">
</video>

<header class="landing-header">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="header-lg-text">
                    <h1><?php echo $banners[0]['title']?></h1>
                    <p class="lead">
                    <?php echo $banners[0]['content']?>
                    </p>
                </div>
            </div>
        </div>

        <form action="<?php echo base_url().'signup'?>">

            <div class="row">
                <div class="col-sm-3 col-sm-offset-3">
                    <div class="form-group">
                        <input type="email" required class="form-control" name="email" id="" aria-describedby="helpId" placeholder="name@company.com" style="font-size: 18px; height: 45px; border-radius: 2px">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block btn-nm">
                            Try now for free
                        </button>
                        <p class="text-helper text-center text-white" style="margin-top: 5px">No Credit Card Required</p>
                    </div>
                </div>
            </div>


        </form>

    </div>


</header>




<div id="home-what" class="section" style="padding-top: 90px">
    <div class="container content-steps-v2">
        <div><?php echo $easiestway[0]['content']?></div>


        <div class="row">

            <div class="col-sm-3 step">

                <img src="<?php echo base_url().'uploads/avatars/'.$easiestways[0]['image'] ?>"
                     height="150"
                     alt="pic">

                <div class="description">
                    <h4 class="step-underline"><?php echo $easiestways[0]['title'] ?></h4>
                    <p><?php echo $easiestways[0]['content'] ?></p>
                </div>

            </div>

            <div class="col-sm-6">

                <div class="row">

                    <div class="col-sm-3 step arrow">
                        <img src="http://www.process.st/wp-content/themes/koombea/images/step-arrow.svg"
                             alt="An arrow facing right">
                    </div>

                    <div class="col-sm-6 step">


                        <img src="<?php echo base_url().'uploads/avatars/'.$easiestways[1]['image'] ?>"
                            height="150"
                            alt="pic">

                        <div class="description">
                            <h4 class="step-underline"><?php echo $easiestways[1]['title'] ?></h4>
                            <p><?php echo $easiestways[1]['content'] ?></p>
                        </div>


                    </div>

                    <div class="col-sm-3 step arrow">
                        <img src="http://www.process.st/wp-content/themes/koombea/images/step-arrow.svg"
                             alt="An arrow facing right">
                    </div>

                </div>

            </div>

            <div class="col-sm-3 step">

                <img src="<?php echo base_url().'uploads/avatars/'.$easiestways[2]['image'] ?>"
                     height="150"
                     alt="pic">

                <div class="description">
                    <h4 class="step-underline"><?php echo $easiestways[2]['title'] ?></h4>
                    <p><?php echo $easiestways[2]['content'] ?></p>
                </div>

            </div>

        </div>
        



    </div> <!-- /container -->
</div> <!-- /homepage-what -->





<div id="our-solutions" class="section" style="border-bottom: 1px solid #eee;" data-center="background-color:rgb(240, 247, 252);" data-center-top="background-color:rgb(255, 255, 255);">
    <div class="container content-benefits-v2">


        <div class="row centeredx our-boxes-holderx" style="margin-top: 0px;">
            <h1><?php echo $alternatingcontenthead[0]['title']?></h1>

            <h2 style="font-size: 21px;"><?php echo $alternatingcontenthead[0]['content']?></h2>

			<?php
				$alternatingcontentscount = 1;
				foreach($alternatingcontents as $r=>$value){
					$mod = $alternatingcontentscount%2;
					//left text
					if($mod == 1){ ?>


        
                        <div class="row benefit">

                            <div class="col-sm-6 image">

                                <img src="<?php echo base_url().'/uploads/avatars/'.$value['image'] ?>"
                                    width="390"
                                    alt="Anyone can create templates in seconds."
                                    title="Anyone can create templates in seconds.">

                            </div>

                            <div class="col-sm-6 description text-left">

                                <img src="<?php echo $value['icon'] ?>" alt="">
                                <h3 class="underline"><?php echo $value['title'] ?></h3>

                                <p>
                                    <?php echo $value['content'] ?>
                                </p>

                            </div>

                        </div>


					<?php
					}
					//right text
					else{ ?>


                    <div class="row benefit">

                        <div class="col-sm-6 description text-left">

                            <img src="<?php echo $value['icon'] ?>" alt="">
                            <h3 class="underline"><?php echo $value['title'] ?></h3>
                            
                            <p>
                                <?php echo $value['content'] ?>
                            </p>
                            

                        </div>

                        <div class="col-sm-6 image benefit-image">

                            <img src="<?php echo base_url().'/uploads/avatars/'.$value['image'] ?>"
                                width="100%"
                                alt="Everyone’s held to the same standards."
                                title="Everyone’s held to the same standards.">

                        </div>

                    </div>




                    <?php

					}

					if($alternatingcontentscount != count($alternatingcontents)){
				?>


        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->


            <?php
					}

					$alternatingcontentscount++;
				}
			?>




        </div><!-- /our-boxes-holder-->



    </div> <!-- /container -->
</div> <!-- /our-solutions -->



<div id="home-why" class="section" style="padding-bottom: 25px;" data-center="background-color:rgb(239, 239, 239);" data-center-top="background-color:rgb(255, 255, 255);">
    <div class="container">
        <h1><?php echo $collab[0]['title']?></h1>
        <?php echo $collab[0]['content']?>

        <div class="row">


        	<?php foreach($collabs as $r=>$value){ ?>
                <div class="col-md-4 col-sm-6 text-center">
                    <img src="<?php echo base_url().'uploads/avatars/'.$value['image'] ?>" /><br />
                    <h4 class="text-light-blue"><?php echo $value['title'] ?></h4>
                </div>

            <?php } ?>





        </div>


        <div class="row centered hidden">
            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-center.png">
                <h4>Crisis Command Centre</h4>
                <p>Coordinate crisis activities with ease and efficiency.</p>
            </div>

            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-incident.png">
                <h4>Pre-Incident Planning</h4>
                <p>Create crisis scenarios and assign tasks and responsibilities.</p>
            </div>

            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-communicate.png">
                <h4>Contact Management</h4>
                <p style="padding-bottom:20px">Easily maintain stakeholder contact details.</p>
            </div>

            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-anytime.png">
                <h4>Crisis Document Repository</h4>
                <p>Centrally store all your mission-critical crisis documents.</p>
            </div>

        </div> <!-- /row-->

    </div> <!-- /container -->
</div> <!-- /homepage-what -->


<div id="more-reasons" class="section" data-center="background-color:rgb(239, 239, 239);" data-center-top="background-color:rgb(255, 255, 255);">
    <div class="container">
        <h1><?php echo $threepartshead[0]['title']?></h1>

        <h2><?php echo $threepartshead[0]['content']?></h2>

        <div class="row centered">
        	<?php foreach($threeparts as $r=>$value){ ?>

            <div class="col-md-4 col-sm-12 item">
                <img src="<?php echo base_url().'uploads/avatars/'.$value['image']?>">
                <h4><?php echo $value['title'] ?></h4>
                <p><?php echo $value['content'] ?></p>
            </div>

			<?php } ?>

        </div> <!-- /row -->
            <?php /*?><div class="col-md-12 item">

            	<p class="text-center">
                    <a class="btn btn-primary btn-lg btn-rounded" href="#loginblue" data-toggle="modal" data-controls-modal="loginblue">Try now for 30 days</a>
                </p>
            </div><?php */?>
    </div> <!-- /container -->
</div> <!-- /more-reasons -->



<div id="pricing" class="section" data-center="background-color:rgb(239, 239, 239);" data-center-top="background-color:rgb(255, 255, 255);">
    <div class="container">

        <h1>Pricing</h1>


        <div id="generic_price_table">
            <div class="container-fluidx">

                <!--BLOCK ROW START-->
                <div class="row sm-gutter">
                    <div class="col-md-4">

                        <!--PRICE CONTENT START-->
                        <div class="generic_content clearfix">

                            <!--FEATURE LIST START-->
                            <div class="generic_feature_list">
                                <h2>
                                    FREE
                                    <br>
                                    <small style="font-size: 45%">&nbsp;</small>
                                </h2>

                                <!--BUTTON START-->
                                <div class="generic_price_btn clearfix">
                                    <a class="btn btn-primary btn-block" href="<?php echo base_url().'signup'; ?>">SIGN UP</a>
                                </div>
                                <!--//BUTTON END-->
                                <ul>
                                    <li>
                                        <span>Up to 3</span> Users
                                    </li>
                                    <li>
                                        <span>1</span> Open Case
                                    </li>
                                    <li>
                                        Group Messaging
                                    </li>
                                    <li>
                                        Wiki Space
                                    </li>
                                    <li>
                                        File Sharing up to 20 MB
                                    </li>
                                </ul>
                            </div>
                            <!--//FEATURE LIST END-->


                        </div>
                        <!--//PRICE CONTENT END-->

                    </div>
                    <div class="col-md-4">

                        <!--PRICE CONTENT START-->
                        <div class="generic_content generic_content_middle clearfix">

                            <!--FEATURE LIST START-->
                            <div class="generic_feature_list">
                                <h2>
                                    AUD$49 / MONTH
                                    <br>
                                    <small style="font-size: 45%">plus GST (if applicable)</small>
                                </h2>

                                <!--BUTTON START-->
                                <div class="generic_price_btn clearfix">
                                    <a class="btn btn-primary btn-block" href="<?php echo base_url().'signup?selected_plan=2'; ?>">SIGN UP</a>
                                </div>
                                <!--//BUTTON END-->

                                <ul>
                                    <li>
                                        <span>Up to 10</span> Users
                                    </li>
                                    <li>
                                        <span>Unlimited</span> Open Cases
                                    </li>
                                    <li>
                                        Group Messaging
                                    </li>
                                    <li>
                                        Wiki Space
                                    </li>
                                    <li>
                                        Video Conferencing
                                    </li>
                                    <li>
                                        Document Sharing
                                    </li>
                                    <li>
                                        Add Standby Templates
                                    </li>
                                    <li>
                                        Post Incident Review
                                    </li>
                                    <li>
                                        File Sharing up to 200 MB
                                    </li>
                                </ul>
                            </div>
                            <!--//FEATURE LIST END-->

                        </div>
                        <!--//PRICE CONTENT END-->

                    </div>
                    <div class="col-md-4">

                        <!--PRICE CONTENT START-->
                        <div class="generic_content clearfix">

                            <!--FEATURE LIST START-->
                            <div class="generic_feature_list">
                                <h2>
                                    AUD$79 / MONTH
                                    <br>
                                    <small style="font-size: 45%">plus GST (if applicable)</small>
                                </h2>

                                <!--BUTTON START-->
                                <div class="generic_price_btn clearfix">
                                    <a class="btn btn-primary btn-block" href="<?php echo base_url().'signup?selected_plan=3'; ?>">SIGN UP</a>
                                </div>
                                <!--//BUTTON END-->

                                <ul>
                                    <li>
                                        <span>Up to 30</span> Users
                                    </li>
                                    <li>
                                        <span>Unlimited</span> Open Cases
                                    </li>
                                    <li>
                                        Group Messaging
                                    </li>
                                    <li>
                                        Wiki Space
                                    </li>
                                    <li>
                                        Video Conferencing
                                    </li>
                                    <li>
                                        Document Sharing
                                    </li>
                                    <li>
                                        Add Standby Templates
                                    </li>
                                    <li>
                                        Post Incident Review
                                    </li>
                                    <li>
                                        File Sharing up to 500 MB
                                    </li>
                                </ul>
                            </div>
                            <!--//FEATURE LIST END-->

                        </div>
                        <!--//PRICE CONTENT END-->

                    </div>

                </div>
                <!--//BLOCK ROW END-->

            </div>
        </div>



    </div> <!-- /container -->
</div> <!-- /pricing -->





    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="buyNowModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Sign Up</h1>
                            <hr class="star-primary">

                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active">Details</li>
                                <li>Payment</li>
                                <li>Confirm</li>
                            </ul>


                            <div class="text-center hidden" style="padding-bottom: 25px;">
                                <div class="btn-group btn-group-justified signup_tab_btns">
                                  <a class="btn btn-success btn-xs active step-details-btn" role="button" data-toggle="tab" href="#details">Details</a>
                                  <a class="btn btn-default btn-xs step-payment-btn" role="button" data-toggle="tab" href="#payment">Payment</a>
                                  <a class="btn btn-default btn-xs step-confirm-btn" role="button" data-toggle="tab" href="#confirm">Confirm</a>
                                </div>
                            </div>


                            <form id="signup" class="text-left">


                                <div class="alert alert-danger hidden" style="margin-bottom: 15px; clear: both;"><span class="payment-errors"></span></div>


                                <!-- Tab panes -->
                                <div class="tab-content signup_tabs">
                                  <div class="tab-pane fade in active" id="details">


                                        <!-- Form Name -->
                                        <legend>Authorized Purchaser</legend>

                                      <div class="fields_to_clone">

                                        <div class="row">

                                        	<div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">First Name <span class="text-danger"><strong>*</strong></span></label>
                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name"/>
                                                </div>
                                            </div>

                                        	<div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Last Name <span class="text-danger"><strong>*</strong></span></label>
                                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name"/>
                                                </div>
                                            </div>
                                        </div>





                                        <div class="form-group">
                                            <label class="control-label">Business Name <span class="text-danger"><strong>*</strong></span></label>
                                            <input type="text" class="form-control" placeholder="Business Name" name="organization"/>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Business Address <span class="text-danger"><strong>*</strong></span></label>
                                            <input type="text" class="form-control user_geolocation" placeholder="Enter location" name="address"/>
                                            <input type="hidden" class="form-control" name="lat"/>
                                            <input type="hidden" class="form-control" name="lng"/>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Business Email <span class="text-danger"><strong>*</strong></span></label>
                                            <input type="text" class="form-control" placeholder="Business Email" name="email"/>
                                        </div>


                                        <div class="row">

                                        	<div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Country Calling Code</label>
                                                    <select class="form-control country_code_dd" name="country_code">
                                                    	<option value="" data-code="">Select..</option>
                                                        <?php
															foreach($mobile_code as $r=>$mc){
																echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
															}
														?>
                                                    </select>
                                                </div>
                                            </div>

                                        	<div class="col-sm-7">
                                                <div class="form-group mobile_input">
                                                    <label class="control-label">Mobile Number</label>
                                                    <div class="input-group">
                                                      <span class="input-group-addon addon-shortcode">+</span>
                                                      <input type="text" class="form-control" placeholder="Mobile Number" name="mobile">
                                                    </div>
                                                    <input type="hidden" name="country_short" value=""/>
                                                </div>
                                            </div>

                                        </div>
                                      </div><!--fields_to_clone-->

                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn-primary btn-rounded continue_btn">Continue <i class="fa fa-angle-double-right"></i></a>
                                        </div>
                                  </div><!--details tab-->

                                  <div class="tab-pane fade" id="payment">


                                        <div class="form-group hidden">
                                            <label class="control-label">Subscription Plan</label>
                                            <a href="#" class="btn btn-default btn-block disabled">CrisisFlo — Unlimited users — monthly subscription (US$<?php echo $admin_info[0]['single_rate']; ?>/user)</a>
                                            <select class="form-control" id="select_plan" name="plan">
                                                <option value="">Select..</option>
                                                <option value="1000" data-price="1000" selected="selected">CrisisFlo — Unlimited users — monthly subscription (US$<?php echo $admin_info[0]['single_rate']; ?>/user)</option>

                                                <?php

                                                if(count($stripe_plans->data) > 0) {
                                                    foreach($stripe_plans->data as $value) {
                                                        $total = number_format($value->amount / 100, 0);
                                                        $amount = $value->amount / 100;
														$unformat_total = $value->amount / 100; //sprintf('%0.2f', $amount);
                                                        $gst = $amount / 11;
                                                        $amount = $gst * 10;
                                                        $amount = number_format($amount, 2);





							                            echo '<option value="'.$value->id.'__'.$value->name.'__'.$amount.'__'.round($gst, 2).'__'.$total.'"';

                                                        if($amount == $price){
                                                            echo ' selected="selected"';
                                                        }
                                                        echo ' data-price="'.$total.'"';
                                                        echo '>'.$value->name .' (US$'.$total.')</option>';

                                                    }
                                                }
                                                ?>


                                            </select>

                                        </div>

                                        <div class="bill_infox hidden <?php // if($total_price == 0) { echo 'hidden'; } ?> ">
                                            <hr />
                                            <span class="text-center text-muted">
                                                You are paying in <strong class="text-primary">AUD$</strong>.  Your total charge is <strong class="text-primary">$<span class="the_total"><?php echo $total_price; ?></span></strong> ($<span class="thee_price"><?php echo $price; ?></span> Subscription Fee + $<span class="the_gst"><?php echo $the_gst; ?></span>, 10% GST).
                                            </span>

                                        </div>


                                        <hr />






                                        <div class="row credit_fields">


                                            <div class="col-sm-12">
                                                <ul class="list-inline pull-right">
                                                    <li><img src="<?php echo base_url().'assets/img/cards/visa.png' ?>" alt="" title="" /></li>
                                                    <li><img src="<?php echo base_url().'assets/img/cards/amex.png' ?>" alt="" title="" /></li>
                                                    <li><img src="<?php echo base_url().'assets/img/cards/mastercard.png' ?>" alt="" title="" /></li>

                                                </ul>
                                           		<span style="font-size: 16px; font-weight: bold">Payment</span>

                                                <div style="clear: both; margin-bottom: 15px;"></div>

                                                <div class="well" style="margin-bottom: 5px; padding-bottom: 0px;">

                                                    <div class="form-group">
                                                      <label><span>Card Number</span></label>
                                                      <input class="form-control" type="text" size="20" data-stripe="number"/>

                                                    </div>


                                                    <div class="row">

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                              <label><span>Expiration (MM)</span></label>
                                                              <input class="form-control" type="text" size="2" data-stripe="exp-month"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group">

                                                                <label><span>Expiration (YYYY)</span></label>
                                                                <input class="form-control" type="text" size="4" data-stripe="exp-year"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                            <label><span>Security Code <i class="fa fa-question-circle text-info security_info"></i></span></label>
                                                            <input class="form-control" type="text" size="4" data-stripe="cvc"/>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div><!--well-->

                                            </div>


                                            <div class="col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1">
                                                <p class="small text-muted text-center" style="margin: 0 auto;">
                                                <img class="pull-left" src="<?php echo base_url().'assets/img/lock-icon.png'?>" alt="" title="" style="margin-left: 10px;"/>
                                               <span class="text-left"> For your security, CrisisFlo does not store credit card details.<br /> Stripe is our secure payment provider. <a href="https://stripe.com" target="_blank">Find out more</a></span></p>

                                                <div id="security_info_content" class="hidden">
                                                    <img class="pull-right" src="<?php echo base_url().'assets/img/cards/visamastercard.png'?>" alt="" title=""/>
                                                    <p><strong>Visa &amp; MasterCard</strong><br />
                                                    The security code is the last 3 digits on the back of your card</p>
                                                    <hr />
                                                    <img class="pull-right" src="<?php echo base_url().'assets/img/cards/amexcard.png'?>" alt="" title=""/>
                                                    <p><strong>American Express</strong><br />
                                                    The security code is the 4 digit number printed on the front of your card</p>


                                                </div>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn-primary btn-rounded continue_btn" style="width: 112px;">Continue <i class="fa fa-angle-double-right"></i></a>
                                            <a href="#" class="pull-left btn btn-default btn-rounded payment_bck_btn" style="width: 112px;"><i class="fa fa-angle-double-left"></i> Back</a>
                                        </div>

                                  </div><!--payment tab-->

                                  <div class="tab-pane fade" id="confirm">

                                        <div class="form-group">
                                            <a href="#" class="btn btn-default btn-xs pull-right update_auth_btn">Update</a>
                                        	<h3>Authorized Purchaser Details</h3>
                                        	<table class="table table-hover table-striped auth_table">

                                            	<tbody>
                                                	<tr>
                                                    	<td>Purchaser Name</td>
                                                        <td>
                                                            <span class="coo_name"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Business Name</td>
                                                        <td>
                                                        	<span class="coo_bssname"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Business Address</td>
                                                        <td>
                                                        	<span class="coo_bssadd"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Business Email</td>
                                                        <td>
                                                        	<span class="coo_email"></span>
                                                        </td>
                                                    </tr>
                                                	<tr>
                                                    	<td>Mobile Number</td>
                                                        <td>
                                                            <span class="coo_mobile"></span>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>

                                            <div class="the_clone_authfields hidden"></div>


                                        </div>

                                        <div class="form-group hidden">
                                        	<h3 class="hidden">Subscription Plan</h3>
                                        	<table class="table table-hover table-striped">
                                            	<thead class="hidden">
                                                	<tr>
                                                    	<th>Description</th>
                                                        <th>Price</th>
                                                    </tr>
                                                </thead>
                                            	<tbody>
                                                	<tr>
                                                    	<td colspan="2">
                                                        	<div class="clone_selplan"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                        <p class="text-right"><small>By signing up, you agree to CrisisFlo's <a href="#" class="terms_btn">Terms of Use</a> and <a href="#" class="privacy_btn">Privacy Policy</a></small></p>

                                        <div class="form-group">

                                            <button class="btn btn-primary btn-rounded pull-right" style="width: 112px;">Submit</button>
                                            <a href="#" class="pull-left btn btn-default btn-rounded confirm_bck_btn" style="width: 112px;"><i class="fa fa-angle-double-left"></i> Back</a>
                                        </div>

                                  </div><!--confirm tab-->

                                </div>






                            </form>


                        </div><!--modal-body-->
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="loginblue" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">


            <nav class="navbar">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>  

                <div class="container-fluid">
                    <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img src="<?php echo base_url()?>assets/frontpage/corporate/images/crisisflo-logo-medium.png" alt="">
                    </a>
                    </div>
                </div>
            </nav>
  
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 new-modal-section">
                            <h1 class="text-left new-modal-title">Sign up for a 30 day trial</h1>
                    </div>
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 bg-grey">
                        <div class="modal-body">

                            <!-- <hr class="star-primary"> -->



                            <form id="trial_form" class="text-left">
                                <div class="alert trial_message alert-dismissable text-left" style="display: none;">
                                    Form successfully submitted.
                                </div>

                                <fieldset>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label">First Name</label>
                                                <input type="text" class="form-control" name="first_name"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label">Last Name</label>
                                                <input type="text" class="form-control" name="last_name"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Organization/Business Name</label>
                                        <input type="text" class="form-control organization_field" name="organization"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Business Email</label>
                                        <input type="text" class="form-control" name="email"/>
                                    </div>

                                    <div class="hidden">
                                        
                                        <div class="form-group">
                                            <label class="control-label">Domain <span class="the_domain_text"></span></label>
                                            <div class="input-group">
                                            <input type="text" class="form-control domain_field" name="domain"/>
                                            <span class="input-group-addon" id="basic-addon2">.crisisflo.com</span>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" class="form-control geocomplete" placeholder="Enter location" name="address"/>
                                            <input type="hidden" class="form-control" name="lat"/>
                                            <input type="hidden" class="form-control" name="lng"/>
                                        </div>




                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Country Calling Code</label>
                                                    <select class="form-control" name="country_code">
                                                        <option value="" data-code="">Select..</option>
                                                        <?php
                                                            foreach($mobile_code as $r=>$mc){
                                                                echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="form-group mobile_input">
                                                    <label class="control-label">Mobile Number</label>
                                                    <div class="input-group">
                                                    <span class="input-group-addon addon-shortcode">+</span>
                                                    <input type="text" class="form-control" placeholder="Mobile Number" name="mobile">
                                                    </div>
                                                    <input type="hidden" name="country_short" value=""/>
                                                </div>
                                            </div>
                                        </div>

                                    </div> 



                                    <div class="form-group hidden">
                                        <label class="control-label">Subscription Plan</label>
                                        <input type="hidden" name="plan" value="5"/>
                                    </div>


                                    <p class="text-right"><small>By signing up, you agree to CrisisFlo's <a href="#" class="terms_btn">Terms of Use</a> and <a href="#" class="privacy_btn">Privacy Policy</a></small></p>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-radius btn-block">
                                            Sign Up <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
                                        </button>
                                    </div>

                                </fieldset>

                            </form>




                        </div><!--modal-body-->
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->

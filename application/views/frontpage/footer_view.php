
    <div id="footer">
        <div class="container">

            <div id="footer-bottom" class="row">

                <div id="copyright">
                    <ul>
                        <li class="text">Risk Monitor Pty Ltd</li>
                    </ul>
                    <ul>
                        <li>
                            <a target="_self" href="<?php echo base_url()?>about">About</a>
                        </li>
                        <li>
                            <a target="_self" href="<?php echo base_url()?>contact">Contact Us</a>
                        </li>
                        <li>
                            <a target="_self" href="<?php echo base_url().'faq'?>">FAQ</a>
                        </li>
                        <li>
                            <a href="#" class="terms_btn">Terms of Use</a>
                        </li>
                        <li>
                            <a href="#" class="privacy_btn">Privacy Policy</a>
                        </li>
                    </ul>

                    <div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /container -->
    </div>
    <!-- /footer -->


    <div id="top-button" class="animated fadeInUp" data-0="display: none" data-100="display: block">
        <img src="<?php echo base_url()?>assets/frontpage/corporate/images/top-button.png" />
    </div>




    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="menuModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal" style="left: 0; top: 10px;">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row" style="margin-top: 70px">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">
                            <ul class="list-unstyled text-center" style="margin-top: 150px;">
                                <li>
                                    <a href="<?php echo base_url()?>about" class="<?php if($this->uri->segment(1)==" about "){echo "on-page ";} ?>">
                                        <p class="lead">
                                            <b>About Us</b>
                                        </p>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>contact" class="<?php if($this->uri->segment(1)==" contact "){echo "on-page ";} ?>">
                                        <p class="lead">
                                            <b>Contact Us</b>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!--modal-body-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--portfolio-modal-->


    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>CrisisFlo - Terms of Use</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div>
                        <!--modal-body-->
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--portfolio-modal-->


    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div>
                        <!--modal-body-->
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--portfolio-modal-->


    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>


	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.geocomplete/jquery.geocomplete.js"></script>


	<script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	<script>
		// This identifies your website in the createToken call below
		var pubkey = 'pk_test_2g76COgjgp2DPnHPHdxrRCZc'; //(base_url == 'https://www.crisisflo.com/x') ? 'pk_test_qLvqgzEetIhJZnK3oSl9HrTT' : 'pk_test_2g76COgjgp2DPnHPHdxrRCZc';
		Stripe.setPublishableKey(pubkey);
		// ...

		function stripeResponseHandler(status, response) {
		    var $form = $('#signup');

		    if (response.error) {
		        // Show the errors on the form
		        $form.find('.payment-errors').text(response.error.message).closest('div').removeClass('hidden');
		        $form.find('button').html('Submit').prop('disabled', false);
		        return false;
		    } else {
		        // response contains id and card, which contains additional card details
		        var token = response.id;
		        $form.find('.payment-errors').closest('div').addClass('hidden');
		        // Insert the token into the form so it gets submitted to the server
		        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
		        // and submit


		        var data = {
		            stripeToken: token,
		            //				amount: $('[name="amount"]').val(),
		            //				nexmo_balance: $('[name="nexmo_balance"]').val()
		        };

		        data = $form.serialize() + '&' + $.param(data);
		        console.log(data);

		        $('.step-confirm-btn').click();
		        $('.step-confirm-btn').addClass('active').siblings().removeClass('active');
		        $('#progressbar').children('.active').removeClass('active').addClass('done').next().addClass('active');



		        return false;

		        //$form.get(0).submit();
		    }
		};
		//end of sign up subscription
	</script>



    <?php if (isset($_GET['stripe_i']) && isset($_GET['stripe_e'])) {
		$cust_id = $_GET['stripe_i'];
		$cust_email = $_GET['stripe_e'];
		$stripe_cust = $this->master_model->getRecords('stripe_customers',array('id'=>$cust_id));

		if(count($stripe_cust) > 0 && md5($stripe_cust[0]['email']) == $cust_email){
			$the_cust_email = $stripe_cust[0]['email'];
			$cust_data =  unserialize($stripe_cust[0]['pre_selected_plan_data']);// unserialize($stripe_cust[0]['cust_data']);
			//$subs = $cust_data->data;
			$sub_id = $stripe_cust[0]['email']; //$cust_data->subscriptions->id;
			$subs_name = '';
			$subs_plan_id = '';
			$subs_amount = '';

			if($cust_data !=''){
					$subs_name = $cust_data->name;
					$subs_plan_id = $stripe_cust[0]['pre_selected_plan_id']; //$sub->plan->id;
					$subs_amount = number_format($cust_data->amount / 100, 2);
					$subs_amount2 = $cust_data->amount;
			}

	?>

    <button id="customButton" class="hidden">Purchase</button>
    <script src="https://checkout.stripe.com/checkout.js"></script>

	<script>
        $(document).ready(function (e) {
            var handler = StripeCheckout.configure({
                key: 'pk_test_2g76COgjgp2DPnHPHdxrRCZc', //pk_test_qLvqgzEetIhJZnK3oSl9HrTT', //pk_test_qLvqgzEetIhJZnK3oSl9HrTT
                image: '<?php echo base_url(); ?>assets/img/favicon-lg.png',
                token: function (token) {
                    // Use the token to create the charge with a server-side script.
                    // You can access the token ID with `token.id`
                    //  alert(token.email);
                    $.ajax({
                        type: "POST",

                        url: base_url + 'adminstripe/subscribe_user',

                        data: {
                            token_id: token.id,
                            token_email: token.email,
                            subs_plan_id: "<?php echo $subs_plan_id; ?>",
                            cust_id: "<?php echo $cust_id; ?>"
                        },

                        success: function (data) {
                            console.log(data);
                            // window.location.reload(true);
                            if (data == 'success') {
                                bootbox.alert('Subscription successful! Please wait shortly until admin verify and activate your account.');

                                //modl;
                            } else {
                                bootbox.alert('Something went wrong! Please contact admin.');
                            }
                        }

                    }); //ajax

                }
            });


            $('#customButton').on('click', function (e) {
                // Open Checkout with further options
                handler.open({
                    name: 'CrisisFlo',
                    description: '<?php echo $subs_name; ?> ($<?php echo $subs_amount; ?>)',
                    plan: '<?php echo $sub_id; ?>',
                    amount: '<?php echo $subs_amount2; ?>',
                    email: '<?php echo $the_cust_email; ?>'
                });
                e.preventDefault();
            });

            // Close Checkout on page navigation
            $(window).on('popstate', function () {
                handler.close();
            });

            $('#customButton').click();
        });
        //end of subscription request

    </script>



    <?php
		}
	} ?>


</body>
</html>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">


    <title><?php echo $this->common_model->appName(); ?> | <?php echo 'Cloud-based product recall management software'; ?> </title>



<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">

<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/2/css/bootstrap.cosmo-form.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontpage/corporate/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>

<script>
	var base_url = '<?php echo base_url(); ?>';
</script>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<![endif]-->

</head>
 <body id="skrollr-body" class="hidden root home landing">

    <div id="browser-detection" class="alert alert-warning" style="display:none; position:relative;">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		Your browser is not supported by CrisisFlo. For a better experience, use one of our <a class='preventDefault' data-toggle='modal' data-target='#modal-browser-options'>supported browsers.</a>
	</div>
	<div id="cookies-detection" class="alert alert-danger" style="display:none; position:relative;">
		*__browser_cookies_disabled*
	</div>
    <nav id="my-menu">
       <ul>
          <li class="hidden"><a href="<?php echo base_url()?>">Home</a></li>
          <?php /*?><li><a href="<?php echo base_url()?>faq">FAQ</a></li<?php */?>
          <li><a href="<?php echo base_url()?>about">About Us</a></li>
          <li><a href="<?php echo base_url()?>contact">Contact</a></li>
          <li class="hidden"><a href="<?php echo base_url()?>blog">Blog</a></li>
       </ul>
    </nav>

    <div id="main-container">
    <div class="mobile-spacer visible-xs hidden-sm hidden-md hidden-lg" data-0="margin-top:120px" data-1="margin-top:60px"></div>

      <!-- Static navbar -->
      <div class="navbar navbar-default navbar-static-top" role="navigation">
       	<div class="container">
            <div class="navbar-header">
              <a id="open-sliderx" class="navbar-toggle" data-0="top:0" data-1="top:-3px" data-toggle="modal" href="#menuModal" data-controls-modal="menuModal">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="navbar-brand">
              	<a href="<?php echo base_url()?>"><img id="nav-logo" class="animated fadeIn animated-top" data-0="display: inline-block" src="<?php echo base_url()?>assets/2/img/logo.png"/></a>
                <div class="mobile-menu visible-xs">



                </div>
              </div>
            </div>
            <?php if($this->uri->segment(2) != 'toshare') { ?>
              <div class="navbar-collapse collapse">
                <div class="mobile-menu hidden-xs text-right" style="padding-top: 35px;">

                  <?php if ($this->session->userdata('logged_display_name') != '') {?>
                    <a href="<?php echo base_url()."signin"; ?>" id="btn-mini-login" class="hidden btn btn-primary btn-xs btn-rounded btn-outline btn-text-white"><?php echo $this->session->userdata('logged_display_name') ?></a>
                  <?php } else {?>
                    <a href="<?php echo base_url()."signin"; ?>" id="btn-mini-login" class="hidden btn btn-primary btn-xs btn-rounded btn-outline btn-text-white" >Get Started for FREE</a>
            
                    <a href="<?php echo base_url()."signin"; ?>" id="btn-mini-login" class="hidden btn btn-primary btn-xs btn-rounded btn-outline btn-text-white" >Log In</a>
                  <?php } ?>

                </div>

                <?php if ($this->session->userdata('logged_display_name') != '') {?>
                  <div class="hidden-xs">
                    <a href="<?php echo base_url()."signin"; ?>" class="pull-right btn btn-default btn-outline" style="margin-top: -6px;">
                      <?php echo $this->session->userdata('logged_display_name') ?>
                    </a>
                  </div>
                <?php } else {?>
                  <div class="hidden-xs pull-right text-right" style="margin-top: -10px;"
                  style="margin-top: -6px;">

                    <a href="<?php echo base_url()."signin"; ?>" class="btn btn-default btn-clear btn-lg">
                      Log In
                    </a>
                    <a href="<?php echo base_url()."signup"; ?>" class="btn btn-default btn-outline btn-lg">
                      Get Started for FREE
                    </a>

                  </div>
                <?php } ?>
              </div><!--/.nav-collapse -->
            <?php } ?>

   	 	</div> <!-- /container -->
      </div>

<div id="home-what" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            
                <h1>
                    <?php echo $title?>
                </h1>

                <div class="well text-left">
                <form method="post" id="incident_form">
                        <input type="hidden" name="cc_id" value="<?php echo $this->uri->segment(3); ?>">
                        <?php
                            $count = 1;
                            foreach($contents as $r=>$value){
                                if(count($value['field'])){
                                    foreach($value['field'] as $fr=>$fvalue){
                                    ?>
                                        <div class="form-group">
                                            <label for=""><?php echo $fvalue['text']?></label>

                                            <?php if ($fvalue['type'] == 'single') {?>
                                            <input type="text" class="form-control" name="<?php echo 'input'.$fvalue['id']; ?>">
                                            <?php } if ($fvalue['type'] == 'paragraph') {?>
                                            <textarea rows="3" class="form-control" name="<?php echo 'input'.$fvalue['id']; ?>"></textarea>
                                            <?php } ?>
                                        </div>
                                    <?php
                                    }
                                }
                                $count ++;
                            }
                        ?>
                        <div class="form-group">
                            <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6Lfd0mMUAAAAAB3S-9qLYZJFjU78G6TSaNbvsw7B"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" disabled id="inci_submit_btn">
                                Submit
                            </button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->
</div>
<!-- /homepage-what -->

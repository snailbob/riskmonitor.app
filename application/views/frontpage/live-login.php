<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CrisisFlo | Cloud-based product recall management system</title>

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
	<style type="text/css">
        body, html{
            margin: 0;
			padding: 0;
			height: 100%;
			overflow: hidden;
        }

		iframe{
			border: 0;
			position:fixed;
			top:0;
			left:0;
			right:0;
			bottom:0;
			width:100%;
			height:100%
		}
    </style>
</head>

<body>

	<iframe width="100%" height="100%" id="i1" src="https://www.crisisflo.com/signin"></iframe>

</body>
</html>
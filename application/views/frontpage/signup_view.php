<div id="home-what" class="section" >
    <div class="container">
        <h1><?php echo $page_title; ?></h1>
        
        <div class="row">
        	<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
     
             
                  <div class="panel panel-default">
                      <div class="panel-body text-left">
                            <form id="signup">
                            	<h3>Personal Information</h3>
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input type="text" class="form-control" name="first_name"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" class="form-control" name="last_name"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Business Email</label>
                                    <input type="text" class="form-control" name="email"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <input type="text" class="form-control user_geolocation" placeholder="Enter location" name="address"/>
                                    <input type="hidden" class="form-control" name="lat"/>
                                    <input type="hidden" class="form-control" name="long"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Organization/Business Name</label>
                                    <input type="text" class="form-control" name="organization"/>
                                </div> 
                                             
                                <div class="form-group">
                                    <label class="control-label">Subscription Plan</label>
                                    <select class="form-control" name="plan">
										<option value="">Select..</option>
										<?php
                    
                                        if(count($stripe_plans->data) > 0) {
                                            foreach($stripe_plans->data as $value) {
												$total = number_format($value->amount / 100, 2);
												$amount = $value->amount / 100;
												$gst = $amount / 11;
												$amount = $gst * 10;
												$amount = number_format($amount, 2);
												echo '<option value="'.$value->id.'__'.$value->name.'__'.$amount.'__'.round($gst, 2).'__'.$total.'"';
												
												if($amount == $price){
													echo ' selected="selected"';
												}
												
												echo '>'.$value->name .' (AUD$'.$total.')</option>';
												
											}
										}
										?>


                                    </select>
                                </div> 
                                
                                <div class="bill_info <?php if($total_price == 0) { echo 'hidden'; } ?> ">
                                    <hr />
                                    <span class="text-center text-muted">
                                        You are paying in <strong class="text-primary">AUD$</strong>.  Your total charge is <strong class="text-primary">$<span class="the_total"><?php echo $total_price; ?></span></strong> ($<span class="the_price"><?php echo $price; ?></span> Subscription Fee + $<span class="the_gst"><?php echo $the_gst; ?></span>, 10% GST).
                                    </span>
                                
                                </div>
                                
                                <hr />
                                <h3>Payment <small>Visa, MasterCard, or American Express</small></h3>
                                
                                                    
                                <div class="row credit_fields">
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger hidden"><span class="payment-errors"></span></div>
                                    </div>
                                    
                                    <div style="clear: both; margin-bottom: 15px;"></div>
                                    
                                    <div class="col-sm-12">
                                        <div class="well">
                                                    
                                            <div class="form-group">
                                              <label><span>Card Number</span></label>
                                              <input class="form-control" type="text" size="20" data-stripe="number"/>
                                            
                                            </div>
                                            
                                            
                                            <div class="row">
                                                
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                      <label><span>Expiration (MM)</span></label>
                                                      <input class="form-control" type="text" size="2" data-stripe="exp-month"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label><span>Expiration (YYYY)</span></label>
                                                        <input class="form-control" type="text" size="4" data-stripe="exp-year"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                    <label><span>CVC</span></label>
                                                    <input class="form-control" type="text" size="4" data-stripe="cvc"/>
                                                    </div>
                                                </div>
                                
                                            </div>
                                                                
                                        
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                                                            
                                             
                                <div class="form-group">
                                    <button class="btn btn-primary btn-rounded">Signup</button>
                                </div> 

                                             
                            </form>
                      </div>
                  </div>
        
     
     
            
            </div>
        </div>
        

        
       
        
        
        
        
    </div> <!-- /container -->
</div> <!-- /homepage-what -->

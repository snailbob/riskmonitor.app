                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Request Customer Activation

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                	<a href="<?php echo base_url().'members'?>">Portal</a></li>

                                <li class="active">Request Customer Activation</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Request Activation</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        
                        <div class="form-group">


                            <label class="col-sm-2 control-label">Organization</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_org" name="cc_org" placeholder=""   data-msg-required="Please enter zipcode" value="<?php echo set_value('cc_org'); ?>"><?php echo form_error('cc_org'); ?>

                            </div>


                        </div><!--.add org-->



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">Modules</label>

                            <div class="col-sm-10">

                              <div class="checkbox">
                                <label for="checkboxes-0">
                                  <input type="checkbox" name="modules[]" id="checkboxes-0" value="1">
                                  Standard CrisisFlo
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-1">
                                  <input type="checkbox" name="modules[]" id="checkboxes-1" value="2">
                                  Case Management
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-2">
                                  <input type="checkbox" name="modules[]" id="checkboxes-2" value="3">
                                  Video Conferencing
                                </label>
                              </div>
                              <?php echo form_error('modules[]'); ?>
                            </div>

                        </div>


                        
                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-md-2 control-label" for="radios">Customer Type</label>
                          <div class="col-md-10">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0">
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1">
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>
                        
                        
                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Preload Input Pack (Optional)</label>

                            <div class="col-sm-10">
                               <select class="form-control" name="input_packs" id="input_packs">

                                    <option value="0">Select</option>

                                    <?php

									if(count($input_packs)>0)

									{

										foreach($input_packs as $packs)

										{

									?>

                                    <option value="<?php echo $packs['id']; ?>"> <?php echo $packs['name']; ?> </option>

                                    <?php

										}

									} 

									?>

                               </select>

                            </div>

                        </div>
                        
                        
                        
                        
                                 <!-- Multiple Radios (inline) -->
                        <div class="form-group">
                          <label class="col-md-2 control-label" for="radios">Assign to: </label>
                          <div class="col-md-10"> 
                            <label class="radio-inline" for="c_indi">
                              <input type="radio" name="radios" id="c_indi" value="1" checked="checked">
                              Existing CC
                            </label> 
                            <label class="radio-inline" for="c_group">
                              <input type="radio" name="radios" id="c_group" value="2">
                              New CC
                            </label> 

                          </div>
                        </div>
   
            
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    <div class="share_indi" style="display:block;">                                                

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Select CC</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="sel_cc" id="sel_cc">

                                    <option value="">Select</option>

                                    <?php

									if(count($cc_list)>0)

									{

										foreach($cc_list as $cc)

										{

									?>

                                    <option value="<?php echo $cc['login_id'].' '.$this->master_model->decryptIt($cc['crt_email']).' '.$this->master_model->decryptIt($cc['crt_first_name']).' '.$this->master_model->decryptIt($cc['crt_last_name']); ?>" <?php if ($this->uri->segment(4) == $cc['login_id']) { echo 'selected="selected"'; }?>>

										<?php echo $this->master_model->decryptIt($cc['crt_first_name']).' '.$this->master_model->decryptIt($cc['crt_last_name']); ?>

                                    </option>

                                    

                                    <?php

										}

									} 

									?>

                               </select><?php echo form_error('sel_cc'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10 add_orgzz_btn">

                            <button type="submit" class="btn btn-primary" name="add_orgzz" id="add_orgzz">Submit</button>

                            </div>

						</div>
                    </div>

										
                        
                    <div class="share_group" style="display:none;">                                                
                        
                        
                         <div class="form-group" style="border-top: 1px #eee solid; padding-top: 15px;"><!--add org-->

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_firstname" name="cc_firstname" placeholder="" data-msg-required="Please enter first name" value="<?php echo set_value('cc_firstname'); ?>"><?php echo form_error('cc_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cc_lastname" name="cc_lastname" placeholder="" data-msg-required="Please enter last name" value="<?php echo set_value('cc_lastname'); ?>"><?php echo form_error('cc_lastname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="organisation" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">

                            <input type="email" class="form-control" id="cc_email" name="cc_email" placeholder="" data-msg-required="Please enter email"><?php echo form_error('cc_email'); ?>

                        	</div>

						</div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['calling_code'].'"';
                                        

                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                            <input type="text" class="form-control" id="cc_phone_number" name="cc_phone_number" value="<?php echo set_value('cc_phone_number'); ?>" data-msg-required="Please enter first name"><?php echo form_error('cc_phone_number'); ?>

                            </div>

                        </div>

                        
                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_address" name="cc_address" placeholder="" data-msg-required="Please enter address" value="<?php echo set_value('cc_address'); ?>"><?php echo form_error('cc_address'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">City</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_city" name="cc_city" placeholder=""  data-msg-required="Please enter city" value="<?php echo set_value('cc_city'); ?>"><?php echo form_error('cc_city'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">State</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_state" name="cc_state" placeholder=""  data-msg-required="Please enter state" value="<?php echo set_value('cc_state'); ?>"><?php echo form_error('cc_state'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Zipcode</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_zip_code" name="cc_zip_code" placeholder=""  data-msg-required="Please enter zipcode" value="<?php echo set_value('cc_zip_code'); ?>"><?php echo form_error('cc_zip_code'); ?>

                            </div>

                        </div>

                         <div class="form-group">

                            <label class="col-sm-2 control-label">User Type</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="cc_type" id="cc_type">

                                    <option value="">Select</option>

                                    <option value="demo" <?php if($this->input->post('cc_type')=='demo'){echo 'selected="selected"';} ?>>Demo</option>

                                    <option value="live" <?php if($this->input->post('cc_type')=='live'){echo 'selected="selected"';} ?>>Live</option>

                               </select>

                            </div>

                        </div>
                        





                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_cc" id="add_cc">Submit</button>

                            </div>

                        </div>

                        

                    </div><!---toggle.hide-->

                    </form>									

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




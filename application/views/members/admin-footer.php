            </div>

        </div>

    </div>
    <!-- /.container -->


    <hr />
    <div class="container">

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-muted">© 2014 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>
    <!--<script src="<?php echo base_url()?>assets/2/js/plugins/popover-extra-placements/popover-extra-placements.js"></script>-->

    <script src="<?php echo base_url()?>assets/2/js/admin-validation.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                viewports: {
                    progress: ".pwstrength_viewport_progress",
                    verdict: ".pwstrength_viewport_verdict"
                }
            };
            options.common = {
                onLoad: function () {
                    $('#messages').text('Start typing password');
                },
                zxcvbn: true
            };
            $(':password').pwstrength(options);
        });
		
		
		//validation for chandge password add new cc
	    $("#change_cc_pw").click(function()
		{ 
			var email=$("#email");
			var new_pass=$("#new_pass");
			var confirm_pass=$("#confirm_pass");
			
			$('#new_pass').removeClass('error_border');
			$('#confirm_pass').removeClass('error_border');
			
			$('#new_pass').addClass('form-control');
			$('#confirm_pass').addClass('form-control');
			
			if ($('.password-verdict').html()!="Medium" && $('.password-verdict').html()!='Strong' && $('.password-verdict').html()!='Very Strong' ){
				
				$('.mediumerror').html('For security reasons, your password needs to be ‘Medium’ strength or above.');

				$('#cc_password').focus();
				return false;
			}
			
			else{
				$('.mediumerror').html('');
			}
			
		     
			if(new_pass.val()=="")
            { 
			    new_pass.val('');
				$('#new_pass').attr('placeholder','Please Add New Password');
				$('#new_pass').removeClass('form-control');
				$('#new_pass').addClass('error_border');
				$('#new_pass').focus();
				return false;
			
			}
			else  if(confirm_pass.val()=="")
            { 
			    confirm_pass.val('');
				$('#confirm_pass').attr('placeholder','Please Confirm Password');
				$('#confirm_pass').removeClass('form-control');
				$('#confirm_pass').addClass('error_border');
				$('#confirm_pass').focus();
				return false;
			
			}
	
		});
		
		
		
		
    </script>

		<?php if ($this->uri->segment(2) == 'packs' && $this->uri->segment(3) == 'view' && $this->uri->segment(5) != ''  ){ ?>
    <script>
		$( document ).ready(function() {
		
			var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close hidden" id="hide_x" data-dismiss="modal" aria-hidden="true">&times;</button><p class="lead" id="hide_spin">';
			
			var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Your file is being analysed.</p><p id="scan_result">';
			
			var myModal3 = '</p></div></div></div> ';

			var myModal = myModal1 + myModal2 + myModal3;
			
			$(myModal).modal('show');
			
			var file_id = <?php echo $this->uri->segment(5); ?>;
			$.ajax({
				  type: "POST",
				  
				  url: "<?php echo base_url().'webmanager/packs/scan_response'; ?>",
				  
				  data: { file_id: file_id },
				  
				  success: function(data) {
					console.log(<?php echo $this->uri->segment(5); ?>);
					  $('#scan_result').html(data);
					  $('#hide_spin').addClass('hidden');
					  $('#hide_x').removeClass('hidden');
					  
				  }
				  
			});
			
				
		});

    </script>
		<?php
		}
		?>




    <script>
	
	$('.common_msg_click').on('click', function ( e ) { 
				$.fn.custombox( this );
				e.preventDefault();
	});
	

    </script>
</body>

</html>

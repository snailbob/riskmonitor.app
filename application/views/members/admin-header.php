<?php if(!isset($_SESSION['logged_member'])){redirect(base_url().'members');} ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CrisisFlo | Member's Portal</title>

    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->

    <!--<link href="<?php echo base_url()?>assets/2/css/plugins/pace/pace.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/2/js/plugins/pace/pace.js"></script>-->

    <!-- Bootstrap core CSS -->
    <?php /*?> <link href="<?php echo base_url()?>assets/2/css/easyui.css" rel="stylesheet"> <?php */?> 
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    <!--[if lt IE 9]>

      <script src="<?php echo base_url() ?>assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url() ?>assets/2/js/respond.min.js"></script>

    <![endif]-->


	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    
    
</head>

<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-xs preloadThis" href="<?php echo base_url()?>members/safety"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="202" height="61" style="margin-top: -20px;"></a>
                <a class="navbar-brand visible-xs preloadThis" href="<?php echo base_url()?>members/safety"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" width="161" height="48" style="margin-top: -15px;"></a>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 hidden-xs" >
                <h3 class="big text-primary" style="text-align: center">Member's Portal</h3>
            </div>

            <div class="navbar-header col-lg-8 col-lg-offset-2 col-md-8 col-lg-md-2 visible-xs" >
                <h4 class="big text-primary" style="text-align: center">Member's Portal</h4>
            </div>

			<?php /*GET THE DATA FROM THE SESSION**************************/

				//active case report count
				$num_report=$this->master_model->getRecordCount('case_master',array('crt_id'=>$this->session->userdata('logged_crt_login_id'),'cc_id'=>$this->session->userdata('logged_parent_crt'),'org_id'=>$this->session->userdata('crt_selected_orgnaization'),'status'=>'0'));
    			
				
				$res_fname = $_SESSION['mfirst_name'];
				$res_lname = $_SESSION['mlast_name'];
	
	
				$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>'); 

            /*********************************************************/?>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#"></a>
                    </li>
                   <li class="page-scroll"> <a href="<?php echo base_url() ?>members" class="visible-xs  active"><i class="fa fa-bullseye text-muted"></i> Portal</a></li>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>coordinator/manage" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-user"></i> Manage CC</a></li>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>organization/manage" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-building-o"></i> Manage Organization</a></li>

                   <li class="page-scroll"> <a href="<?php echo base_url()?>smscenter/manage" class="visible-xs <?php if($this->uri->segment(3)=="managecrt"){echo "active";}?>"><i class="fa fa-mobile"></i> SMS Center</a></li>
                   
                   
                    <li class="dropdown hidden-sm hidden-xs">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>members/settings/changepassword"><i class="fa fa-key"></i> Change Password</a></li>
                        <li><a href="<?php echo base_url() ?>members/settings/updateinfo"><i class="fa fa-info"></i> Update Info</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>members/safety/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                        
                        
                      </ul>
                    </li>  
                    
                    <li class="visible-xs"><a href="<?php echo base_url(); ?>members/settings/changepassword"><i class="fa fa-key"></i> Change Password</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url() ?>members/settings/updateinfo"><i class="fa fa-info"></i> Update Info</a></li>
                    <li class="visible-xs"><a href="<?php echo base_url();?>members/safety/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                    
                    
                    

                    
                    <li class="dropdown dropdown-large visible-sm"><!--menu for small devices-->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
                        
                        <ul class="dropdown-menu dropdown-menu-large row">
                            <li class="col-sm-6">
                                <ul>
                                       <li> <a href="<?php echo base_url(); ?>members" class="dropdown-header preloadThis"><i class="fa fa-bullseye text-muted"></i> Portal</a></li>

                                    <li class="divider"></li>
                
                                       <li> <a href="<?php echo base_url(); ?>coordinator/manage" class="dropdown-header preloadThis"><i class="fa fa-user"></i> Manage CC</a></li>

                                    <li class="divider"></li>
                
                                       <li> <a href="<?php echo base_url(); ?>organization/manage" class="dropdown-header preloadThis"><i class="fa fa-building-o"></i> Manage Organization</a></li>



                                   
                                </ul>
                            </li>
                            
                            
                            
                            <li class="col-sm-6">
                                <ul>

                
                                   <li> <a href="<?php echo base_url(); ?>smscenter/manage" class="dropdown-header preloadThis"><i class="fa fa-mobile"></i> SMS Center</a></li>
                                   
                                    <li class="divider"></li>

                                    <li class="dropdown-header"><i class="fa fa-gear"></i> Settings</li>

                                    <li><a href="<?php echo base_url(); ?>members/settings/changePassword" class="preloadThis">Change Password</a></li>
                                    <li><a href="<?php echo base_url() ?>members/settings/manageEmail" class="preloadThis">Manage Email ID</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="<?php echo base_url();?>members/safety/logout" class="dropdown-header"><i class="fa fa-sign-out"></i> Logout</a></li>
                                </ul>
                            </li>
                            

                            

                        </ul>
                        
                    </li><!---.end of large dropdown-->
                       
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <div class="container main_container">

        <div class="row">
            <div class="col-lg-2 col-md-2 hidden-sm hidden-xs sidebar_nav scrollable">
            	<div class="text-center" style="padding-top: 20px;">
            	<!--<img  class="img-rounded img-thumbnail" src="<?php echo base_url()?>assets/2/img/icon-user-default.png" width="130" height="130">-->
                    <p class="text-muted">
                    	<em><i class="fa fa-key"></i> Logged in as</em>
                        </p>
                    <p class="lead"><?php echo $res_fname.' '.$res_lname; ?></p>
                </div>
                <div class="main_nav">
                    <a href="<?php echo base_url() ?>members/safety" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="safety"){echo "active";}?>"><i class="fa fa-bullseye fa-fw"></i> Portal</a>


                   <?php /*?> <a href="<?php echo base_url()?>coordinator/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="coordinator"){echo "active";}?>"><i class="fa fa-user fa-fw"></i> Activated Customers</a>

                    <a href="<?php echo base_url()?>coordinator/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="coordinator"){echo "active";}?>"><i class="fa fa-user fa-fw"></i> Request Activations</a><?php */?>

                    <?php /*?><a href="<?php echo base_url()?>organization/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="organization"){echo "active";}?>"><i class="fa fa-building-o fa-fw"></i> Manage Organization</a>

                    <a href="<?php echo base_url()?>packs/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="packs"){echo "active";}?>"><i class="fa fa-cube fa-fw"></i> Input Packs</a>

                    <a href="<?php echo base_url()?>smscenter/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="smscenter"){echo "active";}?>"><i class="fa fa-mobile fa-fw"></i> SMS Center</a>

                    <a href="<?php echo base_url()?>videocenter/manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="videocenter"){echo "active";}?>"><i class="fa fa-video-camera fa-fw"></i> Video Center</a>
                    
                    <a href="<?php echo base_url()?>manage" class="list-group-item preloadThis <?php if($this->uri->segment(2)=="members"){echo "active";}?>"><i class="fa fa-briefcase fa-fw"></i> Manage Members</a><?php */?>

                    <a href="javascript:void(0);" class="list-group-item toggleCaretSet" onClick="$('.settings_nav').slideToggle();<?php /*?>$('.preincidentItems').slideUp()<?php */?>"><i class="fa fa-gear fa-fw"></i> Settings <i class="fa fa-caret-down pull-right"></i></a>
                </div>
                <div class="settings_nav" <?php if($this->uri->segment(2)=="settings"){echo ' style="display:block;"';} else {echo ' style="display:none;"'; }?>>
                    <a href="<?php echo base_url()?>members/settings/changepassword" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="changepassword"){echo "active";}?>"><i class="fa fa-key fa-fw"></i> Change Password</a>
                    <a href="<?php echo base_url()?>members/settings/updateinfo" class="list-group-item preloadThis settingChild <?php if($this->uri->segment(3)=="updateinfo"){echo "active";}?>"><i class="fa fa-info fa-fw"></i> Update Info</a>
                </div>
                
                
            </div>
			
            <!--main container-->
            <div class="col-md-10 col-xs-12 col-sm-12 scrollable main_content" style="border-left: 1px solid #e7e7e7">


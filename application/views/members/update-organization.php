                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Organization

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Portal</a></li>

                                <li class="active">Update Organization</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Crisis Organization</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-update-organization" id="frm-update-organization" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-3 control-label">Organization Name</label>

                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="" required data-msg-required="Please enter organization name" value="<?php echo $org_info[0]['organization_name']; ?>"><?php echo form_error('organization_name'); ?>

                            </div>

                        </div>

                        
                        


                        <div class="form-group">

                            <label for="firstname" class="col-sm-3 control-label">Modules</label>

                            <div class="col-sm-9">

                              <div class="checkbox">
                                <label for="checkboxes-0">
                                  <input type="checkbox" name="modules[]" id="checkboxes-0" value="1" <?php if ($org_info[0]['active_module'] == '123' || $org_info[0]['active_module'] == '13' || $org_info[0]['active_module'] == '12' || $org_info[0]['active_module'] == '1'){ echo 'checked="checked"';}?>>
                                  Standard CrisisFlo
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-1">
                                  <input type="checkbox" name="modules[]" id="checkboxes-1" value="2" <?php if ($org_info[0]['active_module'] == '123' || $org_info[0]['active_module'] == '12' || $org_info[0]['active_module'] == '2' || $org_info[0]['active_module'] == '23'){ echo 'checked="checked"';} ?> >
                                  Case Management
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-2">
                                  <input type="checkbox" name="modules[]" id="checkboxes-2" value="3" <?php if ($org_info[0]['active_module'] == '123' || $org_info[0]['active_module'] == '13' || $org_info[0]['active_module'] == '23' || $org_info[0]['active_module'] == '3'){ echo 'checked="checked"';} ?> >
                                  Video Conferencing
                                </label>
                              </div>
                              <?php echo form_error('modules[]'); ?>
                            </div>

                        </div>

                        
                        
                        
                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-md-3 control-label" for="radios">Customer Type</label>
                          <div class="col-md-9">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0" <?php if ($org_info[0]['cust_type'] == '0'){ echo 'checked="checked"'; } ?>>
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1" <?php if ($org_info[0]['cust_type'] == '1'){ echo 'checked="checked"'; } ?>>
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>
                        
                        

                        <div class="form-group">

                            <label class="col-sm-3 control-label">Select CC</label>

                            <div class="col-sm-9">

                               <select class="form-control" name="sel_cc" id="sel_cc">

                                    <option value="">Select</option>

                                    <?php

									if(count($cc_list)>0)

									{

										foreach($cc_list as $cc)

										{

									?>

                                    <option value="<?php echo $cc['login_id']; ?>" <?php if($cc['login_id']==$org_info[0]['cc_id'])echo 'selected="selected"'; ?>>

										<?php echo $this->master_model->decryptIt($cc['crt_first_name']).' '.$this->master_model->decryptIt($cc['crt_last_name']); ?>

                                    </option>

                                    

                                    <?php

										}

									} 

									?>

                               </select>

                            </div>

                        </div>
                        
                        


                        <div class="form-group">

                            <label class="col-sm-3 control-label">Preload Input Pack (Optional)</label>

                            <div class="col-sm-9">
                               <input type="text" class="form-control hidden" id="old_packs" name="old_packs" value="<?php echo $org_info[0]['input_pack_id']; ?>">
                                
                               <select class="form-control" name="input_packs" id="input_packs">

                                    <option value="0" >Select</option>
                                    <?php /*?><option value="0" <?php if($org_info[0]['input_pack_id']=='0'){ echo 'selected="selected"'; } ?>>No Preload</option><?php */?>

                                    <?php

									if(count($input_packs)>0)

									{

										foreach($input_packs as $packs)

										{

									?>

											<?php if($org_info[0]['input_pack_id'] == $packs['id'] || $packs['org_id'] =="0"){ ?>
                                    
                                    	<option value="<?php echo $packs['id']; ?>" <?php if($org_info[0]['input_pack_id'] == $packs['id']){ echo 'selected="selected"'; }?>> <?php echo $packs['name']; ?> </option>
                                        
                                        

                                    <?php
											}
										}

									} 

									?>

                               </select>
                              <?php echo form_error('input_packs'); ?>
                            </div>

                        </div>
                        
                        
                        
                        

                        <div class="form-group">

                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-9">

                            <button type="submit" class="btn btn-primary" name="add_orgz" id="add_orgz">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




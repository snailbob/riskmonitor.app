                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage <?php echo $the_packs[0]['name']?></h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                               <a href="<?php echo base_url().'members'?>">Portal</a></li>

                                </li>

                                <li class="active">Manage <?php echo $the_packs[0]['name']?></li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">


                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>



                        <div class="panel panel-default">


                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Crisis Documents </h4>
                                </div>
                                
                                <div class="panel-widgets">
                                    <a class="btn btn-primary btn-xs" href="<?php echo base_url().'members/packs/add_document/'.$this->uri->segment(4);?>">Add Crisis Document</a>
                                </div>                                
                                

                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">


								<?php 

								if(count($file_upload)>0)

								{
								?>

							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="100%">File Name</th>

                                        <th width="50px"></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 


									foreach($file_upload as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td>
										
											<?php
										
											$fileext = explode ('.',$value['file_upload_name']);
											if ($fileext[1]=='pdf' || $fileext[1]=='PDF'){
											
											echo '<i class="fa fa-file-pdf-o text-red"></i> ';
											
											}
											
											else if ($fileext[1]=='doc' || $fileext[1]=='docx' || $fileext[1]=='DOC' || $fileext[1]=='DOCX'){
											
											echo '<i class="fa fa-file-word-o text-blue"></i> ';
											
											}
											
											else if ($fileext[1]=='txt' || $fileext[1]=='TXT'){
											
											echo '<i class="fa fa-file-text-o text-muted"></i> ';
											
											}
											
											
											 echo substr($value['file_upload_name'],13,50);?>                                        
                                        </td>

                                    	<td>

                                       <?php /*?> <a class="btn btn-green btn-xs" href="<?php echo base_url(); ?>members/organization/update/<?php echo  $value['organization_id']; ?>">Edit</a> 

                                    <a class="btn btn-orange btn-xs" href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'members/organization/delete/'.$value['organization_id']; ?>');">Delete</a><?php */?>


       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="#" data-toggle="modal" data-target="#mydeldocModal<?php echo $value['file_upload_id']; ?>">Delete</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>members/packs/download_doc/<?php echo $value['file_upload_name']; ?>">Download</a> 
            </li>

          </ul>
        </div>    
<!-- Modal -->
<div class="modal fade" id="mydeldocModal<?php echo $value['file_upload_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="lead">Do you really want to delete <?php echo substr($value['file_upload_name'],13,50); ?>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url().'members/packs/delete_doc/'.$this->uri->segment(4).'/'.$value['file_upload_id'].'/'.$value['file_upload_name']; ?>" class="btn btn-primary">OK</a>
      </div>
    </div>
  </div>
</div>


                                    	</td>

                                    </tr>

								<?php 

									}


								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 

									}
									else{
										echo '
											<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-paperclip" style="font-size: 90px"></i></p>
											<p class="text-center" style="color: #ccc; margin-top: 20px;">No Crisis Documents</p>';
										
									}


								?>








                            </div>

                        </div>

                        <!-- /.panel -->



                        <div class="panel panel-default">


                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Response Action Plan </h4>
                                </div>
                                
                                <div class="panel-widgets">
                                    <a class="btn btn-primary btn-xs" href="<?php echo base_url();?>members/packs/add_scenario/<?php echo $this->uri->segment(4); ?>">Add Response Action</a>
                                </div>                                
                                

                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">

								<?php 

								if(count($task_list)>0)

								{
									?>

							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="response-table">

                                <thead>

                                    <tr>

                                        <th width="50%">Scenario</th>

                                        <th width="50%">Task</th>

                                       <?php /*?> <th width="13%">Owner</th>

                                        <th width="13%">Status</th><?php */?>

                                        <th width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($task_list as $r => $value)

									{

								?>


                                    <tr>

                                    	<td><?php echo  $value['scenario']; ?></td>

                                    	<td><?php echo  substr($value['task'],0,50); ?></td>

                                    	<?php /*?><td><?php echo  $value['crt_first_name'].' '.$value['crt_last_name']; ?></td>

                                    	<?php 

                                    if($value['task_status'] == 1 ){

                                    $status = ' <span class="text-red">In Progress</span>';

                                    } if($value['task_status'] == 0 ) {	

                                    $status = '<span class="text-red">Pre-Incident</span>';

                                    }

									 if($value['task_status'] == 2 ){

                                    $status = '<span class="text-green">Completed</span>';

                                    } 

                                    ?>

                                    <td><?php echo  $status ?></td><?php */?>

                                    <td>
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
            <?php /*?><li>
                <a href="<?php echo base_url(); ?>cc/scenario/update/<?php echo  $value['task_id']; ?>">Edit</a> 

            </li><?php */?>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirmed('<?php echo base_url().'members/packs/delete_task/'.$value['scenario_id'].'/'.$this->uri->segment(4).'/'.$value['task_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href=""  data-toggle="modal" data-target="#myModal<?php echo  $value['task_id']; ?>">View</a>
            
            </li>
          </ul>
        </div>    
                                    
                                    


                                    </td>

                                    </tr>
                                    
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $value['task_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Response Action Plan Details</h4>
      </div>
      <div class="modal-body">
			<p>Scenario: <span class="text-muted"><?php echo  $value['scenario']; ?></span></p>
			<p>Task: <span class="text-muted"><?php echo  $value['task']; ?></span></p>
			<p>Task Guidance: <span class="text-muted"><?php echo  $value['task_guide']; ?></span></p>
			<?php /*?><p>Assigned CRT: <span class="text-muted"><?php echo  $value['crt_first_name'].' '.$value['crt_last_name']; ?></span></p><?php */?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
                                    

								<?php } ?>

                                                    

                                                </tbody>


                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php 

									}
									else{
										echo '
											<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-lightbulb-o" style="font-size: 90px"></i></p>
											<p class="text-center" style="color: #ccc; margin-top: 20px;">No Response Action Plan Tasks</p>';
										
									}


								?>







                            </div>

                        </div>

                        <!-- /.panel -->


                        <div class="panel panel-default">


                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4> Standby Messages </h4>
                                </div>
                                
                                <div class="panel-widgets">
                                    <a class="btn btn-primary btn-xs" href="<?php echo base_url();?>members/packs/add_message/<?php echo $this->uri->segment(4); ?>">Add Standby Message</a>
                                </div>                                
                                

                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">

								<?php 

								if(count($msg_list)>0)

								{ ?>

							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="standby-table">

                                <thead>

                                    <tr>

                                        <th width="50%">Scenario</th>

                                        <th  width="50%">Message</th>

                                        <th  width="50px"> </th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 
									foreach($msg_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td>
											<?php echo $value['scenario']; 
                                            
                                            /*foreach ($scenario as $ss=>$indi_scene){
                                                if ($indi_scene['scenario_id'] == $value['scenario_id'] ){
                                                    echo $indi_scene['scenario'];
                                                }
                                            }*/
    
                                            ?>
                                        </td>

                                    	<td>
											<?php
												echo substr(stripslashes($value['standby_message']),0,45).'..';
												
												
												foreach ($standby_messages as $mes=>$indi_mes){
													/*echo '<script>console.log('.$indi_mes['stn_msg_id'].');</script>'; */
													if ($indi_mes['stn_msg_id'] == $value['stn_msg_id'] ){
														
														if ($indi_mes['input_pack_id'] == '0'){
															echo '<span class="text-muted small"> [Added by CC]</span>';
														}
	
													}
												}
												
												
											?>
                                        
                                        </td>

                                    	<td>
                                        
       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-success" role="menu" style="font-size: 90%; min-width: 82px;">
           <?php /*?> <li>
                <a href="<?php echo base_url(); ?>cc/standbymessage/update/<?php echo  $value["stn_msg_id"]; ?>">Edit</a> 

            </li><?php */?>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirmed('<?php echo base_url().'members/packs/delete_message/'.$this->uri->segment(4).'/'.$value['stn_msg_id']; ?>');">Delete</a>
            </li>
            <li>
                <a href=""   data-toggle="modal" data-target="#mymsgModal<?php echo $value["stn_msg_id"];?>">View</a>
            
            </li>
          </ul>
        </div>    
        

                                    </td>

                                    </tr>

<!-- Modal -->
<div class="modal fade" id="mymsgModal<?php echo $value['stn_msg_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Standby Message Details</h4>
      </div>
      <div class="modal-body">
			<p>Scenario: <span class="text-muted"><?php echo $value['scenario']; ?></span></p>
			<p>Standby Message: <span class="text-muted"><?php echo htmlspecialchars_decode(nl2br(stripslashes($value['standby_message'])),true); ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
								<?php 

									}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->


								<?php 

									}
									else{
										echo '
											<p class="text-center" style="color: #F3F3F3; margin-top: 20px;"><i class="fa fa-folder-open" style="font-size: 90px"></i></p>
											<p class="text-center" style="color: #ccc; margin-top: 20px;">No Standby Messages</p>';
										
									}


								?>








                            </div>

                        </div>

                        <!-- /.panel -->



                    </div>



                </div><!--.row -->

					




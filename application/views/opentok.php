<html> 
  <head></head> 
  <body> 
    <h2>Session ID</h2>
    <p><?php echo $sessionId ?></p>
    <h2>Token</h2>
    <p><?php echo $token ?></p>

    <div id='myPublisherDiv'></div> 
    <div id='subscribersDiv'></div> 
    
    <script src='//static.opentok.com/webrtc/v2.2/js/opentok.min.js'></script> 
    <script> 
      var apiKey = '45109742';
      var sessionId = '<?php echo $sessionId ?>'; 
      var token = '<?php echo $token ?>';
      var session = OT.initSession(apiKey, sessionId); 
      session.on({ 
          streamCreated: function(event) { 
            session.subscribe(event.stream, 'subscribersDiv', {insertMode: 'append'}); 
          } 
      }); 
      session.connect(token, function(error) {
        if (error) {
          console.log(error.message);
        } else {
          session.publish('myPublisherDiv', {width: 320, height: 240}); 
        }
      });
    </script> 
  </body> 
</html>      
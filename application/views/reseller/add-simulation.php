                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update Input Pack

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 
                                	<a href="<?php echo base_url().'webmanager'?>">Portal</a></li>
                                <li class="active">Update Input Pack</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



<!-- Form AREA -->

<div class="row">

    <div class="col-lg-12">

        <?php 

        if($this->session->flashdata('success')!="")

        {

        ?>

        <div class="alert alert-success alert-dismissable">

        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

        <?php    

        } 

        if($this->session->flashdata('error')!="")

        {

        ?>

        <div class="text-red">

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

        <?php

        } 

        ?>

        <div>
        	<a href="<?php echo base_url().'reseller/simulation/module/'.$simu_mod_id; ?>" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</a>
		</div>

        <div class="clearfix" style="margin-bottom: 15px;">
		</div>


        <div class="panel panel-default">

            <div class="panel-heading">

                <div class="panel-title">

                    <h4><?php echo $simu_text; ?> Simulation</h4>

                </div>

                <div class="panel-widgets">

                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                </div>

                <div class="clearfix"></div>

            </div>

            <div id="validationExamples" class="panel-collapse collapse in">

                <div class="panel-body">

                    <div class="hidden">
                        <form id="myForm2" action="<?php echo base_url().'reseller/simulation/upload' ?>" method="post" enctype="multipart/form-data">
                             <input type="file" id="myfile" name="myfile" size="0" onchange="$('#myForm2').submit();">
                             <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                             <input type="hidden" id="filetype" name="filetype" value="">
                             <input type="submit" class="hidden" value="Ajax File Upload">
                         </form>    
                    </div>
                    

                    <form id="simulation_form">
                    	<div class="form-group">
                        	<label>Input Name</label>
                            <input type="text" class="form-control" name="simulation_name" placeholder="Simulation Name" autofocus="autofocus" value="<?php echo $name; ?>"/>
                            <input type="hidden" name="simulation_id" value="<?php echo $simu_id; ?>"/>
                            <input type="hidden" name="utime" value="<?php echo $uniquetime; ?>"/>
                            <input type="hidden" name="module_id" value="<?php echo $simu_mod_id; ?>"/>
                        </div>
                        
                                
                    	<div class="form-group select_media">
                        	<div class="row">
                            	<div class="col-sm-4">
                                    <div class="circle-tile blue">
                                        <a href="#" class="circle-tile-footer simu_type" data-type="video">
                                            <div class="circle-tile-number text-faded">
                                                <i class="fa fa-file-movie-o fa-fw fa-3x"></i><br />
                                            	<span>Video</span>
                                            </div>
                                            
                                        </a>
                                    </div>
                                </div>
                                
                            	<div class="col-sm-4">
                                    <div class="circle-tile blue">
                                        <a href="#" class="circle-tile-footer simu_type" data-type="audio">
                                            <div class="circle-tile-number text-faded">
                                                <i class="fa fa-file-audio-o fa-fw fa-3x"></i><br />
                                            	<span>Audio</span>
                                            </div>
                                            
                                        </a>
                                    </div>
                                </div>
                                
                            	<div class="col-sm-4">
                                    <div class="circle-tile blue">
                                        <a href="#" class="circle-tile-footer simu_type" data-type="text">
                                            <div class="circle-tile-number text-faded">
                                                <i class="fa fa-file-text-o fa-fw fa-3x"></i><br />
                                            	<span>Text</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                                
                    	<div class="form-group summernote_field hidden">
                        	<div class="form-group">
                                <!-- Summernote CMS Example -->
                                <div id="summernote"><?php echo $content; ?></div>
                            </div>
                            <div class="form-group">
                                <a href="#" class="btn btn-default preview_btn"><i class="fa fa-eye"></i> Preview</a>
                            </div>
                            
                    	</div>
                       
                  
                    	<div class="form-group upload_field hidden">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="javascript: void(0);" class="btn btn-default btn-lg" onclick="$('#myfile').click();"><i class="fa fa-upload"></i> Upload File</a>
                                    <p class="text-muted">tete</p>
                                 </div>
                                <div class="col-xs-6">
                                    <div id="progress2" style="display: none;">
                                        <div id="bar2"></div>
                                        <div id="percent2">0%</div >
                                    </div>
                                 </div>  
                                 
                            </div>
                        </div>
                        
                        
                        
                    	<div class="form-group audio_video_container <?php if(count($attached_media) == 0){ echo 'hidden'; } ?>">
                        	<b>Preview</b>
                            <div class="well">
                            	<?php
									if(count($attached_media) > 0){
										foreach($attached_media as $r=>$value){
											$filext = substr($value['file_upload_name'], -3);
											
											if($filext == 'mp3' || $filext == 'wav'){
												echo '<p>'.substr($value['file_upload_name'], 13).' <a href="#" class="delete_media" title="delete media" data-id="'.$value['file_upload_id'].'"><i class="fa fa-times-circle-o"></i></a><br><audio controls><source src="'.base_url().'uploads/simulations/'.$value['file_upload_name'].'" type="audio/mpeg">Your browser does not support the audio element.</audio></p>';
											
											}
											else if($filext == 'mp4' || $filext == 'mov' || $filext == 'wmv' || $filext == 'avi'){
												
												echo '<p>'.substr($value['file_upload_name'], 13).' <a href="#" class="delete_media" title="delete media" data-id="'.$value['file_upload_id'].'"><i class="fa fa-times-circle-o"></i></a><br><video style="width: 100% !important; height: auto !important;" controls><source src="'.base_url().'uploads/simulations/'.$value['file_upload_name'].'" type="video/mp4">Your browser does not support the video tag.</video></p>';
												
											}
											
										}
									}
								?>
                            </div>
                        </div>
                        
                        
                    	<div class="form-group">
                        	<label>Trigger</label>
                        	<select name="trigger" class="form-control">
                            	<option value="">Select</option>
                            	<option value="steps" <?php if($the_trigger == 'steps') { echo 'selected="selected"'; } ?>>Base on completion or Steps</option>
                            	<option value="time" <?php if($the_trigger == 'time') { echo 'selected="selected"'; } ?>>Base on time</option>
                            </select>
                        </div>
                        
                    	<div class="form-group steps_list <?php if($the_trigger != 'steps') { echo 'hidden'; } ?>">
                        	<label>Steps</label>
                        	<select name="steps" class="form-control">
                            	<?php
								foreach($steps as $r=>$value){
									echo '<option value="'.$value['id'].'"';
									if($the_step == $value['id']){
										echo 'selected="selected"';
									}
									echo '>'.$value['name'].'</option>';
								}
								?>
                            </select>
                        </div>
                        
                    	<div class="form-group date_picker <?php if($the_trigger != 'time') { echo 'hidden'; } ?>">
                        	<label>Date</label>
                        	<input type="text" name="mediadate" data-date-format="DD-MMM-YYYY"  class="form-control" placeholder="Date" value="<?php echo $the_thedate; ?>">
                        </div>
                        
                        
                    	<div class="form-group">
                        	<button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                                

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->



<!-- Modal -->
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <p class="lead">
            Preview
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </p>
        <div class="preview-content well">
        
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Task Guidance

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                	<a href="<?php echo base_url().'consultant'?>">Portal</a></li>

                                <li class="active">Task Guidance</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                    <div class="col-lg-12">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error :</strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Task Guidance</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									
                                    

                            <?php 
							$steps_total = count($step2) + count($step3) + count($step4) + count($step5) + count($step6) + count($step7) + count($step8);
                            if($steps_total>0)

                            {
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<tbody>
                                
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 2</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									$i = 0;
									foreach ($step2 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>



                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 2 end ***************************/ ?>
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 3</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									foreach ($step3 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>


                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 3 end ***************************/ ?>
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 4</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									foreach ($step4 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>


                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 4 end ***************************/ ?>
                                    
                                    
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 5</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									foreach ($step5 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>



                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 5 end ***************************/ ?>
                                    
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 6</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									foreach ($step6 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>



                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 6 end ***************************/ ?>
                                    
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 7</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									foreach ($step7 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>



                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 7 end ***************************/ ?>
                                    
                                
                                    <tr>
                                    	<td colspan="3" style="background-color:#fff;"><h4>Step 8</h4></td>
                                    </tr>
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Task</th>
                                        <th width="40%" style="background-color:#fff;">Task Guidance</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center">Preview</th>
                                    </tr>
                                    
                                    
                                    <?php
									foreach ($step8 as $r=>$value){?>
                                    <tr>


                                    	<td><?php echo $value['question']; ?></td>

                                    	<td>
										<?php
                                        
										if ( $value['guidance'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['guidance'] ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control"><?php echo $value['guidance'] ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitGuidance('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>

                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="prev_btn_<?php echo $i; ?>" class="btn btn-default btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $value['guidance']; ?>" style="background:#e0e0e0"><i class="fa fa-question"></i></button>
                                        </td>



                                    </tr>

									<?php
									$i++;
									} ?>
                                    
                                    <?php  /******************step 8 end ***************************/ ?>
                                    
                                    
                                </tbody>

                                

                                

                            </table>

                        </div>

                        <!-- /.table-responsive -->
    
                        <?php
                        }
                        else{
                        
                            echo '<p class="text-center text-muted" style="margin-top: 20px;">No task available.</p>';
                        }
                        ?>



                                    
                                    
                                                                        
                                    
                                    
                                    
                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




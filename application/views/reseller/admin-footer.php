            </div>

        </div>

    </div>
    <!-- /.container -->


    <hr />
    <div class="container-fluid">

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-muted">© 2015 CrisisFlo</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootbox.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/jquery.isloading.min.js"></script>

	<script src="<?php echo base_url()?>assets/2/js/jquery.custombox.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/dataTables/datatables-bs3.js"></script>


    <script src="<?php echo base_url()?>assets/2/js/main.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/consultant.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/admin-validation.js"></script>


    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/zxcvbn-async.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/pwstrength/pwstrength.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url()?>assets/2/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/wysiwyg-demo.js"></script>

    <script src="<?php echo base_url()?>assets/2/js/plugins/jquery.form/jquery.form.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/moment.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/plugins/datepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>assets/2/js/plugins/jquery.ui/jquery-ui.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                viewports: {
                    progress: ".pwstrength_viewport_progress",
                    verdict: ".pwstrength_viewport_verdict"
                }
            };
            options.common = {
                onLoad: function () {
                    $('#messages').text('Start typing password');
                },
                zxcvbn: true
            };
            $(':password').pwstrength(options);
        });
		
		
		//validation for chandge password add new cc
	    $("#change_cc_pw").click(function()
		{ 
			var email=$("#email");
			var new_pass=$("#new_pass");
			var confirm_pass=$("#confirm_pass");
			
			$('#new_pass').removeClass('error_border');
			$('#confirm_pass').removeClass('error_border');
			
			$('#new_pass').addClass('form-control');
			$('#confirm_pass').addClass('form-control');
			
			if ($('.password-verdict').html()!="Medium" && $('.password-verdict').html()!='Strong' && $('.password-verdict').html()!='Very Strong' ){
				
				$('.mediumerror').html('For security reasons, your password needs to be ‘Medium’ strength or above.');

				$('#cc_password').focus();
				return false;
			}
			
			else{
				$('.mediumerror').html('');
			}
			
		     
			if(new_pass.val()=="")
            { 
			    new_pass.val('');
				$('#new_pass').attr('placeholder','Please Add New Password');
				$('#new_pass').removeClass('form-control');
				$('#new_pass').addClass('error_border');
				$('#new_pass').focus();
				return false;
			
			}
			else  if(confirm_pass.val()=="")
            { 
			    confirm_pass.val('');
				$('#confirm_pass').attr('placeholder','Please Confirm Password');
				$('#confirm_pass').removeClass('form-control');
				$('#confirm_pass').addClass('error_border');
				$('#confirm_pass').focus();
				return false;
			
			}
	
		});
		
		
		
		
    </script>

		<?php if ($this->uri->segment(2) == 'packs' && $this->uri->segment(3) == 'view' && $this->uri->segment(5) != ''  ){ ?>
    <script>
		$( document ).ready(function() {
		
			var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close hidden" id="hide_x" data-dismiss="modal" aria-hidden="true">&times;</button><p class="lead" id="hide_spin">';
			
			var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Your file is being analysed.</p><p id="scan_result">';
			
			var myModal3 = '</p></div></div></div> ';

			var myModal = myModal1 + myModal2 + myModal3;
			
			$(myModal).modal('show');
			
			var file_id = <?php echo $this->uri->segment(5); ?>;
			$.ajax({
				  type: "POST",
				  
				  url: "<?php echo base_url().'webmanager/packs/scan_response'; ?>",
				  
				  data: { file_id: file_id },
				  
				  success: function(data) {
					console.log(<?php echo $this->uri->segment(5); ?>);
					  $('#scan_result').html(data);
					  $('#hide_spin').addClass('hidden');
					  $('#hide_x').removeClass('hidden');
					  
				  }
				  
			});
			
				
		});

    </script>
		<?php
		}
		?>




    <script>
	
	$('.common_msg_click').on('click', function ( e ) { 
				$.fn.custombox( this );
				e.preventDefault();
	});
	
	

	<?php if ($this->uri->segment(2) == 'recall'){?>
	//activate last visited tab
	 $(document).ready(function() {
		if(location.hash) {
		   $('a[href=' + location.hash + ']').tab('show');
		}
	  $(document.body).on("click", "a[data-toggle]", function(event) {
		  if (this.getAttribute("href") !='#'){
			location.hash = this.getAttribute("href");
		  }
	  });
	});
	  $(window).on('popstate', function() {
		var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
		$('a[href=' + anchor + ']').tab('show');
	  });
	<?php } ?>
	
	


		$(document).ready(function(e) {

			var options = {
				dataType: 'json',
				beforeSend: function() 
				{
					$("#progress2").show();
					//clear everything
					$("#bar2").width('0%');
					$("#message2").html("");
					$("#percent2").html("0%");
				},
				uploadProgress: function(event, position, total, percentComplete) 
				{
					$("#bar2").width(percentComplete+'%');
					$("#percent2").html(percentComplete+'%');
			
				
				},
				success: function(response) 
				{
					$("#bar2").width('100%');
					$("#percent2").html('100%');
			
				},
				complete: function(response) 
				{
					console.log(response);
					
					if(typeof response.responseJSON !== 'undefined'){
						if(response.responseJSON.result == 'video_invalid'){
							bootbox.alert('<p class="lead">Please select a valid video file.</p>');
						}
						else if(response.responseJSON.result == 'audio_invalid'){
							bootbox.alert('<p class="lead">Please select a valid audio file.</p>');
						}
						else if(response.responseJSON.result == 'success'){
							
							if(response.responseJSON.filetype == 'audio'){

							
								$('.audio_video_container .well').prepend('<p>'+response.responseJSON.filename+' <a href="#" class="delete_media" title="delete media" data-id="'+response.responseJSON.file_id+'"><i class="fa fa-times-circle-o"></i></a><br><audio controls><source src="'+response.responseJSON.file_url+'" type="audio/mpeg">Your browser does not support the audio element.</audio></p>');
								$('.audio_video_container').removeClass('hidden');	
							}
							
							else if(response.responseJSON.filetype == 'video'){

								$('.audio_video_container .well').prepend('<p>'+response.responseJSON.filename+' <a href="#" class="delete_media" title="delete media" data-id="'+response.responseJSON.file_id+'"><i class="fa fa-times-circle-o"></i></a><br><video style="width: 100% !important; height: auto !important;" controls><source src="'+response.responseJSON.file_url+'" type="video/mp4">Your browser does not support the video tag.</video></p>');
								
								$('.audio_video_container').removeClass('hidden');	
								
							}
							
						}
					}
					else{
						bootbox.alert('<p class="lead">'+response.responseText+'</p>');
					}
				}
			 
			}; 
		
			$("#myForm2").ajaxForm(options);
								
        });

    </script>
    
    <style>
	    #arrange_arrows{
			cursor: move;
		}

		.popover {
			z-index: 1031; /* A value higher than 1010 that solves the problem */
		}
	</style>
    
</body>

</html>

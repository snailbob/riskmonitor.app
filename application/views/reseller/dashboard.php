<!-- begin PAGE TITLE AREA -->

<!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

<?php /*********variables***********/

	
	$reseller_info = $this->master_model->getRecords('reseller_master',array('login_id'=>$_SESSION['reseller_id']));
	$tabs = $reseller_info[0]['deactivated_tab'];
	
	$single_tab = explode(' ',$tabs);
?>


<div class="row">
<?php
	if (in_array("portal", $single_tab)){ 
?>

  <div class="col-lg-12">

    <div class="page-title">

      <h1> Crisis FLO Reseller Portal</h1>

      <ol class="breadcrumb">

        <li class="active"><i class="fa fa-bullseye"></i> Portal</li>

      </ol>

    </div>

  </div>

  <!-- /.col-lg-12 --> 




</div>

<!-- /.row --> 

<!-- end PAGE TITLE AREA -->



                

                <div class="row">

                    <div class="col-lg-12">

                    	<?php 
						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default" >





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-user fa-fw"></i> Awaiting Admin Activation</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">


                            <?php 

                            if(count($all_cc)>0)

                            {
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="25%">First Name</th>

                                        <th width="25%">Last Name</th>

                                        <!--<th width="20%">Email</th>-->

                                        <th width="30%">Organizations</th>

                                        <th width="20%">Status</th>

                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>
                                
                                <?php

									foreach($all_cc as $r => $value)

									{ 	$str='';

										$organisations=$this->master_model->getRecords('organization_master',array('cc_id'=>$value['login_id']));

										if(count($organisations)>0)

										{

											for($i=0;$i<count($organisations);$i++)

											{

												if($i<(count($organisations)-1)){$suffix=', ';}else{$suffix='';}

												$str.=$organisations[$i]['organization_name'].$suffix;

											

											}

										}

								?>

                                    <tr>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?></td>

                                    	<td><?php echo  $this->master_model->decryptIt($value['crt_last_name']);  ?></td>

                                    	<?php /*?><td><?php echo  $value['crt_email']; ?></td><?php */?>

                                        <td><?php if($str!=''){echo $str;}else{echo "NA";}?></td>


											<?php 
    
                                        if($value['activated'] == 'yes' ){
    
                                        $status = '<i class="fa fa-circle text-green"></i> <span class="text-green">Activated</span>';
    
                                        } else {	
    
                                        $status = '<i class="fa fa-circle text-red"></i> <span class="text-red">Pending</span>';
    
                                        }
    
                                        ?>
    
                                        <td><?php echo $status; ?></td>


                                    <td>

       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>reseller/customer/update/<?php echo  $value['login_id']; ?>">Edit</a>
            </li>
            <li>
                <a href="#" data-toggle="modal" data-target="#ccdetails<?php echo  $value['login_id']; ?>">Details</a> 
            </li>

			<?php if($str==''){?>
            <li>
                <a href="#" data-toggle="modal" data-target="#mydelccModal<?php echo $value['login_id']; ?>">Delete</a>
            </li>
            <?php /*?><li>
                <a href="<?php echo base_url(); ?>webmanager/organization/add/<?php echo $value['login_id']; ?>">Add Org</a>
            </li><?php */?>
			<?php } ?>


          </ul>
        </div>   
        
<!-- Modal -->
<div class="modal fade" id="ccdetails<?php echo  $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">CC Details</h4>
      </div>
      <div class="modal-body">
			<p>Name: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']); ?></span></p>
			<p>Email Address: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_email']); ?></span></p>
			<p>Mobile Number: <span class="text-muted"><?php echo  $this->master_model->decryptIt($value['crt_mobile']); ?></span></p>
			<p>Organization: <span class="text-muted"><?php if($str!=''){echo $str;}else{echo "NA";}?></span></p>
			<p>User Type: <span class="text-muted"><?php echo  $value['user_type']; ?></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
        
        
        
<?php if($str==''){?>
<!-- Modal -->
<div class="modal fade" id="mydelccModal<?php echo $value['login_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="lead">Do you really want to delete Coordinator <?php echo  $this->master_model->decryptIt($value['crt_first_name']); ?> <?php echo  $this->master_model->decryptIt($value['crt_last_name']);  ?>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url().'webmanager/coordinator/delete/'.$value['login_id']; ?>" class="btn btn-primary">OK</a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
                                    </td>

                                    </tr>

								<?php 

									}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->

								<?php
								}
								else{
								
									echo '<p class="text-center text-muted" style="margin-top: 20px;">No pending activation.</p>';
								}
								?>






                            </div>

                        </div>


                        <!-- /.panel -->

                    </div>


	<?php 
    
    }
    else{
        echo '<p class="text-center text-muted" style="padding: 120px;"><i class="fa fa-exclamation-triangle fa-5x "></i><br/>Portal Deactivated</p>';
    
    }//not deactivated
    ?>

                </div><!--.row -->

                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Manage Organization</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                <a href="<?php echo base_url().'consultant'?>">Portal</a>

                                </li>

                                <li class="active">Manage Organization</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="hidden col-lg-12" style="height: 40px;">

                    <a class="btn btn-primary pull-right" href="<?php echo base_url();?>reseller/organization/add">Add Organisation</a> 

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-user fa-fw"></i>Crisis Organization</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">



							<div class="table-responsive">

							<table class="table table-hover table-bordered table-green" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="40%">Organization Name</th>

                                        <th width="50%">Co-ordinator</th>

                                        <th width="10%"></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php 

								if(count($org_list)>0)

								{

									foreach($org_list as $r => $value)

									{

								?>

                                                        

                                    <tr>

                                    	<td><?php echo $value['organization_name']; ?></td>

                                    	<td><?php echo $this->master_model->decryptIt($value['crt_first_name']).' '.$this->master_model->decryptIt($value['crt_last_name']); ?></td>

                                    	<td>

                                       <?php /*?> <a class="btn btn-green btn-xs" href="<?php echo base_url(); ?>reseller/organization/update/<?php echo  $value['organization_id']; ?>">Edit</a> 

                                    <a class="btn btn-orange btn-xs" href="javascript:void(0);" onclick="return del_confirm('<?php echo base_url().'reseller/organization/delete/'.$value['organization_id']; ?>');">Delete</a><?php */?>


       <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu bg-green" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="<?php echo base_url(); ?>reseller/organization/update/<?php echo  $value['organization_id']; ?>">Update</a> 
            </li>
            <li>
                <a href="javascript:void(0);" onclick="return del_confirmed('<?php echo base_url().'reseller/organization/delete/'.$value['organization_id']; ?>');">Delete</a>
                <?php /*?><a href="#" data-toggle="modal" data-target="#mydelccModal<?php echo $value['organization_id']; ?>">Delete</a><?php */?>
            </li>
            
          </ul>
        </div>    
<?php /*?><!-- Modal -->
<div class="modal fade" id="mydelccModal<?php echo $value['organization_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="lead">Do you really want to delete <?php echo  $value['organization_name']; ?> Organization?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url().'reseller/organization/delete/'.$value['organization_id']; ?>" class="btn btn-primary">OK</a>
      </div>
    </div>
  </div>
</div><?php */?>


                                    	</td>

                                    </tr>

								<?php 

									}

								}

								?>

                                                    

                                                </tbody>

                                                

                                                

                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->









                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




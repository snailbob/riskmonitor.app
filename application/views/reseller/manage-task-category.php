                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Task Sub-Category</h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                <a href="<?php echo base_url().'consultant'?>">Portal</a>

                                </li>

                                <li class="active">Task Sub-Category</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->


                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                

                <div class="row">



                    <div class="col-lg-12" style="height: 40px;">

                    <a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Add Sub-Category</a> 
                    


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Sub-Category</h4>
        <p><br />
        
<!-- Text input-->
<div class="form-group">
  <label class="control-label" for="add_cat">Sub-Category</label>  
  <input id="add_cat" name="add_cat" type="text" placeholder="Sub-Category" class="form-control input-md">
  <span class="help-block text-red" id="category_err"></span>  
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="control-label" for="select_step">Category</label>
    <select id="select_step" name="select_step" class="form-control">
      <option value="">Select</option>
      <option value="2">Step 2</option>
      <option value="3">Step 3</option>
      <option value="4">Step 4</option>
      <option value="5">Step 5</option>
      <option value="6">Step 6</option>
      <option value="7">Step 7</option>
      <option value="8">Step 8</option>
    </select>
  <span class="help-block step_err text-red"></span>  
</div>

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="saveNewCategory();">Save changes</button>
      </div>
    </div>
  </div>
</div>
                    
                    
                    

                    </div>

                    <div class="col-lg-12" style="padding-top:10px;">

                    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                        <div class="panel panel-default">





                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4><i class="fa fa-exchange fa-fw"></i>Task Sub-Category</h4>

                                </div>



                            <div class="clearfix"></div>

                            </div>

                            <div class="panel-body">


                            <?php 

                            if(count($category)>0)

                            {
								
							?>

							<div class="table-responsive">

							<table class="table table-hover table-green" id="example-tablexxx">


								<tbody>
                 
                                    <tr>
                                        <th width="50%" style="background-color:#fff;">Sub-Category</th>
                                        <th width="40%" style="background-color:#fff;" class="text-center">Category</th>
                                        <th width="10%" style="background-color:#fff;" class="text-center"></th>
                                    </tr>
                                
                                
                                <?php		
									$i = 0;

									foreach($category as $r => $value) {

								?>
         			
                        
                                    <tr>

                                    	<td>
										<?php
                                        
										if ( $value['category_name'] == ''){ ?>
                                            
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit">Add Task Guidance</a>
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control" autofocus="autofocus"></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitCategoryUpdate('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
										
										<?php } else {?>
                                        
                                            <a class="text-muted" id="empty_guide_label<?php echo $i; ?>" style="text-decoration: none; cursor: pointer" onclick="$(this).hide();$('#toggle_tx<?php echo $i; ?>').show();" title="click to edit"><?php echo $value['category_name']; ?></a>
                                     
                                           <p id="toggle_tx<?php echo $i; ?>" style="display: none;"><textarea id="my_text<?php echo $i; ?>" name="my_text<?php echo $i; ?>" class="form-control" autofocus="autofocus"><?php echo $value['category_name']; ?></textarea>
                                           <button class="btn btn-primary btn-xs pull-right" style="margin-top: 5px; margin-left: 5px;" onclick="submitCategoryUpdate('toggle_tx<?php echo $i; ?>','empty_guide_label<?php echo $i; ?>','#my_text<?php echo $i; ?>','<?php echo $value['id']; ?>','prev_btn_<?php echo $i; ?>');">Submit</button>
                                           <button class="btn btn-default btn-xs pull-right" style="margin-top: 5px;" onclick="$('#toggle_tx<?php echo $i; ?>').hide();$('#empty_guide_label<?php echo $i; ?>').show();">Cancel</button>
                                           </p>
                                            
                                            
										<?php } ?>
                                        </td>


                                    	<td class="text-center" style="vertical-align: middle">
                                        	Step <?php echo $value['step_no'] ?>
                                        </td>
                                    	<td class="text-center" style="vertical-align: middle">
                                        
                                            <button id="del_btn_<?php echo $i; ?>" class="btn btn-danger btn-sm tooltip-test" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="confirmDelete('<?php echo $value['id']?>','cf_recall_steps_category','del_btn_<?php echo $i; ?>');"><i class="fa fa-trash-o"></i></button>
                                        </td>


                                    </tr>

								<?php 
									$i++;
									}

								?>

                                                    

                                </tbody>

                                

                                

                            </table>

                        </div>

                        <!-- /.table-responsive -->
    
                        <?php
                        }
                        else{
                        
                            echo '<p class="text-center text-muted" style="margin-top: 20px;">No Sub-Category.</p>';
                        }
                        ?>



                                    
                                    
                                    




                            </div>

                        </div>

                        <!-- /.panel -->

                    </div>



                </div><!--.row -->

					




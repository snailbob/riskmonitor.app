                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Update CC

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye text-muted"></i> 

                                	<a href="<?php echo base_url().'webmanager'?>">Portal</a></li>

                                <li class="active">Update Crisis Co-ordinator</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="text-red">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Update Crisis Co-ordinator</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_firstname" name="cc_firstname" placeholder="" required data-msg-required="Please enter first name" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_first_name']) ?>"><?php echo form_error('cc_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cc_lastname" name="cc_lastname" placeholder="" required data-msg-required="Please enter last name" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_last_name']) ?>"><?php echo form_error('cc_lastname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="organisation" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">

                            <input type="email" class="form-control" id="cc_email" name="cc_email" placeholder="" required data-msg-required="Please enter email" readonly="readonly" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_email']); ?>"><?php echo form_error('cc_email'); ?>

                        	</div>

						</div>

                        
                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['calling_code'].'"';
                                        
                                        if ($this->master_model->decryptIt($cc_info[0]['countrycode']) == $countries['calling_code']){
                                            echo ' selected="selected"';
                                        }
                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                            <input type="text" class="form-control" id="cc_phone_number" name="cc_phone_number" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_digits']); ?>"><?php echo form_error('cc_phone_number'); ?>

                            </div>

                        </div>

                        
                        
                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_address" name="cc_address" placeholder="" required data-msg-required="Please enter address" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_address']); ?>"><?php echo form_error('cc_address'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">City</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_city" name="cc_city" placeholder="" required  data-msg-required="Please enter city" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_city']); ?>"><?php echo form_error('cc_city'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">State</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_state" name="cc_state" placeholder="" required  data-msg-required="Please enter state" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_state']); ?>"><?php echo form_error('cc_state'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Zipcode</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_zip_code" name="cc_zip_code" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo $this->master_model->decryptIt($cc_info[0]['crt_zip_code']); ?>"><?php echo form_error('cc_zip_code'); ?>

                            </div>

                        </div>

                         <div class="form-group">

                            <label class="col-sm-2 control-label">User Type</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="cc_type" id="cc_type">

                                    <option value="">Select</option>

                                    <option value="demo" <?php if($cc_info[0]['user_type']=='demo'){echo 'selected="selected"';} ?>>Demo</option>

                                    <option value="live" <?php if($cc_info[0]['user_type']=='live'){echo 'selected="selected"';} ?>>Live</option>

                               </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_cc" id="add_cc">Submit</button>

                            </div>

                        </div>

                        

                        

                    </form>									

                </div>

            </div>

        </div>

    </div>



</div><!--.row-->




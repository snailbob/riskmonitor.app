                <!-- begin PAGE TITLE AREA -->

                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->

                

                

                <div class="row">

                    <div class="col-lg-12">

                        <div class="page-title">

                            <h1>Upload Document for <?php echo $the_packs[0]['name']?>

                                

                            </h1>

                            <ol class="breadcrumb">

                                <li><i class="fa fa-bullseye"></i> 

                                	<a href="<?php echo base_url().'consultant'?>">Portal</a></li>

                                <li class="active">Upload Document</li>



                            </ol>

                        </div>

                    </div>

                    <!-- /.col-lg-12 -->

                </div>

                <!-- /.row -->

                <!-- end PAGE TITLE AREA -->





                



                <!-- Form AREA -->

				<div class="row">

                	<div class="col-lg-12">

                        <?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error : </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>

                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <div class="panel-title">

                                    <h4>Upload Document</h4>

                                </div>

                                <div class="panel-widgets">

                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div id="validationExamples" class="panel-collapse collapse in">

                                <div class="panel-body">

									

						<form action='' name="frm-upload-document" id="frm-upload-document" enctype="multipart/form-data" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">File Input</label>

                            <div class="col-sm-10">

                                <input type="file" name="file_upload_name" id="file_upload_name" value=""/>

                                <p class="small text-muted">(.doc, .docx, .pdf, .txt )</p>

                                <?php echo form_error('file_upload_name'); ?>

                                <div class="hidden text-red" id="file_error"></div>

                            </div>

                        </div>

                        
                       <?php /*?> <div class="form-group">

                            <label class="col-sm-2 control-label">User Group</label>

                            <div class="col-sm-10" style="margin-bottom: 6px;">
                                <select class="form-control" id="group" name="group">
                                    <option value="">Select..</option>	
                                    <option value="0">All</option>	
                                <?php
								
								if(count($all_group)!="0"){
                                    
                                    foreach($all_group as $groups){
                                        
                                        
                                        echo '<option value="'.$groups['id'].'">'.$groups['group_name'].'</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select><?php echo form_error('group'); ?>
                                <div class="hidden text-red" id="group_error"></div>

                            </div>


                        </div><?php */?>


                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                            <a class="btn btn-default" href="<?php echo base_url()?>reseller/packs/view/<?php echo $this->uri->segment(4); ?>">Back</a> 
                            <button type="submit" class="btn btn-primary" name="upload_doc" id="upload_doc">Submit</button>

                            </div>

						</div>

										

										

                                    </form>									

                                </div>

                            </div>

                        </div>

                    </div>



                </div><!--.row-->




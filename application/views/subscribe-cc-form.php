
<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO</title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/2/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    

</head>

<body style="background: #efefef;">

			<?php	$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>'); ?>


    <div class="container">

        <div class="row">

            <div class="col-md-10 col-md-offset-1">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Subscribe to CrisisFlo</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">
    	<?php 

						if($this->session->flashdata('success')!="")

						{

						?>

                        <div class="alert alert-success alert-dismissable">

                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

                        <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?></div>

                        <?php    

						} 

						if($this->session->flashdata('error')!="")

						{

						?>

                        <div class="alert alert-danger alert-dismissable">

						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

						<strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

                        <?php

						} 

						?>                    

                        <div id='login_form'>


						<form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

						

                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_firstname" name="cc_firstname" placeholder="" required data-msg-required="Please enter first name" value="<?php echo set_value('cc_firstname'); ?>"><?php echo form_error('cc_firstname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">

                            <input type="text" class="form-control" id="cc_lastname" name="cc_lastname" placeholder="" required data-msg-required="Please enter last name" value="<?php echo set_value('cc_lastname'); ?>"><?php echo form_error('cc_lastname'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="organisation" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">

                            <input type="email" class="form-control" id="cc_email" name="cc_email" placeholder="" required data-msg-required="Please enter email"><?php echo form_error('cc_email'); ?>

                        	</div>

						</div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">Mobile Number</label>

                            <div class="col-sm-4" style="margin-bottom: 6px;">
                                <select class="form-control" id="countrycode" name="countrycode">
                                    <option value="">Select country code..</option>	
                                <?php if(count($countriescode)!="0"){
                                    
                                    foreach($countriescode as $countries){
                                        
                                        
                                        echo '<option value="'.$countries['calling_code'].'"';
                                        

                                        
                                        echo '>'.$countries['short_name'].' (+'.$countries['calling_code'].')</option>';
                                        
                                    }
                                }
                                
                                ?>
                                </select>

                            </div>
                            <div class="col-sm-6">

                            <input type="text" class="form-control" id="cc_phone_number" name="cc_phone_number" value="<?php echo set_value('cc_phone_number'); ?>" required data-msg-required="Please enter first name"><?php echo form_error('cc_phone_number'); ?>

                            </div>

                        </div>

                        
                        

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Address</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_address" name="cc_address" placeholder="" required data-msg-required="Please enter address" value="<?php echo set_value('cc_address'); ?>"><?php echo form_error('cc_address'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">City</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_city" name="cc_city" placeholder="" required  data-msg-required="Please enter city" value="<?php echo set_value('cc_city'); ?>"><?php echo form_error('cc_city'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">State</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_state" name="cc_state" placeholder="" required  data-msg-required="Please enter state" value="<?php echo set_value('cc_state'); ?>"><?php echo form_error('cc_state'); ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Zipcode</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_zip_code" name="cc_zip_code" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo set_value('cc_zip_code'); ?>"><?php echo form_error('cc_zip_code'); ?>

                            </div>

                        </div>

                         <div class="form-group">

                            <label class="col-sm-2 control-label">User Type</label>

                            <div class="col-sm-10">

                               <select class="form-control" name="cc_type" id="cc_type">

                                    <option value="">Select</option>

                                    <option value="demo" <?php if($this->input->post('cc_type')=='demo'){echo 'selected="selected"';} ?>>Demo</option>

                                    <option value="live" <?php if($this->input->post('cc_type')=='live'){echo 'selected="selected"';} ?>>Live</option>

                               </select>

                            </div>

                        </div>
                        
                        
                         <div class="form-group" style="border-top: 1px #eee solid; padding-top: 15px;"><!--add org-->


                            <label class="col-sm-2 control-label">Organization</label>

                            <div class="col-sm-10">

                                <input type="text" class="form-control" id="cc_org" name="cc_org" placeholder="" required  data-msg-required="Please enter zipcode" value="<?php echo set_value('cc_org'); ?>"><?php echo form_error('cc_org'); ?>

                            </div>


                        </div><!--.add org-->



                        <div class="form-group">

                            <label for="firstname" class="col-sm-2 control-label">Modules</label>

                            <div class="col-sm-10">
								<?php 
								
								foreach($modules as $md=>$mds){
								
									echo '
										  <div class="checkbox">
											<label for="checkboxes-'.$mds['id'].'">
											  <input type="checkbox" name="modules[]" id="checkboxes-'.$mds['id'].'" value="'.$mds['id'].'">
											  <input type="hidden" name="modulesp[]" id="module-'.$mds['id'].'" value="'.$mds['price'].'">'.
											  $mds['name']
											.' -- $'.$mds['price'].'/year</label>
										  </div>
									
									';
								
								
								}
								
								?>
                              <?php /*?><div class="checkbox">
                                <label for="checkboxes-1">
                                  <input type="checkbox" name="modules[]" id="checkboxes-1" value="2">
                                  Case Management --- $250
                                </label>
                              </div>
                              <div class="checkbox">
                                <label for="checkboxes-2">
                                  <input type="checkbox" name="modules[]" id="checkboxes-2" value="3">
                                  Video Conferencing --- $300
                                </label>
                              </div><?php */?>
                              
                              <div class="price_total" style="border-top: solid 1px #ccc; display:none; font-size: 150%; margin-top: 5px;">Total: $<span class="price_tt"></span></div>
                              <?php echo form_error('modules[]'); ?>
                            </div>

                        </div>


                        
                        <!-- Multiple Radios -->
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="radios">Customer Type</label>
                          <div class="col-sm-10">
                          <div class="radio">
                            <label for="radios-0">
                              <input type="radio" name="cust_type" id="radios-0" value="0">
                              AIG Customer
                            </label>
                            </div>
                          <div class="radio">
                            <label for="radios-1">
                              <input type="radio" name="cust_type" id="radios-1" value="1">
                              Not AIG Customer
                            </label>
                            </div>
                              <?php echo form_error('cust_type'); ?>
                          </div>
                        </div>
                        
                        
                        






                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">

                                <button type="submit" class="btn btn-primary" name="add_cc" id="add_cc">Submit</button>

                            </div>

                        </div>

                        

                        

                    </form>									

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/subscribe.js"></script>

</body>

</html>


<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO</title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">

    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->

    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/2/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/2/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    

</head>

<body style="background: #efefef;">



    <div class="container">

        <div class="row">

            <div class="col-md-6 col-md-offset-3">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Subscribe to CrisisFlo</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                    

                        <div id='login_form'>

                            <form action='' method='post' name="admin-login" id="admin-login">

                              <br />

                              

                               <?php if($success!="" || $this->session->flashdata('success')!="") { ?>

                            <div class="alert alert-success alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $success;  echo $this->session->flashdata('success'); ?>

                            </div>

                            <?php } 	 if($error!="")  { ?>
                            <div class="alert alert-danger alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $error;?>
                            </div>

							<?php } ?>

                              

                                <fieldset class="row">

                                    <div class="form-group col-sm-6">

                                        <input class="form-control" placeholder="First Name" type='text' name='first_name' id='first_name' autofocus/>

                                    </div>

                                    <div class="form-group col-sm-6">

                                        <input class="form-control" placeholder="Last Name" type='text' name='last_name' id='last_name'/>

                                    </div>


                                    <div class="form-group col-sm-12">

                                        <input class="form-control" placeholder="Email Address" type='email' name='email_add' id='email_add'/>

                                    </div>


                                    <div class="checkbox" style="display: none">

                                        <label>
                                            <input type="checkbox" name="remember_me" id="remember" value="true"/>Remember Me

                                        </label>

                                    </div>
                                    <div class="form-group col-sm-12">

                                        <input  class="btn btn-bluee btn-block" type='submit' value='Submit' name="btn_cc_login" id="btn_cc_login" />	
                                    </div>
                                </fieldset>

                                <br>

                                <p class="hidden">

                                    <a href="<?php echo base_url().'cc/dashboard/forgotpassword' ?>">Forgot your password?</a>

                                </p>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <!-- JavaScript -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url()?>assets/2/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
$(".btn").click(function() {
    var $btn = $(this);
    $btn.button('loading');
    // simulating a timeout
    setTimeout(function () {
        $btn.button('reset');
    }, 6000);
});
$(".user_name").focus();
});

window.onload = function() {
  var input = document.getElementById("user_name").focus();
}
</script>
</body>

</html>

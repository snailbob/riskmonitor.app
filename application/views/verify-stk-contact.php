<html lang="en">



<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Crisis FLO | <?php echo $page_title; ?></title>


    
    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/2/img/favicon.ico" type="image/x-icon" />
    <link href="<?php echo base_url()?>assets/2/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/2/css/font-awesome.min.css" rel="stylesheet">
    
    <link href='<?php echo base_url()?>assets/2/fonts/google/ubuntu.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='<?php echo base_url()?>assets/2/fonts/google/uopensans.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url()?>assets/2/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/2/css/plugins/dataTables/datatables.css" rel="stylesheet">
    
    
<!-- Popup css--> 

    <link rel="stylesheet" href="<?php echo base_url();?>/assets/2/css/jquery.custombox.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/2/css/demo-popup.css">

<!-- END -->
    <script src="<?php echo base_url()?>assets/2/js/jquery-1.10.2.js"></script>
    
	<script type="text/javascript" language="javascript">var base_url='<?php  echo base_url(); ?>';</script>    


    <!--[if lt IE 9]>

      <script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>

      <script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

    <![endif]-->

    
	<style>
		.panel .login-heading {
		padding: 15px;
		}
		.login-banner {
		margin: 50px 0;
		color: #fff;
		}

		.panel-default {
			border: none;
			}
		.panel-default>.panel-heading {
		color: #4d5055;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border: none;
		}
		.panel-default, .panel-default>.panel-heading {
		border-color: rgba(255,255,255,.5);
		}
		
		.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
		
		.btn-bluee:hover, .btn-bluee:focus, .btn-bluee:active, .btn-bluee.active, .open .dropdown-toggle.btn-bluee {
		border-color: #3374b2;
		color: #fff;
		background-color: #3374b2;
		}
	</style>
    
    
     <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->


</head>

<body style="background: #efefef;">


    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="login-banner text-center">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo "http://crisisflo.com/"; ?>"><img src="<?php echo base_url()?>assets/2/img/crisisflo-logo-medium.png" ></a>

                </div>

                <div class="panel panel-default">


                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Update Contact Details</strong>

                            </h3>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    

                    <div class="panel-body">

                    	<?php if($success!=""){?>

                        <div class="alert alert-success alert-dismissable"><?php echo $success;?></div>

                        <div class="clr"></div>

                        <?php } ?>

						<?php 

						if(count($stk_info)>0)

						{ 

						if($stk_info[0]['contact_verified']==1 && $success==""){ ?>

                        <h4>Your Contact details are already confirmed.</h4><br/>

                        <?php } 

						?>


                        <?php

						if($stk_info[0]['contact_verified']=='2')

						{

						?>

                        <div class="form-group" style="margin-top: 30px;">
                        Name : <?php echo $this->master_model->decryptIt($stk_info[0]['stk_first_name']); ?><br/>

                        Last Name : <?php echo $this->master_model->decryptIt($stk_info[0]['stk_last_name']); ?><br/>

                        Email Address : <?php echo $this->master_model->decryptIt($stk_info[0]['stk_email_address']); ?><br/>

                        <?php /*?>Organisation : <?php echo $stk_info[0]['stk_organisation']; ?><br/><?php */?>

                        Organization : <?php echo $stk_info[0]['stk_position']; ?><br/>

                        Phone Number : <?php echo $this->master_model->decryptIt($stk_info[0]['stk_mobile_num']); ?><br/>

                        <?php /*?><!--Address : <?php echo $this->master_model->decryptIt($stk_info[0]['cc_address']) ?><br />
                        City : <?php echo $this->master_model->decryptIt($stk_info[0]['cc_city']) ?><br /><br />--><?php */?>
                        </div>

                        <form method='post' name='process' class="form-horizontal" role="form" action="">

                        <div class="form-group" style="margin-top: 30px;">

                            <div class="col-sm-offset-1 col-sm-10">

                                <button type="submit" name="verify_correct" id="verify_correct" class="btn btn-primary">Correct</button>

                            

                                <button type="submit" name="verify_incorrect" id="verify_incorrect" class="btn btn-primary">Edit</button>

                            </div>

                        </div>

                        </form>

                       <?php }

 						}

						else

						{

						?>

                         <h4>Please check link, invalid link</h4>

                        <?php	

						}

						if($incorrect=='1')

						{

						?>

						

                        <form method='post' name='edit-contact-info' id="edit-contact-info" class="form-horizontal" role="form" action="">

                        	<div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Name </label>

                                    <input type="text" class="form-control"  name='stk_first_name' id='stk_first_name' value='<?php echo $this->master_model->decryptIt($stk_info[0]['stk_first_name']);?>' placeholder="Enter First Name"><?php echo form_error('stk_first_name');?>

                                    <input type="hidden" name="login_id" id="login_id" value="<?php echo $stk_info[0]['login_id'];?>">

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Last Name</label>

                                    <input type="text" class="form-control" name='stk_last_name' id="stk_last_name" value="<?php echo $this->master_model->decryptIt($stk_info[0]['stk_last_name']);?>" placeholder="Enter Last Name"><?php echo form_error('stk_last_name');?>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Position</label>

                                    <input type="text" class="form-control" id="stk_position" name="stk_position" value="<?php echo $stk_info[0]['stk_position'];?>" placeholder="Enter Position"><?php echo form_error('stk_position');?>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                	<label for="exampleInputEmail1">Phone Number</label>

                                    <input type="text" class="form-control" id="stk_mobile_num" name="stk_mobile_num" value="<?php echo $this->master_model->decryptIt($stk_info[0]['stk_mobile_num']);?>" placeholder="Enter Mobile Number"><?php echo form_error('stk_mobile_num');?>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-10">

                                    <button type="submit" name="edit_info" id="edit_info" class="btn btn-primary">Submit Edit</button>

                                </div>

                            </div>

                        </form>

						<?php } ?>	

                    </div>

                    

                </div>

            </div>

        </div>

    </div>

</body>

</html>
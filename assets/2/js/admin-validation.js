// JavaScript Document

$(document).ready(function () {
  $('#utype-0').click();

  $('.div-controller').removeClass('hidden');


  $('.summernote').summernote({
    height: 170,
    oninit: function () {
      //$("div.note-editor button[data-event='codeview']").click();
    }
  });

  $('.email_blue_btn').on('click', function () {
    $(this).hide();
    $('.btn_form').show();
  });

  $('.btn_form').find('.btn-default').on('click', function () {
    $('.btn_form').hide();
    $('.email_blue_btn').show();
    return false;
  });


  $('.btn_form').find('.btn-primary').on('click', function () {
    var $self = $(this);
    $self.button('loading');
    var txt = $('.btn_form').find('input').val();
    var id = $('.btn_form').find('input').data('id');
    $.post(
      base_url + 'webmanager/emailer/button_text', {
        txt: txt,
        id: id
      },
      function (res) {
        console.log(res);
        $('.email_blue_btn').html(txt);
        $('.btn_form').find('.btn-default').click();
        $self.button('reset');
      },
      'json'

    ).error(function (err) {
      console.log(err);
    });
    return false;
  });


  $('.save_pages_contents_btn').on('click', function () {
    var $self = $(this);
    var data = $self.data();
    data.icon = $('[name="icon"]').val();
    data.titlefield = $('[name="titlefield"]').val();
    data.contentfield = ($('.summernote').length > 0) ? $self.closest('div').find('.summernote').code() : '';

    console.log(data);

    $self.addClass('disabled').html('Loading..');

    $.post(
      base_url + 'webmanager/contents/savetexts',
      data,
      function (res) {
        console.log(res);

        if (data.id == '') {
          $('#pagetype').data('id', res.id);
          window.location.href = base_url + 'webmanager/contents/alternate/' + res.id;
        } else {
          bootbox.alert('<h4 class="modal-title">Changes to ' + data.title + ' successully saved!</h4>');
          $self.removeClass('disabled').html('Save Changes');
        }

      },
      'json'
    ).error(function (err) {
      console.log(err);
    });

    return false;

  });


  //profile upload
  $("#myFormAvatar").ajaxForm({
    beforeSend: function () {
      $(".progress-profile").show();
      //clear everything
      $(".bar-profile").width('0%');
      $(".percent-profile").html("0%");
    },
    uploadProgress: function (event, position, total, percentComplete) {
      $(".bar-profile").width(percentComplete + '%');
      $(".percent-profile").html(percentComplete + '%');


    },
    success: function () {
      $(".bar-profile").width('100%');
      $(".percent-profile").html('100%');

    },
    complete: function (response) {
      if (response.responseText == 'not_img') {
        $(".progress-profile").hide();
        bootbox.alert('<p class="lead">File selected is not a valid image.</p>');

      } else {
        $('#avatar_name').val(response.responseText);
        $('.bootstrap-modal-cropper img').attr('src', base_url + '' + response.responseText);
        $("#cropImg").find('.modal-body').css('min-height', '560');
        $("#cropImg").modal('show');
      }
      console.log(response.responseText);

    },
    error: function () {
      bootbox.alert('<h4> Unable to upload file. Please refresh and try again.</h4>');
    }

  });
  //end ajax file upload


  //crop on a modal
  var $modal = $("#cropImg"),
    $image = $modal.find(".bootstrap-modal-cropper > img"),
    originalData = {};


  var aspratio = 1 / 1;
  //202/61; 
  //		if($('#customer_img').length > 0 || $('#pagetype').val() == 'threeparts' || $('#pagetype').val() == 'banner'){
  //			aspratio = 1 / 1;
  //		}
  if ($('#pagetype').val() == 'firsthomesection') {
    aspratio = 740 / 318;
  }
  if ($('#pagetype').val() == 'alternatingcontents') {
    aspratio = 527 / 259;
  }
  if ($('#pagetype').val() == 'banner') {
    aspratio = 1800 / 647;
  }

  $modal.on("shown.bs.modal", function () {
    $image.cropper({
      aspectRatio: aspratio,
      multiple: true,
      data: originalData,

      strict: true,
      guides: false,
      highlight: false,
      dragCrop: false,
      cropBoxMovable: false,
      cropBoxResizable: false,

      done: function (data) {
        console.log(data);
      }
    });
  }).on("hidden.bs.modal", function () {
    originalData = $image.cropper("getData");
    $image.cropper("destroy");
  });



  //save crop image
  $('.sav_crop').click(function (e) {
    var aspect = {
      width: 202,
      height: 61
    };

    if ($('#customer_img').length > 0) {
      aspect = {
        width: 150,
        height: 150
      };
    }
    if ($('#pagetype').val() == 'firsthomesection') {
      aspect = {
        width: 740,
        height: 318
      };
    }
    if ($('#pagetype').val() == 'alternatingcontents') {
      aspect = {
        width: 527,
        height: 259
      };
    }
    if ($('#pagetype').val() == 'threeparts') {
      aspect = {
        width: 152,
        height: 152
      };
    }
    if ($('#pagetype').val() == 'banner') {
      aspect = {
        width: 1800,
        height: 647
      };
    }
    if ($('#pagetype').val() == 'collabs') {
      aspect = {
        width: 29,
        height: 29
      };
    }

    var hey = $image.cropper('getCroppedCanvas', aspect).toDataURL();
    //$image.cropper("getDataURL", "image/jpeg");
    console.log(hey);
    $(this).addClass('disabled');
    $(this).html('Loading...');

    var data = {
      "img_url": hey,
      "img_name": $('#avatar_name').val(),
      "page_type": ($('#pagetype').length > 0) ? $('#pagetype').val() : '',
      "page_id": ($('#pagetype').length > 0) ? $('#pagetype').data('id') : '',
      "img_type": $('#img_type').val()
    };

    data = $.param(data); // $(this).serialize() + "&" +			

    $.ajax({
      type: "POST",
      url: base_url + "webmanager/contents/save_profile_pic",
      dataType: 'json',
      data: data,
      success: function (data) {
        console.log(data);

        if ($('#pagetype').length > 0) {
          $('.img-page-preview').attr('src', hey).closest('p').removeClass('hidden');
          $('.modal').modal('hide');

        } else {
          window.location.reload();
        }

      },
      error: function (err) {
        console.log(err);
      }
    });

  });











  //validation for Admin Change Password

  $("#change_admin_pw").click(function ()

    {

      var password = $("#password");

      var email = $("#email");

      var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;



      $('#password').removeClass('error_border');

      $('#email').removeClass('error_border');



      $('#password').addClass('form-control');

      $('#email').addClass('form-control');



      if (password.val() == "")

      {

        password.val('');

        $('#password').attr('placeholder', 'This field is required');

        $('#password').removeClass('form-control');

        $('#password').addClass('error_border');

        return false;

      } else if (email.val() == "")

      {

        email.val('');

        $('#email').attr('placeholder', 'This field is required');

        $('#email').removeClass('form-control');

        $('#email').addClass('error_border');

        return false;

      } else if (!filter.test(email.val()))

      {

        document.getElementById('email').val() = "";

        $('#email').attr('placeholder', 'Invalid Email ID');

        $('#email').removeClass('form-control');

        $('#email').addClass('error_border');

        return false;

      }

    });


  $('#upload_doc').on("click", function () {

    // $("input:file").change(function (){
    //   var fileName = $(this).val();

    //var group=$("#group");

    var fileName = $("#file_upload_name").val();

    if (fileName == "") {
      $('#file_error').removeClass('hidden');
      $('#file_error').addClass('visible');
      $('#file_error').html('File Input is required.');

      $('#file_upload_name').focus();
      return false;
    }


    if (fileName.lastIndexOf("doc") === fileName.length - 3 || fileName.lastIndexOf("docx") === fileName.length - 4 || fileName.lastIndexOf("pdf") === fileName.length - 3 || fileName.lastIndexOf("txt") === fileName.length - 3 || fileName.lastIndexOf("DOC") === fileName.length - 3 || fileName.lastIndexOf("DOCX") === fileName.length - 4 || fileName.lastIndexOf("PDF") === fileName.length - 3 || fileName.lastIndexOf("TXT") === fileName.length - 3 && group.val() != "") {
      $('#file_error').removeClass('visible');
      $('#file_error').addClass('hidden');
      $('#group_error').removeClass('visible');
      $('#group_error').addClass('hidden');


      var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><p class="lead">';

      //<div class="modal-header"><h4 class="modal-title" id="flexModalLabel">CRT successfully added!</h4></div><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

      var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Uploading.. ';

      var myModal3 = '</p><p>Please wait, do not close the window until the upload ends.<br><br>The time required for this operation depends on the file size, the net load and your connection speed</p></div></div></div> ';

      var myModal = myModal1 + myModal2 + myModal3;


    } else {
      $('#file_error').removeClass('hidden');
      $('#file_error').addClass('visible');
      $('#file_error').html('Valid File Input is required.');

      $('#group_error').removeClass('visible');
      $('#group_error').addClass('hidden');
      $('#file_upload_name').focus();
      return false;
    }







    // });


  });










  //validation for admin update emails

  $("#admin_update_email").click(function ()

    {

      var contact_email = $("#contact_email");

      var info_email = $("#info_email");

      var support_email = $("#support_email");

      var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;



      $('#contact_email').removeClass('error_border');

      $('#info_email').removeClass('error_border');

      $('#support_email').removeClass('error_border');



      $('#contact_email').addClass('form-control');

      $('#info_email').addClass('form-control');

      $('#support_email').addClass('form-control');



      if (contact_email.val() == "")

      {

        contact_email.val('');

        $('#contact_email').attr('placeholder', 'Please Enter Contact Email');

        $('#contact_email').removeClass('form-control');

        $('#contact_email').addClass('error_border');

        return false;

      } else if (!filter.test(contact_email.val()))

      {

        contact_email.val('');

        $('#contact_email').attr('placeholder', 'Invalid Email ID');

        $('#contact_email').removeClass('form-control');

        $('#contact_email').addClass('error_border');

        return false;

      } else if (info_email.val() == "")

      {

        info_email.val('');

        $('#info_email').attr('placeholder', 'This field is required');

        $('#info_email').removeClass('form-control');

        $('#info_email').addClass('error_border');

        return false;

      } else if (!filter.test(info_email.val()))

      {

        info_email.val('');

        $('#info_email').attr('placeholder', 'Invalid Email ID');

        $('#info_email').removeClass('form-control');

        $('#info_email').addClass('error_border');

        return false;

      } else if (support_email.val() == "")

      {

        support_email.val('');

        $('#support_email').attr('placeholder', 'This field is required');

        $('#support_email').removeClass('form-control');

        $('#support_email').addClass('error_border');

        return false;

      } else if (!filter.test(support_email.val()))

      {

        support_email.val('');

        $('#support_email').attr('placeholder', 'Invalid Email ID');

        $('#support_email').removeClass('form-control');

        $('#support_email').addClass('error_border');

        return false;

      }



    });

  //validation for add & update CC

  $("#add_cc").click(function () {

    var cc_firstname = $("#cc_firstname");

    var cc_lastname = $("#cc_lastname");

    var cc_email = $("#cc_email");

    var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;

    var ccode = $("#countrycode");

    var cc_phone_number = $("#cc_phone_number");

    var cc_address = $("#cc_address");

    var cc_city = $("#cc_city");

    var cc_state = $("#cc_state");

    var cc_zip_code = $("#cc_zip_code");

    var zipCodePattern = /^[0-9a-zA-Z]+$/;

    var cc_type = $("#cc_type");

    var cc_org = $("#cc_org");





    $('#cc_firstname').removeClass('error_border');

    $('#cc_lastname').removeClass('error_border');

    $('#cc_email').removeClass('error_border');

    $("#countrycode").removeClass('error_border');

    $('#cc_phone_number').removeClass('error_border');

    $('#cc_address').removeClass('error_border');

    $('#cc_city').removeClass('error_border');

    $('#cc_state').removeClass('error_border');

    $('#cc_zip_code').removeClass('error_border');

    $('#cc_type').removeClass('error_border');

    $('#cc_org').removeClass('error_border');





    $('#cc_firstname').addClass('form-control');

    $('#cc_lastname').addClass('form-control');

    $('#cc_email').addClass('form-control');

    $("#countrycode").addClass('form-control');

    $('#cc_phone_number').addClass('form-control');

    $('#cc_address').addClass('form-control');

    $('#cc_city').addClass('form-control');

    $('#cc_state').addClass('form-control');

    $('#cc_zip_code').addClass('form-control');

    $('#cc_type').addClass('form-control');

    $('#cc_org').addClass('form-control');



    //validate max users

    $('#maxu5_error').html('');
    $('#maxu2_error').html('');
    $('#maxu1_error').html('');
    $('#maxu4_error').html('');

    if ($('#checkboxes-1').is(":checked")) {
      if ($('#maxu1').val() == '0') {
        $('#maxu1').focus();
        $('#maxu1_error').html('Mandatory field');
        return false;
      }
    }

    if ($('#checkboxes-5').is(":checked")) {
      if ($('#maxu5').val() == '0') {
        $('#maxu5').focus();
        $('#maxu5_error').html('Mandatory field');
        return false;
      }
    }

    if ($('#checkboxes-2').is(":checked")) {
      if ($('#maxu2').val() == '0') {
        $('#maxu2').focus();
        $('#maxu2_error').html('Mandatory field');
        return false;
      }
    }

    if ($('#checkboxes-4').is(":checked")) {
      if ($('#maxu4').val() == '0') {
        $('#maxu4').focus();
        $('#maxu4_error').html('Mandatory field');
        return false;
      }
    }








    if (cc_firstname.val() == "") {

      cc_firstname.val('');

      $('#cc_firstname').attr('placeholder', 'Enter First Name');

      $('#cc_firstname').removeClass('form-control');

      $('#cc_firstname').addClass('error_border');

      $('#cc_firstname').focus();

      return false;



    } else if (cc_lastname.val() == "") {

      cc_lastname.val('');

      $('#cc_lastname').attr('placeholder', 'Enter Last Name');

      $('#cc_lastname').removeClass('form-control');

      $('#cc_lastname').addClass('error_border');

      $('#cc_lastname').focus();

      return false;



    } else if (cc_email.val() == "") {

      cc_email.val('');

      $('#cc_email').attr('placeholder', 'Enter Email Id');

      $('#cc_email').removeClass('form-control');

      $('#cc_email').addClass('error_border');

      $('#cc_email').focus();

      return false;

    } else if (!filter.test(cc_email.val())) {

      cc_email.val('');

      $('#cc_email').attr('placeholder', 'Invalid Email ID');
      $('#cc_email').removeClass('form-control');
      $('#cc_email').addClass('error_border');
      $('#cc_email').focus();

      return false;

    }

    //			else if(ccode.val()=="") {
    //
    //				//cc_phone_number.val('');
    //
    //				//$('#cc_phone_number').attr('placeholder','Enter Phone Number');
    //
    //				ccode.removeClass('form-control');
    //
    //				ccode.addClass('error_border');
    //
    //				ccode.focus();
    //
    //				return false;
    //
    //			}
    //
    //			else if(cc_phone_number.val()=="") {
    //
    //				cc_phone_number.val('');
    //
    //				$('#cc_phone_number').attr('placeholder','Enter Phone Number');
    //
    //				$('#cc_phone_number').removeClass('form-control');
    //
    //				$('#cc_phone_number').addClass('error_border');
    //
    //				$('#cc_phone_number').focus();
    //
    //				return false;
    //
    //			}
    //
    //			else if(isNaN(cc_phone_number.val())) {
    //
    //				cc_phone_number.val('');
    //
    //				$('#cc_phone_number').attr('placeholder','Enter  Number');
    //
    //				$('#cc_phone_number').removeClass('form-control');
    //
    //				$('#cc_phone_number').addClass('error_border');
    //
    //				$('#cc_phone_number').focus();
    //
    //				return false;
    //
    //			}
    //
    //			else if(cc_address.val()=="") {
    //
    //				cc_address.val('');
    //
    //				$('#cc_address').attr('placeholder','Enter Address');
    //
    //				$('#cc_address').removeClass('form-control');
    //
    //				$('#cc_address').addClass('error_border');
    //
    //				$('#cc_address').focus();
    //
    //				return false;
    //
    //			}
    //
    //		    else if(cc_city.val()=="") {
    //
    //				cc_city.val('');
    //
    //				$('#cc_city').attr('placeholder','Enter City');
    //
    //				$('#cc_city').removeClass('form-control');
    //
    //				$('#cc_city').addClass('error_border');
    //
    //				$('#cc_city').focus();
    //
    //				return false;
    //
    //			}
    //
    //			else if(cc_state.val()=="") {
    //
    //				cc_state.val('');
    //
    //				$('#cc_state').attr('placeholder','Enter State');
    //
    //				$('#cc_state').removeClass('form-control');
    //
    //				$('#cc_state').addClass('error_border');
    //
    //				$('#cc_state').focus();
    //
    //				return false;
    //
    //			}
    //
    //			else if(cc_zip_code.val()=="") {
    //
    //				cc_zip_code.val('');
    //
    //				$('#cc_zip_code').attr('placeholder','Enter Zip Code');
    //
    //				$('#cc_zip_code').removeClass('form-control');
    //
    //				$('#cc_zip_code').addClass('error_border');
    //
    //				$('#cc_zip_code').focus();  
    //
    //				return false;
    //
    //			}
    //
    //			else if(!zipCodePattern.test(cc_zip_code.val())) {
    //
    //				cc_zip_code.val('');
    //
    //				$('#cc_zip_code').attr('placeholder','Enter Zip Code');
    //
    //				$('#cc_zip_code').removeClass('form-control');
    //
    //				$('#cc_zip_code').addClass('error_border');
    //
    //				$('#cc_zip_code').focus();  
    //
    //				return false;
    //
    //			}
    else if (cc_type.val() == "") {

      cc_type.val('');

      $('#cc_type').attr('placeholder', 'Enter Zip Code');

      $('#cc_type').removeClass('form-control');

      $('#cc_type').addClass('error_border');

      $('#cc_type').focus();

      return false;

    } else if (cc_org.val() == "") {

      cc_org.val('');

      $('#cc_org').attr('placeholder', 'Enter Organization');

      $('#cc_org').removeClass('form-control');

      $('#cc_org').addClass('error_border');

      $('#cc_org').focus();

      return false;

    } else if ($('input[name="modules[]"]:checked').length == 0) {
      $('#modules_error').html('Mandatory field');
      return false;

    } else {
      return true;
    }

  });






  //validation for add & update CC

  $("#update_cc").click(function ()

    {

      var cc_firstname = $("#cc_firstname");

      var cc_lastname = $("#cc_lastname");

      var cc_email = $("#cc_email");

      var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;

      var ccode = $("#countrycode");

      var cc_phone_number = $("#cc_phone_number");

      var cc_address = $("#cc_address");

      var cc_city = $("#cc_city");

      var cc_state = $("#cc_state");

      var cc_zip_code = $("#cc_zip_code");

      var zipCodePattern = /^[0-9a-zA-Z]+$/;

      var cc_type = $("#cc_type");

      var cc_org = $("#cc_org");





      $('#cc_firstname').removeClass('error_border');

      $('#cc_lastname').removeClass('error_border');

      $('#cc_email').removeClass('error_border');

      $("#countrycode").removeClass('error_border');

      $('#cc_phone_number').removeClass('error_border');

      $('#cc_address').removeClass('error_border');

      $('#cc_city').removeClass('error_border');

      $('#cc_state').removeClass('error_border');

      $('#cc_zip_code').removeClass('error_border');

      $('#cc_type').removeClass('error_border');




      $('#cc_firstname').addClass('form-control');

      $('#cc_lastname').addClass('form-control');

      $('#cc_email').addClass('form-control');

      $("#countrycode").addClass('form-control');

      $('#cc_phone_number').addClass('form-control');

      $('#cc_address').addClass('form-control');

      $('#cc_city').addClass('form-control');

      $('#cc_state').addClass('form-control');

      $('#cc_zip_code').addClass('form-control');

      $('#cc_type').addClass('form-control');




      if (cc_firstname.val() == "") {

        cc_firstname.val('');

        $('#cc_firstname').attr('placeholder', 'Enter First Name');

        $('#cc_firstname').removeClass('form-control');

        $('#cc_firstname').addClass('error_border');

        $('#cc_firstname').focus();

        return false;



      } else if (cc_lastname.val() == "") {

        cc_lastname.val('');

        $('#cc_lastname').attr('placeholder', 'Enter Last Name');

        $('#cc_lastname').removeClass('form-control');

        $('#cc_lastname').addClass('error_border');

        $('#cc_lastname').focus();

        return false;



      } else if (cc_type.val() == "") {

        cc_type.val('');

        $('#cc_type').attr('placeholder', 'Enter Zip Code');

        $('#cc_type').removeClass('form-control');

        $('#cc_type').addClass('error_border');

        $('#cc_type').focus();

        return false;

      } else {
        return true;
      }

    });






  //validation for add & update organization

  $("#add_orgz").click(function ()

    {

      var organization_name = $("#organization_name");

      var sel_cc = $("#sel_cc");



      $('#organization_name').removeClass('error_border');

      $('#sel_cc').removeClass('error_border');



      $('#organization_name').addClass('form-control');

      $('#sel_cc').addClass('form-control');


      //validate max users

      $('#maxu5_error').html('');
      $('#maxu2_error').html('');
      $('#maxu1_error').html('');
      $('#maxu4_error').html('');
      $('#maxu8_error').html('');

      if ($('#checkboxes-1').is(":checked")) {
        if ($('#maxu1').val() == '0') {
          $('#maxu1').focus();
          $('#maxu1_error').html('Mandatory field');
          return false;
        }
      }

      if ($('#checkboxes-5').is(":checked")) {
        if ($('#maxu5').val() == '0') {
          $('#maxu5').focus();
          $('#maxu5_error').html('Mandatory field');
          return false;
        }
      }

      if ($('#checkboxes-2').is(":checked")) {
        if ($('#maxu2').val() == '0') {
          $('#maxu2').focus();
          $('#maxu2_error').html('Mandatory field');
          return false;
        }
      }

      if ($('#checkboxes-4').is(":checked")) {
        if ($('#maxu4').val() == '0') {
          $('#maxu4').focus();
          $('#maxu4_error').html('Mandatory field');
          return false;
        }
      }

      if ($('#checkboxes-8').is(":checked")) {
        if ($('#maxu8').val() == '0') {
          $('#maxu8').focus();
          $('#maxu8_error').html('Mandatory field');
          return false;
        }
      }





      if (organization_name.val() == "")

      {

        organization_name.val('');

        $('#organization_name').attr('placeholder', 'Enter Organization name');

        $('#organization_name').removeClass('form-control');

        $('#organization_name').addClass('error_border');

        $('#organization_name').focus();

        return false;

      } else if ($('input[name="modules[]"]:checked').length == 0) {
        $('#modules_error').html('Mandatory field');
        return false;

      } else if (sel_cc.val() == "")

      {

        sel_cc.val('');

        $('#sel_cc').attr('placeholder', 'Enter Organization name');

        $('#sel_cc').removeClass('form-control');

        $('#sel_cc').addClass('error_border');
        $('#modules_error').html('');
        $('#sel_cc').focus();

        return false;

      }



    });

  //validation for login dashbord



  $("#btn_admin_login").click(function ()

    {

      var user_name = $("#user_name");

      var pass_word = $("#pass_word");



      $('#user_name').removeClass('error_border');

      $('#pass_word').removeClass('error_border');



      $('#user_name').addClass('form-control');

      $('#pass_word').addClass('form-control');



      if (user_name.val() == "")

      {

        user_name.val('');

        $('#user_name').attr('placeholder', 'Enter User Name');

        $('#user_name').removeClass('form-control');

        $('#user_name').addClass('error_border');

        $('#user_name').focus();

        return false;

      } else if (pass_word.val() == "")

      {

        pass_word.val('');

        $('#pass_word').attr('placeholder', 'Enter Password');

        $('#pass_word').removeClass('form-control');

        $('#pass_word').addClass('error_border');

        $('#pass_word').focus();

        return false;

      }

    });



  //toggle socialize case 
  $('input#c_indi').click(function () {
    if ($('input#c_indi').is(':checked')) {
      $('div.share_indi').fadeIn();
      $('.add_orgzz_btn').html('<button type="submit" class="btn btn-primary" name="add_orgzz" id="add_orgzz">Submit</button>');
      $('div.share_group').hide();
    }
  });

  $('input#c_group').click(function () {
    if ($('input#c_group').is(':checked')) {
      $('div.share_group').fadeIn();
      $('#add_orgzz').remove();
      $('div.share_indi').hide();
    }
  });

  //display max users on checked modules
  check_module('1');
  check_module('2');
  check_module('4');
  check_module('8');
  check_module('5');

});


function deleteAlternate(id) {
  var del_mess = '<p class="lead">Please confirm you wish to delete content?</p>';


  bootbox.confirm(del_mess, function (e) {

    if (e) {
      window.location.assign(base_url + "webmanager/contents/delete_alt/" + id);

    }
  });

};

function check_module(id) {

  if ($('#checkboxes-' + id).is(":checked")) {
    $('#maxu' + id).closest('div.form-group').slideDown('fast');
  } else {
    $('#maxu' + id).closest('div.form-group').slideUp('fast');
  }
}

function del_confirm(site_path)

{

  $.messager.confirm('Confirm:', "Are you sure you want to delete ?", function (r)

    {

      if (r)

      {

        location.href = site_path;

      } else

      {
        return false;
      }

    });

}

function del_confirmed(site_path) {

  bootbox.confirm("<p class='lead'>Are you sure you want to delete selected item?</p>", function (r) {
    if (r) {
      location.href = site_path;
    }
  });
}

function activate_confirmed(site_path) {

  bootbox.confirm("<p class='lead'>Please confirm you want to activate CC?</p>", function (r) {
    if (r) {
      location.href = site_path;
    }
  });
};

function toggleOrgStat(org_id, status, customer_type, login_id, user_status) {

  var stat_text = '';

  if (status == 'Y') {
    stat_text = 'disable';
  } else {
    stat_text = 'enable';
  }

  bootbox.confirm('<p class="lead">Please confirm you want to ' + stat_text + ' selected organization?</p>', function (r) {
    if (r) {

      if (customer_type == 'trialcustomers' && user_status != '1') {
        $.ajax({
          type: "POST",

          url: base_url + 'webmanager/customers/resend_request',

          data: {
            login_id: login_id
          },

          success: function (data) {
            console.log(data);
            window.location.href = base_url + 'webmanager/' + customer_type + '/org_status/' + status + '/' + org_id;
          }
        });


      } else {
        window.location.href = base_url + 'webmanager/' + customer_type + '/org_status/' + status + '/' + org_id;
      }


    }
  });

};


function resendCC(login_id) {

  bootbox.confirm("<p class='lead'>Please confirm you wish to re-send email request?</p>", function (r) {

    if (r) {

      $.ajax({
        type: "POST",

        url: base_url + 'webmanager/customers/resend_request',

        data: {
          login_id: login_id
        },

        success: function (data) {

          console.log(data);
          bootbox.alert('<p class="lead">Email request successfully re-sent.</p>');
          //window.location.reload(true);

        }

      });
    }
  });

}



function saveNewPack() {
  var name = $('#pack_name').val();
  if (name == '') {
    $('#newRecallPack').find('span.text-red').show();
    $('#newRecallPack').find('span.text-red').html('Mandatory field');
    return false;
  } else {
    $('#newRecallPack').find('button.btn-primary').addClass('disabled');
    $('#newRecallPack').find('button.btn-primary').html('Loading...');

    $('#newRecallPack').find('span.text-red').hide();

    $.ajax({
      type: "POST",

      url: base_url + 'webmanager/recall/save_recall_pack',

      data: {
        name: name

      },

      success: function (data) {
        console.log(data);
        window.location.reload(true);

      }

    });
  }
};

function delRecallPack(id, del_btn) {

  bootbox.confirm("<p class='lead'>Delete Recall Input Pack?.</p>", function (r) {
    if (r) {

      $.ajax({
        type: "POST",

        url: base_url + 'webmanager/recall/delete_recall_pack',

        data: {
          id: id

        },

        success: function (data) {
          console.log(data);

          if (data == 'success') {
            $("#" + del_btn).closest('tr').addClass('bg-danger');
            $("#" + del_btn).closest('tr').fadeOut();
          }

        }

      });

    }
  });
}


$(document).ready(function (e) {
  $('select#select_pack_cat').on('change', function () {
    $('#sub_cat').html('<i class="fa fa-spinner fa-spin"></i>');
    var step = this.value;
    var pack_id = $('#pack_id').val();

    $.ajax({
      type: "POST",

      url: base_url + 'webmanager/recall/select_pack_step',

      data: {
        step: step,
        pack_id: pack_id
      },

      success: function (data) {
        console.log(data);
        $('#sub_cat').html(data);

      }

    });


  }); //SELECT STEP


});

function saveNewPackTask() {
  var tass = $('#text_task').val();
  var task_guidance = $('#text_task_guidance').val();
  var catt = $('#select_pack_cat').val();
  var scatt = $('#select_subcat').val();
  var pack_id = $('#pack_id').val();

  if (tass == '') {
    $('.task_err').show();
    $('.task_err').html('Mandatory field');
    return false;
  } else if (catt == '') {
    $('.category_err').show();
    $('.category_err').html('Mandatory field');
    $('.task_err').html('');
    return false;
  } else {

    $.ajax({
      type: "POST",

      url: base_url + 'webmanager/recall/save_new_pack_task',

      data: {
        task: tass,
        task_guidance: task_guidance,
        category: catt,
        sub_cat: scatt,
        pack_id: pack_id
      },

      success: function (data) {
        console.log(data);
        //event.target
        window.location.reload(true);
      }

    }); //ajax

  }
} //saveNewTask

function addToInputPack(input_pack_id) {
  if ($('#select_rc_pack').val() == '') {
    $('.rc_pack_err').html('Mandatory field');
    return false;
  } else {
    $('.rc_pack_err').html('');

    $.ajax({
      type: "POST",

      url: base_url + 'webmanager/packs/add_recall_pack',

      data: {
        rc_pack: $('#select_rc_pack').val(),
        input_pack_id: input_pack_id
      },

      success: function (data) {
        console.log(data);
        window.location.reload(true);
      }

    }); //ajax


  }
}

function planDateId(cust_date_id) {
  $('#cust_date_id').val(cust_date_id);

}

$(document).ready(function (e) {

  $('#faq_form').find('textarea').on('keyup', function () {
    if ($('#question').val() != '' && $('#answer').val() != '') {
      $('#faq_form').find('button').removeClass('disabled');
    } else {
      $('#faq_form').find('button').addClass('disabled');
    }
  });

  $('#faq_form').on('submit', function () {
    console.log($(this).serialize());

    $.post(
      base_url + 'webmanager/faq/add_faq',
      $(this).serialize(),
      function (response) {
        console.log(response);
        window.location.reload(true);
      }
    );
    return false;
  });


  $('.delete_btn').on('click', function () {
    var $self = $(this);
    var id = $self.attr('data-id');
    bootbox.confirm('<p class="lead">Please confirm you wish to delete item.</p>', function (e) {

      if (e) {
        $.post(
          base_url + 'webmanager/faq/delete', {
            id: id
          },
          function (response) {
            console.log(response);
            $self.closest('tr').addClass('bg-danger').fadeOut('fast');
          },
          'json'
        );

      }
    });


  });


  //admin reset db
  $('.reset_db_btn').on('click', function () {
    bootbox.confirm('<p class="lead">Please confirm you wish to reset database.<br><small class="text-muted">(This will delete CCs/CRTs, Organizations, etc.)</small></p>', function (e) {
      if (e) {
        $('.reset_db_btn').html('<i class="fa fa-spinner fa-spin"></i> Processing..').addClass('disabled');
        $.post(
          base_url + 'webmanager/settings/resetdb', {
            action: 'reset'
          },
          function (response) {
            console.log(response);
            //window.location.reload(true);
            $('.reset_db_btn').html('<i class="fa fa-check-circle"></i> Success!').addClass('disabled');
          }
        ).error(function () {
          bootbox.alert('Something went wrong. Please try again later.');
        });
      }
    });
  });


});

function getFaq(id) {

  $.post(
    base_url + 'webmanager/faq/get_info', {
      id: id
    },
    function (response) {
      console.log(response);
      $('#question').val(response.question);
      $('#answer').val(response.answer);
      $('#faq_id').val(id);
      $('#faqModal').modal('show');
    },
    'json'
  );
}


$(document).ready(function (e) {

  if ($('input[value="11"]').is(':checked')) {
    $('.simulation_module').slideDown('fast');
  }

  $('input[value="11"]').on('change', function () {
    console.log('sdf');
    if ($(this).is(':checked')) {
      $('.simulation_module').slideDown('fast');
    } else {
      $('.simulation_module').slideUp('fast');
    }
  });

});


$(document).ready(function (e) {

  $('.view_simu_list_btn').on('click', function () {
    var id = $(this).attr('data-id');
    $('#viewSimulationModal').find('p.lead.text-center').show().siblings().hide();
    $('#accordion').html('');
    $.post(
      base_url + 'webmanager/organization/simulationlist', {
        id: id
      },
      function (response) {
        console.log(response);
        if (response.simulations.length > 0) {
          $(response.simulations).each(function (index, element) {
            var the_content = '<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading' + index + '"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse' + index + '">' + response.simulations[index].name + '</a></h4></div><div id="collapse' + index + '" class="panel-collapse collapse" role="tabpanel"><div class="panel-body">' + response.simulations[index].content;

            var simulation_id = response.simulations[index].id;
            $(response.simulations_media).each(function (index, element) {
              var media_id = response.simulations_media[index].simulation_id;
              var media_ext = response.simulations_media[index].ext;

              if (media_id == simulation_id) {

                if (media_ext == 'mp4' || media_ext == 'wmv' || media_ext == 'mov' || media_ext == 'avi') {
                  the_content += '<p>' + response.simulations_media[index].name + '<br><video style="width: 100% !important; height: auto !important;" controls><source src="' + response.simulations_media[index].file_url + '" type="video/mp4">Your browser does not support the video tag.</video></p>';


                } else if (media_ext == 'mp3' || media_ext == 'wav') {
                  the_content += '<p>' + response.simulations_media[index].name + '<br><audio controls><source src="' + response.simulations_media[index].file_url + '" type="audio/mpeg">Your browser does not support the audio element.</audio></p>';

                }

                //the_content += '<p class="">'+response.simulations_media[index].name+'</p>';
              }
            });

            the_content += '</div></div></div>';

            $('#accordion').append(the_content);
          });
        } else {
          $('#accordion').append('No simulation found.');
        }
        $('#viewSimulationModal').find('p.lead.text-center').hide().siblings().show();
      },
      'json'
    );

    $('#viewSimulationModal').modal('show');
    return false;
  });

});


$(document).ready(function (e) {

  $('.filter_outgoings').find('a').on('click', function () {
    var search_text = $(this).html();
    $(this).closest('li').addClass('active');
    $(this).closest('li').siblings().removeClass('active');
    if (search_text == 'All') {
      search_text = '';
    }
    $('.dataTables_filter').find('input[type="search"]').val(search_text).keyup();

    if ($(this).closest('.btn-group').hasClass('open')) {
      $(this).closest('.btn-group').find('.dropdown-backdrop').remove();
    }

    $(this).closest('.btn-group').toggleClass('open');

    return false;
  });

  $('.view_org_details').on('click', function (e) {
    var $self = $(this);
    var org_id = $self.data('id');
    var org_name = $self.data('name');


    $('#orgModal').find('.modal-title').html(org_name + ' Information');
    $('#orgModal').find('p.lead').show('fast', function () {
      $(this).siblings().hide()
    });

    $('#orgModal').modal('show');
    //$('.org_info').html('');

    $.post(
      base_url + 'webmanager/organization/get_org_info', {
        org_id: org_id
      },
      function (res) {
        //var stripes = $.param(res.org_stripe[0].customer_data);
        //console.log(res.org_stripe[0].created);


        //$('.org_info').append('');

        if (res.org_data.length > 0) {
          var texto = '';

          //texto += '<div class="well well-sm"><p><strong></strong></p>';
          var the_org = res.org_data[0];
          texto += '<p>CC Assigned: ' + the_org.cc_name + '</p>';
          texto += '<p>Date Organization was provisioned: ' + the_org.date_added + '</p>';

          if (the_org.modules.length > 0) {
            texto += '<p>Active Modules</p>';
            texto += '<ol>';
            $(the_org.modules).each(function (index, element) {
              texto += '<li>' + the_org.modules[index] + '</li>';
            });
            texto += '</ol>';
          } else {
            texto += '<p>Active Modules<br><span class="text-muted">No Modules</span></p>';
          }

          if (the_org.stakeholders.length > 0) {
            texto += '<p>Stakeholders</p>';
            texto += '<ol>';
            $(the_org.stakeholders).each(function (index, element) {
              texto += '<li>' + the_org.stakeholders[index] + '</li>';
            });
            texto += '</ol>';
          } else {
            texto += '<p>Stakeholders<br><span class="text-muted">No Stakeholders</span></p>';
          }


          if (the_org.module_crts.length == 0) {
            texto += '<p>CRTs<br><span class="text-muted">No CRTs</span></p>';
          }

          $('#collapseOrgDetails').find('.panel-body').html(texto);



        }

        //get crts
        if (res.org_data[0].module_crts.length > 0) {

          var texto = '';

          var crts_in_module = res.org_data[0].module_crts;
          $(crts_in_module).each(function (index, element) {
            var module_name = crts_in_module[index].name;
            var module_crts = crts_in_module[index].crts;
            texto += '<p><strong>' + module_name + '</strong></p>';

            if (module_crts.length > 0) {
              texto += '<ol>';
              $(module_crts).each(function (index, element) {
                texto += '<li>' + module_crts[index].full_name + '</li>';
              });
              texto += '</ol>';
            } else {
              texto += '<p class="text-muted">No CRT found.</p>';
            }
          });

          $('#collapseCrts').find('.panel-body').html(texto);

        }





        //get user logs
        var texto = '';


        var the_logs = res.the_logs;
        if (the_logs.length > 0) {
          //$("#log-table").dataTable().fnClearTable();

          $(the_logs).each(function (index, element) {
            var user_name = the_logs[index].user_name;
            var action = the_logs[index].action;
            var module_name = the_logs[index].module_name;
            var date_format = the_logs[index].date_format;

            //texto.push(user_name+' '+action+' '+module_name+' on '+date_format);

            texto += '<tr><td>' + user_name + ' ' + action + ' ' + module_name + ' on ' + date_format + '</td></tr>';

          });
          //console.log(texto);

          $('#log-table').find('tbody').html(texto);

          $('#collapseLogs').find('p').hide().siblings().show();
        } else {
          $('#collapseLogs').find('p').show().siblings().hide();
        }





        //if not from stripe
        if (res.org_stripe.length == 0) {
          var texto = '<p class="text-muted">Not from stripe.</p>';

          $('#collapseStripe').find('.panel-body').html(texto);

        } else {


          var text = '';


          if (res.from_stripe.the_subsription_data.length > 0) {

            $(res.from_stripe.the_subsription_data).each(function (index, element) {

              text += '<p>Plan Subscribed: ' + res.from_stripe.the_subsription_data[index].plan.name + '</p>';

              text += '<p>Current Subscription Period Started: ' + res.from_stripe.the_subsription_data[index].current_period_start + '</p>';

              text += '<p>Current Subscription Period End: ' + res.from_stripe.the_subsription_data[index].current_period_end + '</p>';

              var price = res.from_stripe.the_subsription_data[index].plan.amount;
              price /= Math.pow(10, 2);
              text += '<p>Price: $' + price + ' (Paid)</p>';


            });


          } else {
            text += '<p class="text-muted">No plan subscription</p>';
          }

          $('#collapseStripe').find('.panel-body').html(text);


        }



        console.log(res);
        $('#orgModal').find('p.lead').fadeOut('fast', function () {
          $(this).siblings().show()
        });

      },
      'json'
    ).error(function (err) {
      console.log(err);
    });

    return false;
  });

  //check if stripe user subscribed

  $('.subscribed_td').each(function (index, element) {
    var subscribed_td = $(this).find('a').html();
    console.log(subscribed_td);
    if (subscribed_td == 'Add a Plan') {
      $(this).closest('tr').find('a.btn-default').addClass('disabled');
    }
  });


  //admin access

  $('.add_admin_btn').on('click', function () {
    var $self = $('#admin_access_form');
    $self.find('input[type="text"], input[type="hidden"], input[type="email"]').val('');
    $self.find('[name="privileges[]"]').prop('checked', false);
    $('#adminAccessModal').modal('show');
    return false;
  });


  $('.update_admin_btn').on('click', function () {
    var id = $(this).data('id');
    var email = $(this).data('email');
    var username = $(this).data('username');
    var privileges = $(this).data('privileges');
    var $self = $('#admin_access_form');

    console.log(privileges);
    $self.find('[name="username"]').val(username);
    $self.find('[name="userid"]').val(id);
    $self.find('[name="email"]').val(email);

    $self.find('[name="privileges[]"]').each(function (index, element) {
      //console.log($(element).val());
      if ($.inArray($(this).val(), privileges) < 0) {
        $(this).prop('checked', false);
      } else {
        $(this).prop('checked', true);
      }
    });
    //$self.find('[name="privileges[]"]').prop('checked', true);
    $('#adminAccessModal').modal('show');
  });

  $('.delete_admin_btn').on('click', function () {
    var id = $(this).data('id');
    var $self = $(this);
    bootbox.confirm('<p class="lead">Please confirm you wish to delete selected admin.</p>', function (e) {
      if (e) {
        console.log(id);
        $.post(
          base_url + 'webmanager/adminaccess/delete_admin', {
            id: id
          },
          function (res) {
            console.log(res);
            if (res == 'yeah') {
              $self.closest('tr').fadeOut('slow').addClass('bg-danger').remove();
            }
          }
        ).error(function (err) {
          console.log(err);
        });
      }
    });
  });

  $('#admin_access_form').on('submit', function () {

    var $self = $(this);
    var username = $self.find('[name="username"]');
    var userid = $self.find('[name="userid"]');
    var email = $self.find('[name="email"]');
    var privileges = $self.find('[name="privileges[]"]:checked');
    var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;

    $self.find('input').closest('div').removeClass('has-error');
    $self.find('span').html('');
    if (username.val() == '') {
      username.focus().closest('.form-group').addClass('has-error');
      username.siblings('span').html('Required field.');
    } else if (email.val() == '') {
      email.focus().closest('.form-group').addClass('has-error');
      email.siblings('span').html('Required field.');
    } else if (!filter.test(email.val())) {
      email.focus().closest('.form-group').addClass('has-error');
      email.siblings('span').html('Valid email is required.');
    } else if (privileges.length == 0) {
      $self.find('[name="privileges[]"]').closest('div').siblings('span').html('Check atleast one.');
    } else {
      $self.find('.btn-primary').addClass('disabled').html('Saving changes..');

      console.log($self.serialize());
      $.post(
        base_url + 'webmanager/adminaccess/addupdate',
        $self.serialize(),
        function (res) {
          console.log(res);
          if (res == 'duplicate') {
            username.focus().closest('.form-group').addClass('has-error');
            username.siblings('span').html('Username already exist.');
            $self.find('.btn-primary').removeClass('disabled').html('Save changes');
          } else {
            window.location.reload(true);

          }
        }
      ).error(function (err) {
        console.log(err);
      });
    }

    return false;
  });

  $(document).on('click', '.extend_trial', function () {
    var id = $(this).data('id');
    var trial = $(this).data('trial');

    bootbox.confirm('Confirm 1 month trial extension to user?', function (r) {
      if (r) {
        console.log(r);
        $.post(
          base_url + 'webmanager/trialcustomers/extend_trial', {
            id: id,
            trial: trial
          },
          function (res) {
            console.log(res);
            window.location.reload(true);
          },
          'json'
        ).error(function (err) {
          console.log(err);
        });

      }
    });

  });

  $(document).on('click', '.update_valmess_btn', function () {
    var id = $(this).data('id');
    var err = $(this).data('err');
    var conf = $(this).data('conf');
    var succ = $(this).data('succ');
    var desc = $(this).data('desc');
    var $self = $('#valmess_form');
    $self.find('input').val('');

    $self.find('[name="success"]').val(succ);
    $self.find('[name="confirm"]').val(conf);
    $self.find('[name="description"]').val(desc);
    $self.find('[name="id"]').val(id);
    $self.find('[name="error"]').val(err);


    $('#valMessModal').modal('show');
    return false;
  });



  $('#valmess_form').on('submit', function () {
    var $self = $(this);
    $self.find('.btn-primary').addClass('disabled').html('Saving changes..');

    console.log($self.serialize());
    $.post(
      base_url + 'webmanager/message_validation/addupdate',
      $self.serialize(),
      function (res) {
        console.log(res);
        window.location.reload(true);
      },
      'json'
    ).error(function (err) {
      console.log(err);
    });

    return false;
  });

  $('.add_waiting_credit_btn').on('click', function () {
    var $self = $(this);
    var awaiting = $(this).attr('data-awaiting');
    var credit = $(this).attr('data-credit');
    var orgid = $(this).attr('data-orgid');
    bootbox.confirm('<p class="lead">Confirm allocation.</p>', function (e) {
      if (e) {
        $.post(
          base_url + 'webmanager/smscenter/transfer_waiting_sms', {
            awaiting: awaiting,
            credit: credit,
            orgid: orgid
          },
          function (res) {
            console.log(res);
            $self.closest('tr').find('td.sms_credit_col').html('$' + res);
            $self.closest('td').html('$0');
          }
        );
      }
    });
    return false;
  });



  $('#help_form').find('[name="id"]').on('change', function () {
    var $self = $(this);
    var id = $self.val();
    $('.note-editable').css('background', '#eee');
    $.post(
      base_url + 'webmanager/help/get_help_content', {
        id: id
      },
      function (res) {
        //console.log(res);
        $('.note-editable').html(res);
        $('.note-editable').css('background', '#fff');
      }

    );
  });


  $('#help_form').on('submit', function () {

    if ($(this).find('[name="id"]').val() == '') {
      bootbox.alert('Please select page to update.');
      return false;
    }

    var shtml = $('#summernote').code();
    $('#help_form').find('button.btn-primary').html('Saving').addClass('disabled');

    var data = {
      shtml: shtml
    };
    $.post(
      base_url + 'webmanager/help/save_help',

      $('#help_form').serialize() + '&' + $.param(data),

      function (res) {
        console.log(res);
        $('#help_form').find('button.btn-primary').html('Saved!');
        setTimeout(function () {
          $('#help_form').find('button.btn-primary').html('Save').removeClass('disabled');

        }, 1500);
      }
    );
    return false;
  });

  $('.preview_help_btn').on('click', function () {
    var shtml = $('#summernote').code();
    $('#helpPreviewModal').find('.well').html(shtml);
    $('#helpPreviewModal').modal('show');
    return false;
  });
}); //document.ready
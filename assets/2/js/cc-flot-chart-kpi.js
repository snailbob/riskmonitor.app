$(function() {
    $('#easy-pie-1, #easy-pie-2, #easy-pie-3, #easy-pie-4').easyPieChart({
        barColor: "rgba(255,255,255,.5)",
        trackColor: "rgba(255,255,255,.5)",
        scaleColor: "rgba(255,255,255,.5)",
        lineWidth: 20,
        animate: 1500,
        size: 130,
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

});

$(function () {
	var contact_cntr = $('#contact_cntr').val();
	var kpi_email = $('#kpi_email').val();
	var kpi_website = $('#kpi_website').val();
	var kpi_trading = $('#kpi_trading').val();
	
	if(contact_cntr != 'undefined' && contact_cntr != ''){
		
		var data = [
			{ label: " Contact Centre", data: contact_cntr },
			{ label: " Email", data: kpi_email },
			{ label: " Website", data: kpi_website },
			{ label: " Trading Partner", data: kpi_trading }
		];
		
		var plotObj = $.plot($("#flot-chart-pie"), data, {
			series: {
				pie: {
					show: true
				}
			},
			grid: {
				hoverable: true 
			},
			tooltip: true,
			tooltipOpts: {
				content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
				shifts: {
					x: 20,
					y: 0
				},
				defaultTheme: false
			}
		});
		
	}
	
});	


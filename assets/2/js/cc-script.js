//update clock on browser not chrome

var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
console.log(is_chrome);

if(!is_chrome && $( document ).width() > 1182){ //do not display flipcock
	//incident clock
	$('.inci_clock_lg_btn').removeClass('visible-sm visible-md').addClass('hidden-xs');
	$('.inci_flip_clock_label').hide();
	$('.my_recall_timer').hide();

	//user clocks
	$('.user-clock').closest('span.hidden-lg').removeClass('hidden-lg');
	$('.my_clock').hide();
}



//display current date in the footer
var thecurrentyear = new Date().getFullYear();
$('.curr_year').html(thecurrentyear);


(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);


$(document).ready(function () {
	$('.div-controller').removeClass('hidden');

	//demo upload
	var $uploadCrop;

	$('.check_link_valid_user').on('click', function(){
		if(isUserPlanFree()){
			bootbox.alert('To access this feature, please upgrade your plan.');
			return false;
		}
	});

	if(!isPlanUpgradable()){
		$('.upgrade-btn-holder').hide();
	}

	var users_percentage = $('.table-progress-div').data('users-percentage');
	users_percentage = parseInt(users_percentage);
	console.log(users_percentage);
	if(uri_3 == 'managecrt'){
		if(users_percentage > 100){
			bootbox.alert('Number of users exceeded for selected plan.');
		}
	}

	$('.add_incident_form_btn').on('click', function() {
		$('#addIncidentFormModal').modal('show');
	});

	$('.add_task_template_btn').on('click', function() {
		$(this).closest('.modal').modal('hide');
		$('#addTaskTemplateModal').modal('show');
	});

	$('.save_name_btn_common').on('click', function(){
		var name = $(this).closest('.modal').find('[name="name"]').val();
		var endpoint = $(this).data('endpoint');
		if(!name){
			bootbox.alert('Name cannot be empty.');
			return false;
		}

		$.post(
			base_url+'cc/standbytasks/' + endpoint,
			{name: name},
			function(res){
				if(endpoint === 'save_template'){
					window.location.href = window.base_url + 'cc/standbytasks/index/0/' + res.id + '?template_name=' + res.name;
				}
				else{
					window.location.href = window.base_url + 'cc/standbytasks/incident_form/0/' + res.id + '?template_name=' + res.name;
				}
			},
			'json'
		);
	});

	$(document).on('click', '.add_incident_btn', function(){
		let crisisflo_incident_length = localStorage.getItem('crisisflo_incident_length');
		if(parseInt(crisisflo_incident_length) >= 1 && isUserPlanFree()){
			bootbox.alert('To access this feature, please upgrade your plan.');
		}
		else{
			$('#newIncidentModal').modal('show');
		}
	});

	$('.add_incident_form_btn').on('click', function(){
		$('#newIncidentFormModal').modal('show');

		// let crisisflo_incident_length = localStorage.getItem('crisisflo_incident_length');
		// if(parseInt(crisisflo_incident_length) >= 1){
		// 	bootbox.alert('To access this feature, please upgrade your plan.');
		// }
		// else{
		// 	$('#newIncidentModal').modal('show');
		// }
	});

	function readFile(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('.upload-demo').addClass('ready');
				$uploadCrop.croppie('bind', {
					url: e.target.result
				}).then(function(){
					console.log('jQuery bind complete');
				});
				
			}
			
			reader.readAsDataURL(input.files[0]);
		}
		else {
			swal("Sorry - you're browser doesn't support the FileReader API");
		}
	}

	function hideUpload(){
		$('.upload-demo-wrap').hide();
		$('.upload-result-holder').hide();
		$('.img-display-holder').show();
		$('#myCropModal').modal('hide');
	}
	$uploadCrop = $('#upload-demo').croppie({
		viewport: {
			width: 100,
			height: 100,
			type: 'circle'
		},
		enableExif: true
	});

	$('.upload-cancel').on('click', function(){
		hideUpload();
	});

	$('#upload').on('change', function () { 
		var c = this;
		$('#myCropModal').modal('show');
		$('#myCropModal').on('shown.bs.modal', function() {
			readFile(c);
			$('.upload-demo-wrap').show();
			$('.upload-result-holder').show();
			// $('.img-display-holder').hide();
		})


	});
	$('.upload-result').on('click', function (ev) {
		var $self = $(this);
		$uploadCrop.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (resp) {
			console.log(resp);
			$self.button('loading');
			$.post(
				base_url+'cc/settings/save_avatar',
				{avatar: resp},
				function(res){
					$self.button('reset');
					$('.img-display-holder, .img-displayer').find('img').attr('src', resp);
					hideUpload();
					console.log(res);
				},
				'json'
			).error(function(err){
				console.log(err);
			});
			


			// popupResult({
			// 	src: resp
			// });
		});
	});




	// if(endtrial == 'expired'){
	// 	$('.topLabel').find('.btn').addClass('disabled').css('opacity', '.7');
	// 	$('#page-wrap, .chat-collapse').addClass('hidden');
	// }

	//review date time picker
	$('.review_date').datetimepicker({
		format: 'YYYY-MM-DD',
		minDate: moment()
	});

	$('[name="reminder"]').on('change', function(){
		var month = $(this).val();
		var theDate = moment().add(month, 'months');
		console.log(theDate);
		$('.review_date').data("DateTimePicker").date(theDate);
		console.log($(this).val());
	});

	//change nav-bar class
	if($( document ).width() < 751){
		$('.navbar-default').removeClass('navbar-fixed-top').addClass('navbar-static-top');
		$('.main_container').addClass('main_container_xs').removeClass('main_container');
	}else{
		$('.navbar-default').addClass('navbar-fixed-top').removeClass('navbar-static-top');
		$('.main_content_xs').addClass('main_content').removeClass('main_content_xs');
	}

	//datepicker
	$('#inci_date').datetimepicker({
		/*pickTime: false,*/
		maxDate: moment()

	});

	//prevent typing in date field
	$('#inci_date').keydown(function(e) {
	   e.preventDefault();
	   return false;
	});


	"use strict";
	var options = {};
	options.ui = {
		container: "#pwd-container",
		viewports: {
			progress: ".pwstrength_viewport_progress",
			verdict: ".pwstrength_viewport_verdict"
		}
	};
	options.common = {
		onLoad: function () {
			$('#messages').text('Start typing password');
		},
		zxcvbn: true
	};
	$(':password').pwstrength(options);



});



$(document).ready(function() {

	$('select[name="group"]').on('change', function(){
		var $self = $(this);
		var access = $self.find('option:selected').data('access');
		console.log(access);
		$('[name="access[]"]').prop('checked', false);
		if(access.length > 0){
			for (i = 0; i < access.length; i++) {
				$('input[value="'+access[i]+'"]').prop('checked', true);
			}
		}
	});

	if (uri_2 == 'kpi' && uri_3 == ''){
		$('#myRecalls').modal('show');
	}//uri_2 == 'kpi' && uri_3 == ''



	if (uri_2 == 'document' && uri_3 == 'manage' && uri_4 != ''){


		var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close hidden" id="hide_x" data-dismiss="modal" aria-hidden="true">&times;</button><p class="lead" id="hide_spin">';

		var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Your file is being analysed.</p><p id="scan_result">';

		var myModal3 = '</p></div></div></div> ';

		var myModal = myModal1 + myModal2 + myModal3;

		$(myModal).modal('show');

		var file_id = uri_4;
		$.ajax({
			  type: "POST",

			  url: base_url+'cc/document/scan_response',

			  data: { file_id: file_id },

			  success: function(data) {
				  console.log(uri_5);
				  $('#scan_result').html(data);
				  $('#hide_spin').addClass('hidden');
				  $('#hide_x').removeClass('hidden');

			  }

		});

	}//uri_2 == 'document' && uri_3 == 'manage' && uri_4 != ''

	$('.common_msg_click').on('click', function ( e ) {
		$.fn.custombox( this );
		e.preventDefault();
	});

	$(document).on('click', '.endtourforever', function(){
		var value = ($(this).is(':checked')) ? 'Y' : 'N';
		if(value == 'Y'){
			$(document).find('.endtourforever').prop('checked', true);
		}
		else{
			$(document).find('.endtourforever').prop('checked', false);
		}
		$.post(
			base_url+'cc/dashboard/endtourforever',
			{value: value},
			function(res){
				console.log(res);
			},
			'json'
		).error(function(err){
			console.log(err);
		});
		console.log('ham');
	});


	if ($(window).width() > 768 && endtourforever == 'N'){

		// Instance the tour
		var tour = new Tour({
		  container: "body",
		  steps: [
		  {
			element: "#site_logo",
			title: "Hello! Welcome to CrisisFlo",
			content: 'This is a short and simple guide for our easy-to-use interface. Let\'s get started and press the next button!<br><label><input type="checkbox" class="endtourforever"/> Do not display again</label>',
			placement: "bottom",
			backdrop: false
		  },
		  {
			element: "#lg_sidemenu_btn",
			title: "Menu Tab",
			content: 'This is the main menu tab.',
			placement: "right",
			backdrop: false
		  },
		  {
			element: "#lg_home_nav_btn",
			title: "Home button",
			content: 'Click here to take you back to home.',
			placement: "right",
			backdrop: false
		  },
		  {
			element: "#lg_message_nav_btn",
			title: "Message button",
			content: 'Click here to read and send private messages.',
			placement: "bottom",
			backdrop: false
		  },
		  {
			element: "#lg_forum_nav_btn",
			title: "Forum button",
			content: 'Click here to read and post forum messages.',
			placement: "bottom",
			backdrop: false
		  },
		  {
			element: "#lg_admin_help_btn",
			title: "Help (?) button",
			content: 'Click here for the page help guide.<br><label><input type="checkbox" class="endtourforever"/> Do not display again</label>',
			placement: "left",
			backdrop: false
		  }

		]});

		// Initialize the tour
		// tour.init();

		// Start the tour
		// tour.start();
	}//$(window).width() > 768



	$('.refresh_btn').click(function(){
		$('.glyphicon-refresh').addClass('fa-spin');

	});


	$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
		e.preventDefault();
		$(this).siblings('a.active').removeClass("active");
		$(this).addClass("active");
		var index = $(this).index();
		$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
		$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
	});


	$('a[href^="#assigned_task"]').on('click',function (e) {
		e.preventDefault();

		var target = this.hash,
		$target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
		}, 900, 'swing', function () {
			//window.location.hash = target;
		});
	});


	$('a[href^="#response_init"]').on('click',function (e) {
		e.preventDefault();

		var target = this.hash,
		$target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
		}, 900, 'swing', function () {
			//window.location.hash = target;
		});
	});


	var options = {
		beforeSend: function()
		{
			$("#progress").show();
			//clear everything
			$("#bar").width('0%');
			$("#message").html("");
			$("#percent").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete)
		{
			$("#bar").width(percentComplete+'%');
			$("#percent").html(percentComplete+'%');


		},
		success: function()
		{
			$("#bar").width('100%');
			$("#percent").html('100%');

		},
		complete: function(response)
		{
			$("#file_up_text").show();
			$("#message").after(response.responseText);

			console.log(response.responseText);
		},
		error: function()
		{
			$("#file_up_text").show();
			$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

		}

	};

	$("#myForm").ajaxForm(options);



	var options = {
		beforeSend: function()
		{
			$("#progress2").show();
			//clear everything
			$("#bar2").width('0%');
			$("#message2").html("");
			$("#percent2").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete)
		{
			$("#bar2").width(percentComplete+'%');
			$("#percent2").html(percentComplete+'%');


		},
		success: function()
		{
			$("#bar2").width('100%');
			$("#percent2").html('100%');

		},
		complete: function(response)
		{
			$("#file_up_text2").show();
			$("#message2").after(response.responseText);

			console.log(response.responseText);
		},
		error: function()
		{
			$("#file_up_text2").show();
			$("#message2").html("<font color='red'> ERROR: unable to upload files</font>");

		}

	};

	$("#myForm2").ajaxForm(options);




	$( '.navigation li' ).find('.fa-angle-double-right').hide();
	//$( '.navigation li.active' ).find('.fa-angle-double-right').show();

	$( '.navigation li' ).click(function () {
		$( this ).find('.fa-angle-double-right').slideDown( "slow" );
		$( this ).siblings().find('.fa-angle-double-right').hide( "slow" );

	});


	//click event for main nav
	$('#collapse_nav').click(function(e) {
		hideNav();
		$(this).hide();
		$('#expand_nav').show();
	});

	$('#expand_nav').click(function(e) {
		showNav();
		$(this).hide();
		$('#collapse_nav').show();
	});


	//click event for chat list
	$('#page-wrap button').click(function(e) {
		hideChatList();
	});

	$('#show_chat').click(function(e) {
		showChatList();
		$(this).hide();
	});

	//click event for chat pm
	$('#page-wrap-pm button').click(function(e) {
		hideChatPm();
	});

	$('#show_chat_pm').click(function(e) {
		showChatPm();
		$('#this').hide();
	});


	//activate toggle nav
	checkCookie();

	//activate toggle chat list
	checkCookieChatList();

	//activate toggle chat pm
	if(getCookie("remove_im") !=""){
		checkCookieChatPm();
	}



	//filter recall table
	var activeSystemClass = $('.list-group-item.active');

	//something is entered in search form
	$('#system-search').keyup( function() {
	   var that = this;
		// affect all table rows on in systems table
		var tableBody = $('.table-list-search tbody');
		var tableRowsClass = $('.table-list-search tbody tr');
		$('.search-sf').remove();
		tableRowsClass.each( function(i, val) {

			//Lower text for case insensitive
			var rowText = $(val).text().toLowerCase();
			var inputText = $(that).val().toLowerCase();
			if(inputText != '')
			{
				$('.search-query-sf').remove();
				tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
					+ $(that).val()
					+ '"</strong></td></tr>');
			}
			else
			{
				$('.search-query-sf').remove();
			}

			if( rowText.indexOf( inputText ) == -1 )
			{
				//hide rows
				tableRowsClass.eq(i).hide();

			}
			else
			{
				$('.search-sf').remove();
				tableRowsClass.eq(i).show();
			}
		});
		//all tr elements are hidden
		if(tableRowsClass.children(':visible').length == 0)
		{
			tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
		}
	});


});//end of document.ready



if (uri_2 == 'dashboard' || uri_2 == 'log' || uri_2 == 'standbytasks'){

	$(document).ready(function() {
		//activate last visited tab
		$('.tasktab_loader').hide();


		//deactivate for payment tabs
		if(!$('#buyNowModal').hasClass('in') && location.hash != '#details' && location.hash != '#payment' && location.hash != '#confirm'){
			if(location.hash) {
			   $('a[href=' + location.hash + ']').tab('show');
			   help_page();

			}
			else{
				$('.steptab_active').addClass('active');

				if($('.kpi_menu_tab').length > 0){
					$('.navigation').children().first().addClass('active');
				}

			    help_page();
			}


			$(document.body).on("click", "a[data-toggle]", function(event) {
			  if (this.getAttribute("href") !='#'){

					location.hash = this.getAttribute("href");

					help_page();
			  }
			});
		}

	});//end of document.ready


	//activate last visited tab
	if(!$('#buyNowModal').hasClass('in')){
		$(window).on('popstate', function() {
			var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
			$('a[href=' + anchor + ']').tab('show');
		});
	}

}//if where recall tasks tab



var myWindow;

var isUserPlanFree = function(){
	let billing = localStorage.getItem('crisisflo_billing');
	billing = JSON.parse(billing);

	if(!billing || !billing.selected_plan || billing.selected_plan == '0'){
		return true;
	}
	return false;
}

var isPlanUpgradable = function(){
	let billing = localStorage.getItem('crisisflo_billing');
	billing = JSON.parse(billing);

	if(!billing || !billing.selected_plan || billing.selected_plan == '0' || billing.selected_plan == '1'){
		return true;
	}
	return false;
}

function openWin() {
	if(isUserPlanFree){
		bootbox.alert('To access this feature, you need to update your plan.');
		return false;
	}
	var h = screen.height;
	var w = screen.width;
	myWindow = window.open(base_url+'conference/team/'+cc_id+'/'+org_id,'mywindowtitle','width='+w+',height='+h);

	$('.btn_startvid').addClass('hidden');
	$('.btn_stopvid').removeClass('hidden');
}

function closeWin() {
	if (myWindow) {
		myWindow.close();

		var close_date = new Date().toISOString().slice(0, 19).replace('T', ' ');

		//store end of session of cc
		$.ajax({
		  type: "POST",

		  url: base_url+'conference/stop_meeting',

		  data: {
			  cc_id: cc_id,
			  org_id: org_id,
			  end_date: close_date
		  }

		});

		$('.btn_stopvid').addClass('hidden');
		$('.btn_startvid').removeClass('hidden');

	}
}


function resendCC(login_id){

	bootbox.confirm("<p class='lead'>Please confirm you wish to re-send email request?</p>", function(r) {

		if(r) {

			$.ajax({
			  type: "POST",

			  url:  base_url+'cc/settings/resend_request',

			  data: { login_id: login_id},

			  success: function(data) {

					console.log(data);
					bootbox.alert('<p class="lead">Email request successfully re-sent.</p>');
					//window.location.reload(true);

			  }

			});
		}
	});

}



//message file
function delete_the_file(file_id){
	thenum = file_id.replace( /^\D+/g, '');
	//alert(thenum);

	//delete the file in server and in table
	$.ajax({
	  type: "POST",

	  url: base_url+'cc/message/delete_message_file',

	  data: { thenum: thenum},

	  success: function(data) {

			console.log(data);

	  }

	});

}//.message file

//cost item file
function delete_the_cost_file(file_id){
	thenum = file_id.replace( /^\D+/g, '');
	//alert(thenum);

	//delete the file in server and in table
	$.ajax({
	  type: "POST",

	  url: base_url+'cc/costmonitor/delete_message_file',

	  data: { thenum: thenum},

	  success: function(data) {

			console.log(data);

	  }

	});

}//.cost item



function delete_the_recall_file(file_id){
	thenum = file_id.replace( /^\D+/g, '');
	//alert(thenum);

	//delete the file in server and in table
	$.ajax({
	  type: "POST",

	  url: base_url+'cc/recall/delete_message_file',

	  data: { thenum: thenum},

	  success: function(data) {

			console.log(data);

	  }

	});

}//.delete_the_recall_file


function delete_cost_cat(cat_id){
	bootbox.confirm("<p class='lead'>"+action_messages.recall.delete_cost_category+"</p>", function(e){

		if(e){

			$.ajax({
				  type: "POST",

				  url: base_url + 'cc/costmonitor/delete_cost_cat',

				  data: { cat_id: cat_id},

				  success: function(data) {

						console.log(data);
						window.location.reload(true);

				  }

			});

		}
	})
}//.delete_cost_cat

function delete_cost_item(item_id){
	bootbox.confirm("<p class='lead'>"+action_messages.recall.delete_cost_item+"</p>", function(e){

		if(e){

			$.ajax({
				  type: "POST",

				  url: base_url + 'cc/costmonitor/delete_cost_item',

				  data: { item_id: item_id},

				  success: function(data) {

						console.log(data);
						window.location.reload(true);

				  }

			});

		}
	})
}//.delete_cost_cat


function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
	}
	return "";
}


function checkCookie() {
	var user = getCookie("nav");
	if (user != "") {
		$('#expand_nav').click();
	} else {
		$('#collapse_nav').click();
	}

}

function eraseCookie(name) {
    setCookie(name,"",-1);
}

function hideNav(){

	//$('#viewdetails').children().toggle();
	if($('.main_nav a i span.mngecasebdg').length > 0){
		$('.main_nav a i span.mngecasebdg').show();
		$('.main_nav a i.mngecase').removeClass('fa-life-ring');
	}
	if($('.main_nav a i span.frmnewsbdg').length > 0){
		$('.main_nav a i span.frmnewsbdg').show();
		$('.main_nav a i.frrmnews').removeClass('fa-flag');
	}
	if($('.main_nav a i span.msgicobgd').length > 0){
		$('.main_nav a i span.msgicobgd').show();
		$('.main_nav a i.msgico').removeClass('fa-envelope');
	}


	$('.sidebar_nav').css('width','4%');
	$('.main_content').css('width','96%');

	$('.large_nav').hide();
	$('.slim_nav').show();

	if ($('.sidebar_nav').css('display') == 'none') {
		$('.main_content').css('width','100%');
	}

	setCookie("nav", "", "-1");
};


function showNav(){

	$('.sidebar_nav').css('width','');
	$('.main_content').css('width','');
	$('.slim_nav').hide();
	$('.large_nav').show();


	/*$('.main_nav').siblings().show();
	$('.main_nav a').css('text-align','left');
	$('.main_nav a i').siblings().show();*/


	/*if ($('.sidebar_nav').css('display') == 'none') {
		$('.main_content').css('width','100%');
	}*/

	$('.main_nav a i span.mngecasebdg').hide();
	$('.main_nav a i.mngecase').addClass('fa-life-ring');
	$('.main_nav a i span.frmnewsbdg').hide();
	$('.main_nav a i.frrmnews').addClass('fa-flag');
	$('.main_nav a i span.msgicobgd').hide();
	$('.main_nav a i.msgico').addClass('fa-envelope');

	if(getCookie("nav") != ""){
	}
	else{
		setCookie("nav", "collapse", "1");
	}
};


function checkCookieChatList() {
	var hidechat = getCookie("chat_list");
	if (hidechat != "") {
		$('#show_chat').click();
	} else {
		$('#page-wrap button').click();
	}

}

function showChatList(){

	if(getCookie("chat_list") !=""){
	}
	else{
		setCookie("chat_list", "block", "1");
	}

};

function hideChatList(){

	setCookie("chat_list", "", "-1");

};


//chat pm
function checkCookieChatPm() {
	var hidechat = getCookie("chat_pm");

	var initt = getCookie("initiator");
	var recee = getCookie("receiver");
	var dispp = getCookie("disp_name");

	createChatroom(dispp,initt,recee);


	if (hidechat != "") {
		$('#show_chat_pm').click();
	} else {
		$('#page-wrap-pm button').click();
	}


}

function showChatPm(){

	if(getCookie("chat_pm") !=""){
	}
	else{
		setCookie("chat_pm", "block", "1");
	}

};

function hideChatPm(){

	setCookie("chat_pm", "", "-1");

};

function removeChatPm(){

	setCookie("remove_im", "", "-1");

};




function recallAssign(recall_id,question_id,task_answer,task_assigned){
	$('#the_recall_id').val(recall_id);
	$('#the_question_id').val(question_id);
	$('#the_current_assigned').val(task_assigned);
	//$('#txt_message').val(task_answer);
}

function getRespond(task_id){
	$('#recallRespond').find('.modal-body').hide();
	$('#recallRespond').find('.modal-content').prepend('<div class="spin-loader text-center text-muted" style="padding: 50px;"><i class="fa fa-spinner fa-spin fa-2x"></i><br/>Loading content..</div>');

	$.ajax({
		  type: "POST",

		  url: base_url + 'cc/recall/get_respond_to_edit',

		  data: {
			  task_id: task_id
		  },

		  success: function(data) {
			// console.log(data);
			$('#recallRespond').find('.modal-body').show();
			$('#txt_message').val(data);
			$('#recallRespond').find('div.spin-loader').remove();

		  }
	});

}

function completeTask(recall_id,question_id,uri_sgmt_4,uri_sgmt_5){

	bootbox.confirm("<p class='lead'>"+action_messages.recall.mark_task_completed+"</p>", function(r){

		if(r){

			$.ajax({
			  type: "POST",

			  url: base_url+'cc/recall/mark_task_completed',

			  data: { question_id: question_id},

			  success: function(data) {

					console.log(data);

					if (uri_sgmt_4 == 'r' && (uri_sgmt_5)){

						window.location.reload(true);

					}
					else{
						window.location.reload(true);

					}

			  }

			});
		}
	});


}

var currcount;
var currsteplist;
var currtotq;
var oldcount2;
var oldcount3;
var oldcount4;
var oldcount5;
var oldcount6;
var oldcount7;
var oldcount8;

function checkTheCBox(question_id,btn,btnsm,status,chckcount,steplist,totq,proc){
	console.log($('#'+btn).closest('li').hasClass('disabled'));

	if($('#'+btn).closest('li').hasClass('curr_user_is_approver') == true){
		bootbox.alert('<p class="lead">Approve this task to close-out.</p>');
		return false;
	}


	if($('#'+btn).closest('li').hasClass('disabled') == true && assigned_stat == 'not_assigned'){
		return false;
	}



	if(typeof currsteplist === "undefined"){
		currsteplist = steplist;
		currtotq = parseInt(totq);
	}
	else{
		if (currsteplist != steplist){

			//assign old count
			if (currsteplist == 'step_2btn'){
				oldcount2 = currcount;
			}

			if (currsteplist == 'step_3btn'){
				oldcount3 = currcount;
			}

			if (currsteplist == 'step_4btn'){
				oldcount4 = currcount;
			}

			if (currsteplist == 'step_5btn'){
				oldcount5 = currcount;
			}

			if (currsteplist == 'step_6btn'){
				oldcount6 = currcount;
			}

			if (currsteplist == 'step_7btn'){
				oldcount7 = currcount;
			}

			if (currsteplist == 'step_8btn'){
				oldcount8 = currcount;
			}//.assign old count


			//get old count
			if (steplist == 'step_2btn'){
				currcount = oldcount2;
			}

			else if (steplist == 'step_3btn'){
				currcount = oldcount3;
			}

			else if (steplist == 'step_4btn'){
				currcount = oldcount4;
			}

			else if (steplist == 'step_5btn'){
				currcount = oldcount5;
			}

			else if (steplist == 'step_6btn'){
				currcount = oldcount6;
			}

			else if (steplist == 'step_7btn'){
				currcount = oldcount7;
			}

			else if (steplist == 'step_8btn'){
				currcount = oldcount8;
			}
			else{
				currcount = parseInt(chckcount);
			}//.get old count

			currsteplist = steplist;
			currtotq = parseInt(totq);

		}
	}

	var chckcount = parseInt(chckcount);

	if(status == '0'){

		bootbox.confirm("<p class='lead'>"+action_messages.recall.mark_task_completed+"</p>", function(r){


			if(r){

				//$("#"+btn+", #"+btnsm).html('<i class="fa fa-spinner fa-spin text-muted" style="font-size: 165%;"></i>');
				$.ajax({
				  type: "POST",

				  url: base_url+'cc/recall/mark_task_completed',

				  data: { question_id: question_id},

				  dataType: 'json',

				  error: function(err, mess){
						console.log(err);
				  },

				  success: function(data) {

						console.log(data);
						if(data.result == 'was_update'){
							$('#refreshPageModal').modal('show');

						}
						else{


							//disable
							$('#'+btn).closest('tr').find('.save_stp3_table').addClass('hidden');

							$("#"+btn+", #"+btnsm).fadeOut(500, function(){
//									$("#"+btn+", #"+btnsm).siblings('button').show();
//									$("#"+btn+", #"+btnsm).html('<i class="fa fa-check text-muted" style="font-size: 165%;"></i>');

								$("#"+btn+", #"+btnsm).closest('li').siblings().find('a:hidden').show();


								$('#'+btn).closest('tr').find('.task_text_status').removeClass('text-danger text-muted text-warning').addClass(data.status_class).html(data.status_text+'<span class="hidden">Completed_yes</span>');


								if(data.new_task_owner !=''){
									$('#'+btn).closest('tr').find('span.label').removeClass('danger').addClass('orange');
									$('#'+btn).closest('tr').find('span.label').html(data.new_task_owner);

								}

							});
							//setTimeout(function() {$("#"+btn).siblings('button').show()},500);


							$('#'+btn).closest('tr').find('.date_complete'+question_id).html(data.time_completed);

							$('#'+btn).closest('tr').find('a.btn-sm').fadeOut(500, function(){
								$('#'+btn).closest('tr').find('.date_complete'+question_id).show();
							});

							if (typeof currcount === "undefined" || currcount === null){
								currcount = chckcount - 1;
								console.log(currcount);

								if(currcount == 0){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-green bg-success');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
									$(proc).show();

									$('#'+btn).closest('.tab-pane').find('.simulation_btn').click();
								}
								if(currcount > 0 && currcount != currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-orange bg-warning');
									if($('#' + steplist + ' a i.fa-circle-o').length > 0){
										$('#' + steplist + ' a i.fa-circle-o').remove();
										$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
									}
									$(proc).hide();
								}
								if(currcount == currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-red bg-danger');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
									$(proc).hide();
								}

							}else{

								currcount = currcount - 1;
								console.log(currcount);


								if(currcount == 0){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-green bg-success');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
									$(proc).show();
									$('#'+btn).closest('.tab-pane').find('.simulation_btn').click();
									//console.log('adfsadf');
								}
								if(currcount > 0 && currcount != currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-orange bg-warning');

									if($('#' + steplist + ' a i.fa-circle-o').length > 0){
										$('#' + steplist + ' a i.fa-circle-o').remove();
										$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
									}
									$(proc).hide();
								}
								if(currcount == currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-red bg-danger');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
									$(proc).hide();
								}
							}

						}


				  }

				});	//end of ajax


			}

		});
	}
	else{
		bootbox.confirm("<p class='lead'>"+action_messages.recall.mark_task_incomplete+"</p>", function(r)
		{
			if(r)
			{

				//$("#"+btn+", #"+btnsm).html('<i class="fa fa-spinner fa-spin" style="font-size: 165%;"></i>');
				$.ajax({
				  type: "POST",

				  url: base_url+'cc/recall/mark_task_incomplete',

				  data: { question_id: question_id},

				  dataType: 'json',

				  success: function(data) {
						console.log(data);
						if(data.result == 'was_update'){
							$('#refreshPageModal').modal('show');
						}
						else{

//enable

							$('#'+btn).closest('tr').find('.save_stp3_table').removeClass('hidden');

							$('#'+btn).closest('tr').find('.date_complete'+question_id).hide();

							$("#"+btn+", #"+btnsm).fadeOut(500, function(){

//									$("#"+btn+", #"+btnsm).siblings('button').show();
//
//									$("#"+btn+", #"+btnsm).html('<i class="fa fa-check" style="font-size: 165%;"></i>');

								$("#"+btn+", #"+btnsm).closest('li').siblings().find('a:hidden').show();


								$('#'+btn).closest('tr').find('.task_text_status').removeClass('text-danger text-muted text-warning').addClass(data.status_class).html(data.status_text);



								if(data.new_task_owner !=''){
									$('#'+btn).closest('tr').find('span.label').removeClass('danger').addClass('orange');
									$('#'+btn).closest('tr').find('span.label').html(data.new_task_owner);

								}
								else{
									$('#'+btn).closest('tr').find('span.label').removeClass('orange').addClass('danger');
									$('#'+btn).closest('tr').find('span.label').html('Not Assigned');

								}

							});
							//setTimeout(function() {$("#"+btn).siblings('button').show()},500);

							$('#'+btn).closest('tr').find('a.btn-sm').fadeIn(500);
							if (typeof currcount === "undefined" || currcount === null){
								currcount = chckcount + 1;
								console.log(currcount);
								if(currcount == 0){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-green bg-success');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
									$(proc).show();
								}
								if(currcount > 0 && currcount != currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-orange bg-warning');

									if($('#' + steplist + ' a i.fa-circle-o').length > 0){
										$('#' + steplist + ' a i.fa-circle-o').remove();
										$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
									}

									if($('#' + steplist + ' a i.fa-check-circle').length > 0){
										$('#' + steplist + ' a i.fa-check-circle').remove();
										$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
									}
									$(proc).hide();
								}
								if(currcount == currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-red bg-danger');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
									$(proc).hide();
								}
							}else{
								currcount = currcount + 1;
								console.log(currcount);
								if(currcount == 0){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-green bg-success');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
									$(proc).show();
								}
								if(currcount > 0 && currcount != currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-orange bg-warning');

									if($('#' + steplist + ' a i.fa-circle-o').length > 0){
										$('#' + steplist + ' a i.fa-circle-o').remove();
										$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
									}

									if($('#' + steplist + ' a i.fa-check-circle').length > 0){
										$('#' + steplist + ' a i.fa-check-circle').remove();
										$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
									}
									$(proc).hide();
								}
								if(currcount == currtotq){
									$('#' + steplist + ' a').removeClass();
									$('#' + steplist + ' a').addClass('text-red bg-danger');
									$('#' + steplist + ' a i.fa-minus-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
									$(proc).hide();
								}
							}


						}

				  }

				});	//end of ajax



			}

		});

	}

}//.checkTheCBox


function checkCloseRC(id){
	$.ajax({
	  type: "POST",

	  url: base_url+'cc/recall/check_to_close',

	  data: {
		  id: id
	  },

	  success: function(data) {
			console.log(data);
			if(data == 'error'){
				$('#close_inci_form').find('.close_for_complete').hide();
				$('#close_inci_form').find('.close_for_incomplete').show();
				$('#close_inci_form').find('button.btn-primary').attr('disabled','disabled');
				$('#verifyCloseIncident').modal('show');
				$('#close_inci_form').show().siblings().hide();
				//bootbox.alert('<div class="lead"><i class="fa fa-info-circle text-info"></i> Please complete all tasks to close incident</div>');
			} else{
				closeRecall(id);
			}

	  }

	});
}



//close recall incident
function closeRecall(recall_id){
	$('#close_inci_form').find('.close_for_complete').show();
	$('#close_inci_form').find('.close_for_incomplete').hide();
	$('#close_inci_form').find('button.btn-primary').removeAttr('disabled');
	$('#verifyCloseIncident').modal('show');

}//close recall incident


$(document).ready(function(e) {
	$('.extract_field').on('keyup', function(){
		if($(this).val() == '' || $(this).val() != 'EXTRACT'){
			$(this).siblings('span').html('Please type \'EXTRACT\'.');
			$(this).closest('form').find('button.btn-primary').attr('disabled','disabled');
		}
		else{
			$(this).siblings('span').html('');
			$(this).closest('form').find('button.btn-primary').removeAttr('disabled');
		}
	});

	$('#extract_form').on('submit', function(){
		save_inci_csv('download');
		$('.modal').modal('hide');
		return false;

	});


	$('#close_inci_form').find('input').on('keyup', function(){

		if($(this).val() == '' || $(this).val() != 'CLOSEOUT'){
			$(this).siblings('span').html(' Please type \'CLOSEOUT\'.');
			$(this).closest('form').find('button.btn-primary').attr('disabled','disabled');
		}
		else{
			$(this).siblings('span').html('');

			var $self = $('#close_inci_form');
			var inci_id = $self.attr('data-id');
			//window.location.href= base_url + 'cc/recall/close_recall/' + inci_id;

			$self.find('button.btn-primary').removeAttr('disabled');
			//$self.find('a.btn-default').addClass('disabled');

			//$self.submit();

			/*$.post(
				base_url+'cc/recall/close_recall/'+inci_id,
				{},
				function(res){
					console.log(res);

					setTimeout(function(){
						$('#close_inci_form').fadeOut('fast', function(){
							$(this).siblings().show();
						});
					},1400);


				}
			).error(function(err){
				console.log(err);
			});*/

		}
	});

	$('.save_csv_btn').on('click', function(){
		save_inci_csv('download');
		$('.modal').modal('hide');
	});

	$('.cancel_csv_btn').on('click', function(){
		save_inci_csv('not_download');
		$('.modal').modal('hide');
	});

	$('#close_inci_form').on('submit', function(){
		var $self = $(this);
		var inci_id = $(this).attr('data-id');
		//window.location.href= base_url + 'cc/recall/close_recall/' + inci_id;
		$self.find('a.btn-default').addClass('disabled');

		$self.find('button.btn-primary').html('<i class="fa fa-spinner fa-spin"></i> Loading..').addClass('disabled');
		$self.find('a.btn-default').addClass('disabled');

		$.post(
			base_url+'cc/recall/close_recall/'+inci_id,
			{},
			function(res){
				console.log(res);

				setTimeout(function(){
					$('#close_inci_form').fadeOut('fast', function(){
						$(this).siblings().show();
					});
				},1400);


			}
		).error(function(err){
			console.log(err);
		});

		return false;
	});

});

function save_inci_csv(the_action){
	var inci_id = $('#close_inci_form').attr('data-id');
	window.location.href= base_url+'cc/recall/generate_csv/'+inci_id+'/'+the_action;

	setTimeout(function(){
		window.location.reload(true);
	}, 6000);

};

function closeRecallInvalid(){
	bootbox.alert('<div class="lead"><i class="fa fa-info-circle text-info"></i> Complete all tasks to close incident</div>');
}


$('#assign_recall').click(function(e) {

	var tsk_id = $('#the_question_id').val();
	var curr_assigned = $('#the_current_assigned').val();
	the_current_assigned
	if ($('[name="message_receiver"]:checked').length == 0){
		$('.err_holder').html('Mandatory field');
		return false;
	}
	else{

		$.ajax({
		  type: "POST",

		  url: base_url+'cc/recall/assign_the_crt',

		  data: {
			  question_id: tsk_id,
			  curr_assigned: curr_assigned,
			  message_receiver: $('[name="message_receiver"]:checked').val()
		  },

		  success: function(data) {
				console.log(data);
				$('#recallModal').modal('hide');

				if(data == 'was_update'){
					$('#refreshPageModal').modal('show');
					return false;
				}

				else if(data != 'error'){
					$('#assign_btn'+tsk_id).closest('tr').find('span.label').removeClass('danger').addClass('orange');
					$('#assign_btn'+tsk_id).closest('tr').find('span.label').html(data);

					//filter again after assigning crt
					$('#assign_btn'+tsk_id).closest('table').find('input[data-action="filter"]').change();
				}

				$('.err_holder').html('');
				$('#assign_btn'+tsk_id).closest('tr').find('a.complete-task-btn').addClass('disabled');
		  }

		});
	}

});//.assign recall


var the_blocker_count;

function checkTheBlocker(status,blocker_id,blocker_count,recall_id){


	if(typeof the_blocker_count === "undefined"){
		the_blocker_count = parseInt(blocker_count);
	}

	if(status == '1'){
		bootbox.confirm("<p class='lead'>"+action_messages.recall.mark_blocker_resolved+"</p>", function(r)
		{
			if(r)
			{
				$.ajax({
				  type: "POST",

				  url: base_url+'cc/recall/mark_check_blocker',

				  data: {
					  id: blocker_id,
					  status: status
				  },

				  success: function(data) {
						console.log(data);


						$("#inc_blck"+blocker_id).fadeOut(500, function(){
							$("#inc_blck"+blocker_id).siblings('button').show();
						});

						$("#comp_blck"+blocker_id).closest('tr').find('a.btn').fadeOut('fast');
						$("#comp_blck"+blocker_id).closest('tr').find('.date_resolved_holder').html(data).fadeIn('fast');

						the_blocker_count = the_blocker_count - 1;

						console.log(the_blocker_count);

						if(the_blocker_count > 0){
							$('#blockers_li'+recall_id+' a').removeClass('text-green bg-success');
							$('#blockers_li'+recall_id+' a').addClass('text-red bg-danger');

							if($('#blockers_li'+recall_id).find('.label-danger').length > 0){

								$('#blockers_li'+recall_id).find('.label-danger').html(the_blocker_count);
							}else{
								$('#blockers_li'+recall_id+' a').prepend('<span class="label label-danger" style="font-size: 65%; border-radius:50%; padding-left: 7px;padding-right: 7px;">'+the_blocker_count+'</span>');
							}

							$('#blockers_li'+recall_id).find('.fa-check-circle').remove();

						}else{
							$('#blockers_li'+recall_id+' a').removeClass('text-red bg-danger');
							$('#blockers_li'+recall_id+' a').addClass('text-green bg-success');
							$('#blockers_li'+recall_id).find('.label-danger').remove();
							if ($('#blockers_li'+recall_id).find('.fa-check-circle').length > 0){
								$('#blockers_li'+recall_id).find('.fa-check-circle').show();

							}else{
								$('#blockers_li'+recall_id+' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
							}

						}

				  }
				});

			}
		});

	}else{
		bootbox.confirm("<p class='lead'>"+action_messages.recall.mark_blocker_unresolved+"</p>", function(r)
		{
			if(r)
			{
				$.ajax({
				  type: "POST",

				  url: base_url+'cc/recall/mark_check_blocker',

				  data: {
					  id: blocker_id,
					  status: status
				  },

				  success: function(data) {
						console.log(data);

						$("#comp_blck"+blocker_id).fadeOut(500, function(){
							$("#comp_blck"+blocker_id).siblings('button').show();
						});
						$("#comp_blck"+blocker_id).closest('tr').find('a.btn').fadeIn('fast');
						$("#comp_blck"+blocker_id).closest('tr').find('.date_resolved_holder').html(data).fadeIn('fast');


						the_blocker_count = the_blocker_count + 1;
						console.log(the_blocker_count);

						if(the_blocker_count > 0){
							$('#blockers_li'+recall_id+' a').removeClass('text-green bg-success');
							$('#blockers_li'+recall_id+' a').addClass('text-red bg-danger');
							$('#blockers_li'+recall_id).find('.fa-check-circle').remove();

							if($('#blockers_li'+recall_id).find('.label-danger').length > 0){

								$('#blockers_li'+recall_id).find('.label-danger').html(the_blocker_count);
							}else{
								$('#blockers_li'+recall_id+' a').prepend('<span class="label label-danger" style="font-size: 65%; border-radius:50%; padding-left: 7px;padding-right: 7px;">'+the_blocker_count+'</span>');
							}

						}else{
							$('#blockers_li'+recall_id+' a').removeClass('text-red bg-danger');
							$('#blockers_li'+recall_id+' a').addClass('text-green bg-success');
							$('#blockers_li'+recall_id).find('.label-danger').remove();
							if ($('#blockers_li'+recall_id).find('.fa-check-circle').length > 0){
								$('#blockers_li'+recall_id+' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');

							}else{
								$('#blockers_li'+recall_id).find('.fa-check-circle').show();
							}

						}


				  }
				});

			}
		});
		}
}


$('#save_message_rc').click(function(e) {

	var tsk_id = $('#the_question_id').val();
	if ($('#txt_message').val() == ''){
		$('.text_error').html('Message is required');
		return false;
	}
	else{

		$.ajax({
		  type: "POST",

		  url: base_url+'cc/recall/add_message_respond',

		  data: {
			  question_id: tsk_id,
			  txt_message: $('#txt_message').val()
		  },

		  success: function(data) {

				console.log(data);

				$('.text_error').html('');

				$('#recallRespond').modal('hide');
				$('#comptask_btn'+tsk_id).closest('tr').find('#show_rc_respond_btn').show();
				$('#comptask_btn'+tsk_id).closest('tr').find('#tsk_not_strted_holder').hide();

				$('#comptask_btn'+tsk_id).closest('tr').find('span.danger').html(user_full_name);
				$('#comptask_btn'+tsk_id).closest('tr').find('span.danger').removeClass('danger').addClass('orange');

		  }

		});
	}

});//assign recall


//display save button when start to answer stp3 table
$('.thumbnail.no-margin-btm').click(function(){

	if($( '.no-margin-btm.selected' ).length > 0){
		$('.save_stp3_table').show();
	}

});

//disabling clicking of boxes in table

$('.thumbnail.no-margin-btm').click(function(e){
	if($('.save_stp3_table').hasClass('hidden')){
		e.stopImmediatePropagation();
		//bootbox.alert("<p class='lead'><i class='fa fa-info-circle text-info'></i> This task is already completed.</p>");
		return false;
	}
});


$('.save_stp3_table').click(function(){

	var step_3_table_id = $("input[name='step_3_table_id']").val();
	var severity = $("input[name='severity']:checked").val();
	var presence = $("input[name='presence']:checked").val();
	var like_dist = $("input[name='like_dist']:checked").val();
	var distribution = $("input[name='distribution']:checked").val();
	var identification = $("input[name='identification']:checked").val();
	var hazard = $("input[name='hazard']:checked").val();



	if(!severity){

		$('.stp3_remaining_items').html('Select Severity');
		return false
	}
	else if(!presence){

		$('.stp3_remaining_items').html('Select Presence');
		return false
	}
	else if(!like_dist){

		$('.stp3_remaining_items').html('Select Likely Injury');
		return false
	}
	else if(!distribution){

		$('.stp3_remaining_items').html('Select Distribution');
		return false
	}
	else if(!identification){

		$('.stp3_remaining_items').html('Select Identification');
		return false
	}
	else if(!hazard){

		$('.stp3_remaining_items').html('Select Hazard');
		return false
	}
	else{
		$('.stp3_remaining_items').html('<i class="fa fa-spinner fa-spin text-muted"></i> <span class="text-muted">Saving..</span>');
		console.log('ow yeah');


		$.ajax({
		  type: "POST",

		  url: base_url+'cc/recall/save_stp3_table',

		  data: {
			  severity: severity,
			  presence: presence,
			  like_dist: like_dist,
			  distribution: distribution,
			  identification: identification,
			  hazard: hazard,
			  step_3_table_id: step_3_table_id
		  },

		  success: function(data) {

				console.log(data);


				$('.stp3_remaining_items').html('<i class="fa fa-check-circle text-success"></i> <span class="text-success">Saved</span>');

			  setTimeout(function(){
				$('.stp3_remaining_items').html('');
			  }, 2000 );
		  }

		});

	}
}); //display save button when start to answer stp3 table

/*save new recall task*/
$('#save_new_rc_tsk').click(function(){
	var tsk_name = $('#new_tsk_name').val();
	var tsk_guide = $('#new_tsk_guide').val();
	var step_no = $('#newtsk_step_no').val();
	var recall_id = $('#newtsk_recall_id').val();
	var select_subcat = $('#select_subcat').val();

	if(tsk_name == ''){
		$('.new_tsk_name_err').html('Mandatory field');
		return false;
	}
	else{

		$.ajax({
		  type: "POST",

		  url: base_url+'cc/recall/save_new_rc_tsk',

		  data: {
			  tsk_name: tsk_name,
			  category_id: select_subcat,
			  tsk_guide: tsk_guide,
			  step_no: step_no,
			  recall_id: recall_id
		  },

		  success: function(data) {

			console.log(data);
			window.location.reload(true);

		  }

		});

	}

});

function getRCTaskRespond(task_id){
	$('#respondtask_holder').html('<div class="text-center text-muted" style="padding: 50px;"><i class="fa fa-spinner fa-spin fa-2x"></i><br/>Loading content..</div>');
	$.ajax({
		  type: "POST",

		  url: base_url + 'cc/recall/get_task_response',

		  data: {
			  task_id: task_id
		  },

		  success: function(data) {
			 //console.log(data);
			$('#respondtask_holder').html(data);

		  }
	});

}


function addNewRCTask(step_no,rc_id){
	$('#newtsk_step_no').val(step_no);
	$('#newtsk_recall_id').val(rc_id);



	$('#sub_cat').html('<i class="fa fa-spinner fa-spin"></i>');
	$.ajax({
		  type: "POST",

		  url: base_url + 'cc/recall/select_step',

		  data: {
			  step: step_no,
			  rc_id: rc_id
		  },

		  success: function(data) {
			 console.log(data);
			 $('#sub_cat').html(data);

		  }

	});
}

function addTGuide(guide){
	var control = $('#myfile');
	control.replaceWith( control = control.clone( true ) );

	$('#atta_file_btn').show();
	$('.upload_section').hide();
	$('#progress').hide();
	$('#file_up_text').hide();
	$('#file_up_text a').remove();
	$('#file_up_text span').remove();
	if (guide == ''){
		$('#modal_guide_btn').hide();
		$('#modal_guide_holder div').hide();
	}
	else{
		$('#modal_guide_btn').show();
		$('#modal_guide_holder div').show();
	}
	$('#modal_guide_holder div').html(guide);
}




/*chat*/

var name = user_full_name;
var org = "";
var senderr = "";
var receiverr = "";
var time_sent = now_time;
// default name is 'Guest'
if (!name || name === ' ') {
   name = "Guest";
}

// strip tags
name = name.replace(/(<([^>]+)>)/ig,"");

// display name on page
//$("#name-area").html("You are: <span>" + name + "</span>");

// kick off chat
var chat =  new Chat();



$(document).ready(function(e) {


	 // watch textarea for key presses
	 $("#sendie").keydown(function(event) {

		 var key = event.which;

		 //all keys including return.
		 if (key >= 33) {

			 var maxLength = $(this).attr("maxlength");
			 var length = this.value.length;

			 // don't allow new content if length is maxed out
			 if (length >= maxLength) {
				 event.preventDefault();
			 }
		  }
	  });
	 // watch textarea for release of key press
	 $('#sendie').keyup(function(e) {

		  if (e.keyCode == 13) {
			var text = $(this).val();
			var maxLength = $(this).attr("maxlength");
			var length = text.length;

			// send
			if (length <= maxLength + 1) {

				chat.send(text, name, time_sent, org, senderr, receiverr);
				$(this).val("");

			} else {

				$(this).val(text.substring(0, maxLength));

			}


		  }
	 });

	//load the online list automatically
	$.ajax({
		url: base_url+'cc/conference/onlines',
		cache: false,
		success: function(data){
			$("#results").html(data);
		}
	});

	//refresh list of online
	var refreshIntervalId = setInterval(function(){

		$.ajax({
			url: base_url+'cc/conference/onlines',
			cache: false,
			success: function(data){
				//refresh on session expires
				if (data == '<li><span class="text-muted"><i class="fa fa-warning"></i> Your session expired. Please login again to continue.</span></li>'){
					$('.modal').modal('hide');
					bootbox.alert('<p class="lead"><i class="fa fa-info-circle text-info"></i> Your session expired. Please login again to continue.</p>', function(){
						window.location.reload(true);
					});
					clearInterval(refreshIntervalId);
				}

				$("#results").html(data);
				// console.log(data);

			}
		});



	}, 4000);

	refreshIntervalId;



	$('#coll_count_unread').hide();
	//refresh list of online
	var check_unread_count = parseInt('0');
	setInterval(function(){

		//number of online
		var count = $("#results").children('li.ol').length;

		if (count > 1){

			$('#count_online').html('There are ' + count + ' online peers');
			$('#number_online').html(count);

			var total_unread = parseInt($('#total_unread').html());

			if(total_unread != '0'){
				$('#coll_count_unread').show();
				$('#unread_number').html(total_unread);

				if(total_unread > check_unread_count){
					document.getElementById('audiotag1').play();
					check_unread_count = total_unread;
				}
			}
			else{
				$('#coll_count_unread').hide();
				check_unread_count = parseInt('0');
			}
		}
		else{
			$('#count_online').html('There is ' + count + ' online peer');
			$('#number_online').html(count);

			var total_unread = parseInt($('#total_unread').html());

			if(total_unread != '0'){
				$('#coll_count_unread').show();
				$('#unread_number').html(total_unread);

				if(total_unread > check_unread_count){
					document.getElementById('audiotag1').play();
					check_unread_count = total_unread;
				}
			}
			else{
				$('#coll_count_unread').hide();
				check_unread_count = parseInt('0');
			}

		}


	}, 1000);

});//document.ready




function changeReiceverCookie(disp_name,initiator,receiver){

	if(getCookie("initiator") != ""){
		setCookie("initiator", "", "-1");
		setCookie("initiator", initiator, "1");
	}
	else{
		setCookie("initiator", initiator, "1");
	}

	if(getCookie("receiver") !=""){
		setCookie("receiver", "", "-1");
		setCookie("receiver", receiver, "1");
	}
	else{
		setCookie("receiver", receiver, "1");
	}

	if(getCookie("disp_name") !=""){
		setCookie("disp_name", "", "-1");
		setCookie("disp_name", disp_name, "1");
	}
	else{
		setCookie("disp_name", disp_name, "1");
	}

}

function createChatroom(disp_name,initiator,receiver){


	changeReiceverCookie(disp_name,initiator,receiver);

	$.ajax({
		  type: "POST",

		  url: base_url+'cc/conference/have_read',

		  data: {
			  initiator: initiator,
			  receiver: receiver
		  },

		  success: function(data) {

			console.log(data);

		  }

	});

	//console.log(receiver);
	var sender = parseInt(initiator);
	var receive = parseInt(receiver);

	if(sender > receive){
		var the_im_room = initiator + "-" + receiver;
	}
	else{
		var the_im_room = receiver + "-" + initiator;
	}
	org = '';
	org = 'im_'+the_im_room;
	receiverr = '';
	senderr = '';
	receiverr = receiver;
	senderr = initiator;
	//org.push(the_im_room);

	startChat();

	//set cookie for chat pm
	showChatPm();

	if(getCookie("remove_im") !=""){
	}
	else{
		setCookie("remove_im", "block", "1");
	}


	$('#sendie').val('');
	$('#chat_username_top').html(disp_name);
	$('#chat_username_bottom').html(disp_name);
	$('#page-wrap-pm').show();
	$('#chat-area').html('');

	if($('#chat-area').html() == ''){

			$('#chat-area').html('<div class="text-center text-muted start_con">Start conversation</div><div class="text-center loading_mess" style="padding-top: 30px"><i class="fa fa-spinner text-muted fa-spin"></i></div>');
			//removeLoadingMess();
	}

}

function removeLoadingMess(){
	$('.loading_mess').delay(500).hide();
}

function startChat(){

	chat.getState();
	setInterval( 'chat.update()' , 400);

}

function resendCRT(login_id){

	bootbox.confirm("<p class='lead'>"+action_messages.global.resend_crt_request+"</p>", function(r){

		if(r){

			$.ajax({
			  type: "POST",

			  url: base_url+'cc/crisisteam/resend_request',

			  data: { login_id: login_id},

			  success: function(data) {

					console.log(data);
					window.location.reload(true);

			  }

			});
		}
	});



}

//import crts
function importCRTs(active_module,addmore){

	$.ajax({
	  type: "POST",

	  url: base_url+'cc/crisisteam/import_crt',

	  data: { active_module: active_module, addmore: addmore},

	  success: function(data) {

			console.log(data);

			$('#importCRT').find('.modal-body').html(data);

	  }

	});
}


function countToImport(addmore){
	var addm = parseInt(addmore);
	var checked = $('input[name="checkimport[]"]:checked').length;

	if( $('input[name="checkimport[]"]:checked').length == 0){
		$('#save_import').addClass('disabled');
	}

	else if(checked > addm){
		$('#importCRT').find('.modal-body div.alert').show();
		$('#save_import').addClass('disabled');
	}else{
		$('#importCRT').find('.modal-body div.alert').hide();
		$('#save_import').removeClass('disabled');
	}
}

$(document).ready(function(e) {

	if( $('input[name="checkimport[]"]:checked').length == 0){
		$('#save_import').addClass('disabled');
	}else{
		$('#save_import').removeClass('disabled');
	}


	$('#save_import').click(function(e) {

		var crts = $('input[name="checkimport[]"]:checked').map(function(){
		  return $(this).val();
		}).get();

		console.log(crts);

		$.ajax({
		  type: "POST",

		  url: base_url+'cc/crisisteam/save_import_crt',

		  data: {
			  checked: crts,
			  active_module: $('#active_module').val()
		  },

		  success: function(data) {

				console.log(data);
				window.location = data;

		  }

		});

	});


	//check if selected message
	$('input.selectedId').click(function(e) {
		if($('input[name="selectedId[]"]:checked').length > 0){
			$('#addtotrash').removeClass('disabled');

		}else{
			$('#addtotrash').addClass('disabled');
		}
	});

	$('#selectall').click(function(e) {
		if($('input[name="selectedId[]"]:checked').length > 0){
			$('#addtotrash').removeClass('disabled');

		}else{
			$('#addtotrash').addClass('disabled');
		}
	});

	//submit the form
	$( "#addtotrash" ).click(function() {
		$(this).closest('form').submit();
	});

});//document.ready

$(".tooltip-test, [data-tooltip]").tooltip({
	'selector': '',
	'container':'body'
});

//filter task button
function filterTasks(step_no,crt_name,this_btn){
	console.log(this_btn);
	$('#recall-step-filter'+step_no).val(crt_name);
	$('#recall-step-filter'+step_no).change();
	$(this_btn).closest('li').addClass('active');
	$(this_btn).closest('li').siblings().removeClass('active');

	$('label'+this_btn).removeClass('btn-default');
	$('label'+this_btn).addClass('btn-primary');
	$('label'+this_btn).siblings().removeClass('btn-primary');
	$('label'+this_btn).siblings().addClass('btn-default');

}

function sendIncidentStatus(recall_id){

	bootbox.confirm("<p class='lead'>"+action_messages.recall.send_incident_status+"</p>", function(r){

		if(r){

			$.ajax({
			  type: "POST",

			  url: base_url+'cc/recall/send_incident_status',

			  data: { recall_id: recall_id},

			  success: function(data) {

					console.log(data);
					window.location.reload(true);

			  }

			});
		}
	});

}


function updateCostCat(id,name){
	$('#cat_id').val(id);
	$('#name_cat').val(name);
	$('#addCostCat').modal('show');
}








//cost items

$(document).ready(function(e) {

	$('#name_cat_btn').click(function(e) {

		$('.name_cat_error').html('');

		var name_cat = $('#name_cat').val();
		var recall_id = $('#recall_id').val();
		var cat_id = $('#cat_id').val();

		if(name_cat == ''){
			$('.name_cat_error').html('This field is required.');
			return false;
		}
		else{

			$('#name_cat_btn').html('loading...');
			$('#name_cat_btn').addClass('disabled');
			$.ajax({
			  type: "POST",

			  url: base_url+'cc/costmonitor/add_category',

			  data: {
				  name_cat: name_cat,
				  recall_id: recall_id,
				  cat_id: cat_id
			  },

			  success: function(data) {

					console.log(data);
					if(data =='success'){
						window.location.reload();
					}

			  }

			});
			return false;
		}
	});//end of name_cat_btn



	$('#cost_item_btn').click(function(e) {

		$('.item_cost_error').html('');
		$('.item_name_error').html('');
		$('.currency_code_error').html('');
		$('.item_invoice_error').html('');

		var item_name = $('#item_name').val();
		var item_cost = $('#item_cost').val();
		var currency_code = $('#currency_code').val();
		var uniquetime = $('#uniquetime').val();
		var item_invoice = $('#inci_date').val();

		var recall_id = $('#recall_id').val();
		var cost_item_id = $('#cost_item_id').val();
		var cat_id = $('#cost_id').val();
		if(item_name == ''){
			$('.item_name_error').html('This field is required.');
			return false;
		}
		else if(item_cost == ''){
			$('.item_cost_error').html('This field is required.');
			return false;
		}
		/*else if(isNaN(item_cost)){
			$('.item_cost_error').html('Positive digit is required.');
			return false;
		}*/
		else if(currency_code == ''){
			$('.currency_code_error').html('This field is required.');
			return false;
		}
		else if(item_invoice == ''){
			$('.item_invoice_error').html('This field is required.');
			return false;
		}
		else{

			$('#cost_item_btn').html('loading...');
			$('#cost_item_btn').addClass('disabled');
			$.ajax({
			  type: "POST",

			  url: base_url+'cc/costmonitor/add_cost_item',

			  data: {
				  item_name: item_name,
				  item_cost: item_cost,
				  recall_id: recall_id,
				  item_invoice: item_invoice,
				  currency_code: currency_code,
				  uniquetime: uniquetime,
				  cost_item_id: cost_item_id,
				  cat_id: cat_id
			  },

			  success: function(data) {

					console.log(data);
					if(data =='success'){
						window.location.reload();
					}

			  }

			});
			return false;

		}
	});//end of cost_item_btn



});//document.ready


function updateItemCost(id,name,cost,code,invoice){
	$('#item_name').val(name);
	$('#item_cost').val(cost);
	$('#cost_item_id').val(id);
	$('#inci_date').val(invoice);

	$('#currency_code option[value="'+code+'"]').siblings().removeAttr('selected');
	$('#currency_code option[value="'+code+'"]').attr('selected','selected');
	$('#addCostItem').modal('show');
}

function resetItemFields(cost_id,currency_default){
	$('#cost_id').val(cost_id);
	$('#cost_item_id').val('');
	$('#item_cost').val('');
	$('#item_name').val('');
	$('#inci_date').val('');

	$('#currency_code option[value="'+currency_default+'"]').siblings().removeAttr('selected');
	$('#currency_code option[value="'+currency_default+'"]').attr('selected','selected');
}
function showCostSummary(id){
	$('#viewCategorySummary .modal-body').html('<h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>');
	$('#viewCategorySummary .modal-body').load(base_url+"cc/costmonitor/get_summary_details/"+id,function(e) {
		console.log(id);
	});
}

function viewCostItemDetails(id){
	$.ajax({
	  type: "POST",

	  url: base_url+'cc/costmonitor/get_item_details',

	  data: {
		  id:id
	  },

	  success: function(data) {

			console.log(data);
			$('#viewCostItem .modal-body').html(data);
	  }

	});
}

function addCurrencyDropdown(default_currency_id,recall_id){
	$('#updateDefaultCurrency .modal-body').html('<h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>');
	$('#updateDefaultCurrency .modal-body').load(base_url+"cc/costmonitor/get_currency_dropdown/"+default_currency_id+"/"+recall_id,function(e) {
		console.log(default_currency_id);
	});

}


$(document).ready(function(e) {

	$('#default_currency_btn').click(function(e) {

		$('.default_currency_code_error').html('');

		var currency_id = $('#default_currency_code').val();
		var recall_id = $('#recall_id').val();

		if(currency_id == ''){
			$('.default_currency_code_error').html('This field is required.');
			return false;
		}

		else{

			$('#default_currency_btn').html('loading...');
			$('#default_currency_btn').addClass('disabled');
			$.ajax({
			  type: "POST",

			  url: base_url+'cc/costmonitor/update_default_currency',

			  data: {
				  currency_id: currency_id,
				  recall_id: recall_id,
			  },

			  success: function(data) {

					console.log(data);
					if(data =='success'){
						window.location.reload();
					}

			  }

			});
			return false;

		}
	});//end of default_currency_btn

	$('#item_cost').keyup(function(event) {

	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40){
		event.preventDefault();
	  }

	  $(this).val(function(index, value) {
		return value
		  .replace(/\D/g, "")
		  .replace(/([0-9])([0-9]{2})$/, '$1.$2')
		  .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
		;
	  });
	});


});


//pre incident - documents
$(document).ready(function(e) {

	$('.move_doc_btn').on('click', function(){
		$self = $(this);
		var cat = $self.data('cat');
		var id = $self.data('id');
		$('#moveDocModal').find('[name="categories"]').val(cat);
		$('#moveDocModal').find('[name="doc_id"]').val(id);
		$('#moveDocModal').modal('show');
		return false;
	});

	$('#move_doc_form').on('submit', function(){

		$self = $(this);
		$self.find('button').addClass('disabled').html('Loading..');
		$.post(
			base_url+'cc/document/move_doc',
			$self.serialize(),
			function(res){
				console.log(res);
				window.location.reload(true);
			}
		);
		return false;
	});

    $('.add_doc_cat_btn').on('click', function(){
		$('#docCategoryModal').modal('show').find('.form-control').val('');
		$('#docCategoryModal').find('input[type="checkbox"]').prop('checked', false);

		return false;
	});

    $('.update_doccat_btn').on('click', function(){
		var id = $(this).data('id');
		var name = $(this).data('name');
		console.log($('#docCategoryModal').find('[name="name"]').length);
		$('#docCategoryModal').find('[name="id"]').val(id);
		$('#docCategoryModal').find('[name="name"]').val(name);
		$('#docCategoryModal').find('input[type="checkbox"]').prop('checked', true);
		$('#docCategoryModal').modal('show');
		return false;
	});

	$('#doc_cat_form').find('.form-control').on('keyup', function(){
		var count = 0;
		$the_form = $('#doc_cat_form');
		$the_form.find('.form-control').each(function(index, element) {
            if($(this).val() == ''){
				count++;
			}
        });

		if(count > 0){
			$the_form.find('.btn-primary').attr('disabled','disabled');
		}
		else{
			$the_form.find('.btn-primary').removeAttr('disabled');
		}
	});

	$('#group_all').on('change', function(){
		if($(this).is(":checked")){
			$('[name="group[]"]').prop('checked', true);
		}
		else{
			$('[name="group[]"]').prop('checked', false);
		}
	});

	$('#doc_cat_form').on('submit', function(){
		var $self = $(this);
		$self.find('.btn-primary').addClass('disabled').html('Saving..');
		console.log($self.serialize());

		$.post(
			base_url+'cc/document/addcategory',
			$self.serialize(),
			function(res){
				console.log(res);
				$self.find('.btn-primary').addClass('disabled').html('Saved!');
				setTimeout(function(){
					$('.modal').modal('hide');
					$self.find('.btn-primary').removeClass('disabled').attr('disabled','disabled').html('Submit');
					window.location.reload(true);
				}, 1500);
			},
			'json'
		).error(function(err){
			console.log(err);
		});

		return false;
	});


	$('#top_up_form').on('submit', function(){
		var $self = $(this);
		$self.find('button').html('Processing..').prop('disabled', true);


		if($('.credit_fields.hidden').length > 0){

			$.post(
				base_url+'cc/communication/top_up',
				$self.serialize(),
				function(res){
					console.log(res);

					if(res.result != 'success'){
						$self.find('.payment-errors').text(res.result).closest('div').removeClass('hidden');
						$('.credit_fields').removeClass('hidden');
						$self.find('button').html('Purchase').prop('disabled', false);
					}
					else{
						//alert(res.result);
						window.location.reload(true);
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}//if credit form not hidden
		else{
			Stripe.card.createToken($self, stripeResponseHandler);
		}




		return false;
	});


});//document.ready



//sending and sms - notification area
var sendbulk = function(bulk_type, smsmessage, emailmessage){

	console.log('edi wow');

	if(bulk_type == 'emailsms') {
		$('.send_email_btn').addClass('disabled');
		$('.send_sms_btn').addClass('disabled').html('Sending..');
	}
	else{
		$('.send_sms_btn').addClass('disabled');
		$('.send_email_btn').addClass('disabled').html('Sending..');
	}

	//loop each contacts
	var selected_contact = $('input[name="bulk_notis[]"]:checked').length;
	var count_sent = 1;
	$.each($('input[name="bulk_notis[]"]:checked'), function() {
		//values.push($(this).val());

		var data = {
			"bulk_data_id": $(this).val(),
			"bulk_type": bulk_type,
			"smsmessage": smsmessage,
			"emailmessage": emailmessage
		};

		console.log($(this).val());

		data = $(this).serialize() + "&" + $.param(data);

		$.ajax({
			type: "POST",
			url : base_url+'cc/communication/send_bulk_email',
			dataType : 'json',
			data: data,
			success : function(response) {

				console.log(response);
				var total_percent = (count_sent/selected_contact) *100;
				var n = total_percent.toFixed(0);

				if(bulk_type == 'emailsms') {
					$('.send_sms_btn').html('Sending..('+n+'%)');
				}
				else{
					$('.send_email_btn').html('Sending..('+n+'%)');
				}


				//console.log(count_sent+' '+selected_contact);
				if(selected_contact == count_sent){
					window.location.reload(true);
					//$('.send_sms_btn').html('Sent');
				}
				count_sent++;

			},
			error: function(err){
				console.log(err);
			}
		});//end of ajax


	});	//end of foreach checked rows



}//end sendbulk


$(document).ready(function(e) {
	//add new bulk
	$('.add_new_bulk').on('click', function(){
		$(this).addClass('disabled').html('Loading..');
		console.log('er');
		$.ajax({
			url: base_url+"cc/communication/addbulk"
		}).done(function(data) {
			console.log(data);
			window.location.href = base_url+'cc/communication/manage/'+data;
		});
	});


	//save rename bulk
	$('#comm_name_form').on('submit', function(){
		var bulk_name = $(this).find('input[name="bulk_name"]').val();
		var assoc_incident = $(this).find('#assoc_incident').val();
		var bulk_id = $(this).find('input[name="bulk_id"]').val();

		$(this).find('span.text-danger').addClass('hidden');

		if(bulk_name == ''){
			$(this).find('input[name="bulk_name"]').focus().siblings('span').removeClass('hidden').html('Mandatory field.');
			return false;
		}
		else if(assoc_incident == '0'){
			$(this).find('#assoc_incident').focus().siblings('span').removeClass('hidden').html('Mandatory field.');
			return false;
		}

		var data = {
			"bulk_name": bulk_name,
			"bulk_id": bulk_id,
			"assoc_incident": assoc_incident
		};


		data = $(this).serialize() + "&" + $.param(data);

		$.ajax({
			type: "POST",
			url : base_url+'cc/communication/addupdatebulk',
			dataType : 'json',
			data: data,

			success: function(respond) {
				console.log(respond);

				if(respond.type == 'add'){
					window.location.href = base_url+'cc/communication/manage/'+respond.id;
				}
				else{
					window.location.reload(true);
				}
			}
		});

		return false;
	});

	//save kpi
	$('#kpis_form').on('submit', function(){
		var company_control = $(this).find('#company_control').val();
		var distribution = $(this).find('#distribution').val();
		var consumer_market = $(this).find('#consumer_market').val();
		var assoc_incident = $(this).find('#assoc_incident').val();
		var kpi_id = $(this).find('#kpi_id').val();

		$(this).find('span.text-danger').addClass('hidden');

		if(company_control == ''){
			$(this).find('#company_control').focus().siblings('span').removeClass('hidden').html('Mandatory field.');
			return false;
		}
		else if(distribution == ''){
			$(this).find('#distribution').focus().siblings('span').removeClass('hidden').html('Mandatory field.');
			return false;
		}

		else if(consumer_market == ''){
			$(this).find('#consumer_market').focus().siblings('span').removeClass('hidden').html('Mandatory field.');
			return false;
		}

		else if(assoc_incident == '0'){
			$(this).find('#assoc_incident').focus().siblings('span').removeClass('hidden').html('Mandatory field.');
			return false;
		}

		var data = {
			"company_control": company_control,
			"distribution": distribution,
			"consumer_market": consumer_market,
			"kpi_id": kpi_id,
			"assoc_incident": assoc_incident
		};


		data = $(this).serialize() + "&" + $.param(data);

		$.ajax({
			type: "POST",
			url : base_url+'cc/kpi/addupdate',
			dataType : 'json',
			data: data,

			success: function(respond) {
				console.log(respond);
				window.location.reload(true);
			},
			error: function(){
				console.log('error');
			}
		});

		return false;
	});



	$("#letter_code").on('change', function(){
		var calling_code = $('option:selected', this).attr('data-calling_code');
		console.log($('option:selected', this).attr('data-calling_code'));
		$('#calling_code').val(calling_code);
	});



	//new contact
	$('#new_contact_form').on('submit', function(){
		var name = $(this).find('input[name="bulk_name"]').val();
		var contact_email = $(this).find('input[name="contact_email"]').val();
		var contact_mobile = $(this).find('input[name="contact_mobile"]').val();
		var letter_code = $(this).find('#letter_code').val();

		$(this).find('span.form-helper').addClass('hidden');
		var $self = $(this);

		if(name == ''){
			$self.find('input[name="bulk_name"]').focus().siblings('span').removeClass('hidden');
			return false;
		}
		else if(contact_email == ''){
			$self.find('input[name="contact_email"]').focus().siblings('span').html('Mandatory field.').removeClass('hidden');
			return false;
		}
		else if(contact_email != ''){
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

			if(!regex.test(contact_email)){
				$self.find('input[name="contact_email"]').focus().siblings('span').html('Valid email is required').removeClass('hidden');
				return false;
			}


			else if(letter_code == ''){
				$self.find('#letter_code').focus().siblings('span').html('Mandatory field.').removeClass('hidden');
				return false;
			}


			else if(contact_mobile != ''){
				if(isNaN(contact_mobile)){

					$self.find('input[name="contact_mobile"]').focus().siblings('span').html('Valid number is required.').removeClass('hidden');
					return false;
				}
				else{

					value = contact_mobile.replace(/^(0*)/,"");
					contact_mobile = value;

				}
			}

		}

		var data = {
			"bulk_name": name,
			"contact_email": contact_email,
			"contact_mobile": contact_mobile,
			"letter_code": letter_code,
			"bulk_id": $(this).find('input[name="bulk_id"]').val()
		};


		data = $(this).serialize() + "&" + $.param(data);

		$.ajax({
			type: "POST",
			url : base_url+'cc/communication/newcontact',
			dataType : 'json',
			data: data,

			success: function(respond) {
				console.log(respond);
				window.location.reload(true);
			}
		});

		return false;

	});

	$('#bulkmessages_form').on('submit', function(){
		var bulk_type = $('#bulk_type').val();
		var smsmessage = $('#smsmessage').val();
		var emailmessage = $('#emailmessage').val();


		if(emailmessage == ''){
			$('#emailmessage').focus().siblings('span').show();
		}
		else{
			$('.modal').modal('hide');
			sendbulk(bulk_type, smsmessage, emailmessage);

		}

		return false;
	});

	//send emails
	$('.send_email_btn').on('click', function(){
		var bulk_type = 'emailonly';
		$('#bulk_type').val(bulk_type);
		//sendbulk(bulk_type);
		$('#profile').addClass('active');
		$('#profile').siblings().removeClass('active');
		$('#bulkMessage').find('.modal-footer').show();
		$('#bulkMessage').find('.modal-footer a').hide();//hide back button
		$('#bulkMessage').modal('show');
	});//end send emails

	//send emails
	$('.send_sms_btn').on('click', function(){
		var bulk_type = 'emailsms';
		$('#bulk_type').val(bulk_type);
		//sendbulk(bulktype);
		$('#home').addClass('active');
		$('#home').siblings().removeClass('active');
		$('#bulkMessage').find('.modal-footer').hide();
		$('#bulkMessage').find('.modal-footer a').show();//show back button
		$('#bulkMessage').modal('show');
	});//end send emails


	//toggle select all
    $('#sel_all_bulk').on('click', function(){
		if($('input[name="sel_all_bulk"]:checked').length > 0){
			$('input[name="bulk_notis[]"]:not(:disabled)').prop("checked" , this.checked);

		}else{
			$('input[name="bulk_notis[]"]').prop("checked" , '');
		}

	});

	var sms_credit_text = $('.display_sms_total').html();
	//display sms price
	$('input[name="bulk_notis[]"], input[name="sel_all_bulk"]').on('click', function(){

		if($('input[name="bulk_notis[]"]:checked').length == 0){ //check if no checked row
			$('.display_sms_total').html(sms_credit_text);
			$('input[name="sel_all_bulk"]').prop("checked" , '');
			$('.top_up_message').hide();
			$('.send_sms_btn').addClass('disabled');
			$('.send_email_btn').addClass('disabled');
		}
		else{

			$('.send_email_btn').removeClass('disabled');
			$('.display_sms_total').html('Calculating total cost..').fadeIn('fast').addClass('blink');

			var values = new Array();
			var selected_contact = $('input[name="bulk_notis[]"]:checked').length;
			var count_sent = 1;
			$.each($('input[name="bulk_notis[]"]:checked'), function() {
				values.push($(this).val());

				var data = {
					"bulk_data_id": $(this).val()
				};

				console.log(values);

				data = $(this).serialize() + "&" + $.param(data);

				$.ajax({
					type: "POST",
					url : base_url+'cc/communication/get_sms_price',
					dataType : 'json',
					data: data,
					success : function(response) {

						console.log(response);
						//console.log(count_sent+' '+selected_contact);
						if(selected_contact == count_sent){

							//display total when all query ends
							var data_ids = {
								"bulk_data_id": values
							};

							console.log(values);

							data_ids = $(this).serialize() + "&" + $.param(data_ids);

							$.ajax({
								type: "POST",
								url : base_url+'cc/communication/get_total_price',
								dataType : 'json',
								data: data_ids,
								success : function(response) {
									$('.display_sms_total').html('Total Cost: $'+response.the_total).removeClass('blink');
									console.log(response);

									if(response.status == 'not_ok'){
										$('.top_up_message').show();
										$('.send_sms_btn').addClass('disabled');
									}
									else{
										$('.top_up_message').hide();
										$('.send_sms_btn').removeClass('disabled');
									}


								}
							});//end of ajax




						}
						count_sent++;



					}
				});//end of ajax

			});	//end of foreach checked rows


		}


	});//end display sms price


	//check what activ tab
	if($('#home').hasClass('active')){
		$('#bulkMessage').find('.modal-footer').hide();
	}

	if($('#profile').hasClass('active')){
		$('#bulkMessage').find('.modal-footer').fadeIn(400);
	}

	$('a[href="#profile"]').on('click', function(){
		var smsmessage = $('#smsmessage').val();

		if(smsmessage == ''){
			$('#smsmessage').focus().siblings('span').show();
			return false;
		}
		else{
			$('#bulkMessage').find('.modal-footer').fadeIn(400);
			$('#smsmessage').siblings('span').hide();
		}

	});

	$('a[href="#home"]').on('click', function(){
		$('#bulkMessage').find('.modal-footer').hide();
	});



});

//delete attached file
$(document).on("click", '.delbulk_attach', function(event) {
	console.log($(this).attr('data-filename'));
	var $self = $(this);
	var file_name = $(this).attr('data-filename');
	var unique = $(this).attr('data-unique');

	var data = {
		"filename": file_name,
		"attid": $(this).attr('data-attid')
	};

	data = $(this).serialize() + "&" + $.param(data);

	$.ajax({
		type: "POST",
		url : base_url+'cc/communication/delete_bulkattached',
		dataType : 'json',
		data: data,
		success : function(response) {
			console.log(response);
			$('#'+unique).remove();
			$self.remove();
		}
	});


});


//update bulk
function updateBulk(bulk_id, incident_id ,bulk_name){
	$('.bulk_id').val(bulk_id);
	$('#bulk_name').val(bulk_name);
	$('#assoc_incident').val(incident_id);
	console.log(incident_id);
}

$(document).ready(function(e) {
    $('[name="countrycodedd"]').on('change', function(){
		var colling = $(this).find(':selected').data('code');
		$('[name="countrycode"]').val(colling);
	});


	if(uri_2 == 'message' && uri_3 == 'group'){
		$.post(
			base_url+'cc/message/visited',
			{},
			function(res){
				console.log(res);
			},
			'json'
		).error(function(err){
			console.log(err);
		});
	}

	$('#crt_create_form').validate({
		rules: {
			'emails[]': {
				email: true,
				// required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
			//$('input#home_location').tooltip({'trigger':'focus', 'title': 'This field is required.'});
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			//$('input#home_location').tooltip('destroy');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			// element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			console.log($(form).serialize());
			var $email_field = $('[name="emails[]"]');
			// return false;

			if($email_field.length > 1){
				$(form).find('button.btn-block').button('loading');

				$.post(
					base_url+'cc/crisisteam/create_teammates_submit',
					$(form).serialize(),
					function(res){
						console.log(res);

						window.location.href = base_url+'cc/crisisteam/managecrt'; //setup';

					},
					'json'
				).error(function(err){
					console.log(err);
				});
			}
			else{
				bootbox.alert('Please add at least 1 teammate.');
			}


		}
	});

	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	$('.add-crt-email').on('click', function(){
		var $email_field = $('[name="emails[]"]:eq(0)');
		var email = $email_field.val();

		if(validateEmail(email)){

			bootbox.confirm({
				message: 'Select user type:',
				buttons: {
					confirm: {
						label: 'Administrator',
						className: 'btn-default btn-116w'
					},
					cancel: {
						label: 'User',
						className: 'btn-default btn-116w'
					}
				},
				callback: function (result) {
					console.log('This was logged in the callback: ' + result);

					var utype = (result) ? '0' : '1';
					var utype_txt = (result) ? 'Administrator' : 'User';
					
					var arr = '<p><a href="#" class="delete-create-teammate btn btn-default btn-xs pull-right"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span>'+email+'</span> <span class="label label-primary">'+utype_txt+'</span><input type="hidden" name="emails[]" value="'+email+'"><input type="hidden" name="utype[]" value="'+utype+'"></p>';
		
					$('.group-email-list').append(arr);
		
					$email_field.val('');

				}
			});

		}
		
	});

	$(document).on('click', '.delete-create-teammate', function(){
		$(this).closest('p').remove();
		return false;
	});
	$('#cc_change_password').validate({

		rules: {
			new_pass: {
				required: true
			},
			confirm_pass: {
				required: true,
				equalTo: '[name="new_pass"]'

			},
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('div').append(error);
		},

	});
	$('#frm-add-cc').validate({

		rules: {
			cct_firstname: {
				required: true
			},
			cct_lastname: {
				required: true
			},
			crt_position: {
				required: true
			},
			crt_email: {
				required: true,
				email: true
			},
			countrycodedd: {
				required: true
			},
			crt_no: {
				required: true
			},
			crt_func: {
				required: true
			}

		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
			//$('input#home_location').tooltip({'trigger':'focus', 'title': 'This field is required.'});
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			//$('input#home_location').tooltip('destroy');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('div').append(error);
		},

		submitHandler: function(form) {
			$(form).find('button').prop('disabled', true).html('Processing..');

			console.log($(form).serialize());

			$.post(
				base_url+'cc/crisisteam/crt_submit',
				$(form).serialize(),
				function(res){
					console.log(res);

					if(res.result == 'ok'){
						window.location.href = base_url+'cc/crisisteam/managecrt';
					}
					else if(res.result == 'exist'){
						bootbox.alert('<p class="lead">User already exist in current module and organization.</p>');
					}

					else{
						window.location.reload(true);
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}


	});

	$(document).on('keyup', '.input-limit-char', function(){
		var $self = $(this);
		var length = $self.data('length');
		length = parseInt(length);
		length = (isNaN(length)) ? 70 : length;
		var value = $self.val();
		var count = length - $self.val().length;
		if(count < 0){
			value = value.slice(0, length);
			$self.val(value);
			count = 0;
		}
		$self.siblings('span.small').html(count+' characters remaining.');
	});

	//buyNowModal modal
	$('.demo_btn').on('click', function(){
		$('#newPaymentModal').modal('show');
		$('.row-subscription').addClass('hidden');
		$('.row-country').removeClass('hidden');
		activateSubscriptionTab('subscription');
		return false;
	});

	$('#billing_details_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			activateSubscriptionTab('billing_contacts');
		}
	});

	$('#billing_contacts_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			activateSubscriptionTab('payment_method');
		}
	});

	$('input[name=number]').payment('formatCardNumber');
	$('#subscription_payment_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			const $details_form = $('#billing_details_form');
			const subscription_payment = $(form).serializeObject();
			const billing_details = $details_form.serializeObject();
			
			let payment = {...subscription_payment, ...billing_details};
			
			payment.selected_plan = selectedSub;

			let selectedCountry = $details_form.find('[name="country"]').find('option:selected').data('info');

			payment.address_country = selectedCountry.short_name;
			payment.country = selectedCountry.iso2;

			console.log(payment,'payment'); // return false;
		
			var PublishableKey = 'pk_test_2g76COgjgp2DPnHPHdxrRCZc'; // Replace with your API publishable key
			Stripe.setPublishableKey(PublishableKey);
			Stripe.card.createToken(payment, function (status, response) {
				console.log(response, status, 'stripeResponseHandler');
		
				if (response.error) { // Problem!
					bootbox.alert(response.error);
				} else { // Token was created!
					// Get the token ID:
					var token = response.id;
					console.log('token', token);
					response.card.token = token;

					$.post(
						base_url+'apiv2/save_stripe_card',
						response.card,
						function(res){
							
							payment.id = res.id;
							payment.client_ip = response.client_ip;
							payment.created = response.created;
							payment.token = response.id;
							payment.livemode = response.livemode;
							payment.object = response.object;
							payment.type = response.type;
							$.post(
								base_url+'apiv2/save_form_payment',
								payment,
								function(res2){
									
									localStorage.setItem('crisisflo_billing', JSON.stringify(res2.format_billing));
									isUserPlanFree();
									isPlanUpgradable();
									bootbox.alert('You’ve successfully upgraded your subscription', function(){
										window.location.reload(true);
									});

								},
								'json'
							).error((err)=>{
								console.log(err);
							});
				
						},
						'json'
					).error(function(err){
						console.log(err);
					});

				}
			});
		

			//SAVE save_billing_contact
			const $billing_contact = $('#billing_contacts_form');
			const billing_contact = $billing_contact.serializeObject();

			$.post(
				base_url+'apiv2/save_billing_contact',
				billing_contact,
				function(res2){
					console.log(res2, 'save_billing_contact');
				},
				'json'
			).error((err)=>{
				console.log(err);
			});

		}
	});



	$('[data-toggle="popover"]').popover();

	$('.popover-html').popover({
		container: 'body',
		html: true,
		content: function(){
			return $('#media-btns').html();
		}
	});

	$(".popover-template").each(function(i, obj) {
		$(this).popover({
			html: true,
			content: function() {
				var id = $(this).attr('id')
				return $('#popover-content-' + id).html();
			}
		});
	});

	$('.row-country .btn').on('click', function() {
		$('.row-country .btn').removeClass('active');
		$(this).addClass('active');
		$('.row-subscription').removeClass('hidden');
		var country = $(this).data('country');
		$('[name="subscribe_country"]').val(country);
		selectSubscription(selectedSub);
	});

	$('.security_info').popover({
		container: 'body',
		html : true,
        content: function() {
          return $('#security_info_content').html();
        },
		placement: 'top',
		trigger: 'click hover'
	});

	$('#new_incident_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			$('.new_incident_btn').button('loading');
			console.log($(form).serialize());
			$.post(
				base_url+'cc/standbytasks/save_incident',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.href = window.base_url+ 'cc/standbytasks/activetask/'+res.id+'/'+res.template_id;
					$('.new_incident_btn').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
			});

		}
	});

	$('#incident_live_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			$(form).find('button').prop('disabled', true).html('Processing..');
			console.log($(form).serialize());
			$.post(
				base_url+'cc/incidentform/save_incident_live_form',
				{ data: $(form).serializeArray() },
				function(res){
					console.log(res);
					$(form).find('button').html('Success!');
					// setTimeout(function(){
					// 	window.location.href = base_url+'cc/dashboard';
					// }, 1500);

				}

			).error(function(err){
				console.log(err);
			});

		}
	});


	//re
	$('#initiate_recall_form').validate({
		ignore: [],
		rules: {
			radiovalit: {
				required: true
			},
			inci_no: {
				required: true
			},
			inci_date: {
				required: true
			},
			inci_name: {
				required: true
			},
			inform: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			$(form).find('button').prop('disabled', true).html('Processing..');
			console.log($(form).serialize());


			$.post(
				base_url+'cc/recall/manage',
				$(form).serialize(),
				function(res){
					console.log(res);
					$(form).find('button').html('Success!');
					setTimeout(function(){
						window.location.href = base_url+'cc/dashboard';
					}, 1500);



				}

			).error(function(err){
				console.log(err);
			});
		}
	});


	$(document).on('change', '[name="plan"]', function(){
		var $self = $(this);
		var $form = $('#signup');
		var value = $self.val();
		var plan_desc = $self.find(':selected').text();

		$form.find('[name="plan"]').val(value);

		if(value == ''){
			$('.bill_info').addClass('hidden');
		}
		else{
			$('.bill_info').removeClass('hidden');
			var ssplit = value.split('__');
			var price = ssplit[2];
			var gst = ssplit[3];
			var total = ssplit[4];

			$('.the_total').html(total);
			$('.the_gst').html(gst);
			$('.thee_price').html(price);
			$('.plan_desc').html(plan_desc);
		}
	});


	$('[name="country_code"]').on('change', function(){
		var ccode = $(this).find(':selected').attr('data-code');
		$('#addon-shortcode').html('+'+ccode);
	});

	$('[name="cccountry_code"]').on('change', function(){
		var ccode = $(this).find(':selected').attr('data-code');
		$('#ccaddon-shortcode').html('+'+ccode);
	});




	$('.continue_btn').on('click', function(){
		$(this).closest('form').submit();
		return false;
	});


	$('.payment_bck_btn').on('click', function(){
		$('#progressbar').children('.active').removeClass('active').prev().removeClass('done').addClass('active');
		$('.step-details-btn').click();
		$('.step-details-btn').addClass('active').siblings().removeClass('active');
		return false;
	});

	$('.confirm_bck_btn').on('click', function(){
		$('#progressbar').children('.active').removeClass('active').prev().removeClass('done').addClass('active');
		$('.step-payment-btn').click();
		$('.step-payment-btn').addClass('active').siblings().removeClass('active');
		return false;
	});

	$(document).on('focus', '.uinfo_geolocation', function(){
		var $self = $(this);
		$self.geocomplete().bind("geocode:result", function(event, result){
			$(this).siblings('input[name="lat"]').val(result.geometry.location.lat());
			$(this).siblings('input[name="lng"]').val(result.geometry.location.lng());
		});

	});

	$('.user_geolocation').geocomplete({ details: "form#signup" })
	.bind("geocode:result", function(event, result){

		var $form = $('#signup');
		var country_short = $form.find('[name="country_short"]').val();

		console.log(result);
		$.ajax({
			  type: "POST",

			  url: base_url+"frontpage/anewskin/show_country_code",

			  data: { country_short: country_short },

			  dataType: 'json',

			  success: function(data) {
				console.log(data);

				$form.find('#addon-shortcode').html('+'+data.calling_code);
				$form.find('[name="country_code"]').val(data.country_id);
				//$('[name="country_short"]').val(data);

			  },
			  error: function(err){
				  console.log(err);
			  }
		});

	});

	$('.update_auth_btn').on('click', function(){
		$self = $(this);
		if($(this).html() == 'Update'){
			$('.auth_table').fadeOut('slow', function(){
				$('.the_clone_authfields').removeClass('hidden');
				$self.html('Save');
				$self.toggleClass('btn-default btn-primary');
			});
		}
		else{
			var form = '.the_clone_authfields';
			var first_name = $(form).find('[name="first_name"]').val();
			var last_name = $(form).find('[name="last_name"]').val();
			var address = $(form).find('[name="address"]').val();
			var organization = $(form).find('[name="organization"]').val();
			var email = $(form).find('[name="email"]').val();
			var mobile = $(form).find('[name="mobile"]').val();
			var country_code = $(form).find('[name="country_code"]').val();
			var the_code = $(form).find('[name="country_code"]').find(':selected').attr('data-code');

			var $signup = $('#signup');
			$signup.find('[name="first_name"]').first().val(first_name);
			$signup.find('[name="last_name"]').first().val(last_name);
			$signup.find('[name="address"]').first().val(address);
			$signup.find('[name="organization"]').first().val(organization);
			$signup.find('[name="email"]').first().val(email);
			$signup.find('[name="mobile"]').first().val(mobile);
			$signup.find('[name="country_code"]').first().val(country_code);

			$('.coo_name').html(first_name+ ' '+last_name);
			$('.coo_email').html(email);
			$('.coo_mobile').html('+'+the_code+''+mobile);
			$('.coo_bssadd').html(address);
			$('.coo_bssname').html(organization);


			$('.the_clone_authfields').addClass('hidden');
			$('.auth_table').fadeIn('slow', function(){
				$self.html('Update');
				$self.toggleClass('btn-default btn-primary');
			});

		}
		return false;

	});

	$(document).on('hidden.bs.modal', '.modal', function () {
		if($('.modal:visible').length > 0){
			setTimeout(function(){
				$('body').addClass('modal-open');
			},200);
		}

	});
	$('.user_geolocation').on('keyup', function(){
		$(this).siblings('[name="lat"]').val('');
		$(this).siblings('[name="lng"]').val('');
	});

	$('#signup').validate({
		rules: {
			organization: {
				required: true
			},
			address: {
				required: true
			},
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			plan: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			$(element).closest('.form-group').append(error);
		},

		submitHandler: function(form) {
			var lat = $(form).find('.user_geolocation').siblings('input').val();

			if(lat == ''){
				bootbox.alert('Please select Business Address from suggestion dropdown.');
				return false;
			}
			console.log($(form).serialize());
			if($('#details').hasClass('active')){

				var first_name = $(form).find('[name="first_name"]').val();
				var last_name = $(form).find('[name="last_name"]').val();
				var address = $(form).find('[name="address"]').val();
				var organization = $(form).find('[name="organization"]').val();
				var email = $(form).find('[name="email"]').val();
				var mobile = $(form).find('[name="mobile"]').val();
				var country_code = $(form).find('[name="country_code"]').val();
				var the_code = $(form).find('[name="country_code"]').find(':selected').attr('data-code');

				$('.coo_name').html(first_name+ ' '+last_name);
				$('.coo_email').html(email);
				$('.coo_mobile').html('+'+the_code+''+mobile);
				$('.coo_bssadd').html(address);
				$('.coo_bssname').html(organization);

				var the_clone_authfields = $('.fields_to_clone').first().clone();

				$('.the_clone_authfields').html(the_clone_authfields);
				$('.the_clone_authfields').find('[name="country_code"]').val(country_code);
				setTimeout(function(){
				}, 500);

				$('#progressbar').children('.active').removeClass('in active').addClass('done').next().addClass('active');
				$('.step-payment-btn').click();
				$('.step-payment-btn').addClass('active').siblings().removeClass('active');

				$(form).find('.payment-errors').closest('div').addClass('hidden');
				$(form).find('[name="ccemail"]').closest('.form-group').removeClass('has-error');


			}
			else if($('#payment').hasClass('active')){

				//clone plan dropdown
				var clone_selplan = $('#payment').find('[name="plan"]').clone();
				var the_plan = $('#payment').find('[name="plan"]').val();
				$('.clone_selplan').html(clone_selplan);
				$('.clone_selplan').find('[name="plan"]').val(the_plan);

				//created a token already
				if($(form).find('[name="stripeToken"]').length > 0){
					$('.step-confirm-btn').click();
					$('.step-confirm-btn').addClass('active').siblings().removeClass('active');
					$('#progressbar').children('.active').removeClass('active').addClass('done').next().addClass('active');

				}
				else{
					Stripe.card.createToken($(form), stripeSignUpResponseHandler);
				}
			}
			else if($('#confirm').hasClass('active')){
				$(form).find('button').html('Processing..').prop('disabled', true);
				$(form).find('a.btn').addClass('disabled');

				var data_arr = $(form).serializeArray();

				var data = {
					data_arr: data_arr,
					calling_code: $(form).find('[name="country_code"]').find(':selected').data('code')

				};

				data = $(form).serialize() + '&' + $.param(data);
				console.log(data);

				$.post(
					base_url+'frontpage/anewskin/signup_payment',
					data,
					function(res){
						console.log(res);
						if(res.result == 'success'){
							$(form).find('button').html('<i class="fa fa-check-circle"></i> Success').prop('disabled', true);

							$('.modal').modal('hide');

							setTimeout(function(){
								bootbox.alert('<p class="lead"><i class="fa fa-check-circle text-success"></i> '+res.message+'</p>', function(){
									window.location.reload(true);
								});

							}, 1500);
						}
						else{
							$(form).find('button').html('Submit').prop('disabled', false);
							$(form).find('a.btn').removeClass('disabled');
							$(form).find('[name="stripeToken"]').remove();
							bootbox.alert('<p class="lead">Something went wrong. Please try again.</p>');
						}
						//window.location.reload(true);
					},
					'json'
				).error(function(err){
					console.log(err);
				});


			}

		}
	});



	help_page();

});//document.ready


var help_page = function(){
	//check admin help page content
	var kpi_menu_active = $('.kpi_menu_tab.active').length;
	var costmon_menu_active = $('.costmon_menu_tab.active').length;
	var blockers_menu_active = $('.blockers_menu_tab.active').length;

	var helpdata = {
		uri_2: uri_2,
		uri_3: uri_3,
		uri_4: uri_4,
		uri_5: uri_5,
		uri_6: uri_6,
		kpi_menu_active: kpi_menu_active,
		costmon_menu_active: costmon_menu_active,
		blockers_menu_active: blockers_menu_active
	};

	$.post(
		base_url+'cc/auditlog/admin_help_content',
		$.param(helpdata),
		function(res){
			console.log(res);

			if(res != ''){

				//show help btn and add help content
				$('.admin_help_btn').removeClass('hidden');
				$('#adminHelpModal').find('.well').html(res).show().siblings('p').remove();
			}
			else{
				$('.admin_help_btn').addClass('hidden');
			}

		}

	).error(function(err){
		console.log(err);
	});


};


$(document).ready(function(e) {
	if(uri_3 == 'payments'){


		var $form = $('#payment-form');
		$form.on('submit', payWithStripe);

		/* If you're using Stripe for payments */
		function payWithStripe(e) {
			e.preventDefault();

			/* Visual feedback */
			$form.find('[type=submit]').html('Validating <i class="fa fa-spinner fa-pulse"></i>');

			var PublishableKey = 'pk_test_2g76COgjgp2DPnHPHdxrRCZc'; // Replace with your API publishable key
			Stripe.setPublishableKey(PublishableKey);

			/* Create token */
			var expiry = $form.find('[name=cardExpiry]').payment('cardExpiryVal');
			var ccData = {
				number: $form.find('[name=cardNumber]').val().replace(/\s/g,''),
				cvc: $form.find('[name=cardCVC]').val(),
				exp_month: expiry.month,
				exp_year: expiry.year
			};

			Stripe.card.createToken(ccData, function stripeResponseHandler(status, response) {
				if (response.error) {
					/* Visual feedback */
					$form.find('[type=submit]').html('Try again');
					/* Show Stripe errors on the form */
					$form.find('.payment-errors').text(response.error.message);
					$form.find('.payment-errors').closest('.row').show();
				} else {
					/* Visual feedback */
					$form.find('[type=submit]').html('Processing <i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
					/* Hide Stripe errors on the form */
					$form.find('.payment-errors').closest('.row').hide();
					$form.find('.payment-errors').text("");
					// response contains id and card, which contains additional card details
					console.log(response.id);
					console.log(response.card);
					var token = response.id;
					// AJAX - you would send 'token' to your server here.
					$.post(

						base_url+'cc/settings/stripe_card_token',
						{token: token},
						function(res){
							console.log(res);
						},
						'json'
					).error(function(err){
						console.log(err);
					})
					// Assign handlers immediately after making the request,
					.done(function(data, textStatus, jqXHR) {
						$form.find('[type=submit]').html('Payment method added successfully <i class="fa fa-check"></i>').prop('disabled', true);
						setTimeout(function(){
							window.location.reload(true);
						}, 1500);
					})
					.fail(function(jqXHR, textStatus, errorThrown) {
						$form.find('[type=submit]').html('There was a problem').removeClass('success').addClass('error');
						/* Show Stripe errors on the form */
						$form.find('.payment-errors').text('Try refreshing the page and trying again.');
						$form.find('.payment-errors').closest('.row').show();
					});
				}
			});
		}
		/* Fancy restrictive input formatting via jQuery.payment library*/
		$('input[name=cardNumber]').payment('formatCardNumber');
		$('input[name=cardCVC]').payment('formatCardCVC');
		$('input[name=cardExpiry').payment('formatCardExpiry');

		/* Form validation using Stripe client-side validation helpers */
		jQuery.validator.addMethod("cardNumber", function(value, element) {
			return this.optional(element) || Stripe.card.validateCardNumber(value);
		}, "Please specify a valid credit card number.");

		jQuery.validator.addMethod("cardExpiry", function(value, element) {
			/* Parsing month/year uses jQuery.payment library */
			value = $.payment.cardExpiryVal(value);
			return this.optional(element) || Stripe.card.validateExpiry(value.month, value.year);
		}, "Invalid expiration date.");

		jQuery.validator.addMethod("cardCVC", function(value, element) {
			return this.optional(element) || Stripe.card.validateCVC(value);
		}, "Invalid CVC.");

		validator = $form.validate({
			rules: {
				cardNumber: {
					required: true,
					cardNumber: true
				},
				cardExpiry: {
					required: true,
					cardExpiry: true
				},
				cardCVC: {
					required: true,
					cardCVC: true
				}
			},
			highlight: function(element) {
				$(element).closest('.form-control').removeClass('success').addClass('error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-control').removeClass('error').addClass('success');
			},
			errorPlacement: function(error, element) {
				$(element).closest('.form-group').append(error);
			}
		});

		paymentFormReady = function() {
			if ($form.find('[name=cardNumber]').hasClass("success") &&
				$form.find('[name=cardExpiry]').hasClass("success") &&
				$form.find('[name=cardCVC]').val().length > 1) {
				return true;
			} else {
				return false;
			}
		}

		$form.find('[type=submit]').prop('disabled', true);
		var readyInterval = setInterval(function() {
			if (paymentFormReady()) {
				$form.find('[type=submit]').prop('disabled', false);
				clearInterval(readyInterval);
			}
		}, 250);
	}//uri_3 == 'payments'

});


function activateSubscriptionTab(tab){
	$('.list-group-subscription a[href="#' + tab + '"]').addClass('active').tab('show').siblings().removeClass('active');
	const $subsList = $('.life-list');

	$subsList.find('li').removeClass('current complete');
	if(tab == 'subscription'){
		$subsList.find('li.subscription').addClass('current');
	}
	if(tab == 'billing_details'){
		$subsList.find('li.subscription').addClass('complete');
		$subsList.find('li.billing_details').addClass('current');
	}
	if(tab == 'billing_contacts'){
		$subsList.find('li.subscription').addClass('complete');
		$subsList.find('li.billing_details').addClass('complete');
		$subsList.find('li.billing_contacts').addClass('current');
	}
	if(tab == 'payment_method'){
		$subsList.find('li.subscription').addClass('complete');
		$subsList.find('li.billing_details').addClass('complete');
		$subsList.find('li.billing_contacts').addClass('complete');
		$subsList.find('li.payment_method').addClass('current');
	}
};

var selectedSub = '0';
var selectedSubPrice = 0;
var prorated = 0;
function selectSubscription(type){
	selectedSub = type;
	switch (type) {
		case '0':
			selectedSubPrice = 0;
			break;
		case '1':
			selectedSubPrice = 49;
			break;
		case '2':
			selectedSubPrice = 79;
			break;
	}

	var country = $('[name="subscribe_country"]').val();
	var GST = (country === 'au' && selectedSubPrice) ? selectedSubPrice * 0.1 : 0;
	var gstText = (country === 'au' && selectedSubPrice) ? ' +10% GST' : '';

	var date1 = new Date();
	var date2 = new Date(date1.getFullYear(), date1.getMonth()+1, 1);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	var proratedPrice = 0;
	var total = selectedSubPrice + GST;
	if(selectedSubPrice){
		proratedPrice = ((total) / 30) * diffDays;
	}

	console.log(diffDays, total, proratedPrice, 'diffDays, total, proratedPrice');
	$('.monthly_payment_label').html(selectedSubPrice + gstText);	
	$('.prorated_payment_label').html(proratedPrice.toFixed(2));	

	activateSubscriptionTab('payment_method');

}
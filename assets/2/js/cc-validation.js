$(document).ready(function () {
	$(document).on('click', '.delete_user', function () {
		const id = $(this).data('id');
		bootbox.confirm('Are you sure you want to completely delete this user?', function (res) {
			if (res) {
				$.post(
					base_url + 'cc/crisisteam/delete_user', {
						id: id
					},
					function (res) {
						console.log(res);
						window.location.reload(true);
					},
					'json'
				).error(function (respond) {
					console.log(respond);
					bootbox.alert('Something went wrong. Please try again later.');
				});
			}
		});
	});

	//validation for cc admin
	$("#btn_cc_login").click(function () {
		var user_name = $("#user_name");
		var pass_word = $("#pass_word");

		$('#user_name').removeClass('error_border');
		$('#pass_word').removeClass('error_border');

		$('#user_name').addClass('form-control');
		$('#pass_word').addClass('form-control');

		if (user_name.val() == "") {
			user_name.val('');
			$('#user_name').attr('placeholder', 'Mandatory field');
			$('#user_name').removeClass('form-control');
			$('#user_name').addClass('error_border');
			$('#user_name').focus();
			return false;
		} else if (pass_word.val() == "") {
			pass_word.val('');
			$('#pass_word').attr('placeholder', 'Mandatory field');
			$('#pass_word').removeClass('form-control');
			$('#pass_word').addClass('error_border');
			$('#pass_word').focus();
			return false;
		}
	});


	//validation for add & update Stakeholder
	$("#btn_add_stakeholder").click(function () {
		var stk_firstname = $("#stk_firstname");
		var stk_lastname = $("#stk_lastname");
		var stk_position = $("#stk_position");
		var stk_email = $("#stk_email");
		var stk_no = $("#stk_no");
		var countrycode = $("#countrycode");


		var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;


		$('#stk_firstname').siblings('p').remove();
		$('#stk_lastname').siblings('p').remove();
		$('#stk_position').siblings('p').remove();
		$('#stk_email').siblings('p').remove();
		$('#stk_no').siblings('p').remove();
		$('#countrycode').siblings('p').remove();

		if (stk_firstname.val() == "") {
			if (stk_firstname.siblings('p').length == 0) {
				stk_firstname.after('<p class="text-red">Mandatory field</p>');
			}
			stk_firstname.focus();
			return false;
		} else if (stk_lastname.val() == "") {
			if (stk_lastname.siblings('p').length == 0) {
				stk_lastname.after('<p class="text-red">Mandatory field</p>');
			}
			stk_lastname.focus();
			return false;
		} else if (stk_position.val() == "") //organization
		{
			if (stk_position.siblings('p').length == 0) {
				stk_position.after('<p class="text-red">Mandatory field</p>');
			}
			stk_position.focus();
			return false;
		} else if (stk_email.val() == "") {
			if (stk_email.siblings('p').length == 0) {
				stk_email.after('<p class="text-red">Mandatory field</p>');
			}
			stk_email.focus();
			return false;
		} else if (!filter.test(stk_email.val())) {
			if (stk_email.siblings('p').length == 0) {
				stk_email.after('<p class="text-red">Valid email is required</p>');
			}
			stk_email.focus();
			return false;

		} else if ($('#countrycode').val() == "") {
			if (countrycode.siblings('p').length == 0) {
				countrycode.after('<p class="text-red">Mandatory field</p>');
			}
			countrycode.focus();
			return false;
		} else if (stk_no.val() == "") {
			if (stk_no.siblings('p').length == 0) {
				stk_no.after('<p class="text-red">Mandatory field</p>');
			}
			stk_no.focus();
			return false;
		} else if (isNaN(stk_no.val())) {
			if (stk_no.siblings('p').length == 0) {
				stk_no.after('<p class="text-red">Valid number is required</p>');
			}
			stk_no.focus();
			return false;
		}

	});

	//prevent typing in date
	$('#inci_date').keydown(function (e) {
		e.preventDefault();
		return false;
	});


	// validation of verify stk password validation
	$("#verify_stk").click(function () {
		var stk_password = $("#stk_password");
		var confirm_stk_password = $("#confirm_stk_password");

		$('#stk_password').removeClass('error_border');
		$('#confirm_stk_password').removeClass('error_border');

		$('#stk_password').addClass('form-control');
		$('#confirm_stk_password').addClass('form-control');

		if (stk_password.val() == "") {
			stk_password.val('');
			$('#stk_password').attr('placeholder', 'Password is required');
			$('#stk_password').removeClass('form-control');
			$('#stk_password').addClass('error_border');
			$('#stk_password').focus();
			return false;
		}
		if (stk_password.val().length < 6) {
			stk_password.val('');
			$('#stk_password').attr('placeholder', 'Password Must Be Atleast 6 Character');
			$('#stk_password').removeClass('form-control');
			$('#stk_password').addClass('error_border');
			$('#stk_password').focus();
			return false;
		} else if (confirm_stk_password.val() == "") {
			confirm_stk_password.val('');
			$('#confirm_stk_password').attr('placeholder', 'Confirm Password is required.');
			$('#confirm_stk_password').removeClass('form-control');
			$('#confirm_stk_password').addClass('error_border');
			$('#confirm_stk_password').focus();
			return false;
		} else if (stk_password.val() != confirm_stk_password.val()) {
			confirm_stk_password.val('');
			$('#confirm_stk_password').attr('placeholder', 'Password Does Not Match');
			$('#confirm_stk_password').removeClass('form-control');
			$('#confirm_stk_password').addClass('error_border');
			$('#confirm_stk_password').focus();
			return false;
		}
	});

	//validation messaging
	$("#send_msg").click(function () {
		var message_receiver = $("#message_receiver");
		var subject = $("#subject");
		var txt_message = $("#txt_message");

		message_receiver.removeClass('error_border');
		subject.removeClass('error_border');
		txt_message.removeClass('error_border');

		message_receiver.addClass('form-control');
		subject.addClass('form-control');
		txt_message.addClass('form-control');

		if (message_receiver.val() == "") {
			message_receiver.val('');
			message_receiver.removeClass('form-control');
			message_receiver.addClass('error_border');
			message_receiver.focus();
			return false;

		}
		if (subject.val() == "") {
			subject.val('');
			subject.attr('placeholder', 'Mandatory field');
			subject.removeClass('form-control');
			subject.addClass('error_border');
			subject.focus();
			return false;

		}
		/*			if(txt_message.val()=="")
		            { 
					    txt_message.val('');
						txt_message.attr('placeholder','Please Add Message');
						txt_message.addClass('error_border');
						txt_message.focus();
						return false;
					
					}
		*/
		if ($('.note-editable').html() == '') {
			var modal = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
			modal += '<div class="modal-dialog">';
			modal += ' <div class="modal-content">';
			modal += '     <div class="modal-body">';
			modal += '    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
			modal += '       <p class="lead">Your message body is empty. Please add a message.</p>';
			modal += '     </div>';
			modal += '   <div class="modal-footer">';
			modal += '     <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>';
			modal += '   </div>';
			modal += '  </div>';
			modal += '  </div>';
			modal += '</div>';

			$(modal).modal('show');
			return false;
		} else {

			txt_message.val($('.note-editable').html());

			console.log(txt_message.val());
			//return false;

		}



	});


	//validation for chandge password add new cc
	$("#change_cc_pw").click(function () {
		var email = $("#email");
		var new_pass = $("#new_pass");
		var confirm_pass = $("#confirm_pass");

		$('#new_pass').removeClass('error_border');
		$('#confirm_pass').removeClass('error_border');

		$('#new_pass').addClass('form-control');
		$('#confirm_pass').addClass('form-control');

		if ($('.password-verdict').html() != "Medium" && $('.password-verdict').html() != 'Strong' && $('.password-verdict').html() != 'Very Strong') {

			$('.mediumerror').html('For security reasons, your password needs to be ‘Medium’ strength or above.');

			$('#cc_password').focus();
			return false;
		} else {
			$('.mediumerror').html('');
		}


		if (new_pass.val() == "") {
			new_pass.val('');
			$('#new_pass').attr('placeholder', 'New Password is required');
			$('#new_pass').removeClass('form-control');
			$('#new_pass').addClass('error_border');
			$('#new_pass').focus();
			return false;

		} else if (confirm_pass.val() == "") {
			confirm_pass.val('');
			$('#confirm_pass').attr('placeholder', 'Please Confirm Password');
			$('#confirm_pass').removeClass('form-control');
			$('#confirm_pass').addClass('error_border');
			$('#confirm_pass').focus();
			return false;

		}

	});


	//validation for reminder cc
	$("#btn_set_reminder").click(function () {
		var reminder = $("#reminder");

		$('#reminder').removeClass('error_borderl');
		$('#reminder').addClass('form-control');

		if (reminder.val() == "") {
			reminder.val('');
			$('#reminder').attr('placeholder', 'Mandatory field');
			$('#reminder').removeClass('form-control');
			$('#reminder').addClass('error_border');
			$('#reminder').focus();
			return false;

		}
	});

	//validation for initiate response

	$("#btn_initiate_responce").click(function () {
		var initiate_scenario = $("#initiate_scenario");
		var initiate_standbymsg = $("#initiate_standbymsg");
		var newmsg = $("#newmsg");
		var inform = $("inform");

		$('#initiate_add_scen').removeClass('error_borderl');
		$('#initiate_standbymsg').removeClass('error_borderl');
		$('#newmsg').removeClass('error_borderl');
		$('#inform').removeClass('error_borderl');

		$('#initiate_scenario').addClass('form-control');
		$('#initiate_standbymsg').addClass('form-control');
		$('#newmsg').addClass('form-control');
		$('#inform').addClass('form-control');


		if (initiate_repose.radiovalit[0].checked == false && initiate_repose.radiovalit[1].checked == false) {
			document.getElementById('err_it').innerHTML = "Mandatory field";
			return false;
		}

		if (initiate_scenario.val() == "") {
			initiate_scenario.val('');
			$('#initiate_scenario').attr('placeholder', 'Mandatory field');
			$('#initiate_scenario').removeClass('form-control');
			$('#initiate_scenario').addClass('error_border');
			$('#initiate_scenario').focus();
			return false;

		} else if (initiate_standbymsg.val() == "" && initiate_repose.radioval[0].checked == true) {
			initiate_standbymsg.val('');
			$('#initiate_standbymsg').attr('placeholder', 'Add Message');
			//$('#initiate_standbymsg').removeClass('form-control');
			$('#initiate_standbymsg').addClass('error_border');
			$('#initiate_standbymsg').focus();
			return false;
		}
		/* else if(newmsg.val()=="")
		  { 
				newmsg.val('');
				$('#newmsg').attr('placeholder','Enter Message');
				$('#newmsg').removeClass('form-control');
				$('#newmsg').addClass('error_border');
				$('#newmsg').focus();
				return false;
		 }}*/
		else if (initiate_repose.inform[0].checked == false && initiate_repose.inform[1].checked == false) {
			document.getElementById('err_inform').innerHTML = "Mandatory field";
			document.getElementById('err_it').innerHTML = "";
			return false;
		}
	});

	$("#add_stand_by").click(function () {
		var sel_scenario = $("#sel_scenario");
		var stand_by_msg = $("#stand_by_msg");

		if (sel_scenario.val() == "") {
			sel_scenario.val('');
			$('#sel_scenario').attr('placeholder', 'Mandatory field');
			$('#sel_scenario').removeClass('form-control');
			$('#sel_scenario').addClass('error_border');
			$('#sel_scenario').focus();
			return false;
		} else if (stand_by_msg.val() == "") {
			stand_by_msg.val('');
			$('#stand_by_msg').attr('placeholder', 'Mandatory field');
			$('#stand_by_msg').removeClass('form-control');
			$('#stand_by_msg').addClass('error_border');
			$('#stand_by_msg').focus();
			return false;
		}
	});

	//validation for Add Scenario 
	$("#add_scen").click(function () {
		var scenario_desc = $("#scenario_desc");
		if (scenario_desc.val() == "") {
			scenario_desc.val('');
			$('#scenario_desc').attr('placeholder', 'Mandatory field');
			//	$('#scenario_desc').removeClass('form-control');
			$('#scenario_desc').addClass('error_border');
			$('#scenario_desc').focus();
			return false;

		}
	});

	//validation for Upload document
	/*$("#upload_doc").click(function()
	{ 
		var file_upload_name=$("#file_upload_name");
		var file_name=file_upload_name.value;
		var ext=file_name.substring(file_name.lastIndexOf('.')+1);
		if(file_name!="" && ext!='doc' && ext!='DOC' && ext!='docx' && ext!='DOCX' && ext!='pdf' && ext!='PDF' && ext!='txt' && ext!="TXT")
		{ 
			$('err_file_upload_name').fadeIn("slow");
			document.getElementById("err_file_upload_name").innerHTML="Invalid document type";
			$('err_file_upload_name').fadeOut("slow");
			return false;
		}
	});*/


	//upload doc for crt
	$('#upload_docc').on("click", function () {


		var fileName = $("#file_upload_name").val();
		var comment = $(".note-editable");


		if (fileName == "") {
			$('#file_error').removeClass('hidden');
			$('#file_error').addClass('visible');
			$('#file_error').html('Mandatory field.');

			$('#file_upload_name').focus();
			return false;
		}

		if (comment.is(':empty')) {
			$('#comment_error').removeClass('hidden');
			$('#comment_error').addClass('visible');
			$('#comment_error').html('Please add any comment about the changes.');

			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');

			//comment.focus();
			return false;
		}


		if (fileName.lastIndexOf("doc") === fileName.length - 3 || fileName.lastIndexOf("docx") === fileName.length - 4 || fileName.lastIndexOf("pdf") === fileName.length - 3 || fileName.lastIndexOf("txt") === fileName.length - 3 || fileName.lastIndexOf("DOC") === fileName.length - 3 || fileName.lastIndexOf("DOCX") === fileName.length - 4 || fileName.lastIndexOf("PDF") === fileName.length - 3 || fileName.lastIndexOf("TXT") === fileName.length - 3 && group.val() != "") {

			$('#comment').val(comment.html());

			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');
			$('#comment_error').removeClass('visible');
			$('#comment_error').addClass('hidden');


			var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><p class="lead">';


			var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Uploading.. ';

			var myModal3 = '</p><p>Please wait, do not close the window until the upload ends.<br><br>The time required for this operation depends on the file size, the net load and your connection speed</p></div></div></div> ';

			var myModal = myModal1 + myModal2 + myModal3;


			//$(myModal).modal('show');


		} else {
			$('#file_error').removeClass('hidden');
			$('#file_error').addClass('visible');
			$('#file_error').html('Valid File Input is required.');

			$('#comment_error').removeClass('visible');
			$('#comment_error').addClass('hidden');
			$('#file_upload_name').focus();
			return false;
		}




	});


	//upload doc
	$('#upload_doc').on("click", function () {

		// $("input:file").change(function (){
		//   var fileName = $(this).val();

		var group = $("#group");

		var c_pack = $("#c_pack");

		var fileName = $("#file_upload_name").val();

		if (fileName == "") {
			$('#file_error').removeClass('hidden');
			$('#file_error').addClass('visible');
			$('#file_error').html('Mandatory field');

			$('#file_upload_name').focus();
			return false;
		} else if (!$('input[name=c_pack]').is(':checked')) {
			//group.val('');
			$('#cpack_error').removeClass('hidden');
			$('#cpack_error').addClass('visible');
			$('#cpack_error').html('Crisis Pack is required.');

			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');


			//$('#group').focus();
			return false;
		} else if (group.val() == "") {
			group.val('');
			$('#group_error').removeClass('hidden');
			$('#group_error').addClass('visible');
			$('#group_error').html('Mandatory field');

			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');
			$('#cpack_error').removeClass('visible');
			$('#cpack_error').addClass('hidden');


			$('#group').focus();
			return false;
		}


		if (fileName.lastIndexOf("doc") === fileName.length - 3 || fileName.lastIndexOf("docx") === fileName.length - 4 || fileName.lastIndexOf("pdf") === fileName.length - 3 || fileName.lastIndexOf("txt") === fileName.length - 3 || fileName.lastIndexOf("DOC") === fileName.length - 3 || fileName.lastIndexOf("DOCX") === fileName.length - 4 || fileName.lastIndexOf("PDF") === fileName.length - 3 || fileName.lastIndexOf("TXT") === fileName.length - 3 && group.val() != "") {
			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');
			$('#group_error').removeClass('visible');
			$('#group_error').addClass('hidden');


			var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><p class="lead">';


			var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Uploading.. ';

			var myModal3 = '</p><p>Please wait, do not close the window until the upload ends.<br><br>The time required for this operation depends on the file size, the net load and your connection speed</p></div></div></div> ';


			var myModal = myModal1 + myModal2 + myModal3;


		} else {
			$('#file_error').removeClass('hidden');
			$('#file_error').addClass('visible');
			$('#file_error').html('Valid File Input is required.');

			$('#group_error').removeClass('visible');
			$('#group_error').addClass('hidden');
			$('#file_upload_name').focus();
			return false;
		}







		// });


	});

	//add_casedoc doc
	$('#add_casedoc').on("click", function () {

		// $("input:file").change(function (){
		//   var fileName = $(this).val();

		var file_desc = $("#file_desc");

		var fileName = $("#file_upload_name").val();

		if (fileName == "") {
			$('#file_error').removeClass('hidden');
			//$('#file_error').addClass('visible');
			$('#file_error').html('Mandatory field');

			$('#file_upload_name').focus();
			return false;
		} else if (file_desc.val() == "") {
			file_desc.val('');
			$('#desc_error').removeClass('hidden');
			//$('#file_desc').addClass('visible');
			$('#desc_error').html('Mandatory field');

			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');


			$('#file_desc').focus();
			return false;
		}


		if (fileName.lastIndexOf("doc") === fileName.length - 3 || fileName.lastIndexOf("docx") === fileName.length - 4 || fileName.lastIndexOf("pdf") === fileName.length - 3 || fileName.lastIndexOf("txt") === fileName.length - 3 || fileName.lastIndexOf("DOC") === fileName.length - 3 || fileName.lastIndexOf("DOCX") === fileName.length - 4 || fileName.lastIndexOf("PDF") === fileName.length - 3 || fileName.lastIndexOf("TXT") === fileName.length - 3 && group.val() != "") {
			$('#file_error').removeClass('visible');
			$('#file_error').addClass('hidden');
			$('#group_error').removeClass('visible');
			$('#group_error').addClass('hidden');


			var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><p class="lead">';

			var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Uploading.. ';

			var myModal3 = '</p><p>Please wait, do not close the window until the upload ends.<br><br>The time required for this operation depends on the file size, the net load and your connection speed</p></div></div></div> ';

			var myModal = myModal1 + myModal2 + myModal3;


			//$(myModal).modal('show');


		} else {
			$('#file_error').removeClass('hidden');
			$('#file_error').addClass('visible');
			$('#file_error').html('Valid File Input is required.');

			$('#group_error').removeClass('visible');
			$('#group_error').addClass('hidden');
			$('#file_upload_name').focus();
			return false;
		}



	});


	/* fill stand by message on chnage of scenarion on Initiate Response*/
	$("#initiate_scenario").change(function () {
		var scn_id = $("#initiate_scenario").val();
		//$("#initiate_standbymsg > option").remove();
		$.ajax({
			type: 'POST',
			url: base_url + 'cc/responce/fetch_stand_by_message/' + scn_id,
			dataType: 'json',
			success: function (message) {
				var default_opt = $('<option>');
				default_opt.val("");
				default_opt.text("Select");
				$("#initiate_standbymsg").append(default_opt);
				$.each(message, function (msg_id, msg_val) {
					//var opt=$('<option>');
					$("#stnd_by_id").val(msg_id);
					$("#initiate_standbymsg").text(msg_val);
					//$("#initiate_standbymsg").append(opt);
				});
			}
		});
	});


	//validation for change personal info cc
	$("#update_personal_info").click(function () {
		var f_name = $("#f_name");
		var l_name = $("#l_name");
		var ph_no = $("#ph_no");
		var address = $("#address");
		var city = $("#city");
		var state = $("#state");
		var zip = $("#zip");
		var zipcode = /^[0-9a-zA-Z]+$/;

		if (f_name.val() == "") {
			f_name.val('');
			$('#f_name').attr('placeholder', 'Mandatory field');
			$('#f_name').removeClass('form-control');
			$('#f_name').addClass('error_border');
			$('#f_name').focus();
			return false;

		} else if (l_name.val() == "") {
			l_name.val('');
			$('#l_name').attr('placeholder', 'Mandatory field');
			$('#l_name').removeClass('form-control');
			$('#l_name').addClass('error_border');
			$('#l_name').focus();
			return false;

		} else if (ph_no.val() == "") {
			ph_no.val('');
			$('#ph_no').attr('placeholder', 'Mandatory field');
			$('#ph_no').removeClass('form-control');
			$('#ph_no').addClass('error_border');
			$('#ph_no').focus();
			return false;

		} else if (isNaN(ph_no.val())) {
			ph_no.val('');
			$('#ph_no').attr('placeholder', 'Enter Valid Mobile Number');
			$('#ph_no').removeClass('form-control');
			$('#ph_no').addClass('error_border');
			$('#ph_no').focus();
			return false;

		} else if (address.val() == "") {
			address.val('');
			$('#address').attr('placeholder', 'Mandatory field');
			$('#address').removeClass('form-control');
			$('#address').addClass('error_border');
			$('#address').focus();
			return false;

		} else if (city.val() == "") {
			city.val('');
			$('#city').attr('placeholder', 'Mandatory field');
			$('#city').removeClass('form-control');
			$('#city').addClass('error_border');
			$('#city').focus();
			return false;

		} else if (state.val() == "") {
			state.val('');
			$('#state').attr('placeholder', 'Mandatory field');
			$('#state').removeClass('form-control');
			$('#state').addClass('error_border');
			$('#state').focus();
			return false;

		} else if (zip.val() == "") {
			zip.val('');
			$('#zip').attr('placeholder', 'Mandatory field');
			$('#zip').removeClass('form-control');
			$('#zip').addClass('error_border');
			$('#zip').focus();
			return false;

		} else if (!zipcode.test(zip.val())) {
			zip.val('');
			$('#zip').attr('placeholder', 'Alphanumeric value is required');
			$('#zip').removeClass('form-control');
			$('#zip').addClass('error_border');
			$('#zip').focus();
			return false;
		}
	});


	//validation for change personal info crt
	$("#update_personal_info_crt").click(function () {
		var f_name = $("#f_name");
		var l_name = $("#l_name");
		var ph_no = $("#ph_no");
		var position = $("#position");


		if (f_name.val() == "") {
			f_name.val('');
			$('#f_name').attr('placeholder', 'Mandatory field');
			$('#f_name').removeClass('form-control');
			$('#f_name').addClass('error_border');
			$('#f_name').focus();
			return false;

		} else if (l_name.val() == "") {
			l_name.val('');
			$('#l_name').attr('placeholder', 'Mandatory field');
			$('#l_name').removeClass('form-control');
			$('#l_name').addClass('error_border');
			$('#l_name').focus();
			return false;

		} else if (ph_no.val() == "") {
			ph_no.val('');
			$('#ph_no').attr('placeholder', 'Mandatory field');
			$('#ph_no').removeClass('form-control');
			$('#ph_no').addClass('error_border');
			$('#ph_no').focus();
			return false;

		} else if (isNaN(ph_no.val())) {
			ph_no.val('');
			$('#ph_no').attr('placeholder', 'Enter Valid Mobile Number');
			$('#ph_no').removeClass('form-control');
			$('#ph_no').addClass('error_border');
			$('#ph_no').focus();
			return false;

		} else if (position.val() == "") {
			position.val('');
			position.attr('placeholder', 'Mandatory field');
			position.removeClass('form-control');
			position.addClass('error_border');
			position.focus();
			return false;

		}

	});



	/*$("#add_message").click(function()
	{  alert("here");
		var common_message=$("#common_message");
		
		if(common_message.val()=="")
		{ 
			common_message.val('');
			$('#common_message').attr('placeholder','Enter message');
			$('#common_message').removeClass('form-control');
			$('#common_message').addClass('error_border');
			$('#common_message').focus();
			return false;
		}
	});*/

	/* set status is_read=1 from inbox of CRT panel */
	$(".read_message").click(function () {
		var celement = $(this);
		var is_read = parseInt(celement.attr('data-read'));
		var msgid = parseInt(celement.attr('data-msgid'));

		if (is_read == 0) {
			$.post(base_url + 'crt/message/mark_as_read/', {
					msgid: msgid
				},
				function (data) {
					window.location.href = base_url + "crt/message/read/" + msgid + '/inbox';
				});
		} else {
			window.location.href = base_url + "crt/message/read/" + msgid + '/inbox';
		}
	});

	/* set status is_read=1 from inbox of CC panel */
	$(".cc_read_message").click(function () {
		var celement = $(this);
		var is_read = parseInt(celement.attr('data-read'));
		var msgid = parseInt(celement.attr('data-msgid'));

		if (is_read == 0) {
			$.post(base_url + 'cc/message/mark_as_read/', {
					msgid: msgid
				},
				function (data) {
					window.location.href = base_url + "cc/message/read/" + msgid + '/inbox';
				});
		} else {
			window.location.href = base_url + "cc/message/read/" + msgid + '/inbox';
		}
	});


	/* case management cc decision to case report*/
	$("#cc_decision").click(function () {
		var selectdecision = $("#selectdecision");
		var decision_note = $("#decision_note");

		if (selectdecision.val() == "") {
			selectdecision.val('');
			$('#selectdecision').removeClass('form-control');
			$('#selectdecision').addClass('error_border');
			$('#selectdecision').focus();
			return false;

		} else if (decision_note.val() == "") {
			decision_note.val('');
			$('#decision_note').attr('placeholder', 'Enter decision note');
			$('#decision_note').addClass('error_border');

			$('#selectdecision').removeClass('error_border');
			$('#selectdecision').addClass('form-control');
			$('#decision_note').focus();
			return false;

		}
	});



	//toggle btn in upload doc
	$('button#btn1_btn').click(function () {
		$('div.btn1_show').fadeIn();
		$('div.btn2_show').hide();
		$(this).addClass('active');
		$('button#btn2_btn').removeClass('active');

	});

	$('button#btn2_btn').click(function () {
		$('div.btn2_show').fadeIn();
		$('div.btn1_show').hide();
		$(this).addClass('active');
		$('button#btn1_btn').removeClass('active');

	});


	//cpack
	$('#check_rd0').click(function (e) {

		$('input#radios-0').attr('checked', 'checked');
		$(this).addClass('active');
		$('input#radios-1').removeAttr('checked');
		$('#check_rd1').removeClass('active');
	});

	$('#check_rd1').click(function (e) {

		$('input#radios-1').attr('checked', 'checked');
		$(this).addClass('active');
		$('input#radios-0').removeAttr('checked');
		$('#check_rd0').removeClass('active');
	});



	//toggle case management crt
	$('button#show_all').click(function () {
		//   if($('input#show_all').is(':checked')) {
		$('div.show_all_form').fadeIn();
		$('div.filter_crt_form').hide();
		$(this).addClass('active');
		$('button#filter_crt').removeClass('active');
		$('#crt_mem').attr('name', 'crt_mem');
		$('.filter_crt_result').html('');
		//	}
	});

	$('button#filter_crt').click(function () {
		//   if($('input#filter_crt').is(':checked')) {
		$('div.filter_crt_form').fadeIn();
		$('div.show_all_form').hide();
		$(this).addClass('active');
		$('button#show_all').removeClass('active');
		$('#crt_mem').attr('name', 'crt_mem_x');
		//	}
	});




	//toggle case officer 
	$('input#case_off_no').click(function () {
		if ($('input#case_off_no').is(':checked')) {
			$('div.case_officer_holder').slideUp();
		}
	});

	$('input#case_off_no_yes').click(function () {
		if ($('input#case_off_no_yes').is(':checked')) {
			$('div.case_officer_holder').slideDown();
			$('form.share_indi').hide();

			$("#cc_share").click(function () {
				if ($('#city').val() == "") {
					$('#city').val('');
					$('#city').removeClass('form-control');
					$('#city').addClass('error_border');
					$('#city').focus();
					return false;

				}
			});

		}
	});

	//toggle socialize case 
	$('input#c_indi').click(function () {
		if ($('input#c_indi').is(':checked')) {
			$('.share_indi').fadeIn();
			$('.share_group').hide();
		}
	});

	$('input#c_group').click(function () {
		if ($('input#c_group').is(':checked')) {
			$('.share_group').fadeIn();
			$('.share_indi').hide();
		}
	});


	/* case management share to crt*/
	$("#cc_share").click(function () {
		var selectcrt = $("#selectcrt");

		if (selectcrt.val() == "") {
			//selectdecision.val('');
			$('.selectcrt_error').html('Mandatory field');
			$('#selectcrt').focus();
			return false;

		}
	});

	/* case management share to group*/
	$("#group_share").click(function () {
		var selectcrt = $("#selectgroup");

		if (selectcrt.val() == "") {
			//selectdecision.val('');
			$('.selectgroup_error').html('Mandatory field');
			$('#selectgroup').focus();
			return false;

		}
	});


	$("#btn_add_group").click(function () {
		if ($("#group_name").val() == "") {
			if ($("#group_name").siblings('p').length == 0) {
				$("#group_name").after('<p class="text-red">Mandatory field</p>');
			}
			$("#group_name").focus();
			return false;
		} else {
			$("#group_name").siblings().hide();
		}
	});


});

function del_confirm(site_path) {

	bootbox.confirm("<p class='lead'>" + action_messages.standard.delete_crt + "</p>", function (r) {
		if (r) {
			location.href = site_path;
		}
	});
}

function complete_confirm(site_path) {

	bootbox.confirm("<p class='lead'>Confirm task has been completed.</p>", function (r) {
		if (r) {
			location.href = site_path;
		}
	});
}

function contained_confirm(site_path) {

	bootbox.confirm("<p class='lead'>Confirm you wish to close crisis incident.</p>", function (r) {;
		if (r) {
			location.href = site_path;
		}
	});
}

$(function () {

	$('.addScnt').on('click', function () {
		var loc_form = $('#p_scents').find('p').clone();
		$(loc_form).find('.small').removeClass('hidden');
		$(loc_form).find('input').val('');
		$('.aff_loc_clones').append(loc_form);

	});

	$(document).on('click', '.removeInp', function () {
		$(this).closest('p').remove();
		return false;
	});
});

$(document).ready(function (e) {

	//geocomplete
	$('.nm_location').geocomplete().bind("geocode:result", function (event, result) {
		console.log(JSON.stringify(result));
		$(this).siblings('input[name="lat"]').val(result.geometry.location.lat());
		$(this).siblings('input[name="lng"]').val(result.geometry.location.lng()); //result.geometry.location.xB
	});

	//geocomplete
	$('.update_location').geocomplete().bind("geocode:result", function (event, result) {
		console.log(result);
		$(this).siblings('input[name="lat"]').val(result.geometry.location.lat()); //result.geometry.location.k
		$(this).siblings('input[name="lng"]').val(result.geometry.location.lng()); //result.geometry.location.D
	});

	$(".update_location").trigger("geocode");


	//geocomplete
	$(document).on('focus', '.aff_location', function () {
		$(this).geocomplete().bind("geocode:result", function (event, result) {
			$(this).siblings('input[name="lat[]"]').val(result.geometry.location.lat());
			$(this).siblings('input[name="lng[]"]').val(result.geometry.location.lng());
		});
	});

	//geocomplete
	$('.geocityloc').geocomplete().bind("geocode:result", function (event, result) {
		$(this).siblings('input[name="city_lat"]').val(result.geometry.location.lat());
		$(this).siblings('input[name="city_lng"]').val(result.geometry.location.lng());
	});

	//geocomplete
	$('.searchcaseloc').geocomplete().bind("geocode:result", function (event, result) {
		$(this).siblings('input[name="city_lat"]').val(result.geometry.location.lat());
		$(this).siblings('input[name="city_lng"]').val(result.geometry.location.lng());

		$('#filter_btn').click();

	});


	$('.geocomplete').geocomplete({
		details: "form#frm-add-cc, form#frm-update-crt, form#admin_change_password"
	}).bind("geocode:result", function (event, result) {

		var country_short = $('#country_short').val();

		$.ajax({
			type: "POST",

			url: base_url + "cc/crisisteam/show_country_code",

			data: {
				country_short: country_short
			},

			dataType: 'json',

			success: function (data) {
				console.log(data);

				$('#countrycode').val(data.calling_code);

				$('#countrycodedd').val(data.country_id);

			},
			error: function (err) {
				console.log(err);
			}
		});

		//console.log(result);
		//console.log(result.address_components[3].short_name);
	});


});

function getLocalTime(timeZoneId, user_id, the_time) {
	var the_time = moment(the_time); //moment().format('YYYY-MM-D h:mm'); ; 
	var the_loc = the_time.tz(timeZoneId).format('h:mm z');
	console.log(the_loc);
	$('.my_local_td' + user_id).html(the_loc);

}

function teamLocalTime() {
	$('#myModal').find('.table-responsive').hide();
	$('#myModal').find('.modal-body').append('<p class="lead text-center"><i class="fa fa-spinner fa-spin"></i><br>Loading content...</p>');
	$('#myModal').modal('show');

	$('#myModal').find('.modal-body .lead').remove();
	$('#myModal').find('.table-responsive').show();

}


function editNewMarker(id) {
	$('#myMapModal').modal('hide');

	var data = {
		"id": id
	};

	data = $(this).serialize() + "&" + $.param(data);

	jQuery.ajax({
		type: "POST",
		url: base_url + 'cc/recall/get_new_marker',
		dataType: 'json',
		data: data,
		success: function (response) {
			console.log(response);

			if (response.status == 'OK') {
				$('#nm_form').find('#nm_location').val(response.location);
				$('#nm_form').find('#lat').val(response.lat);
				$('#nm_form').find('#lng').val(response.lng);
				$('#nm_form').find('#nm_id').val(response.id);
				$('#nm_form').find('#nm_recall_id').val(response.recall_id);
				$('#nm_form').find('#nm_comment').val(response.comment);
				$('#myAddPinModal').modal('show');
			} else {
				bootbox.alert('Something went wrong. Please try again.');
			}
		}
	});

}

function delNewMarker(id) {
	$('#myMapModal').modal('hide'); //'Delete Marker?'
	bootbox.confirm('<p class="lead">' + action_messages.global.generic_delete_message + '</p>', function (e) {
		if (e) {


			var data = {
				"id": id
			};

			data = $(this).serialize() + "&" + $.param(data);

			jQuery.ajax({
				type: "POST",
				url: base_url + 'cc/recall/delete_marker',
				dataType: 'json',
				data: data,
				success: function (response) {
					console.log(response);

					if (response.status == 'success') {
						$('#myMapModal').modal('show');
						var recall_id = $('form#nm_form').find('#nm_recall_id').val();
						recallMap(recall_id);
					} else {
						bootbox.alert('Something went wrong. Please try again.');
					}

				}
			}); //ajax end

		}
	});
}


function recallMap(id) {

	var mm = [];

	var rnd = Math.random;
	//reference_details
	var data = {
		"id": id
	};

	data = $(this).serialize() + "&" + $.param(data);

	jQuery.ajax({
		type: "POST",
		url: base_url + 'cc/recall/places',
		dataType: 'json',
		data: data,
		success: function (response) {
			console.log(response);
			if (response.status == 'OK') {
				places = response.places;
				var redmark = 0,
					bluemark = 0,
					yellowmark = 0;

				for (p in places) {
					var t_content = '<div id="content" style="min-height: 80px"><b>' + places[p].name + '</b><br>' + places[p].geo_name;

					if (places[p].type == 'nm') {
						t_content = t_content + '<br><button class="btn btn-xs btn-primary for_cc_only" style="margin-top: 5px;" onclick="editNewMarker(\'' + places[p].id + '\');"><i class="fa fa-edit"></i> Edit</button> <button class="btn btn-xs btn-primary for_cc_only" style="margin-top: 5px;" onclick="delNewMarker(\'' + places[p].id + '\');"><i class="fa fa-trash"></i> Delete</button>';
					}

					t_content = t_content + '</div>';
					mm.push({
						lon: places[p].geo[1],
						lat: places[p].geo[0],
						h: places[p].name, //new Date(1E12 + rnd() * 1E11).toString(),
						d: t_content,
						type: places[p].type
					});

					if (places[p].type == 'crts') {
						bluemark++;
					} else if (places[p].type == 'aff') {
						redmark++;
					} else {
						yellowmark++;
					}
				}

			}
			var my_redmark;
			var my_bluemark;
			var my_yellowmark;

			if (redmark > 1) {
				my_redmark = redmark + ' Affected Sites';
			} else {
				my_redmark = redmark + ' Affected Site';
			}

			if (bluemark > 1) {
				my_bluemark = bluemark + ' Response Team Members';
			} else {
				my_bluemark = bluemark + ' Response Team Member';
			}

			if (yellowmark > 1) {
				my_yellowmark = yellowmark + ' New Markers';
			} else {
				my_yellowmark = yellowmark + ' New Marker';
			}

			$('.redmark-count').html(my_redmark);
			$('.bluemark-count').html(my_bluemark);
			$('.yellow-count').html(my_yellowmark);
		}
	});

	window.mapData = mm;



	var gm = google.maps;
	var map = new gm.Map(document.getElementById('map-canvas'), {
		mapTypeId: gm.MapTypeId.ROADMAP,
		center: new gm.LatLng(50, 0),
		zoom: 6, // whatevs: fitBounds will override
		scrollwheel: true
	});
	var iw = new gm.InfoWindow();
	var oms = new OverlappingMarkerSpiderfier(map, {
		markersWontMove: true,
		markersWontHide: true,
		keepSpiderfied: true
	});

	var usualColor = 'eebb22';
	var spiderfiedColor = base_url + 'assets/img/map-marker-green.png'; //'ffee22';
	var iconWithColor = function (color) {
		return color;
		//return 'http://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|+|' + color + '|000000|ffff00';
	}
	var shadow = new gm.MarkerImage(
		'https://www.google.com/intl/en_ALL/mapfiles/shadow50.png',
		new gm.Size(37, 34), // size   - for sprite clipping
		new gm.Point(0, 0), // origin - ditto
		new gm.Point(10, 34) // anchor - where to meet map location
	);

	oms.addListener('click', function (marker) {
		iw.setContent(marker.desc);
		iw.open(map, marker);
	});
	oms.addListener('spiderfy', function (markers) {
		for (var i = 0; i < markers.length; i++) {
			//markers[i].setIcon(iconWithColor(spiderfiedColor));
			markers[i].setShadow(null);
		}
		iw.close();
	});
	oms.addListener('unspiderfy', function (markers) {
		for (var i = 0; i < markers.length; i++) {
			//markers[i].setIcon(iconWithColor(usualColor));
			markers[i].setShadow(shadow);
		}
		iw.close();
	});


	window.setTimeout(function () {
		google.maps.event.trigger(map, 'resize')

		var bounds = new gm.LatLngBounds();
		for (var i = 0; i < window.mapData.length; i++) {
			var datum = window.mapData[i];
			var loc = new gm.LatLng(datum.lat, datum.lon);

			if (datum.type == 'crts') {
				icon_marker = base_url + 'assets/img/map-marker-blue.png';
			} else if (datum.type == 'aff') {
				icon_marker = base_url + 'assets/img/marker-gif.gif';
			} else {
				icon_marker = base_url + 'assets/img/google_purple_icon_sm.png';
			}

			bounds.extend(loc);
			var marker = new gm.Marker({
				position: loc,
				title: datum.h,
				map: map,
				icon: iconWithColor(icon_marker),
				shadow: shadow,
				optimized: false
				//animation: google.maps.Animation.BOUNCE
			});
			marker.desc = datum.d;
			oms.addMarker(marker);
		}
		//map.fitBounds(bounds);

		map.fitBounds(bounds);
		//map.setCenter(new google.maps.LatLng(Lat,Lng));
		//map.setZoom(3);
	}, 999);


	// Limit the zoom level
	google.maps.event.addListener(map, 'zoom_changed', function () {
		if (map.getZoom() < 1) map.setZoom(1);
	});



	// for debugging/exploratory use in console
	window.map = map;
	window.oms = oms;


	$('#myMapModal').modal('show');

}


$(document).ready(function (e) {


	//clock
	var date = new Date(mydatetime); //'2014-01-01 05:02:12 pm'
	var clocks = [];

	//validate up_loc_form
	$('form#up_loc_form').on('submit', function () {
		var location = $(this).find('#up_location').val();
		var lat = $(this).find('input[name=lat]').val();
		var lng = $(this).find('input[name=lng]').val();

		$(this).find('.control-helper').hide();

		if ($(this).find('#up_location').val() == '') {
			$(this).find('.control-helper').show();
			return false;
		} else if (lat == '') {
			bootbox.alert('Please select location from the suggestion dropdown.');
			return false;
		} else {
			$(this).find('button').addClass('disabled');
			$(this).find('button').html('Loading...');


			//$("#initiate_standbymsg > option").remove();
			$.ajax({
				type: 'POST',
				url: base_url + 'cc/recall/update_my_loc',
				data: {
					loc: location,
					lat: lat,
					lng: lng
				},

				success: function (message) {
					window.location.reload(true);
				}
			});

			return false;

		}
	});



	//validate up_loc_form
	$('form#nm_form').on('submit', function () {
		var location = $(this).find('#nm_location').val();
		var comment = $(this).find('#nm_comment').val();
		var nm_id = $(this).find('#nm_id').val();
		var lat = $(this).find('input[name=lat]').val();
		var lng = $(this).find('input[name=lng]').val();
		var recall_id = $(this).find('#nm_recall_id').val();

		$('.control-helper').addClass('hidden');
		if (location == '') {
			$(this).find('#nm_location').siblings('span').html('Mandatory field.');
			$(this).find('#nm_location').siblings('span').removeClass('hidden');
			$(this).find('#nm_location').focus();
			return false;
		} else if (comment == '') {
			$(this).find('#nm_comment').siblings('span').removeClass('hidden');
			$(this).find('#nm_comment').focus();
			return false;
		} else if (lat == '') {
			$(this).find('#nm_location').siblings('span').html('Please select location from suggestion dropdown.');
			$(this).find('#nm_location').siblings('span').removeClass('hidden');
			$(this).find('#nm_location').focus();
			return false;
		} else {
			$(this).find('button').addClass('disabled');
			$(this).find('button').html('Loading...');
			//$("#initiate_standbymsg > option").remove();
			$.ajax({
				type: 'POST',
				url: base_url + 'cc/recall/add_marker',
				data: {
					loc: location,
					comment: comment,
					recall_id: recall_id,
					lat: lat,
					lng: lng,
					nm_id: nm_id
				},

				success: function (message) {
					console.log(message);
					console.log(recall_id);

					if (message == 'success') {
						$('.modal').modal('hide');
						recallMap(recall_id);
						$('#myMapModal').modal('show');
						$('form#nm_form').find('button').removeClass('disabled');
						$('form#nm_form').find('button').html('Save');
					}

				}
			});
			return false;
		}
	});






});


$(document).ready(function () {

	var hidWidth;
	var scrollBarWidths = 40;

	var widthOfList = function () {
		var itemsWidth = 0;
		$('.list li').each(function () {
			var itemWidth = $(this).outerWidth();
			itemsWidth += itemWidth;
		});
		return itemsWidth;
	};

	var widthOfHidden = function () {
		return (($('.wrapper').outerWidth()) - widthOfList() - getLeftPosi()) - scrollBarWidths;
	};

	var getLeftPosi = function () {
		if ($('.list').length) {
			return $('.list').position().left;
		}
	};

	var reAdjust = function () {
		if (($('.wrapper').outerWidth()) < widthOfList()) {

			$('.scroller-right').show();
		} else {
			$('.scroller-right').hide();
		}

		if (getLeftPosi() < 0) {
			$('.scroller-left').show();
		} else {
			$('.item').animate({
				left: "-=" + getLeftPosi() + "px"
			}, 'slow');
			$('.scroller-left').hide();
		}
	}

	reAdjust();

	$(window).on('resize', function (e) {
		reAdjust();
	});

	$('.scroller-right').click(function () {

		$('.scroller-left').fadeIn('slow');
		$('.scroller-right').fadeOut('slow');

		$('.list').animate({
			left: "+=" + widthOfHidden() + "px"
		}, 'slow', function () {

		});
	});

	$('.scroller-left').click(function () {

		$('.scroller-right').fadeIn('slow');
		$('.scroller-left').fadeOut('slow');

		$('.list').animate({
			left: "-=" + getLeftPosi() + "px"
		}, 'slow', function () {

		});
	});

});


$(document).ready(function (e) {
	$('input[name="tobegen[]"]').on('click', function () {

		if ($('input[name="tobegen[]"]:checked').length == 0) {
			$('#genform').find('button').addClass('disabled');
		} else {
			$('#genform').find('button').removeClass('disabled');
		}

	});

	$('#genform').on('submit', function () {
		$('.modal').modal('hide');
	});
});

var getHistory = function (id) {

	$('.report-history').html('<p class="lead text-center"><i class="fa fa-spinner fa-spin"></i></p>');

	$.ajax({
		type: "POST",

		url: base_url + "cc/reporting/gethistory",

		data: {
			id: id
		},

		success: function (data) {
			console.log(data);
			$('.report-history').html(data);
		}
	});


};

$(document).ready(function (e) {

	if ($('[name="stkcheck[]"]').length == 0) {
		$('#importstk-form').find('.form-group').html('<p class="text-center text-muted">No stakeholder found.</p>');
	}

	$('[name="stkcheck[]"]').on('click', function () {
		var checkbox = $('[name="stkcheck[]"]:checked');
		if (checkbox.length == 0) {
			$('#importstk-form').find('button').addClass('disabled');
		} else {
			$('#importstk-form').find('button').removeClass('disabled');
		}

	});
});


//plain recall clock 


function display_c(start) {
	window.start = parseFloat(start);
	var end = 0 // change this to stop the counter at a higher value
	var refresh = 1000; // Refresh rate in milli seconds
	if (window.start >= end) {
		mytime = setTimeout('display_ct()', refresh)
	} else {
		//alert("Time Over ");
	}
}

function display_ct() {
	// Calculate the number of days left
	var days = Math.floor(window.start / 86400);
	// After deducting the days calculate the number of hours left
	var hours = Math.floor((window.start - (days * 86400)) / 3600)
	// After days and hours , how many minutes are left
	var minutes = Math.floor((window.start - (days * 86400) - (hours * 3600)) / 60)
	// Finally how many seconds left after removing days, hours and minutes.
	var secs = Math.floor((window.start - (days * 86400) - (hours * 3600) - (minutes * 60)))

	var x = days + " D: " + hours + " H: " + minutes + " M: " + secs + " S"; //window.start + 

	$('.mobile_clock').html(x);
	//document.getElementById('ct').innerHTML = x;
	window.start = window.start + 1;

	tt = display_c(window.start);
}

$(document).ready(function (e) {
	display_c(recall_time);
});



/*******************user time************************/
function displayTime(mydatetime) {
	var time_m = moment(mydatetime);
	//var new_time = time_m.add(1, 'seconds').format('hh:mm:ss A'); //'YYYY-M-D hh:mm:ss a');
	var time = time_m.format('hh:mm:ss A');
	$('.user-clock').html(time);
	setTimeout(displayTime, 1000);
}

$(document).ready(function () {
	displayTime(mydatetime);
	//setInterval(displayTime(mydatetime), 1000);
});

function reloadForUpdate() {
	$('#refreshPageModal').find('button.btn-primary').addClass('disabled').html('Loading..');
	window.location.reload(true);
}


/************************bulk notification**********************************/

$("#myCsvForm").ajaxForm({
	beforeSend: function () {
		$(".progress-csv").show();
		//clear everything
		$(".bar-csv").width('0%');
		$(".percent-csv").html("0%");
	},
	uploadProgress: function (event, position, total, percentComplete) {
		$(".bar-csv").width(percentComplete + '%');
		$(".percent-csv").html(percentComplete + '%');
		$(".upload_file_btn").addClass('disabled');

	},
	success: function () {
		$(".bar-csv").width('100%');
		$(".percent-csv").html('100%');
		$(".progress-csv").hide();
	},
	complete: function (response) {
		console.log(response.responseText);
		$(".upload_file_btn").removeClass('disabled');
		if (isNaN(response.responseText)) {
			if (response.responseText == 'not_valid_file') {
				$(".progress-csv").hide();
				$('.bulk_error_mess').removeClass('hidden').html('<p class="">The file selected is invalid.</p>');
			} else if (response.responseText == 'not_readable') {
				$(".progress-csv").hide();
				$('.bulk_error_mess').removeClass('hidden').html('<p class="">File is empty or not readable.</p>');
			} else {
				$('.bulk_error_mess').addClass('hidden');

				//$("#file_up_text").show();
				//$("#message").after(response.responseText);
				$('.bulk_error_mess').removeClass('hidden').html('<p class="">Please update your file.<br>' + response.responseText + '</p>');

				//$('.bulk_error_mess').html('<p class="">Succcess! You can now send a request email to your friends.</p>');
			}


		} else {

			window.location.href = base_url + 'cc/communication/manage/' + response.responseText;
		}


	},
	error: function (response) {
		$('.bulk_error_mess').removeClass('hidden').html('<p class="lead"> Unable to upload file. Please refresh and try again.</p>');
		console.log(response.responseText);
	}

});
//end ajax file upload


/************************bulk attached files**********************************/

$("#bulkattachedform").ajaxForm({
	beforeSend: function () {
		$(".progress-csv").show();
		//clear everything
		$(".bar-csv").width('0%');
		$(".percent-csv").html("0%");
	},
	uploadProgress: function (event, position, total, percentComplete) {
		$(".bar-csv").width(percentComplete + '%');
		$(".percent-csv").html(percentComplete + '%');
		//$(".upload_file_btn").addClass('disabled');

	},
	success: function () {
		$(".bar-csv").width('100%');
		$(".percent-csv").html('100%');
		$(".progress-csv").hide();
	},
	complete: function (response) {
		console.log(response.responseText);
		$('.bulk_error_mess').removeClass('hidden');

		$('#the_attaches').after(response.responseText);
	},
	error: function (response) {
		$('.bulk_error_mess').removeClass('hidden').html('<p class="text-danger"> Unable to upload file. Please refresh and try again.</p>');
		console.log(response.responseText);
	}

});
//end ajax file upload bulk attached


var confirmDelBulk = function (bulk_id) {

	bootbox.confirm('<p class="lead">' + action_messages.global.generic_delete_message + '</p>', function (e) {
		if (e) {
			window.location.href = base_url + 'cc/communication/delete_bulk/' + bulk_id;
		}
	});
};
var confirmDelKPI = function (id, page) {

	bootbox.confirm('<p class="lead">' + action_messages.global.generic_delete_message + '</p>', function (e) {
		if (e) {
			window.location.href = base_url + 'cc/kpi/delete/' + id + '/' + page;
		}
	});
};


var confirmDelContact = function (contact_id, bulk_id) {

	bootbox.confirm('<p class="lead">' + action_messages.global.generic_delete_message + '</p>', function (e) {
		if (e) {
			window.location.href = base_url + 'cc/communication/delete_contact/' + contact_id + '/' + bulk_id;
		}
	});
};


$(document).ready(function (e) {
	//check form inputs
	$('#company_control, #compcon_disposed, #compcon_transformed').on('keyup', function () {
		if ($('#company_control').val() != '' && $('#compcon_disposed').val() != '' && $('#compcon_transformed').val() != '') {
			$('#kpicontrol').find('a.btn').removeClass('disabled');
			$('#kpis_form').find('button[type="submit"]').removeAttr('disabled');
		} else {
			$('#kpicontrol').find('a.btn').addClass('disabled');
			$('#kpis_form').find('button[type="submit"]').attr('disabled', 'disabled');
		}

	});

	$('#distribution, #dist_disposed, #dist_transformed').on('keyup', function () {
		if ($('#distribution').val() != '' && $('#dist_disposed').val() != '' && $('#dist_transformed').val() != '') {
			$('#kpidist').find('a.btn[href="#kpiconsume"]').removeClass('disabled');
			$('#kpis_form').find('button[type="submit"]').removeAttr('disabled');
		} else {
			$('#kpidist').find('a.btn[href="#kpiconsume"]').addClass('disabled');
			$('#kpis_form').find('button[type="submit"]').attr('disabled', 'disabled');
		}

	});


	$('#kpis_form, #chart_form, tr.tr_main').find('input').keyup(function (event) {

		// skip for arrow keys
		if (event.which >= 37 && event.which <= 40) {
			event.preventDefault();
		}

		$(this).val(function (index, value) {
			return value
				.replace(/\D/g, "")
				//.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
				.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
		});
	});

	//kpipanel
	$('a.empty_kpi, a.cancel_kpi_btn').on('click', function () {
		//hide other fields

		if ($(this).closest('tr.tr_main').find('input.kpi_thresh').is(':visible')) {
			$(this).closest('tr.tr_main').find('input.kpi_thresh, button.thresh_btn, a.cancel_thresh_btn').addClass('hidden');
			$('a.empty_thresh').removeClass('hidden');
		}

		//show thresh field
		$(this).closest('tr.tr_main').find('input.kpi_input, button.submit_kpis, a.cancel_kpi_btn').toggleClass('hidden');
		$(this).closest('tr.tr_main').find('a.empty_kpi').toggleClass('hidden');

		return false;
	});


	$('a.empty_thresh, a.cancel_thresh_btn').on('click', function () {
		//hide other fields
		if ($(this).closest('tr.tr_main').find('input.kpi_input').is(':visible')) {
			$(this).closest('tr.tr_main').find('input.kpi_input, button.submit_kpis, a.cancel_kpi_btn').addClass('hidden');
			$(this).closest('tr.tr_main').find('a.empty_kpi').removeClass('hidden');
		}

		//show thresh field
		$(this).closest('tr.tr_main').find('input.kpi_thresh, button.thresh_btn, a.cancel_thresh_btn, a.empty_thresh').toggleClass('hidden');

		return false;
	});



	//kpi stat
	$('a.kpi_stat_btn').on('click', function () {
		$self = $(this);
		var text = '';
		var title = $(this).attr('data-original-title');
		var kpitype = $(this).attr('data-kpitype');
		var kpiid = $(this).attr('data-kpiid');

		if (title == 'enabled') {
			text = '<p class="lead">' + action_messages.global.generic_disable_message + '</p>';
		} else {
			text = '<p class="lead">' + action_messages.global.generic_enable_message + '</p>';
		}
		console.log($(this).attr('data-original-title'));
		bootbox.confirm(text, function (e) {
			if (e) {
				$.post(
					base_url + 'cc/kpi/updatestatus',
					'title=' + title + '&kpitype=' + kpitype + '&kpiid=' + kpiid,
					function (respond) {
						console.log(JSON.parse(respond));
						var jsondata = JSON.parse(respond);
						$self.attr('data-original-title', jsondata.title);
						$self.removeClass('btn-success btn-danger');
						$self.addClass(jsondata.class);
						$self.find('i').removeClass('fa-check fa-times');
						$self.find('i').addClass(jsondata.text);
					}
				).error(function (respond) {
					bootbox.alert('Something went wrong. Please try again later.');
				});
			}
		});
		return false;
	});


	//toggle enable disable
	$('a.enable_toggle_btn').on('click', function () {
		$self = $(this);
		var text = '';
		var title = $(this).attr('data-original-title');
		var db = $(this).attr('data-db');
		var id = $(this).attr('data-id');

		if (title == 'enabled') {
			text = '<p class="lead">' + action_messages.global.generic_disable_message + '</p>';
		} else {
			text = '<p class="lead">' + action_messages.global.generic_enable_message + '</p>';
		}
		console.log('title=' + title + '&db=' + db + '&id=' + id);
		bootbox.confirm(text, function (e) {
			if (e) {


				$.post(
					base_url + 'cc/communication/updatestatus',
					'title=' + title + '&db=' + db + '&id=' + id,
					function (respond) {
						console.log(JSON.parse(respond));
						var jsondata = JSON.parse(respond);
						$self.attr('data-original-title', jsondata.title);
						$self.removeClass('btn-success btn-danger');
						$self.addClass(jsondata.class);
						$self.find('i').removeClass('fa-check fa-times');
						$self.find('i').addClass(jsondata.text);

						if (jsondata.title == 'enabled') {
							$self.closest('tr').find('input[type="checkbox"]').removeAttr("disabled");
						} else {
							$self.closest('tr').find('input[type="checkbox"]').attr("disabled", "disabled");
						}
					}
				).error(function (respond) {
					bootbox.alert('Something went wrong. Please try again later.');
				});
			}
		});
		return false;
	});



	$('a.empty_kpi:empty, span.empty_kpi:empty').html("No data");

	//kpipanel
	$('a.empty_inbound').on('click', function () {
		$('#chartDataModal').modal('show');
		return false;
	});


	//validate pie chart data
	$('#chart_form').find('input').on('keyup', function () {
		var contact_cntr = $('#contact_cntr').val();
		var kpi_email = $('#kpi_email').val();
		var kpi_website = $('#kpi_website').val();
		var kpi_trading = $('#kpi_trading').val();
		var kpi_id = $(this).find('.kpi_id').val();

		if (contact_cntr == '' || kpi_email == '' || kpi_website == '' || kpi_trading == '') {
			$('#chart_form').find('button').attr('disabled', 'disabled');
		} else {
			$('#chart_form').find('button').removeAttr('disabled');
		}
	});

	//pie chart data form
	$('#chart_form').on('submit', function () {

		$.ajax({
			type: "POST",
			url: base_url + 'cc/kpi/addupdatepie',
			dataType: 'json',
			data: $(this).serialize(),
			success: function (response) {
				console.log(response);
				window.location.reload(true);

			}
		});
		return false;
	});


	$('.kpirecontable').hover(function () {
		$(this).find('a[data-toggle="modal"]').show();
	}, function () {
		$(this).find('a[data-toggle="modal"]').hide();
	});
	$('.customcontactstable').hover(function () {
		$(this).find('a[data-toggle="modal"]').show();
	}, function () {
		$(this).find('a[data-toggle="modal"]').hide();
	});


	//save kpi	
	$('tr.tr_main').find('input[type="text"]').on('keyup', function () {
		$self = $(this);
		$self.closest('tr.tr_main').find('button.btn-primary').removeClass('disabled');
	});



	//save kpi data
	$('.submit_kpi_changes').on('click', function () {
		var val = [];
		var field_name = [];
		$(this).closest('.tr_main').find('input').each(function (index, obj) {
			val[index] = $(this).val();
			field_name[index] = $(this).attr('name');
		});
		console.log(JSON.stringify(val));
		console.log(JSON.stringify(field_name));

		$.post(
			base_url + 'cc/kpi/savefields',
			'val=' + JSON.stringify(val) + '&field=' + JSON.stringify(field_name),
			function (respond) {

				console.log(respond);

				if (respond.type == 'greater') {
					bootbox.alert('<p class="lead">' + respond.message + '</p>');
				} else {
					window.location.reload(true);
				}

			},
			'json'

		).error(function (respond) {
			console.log(respond);
			bootbox.alert('Something went wrong. Please try again later.');
		});
	});



	//toggle select all
	$('.sel_all_kpio').on('click', function () {
		$self = $(this);
		if ($(this).is(":checked")) {
			$self.closest('table').find('input[name="kpio[]"]').prop("checked", this.checked);

		} else {
			$self.closest('table').find('input[name="kpio[]"]').prop("checked", '');
		}

	});

	$('input[name="kpio[]"], #sel_all_kpio').on('click', function () {
		$self = $(this);
		if ($self.closest('table').find('input[name="kpio[]"]:checked').length == 0) {
			$('.send_owner_email_btn').addClass('disabled').html('Send Email');
		} else {
			$('.send_owner_email_btn').removeClass('disabled').html('Send Email');
		}
	});


	//confirm send email to owner
	$('.send_owner_email_btn').on('click', function () {
		$self = $(this);
		bootbox.confirm('<p class="lead">' + action_messages.recall.kpi_sendowner + '</p>', function (e) {
			if (e) {
				$self.addClass('disabled').html('Sending..');
				var ids = 1;
				var items = $('input[name="kpio[]"]:checked').length;
				$('input[name="kpio[]"]:checked').each(function (index, element) {

					var id = $(element).val();

					$.post(
						base_url + 'cc/kpi/sendowner',
						'id=' + id,
						function (respond) {
							console.log(respond);

							if (ids == items) {
								$self.html('Sent');
								window.location.reload(true);
							}
							//$(element).closest('tr').find('td.last_sent').html(respond.last_sent);
							ids++;
						},
						"json"
					).error(function () {
						bootbox.alert('Something went wrong. Please try again later.');
					});

				});

			}

		});
	});

	//owner toggle view
	$('.dist_toggle_btn').on('click', function () {
		$('.dist_kpi_owners').slideToggle('fast', function () {
			if ($(this).is(':visible')) {
				$(this).css('padding', '0px');
			} else {
				$(this).css('padding', '');
			}

		});
		$(this).find('span').toggleClass('fa-caret-down fa-caret-up');
		return false;
	});

	$('.market_toggle_btn').on('click', function () {
		$('.market_kpi_owners').slideToggle('fast', function () {
			if ($(this).is(':visible')) {
				$(this).css('padding', '0px');
			} else {
				$(this).css('padding', '');
			}

		});
		$(this).find('span').toggleClass('fa-caret-down fa-caret-up');
		return false;
	});


	//validate owner form
	$('#owner_form').find('input[type="text"]').on('keyup', function () {
		var len = [];
		$('#owner_form').find('input[type="text"]').each(function (index, obj) {
			if ($(this).val() == '') {
				len[index] = 'aw';
			}
		});
		console.log(len.length);
		if (len.length > 0) {
			$('#owner_form').find('button').attr('disabled', 'disabled');
		} else {
			$('#owner_form').find('button').removeAttr('disabled');
		}
	});

	$('#owner_form').find('input[type="radio"]').on('click', function () {
		var len = [];
		$('#owner_form').find('input[type="text"]').each(function (index, obj) {
			if ($(this).val() == '') {
				len[index] = 'aw';
			}
		});
		if (len.length > 0) {
			$('#owner_form').find('button').attr('disabled', 'disabled');
		} else {
			$('#owner_form').find('button').removeAttr('disabled');
		}

	});

	$('#owner_form').on('submit', function () {
		$(this).find('span.form-helper').hide();
		var email = $('#owner_email').val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!regex.test(email)) {
			$('#owner_email').siblings('span').show().html('Valid email is required.');
			return false;
		} else {
			$.post(
				base_url + 'cc/kpi/addup_owner',
				$(this).serialize(),
				function (response) {
					console.log(response);
					$('.modal').modal('hide');
					//bootbox.alert('<p class="lead">'+response.message+'</p>', function(){
					//});
					window.location.reload(true);
				},
				"json"

			).error(function () {
				bootbox.alert('Something went wrong. Please try again later.');
			});

		}

		return false;
	});

	//delete function 
	$('.delete_btn').on('click', function () {
		$self = $(this);
		var id = $(this).attr('data-id');
		var db = $(this).attr('data-db');
		var mess = '';
		mess = 'Confirm item to be delete.';

		bootbox.confirm('<p class="lead">' + action_messages.global.generic_delete_message + '</p>', function (e) {
			if (e) {
				$.post(
					base_url + 'cc/kpi/delete_data',
					'id=' + id + '&db=' + db,
					function (respond) {
						console.log(respond);
						$self.closest('tr').addClass('bg-danger').slideUp('fast', function () {
							$(this).remove();
						});
					},
					"json"
				).error(function () {
					bootbox.alert('Something went wrong. Please try again later.');
				});

			}
		});


	});


	//delete_kpidata_btn
	$('.delete_kpidata_btn').on('click', function () {
		$self = $(this);
		var id = $(this).attr('data-id');
		var type = $(this).attr('data-type');
		var deltype = $(this).attr('data-deltype');

		var mess = '';

		if (deltype == 'datacontact') {
			mess = 'Confirm contact and data to be delete.';
		} else {
			mess = 'Confirm data to be delete.';
		}

		bootbox.confirm('<p class="lead">' + mess + '</p>', function (e) {
			if (e) {
				$.post(
					base_url + 'cc/kpi/delete_kpidata',
					'id=' + id + '&type=' + type + '&deltype=' + deltype,
					function (respond) {
						console.log(respond);
						window.location.reload(true);
					},
					"json"
				).error(function (respond) {
					console.log(respond);
					bootbox.alert('Something went wrong. Please try again later.');
				});

			}
		});

	});




});

//task guidance modal
var openTaskGModal = function (stand_id) {
	var $themodal = $('#taskGuidanceModal');
	$themodal.find('input.standbytask_id').val(stand_id);
	$themodal.find('p.loader').removeClass('hidden');
	$themodal.find('form').addClass('hidden');
	$themodal.find('button').attr('disabled', 'disabled');

	$.post(
		base_url + 'cc/standbytasks/getguidance', {
			stand_id: stand_id
		},
		function (response) {
			console.log(response);
			$themodal.find('textarea').val(response[0].guidance);

		},
		'json'
	);

	$themodal.find('p.loader').addClass('hidden');
	$themodal.find('form').removeClass('hidden');
	$themodal.modal('show');

};

$(document).ready(function (e) {
	var $themodal = $('#taskGuidanceModal');

	$themodal.find('textarea').on('keyup', function () {
		$themodal.find('button').removeAttr('disabled');
	});


	$themodal.find('form').on('submit', function () {
		console.log($(this).serialize());

		$.post(
			base_url + 'cc/standbytasks/updateguidance',
			$(this).serialize(),
			function (response) {
				console.log(response);
				$('.modal').modal('hide');
				//window.location.reload(true);
			},
			'json'

		);
		return false;


	});

});




//approvers modal
var getApprovers = function (stand_id) {
	$('#assign_approver_form').find('input.standbytask_id').val(stand_id);
	$('#assignApproverModal').find('p.loader').removeClass('hidden');
	$('#assignApproverModal').find('form').addClass('hidden');

	$.post(
		base_url + 'cc/standbytasks/getapprovers', {
			stand_id: stand_id
		},
		function (response) {
			console.log(response);
			$('#assign_approver_form').find('input[type="checkbox"]').prop('checked', false);
			$('#assign_approver_form').find('input.approvers_id').val(response.approvers_id);

			$('#assignApproverModal').find('p.loader').addClass('hidden');
			$('#assignApproverModal').find('form').removeClass('hidden');
			$('#assignApproverModal').modal('show');
		},
		'json'
	);

};

$(document).ready(function (e) {

	// Keep a single clone of the original
	var clonedField = $('.clone_this_group').clone(),
		clones_holder = $('.clones_holder');

	// Add in the delete <a>
	$('<a>', {
		text: ' Remove',
		class: 'remove-cloned',
		href: '#'
	}).appendTo(clonedField);

	$(document).on('click', '.remove-cloned', function () {
		$(this).closest('.clone_this_group').remove();
		$('.clones_holder').find('.clone_this_group').each(function (index, element) {
			$(element).find('label span').html(index + 2);
		});
		return false;
	});

	$('.clone_approver_btn').on('click', function () {
		var clonedcount = $('.clone_this_group').length + 1;
		clonedField.find('label span').html(clonedcount);
		clones_holder.append(clonedField.clone());
		return false;
		return false;

	});

	$('#assign_approver_form').find('select').on('change', function () {
		$('#assign_approver_form').find('button').removeAttr('disabled');
	});

	$('#assign_approver_form').on('submit', function () {
		console.log($(this).serialize());

		$.post(
			base_url + 'cc/standbytasks/assignapprover',
			$(this).serialize(),
			function (response) {
				console.log(response);
				window.location.reload(true);
			},
			'json'

		);
		return false;
	});
});


//mark task approved
$(document).ready(function (e) {
	$('.update_approvers_btn').on('click', function () {
		$('#assignApproverModal').find('div.approver_list').addClass('hidden');
		$('#assignApproverModal').find('form').removeClass('hidden');
	});

	$('.crt_view_approvers_btn').on('click', function () {
		var stand_id = $(this).attr('data-id');
		var assigned = $(this).attr('data-assigned');
		$('#assign_approver_form_crt').find('input.standbytask_id').val(stand_id);
		$('#assignApproverModal').find('p.loader').removeClass('hidden');
		$('#assignApproverModal').find('div.approver_list').addClass('hidden');
		$('#assignApproverModal').find('form').addClass('hidden');

		//empty list of approvers
		$('#assignApproverModal').find('div.approver_list ol').html('');

		if (assigned == 'yes') {
			$('div.approver_list').find('button').removeClass('hidden');
		} else {
			$('div.approver_list').find('button').addClass('hidden');
		}
		$('#assignApproverModal').modal('show');

		$.post(
			base_url + 'cc/recall/getapprovers', {
				stand_id: stand_id
			},
			function (response) {
				console.log(response);

				if (response.approvers.length == 0) {
					$('#assignApproverModal').find('div.approver_list h4').html('No Approver Assigned.');
				} else {
					$('#assignApproverModal').find('div.approver_list h4').html('Assigned Approver(s)');
					for (var i = 0; i < response.approvers.length; i++) {
						console.log(response.approvers[i]);
						$('#assignApproverModal').find('div.approver_list ol').append('<li>' + response.approvers[i].name + ' ' + response.approvers[i].status + '</li>');
					}
				}


				$('#assignApproverModal').find('p.loader').addClass('hidden');
				$('#assignApproverModal').find('div.approver_list').removeClass('hidden');
			},
			'json'
		);


	});


	$('.crt_approve_btn').on('click', function () {
		var myself = $(this);
		var task_id = myself.attr('data-id');
		var self = $('button[data-id="' + task_id + '"]');

		bootbox.confirm('<p class="lead">' + action_messages.recall.approve_crt + '</p>', function (e) {
			if (e) {
				console.log('wer');
				$.post(
					base_url + 'cc/recall/markapproved', {
						task_id: task_id
					},
					function (data) {
						console.log(data);
						console.log(data.status_text);

						self.remove();
						window.location.reload(true);

						console.log(self.closest('tr').length);

					},
					'json'
				).error(function (err) {
					console.log(err);
				});
			}
		});

	});

});



$(document).ready(function (e) {


	$('#assign_approver_form_crt').find('select').on('change', function () {
		$('#assign_approver_form_crt').find('button').removeAttr('disabled');
	});

	$('#assign_approver_form_crt').on('submit', function () {
		console.log($(this).serialize());

		$.post(
			base_url + 'cc/recall/assignapprover',
			$(this).serialize(),
			function (response) {
				console.log(response);
				window.location.reload(true);
			},
			'json'

		);
		return false;
	});

});

$(document).ready(function (e) {
	$('.simulation_btn').on('click', function () {
		var recall_id = $(this).attr('data-id');
		var step_no = $(this).attr('data-step');
		var simu_id = $(this).attr('data-sid');

		var curr_step = parseInt(step_no) - 1;
		$('#viewSimulationModal').find('.step_simu_text').html('Step ' + curr_step + ' Simulation');
		$('#viewSimulationModal').find('p.lead.text-center').show().siblings().hide();
		$('#accordion').html('');
		$.post(
			base_url + 'webmanager/organization/simulationlist', {
				id: simu_id,
				step_no: step_no
			},
			function (response) {
				console.log(response);
				if (response.simulations.length > 0) {
					$(response.simulations).each(function (index, element) {
						var the_content = '<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading' + index + '"><h4 class="panel-title">' + response.simulations[index].name + '</h4></div><div id="collapse' + index + '" class="panel-collapse collapse in" role="tabpanel"><div class="panel-body">' + response.simulations[index].content;

						var simulation_id = response.simulations[index].id;
						$(response.simulations_media).each(function (index, element) {
							var media_id = response.simulations_media[index].simulation_id;
							var media_ext = response.simulations_media[index].ext;

							if (media_id == simulation_id) {

								if (media_ext == 'mp4' || media_ext == 'wmv' || media_ext == 'mov' || media_ext == 'avi') {
									the_content += '<p>' + response.simulations_media[index].name + '<br><video style="width: 100% !important; height: auto !important;" controls><source src="' + response.simulations_media[index].file_url + '" type="video/mp4">Your browser does not support the video tag.</video></p>';


								} else if (media_ext == 'mp3' || media_ext == 'wav') {
									the_content += '<p>' + response.simulations_media[index].name + '<br><audio controls><source src="' + response.simulations_media[index].file_url + '" type="audio/mpeg">Your browser does not support the audio element.</audio></p>';

								}

							}
						});

						the_content += '</div></div></div>';

						$('#accordion').append(the_content);
					});
				} else {
					$('#accordion').append('No simulation found.');
				}
				$('#viewSimulationModal').find('p.lead.text-center').hide().siblings().show();
			},
			'json'
		);

		$('#viewSimulationModal').modal('show');



	});
});


//case manager filter
$(document).ready(function (e) {


	$('#filter_btn').on('click', function (e) {

		$('.filter_crt_result').html('<p class="text-center" style="margin-top: 50px"><i class="fa fa-refresh fa-spin fa-2x text-muted"></i></p>');

		var city_lat = $('input[name="city_lat"]').val();
		var city_lng = $('input[name="city_lng"]').val();

		$.ajax({
			type: "POST",

			url: base_url + 'cc/managecase/search_crt',

			data: {
				location: $('#location').val(),
				city_lat: city_lat,
				city_lng: city_lng,
			},
			dataType: "json",
			success: function (data) {
				console.log(data);
				$('.filter_crt_result').html(data.text);
			},
			error: function (err) {
				console.log(err);
			}

		});


	});

	$('.noEnterSubmit').on('change', function () {
		//$('.search_latit .well').hide().html('');
		$('input[name="city_lat"]').val('');
		$('input[name="city_lng"]').val('');
		$('#filter_btn').click();
	});

	$('.noEnterSubmit').keypress(function (e) {
		if (e.which == 13) return false;
	});

});




//crt availability

$(document).ready(function () {

	//identify click vs drag
	var clickdrag = 0;
	var $element = $('#calendar');
	$element.on('mousedown', function () {
		clickdrag = 0;
		console.log("click");
	});

	$element.on('mousemove', function () {
		clickdrag = 1;
		console.log("drag");
	});

	$element.on('mouseup', function () {
		if (clickdrag === 0) {
			console.log("click");
		} else if (clickdrag === 1) {
			console.log("drag");
		}
	});


	/* initialize the external events
	-----------------------------------------------------------------*/

	$('#external-events .fc-event').each(function () {

		// store data so the calendar knows to render an event upon drop
		$(this).data('event', {
			title: $.trim($(this).text()), // use the element's text as the event title
			id: $(this).attr('data-id'), // use the element's text as the event title
			the_id: $(this).attr('data-id'), // use the element's text as the event title
			stick: true // maintain when user navigates (see docs on the renderEvent method)
		});

		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex: 999,
			revert: true, // will cause the event to go back to its
			revertDuration: 0 //  original position after the drag
		});

	});



	/* initialize the calendar
	-----------------------------------------------------------------*/

	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaDay'
		},
		//defaultDate: '2015-02-12',
		editable: true,
		selectable: true,
		selectHelper: true,
		select: function (start, end, jsEvent, view) {


			var data = {
				"start_time": start.format(),
				"end_time": end.format()
			};

			console.log(jsEvent);


			if (view.name == 'month') {

				if (clickdrag === 1) { //drag

					$.post(
						base_url + 'crt/availability/add_time_schedule',
						$.param(data),
						function (res) {
							console.log(res);

							eventData = {
								title: 'Available',
								start: start,
								end: end,
								the_id: res.id,
								id: res.id
							};

							$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

						},
						'json'
					).error(function (err) {
						console.log(err);
					});


				} //if clickdrag		

			} //if month
			else {


				$.post(
					base_url + 'crt/availability/add_time_schedule',
					$.param(data),
					function (res) {
						console.log(res);

						eventData = {
							title: 'Available',
							start: start,
							end: end,
							the_id: res.id,
							id: res.id
						};

						$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

					},
					'json'
				).error(function (err) {
					console.log(err);
				});


			}


			$('#calendar').fullCalendar('unselect');

		},

		droppable: true, // this allows things to be dropped onto the calendar
		drop: function (date, jsEvent, ui) {
			var new_id = parseInt($(this).attr('data-id')) + 1;
			var start_time = date.format();
			var end_time = '';
			//increment id
			$(this).attr('data-id', new_id);
			console.log(date);


			var data = {
				"start_time": start_time,
				"end_time": end_time
			};

			console.log($.param(data));

			$.post(
				base_url + 'crt/availability/add_time_schedule',
				$.param(data),
				function (res) {
					console.log(res);
				},
				'json'
			).error(function (err) {
				console.log(err);
			});


		},

		eventResize: function (event, delta, revertFunc) {
			var newstart_date = event.start.format();
			var newend_date = '';
			var sched_id = event.the_id;

			if (event.end != null) {
				var newend_date = event.end.format();
			}

			var data = {
				"sched_id": sched_id,
				"newstart_date": newstart_date,
				"newend_date": newend_date
			};

			console.log(newend_date);

			$.post(
				base_url + 'crt/availability/update_schedule',
				$.param(data),
				function (res) {
					console.log(res);
					//window.location.reload(true);
				},
				'json'
			).error(function (err) {
				console.log(err);
			});

			//hide popover on eventResize
			$(this).popover('hide');

		},

		eventDrop: function (event, delta, revertFunc) {
			var newstart_date = event.start.format();
			var newend_date = '';
			var sched_id = event.the_id;

			if (event.end != null) {
				var newend_date = event.end.format();
			}

			var data = {
				"sched_id": sched_id,
				"newstart_date": newstart_date,
				"newend_date": newend_date
			};

			console.log(event);


			$.post(
				base_url + 'crt/availability/update_schedule',
				$.param(data),
				function (res) {
					console.log(res);
					//window.location.reload(true);
				},
				'json'
			).error(function (err) {
				console.log(err);
			});

		},

		dayClick: function (date, jsEvent, view) {

			console.log(view.name);

			if (view.name == 'month') {
				if (clickdrag !== 1) { //drag
					$('#calendar').fullCalendar('gotoDate', date);
					$('#calendar').fullCalendar('changeView', 'agendaDay');
				}
			}


		},

		eventMouseover: function (calEvent, jsEvent, view) {
			console.log(calEvent);

			var the_date = moment(calEvent.start, moment.ISO_8601);
			the_date = moment(the_date).format('Do of MMM, YYYY hh:mm A');

			var the_title = calEvent.title; // 'Available';// 


			var the_id = calEvent.id;

			var the_text = '<h4>';
			the_text += '<i class="fa fa-circle text-green"></i> ' + the_title;
			//the_text += ' <button class="btn btn-default btn-xs delete_sched_btn" data-toggle="tooltip" title="Delete" data-id="'+the_id+'"><i class="fa fa-trash-o"></i></button>';

			the_text += '</h4>';
			the_text += '<p>';
			the_text += 'Start: ' + the_date;
			if (calEvent.end !== null) {
				the_text += '<br>';
				var the_enddate = moment(calEvent.end, moment.ISO_8601);
				the_enddate = moment(the_enddate).format('Do of MMM, YYYY hh:mm A');
				the_text += 'End: ' + the_enddate;
			}
			the_text += '</p>';


			if (view.name == 'month') {

				$(this).popover({
					container: 'body',
					html: true,
					trigger: 'hover',
					content: the_text,
					placement: 'bottom'

				}).on("show.bs.popover", function (e) {
					// hide all other popovers
					$("[rel=popover]").not(e.target).popover("destroy");
					$(".popover").remove();
				});

				$(this).popover('show');



			}


		},

		eventClick: function (calEvent, jsEvent, view) {
			console.log(calEvent);
			var the_date = moment(calEvent.start, moment.ISO_8601);
			the_date = moment(the_date).format('Do of MMM, YYYY hh:mm A');

			var the_title = calEvent.title;
			var the_id = calEvent.id;


			var the_text = action_messages.global.generic_delete_message; //'<p class="lead">Delete this schedule?';
			the_text += '</p>';

			bootbox.confirm(the_text, function (e) {
				if (e) {

					$.post(
						base_url + 'crt/availability/delete', {
							id: the_id
						},
						function (res) {
							console.log(res);
							$('#calendar').fullCalendar('removeEvents', [the_id]);
						}
					).error(function (err) {
						console.log(err);
					});

				}
			});

		},

		eventLimit: true, // allow "more" link when too many events

		events: {
			url: base_url + 'crt/availability/get_events',
			error: function (ee) {
				$('#script-warning').show();
				console.log(ee);
			},
			success: function (res) {
				if (res.length == 0) {
					$('.no-sched-alert').removeClass('hidden');
				}
				console.log(res.length);
			}
		},

		loading: function (bool) {
			$('#loading').toggle(bool);
		}
	});

});


$(document).ready(function (e) {

	$('input[name="availability_btn"]').on('change', function () {
		var $self = $(this);
		var the_val = $(this).val();
		$(this).closest('label').addClass('btn-success');
		$(this).closest('label').siblings().removeClass('btn-success').addClass('btn-default');

		$.post(
			base_url + 'crt/availability/update_availability', {
				the_val: the_val
			},
			function (response) {
				console.log(response);
				$self.closest('.form-group').find('.fa').removeClass('hidden');

				setTimeout(function () {
					$self.closest('.form-group').find('.fa').addClass('hidden');
				}, 2000);
			}

		);

		console.log($(this).val());
	});


	//set week and day
	var the_week = moment().format('e');
	$('[name="monthly_every_day"], [name="yearly_day"]').val(moment().format('DD'));
	$('[name="yearly_on_month"], [name="yearly_onthe_month"]').val(moment().format('M'));
	$('[name="days_in_week[]"]').each(function (index, element) {
		if ($(this).val() == the_week) {
			$(this).prop('checked', true);
			console.log('week d' + the_week);
		}
	});


	$('input[name="recur_radios"]').on('change', function () {
		var recur_settings = $(this).val();
		$('.' + recur_settings + '_settings').removeClass('hidden').siblings().addClass('hidden');
	});

	$('.timepicker').datetimepicker({
		format: 'LT'
	});


	$('input[name="start_time"]').on('dp.change', function () {
		var duration = $('[name="duration"]').val();
		var start_time = $(this).val();
		var new_time = moment('2013-02-08 ' + start_time).add(duration, 'minutes').format('h:mm A');

		console.log(new_time);
		$('input[name="end_time"]').val(new_time);
	});

	$('select[name="duration"]').on('change', function () {
		var start_time = $('[name="start_time"]').val();
		var duration = $(this).val();
		var nowdate = moment().format('YYYY-MM-DD');
		var new_time = moment(nowdate + ' ' + start_time).add(duration, 'minutes').format('h:mm A');

		console.log(new_time);
		$('input[name="end_time"]').val(new_time);

	});

	$('input[name="end_time"]').on('dp.change', function () {
		$('select[name="duration"]').val('0');
	});

	var current_year = moment().format('YYYY');
	$('input[name="start_date"]').datetimepicker({
		format: 'ddd M/D/YYYY',
		maxDate: moment(current_year + '-12-31'),
		minDate: moment(current_year + '-01-01')
	});

	$('input[name="end_date"]').datetimepicker({
		format: 'ddd M/D/YYYY',
		maxDate: moment(current_year + '-12-31')
	});



	$('input[name="start_date"]').on('dp.change', function (e) {
		$(this).closest('form').find('button').removeAttr('disabled');
		$('input[name="end_date"]').data("DateTimePicker").minDate(e.date);
	});


	$('#scheduler_form').find('input[type="number"]').on('keyup', function (event) {

		// skip for arrow keys
		if (event.which >= 37 && event.which <= 40) {
			event.preventDefault();
		}

		$(this).val(function (index, value) {
			return value
				.replace(/\D/g, "")
			//.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
			//.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
			;
		});

	});



	$('#dayscheduler_form').on('submit', function () {
		var $self = $(this);
		$self.find('button.submit_form_btn').addClass('disabled').html('Saving schedule..');
		var nowdate = moment().format('YYYY-MM-DD');
		var start_time = $('input[name="daystart_time"]').val();
		if (start_time != '') {
			start_time = moment(nowdate + ' ' + start_time).format('HH:mm');
		}

		var end_time = $('input[name="dayend_time"]').val();
		if (end_time != '') {
			end_time = moment(nowdate + ' ' + end_time).format('HH:mm');
		}

		var data = {
			"start_time_format": start_time,
			"end_time_format": end_time
		};

		console.log($self.serialize() + "&" + $.param(data));

		$.post(
			base_url + 'crt/availability/add_time_schedule',
			$self.serialize() + "&" + $.param(data),
			function (res) {
				console.log(res);
				window.location.reload(true);
			},
			'json'
		).error(function (err) {
			console.log(err);
		});

		return false;
	});


	$('#scheduler_form').on('submit', function () {
		var $self = $(this);
		$self.find('button.submit_form_btn').addClass('disabled').html('Saving schedule..');
		var nowdate = moment().format('YYYY-MM-DD');

		var start_time = $('input[name="start_time"]').val();
		if (start_time != '') {
			start_time = moment(nowdate + ' ' + start_time).format('HH:mm');
		}

		var end_time = $('input[name="end_time"]').val();
		if (end_time != '') {
			end_time = moment(nowdate + ' ' + end_time).format('HH:mm');
		}

		var start_date = $('input[name="start_date"]').val();
		start_date = moment(start_date).format('YYYY-MM-DD');

		var end_date = $('input[name="end_date"]').val();
		if (end_date != '') {
			end_date = moment(end_date).format('YYYY-MM-DD');
		}

		var data = {
			"start_time_format": start_time,
			"end_time_format": end_time,
			"start_date_format": start_date,
			"end_date_format": end_date
		};

		console.log($self.serialize() + "&" + $.param(data));

		$.post(
			base_url + 'crt/availability/add_schedule',
			$self.serialize() + "&" + $.param(data),
			function (res) {
				console.log(res);
				window.location.reload(true);
			},
			'json'
		).error(function (err) {
			console.log(err);
			bootbox.alert('The recurrence pattern is not valid.');
		});
		return false;
	});

	$('.add_recur_btn').on('click', function () {
		var the_date = moment().format('ddd M/D/YYYY');
		console.log(the_date);
		$('input[name="end_date"]').data("DateTimePicker").minDate(the_date);

		$('input[name="start_date"], input[name="end_date"]').val(the_date);

		$('#mySchedModal').modal('show');
	});

	$(document).on('click', '.delete_sched_btn', function () {
		var id = $(this).attr('data-id');
		$(this).addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i>');

		$.post(
			base_url + 'crt/availability/delete', {
				id: id
			},
			function (res) {
				console.log(res);
				window.location.reload(true);
			}
		).error(function (err) {
			console.log(err);
		});

	});


	$('#cc_calendar').fullCalendar({
		header: {
			left: '', //'prev,next today',
			center: 'prev title next',
			right: 'month,agendaDay'
		},
		//defaultDate: '2015-02-12',
		editable: false,

		eventLimit: true, // allow "more" link when too many events

		eventMouseover: function (calEvent, jsEvent, view) {
			console.log(calEvent);
			var the_date = moment(calEvent.start, moment.ISO_8601);
			the_date = moment(the_date).format('Do of MMM, YYYY hh:mm A');

			var the_title = calEvent.title;
			var the_id = calEvent.id;

			var the_text = '<h4>';
			the_text += '<i class="fa fa-circle text-green"></i> ' + the_title;
			//the_text += ' <button class="btn btn-default btn-xs delete_sched_btn" data-toggle="tooltip" title="Delete" data-id="'+the_id+'"><i class="fa fa-trash-o"></i></button>';

			the_text += '</h4>';
			the_text += '<p>';
			the_text += 'Start: ' + the_date;
			if (calEvent.end !== null) {
				the_text += '<br>';
				var the_enddate = moment(calEvent.end, moment.ISO_8601);
				the_enddate = moment(the_enddate).format('Do of MMM, YYYY hh:mm A');
				the_text += 'End: ' + the_enddate;
			}
			the_text += '</p>';


			$(this).popover({
				container: 'body',
				html: true,
				trigger: 'hover',
				content: the_text,
				placement: 'bottom'
			});
			$(this).popover('show');
		},
		loading: function (bool) {
			$('#loading').toggle(bool);
		}
	});

	$(document).on('click', '.view_calendar_btn', function () {
		var login_id = $(this).attr('data-id');
		var name = $(this).attr('data-name');

		$('#calendarViewModal').find('.modal-title').html(name + ' Schedules');
		$('#calendarViewModal').find('.calendar_crt_name').html(name);

		$('#calendarViewModal').modal('show');

		getCrtSched(login_id);

		return false;
	});




});


var getCrtSched = function (login_id) {
	console.log(login_id);
	$('#cc_calendar').fullCalendar('removeEvents');

	$.ajax({
		url: base_url + 'cc/managecase/get_events/' + login_id,
		dataType: 'json',
		success: function (res) {
			if (res.length == 0) {
				$('.no-sched-alert').removeClass('hidden');
			} else {
				$('.no-sched-alert').addClass('hidden');
			}
			console.log(res);
			$('#cc_calendar').fullCalendar('addEventSource', res);
			$('#cc_calendar').fullCalendar('refetchEvents');
			$('#cc_calendar').fullCalendar('gotoDate', moment());
			$('.fc-agendaDay-button').click();
			$('.fc-month-button').click();

		}
	});

	$('#calendarViewModal').on('shown.bs.modal', function () {
		$("#cc_calendar").fullCalendar('render');
	});

};




//add issue
$(document).ready(function (e) {
	$('.add_issue_btn').on('click', function () {
		var task_id = $(this).data('id');
		var $issue_form = $('#issue_form');

		if (task_id != '') {
			$issue_form.find('[name="task_id"]').val(task_id);
			$issue_form.find('[name="step_no"]').closest('div').addClass('hidden');
			$issue_form.find('[name="step_task"]').closest('div').addClass('hidden');
			$issue_form.find('[name="step_no"]').removeClass('form-control');
			$issue_form.find('[name="step_task"]').removeClass('form-control');
		} else {
			$issue_form.find('[name="step_no"]').closest('div').removeClass('hidden');
			$issue_form.find('[name="step_task"]').closest('div').addClass('hidden');
			$issue_form.find('[name="step_no"]').addClass('form-control');
			$issue_form.find('[name="step_task"]').addClass('form-control');
		}
		$issue_form.find('.btn-primary').attr('disabled', 'disabled');
		$issue_form.find('.form-control').val('');
		$('#issueModal').modal('show');
	});

	$('#issue_form').find('.form-control').on('keyup change', function () {
		var count = 0;

		$('#issue_form').find('.form-control').each(function (index, element) {
			if ($(this).val() == '') {
				count++;
			}
		});
		console.log(count);
		if (count > 1 || $('#issue_form').find('[name="task_id"]').val() == '') {
			$('#issue_form').find('.btn-primary').attr('disabled', 'disabled');
		} else {
			$('#issue_form').find('.btn-primary').removeAttr('disabled');
		}
	});


	$('#issue_form').find('[name="step_task"]').on('change', function () {
		var task_id = $(this).val();
		$('#issue_form').find('[name="task_id"]').val(task_id);
		//$(this).change();
	});

	//select step no
	$('#issue_form').find('[name="step_no"]').on('change', function () {
		var $self = $(this);
		var $issue_form = $('#issue_form');
		if ($(this).val() != '') {
			$issue_form.find('[name="step_task"]').closest('div').find('i').show();
			$.get(
				base_url + 'cc/recall/getsteptasks', {
					step_no: $self.val(),
					recall_id: $issue_form.find('[name="recall_id"]').val(),
					incident_type: $issue_form.find('[name="incident_type"]').val()
				},
				function (res) {
					console.log(res);
					var text = '<option value="">Select</option>';
					if (res.length > 0) {
						$(res).each(function (index, element) {
							text += '<option value="' + res[index].id + '">' + res[index].question + '</option>';
						});
					}
					console.log(text);
					$issue_form.find('[name="step_task"]').html(text);
					$issue_form.find('[name="step_task"]').closest('div').removeClass('hidden');
					$issue_form.find('[name="step_task"]').closest('div').find('i').hide();
				},
				'json'
			).error(function (err) {
				console.log(err);
			});
		} else {
			$issue_form.find('[name="step_task"]').closest('div').addClass('hidden');
		}
	});

	$('#issue_form').on('submit', function () {
		var $self = $(this);
		$self.find('.btn-primary').addClass('disabled').html('Saving..');
		console.log($self.serialize());
		$.post(
			base_url + 'cc/recall/issueboard',
			$self.serialize(),
			function (res) {
				console.log(res);
				$self.find('.btn-primary').addClass('disabled').html('Saved!');

				if ($self.find('[name="step_task"]').closest('div').hasClass('hidden')) {
					setTimeout(function () {
						if (res.task_status == '0') {
							$('#raise_btn' + res.task_id).closest('tr').find('.view_issue_btn').show();
						}

						$('#raise_btn' + res.task_id).closest('tr').find('.view_issue_btn i').attr('data-original-title', res.issue_count);
						$('.modal').modal('hide');
						$self.find('.btn-primary').removeClass('disabled').attr('disabled', 'disabled').html('Submit');
					}, 1500);

				} else {
					window.location.reload(true);
				}
			},
			'json'
		).error(function (err) {
			console.log(err);
		});
		return false;
	});

	$('.view_issue_btn').on('click', function () {
		var task_id = $(this).data('id');

		$('#viewIssueModal').find('.modal-body').html('<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i></p>');

		$('#viewIssueModal').modal('show');
		$.get(
			base_url + 'cc/recall/get_issues', {
				task_id: task_id
			},
			function (res) {
				console.log(res);

				if (res.length > 0) {
					var text = '<ul class="list-group">';
					$(res).each(function (index, element) {
						text += '<li class="list-group-item">';
						text += '<h4>' + res[index].blocker + '<br>';
						text += '<small>Notes: ' + res[index].note + '<br>';
						text += 'Issue: ' + res[index].issue + '<br>';
						text += 'Impact: ' + res[index].impact + '<br>';
						text += 'Raised by: ' + res[index].crt_name + '<br>';
						text += 'Assigned: ' + res[index].assigned_name + '<br>';
						text += 'Status: ' + res[index].status_name + '<br>';

						text += '</small></h4>';
						text += '<p class="small">' + res[index].date_created_format + '</p>';
						text += '</li>';
					});
					text += '</ul>';
				}

				$('#viewIssueModal').find('.modal-body').html(text);
			},
			'json'
		).error(function (err) {
			console.log(err);
		});

		return false;
	});

}); //document.ready





function openResolveBlocker(recall_id, blocker_id) {
	$('#the_recall_id').val(recall_id);
	$('#the_blocker_id').val(blocker_id);
};


function loadCCBlockerNote(id) {
	$('.block_load_holder').show();
	$('.blocker_note_holder').hide();

	$('.blocker_note_holder').load(base_url + "cc/recall/load_blocker_note/" + id, function () {

		$('.block_load_holder').hide();
		$('.blocker_note_holder').show();

	});
};

function loadBlockerNote(id) {
	$('.block_load_holder').show();
	$('.blocker_note_holder').hide();

	$('.blocker_note_holder').load(base_url + "cc/recall/load_blocker_note/" + id, function () {

		$('.block_load_holder').hide();
		$('.blocker_note_holder').show();

	});
};


$(document).ready(function (e) {

	$('#save_blocker_note').click(function (e) {

		$('.blocker_note_error').html('');
		var blocker_id = $('#the_blocker_id').val();
		if ($('#blocker_note').val() == '') {
			$('.blocker_note_error').html('Mandatory field.');
			$('#blocker_note').focus();
			return false;
		} else {

			$.ajax({
				type: "POST",

				url: base_url + 'cc/recall/addnote_blocker',

				data: {
					blocker_note: $('#blocker_note').val(),
					the_blocker_id: blocker_id
				},

				success: function (data) {
					console.log(data);

					$('#resolveBlocker').modal('hide');
					$('#add_note_blck' + blocker_id).closest('tr').find('.blocker_note_btn').show();
					$('#add_note_blck' + blocker_id).closest('tr').find('.blocker_note_empty').hide();

				}
			});


		}
	});

	//get user location
	if (my_location == '') {


		// Try HTML5 geolocation
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				var lat = position.coords.latitude;
				var long = position.coords.longitude;
				//bootbox.alert(lat+'<br>'+long);
				console.log(position);

				$.post(
					'https://maps.google.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=false', {},
					function (res) {
						console.log(res);
						if (res.results.length > 0) {

							$('#up_location').val(res.results[0].formatted_address);
							$('#up_loc_form').find('input[name="lat"]').val(lat);
							$('#up_loc_form').find('input[name="lng"]').val(long);

							if (!getCookie('check_loc')) {
								$('#myLocationModal').modal('show');
								console.log(res.results[0].formatted_address);
							}

							setCookie('check_loc', 'not_set', 1);

						}
					},
					'json'
				);

			}, function () {
				//alert('aw');
			});
		} else {
			//alert('no');
		}


	}



}); //document.ready
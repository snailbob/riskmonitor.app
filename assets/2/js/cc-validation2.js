$(document).ready(function(){
	//validation for cc admin
 	$("#btn_cc_login").click(function()
		{
			var user_name=$("#user_name");
			var pass_word=$("#pass_word");
			
			$('#user_name').removeClass('error_border');
			$('#pass_word').removeClass('error_border');
			
			$('#user_name').addClass('form-control');
			$('#pass_word').addClass('form-control');
			
			if(user_name.val()=="")
			{
				user_name.val('');
				$('#user_name').attr('placeholder','Enter User Name');
				$('#user_name').removeClass('form-control');
				$('#user_name').addClass('error_border');
				$('#user_name').focus();
				return false;
			}
			 else if(pass_word.val()=="")
             {
			    pass_word.val('');
				$('#pass_word').attr('placeholder','Enter Password');
				$('#pass_word').removeClass('form-control');
				$('#pass_word').addClass('error_border');
				$('#pass_word').focus();
				return false;
		     }			
		});
	
	//validation for add & update CRT 
		$("#btn_add_crt_member").click(function()
		{
			var cct_firstname=$("#cct_firstname");
			var cct_lastname=$("#cct_lastname");
			var crt_position=$("#crt_position");
			var crt_email=$("#crt_email");
			var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
			var crt_no=$("#crt_no");
			
			$('#cct_firstname').removeClass('error_border');
			$('#cct_lastname').removeClass('error_border');
			$('#crt_position').removeClass('error_border');
			$('#crt_email').removeClass('error_border');
			$('#crt_no').removeClass('error_border');
			
			$('#cct_firstname').addClass('form-control');
			$('#cct_lastname').addClass('form-control');
			$('#crt_position').addClass('form-control');
			$('#crt_email').addClass('form-control');
			$('#crt_no').addClass('form-control');
			
			if(cct_firstname.val()=="")
            {
			    cct_firstname.val('');
				$('#cct_firstname').attr('placeholder','Enter First  name');
				$('#cct_firstname').removeClass('form-control');
				$('#cct_firstname').addClass('error_border');
				$('#cct_firstname').focus();
				return false;
			 }	
			 		
			else if(cct_lastname.val()=="")
            {
			    cct_lastname.val('');
				$('#cct_lastname').attr('placeholder','Enter Last name');
				$('#cct_lastname').removeClass('form-control');
				$('#cct_lastname').addClass('error_border');
				$('#cct_lastname').focus();
				return false;
			 }			
			
			else	if(crt_position.val()=="")
            {
			    crt_position.val('');
				$('#crt_position').attr('placeholder','Enter Position name');
				$('#crt_position').removeClass('form-control');
				$('#crt_position').addClass('error_border');
				$('#crt_position').focus();
				return false;
			 }			
			else	if(crt_email.val()=="")
            {
			    crt_email.val('');
				$('#crt_email').attr('placeholder','Enter Email Id');
				$('#crt_email').removeClass('form-control');
				$('#crt_email').addClass('error_border');
				$('#crt_email').focus();
				return false;
			 }			
				else if(!filter.test(crt_email.val()))
			{
				  crt_email.val('');
				$('#crt_email').attr('placeholder','Enter valid Email Id');
				$('#crt_email').removeClass('form-control');
				$('#crt_email').addClass('error_border');
				$('#crt_email').focus();
				return false;
			}
			else	if(crt_no.val()=="")
            {
			    crt_no.val('');
				$('#crt_no').attr('placeholder','Enter Contact No');
				$('#crt_no').removeClass('form-control');
				$('#crt_no').addClass('error_border');
				$('#crt_no').focus();
				return false;
			 }			
			else	if(isNaN(crt_no.val()))
            {
			    crt_no.val('');
				$('#crt_no').attr('placeholder','Enter Number');
				$('#crt_no').removeClass('form-control');
				$('#crt_no').addClass('error_border');
				$('#crt_no').focus();
				return false;
			 }			
			
		});
	     //validation for add & update Stakeholder
		 $("#btn_add_stakeholder").click(function()
		{
			var stk_firstname=$("#stk_firstname");
			var stk_lastname=$("#stk_lastname");
			var stk_position=$("#stk_position");
			var stk_email=$("#stk_email");
			var stk_no=$("#stk_no");
			var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
			
			$('#stk_firstname').removeClass('error_border');
			$('#stk_lastname').removeClass('error_border');
			$('#stk_position').removeClass('error_border');
			$('#stk_email').removeClass('error_border');
			$('#stk_no').removeClass('error_border');
			
			$('#stk_firstname').addClass('form-control');
			$('#stk_lastname').addClass('form-control');
			$('#stk_position').addClass('form-control');
			$('#stk_email').addClass('form-control');
			$('#stk_no').addClass('form-control');
			
			if(stk_firstname.val()=="")
            {
			    stk_firstname.val('');
				$('#stk_firstname').attr('placeholder','Enter First  name');
				$('#stk_firstname').removeClass('form-control');
				$('#stk_firstname').addClass('error_border');
				$('#stk_firstname').focus();
				return false;
			 }	
			 		
			else if(stk_lastname.val()=="")
            {
			    stk_lastname.val('');
				$('#stk_lastname').attr('placeholder','Enter Last name');
				$('#stk_lastname').removeClass('form-control');
				$('#stk_lastname').addClass('error_border');
				$('#stk_lastname').focus();
				return false;
			 }			
			
			else	if(stk_position.val()=="")
            {
			    stk_position.val('');
				$('#stk_position').attr('placeholder','Enter Position name');
				$('#stk_position').removeClass('form-control');
				$('#stk_position').addClass('error_border');
				$('#stk_position').focus();
				return false;
			 }			
			else	if(stk_email.val()=="")
            {
			    stk_email.val('');
				$('#stk_email').attr('placeholder','Enter Email Id');
				$('#stk_email').removeClass('form-control');
				$('#stk_email').addClass('error_border');
				$('#stk_email').focus();
				return false;
			 }			
			else if(!filter.test(stk_email.val()))
			{
				stk_email.val('');
				$('#stk_email').attr('placeholder','Enter valid Email Id');
				$('#stk_email').removeClass('form-control');
				$('#stk_email').addClass('error_border');
				$('#stk_email').focus();
				return false;
			}
			else if(stk_no.val()=="")
            {
			    stk_no.val('');
				$('#stk_no').attr('placeholder','Enter Contact No');
				$('#stk_no').removeClass('form-control');
				$('#stk_no').addClass('error_border');
				$('#stk_no').focus();
				return false;
			 }			
			else	if(isNaN(stk_no.val()))
            {
			    stk_no.val('');
				$('#stk_no').attr('placeholder','Enter Numbers only');
				$('#stk_no').removeClass('form-control');
				$('#stk_no').addClass('error_border');
				$('#stk_no').focus();
				return false;
			 }			
			
		});
	
		// validation of verify stk password validation
		$("#verify_stk").click(function()
		{ 	
			var stk_password=$("#stk_password");
			var confirm_stk_password=$("#confirm_stk_password");
			
			$('#stk_password').removeClass('error_border');
			$('#confirm_stk_password').removeClass('error_border');
			
			$('#stk_password').addClass('form-control');
			$('#confirm_stk_password').addClass('form-control');
			
			if(stk_password.val()=="")
            { 
			    stk_password.val('');
				$('#stk_password').attr('placeholder','Please Enter Password');
				$('#stk_password').removeClass('form-control');
				$('#stk_password').addClass('error_border');
				$('#stk_password').focus();
				return false;
			}
			if(stk_password.val().length<6)
            { 
			    stk_password.val('');
				$('#stk_password').attr('placeholder','Password Must Be Atleast 6 Character');
				$('#stk_password').removeClass('form-control');
				$('#stk_password').addClass('error_border');
				$('#stk_password').focus();
				return false;
			}
			else if(confirm_stk_password.val()=="")
            { 
			    confirm_stk_password.val('');
				$('#confirm_stk_password').attr('placeholder','Please Enter Confirm Password');
				$('#confirm_stk_password').removeClass('form-control');
				$('#confirm_stk_password').addClass('error_border');
				$('#confirm_stk_password').focus();
				return false;
			}
			else if(stk_password.val()!=confirm_stk_password.val())
            { 
			    confirm_stk_password.val('');
				$('#confirm_stk_password').attr('placeholder','Confirm Password Does Not Match');
				$('#confirm_stk_password').removeClass('form-control');
				$('#confirm_stk_password').addClass('error_border');
				$('#confirm_stk_password').focus();
				return false;
			 }			
		});
		
		//validation for chandge password add new cc
	    $("#change_cc_pw").click(function()
		{ 
			var email=$("#email");
			var new_pass=$("#new_pass");
			var confirm_pass=$("#confirm_pass");
			
			$('#new_pass').removeClass('error_border');
			$('#confirm_pass').removeClass('error_border');
			
			$('#new_pass').addClass('form-control');
			$('#confirm_pass').addClass('form-control');
		     
			if(new_pass.val()=="")
            { 
			    new_pass.val('');
				$('#new_pass').attr('placeholder','Please New Password');
				$('#new_pass').removeClass('form-control');
				$('#new_pass').addClass('error_border');
				$('#new_pass').focus();
				return false;
			
			}
			else  if(confirm_pass.val()=="")
            { 
			    confirm_pass.val('');
				$('#confirm_pass').attr('placeholder','Please Confirm Password');
				$('#confirm_pass').removeClass('form-control');
				$('#confirm_pass').addClass('error_border');
				$('#confirm_pass').focus();
				return false;
			
			}
	
		});
		
		
		//validation for reminder cc
		$("#btn_set_reminder").click(function()
		{
			var reminder=$("#reminder");
			
			$('#reminder').removeClass('error_borderl');
			$('#reminder').addClass('form-control');
			
			if(reminder.val()=="")
			{ 
				reminder.val('');
				$('#reminder').attr('placeholder','Select filed');
				$('#reminder').removeClass('form-control');
				$('#reminder').addClass('error_border');
				$('#reminder').focus();
				return false;
			
			}
		});
		
		//validation for initiate response
		
		$("#btn_initiate_responce").click(function()
		{
		var initiate_scenario=$("#initiate_scenario");
		var initiate_standbymsg=$("#initiate_standbymsg");
		var newmsg=$("#newmsg");
		var inform=$("inform");
		 
		$('#initiate_scenario').removeClass('error_borderl');
		$('#initiate_standbymsg').removeClass('error_borderl');
		$('#newmsg').removeClass('error_borderl');
		$('#inform').removeClass('error_borderl');
		
		$('#initiate_scenario').addClass('form-control');
		$('#initiate_standbymsg').addClass('form-control');
		$('#newmsg').addClass('form-control');
		$('#inform').addClass('form-control');
		 
		  if(initiate_scenario.val()=="")
		  { 
				initiate_scenario.val('');
				$('#initiate_scenario').attr('placeholder','Select filed');
				$('#initiate_scenario').removeClass('form-control');
				$('#initiate_scenario').addClass('error_border');
				$('#initiate_scenario').focus();
				return false;
			
			}
		  else if(initiate_standbymsg.val()=="" && initiate_repose.radioval[0].checked==true)
		  { 
				initiate_standbymsg.val('');
				$('#initiate_standbymsg').attr('placeholder','Select filed');
				$('#initiate_standbymsg').removeClass('form-control');
				$('#initiate_standbymsg').addClass('error_border');
				$('#initiate_standbymsg').focus();
				return false;
			}
		 /* else if(newmsg.val()=="")
		  { 
				newmsg.val('');
				$('#newmsg').attr('placeholder','Enter Message');
				$('#newmsg').removeClass('form-control');
				$('#newmsg').addClass('error_border');
				$('#newmsg').focus();
				return false;
			}*/
		  else if(initiate_repose.inform[0].checked==false && initiate_repose.inform[1].checked==false)
		  {
				document.getElementById('err_inform').innerHTML="Select Notify";
				return false;
		 }
		});	
		
		$("#add_stand_by").click(function()
		{
		var sel_scenario=$("#sel_scenario");
		var stand_by_msg=$("#stand_by_msg");
			
			if(sel_scenario.val()=="")
			{ 
				sel_scenario.val('');
				$('#sel_scenario').attr('placeholder','Select filed');
				$('#sel_scenario').removeClass('form-control');
				$('#sel_scenario').addClass('error_border');
				$('#sel_scenario').focus();
				return false;
			}
			else if(stand_by_msg.val()=="")
			{ 
				stand_by_msg.val('');
				$('#stand_by_msg').attr('placeholder','Enter Message');
				$('#stand_by_msg').removeClass('form-control');
				$('#stand_by_msg').addClass('error_border');
				$('#stand_by_msg').focus();
				return false;
			}
		});
		
		//validation for Add Scenario 
		$("#add_scen").click(function()
		{
		var scenario_desc=$("#scenario_desc");
		if(scenario_desc.val()=="")
		  { 
				scenario_desc.val('');
				$('#scenario_desc').attr('placeholder','Enter Scenario');
				$('#scenario_desc').removeClass('form-control');
				$('#scenario_desc').addClass('error_border');
				$('#scenario_desc').focus();
				return false;
			
			}
		});
		
		//validation for Upload document
		$("#upload_doc").click(function()
		{ 
			var file_upload_name=$("#file_upload_name");
			var file_name=file_upload_name.value;
			var ext=file_name.substring(file_name.lastIndexOf('.')+1);
			if(file_name!="" && ext!='doc' && ext!='DOC' && ext!='docx' && ext!='DOCX' && ext!='pdf' && ext!='PDF' && ext!='txt' && ext!="TXT")
			{ 
				$('err_file_upload_name').fadeIn("slow");
				document.getElementById("err_file_upload_name").innerHTML="Invalid document type";
				$('err_file_upload_name').fadeOut("slow");
				return false;
			}
		});
		
		/* fill stand by message on chnage of scenarion on Initiate Response*/
		$("#initiate_scenario").change(function(){
				var scn_id=$("#initiate_scenario").val();
				//$("#initiate_standbymsg > option").remove();
				$.ajax({
					type: 'POST',
					url: base_url+'cc/responce/fetch_stand_by_message/'+scn_id,
					dataType:'json',
					success:function(message)
					{ 
						var default_opt=$('<option>');
						default_opt.val("");
						default_opt.text("Select");
						$("#initiate_standbymsg").append(default_opt);
						$.each(message,function(msg_id,msg_val){
							//var opt=$('<option>');
							$("#stnd_by_id").val(msg_id);
							$("#initiate_standbymsg").text(msg_val);
							//$("#initiate_standbymsg").append(opt);
						});
					}
				});	
		});
		
		
		//validation for chandge password add new cc
		$("#update_personal_info").click(function()
		{ 
		var f_name=$("#f_name");
		var l_name=$("#l_name");
		var ph_no=$("#ph_no");
		var address=$("#address");
		var city=$("#city");
		var state=$("#state");
		var zip=$("#zip");
		var zipcode = /^[0-9a-zA-Z]+$/; 
		
		  if(f_name.val()=="")
            { 
			    f_name.val('');
				$('#f_name').attr('placeholder','Please Enter First Name');
				$('#f_name').removeClass('form-control');
				$('#f_name').addClass('error_border');
				$('#f_name').focus();
				return false;
			
			}
			else if(l_name.val()=="")
            { 
			    l_name.val('');
				$('#l_name').attr('placeholder','Please Enter last Name');
				$('#l_name').removeClass('form-control');
				$('#l_name').addClass('error_border');
				$('#l_name').focus();
				return false;
			
			}
			else if(ph_no.val()=="")
            { 
			    ph_no.val('');
				$('#ph_no').attr('placeholder','Please Enter Phone Number');
				$('#ph_no').removeClass('form-control');
				$('#ph_no').addClass('error_border');
				$('#ph_no').focus();
				return false;
			
			}
			else if(isNaN(ph_no.val()))
            { 
			    ph_no.val('');
				$('#ph_no').attr('placeholder','Please Enter  Number');
				$('#ph_no').removeClass('form-control');
				$('#ph_no').addClass('error_border');
				$('#ph_no').focus();
				return false;
			
			}
			else if(address.val()=="")
            { 
			    address.val('');
				$('#address').attr('placeholder','Please Enter Address');
				$('#address').removeClass('form-control');
				$('#address').addClass('error_border');
				$('#address').focus();
				return false;
			
			}
			else if(city.val()=="")
            { 
			    city.val('');
				$('#city').attr('placeholder','Please Enter City');
				$('#city').removeClass('form-control');
				$('#city').addClass('error_border');
				$('#city').focus();
				return false;
			
			}
			else if(state.val()=="")
            { 
			    state.val('');
				$('#state').attr('placeholder','Please Enter State');
				$('#state').removeClass('form-control');
				$('#state').addClass('error_border');
				$('#state').focus();
				return false;
			
			}
			else if(zip.val()=="")
            { 
			    zip.val('');
				$('#zip').attr('placeholder','Please Enter Zip Code');
				$('#zip').removeClass('form-control');
				$('#zip').addClass('error_border');
				$('#zip').focus();
				return false;
			
			}
			else if(!zipcode.test(zip.val())) 
			{ 
			 	zip.val('');
				$('#zip').attr('placeholder','Please EnterAlphnumeric value');
				$('#zip').removeClass('form-control');
				$('#zip').addClass('error_border');
				$('#zip').focus();
				return false;
			} 
		});
		
		
		
		/*$("#add_message").click(function()
		{  alert("here");
			var common_message=$("#common_message");
			
			if(common_message.val()=="")
			{ 
				common_message.val('');
				$('#common_message').attr('placeholder','Enter message');
				$('#common_message').removeClass('form-control');
				$('#common_message').addClass('error_border');
				$('#common_message').focus();
				return false;
			}
		});*/
		
		/* set status is_read=1 from inbox of CRT panel */	
		$(".read_message").click(function(){
			var celement=$(this);
			var is_read=parseInt(celement.attr('data-read'));
			var msgid=parseInt(celement.attr('data-msgid'));
			
			if(is_read==0)
			{
				$.post(base_url+'crt/message/mark_as_read/',{msgid:msgid},
				function(data)
				{
					window.location.href=base_url+"crt/message/read/"+msgid+'/inbox';
				});
			}
			else
			{
				window.location.href=base_url+"crt/message/read/"+msgid+'/inbox';
			}
		});
		
		/* set status is_read=1 from inbox of CC panel */	
		$(".cc_read_message").click(function(){
			var celement=$(this);
			var is_read=parseInt(celement.attr('data-read'));
			var msgid=parseInt(celement.attr('data-msgid'));
			
			if(is_read==0)
			{
				$.post(base_url+'cc/message/mark_as_read/',{msgid:msgid},
				function(data)
				{
					window.location.href=base_url+"cc/message/read/"+msgid+'/inbox';
				});
			}
			else
			{
				window.location.href=base_url+"cc/message/read/"+msgid+'/inbox';
			}
		});
		
});

function del_confirm(site_path)
{ 
/*	$.messager.confirm('Confirm:', "Are you sure you want to delete ?", function(r)
	{;
		if(r)
		{ 
			location.href=site_path;
		}
		else
		{return false;}
	});*/
	
	bootbox.confirm("<p class='lead'>Are you sure you want to delete selected item?</p>", function(r)
	{;
		if(r)
		{ 
			location.href=site_path;
		}
	});
} 

function complete_confirm(site_path)
{ 
/*	$.messager.confirm('Confirm:', "Are you sure you want to delete ?", function(r)
	{;
		if(r)
		{ 
			location.href=site_path;
		}
		else
		{return false;}
	});*/
	
	bootbox.confirm("<p class='lead'>Are you sure Task is completed?</p>", function(r)
	{;
		if(r)
		{ 
			location.href=site_path;
		}
	});
} 
	
function contained_confirm(site_path)
{ 
/*	$.messager.confirm('Confirm:', "Are you sure you want to delete ?", function(r)
	{;
		if(r)
		{ 
			location.href=site_path;
		}
		else
		{return false;}
	});*/
	
	bootbox.confirm("<p class='lead'>Are you sure Crisis Contained?</p>", function(r)
	{;
		if(r)
		{ 
			location.href=site_path;
		}
	});
} 

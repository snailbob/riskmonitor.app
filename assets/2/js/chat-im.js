/* 
Created by: Kenrick Beckett

Name: Chat Engine
*/

var instanse = false;
var state;
var mes;
var file;

function Chat () {
    this.update = updateChat;
    this.send = sendChat;
	this.getState = getStateOfChat;
}

//gets the state of the chat
function getStateOfChat(){
	if(!instanse){
		 instanse = true;
		 $.ajax({
			   type: "POST",
			   url: base_url+"cc/conference/process_chat",
			   data: {  
			   			'function': 'getState',
			   			'org': org,
						'file': file
						},
			   dataType: "json",
			
			   success: function(data){
				   state = data.state-25;//25 most recent message
				   updateChat();
				   instanse = false;
			   },
			});
	}	 
}

//Updates the chat
function updateChat(){
	 if(!instanse){
		 instanse = true;
	     $.ajax({
			   type: "POST",
			   url: base_url+"cc/conference/process_chat",
			   data: {  
			   			'function': 'update',
						'org': org,
						'state': state,
						'file': file
						},
			   dataType: "json",
			   success: function(data){
				   if(data.text){
						for (var i = 0; i < data.text.length; i++) {
                            $('#chat-area').append($("<p>"+ data.text[i] +"</p>"));
                        }								  
				   }
				   
				   $('.loading_mess').hide();
				   
				   if(data.text.length > 0){
					   $('.start_con').hide();
				   }
				   
				   document.getElementById('chat-area').scrollTop = document.getElementById('chat-area').scrollHeight;
				   instanse = false;
				   state = data.state;
			   },
			});
	 }
	 else {
		 setTimeout(updateChat, 600);
	 }
}

//send the message
function sendChat(message, nickname, time_sent, org, senderr, receiverr)
{       
    updateChat();
     $.ajax({
		   type: "POST",
		   url: base_url+"cc/conference/process_chat",
		   data: {  
		   			'function': 'send',
					'message': message,
					'nickname': nickname,
					'senderr': senderr,
					'receiverr': receiverr,
					'time_sent': time_sent,
					'org': org,
					'file': file
				 },
		   dataType: "json",
		   success: function(data){
			   updateChat();
			   //$('#page-wrap').hide();$('#show_chat').show();
		   },
		});
}

// JavaScript Document


/***********************consultant***************************/

$(document).ready(function(e) {
	
	$('select#select_cat').on('change', function() {
		$('#sub_cat').html('<i class="fa fa-spinner fa-spin"></i>');
		var step = this.value;
		$.ajax({
			  type: "POST",
			  
			  url: base_url + 'reseller/recall/select_step',
			  
			  data: {
				  step: step
			  },
			  
			  success: function(data) {
				 console.log(data);
			     $('#sub_cat').html(data);
	
			  }
			  
		});
	

	});//SELECT STEP

	$('tr').hover(function(){
		$(this).find('#arrange_arrows').show();
	},
	function(){
		$(this).find('#arrange_arrows').hide();
	});
	
    /*$(".fa-arrow-up,.fa-arrow-down").click(function(){
        var row = $(this).parents("tr:first");
        if ($(this).is(".fa-arrow-up")) {
            row.insertBefore(row.prev());
        } else {
            row.insertAfter(row.next());
        }
    });*/	
	
	
	var step2List = $('#draggablePanelList');
	var step3List = $('#draggablePanelList3');
	var step4List = $('#draggablePanelList4');
	var step5List = $('#draggablePanelList5');
	var step6List = $('#draggablePanelList6');
	var step7List = $('#draggablePanelList7');
	var step8List = $('#draggablePanelList8');


	$('.tasks_draggable').sortable({
		// Only make the .panel-heading child elements support dragging.
		// Omit this to make the entire <li>...</li> draggable.
		opacity: 0.6, 
		handle: '#arrange_arrows',
		cursor: 'move',
		update: function(event,ui) {
			var $self = $(this); 
			$('tr', $(this)).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					 //console.log(ui);
					//saveSorts($(this));
					
					var order = $self.sortable("serialize");
					$self.closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');
					
					$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
						console.log(theResponse);
						$self.closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
					});
					
					
				 // Persist the new indices.
			});
		}
	});
	
	/*step2List.sortable({
		// Only make the .panel-heading child elements support dragging.
		// Omit this to make the entire <li>...</li> draggable.
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step2List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort();
				 // Persist the new indices.
			});
		}
	});
	
	
	step3List.sortable({
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step3List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort3();
			});
		}
	});
	
	step4List.sortable({
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step4List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort4();
			});
		}
	});
	
	step5List.sortable({
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step5List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort5();
			});
		}
	});
	
	step6List.sortable({
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step6List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort6();
			});
		}
	});
	
	step7List.sortable({
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step7List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort7();
			});
		}
	});

	step8List.sortable({
		opacity: 0.6, 
		handle: '#arrange_arrows', 
		update: function() {
			$('tr', step8List).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					saveSort8();
			});
		}
	});*/
	
	
	//show sub-cat panel
	if($('.new_cat').length > 0){
		$('#cat_chevron').removeClass('fa-chevron-up');
		$('#cat_chevron').addClass('fa-chevron-down');
		$('#subCat').addClass('in');
	}
	
	$('#cat_chevron').click(function(e) {
		var atttr;
		if ($(this).hasClass("fa-chevron-up"))
			atttr = "Expand";   
		else
			atttr = "Minimize";
		
		$("#cat_chevron").attr("data-original-title" , atttr);
		
    });
});

/*function saveSort(){
	var order = $('#draggablePanelList').sortable("serialize");
	$('#draggablePanelList').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');
	//$('#draggablePanelList').find('#arrange_arrows').html('<i class="fa fa-spinner fa-spin"></i>');
	
	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
		//$('#draggablePanelList').find('#arrange_arrows').html('<i class="fa fa-arrows"></i>');
	});
}

function saveSort3(){
	var order = $('#draggablePanelList3').sortable("serialize");
	$('#draggablePanelList3').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');

	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList3').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
	});
}

function saveSort4(){
	var order = $('#draggablePanelList4').sortable("serialize");
	$('#draggablePanelList4').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');

	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList4').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
	});
}

function saveSort5(){
	var order = $('#draggablePanelList5').sortable("serialize");
	$('#draggablePanelList5').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');

	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList5').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
	});
}

function saveSort6(){
	var order = $('#draggablePanelList6').sortable("serialize");
	$('#draggablePanelList6').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');

	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList6').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
	});
}

function saveSort7(){
	var order = $('#draggablePanelList7').sortable("serialize");
	$('#draggablePanelList7').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');

	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList7').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
	});
}

function saveSort8(){
	var order = $('#draggablePanelList8').sortable("serialize");
	$('#draggablePanelList8').closest('table').find('#load_save_step').show().html('<i class="fa fa-spinner fa-spin"></i>');

	$.post(base_url + "reseller/recall/save_sort",order,function(theResponse){
		console.log(theResponse);
		$('#draggablePanelList8').closest('table').find('#load_save_step').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
	});
}*/




function submitGuidance(toggle_p,a_link,guidance_val,task_id,btn){
	//var btnsub = $(event.target);
	//btnsub.addClass('disabled');

	var guidance = $(guidance_val).val();
//	if (guidance == ''){
//		bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Please add guidance</p>');
//		return false;
//	}
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'reseller/recall/add_guide',
		  
		  data: {
			  guidance: guidance,
			  task_id: task_id
		  },
		  
		  success: function(data) {
			 console.log(data);
			if (data == 'success'){
				$('#'+btn).attr('data-original-title',guidance);
				$('#'+toggle_p).slideUp('slow');
				
				$('#'+a_link).html(guidance);
				if(guidance == ''){
					$('#'+a_link).html('Add Task Guidance');
				}
				
				$('#'+a_link).delay(600).fadeIn();
				//btnsub.removeClass('disabled');
			}
			else{
				bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Something went wrong! Please try to refresh page</p>');
				return false;
			}

		  }
		  
	});
	
}//submitGuidance

function submitTaskUpdate(toggle_p,a_link,task_val,task_id,btn){

	//var btnsub = $(event.target);
	//btnsub.addClass('disabled');

	var task = $(task_val).val();
	if (task == ''){
		bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Please add task</p>');
		return false;
	}
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'reseller/recall/update_task',
		  
		  data: {
			  task: task,
			  task_id: task_id
		  },
		  
		  success: function(data) {
			 console.log(data);
			if (data == 'success'){
				$('#'+toggle_p).slideUp('slow');
				$('#'+a_link).html(task);
				$('#'+a_link).delay(600).fadeIn();
				//btnsub.removeClass('disabled');
			}
			else{
				bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Something went wrong! Please try to refresh page</p>');
				return false;
			}

		  }
		  
	});
	
}//submitTaskUpdate


function submitCategoryUpdate(toggle_p,a_link,category_val,category_id,btn){
	
	//var btnsub = $(event.target);
	//btnsub.addClass('disabled');

	var category = $(category_val).val();
	if (category == ''){
		bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Please add task</p>');
		return false;
	}
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'reseller/recall/update_category',
		  
		  data: {
			  category: category,
			  category_id: category_id
		  },
		  
		  success: function(data) {
			 console.log(data);
			if (data == 'success'){
				$('#'+toggle_p).slideUp('slow');
				$('#'+a_link).html(category);
				$('#'+a_link).delay(600).fadeIn();
				//btnsub.removeClass('disabled');
			}
			else{
				bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Something went wrong! Please try to refresh page</p>');
				return false;
			}

		  }
		  
	});
	
}//submitCategoryUpdate

function selectTaskCat(toggle_p,a_link,category_val,category_id){
	//var btnsub = $(event.target);
	//btnsub.addClass('disabled');

	var category = $(category_val).val();
	if (category == ''){
		bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Please add task</p>');
		return false;
	}
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'reseller/recall/select_new_category',
		  
		  data: {
			  category: category,
			  task_id: category_id
		  },
		  
		  success: function(data) {
			 console.log(data);
			 
			if (data != 'error'){
//					$('#'+toggle_p).slideUp('slow');
//					$('#'+a_link).html(data);
//					$('#'+a_link).delay(600).fadeIn();
					window.location.reload();
				
				//btnsub.removeClass('disabled');
			}
			else{
				bootbox.alert('<p class="lead"><i class="fa fa-info text-info"></i> Something went wrong! Please try to refresh page</p>');
				return false;
			}

		  }
		  
	});
	
}//selectTaskCat

function saveNewCategory(){
	var catt = $('#add_cat').val();
	var select_step = $('#select_step').val();
		
		if (catt == ''){
			$('#category_err').html('Mandatory field');
			return false;
		}
		else if (select_step == ''){
			$('#category_err').html('');
			$('.step_err').html('Mandatory field');
			return false;
		}
		else{

			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'reseller/recall/save_new_category',
				  
				  data: {
					  catt: catt,
					  step: select_step
				  },
				  
				  success: function(data) {
					  console.log(data);
					 //event.target
					 window.location.reload(true);
				  }
				  
			});//ajax

		}
}//saveNewCategory


function saveNewTask(){
	var tass = $('#text_task').val();
	var task_guidance = $('#text_task_guidance').val();
	var catt = $('#select_cat').val();
	var scatt = $('#select_subcat').val();
		
		if (tass == ''){
			$('.task_err').show();
			$('.task_err').html('Mandatory field');
			return false;
		}
		else if (catt == ''){
			$('.category_err').show();
			$('.category_err').html('Mandatory field');
			$('.task_err').html('');
			return false;
		}
		else{

			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'reseller/recall/save_new_task',
				  
				  data: {
					  task: tass,
					  task_guidance: task_guidance,
					  category: catt,
					  sub_cat: scatt
				  },
				  
				  success: function(data) {
					  console.log(data);
					 //event.target
					 window.location.reload(true);
				  }
				  
			});//ajax

		}
}//saveNewTask


function saveEnableDisable(id,disabled,btn){
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'reseller/recall/toggle_task_enable',
		  
		  data: {
			  id: id,
			  disabled: disabled
		  },
		  
		  success: function(data) {
			 console.log(data);
			 if (data == 'success'){
				$("#"+btn).hide();
				$("#"+btn).siblings('button').show();
			 }
			 //event.target
			 //window.location.reload(true);
		  }
		  
	});//ajax
}
function toggleDisable(id,disabled,btn){

	if(disabled == '0'){
		
		bootbox.confirm("<p class='lead'>"+action_messages.recall.task_disable+"</p>", function(r) {
			if(r) {

				$.ajax({
					  type: "POST",
					  
					  url: base_url + 'reseller/recall/toggle_task_enable',
					  
					  data: {
						  id: id,
						  disabled: disabled
					  },
					  
					  success: function(data) {
						 console.log(data);
						 if (data == 'success'){
							$("#"+btn).fadeOut(500);
							setTimeout(function() {$("#"+btn).siblings('button').show()},500);
						 }
						 //event.target
						 //window.location.reload(true);
					  }
					  
				});//ajax

			}
		});
	}
	else{
	
		bootbox.confirm("<p class='lead'>"+action_messages.recall.task_enable+"</p>", function(r) {
			if(r) {
			
			
				$.ajax({
					  type: "POST",
					  
					  url: base_url + 'reseller/recall/toggle_task_enable',
					  
					  data: {
						  id: id,
						  disabled: disabled
					  },
					  
					  success: function(data) {
						 console.log(data);
						 if (data == 'success'){
							$("#"+btn).fadeOut(500);
							setTimeout(function() {$("#"+btn).siblings('button').show()},500);
						 }
						 //event.target
						 //window.location.reload(true);
					  }
					  
				});//ajax
			
			}
		});
	}
}//toggleDisable




function confirmDelete(id,table,del_btn){
	if(table == 'cf_recall_steps_category'){
		var the_table = "category";
		var the_message = action_messages.recall.sb_category_delete;
	}
	if(table == 'cf_recall_guidance'){
		var the_table = "task";
		var the_message = action_messages.recall.sb_task_delete;
	}
	
	bootbox.confirm("<p class='lead'>"+the_message+"</p>", function(r)
	{
		if(r)
		{ 

		
			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'reseller/recall/ajax_delete',
				  
				  data: {
					  id: id,
					  table: table
				  },
				  
				  success: function(data) {
					 console.log(data);
					 if (data == 'success'){
						 
						 if(the_table == 'Category'){
						 	window.location.reload(true);
						 }else{
							$("#"+del_btn).closest('tr').addClass('bg-danger');
							$("#"+del_btn).closest('tr').fadeOut();
						 }
					 }
					 //event.target
					 //window.location.reload(true);
				  }
				  
			});//ajax
			
		}
	});

}

$(document).ready(function(e) {
	
	//confirm task review date
    $('.task_reviewed').on('click', function(){
		bootbox.confirm('<p class="lead">'+action_messages.recall.reviewed_sb_tasks+'</p>', function(e){
			if(e){
				
		
				$.ajax({
					  url: base_url + 'cc/standbytasks/tasks_reviewed',
					  success: function(data) {
						 console.log(data);
						 window.location.reload(true);
					  }
					  
				});//ajax
								
			}
		});
	});
	
	
	//filter if cc has reviewed standby tasks
	$('.recall-form').on('submit', function(){
		if(reviewdate == '0000-00-00 00:00:00'){
			bootbox.alert('<p class="lead">You need to confirm first that you have reviewed Standby Tasks as part of the pre-incident planning.</p>');
			return false;
		}
	});
});



function saveNewRPackCat(pack_id){
	var name = $('#step_name').val();
	if(name == ''){
		$('#addRPackStep').find('span.text-red').show();
		$('#addRPackStep').find('span.text-red').html('Mandatory field');
		return false;
	}
	else{
		$('#addRPackStep').find('button.btn-primary').addClass('disabled');
		$('#addRPackStep').find('button.btn-primary').html('Loading...');
		
		$('#addRPackStep').find('span.text-red').hide();
		
		$.ajax({
			  type: "POST",
			  
			  url: base_url + 'webmanager/recall/save_recall_pack_category',
			  
			  data: {
				  name: name,
				  pack_id: pack_id
				  
			  },
			  
			  success: function(data) {
				 console.log(data);
				 window.location.reload(true);
	
			  }
			  
		});
	}
}


function addStepName(name,id){
	$('#updateStep').find('input#step_id').val(id);
	$('#updateStep').find('input#step_name').val(name);
}


function saveUpdateStep(){
	var name = $('#updateStep').find('input#step_name').val();
	var id = $('#updateStep').find('input#step_id').val();
	
	if(name == ''){
		$('#updateStep').find('span.text-red').show();
		$('#updateStep').find('span.text-red').html('Mandatory field');
		return false;
	}
	else{
		$('#updateStep').find('button.btn-primary').addClass('disabled');
		$('#updateStep').find('button.btn-primary').html('Loading...');
		
		$('#updateStep').find('span.text-red').hide();
		
		$.ajax({
			  type: "POST",
			  
			  url: base_url + 'webmanager/recall/update_recall_pack_category',
			  
			  data: {
				  name: name,
				  id: id
				  
			  },
			  
			  success: function(data) {
				 console.log(data);
				 //return false;
				 window.location.reload(true);
	
			  }
			  
		});

	}
}



function delRPackCat(id,del_btn){
	
	bootbox.confirm("<p class='lead'>"+action_messages.recall.delete_sb_step+"</p>",function(r){
		if(r){

			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'webmanager/recall/delete_recall_pack_cat',
				  
				  data: {
					  id: id
					  
				  },
				  
				  success: function(data) {
					 console.log(data);
					 
					 if (data == 'success'){
//						$("#"+del_btn).closest('tr').addClass('bg-danger');
//						$("#"+del_btn).closest('tr').fadeOut();
						window.location.reload(true);
					 }
		
				  }
				  
			});
			
		}
	});
}


function savePackSubCat(){
	var catt = $('#add_cat').val();
	var select_step = $('#select_step').val();
	var pack_id = $('#pack_id').val();
		
		if (catt == ''){
			$('#category_err').html('Mandatory field');
			return false;
		}
		else if (select_step == ''){
			$('#category_err').html('');
			$('.step_err').html('Mandatory field');
			return false;
		}
		else{

			$.ajax({
				  type: "POST",
				  
				  url: base_url + 'webmanager/recall/save_pack_subcat',
				  
				  data: {
					  pack_id: pack_id,
					  catt: catt,
					  step: select_step
				  },
				  
				  success: function(data) {
					  console.log(data);
					 //event.target
					 window.location.reload(true);
				  }
				  
			});//ajax

		}
}//saveNewCategory

$(document).ready(function(e) {
    $('.step-name').hover(
		function(){
			$(this).find('small').show();
		},
		function(){
			$(this).find('small').hide();
		}
	);
	
//    $('.category-name').hover(
//		function(){
//			$(this).find('small').show();
//		},
//		function(){
//			$(this).find('small').hide();
//		}
//	);
	
	if($(document).width() > 892){
		
		$('#steporder li').hover(function(){
			$(this).find('.fa-arrows').css('visibility','visible');
		},
		function(){
			$(this).find('.fa-arrows').css('visibility','hidden');
		});
		
	}
	
	
	$("ul#steporder").sortable({ 
		opacity: 0.6, 
		handle: '.fa-arrows', 
		cursor: 'move',
		update: function(event,ui) {
			var $self = $(this); 
			$('li', $(this)).each(function(index, elem) {
				 var $listItem = $(elem),
					 newIndex = $listItem.index();
					 //console.log(ui);
					//saveSorts($(this));
					
					var order = $self.sortable("serialize");
					$self.closest('div.panel').find('.save_steps_indicate').show().html('<i class="fa fa-spinner fa-spin"></i>');
					
					$.post(base_url + "reseller/recall/save_sort_step",order,function(theResponse){
						console.log(theResponse);
						$self.closest('div.panel').find('.save_steps_indicate').html('<i class="fa fa-check text-success"></i>').delay(2000).fadeOut();
					});
					
					
				 // Persist the new indices.
			});
		}

	});	
	
});

var selStep = function(stepno){
	console.log(stepno);
	
	$('#select_pack_cat').val(stepno);
	$('#select_pack_cat').change();
};

var selStepForCat = function(stepno){
	console.log(stepno);
	
	$('#select_step').val(stepno);
	var step_no = $('#select_step').val();
	var pack_id = $('#pack_id').val();
	$('#delete_empty_cat').find('button').addClass('hidden');

	$('#delete_empty_cat').find('.form-group').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin"></i></p>');
	$('#delete_empty_cat').find('.form-group').load(base_url+"reseller/recall/load_emptycat/"+step_no+"/"+pack_id);
	
	//$('#select_step').change();
};

$(document).on("click", 'input[name="empty_cat[]"]', function(event) { 
	if($('input[name="empty_cat[]"]:checked').length > 0){
		$('#delete_empty_cat').find('button').removeClass('hidden');
	}else{
		$('#delete_empty_cat').find('button').addClass('hidden');
	}
});


$(document).ready(function(e) {
    $('#delete_empty_cat').on('submit', function(){
        var cate = [];
		
        $('input[name="empty_cat[]"]:checked').each(function(i){
          cate[i] = $(this).val();
        });
		
		
		$.ajax({
			  type: "POST",
			  
			  url: base_url + 'reseller/recall/delete_empty_cat',
			  
			  data: {
				  cate: cate
			  },
			  
			  success: function(data) {
				 console.log(data);
			     window.location.reload(true);
	
			  }
			  
		});

		return false;
	});
});



$(document).ready(function(e) {
	$('#simu_module_form input[type="text"]').on('keyup', function(){
		if($(this).val() != ''){
			$('#simu_module_form').find('button').removeAttr('disabled');
		}
		else{
			$('#simu_module_form').find('button').attr('disabled', 'disabled');
		}
	});
	
	$('#simu_module_form').on('submit', function(){
		
		$.post(
			base_url+'reseller/simulation/index',
			$('#simu_module_form').serialize(),
			function(response){
				console.log(response);
				window.location.reload(true);
			}
			
		);
		return false;
	});
	
	//datepicker
	$('input[name="mediadate"]').datetimepicker({
		pickTime: false,
		minDate: moment() 
		
	});

	
    $('#simulation_form').on('submit', function(){
		var content = $('#summernote').code();
		var trigger = $('select[name="trigger"]').val();

		var mediacount = $('.audio_video_container .well').children().length;
		
		console.log($('#simulation_form').serialize());
		
		var data = {
			content: content	
		};
		
		if(content == '' && mediacount == 0){
			bootbox.alert('<p class="lead">Simulation content cannot be blank.</p>');
		}
		else if(trigger == ''){
			bootbox.alert('<p class="lead">Select trigger.</p>');
		}
		else{

			$.post(
				base_url+'reseller/simulation/manage',
				$('#simulation_form').serialize() + '&' + $.param(data),
				//{utime: utime, content: content, module_id: module_id, simulation_id: simulation_id, simulation_name: simulation_name},
				function(response){
					console.log(response);
					$('#simulation_form').find('button[type="submit"]').html('Changes Saved!');
					setTimeout(
					function(){
						$('#simulation_form').find('button[type="submit"]').removeClass('disabled').html('Save Changes');
					}, 1500);
					
					$('input[name="simulation_id"]').val(response.id);
				},
				'json'
			);
		}

		return false;
	});
	
	$(document).on('click', '.delete_media', function(){
		var $self = $(this);
		var id = $(this).attr('data-id');
		bootbox.confirm('<p class="lead">'+action_messages.global.generic_delete_message+'</p>', function(e){
			if(e){
				console.log(id);
				$.post(
					base_url+'reseller/simulation/deletemedia',
					{id: id},
					function(res){
						console.log(res);
						$self.closest('p').remove();
					}
					
				);
			
			}
		});
		return false;
	});
	
	$('.preview_btn').on('click', function(){
		var content = $('#summernote').code();
		if(content == ''){
			bootbox.alert('<p class="lead">No preview available.</p>');
		}
		else{
			$('.preview-content').html(content);
			$('#previewModal').modal('show');
		}
		return false;
	});
	
	$('.note-codable').on('change', function(){
		console.log($(this).val());
	});
	
	
	
	$('.delete_simu_module_id').on('click', function(){
		var $self = $(this);
		var id = $self.attr('data-id');
		bootbox.confirm('<p class="lead">'+action_messages.global.generic_delete_message+'</p>', function(e){
			
			if(e){
				$.post(
					base_url+'reseller/simulation/delete_mod',
					{id: id},
					function(response){
						console.log(response);
						$self.closest('tr').addClass('bg-danger').fadeOut();
					}
				);
			}
			
		});
		
	
	});
	
	
	$('.delete_simu_id').on('click', function(){
		var $self = $(this);
		var id = $self.attr('data-id');
		bootbox.confirm('<p class="lead">'+action_messages.global.generic_delete_message+'</p>', function(e){
			
			if(e){
				$.post(
					base_url+'reseller/simulation/delete',
					{id: id},
					function(response){
						console.log(response);
						$self.closest('tr').addClass('bg-danger').fadeOut();
					}
				);
			}
			
		});
	});
	
	$('.update_simu_btn').on('click', function(){
		var name = $(this).attr('data-name');
		var id = $(this).attr('data-id');
		
		
		$('input[name="id"]').val(id);
		$('input[name="name"]').val(name);
		$('#addSimulationModal').modal('show');
	
	});
	
	
	$('.simu_type').on('click', function(){
		var type = $(this).attr('data-type');
		$(this).closest('.circle-tile').addClass('red');
		$(this).closest('.col-sm-4').siblings().find('.circle-tile').removeClass('red');

		$('#filetype').val(type);

		$('.upload_field').addClass('hidden');
		$('.summernote_field').addClass('hidden');
		if(type == 'video'){
			$('.upload_field').removeClass('hidden').find('.text-muted').html('(mp4, mov, wmv)');
		}
		else if( type == 'audio'){
			$('.upload_field').removeClass('hidden').find('.text-muted').html('(mp3, wav)');
		}
		else{
			$('.summernote_field').removeClass('hidden');
		}
		
		return false;
		
	});
	
	
	$('select[name="trigger"]').on('change', function(){
		$('.date_picker').addClass('hidden');
		$('.steps_list').addClass('hidden');
		
		if($(this).val() == 'steps'){
			$('.steps_list').removeClass('hidden');
		}
		else if($(this).val() == 'time'){
			$('.date_picker').removeClass('hidden');
		}
	});
	
	
});
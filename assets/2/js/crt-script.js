	
//display current date in the footer
var thecurrentyear = new Date().getFullYear();
$('.curr_year').html(thecurrentyear);

$(document).ready(function(e) {
	//change nav-bar class
	if($( document ).width() < 751){
		$('.navbar-default').removeClass('navbar-fixed-top').addClass('navbar-static-top');
		$('.main_container').addClass('main_container_xs').removeClass('main_container');
	}else{
		$('.navbar-default').addClass('navbar-fixed-top').removeClass('navbar-static-top');
		$('.main_content_xs').addClass('main_content').removeClass('main_content_xs');
	}
	
	
	"use strict";
	var options = {};
	options.ui = {
		container: "#pwd-container",
		viewports: {
			progress: ".pwstrength_viewport_progress",
			verdict: ".pwstrength_viewport_verdict"
		}
	};
	options.common = {
		onLoad: function () {
			$('#messages').text('Start typing password');
		},
		zxcvbn: true
	};
	$(':password').pwstrength(options);	

	$('.common_msg_click').on('click', function ( e ) { 
		$.fn.custombox( this );
		e.preventDefault();
	});


	if (uri_2 == 'managecase' && uri_3 == 'reportdoc' && uri_5 != ''){
		
		var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close hidden" id="hide_x" data-dismiss="modal" aria-hidden="true">&times;</button><p class="lead" id="hide_spin">';
		
		var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Your file is being analysed.</p><p id="scan_result">';
		
		var myModal3 = '</p></div></div></div> ';
	
		var myModal = myModal1 + myModal2 + myModal3;
		
		$(myModal).modal('show');
		
		var file_id = uri_5;
		$.ajax({
			  type: "POST",
			  
			  url:  base_url+'crt/managecase/scan_response',
			  
			  data: { file_id: file_id },
			  
			  success: function(data) {
				  console.log(uri_5);
				  $('#scan_result').html(data);
				  $('#hide_spin').addClass('hidden');
				  $('#hide_x').removeClass('hidden');
				  
			  }
			  
		});	
		
	}//uri_2 == 'managecase' && uri_3 == 'reportdoc' && uri_5 != '' 



	$('.refresh_btn').click(function(){
		$('.glyphicon-refresh').addClass('fa-spin');	
	});


	var options = { 
		beforeSend: function() 
		{
			$("#progress").show();
			//clear everything
			$("#bar").width('0%');
			$("#message").html("");
			$("#percent").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete) 
		{
			$("#bar").width(percentComplete+'%');
			$("#percent").html(percentComplete+'%');
	
		
		},
		success: function() 
		{
			$("#bar").width('100%');
			$("#percent").html('100%');
	
		},
		complete: function(response) 
		{
			$("#file_up_text").show();
			$("#message").after(response.responseText);
			
			console.log(response.responseText);
		},
		error: function()
		{
			$("#file_up_text").show();
			$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
	
		}
	 
	}; 

	$("#myForm").ajaxForm(options);
	
	
	
	if (uri_2 == 'document' && uri_3 == 'manage' && uri_4 != ''){

		var myModal1 = ' <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close hidden" id="hide_x" data-dismiss="modal" aria-hidden="true">&times;</button><p class="lead" id="hide_spin">';
		
		var myModal2 = '<i class="fa fa-circle-o-notch fa-spin"></i> Your file is being analysed.</p><p id="scan_result">';
		
		var myModal3 = '</p></div></div></div> ';

		var myModal = myModal1 + myModal2 + myModal3;
		
		$(myModal).modal('show');
		
		var file_id = uri_4;
		$.ajax({
			  type: "POST",
			  
			  url: base_url+'crt/document/scan_response',
			  
			  data: { file_id: file_id },
			  
			  success: function(data) {
				  console.log(uri_4);
				  $('#scan_result').html(data);
				  $('#hide_spin').addClass('hidden');
				  $('#hide_x').removeClass('hidden');
				  
			  }
			  
		});
				
	}//uri_2 == 'document' && uri_3 == 'manage' && uri_4 != ''
	
	

	$('#save_new_blocker').click(function(e) {
      	var blocker_name = $('#blocker_name').val();
      	var blocker_issue = $('#blocker_issue').val();
      	var blocker_impact = $('#blocker_impact').val();
		
		$('.blocker_name_error').html('');
		$('.blocker_issue_error').html('');
		$('.blocker_impact_error').html('');
		
		if(blocker_name == ''){
			$('.blocker_name_error').html('Mandatory field.');
			$('#blocker_name').focus();
			return false;
		}
		
		else if(blocker_issue == ''){
			$('.blocker_issue_error').html('Mandatory field.');
			$('#blocker_issue').focus();
			return false;
		}
		
		else if(blocker_impact == ''){
			$('.blocker_impact_error').html('Mandatory field.');
			$('#blocker_impact').focus();
			return false;
		}
		else{
			$.ajax({
				  type: "POST",
				  
				  url: base_url + "crt/recall/save_new_blocker",
				  
				  data: {
					  blocker_name: blocker_name,
					  blocker_issue: blocker_issue,
					  blocker_impact: blocker_impact,
					  recall_id: $('#the_recall_id').val()
				  },
				  
				  success: function(data) {
					  console.log(data);
					  window.location.reload(true);
				  }
			});
		}
    });

});//document.ready





function addTGuide(guide){
	var control = $('#myfile');
	control.replaceWith( control = control.clone( true ) );

	$('#atta_file_btn').show();
	$('.upload_section').hide();
	$('#progress').hide();
	$('#file_up_text').hide();
	$('#file_up_text a').remove();
	$('#file_up_text span').remove();
	if (guide == ''){
		$('#modal_guide_btn').hide();
		$('#modal_guide_holder div').hide();
	}
	else{
		$('#modal_guide_btn').show();
		$('#modal_guide_holder div').show();
	}
	$('#modal_guide_holder div').html(guide);
};



//approvers crt modal
var getCRTApprovers = function(stand_id){
	console.log('aheh');
	$('#assign_approver_form_crt').find('input.standbytask_id').val(stand_id);
	$('#assignApproverModal').find('p.loader').removeClass('hidden');
	$('#assignApproverModal').find('form').addClass('hidden');
	$('#assignApproverModal').find('div.approver_list').addClass('hidden');

	$('#assignApproverModal').modal('show');

	$.post(
		base_url+'cc/recall/getapprovers',
		{stand_id: stand_id},
		function(response){
			console.log(response);
			$('#assign_approver_form_crt').find('input[type="checkbox"]').prop('checked', false);
			$('#assign_approver_form_crt').find('input.approvers_id').val(response.approvers_id);

			$('#assignApproverModal').find('p.loader').addClass('hidden');
			$('#assignApproverModal').find('form').removeClass('hidden');
		},
		'json'
	);
	
};







var currcount;
var currsteplist;
var currtotq;
var oldcount2;
var oldcount3;
var oldcount4;
var oldcount5;
var oldcount6;
var oldcount7;
var oldcount8;

function checkTheCBox(question_id,btn,btnsm,status,chckcount,steplist,totq,proc, assigned_stat){
	console.log($('#'+btn).closest('li').hasClass('disabled'));

	if($('#'+btn).closest('li').hasClass('curr_user_is_approver') == true){
		bootbox.alert('<p class="lead">Please approve this task to close-out.</p>');
		return false;
	}

	if($('#'+btn).closest('li').hasClass('disabled') == true && assigned_stat == 'not_assigned'){
		return false;
	}
	
	if(typeof currsteplist === "undefined"){
		currsteplist = steplist;
		currtotq = parseInt(totq);
	}
	else{
		if (currsteplist != steplist){
			
			//assign old count
			if (currsteplist == 'step_2btn'){
				oldcount2 = currcount;
			}
			
			if (currsteplist == 'step_3btn'){
				oldcount3 = currcount;
			}
			
			if (currsteplist == 'step_4btn'){
				oldcount4 = currcount;
			}
			
			if (currsteplist == 'step_5btn'){
				oldcount5 = currcount;
			}
			
			if (currsteplist == 'step_6btn'){
				oldcount6 = currcount;
			}
			
			if (currsteplist == 'step_7btn'){
				oldcount7 = currcount;
			}
			
			if (currsteplist == 'step_8btn'){
				oldcount8 = currcount;
			}//.assign old count
			
			
			//get old count
			if (steplist == 'step_2btn'){
				currcount = oldcount2;
			}
			
			else if (steplist == 'step_3btn'){
				currcount = oldcount3;
			}
			
			else if (steplist == 'step_4btn'){
				currcount = oldcount4;
			}
			
			else if (steplist == 'step_5btn'){
				currcount = oldcount5;
			}
			
			else if (steplist == 'step_6btn'){
				currcount = oldcount6;
			}
			
			else if (steplist == 'step_7btn'){
				currcount = oldcount7;
			}
			
			else if (steplist == 'step_8btn'){
				currcount = oldcount8;
			}
			else{
				currcount = parseInt(chckcount);
			}//.get old count
			
			currsteplist = steplist;
			currtotq = parseInt(totq);
			
		}
	}

	var chckcount = parseInt(chckcount);
	
	if(status == '0'){
		
		bootbox.confirm("<p class='lead'><i class='fa fa-question-circle text-info'></i> Change task status to completed?</p>", function(r)
		{ 
		
		
			if(r)
			{ 
				
				//$("#"+btn+", #"+btnsm).html('<i class="fa fa-spinner fa-spin text-muted" style="font-size: 165%;"></i>');
				$.ajax({
				  type: "POST",
				  
				  url: base_url+'crt/recall/mark_task_completed',
				  
				  data: { question_id: question_id},
				  
				  dataType: 'json',
				  
				  error: function(err, mess){
						console.log(err);
				  },

				  success: function(data) {
							
						console.log(data);
						if(data.result == 'was_update'){
							$('#refreshPageModal').modal('show');
							return false;
						}
						
						//disable
						$('#'+btn).closest('tr').find('.save_stp3_table').addClass('disabled');
						
						$("#"+btn+", #"+btnsm).fadeOut(500, function(){
							//$("#"+btn+", #"+btnsm).siblings('button').show();
							//$("#"+btn+", #"+btnsm).html('<i class="fa fa-check text-muted" style="font-size: 165%;"></i>');
							
							$("#"+btn+", #"+btnsm).closest('li').siblings().find('a:hidden').show();

							$('#'+btn).closest('tr').find('.task_text_status').removeClass('text-danger text-muted text-warning').addClass(data.status_class).html(data.status_text);
							
							
							if(data.new_task_owner !=''){
								$('#'+btn).closest('tr').find('span.label').removeClass('danger').addClass('orange');
								$('#'+btn).closest('tr').find('span.label').html(data.new_task_owner);
								//$('#'+btn).closest('tr').find('.crt_view_approvers_btn').removeClass('hidden');

								
							}
							
							//check if no approver
							if(data.action != ''){
								console.log(data.action);
								//getCRTApprovers(question_id);
							}
							
						});
						//setTimeout(function() {$("#"+btn).siblings('button').show()},500);
						
						$('#'+btn).closest('tr').find('.date_complete'+question_id).html(data.time_completed);

						$('#'+btn).closest('tr').find('a.btn-sm').fadeOut(500, function(){
							$('#'+btn).closest('tr').find('.date_complete'+question_id).show();
						});

						
						if (typeof currcount === "undefined" || currcount === null){
							currcount = chckcount - 1;
							console.log(currcount);
							
							if(currcount == 0){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-green bg-success');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
								$(proc).show();
								
								$('#'+btn).closest('.tab-pane').find('.simulation_btn').click();
							}
							if(currcount > 0 && currcount != currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-orange bg-warning');
								
								if($('#' + steplist + ' a i.fa-circle-o').length > 0){
									$('#' + steplist + ' a i.fa-circle-o').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
								}
								$(proc).hide();
							}
							if(currcount == currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-red bg-danger');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
								$(proc).hide();
							}
							
						}else{
							
							currcount = currcount - 1;
							console.log(currcount);
							
							
							if(currcount == 0){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-green bg-success');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
								$(proc).show();
								
								$('#'+btn).closest('.tab-pane').find('.simulation_btn').click();
							}
							if(currcount > 0 && currcount != currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-orange bg-warning');
								
								if($('#' + steplist + ' a i.fa-circle-o').length > 0){
									$('#' + steplist + ' a i.fa-circle-o').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
								}
								$(proc).hide();
							}
							if(currcount == currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-red bg-danger');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
								$(proc).hide();
							}
						}
						
				  }
				  
				});	


			}

		});
	}
	else{ 
		bootbox.confirm("<p class='lead'><i class='fa fa-question-circle text-info'></i> Change task status to incomplete?</p>", function(r)
		{ 
			if(r)
			{ 

				//$("#"+btn+", #"+btnsm).html('<i class="fa fa-spinner fa-spin" style="font-size: 165%;"></i>');
				
				
				$.ajax({
				  type: "POST",
				  
				  url: base_url+'crt/recall/mark_task_incomplete',
				  
				  data: { question_id: question_id},
				  
				  dataType: 'json',

				  success: function(data) {
						console.log(data);
						
						if(data.result == 'was_update'){
							$('#refreshPageModal').modal('show');
							return false;
						}

						$("#"+btn+", #"+btnsm).fadeOut(500, function(){
							//$("#"+btn+", #"+btnsm).siblings('button').show();
							//$("#"+btn+", #"+btnsm).html('<i class="fa fa-check" style="font-size: 165%;"></i>');
						
							$("#"+btn+", #"+btnsm).closest('li').siblings().find('a:hidden').show();
						
							$('#'+btn).closest('tr').find('.task_text_status').removeClass('text-danger text-muted text-warning').addClass(data.status_class).html(data.status_text);
							
							
							
							if(data.new_task_owner !=''){
								$('#'+btn).closest('tr').find('span.label').removeClass('danger').addClass('orange');
								$('#'+btn).closest('tr').find('span.label').html(data.new_task_owner);
								
							}
							else{
								$('#'+btn).closest('tr').find('span.label').removeClass('orange').addClass('danger');
								$('#'+btn).closest('tr').find('span.label').html('Not Assigned');
								
							}
							
							
						});
						//setTimeout(function() {$("#"+btn).siblings('button').show()},500);
						

						$('#'+btn).closest('tr').find('.date_complete'+question_id).hide();
						
						//enable
						$('#'+btn).closest('tr').find('.save_stp3_table').removeClass('disabled');
						
						$('#'+btn).closest('tr').find('a.btn-sm').fadeIn(500);
						if (typeof currcount === "undefined" || currcount === null){
							currcount = chckcount + 1;
							console.log(currcount);
							if(currcount == 0){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-green bg-success');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
								$(proc).show();
							}
							if(currcount > 0 && currcount != currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-orange bg-warning');
								
								if($('#' + steplist + ' a i.fa-circle-o').length > 0){
									$('#' + steplist + ' a i.fa-circle-o').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
								}
								
								if($('#' + steplist + ' a i.fa-check-circle').length > 0){
									$('#' + steplist + ' a i.fa-check-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
								}
								$(proc).hide();
							}
							if(currcount == currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-red bg-danger');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
								$(proc).hide();
							}
						}else{
							currcount = currcount + 1;
							console.log(currcount);
							if(currcount == 0){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-green bg-success');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-check-circle fa-fw"></i>');
								$(proc).show();
							}
							if(currcount > 0 && currcount != currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-orange bg-warning');
								
								if($('#' + steplist + ' a i.fa-circle-o').length > 0){
									$('#' + steplist + ' a i.fa-circle-o').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
								}
								
								if($('#' + steplist + ' a i.fa-check-circle').length > 0){
									$('#' + steplist + ' a i.fa-check-circle').remove();
									$('#' + steplist + ' a').prepend('<i class="fa fa-minus-circle fa-fw"></i>');
								}
								$(proc).hide();
							}
							if(currcount == currtotq){
								$('#' + steplist + ' a').removeClass();
								$('#' + steplist + ' a').addClass('text-red bg-danger');
								$('#' + steplist + ' a i.fa-minus-circle').remove();
								$('#' + steplist + ' a').prepend('<i class="fa fa-circle-o fa-fw"></i>');
								$(proc).hide();
							}
						}
							
				  }
				  
				});	



			}

		});
	
	}

}//.checkTheCBox


function delete_the_file(file_id){
	thenum = file_id.replace( /^\D+/g, '');
	//alert(thenum);
	
	//delete the file in server and in table
	$.ajax({
	  type: "POST",
	  
	  url: base_url+'crt/message/delete_message_file',
	  
	  data: { thenum: thenum},
	  
	  success: function(data) {
			
			console.log(data);
			
	  }
	  
	});
	
}//message file



function delete_the_recall_file(file_id){
	thenum = file_id.replace( /^\D+/g, '');
	//alert(thenum);
	
	//delete the file in server and in table
	$.ajax({
	  type: "POST",
	  
	  url: base_url+'crt/recall/delete_message_file',
	  
	  data: { thenum: thenum},
	  
	  success: function(data) {
			
			console.log(data);
			
	  }
	  
	});
	
}//.delete_the_recall_file
		


//vid con window
var myWindow;

function openWin() {
	myWindow = window.open(base_url+'conference/team/'+cc_id+'/'+org_id,'mywindowtitle','width=1100,height=550');
}

function closeWin() {
	if (myWindow) {
		myWindow.close();
		
		var close_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
	
		//store end of session of cc
		$.ajax({
		  type: "POST",
		  
		  url: base_url+'conference/stop_meeting',
		  
		  data: {
			  cc_id: cc_id,
			  org_id: org_id,
			  end_date: close_date
		 }
		  
		});
		
		$('.btn_stopvid').addClass('hidden');
		$('.btn_startvid').removeClass('hidden');
		
	}
}





function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
	}
	return "";
}

function checkCookie() {
	var user = getCookie("nav");
	if (user != "") {
		$('#expand_nav').click();
	} else {
		$('#collapse_nav').click();
	}
	
}	


function eraseCookie(name) {
    setCookie(name,"",-1);
}

function hideNav(){

	//$('#viewdetails').children().toggle();
	if($('.main_nav a i span.mngecasebdg').length > 0){
		$('.main_nav a i span.mngecasebdg').show();
		$('.main_nav a i.mngecase').removeClass('fa-life-ring');
	}
	if($('.main_nav a i span.frmnewsbdg').length > 0){
		$('.main_nav a i span.frmnewsbdg').show();
		$('.main_nav a i.frrmnews').removeClass('fa-flag');
	}
	if($('.main_nav a i span.msgicobgd').length > 0){
		$('.main_nav a i span.msgicobgd').show();
		$('.main_nav a i.msgico').removeClass('fa-envelope');
	}
	

	$('.sidebar_nav').css('width','4%');
	$('.main_content').css('width','96%');

	$('.large_nav').hide();
	$('.slim_nav').show();

	if ($('.sidebar_nav').css('display') == 'none') {
		$('.main_content').css('width','100%');
	}		

	setCookie("nav", "", "-1");
};


function showNav(){

	$('.sidebar_nav').css('width','');
	$('.main_content').css('width','');
	$('.slim_nav').hide();
	$('.large_nav').show();

	$('.main_nav a i span.mngecasebdg').hide();
	$('.main_nav a i.mngecase').addClass('fa-life-ring');
	$('.main_nav a i span.frmnewsbdg').hide();
	$('.main_nav a i.frrmnews').addClass('fa-flag');
	$('.main_nav a i span.msgicobgd').hide();
	$('.main_nav a i.msgico').addClass('fa-envelope');

	if(getCookie("nav") !=""){
	}
	else{
		setCookie("nav", "collapse", "1");
	}
};




function checkCookieChatList() {
	var hidechat = getCookie("chat_list");
	if (hidechat != "") {
		$('#show_chat').click();
	} else {
		$('#page-wrap button').click();
	}
	
}	

function showChatList(){

	if(getCookie("chat_list") !=""){
	}
	else{
		setCookie("chat_list", "block", "1");
	}

};

function hideChatList(){

	setCookie("chat_list", "", "-1");

};


//chat pm
function checkCookieChatPm() {
	var hidechat = getCookie("chat_pm");
	
	var initt = getCookie("initiator");
	var recee = getCookie("receiver");
	var dispp = getCookie("disp_name");
	
	createChatroom(dispp,initt,recee);
	
	
	if (hidechat != "") {
		$('#show_chat_pm').click();
	} else {
		$('#page-wrap-pm button').click();
	}
	
	
}	

function showChatPm(){

	if(getCookie("chat_pm") !=""){
	}
	else{
		setCookie("chat_pm", "block", "1");
	}

};

function hideChatPm(){

	setCookie("chat_pm", "", "-1");

};

function removeChatPm(){

	setCookie("remove_im", "", "-1");

};


$(document).ready(function(e) {

	$( '.navigation li' ).find('.fa-angle-double-right').hide();
	//$( '.navigation li.active' ).find('.fa-angle-double-right').show();
	
	$( '.navigation li' ).click(function () {
		$( this ).find('.fa-angle-double-right').slideDown( "slow" );
		$( this ).siblings().find('.fa-angle-double-right').hide( "slow" );
		
	});	
	
	

	//click event for main nav
	$('#collapse_nav').click(function(e) {
		hideNav();
		$(this).hide();
		$('#expand_nav').show();
	});
	
	$('#expand_nav').click(function(e) {
		showNav();
		$(this).hide();
		$('#collapse_nav').show();
	});
		
	
	//click event for chat list
	$('#page-wrap button').click(function(e) {
		hideChatList();
	});
	
	$('#show_chat').click(function(e) {
		showChatList();
		$(this).hide();
	});
	
	//click event for chat pm
	$('#page-wrap-pm button').click(function(e) {
		hideChatPm();
	});
	
	$('#show_chat_pm').click(function(e) {
		showChatPm();
		$('#this').hide();
	});
	
	
	//activate toggle nav
	checkCookie();
	
	//activate toggle chat list
	checkCookieChatList();
	
	
	//activate toggle chat pm
	if(getCookie("remove_im") !=""){
		checkCookieChatPm();
	}
   
});//document.ready





function recallAssign(recall_id,question_id,task_answer,crtname,task_assigned){
		$('#the_recall_id').val(recall_id);
		$('#the_question_id').val(question_id);
		//$('#txt_message').val(task_answer);
		$('#pickup_crtname').val(crtname);
		$('#the_current_assigned').val(task_assigned);
}



function getRespond(task_id){
	$('#recallRespond').find('.modal-body').hide();
	$('#recallRespond').find('.modal-content').prepend('<div class="spin-loader text-center text-muted" style="padding: 50px;"><i class="fa fa-spinner fa-spin fa-2x"></i><br/>Loading content..</div>');
	
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'crt/recall/get_respond_to_edit',
		  
		  data: {
			  task_id: task_id
		  },
		  
		  success: function(data) {
			// console.log(data);
			$('#recallRespond').find('.modal-body').show();
			$('#txt_message').val(data);
			$('#recallRespond').find('div.spin-loader').remove();

		  }
	});

}//end getRespond
	

$('#assign_recall').click(function(e) {
	
	var crtname = $('#pickup_crtname').val();
	var tsk_id = $('#the_question_id').val();
	var curr_assigned = $('#the_current_assigned').val();

	if ($('#message_receiver').val() == ''){
		$('.err_holder').html('Mandatory field');
		return false;
	}
	else{

		$.ajax({
		  type: "POST",
		  
		  url: base_url+'crt/recall/assign_the_crt',
		  
		  data: {
			  question_id: $('#the_question_id').val(),
			  curr_assigned: curr_assigned,
			  message_receiver: $('#message_receiver').val()
		  },
		  
		  success: function(data) {
				
				console.log(data);
				$('#recallModal').modal('hide');

				if(data == 'was_update'){
					$('#refreshPageModal').modal('show');
					return false;
				}
					
				
				
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('.save_stp3_table').removeClass('hidden');
				
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('span.na_label').removeClass('danger').addClass('orange');
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('span.na_label').html(crtname);
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('a.btn').show();
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('button.disabled').removeClass('disabled');
				
				
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('li.disabled').show();
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('li.disabled').removeClass('disabled');
				
				$('#pickup_btn_sm'+tsk_id).closest('tr').find('.crt_view_approvers_btn').attr('data-assigned','yes');

				
				//filter again after assigning crt
				//$('#pickup_btn_sm'+tsk_id).closest('table').find('input[data-action="filter"]').change();
				
				$('#pickup_btn_sm'+tsk_id).hide();
				$('#pickup_btn_xs'+tsk_id).hide();
				$('#pickup_btn_md'+tsk_id).hide();
		  }
		  
		});
	}
	
});//.assign recall

function getRCTaskRespond(task_id){
	$('#respondtask_holder').html('<div class="text-center text-muted" style="padding: 50px;"><i class="fa fa-spinner fa-spin fa-2x"></i><br/>Loading content..</div>');
	$.ajax({
		  type: "POST",
		  
		  url: base_url + 'crt/recall/get_task_response',
		  
		  data: {
			  task_id: task_id
		  },
		  
		  success: function(data) {
			 //console.log(data);
			$('#respondtask_holder').html(data);

		  }
	});

}//getRCTaskResponds

function completeTask(recall_id,question_id,uri_sgmt_4,uri_sgmt_5){


	$.ajax({
	  type: "POST",
	  
	  url: base_url+'crt/recall/mark_task_completed',
	  
	  data: { question_id: question_id},
	  
	  success: function(data) {
			
			console.log(data);
			//the_act;
			//the_act2;
			//the_act3;

			if (uri_sgmt_4 == 'r'){

				window.location.reload(true);
			}
			else{

				window.location.reload(true);
			}
		
	  }
	  
	});

};



$(document).ready(function(e) {
    
	$('#save_message_rc').click(function(e) {
        
		var tsk_id = $('#the_question_id').val();
		if ($('#txt_message').val() == ''){
			$('.text_error').html('Mandatory field');
			return false;
		}
		else{

			$.ajax({
			  type: "POST",
			  
			  url: base_url+'crt/recall/add_message_respond',
			  
			  data: {
				  question_id: tsk_id,
				  txt_message: $('#txt_message').val()
			  },
			  
			  success: function(data) {
					
					console.log(data);
					
					$('.text_error').html('');

					$('#recallRespond').modal('hide');
					$('#comptask_btn'+tsk_id).closest('tr').find('#show_rc_respond_btn').show();
					$('#comptask_btn'+tsk_id).closest('tr').find('#tsk_not_strted_holder').hide();
						
			  }
			  
			});
		}
		
    });//assign recall
	
	

	
	//display save button when start to answer stp3 table
	$('.thumbnail.no-margin-btm').click(function(){
		
		if($( '.no-margin-btm.selected' ).length > 0){
			$('.save_stp3_table').show();
			//$('.save_stp3_table').removeClass('hidden');
		}
	
	});

	//disabling clicking of boxes in table
	
	$('.thumbnail.no-margin-btm').click(function(e){
		if($('.save_stp3_table').hasClass('hidden')){
			e.stopImmediatePropagation();
			//bootbox.alert("<p class='lead'><i class='fa fa-info-circle text-info'></i> This task is already completed.</p>");
			return false;
		}
	});
		
	$('.save_stp3_table').click(function(){
		
		var step_3_table_id = $("input[name='step_3_table_id']").val();
		var severity = $("input[name='severity']:checked").val();
		var presence = $("input[name='presence']:checked").val();
		var like_dist = $("input[name='like_dist']:checked").val();
		var distribution = $("input[name='distribution']:checked").val();
		var identification = $("input[name='identification']:checked").val();
		var hazard = $("input[name='hazard']:checked").val();
		


		if(!severity){
			
			$('.stp3_remaining_items').html('Select Severity');
			return false
		}
		else if(!presence){
			
			$('.stp3_remaining_items').html('Select Presence');
			return false
		}
		else if(!like_dist){
			
			$('.stp3_remaining_items').html('Select Likely Injury');
			return false
		}
		else if(!distribution){
			
			$('.stp3_remaining_items').html('Select Distribution');
			return false
		}
		else if(!identification){
			
			$('.stp3_remaining_items').html('Select Identification');
			return false
		}
		else if(!hazard){
						
			$('.stp3_remaining_items').html('Select Hazard');
			return false
		}
		else{
			$('.stp3_remaining_items').html('<i class="fa fa-spinner fa-spin text-muted"></i> <span class="text-muted">Saving..</span>');
			console.log('ow yeah');
			

			$.ajax({
			  type: "POST",
			  
			  url: base_url+'crt/recall/save_stp3_table',
			  
			  data: {
				  severity: severity,
				  presence: presence,
				  like_dist: like_dist,
				  distribution: distribution,
				  identification: identification,
				  hazard: hazard,
				  step_3_table_id: step_3_table_id
			  },
			  
			  success: function(data) {
					
					console.log(data);

					
					$('.stp3_remaining_items').html('<i class="fa fa-check-circle text-success"></i> <span class="text-success">Saved</span>');
					
				  setTimeout(function(){ 
					$('.stp3_remaining_items').html('');
				  }, 2000 ); 					
			  }
			  
			});			
			
		}
			
	});
	//display save button when start to answer stp3 table
		
	
	//activate last visited tab
	$('.tasktab_loader').hide();
	if(location.hash) {
	   $('a[href=' + location.hash + ']').tab('show');
	   help_page();
	}
	else{
		$('.steptab_active').addClass('active');
				
		if($('.kpi_menu_tab').length > 0){
			$('.navigation').children().first().addClass('active');
		}
		
		help_page();
	}

	$(document.body).on("click", "a[data-toggle]", function(event) {
	  if (this.getAttribute("href") !='#'){
		location.hash = this.getAttribute("href");
		help_page();
	  }
	});
			
});//document.ready


$(window).on('popstate', function() {
	var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
	$('a[href=' + anchor + ']').tab('show');
});




/*chat*/
var name = user_full_name;
var org = "";
var senderr = "";
var receiverr = "";
var time_sent = now_time;

// default name is 'Guest'
if (!name || name === ' ') {
   name = "Guest";	
}

// strip tags
name = name.replace(/(<([^>]+)>)/ig,"");

// display name on page
//$("#name-area").html("You are: <span>" + name + "</span>");

// kick off chat
var chat =  new Chat();


$(document).ready(function(e) {
	
	// watch textarea for key presses
	$("#sendie").keydown(function(event) {  
		
		var key = event.which;  
		//all keys including return.  
		if (key >= 33) {
			 var maxLength = $(this).attr("maxlength");  
			 var length = this.value.length;  
			 // don't allow new content if length is maxed out
			 if (length >= maxLength) {  
				 event.preventDefault();  
			 }  
		 }  
	});
	
	// watch textarea for release of key press
	$('#sendie').keyup(function(e) {	
					 
		if (e.keyCode == 13) { 
			var text = $(this).val();
			var maxLength = $(this).attr("maxlength");  
			var length = text.length; 
			 
			// send 
			if (length <= maxLength + 1) { 
				chat.send(text, name, time_sent, org, senderr, receiverr);	
				$(this).val("");
			}
			else {
				$(this).val(text.substring(0, maxLength));
			}	
		}
	});		
	
	//load the online list automatically
	$.ajax({
		url: base_url+'crt/conference/onlines',
		cache: false,
		success: function(data){
			$("#results").html(data);
		}
	});

	
	//refresh list of online
	var refreshIntervalId = setInterval(function(){
		
		$.ajax({
			url: base_url+'crt/conference/onlines',
			cache: false,
			success: function(data){

				//refresh on session expires
				if (data == '<li><span class="text-muted"><i class="fa fa-warning"></i> Your session expired. Please login again to continue.</span></li>'){
					$('.modal').modal('hide');
					bootbox.alert('<p class="lead"><i class="fa fa-info-circle text-info"></i> Your session expired. Please login again to continue.</p>', function(){
						window.location.reload(true);
					});
					clearInterval(refreshIntervalId);
				}
				
				$("#results").html(data);

			}
		});

	}, 4000);
	
	refreshIntervalId;



	$('#coll_count_unread').hide();
	//refresh list of online
	var check_unread_count = parseInt('0');
	setInterval(function(){
		
		//number of online
		var count = $("#results").children('li.ol').length;

		if (count > 1){
			
			$('#count_online').html('There are ' + count + ' online users');
			$('#number_online').html(count);
			
			var total_unread = parseInt($('#total_unread').html());
			
			if(total_unread != '0'){
				$('#coll_count_unread').show();
				$('#unread_number').html(total_unread);
				
				if(total_unread > check_unread_count){
					document.getElementById('audiotag1').play();
					check_unread_count = total_unread;
				}
			}
			else{
				$('#coll_count_unread').hide();
				check_unread_count = parseInt('0');
			}
		}
		else{
			$('#count_online').html('There is ' + count + ' online user');
			$('#number_online').html(count);
			
			var total_unread = parseInt($('#total_unread').html());
			
			if(total_unread != '0'){
				$('#coll_count_unread').show();
				$('#unread_number').html(total_unread);
				
				if(total_unread > check_unread_count){
					document.getElementById('audiotag1').play();
					check_unread_count = total_unread;
				}
			}
			else{
				$('#coll_count_unread').hide();
				check_unread_count = parseInt('0');
			}
		}					
		
		
	}, 1000);
	

});//document.ready




function changeReiceverCookie(disp_name,initiator,receiver){
	
	if(getCookie("initiator") !=""){
		setCookie("initiator", "", "-1");
		setCookie("initiator", initiator, "1");
	}
	else{
		setCookie("initiator", initiator, "1");
	}
	
	if(getCookie("receiver") !=""){
		setCookie("receiver", "", "-1");
		setCookie("receiver", receiver, "1");
	}
	else{
		setCookie("receiver", receiver, "1");
	}
	
	if(getCookie("disp_name") !=""){
		setCookie("disp_name", "", "-1");
		setCookie("disp_name", disp_name, "1");
	}
	else{
		setCookie("disp_name", disp_name, "1");
	}
	
}

function createChatroom(disp_name,initiator,receiver){
	
	changeReiceverCookie(disp_name,initiator,receiver);
	
	$.ajax({
		  type: "POST",
		  
		  url: base_url+'crt/conference/have_read',
		  
		  data: {
			  initiator: initiator,
			  receiver: receiver
		  },
		  
		  success: function(data) {
			  
			console.log(data);

		  }
		  
	});
	
	
	//console.log(receiver);
	var sender = parseInt(initiator);
	var receive = parseInt(receiver);
	
	if(sender > receive){
		var the_im_room = initiator + "-" + receiver;
	}
	else{
		var the_im_room = receiver + "-" + initiator;
	}
	//alert(the_im_room);
	org = '';
	org = 'im_'+the_im_room; //echo $txt_file[0]['chat_id']
	receiverr = '';
	senderr = '';
	receiverr = receiver;
	senderr = initiator;
	//org.push(the_im_room);
	
	startChat();	
	
	//set cookie for chat pm
	showChatPm();
	
	if(getCookie("remove_im") !=""){
	}
	else{
		setCookie("remove_im", "block", "1");
	}
	

	$('#sendie').val('');
	$('#chat_username_top').html(disp_name);
	$('#chat_username_bottom').html(disp_name);
	$('#page-wrap-pm').show();
	$('#chat-area').html('');
	
	if($('#chat-area').html() == ''){

			$('#chat-area').html('<div class="text-center text-muted start_con">Start conversation</div><div class="text-center loading_mess" style="padding-top: 30px"><i class="fa fa-spinner text-muted fa-spin"></i></div>');
			//removeLoadingMess();
	}

	
}

function removeLoadingMess(){
	$('.loading_mess').delay(500).hide();
}

function startChat(){

	chat.getState(); 
	setInterval('chat.update()', 400);
	
}
	


$(document).ready(function(e) {
    
	//check if selected message
	$('input.selectedId').click(function(e) {
		if($('input[name="selectedId[]"]:checked').length > 0){
			$('#addtotrash').removeClass('disabled');
			
		}else{
			$('#addtotrash').addClass('disabled');
		}
	});
	
	$('#selectall').click(function(e) {
		if($('input[name="selectedId[]"]:checked').length > 0){
			$('#addtotrash').removeClass('disabled');
			
		}else{
			$('#addtotrash').addClass('disabled');
		}
	});
	
	//submit the form
	$( "#addtotrash" ).click(function() {
		$(this).closest('form').submit();
	});	
	
	if(uri_2 == 'costmonitor' && uri_3 == '' || (uri_2 == 'costmonitor' && uri_3 == 'index' && uri_4 == '')){
		$('#myRecalls').modal('show');
	}
	
	
	help_page();
	
});//document.ready


var help_page = function(){
	//check admin help page content
	var kpi_menu_active = $('.kpi_menu_tab.active').length;
	var costmon_menu_active = $('.costmon_menu_tab.active').length;
	var blockers_menu_active = $('.blockers_menu_tab.active').length;
	
	var helpdata = {
		uri_2: uri_2,
		uri_3: uri_3,
		uri_4: uri_4,
		uri_5: uri_5,
		uri_6: uri_6,
		kpi_menu_active: kpi_menu_active,
		costmon_menu_active: costmon_menu_active,
		blockers_menu_active: blockers_menu_active
	};

	$.post(
		base_url+'cc/auditlog/admin_help_content',
		$.param(helpdata),
		function(res){
			console.log(res);
			
			if(res != ''){
				
				//show help btn and add help content
				$('.admin_help_btn').removeClass('hidden');
				$('#adminHelpModal').find('.well').html(res).show().siblings('p').remove();
			}
			else{
				$('.admin_help_btn').addClass('hidden');
			}

		}
		
	).error(function(err){
		console.log(err);
	});
	
};


//filter task button
function filterTasks(step_no,crt_name,this_btn){
	console.log(this_btn);
	$('#recall-step-filter'+step_no).val(crt_name);
	$('#recall-step-filter'+step_no).change();
	$(this_btn).closest('li').addClass('active');
	$(this_btn).closest('li').siblings().removeClass('active');
	
	$('label'+this_btn).removeClass('btn-default');
	$('label'+this_btn).addClass('btn-primary');
	$('label'+this_btn).siblings().removeClass('btn-primary');
	$('label'+this_btn).siblings().addClass('btn-default');
}



function showCostSummary(id){
	$('#viewCategorySummary .modal-body').html('<h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>');
	$('#viewCategorySummary .modal-body').load(base_url+"crt/costmonitor/get_summary_details/"+id,function(e) {
		console.log(id);
	});
}

function viewCostItemDetails(id){
	$.ajax({
	  type: "POST",
	  
	  url: base_url+'crt/costmonitor/get_item_details',
	  
	  data: {
		  id:id
	  },
	  
	  success: function(data) {
		  
			console.log(data);
			$('#viewCostItem .modal-body').html(data);
	  }
	  
	});			
}



var action_messages = '';

$(document).ready(function(e) {
	$.post(
		base_url+'cc/recall/action_messages',
		{},
		function(res){
			console.log(res);
			action_messages = res;
		},
		'json'
	).error(function(err){
		console.log(err);
	});
});


//spinner loader
$(function() {

    // Action on Click
    $( ".preloadThis" ).click(function() {

        $.isLoading({ text: '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' });
        
        // Setup Loading plugin
        //$( "#load-overlay .demo p" ).removeClass("alert-success");

        // Re-enabling event
        setTimeout( function(){ 
            $.isLoading( "hide" );
        }, 5000 );

    });
	
    // Action on Click
    $( ".preloadThis1x" ).click(function() {

        $.isLoading({ text: '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' });
        
        // Setup Loading plugin
        //$( "#load-overlay .demo p" ).removeClass("alert-success");

        // Re-enabling event
        setTimeout( function(){ 
            $.isLoading( "hide" );
        }, 300 );

    });

});

//Panel Icon Toggle
$(".panel-widgets .fa-chevron-down, .panel-widgets .fa-chevron-up").click(function() {
    $(this).toggleClass("fa-chevron-down fa-chevron-up");
});


//toggle caret on the sidebar
$(".toggleCaret").click(function() {

    $(this).find('.fa.pull-right').toggleClass("fa-caret-up fa-caret-down");
});
$(".toggleCaretSet").click(function() {
    $(this).find('.fa.pull-right').toggleClass("fa-caret-up fa-caret-down");
//    $(".toggleCaretSet .fa-caret-down, .toggleCaretSet .fa-caret-up").toggleClass("fa-caret-down fa-caret-up");
//    $(".toggleCaret .fa-caret-up").toggleClass("fa-caret-up fa-caret-down");
});

if($('.preincidentItems').css('display') == 'block'){
    $(".toggleCaret .fa-caret-down").toggleClass("fa-caret-down fa-caret-up");
}
	
if($('.settings_nav').css('display') == 'block'){
    $(".toggleCaretSet .fa-caret-down").toggleClass("fa-caret-down fa-caret-up");
}
	
//DataTables Initialization
$(document).ready(function() {
	$('#example-table').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true
	 });
	$('.myexample-table').dataTable();
    $('#stake-table').dataTable();
    $('#docs-table').dataTable();
    $('#pre-incident-table').dataTable();
    $('#response-inis-table').dataTable();
    $('#response-table').dataTable();
    $('#standby-table').dataTable();
	
	$('.table-datatable').dataTable();
	$('.table-dt-sort').dataTable({
		'searching': false,
		'bLengthChange': false
		// 'pageLength': false
		// 'bPaginate': false
	});
});


//Activates Bootstrap Tooltips/Popovers in Example
$(function() {

    // tooltip demo
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

    $('.tooltip-test').tooltip();
    $('.popover-test').popover();

    // popover demo
    $("[data-toggle=popover]")
        .popover({html:true});
		

    $("[data-toggle=popover]").focusout(function(e) {
        $(this).popover('hide')
    });

})

//clickableRow table
jQuery(document).ready(function($) {
    $(".clickableRow").click(function() {
        window.document.location = "#";
    });
});

$(document).ready(function() {
	//admin help function
	$('.admin_help_btn').on('click', function(){
		//$('#adminHelpModal').find('.well').hide().after('<p class="lead text-center text-muted"><i class="fa fa-spinner fa-spin"></i><br>Loading..</p>');

		$('#adminHelpModal').modal('show');
		return false;
	});

	
	
    $('#selectall').click(function() {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function() {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
	

	$("[type=submit]").click(function() {
		var $btn = $(this);
		$btn.button('loading');
		// simulating a timeout
		setTimeout(function () {
			$btn.button('reset');
		}, 3000);
	});
		
		
	//radiobutton 
	
//	$('.no-margin-btm input:radio').addClass('input_hidden');
	$('.thumbnail.no-margin-btm').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});	
	
	
	//updated menu
	$(document).on('click','.sidemenu_btn', function(){
		$self = $(this);
		if(!$self.closest('.dropdown').hasClass('open')){
			$self.find('i').addClass('fa-spin');
			setTimeout(function(){
				$self.find('i').removeClass('fa-spin').toggleClass('fa-bars fa-times');
				$self.toggleClass('active');
				$self.closest('.dropdown').toggleClass('open');
			}, 250);
		}
		else{
			$('.sidemenu_btn').removeClass('active').closest('.dropdown').removeClass('open');
			$('.sidemenu_btn').find('i').removeClass('fa-spin fa-times').addClass('fa-bars');
		}
	});

	$('[data-toggle="dropdown"]').on('click', function(){
		$('.sidemenu_btn').removeClass('active').closest('.dropdown').removeClass('open');
		$('.sidemenu_btn').find('i').removeClass('fa-spin fa-times').addClass('fa-bars');
	});
	
	var megaclone = $('.mega-dropdown').clone();
	$('.megamenu_clone').html(megaclone);
	
	
	$('html').click(function() {
		setTimeout(function(){
			$('.sidemenu_btn').removeClass('active').closest('.dropdown').removeClass('open');
			$('.sidemenu_btn').find('i').removeClass('fa-spin fa-times').addClass('fa-bars');
		}, 251);
	});	
});

/**toggle edit standby message*/
$(function() {
    $("#btn_edit_stmes").click(function() {
		
		if ($("#initiate_standbymsg").attr('disabled')){
			$("#initiate_standbymsg").removeAttr('disabled');
			 $("#btn_edit_stmes").html('Confirm Update');
		}
		else {
			// $("#initiate_standbymsg").attr('disabled', 'disabled');
			var $btn = $(this);
			$btn.button('loading');
			// simulating a timeout
			setTimeout(function () {
				$btn.button('reset');
				$("#btn_edit_stmes").html('<i class="fa fa-check"></i> Message Updated');
				$("#btn_edit_stmes").removeClass('btn-primary');
				$("#btn_edit_stmes").addClass('btn-success');
			}, 2000);
			setTimeout(function () {
				$("#btn_edit_stmes").html('Confirm Update');
				$("#btn_edit_stmes").addClass('btn-primary');
				$("#btn_edit_stmes").removeClass('btn-success');
			}, 4000);
			$("#btn_edit_stmes").html('Updating message..');
		}
		
    });
});



/**************************filter table***********************************/

/**
*		<input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Developers" />
*		$(input-element).filterTable()
*		
*	The important attributes are 'data-action="filter"' and 'data-filters="#table-selector"'
*/
(function(){
    'use strict';
	var $ = jQuery;
	$.fn.extend({
		filterTable: function(){
			return this.each(function(){
				$(this).on('change', function(e){ //keyup
					$('.filterTable_no_results').remove();
					var $this = $(this), search = $this.val().toLowerCase(), target = $this.attr('data-filters'), $target = $(target), $rows = $target.find('tbody tr');
					if(search == '') {
						$rows.show(); 
					} else {
						$rows.each(function(){
							var $this = $(this);
							$this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
						})
						if($target.find('tbody tr:visible').size() === 0) {
							var col_count = $target.find('tr').first().find('th').size();
							var no_results = $('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No results found</td></tr>')
							$target.find('tbody').append(no_results);
						}
					}
				});
			});
		}
	});
	$('[data-action="filter"]').filterTable();
})(jQuery);

$(function(){
    // attach table filter plugin to inputs
	$('[data-action="filter"]').filterTable();
	
	$('.container').on('click', '.panel-heading span.filter', function(e){
		var $this = $(this), 
				$panel = $this.parents('.panel');
		
		$panel.find('.panel-body').slideToggle();
		if($this.css('display') != 'none') {
			$panel.find('.panel-body input').focus();
		}
	});
	$('[data-toggle="tooltip"]').tooltip();
});
// JavaScript Document

$(document).ready(function(e) {
	
	$('[name="country_code"]').on('change', function(){
		var ccode = $(this).find(':selected').attr('data-code');
		$('#addon-shortcode').html('+'+ccode);
	});

	//get country code
	$('.user_geolocation').geocomplete({ details: "form#trial_form" })
	.bind("geocode:result", function(event, result){

		var $form = $('#trial_form');
		var country_short = $form.find('[name="country_short"]').val();
		
		console.log(result);
		$.ajax({
			  type: "POST",
			  
			  url: base_url+"frontpage/anewskin/show_country_code",
			  
			  data: { country_short: country_short },
			  
			  dataType: 'json',
			  
			  success: function(data) {
				console.log(data);

				$('#addon-shortcode').html('+'+data.calling_code);
				$form.find('[name="country_code"]').val(data.country_id);
				//$('[name="country_short"]').val(data);

			  },
			  error: function(err){
				  console.log(err);
			  }
		});
		
	});

		
			
	//trial_form
	$('#trial_form').validate({
		rules: {
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			address: {
				required: true
			},
			organization: {
				required: true
			},
			plan: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
			
		errorPlacement: function(error, element) {
			$(element).closest('.form-group').append(error);
		},	
			
		submitHandler: function(form) {
			$(form).find('button').html('Processing..').prop('disabled', true);
			
			var data = {
				calling_code: $('[name="country_code"]').find(':selected').data('code'),
				form_data: $(form).serializeArray()
			};
			
			data = $(form).serialize() +'&'+ $.param(data);
			console.log(data);

			$.post(
				base_url+'frontpage/anewskin/trial_customer',
				data, //$(form).serialize(),
				function(res){
					console.log(res);
					if(res.result == 'ok'){
						$(form).find('.trial_message').hide().removeClass('alert-danger').addClass('alert-success').html('Yeah');
						
//						bootbox.alert('<p class="lead"><i class="fa fa-check-circle text-success"></i> Account successfully created. We\'ll send you an email shortly with a link to activate your account.</p>');
						$(form).find('button').html('Success').prop('disabled', true);
						$('.modal').modal('hide');
						$('.panel-heading').hide();
						$('.panel-body').html('<div class="alert alert-success"><i class="fa fa-check-circle text-success"></i> Account successfully created.</div>');
					}
					else{
						$(form).find('button').html('Signup').prop('disabled', false);
						$(form).find('.trial_message').show().removeClass('alert-success').addClass('alert-danger').html('Coordinator\'s email already registered.');
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});


		}
	});
				
				
});//document.ready
function getBrowser() {
    var n,v,t,u,ua = navigator.userAgent;
    //Non supported versions and bellow.
    var vsmin={i:7.99,f:8.99,o:12,s:5.99,n:10.99,c:23};
    var names={i:'Internet Explorer',f:'Firefox',o:'Opera',s:'Apple Safari',n:'Netscape Navigator', c:"Chrome", x:"Other"};
    if (/bot|googlebot|facebook|slurp|wii|silk|blackberry|mediapartners|adsbot|silk|android|phone|bingbot|google web preview|like firefox|chromeframe|seamonkey|opera mini|min|meego|netfront|moblin|maemo|arora|camino|flot|k-meleon|fennec|kazehakase|galeon|android|mobile|iphone|ipod|ipad|epiphany|rekonq|symbian|webos/i.test(ua)) n="x";
    else if (/Trident.*rv:(\d+\.\d+)/i.test(ua)) n="i";
    else if (/Trident.(\d+\.\d+)/i.test(ua)) n="io";
    else if (/MSIE.(\d+\.\d+)/i.test(ua)) n="i";
    else if (/OPR.(\d+\.\d+)/i.test(ua)) n="o";
    else if (/Chrome.(\d+\.\d+)/i.test(ua)) n="c";
    else if (/Firefox.(\d+\.\d+)/i.test(ua)) n="f";
    else if (/Version.(\d+.\d+).{0,10}Safari/i.test(ua))	n="s";
    else if (/Safari.(\d+)/i.test(ua)) n="so";
    else if (/Opera.*Version.(\d+\.\d+)/i.test(ua)) n="o";
    else if (/Opera.(\d+\.?\d+)/i.test(ua)) n="o";
    else if (/Netscape.(\d+)/i.test(ua)) n="n";
    else return {n:"x",v:0,t:names[n],u:true};
    
    //do not notify ver old systems since their is no up-to-date browser available
    if (/windows.nt.5.0|windows.nt.4.0|windows.98|os x 10.4|os x 10.5|os x 10.3|os x 10.2/.test(ua)) n="x";
    
    //do not notify firefox ESR
    if (n=="f" && v==24)
        n="x";
    //do not notify opera 12 on linux since it is the latest version
    if (/linux|x11|unix|bsd/.test(ua) && n=="o" && v>12) 
        n="x";
    
    if (n=="x") return {n:"x",v:0,t:names[n]};
    
    v=new Number(RegExp.$1);
    if (n=="so") {
        v=((v<100) && 1.0) || ((v<130) && 1.2) || ((v<320) && 1.3) || ((v<520) && 2.0) || ((v<524) && 3.0) || ((v<526) && 3.2) ||4.0;
        n="s";
    }
    if (n=="i" && v==7 && window.XDomainRequest) {
        v=8;
    }
    if (n=="io") {
        n="i";
        if (v>6) v=11;
        else if (v>5) v=10;
        else if (v>4) v=9;
        else if (v>3.1) v=8;
        else if (v>3) v=7;
        else v=9;
    }	
    u=Number(vsmin[n])>=v||n=="x";
    //n:browser i:Internet Explorer,f:Firefox,o:Opera,s:Apple Safari,n:Netscape Navigator, c:Chrome, x:Other
    //v:version
    //t:text ex. Chrome 33
    //c:cookies enabled
    //u:requires update
    return {n:n,v:v,t:names[n]+" "+v,u:u};
}	

function cookiesEnabled() {
	c=(navigator.cookieEnabled) ? true : false;
    if (typeof navigator.cookieEnabled == "undefined" && !c)
    { 
        document.cookie="epacttcookie";
        c = (document.cookie.indexOf("epacttcookie") != -1) ? true : false;
    }
    return c;
}

var browser = getBrowser();

if (browser.u)
{
	document.getElementById("browser-detection").style.display = "block";
}
if (!cookiesEnabled())
{
	document.getElementById("cookies-detection").style.display = "block";
}
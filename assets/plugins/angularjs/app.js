// Define the `crisisApp` module
var crisisApp = angular.module('crisisApp', ['ngQuill', 'ui.bootstrap', 'ui.sortable', 'summernote', 'ui.bootstrap.dialogs', 'angularFileUpload', 'dialogs.main', 'dndLists', 'angular.vertilize', 'angularMoment', 'ngToast', 'ang-drag-drop']);

Object.toparams = function ObjecttoParams(obj) {
  var p = [];
  for (var key in obj) {
    p.push(key + '=' + encodeURIComponent(obj[key]));
  }
  return p.join('&');
};

crisisApp.config(['ngToastProvider', function (ngToast) {
  ngToast.configure({
    //   verticalPosition: 'bottom',
    horizontalPosition: 'center',
    maxNumber: 1
  });
}]);

crisisApp.config(['ngQuillConfigProvider', function (ngQuillConfigProvider) {
  ngQuillConfigProvider.set(null, null, 'custom placeholder')
}]);

function myHttpService($http) {
  this.url = window.base_url;
  this.get = function (link = 'header_tasks') {
    console.log(this.url);
    return $http.get(this.url + 'cc/standbytasks/' + link);
  }

  this.getWithParams = function (link = 'header_tasks', data = {}) {
    console.log(this.url);
    return $http.get(this.url + 'cc/standbytasks/' + link, {
      params: data
    });
  }



  this.post = function (data, db) {
    // console.log('data', data, db);

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    var d = Object.toparams(data);
    return $http.post(this.url + 'cc/standbytasks/' + db, d); //Object.toparams(data));
  }
}

crisisApp.service('myHttpService', myHttpService);



// Define the `headerTasksController` controller on the `crisisApp` module
crisisApp.controller('headerTasksController', function headerTasksController($scope, myHttpService, $timeout, $compile, $sce, $q, $filter, $dialogConfirm, dialogs, ngToast) {



  var self = this;
  $scope.headerTasks = [];
  $scope.headerKanban = [];
  $scope.incident = null;
  $scope.incidents = [];
  $scope.incident_id = window.uri_4;
  $scope.page_type = window.uri_3;
  $scope.edit_mode = false;
  $scope.template_id = (window.uri_5) ? window.uri_5 : '18';
  $scope.logged_admin = window.uri_1;
  $scope.templates = [];
  $scope.activeHeader = 0;
  $scope.tasksOpen = true;
  $scope.activeTask = 0;
  $scope.activeTaskIndex = 0;
  $scope.activeTaskInfo = {};
  $scope.contents = [];
  $scope.popoverOpen = false;
  $scope.loading = false;
  $scope.saveStandbyShown = false;
  $scope.pages = [];
  $scope.crts = [];
  $scope.isAssigned = null;
  $scope.crtSelected = null;
  $scope.viewListMode = true;
  $scope.changesCounter = 0;
  $scope.uri_4 = window.uri_4;
  $scope.isFreePlan = isUserPlanFree();


  $scope.myInfo = null;

  $scope.getMyInfo = function (refocus = true) {
    myHttpService.get('logged_info')
      .then(function (res) {
        $scope.myInfo = res.data; //.message;
        $scope.formLink = base_url + 'incidentform/toshare/'+$scope.myInfo.login_id;
        console.log($scope.myInfo, 'logged_info');
      });
  }
  $scope.getMyInfo();

  $scope.copyLink = () => {
    var copyText = document.getElementById("input-formlink");
    copyText.select();
    document.execCommand("copy");
    $scope.showToast('success', 'Copied to clipboard..');
  };

  $scope.getPages = function (refocus = true) {
    myHttpService.get('get_pages')
      .then(function (res) {
        $scope.pages = res.data; //.message;
        console.log($scope.pages, 'get_pages');
      });
  }
  $scope.getPages();
  
  $scope.formElements2 = [
    {
      name: 'Single Line',
      icon: 'fa-edit'
    },
    {
      name: 'Paragraph',
      icon: 'fa-paragraph'
    },
  ];

  $scope.formElements = [
    {
      name: 'Text',
      icon: 'fa-text-width'
    },
    {
      name: 'Image',
      icon: 'fa-picture-o'
    },
    {
      name: 'Video',
      icon: 'fa-video-camera'
    },
    // {
    //   name: 'Single Line',
    //   icon: 'fa-edit'
    // },
    // {
    //   name: 'Paragraph',
    //   icon: 'fa-paragraph'
    // },
    // {
    //   name: 'Email',
    //   icon: 'fa-envelope-o'
    // },
    // {
    //   name: 'Url',
    //   icon: 'fa-globe'
    // },
    {
      name: 'File',
      icon: 'fa-file-o'
    },
    // {
    //   name: 'Date',
    //   icon: 'fa-calendar'
    // },
    // {
    //   name: 'Dropdown',
    //   icon: 'fa-arrow-circle-down'
    // },
    {
      name: 'Sub checklist',
      icon: 'fa-list'
    },
    {
      name: 'Send Email',
      icon: 'fa-envelope'
    },
  ];
  
  
  $scope.dropSuccessHandler = function($event,index,array){
      // array.splice(index,1);
  };

  $scope.dropValidate = function(){
    return false;
  };

  $scope.onDrop = function($event,$data,array){
    // console.log($event, $data, array);
    $scope.clickFormElement($data);
  };

  $scope.clickFormElement = function(el){
    // console.log(el);
    if(el.name == 'Text'){
      $scope.addText();
    }
    if(el.name == 'Image'){
      $scope.addImage();
    }
    if(el.name == 'Video'){
      $scope.addVideo();
    }
    if(el.name == 'Single Line'){
      $scope.addSingleLine();
    }
    if(el.name == 'Paragraph'){
      $scope.addParagraph();
    }
    if(el.name == 'Email'){
      $scope.addEmailForm();
    }
    if(el.name == 'Url'){
      $scope.addSiteForm();
    }
    if(el.name == 'File'){
      $scope.addFileForm();
    }
    if(el.name == 'Date'){
      $scope.addDateForm();
    }
    if(el.name == 'Dropdown'){
      $scope.addDropdownForm();
    }
    if(el.name == 'Sub checklist'){
      $scope.addSubchecklist();
    }
    if(el.name == 'Send Email'){
      $scope.addEmail();
    }
  };


  $scope.keyUpContent = function(content){
    if(content.module == 'text'){
      // $scope.saveChanges();
    }
  }

  $scope.saveMultiField = function(length, index){
    if(length == (index + 1)){
      // $scope.saveChanges();
    }
  }



  $scope.checkGenReport = function(event){
    if($scope.isFreePlan){
      bootbox.alert('To access this feature, please upgrade your plan.');
      event.preventDefault();
    }
  };

  $scope.getIncidents = function () {
    myHttpService.getWithParams('get_incidents', {
        incident_type: 'dashboard'
      })
      .then(function (res) {
        $scope.incidents = res.data; //.message;
        console.log($scope.incidents, 'get_incidents asdfdsf');

      });
  }
  $scope.getIncidents();

  $scope.toEdit = function (type) {
    $scope.edit_mode = type;
  }

  $scope.incidentsModal = function () {
    var dlg = dialogs.create('incidentsModal.html', 'incidentsDialogCtrl', {
      incidents: $scope.incidents
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      // $scope.raiseTicket();
    }, function () {});
  }


  $scope.toggleListView = function () {
    console.log($scope.changesCounter, '$scope.changesCounter');
    // if($scope.changesCounter){
    //   if($scope.viewListMode){
    //     $scope.getHeaderKanban();
    //   }
    //   else{
    //     self.getHeaderTasks(false);
    //   }
    //   $scope.changesCounter = 0;
    // }

    bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>' ,closeButton: false});

    if ($scope.viewListMode) {
      $scope.getHeaderKanban();
    } else {
      self.getHeaderTasks(false);
    }

    $scope.viewListMode = !$scope.viewListMode;
  }


  //quill
  $scope.title = 'Quill works';

  
  $scope.optionsSummer = {
    height: 350
  };

  self.quillMods = {
    // height: 50,
    toolbar: [
      ['bold', 'italic', 'underline'],
      ['link'],
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }], // superscript/subscript
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }],
      [{
        'header': [1, 2, 3, 4, 5, 6, false]
      }],
      ['clean'],
    ]
  };

  self.quillModsImage = {
    toolbar: [
      ['image']
    ]
  };

  self.quillModsVideo = {
    toolbar: [
      ['video']
    ]
  };

  $(document).on('click', '.ql-editor', function (i) {
    console.log($(this).find('.ql-clean').length, i, 'i');
    $(this).closest('.editor-holder').find('.ql-clean').click();
  });
  $(document).on('keyup', '.ql-editor', function (i) {
    console.log($(this).find('.ql-clean').length, i, 'i');
    $(this).closest('.editor-holder').find('.ql-clean').click();
  });

  $scope.previewIncident = function () {
    console.log($scope.contents, '$scope.contents');
    var h = screen.height;
    var w = screen.width;
    window.open(
      base_url + 'cc/standbytasks/preview_incident',
      'targetWindow',
      'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=' + w + ',height=' + h
    );
  }

  $scope.openPreview = function () {
    console.log($scope.contents, '$scope.contents'),
      window.localStorage.setItem('activeTaskInfo', JSON.stringify($scope.activeTaskInfo));
    window.localStorage.setItem('activeContents', JSON.stringify($scope.contents));

    window.open(
      base_url + 'cc/standbytasks/preview',
      'targetWindow',
      'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=890,height=550'
    );
  }

  $scope.sortableTableOptions = {
    handle: '> .num div',
    placeholder: "ui-state-highlight"
  }

  $scope.sortableContentOptions = {
    handle: '> .move-handle',
    placeholder: "ui-state-highlight"
  }

  $scope.assignCrt = function (i) {
    $scope.isAssigned = i;
  }

  $scope.getQuillModule = function (i) {
    if (i == 'video') {
      return self.quillModsVideo;
    } else if (i == 'image') {
      return self.quillModsImage;
    } else {
      return self.quillMods;
    }
  }

  $scope.optionsPicker = {
    minDate: new Date(),
    // showWeeks: false
  }

  $scope.dt = new Date();

  $scope.datePopSettings = {
    isOpen: false
  };

  $scope.assignPopSettings = {
    isOpen: false
  };

  $scope.saveAssigned = function () {
    $scope.assignPopSettings.isOpen = false;

    if ($scope.activeTaskInfo.assigned.crt_email) {
      $scope.activeTaskInfo.assigned_id = $scope.activeTaskInfo.assigned.login_id;
      $scope.activeTaskInfo.assigned_info = $scope.activeTaskInfo.assigned;
    } else {
      $scope.activeTaskInfo.assigned_id = '0';
      $scope.activeTaskInfo.assigned_info = null;
    }
    $scope.saveChanges();

  }

  $scope.saveDate = function () {
    var fdate = $filter('date')($scope.activeTaskInfo.due_date, "yyyy-MM-dd");
    console.log(fdate, 'fdate');
    // $scope.datePopSettings.isOpen = false;
    $scope.saveChanges();
  }
  $scope.removeDate = function () {
    // $scope.activeTaskInfo.due_date = null;
    $scope.activeTaskInfo.months = '';
    $scope.activeTaskInfo.days = '';
    $scope.activeTaskInfo.hours = '';
    $scope.activeTaskInfo.mins = '';
  }


  self.getHeaderTasks = function (refocus = true) {
    var incident_id = $scope.incident_id;

    myHttpService.getWithParams('header_tasks', {
        incident_id: incident_id,
        template_id: $scope.template_id
      })
      .then(function (res) {

        $scope.headerTasks = res.data; //.message;
        console.log($scope.headerTasks, 'self.message');
        $('.modal').modal('hide');
        
        if (refocus) {
          //show first header by default
          $scope.focusHeader(0);
        }


      });
  }
  self.getHeaderTasks();


  $scope.getHeaderKanban = function () {
    var incident_id = $scope.incident_id;
    myHttpService.getWithParams('header_kanban', {
        incident_id: incident_id,
        template_id: $scope.template_id
      })
      .then(function (res) {
        $scope.headerKanban = res.data; //.message;
        console.log($scope.headerKanban, '$scope.getHeaderKanban');
        $('.modal').modal('hide');
      });
  }
  $scope.getHeaderKanban();


  $scope.getIncident = function () {
    var incident_id = $scope.incident_id;

    myHttpService.getWithParams('get_incidents', {
        incident_id: incident_id
      })
      .then(function (res) {
        $scope.incident = res.data; //.message;
        console.log($scope.incident, 'get_incidents');
      });
  }
  $scope.getIncident();

  $scope.getCrts = function () {
    myHttpService.get('get_crts')
      .then(function (res) {

        $scope.crts = res.data; //.message;
        console.log($scope.crts, 'get_crts');

      });
  }
  $scope.getCrts();


  $scope.clickTask = function (id, index) {
    $scope.activeTask = id;
    $scope.activeTaskIndex = index;
    var h = $scope.activeHeader;

    $scope.activeTaskInfo = $scope.headerTasks[h].tasks[index];
    $scope.contents = $scope.headerTasks[h].tasks[index].contents;
    console.log(id);

    //hide showing assign, date popovers
    $scope.assignPopSettings.isOpen = false;
    $scope.datePopSettings.isOpen = false;

    // if($scope.edit_mode){
    //   $scope.saveChanges();
    // }
  }


  $scope.showLog = function (i) {
    var dlg = dialogs.create('showLogModal.html', 'showLogDialogCtrl', {
      i: i
    }, {
      size: 'md',
      keyboard: true,
      backdrop: 'static',
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      console.log(data, 'dlg.result');
      // $scope.headerKanban[i3].disected_tasks[i2][i].contents = data;      
    }, function () {});

  }
  $scope.clickTaskBanModal = function (d, i, i2, i3, incident_id) {
    var dlg = dialogs.create('taskKanbanModal.html', 'taskKanbanDialogCtrl', {
      task: d,
      incident_id: incident_id
    }, {
      size: 'lg',
      keyboard: true,
      backdrop: 'static',
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      console.log(data, 'dlg.result');
      // $scope.headerKanban[i3].disected_tasks[i2][i].contents = data.contents;
      // $scope.headerKanban[i3].disected_tasks[i2][i].question = data.task.question;
      // $scope.headerKanban[i3].disected_tasks[i2][i].assigned_id = data.task.assigned_id;
      // $scope.headerKanban[i3].disected_tasks[i2][i].months = data.task.months;
      // $scope.headerKanban[i3].disected_tasks[i2][i].days = data.task.days;
      // $scope.headerKanban[i3].disected_tasks[i2][i].hours = data.task.hours;
      // $scope.headerKanban[i3].disected_tasks[i2][i].mins = data.task.mins;

      $scope.getHeaderKanban();

    }, function () {
      $scope.getHeaderKanban()
    });

  }

  $scope.clickTaskBan = function (id, index, headerIndex) {
    $scope.activeTask = id;
    $scope.activeTaskIndex = index;

    $scope.activeHeader = headerIndex;
    var h = $scope.activeHeader;
    $scope.toggleListView();

    $scope.headerTasks[h].show = true;
    $scope.activeTaskInfo = $scope.headerTasks[h].tasks[index];
    $scope.contents = $scope.headerTasks[h].tasks[index].contents;
    console.log(id);

    //hide showing assign, date popovers
    $scope.assignPopSettings.isOpen = false;
    $scope.datePopSettings.isOpen = false;
  }

  $scope.focusHeader = function (i, t = false) {


    if ($scope.headerTasks.length) {

      if ($scope.activeTaskInfo.id) {
        setTimeout(function () {
          var $container = $(document).find('.col-headers .scroll-content');
          var $el = $container.find('tr:eq(' + i + ')');
          $container.animate({
            scrollTop: $container.scrollTop() + ($el.offset().top - $container.offset().top - 11)
          });

        }, 0);
      }

      if (i != $scope.activeHeader || !$scope.activeTask) {
        $scope.activeTaskIndex = 0;

        if ($scope.headerTasks[i].tasks.length) {
          $scope.activeTask = $scope.headerTasks[i].tasks[0].id;
          $scope.activeTaskInfo = $scope.headerTasks[i].tasks[0];
          $scope.contents = $scope.headerTasks[i].tasks[0].contents;
        }

        $scope.activeHeader = i;
      }

      if (!t) {
        $scope.headerTasks[i].show = true;
      }
      console.log(i, t, $scope.tasksOpen, '$scope.tasksOpen');

    }



  }


  self.contentModel = {
    id: '',
    show: false,
    content: '',
    email: null,
    sort: 1,
    field: [],
    options: [],
    checklist_id: $scope.activeTask,
    module: 'text',
    answer: ''
  }

  self.fieldModel = {
    id: '',
    text: '',
    content_id: 0,
    answer: '',
    type: 'sublist'
  }

  self.headerModel = {
    id: "",
    name: "",
    step_no: 0,
    allowed_type: [],
    recall_pack_id: $scope.template_id,
    disected_tasks: [
      [],
      [],
      []
    ],
    tasks: []
  }

  self.taskModel = {
    id: "",
    contents: [],
    question: "New Task",
    step_no: $scope.activeHeader + 2,
    recall_pack_id: $scope.template_id,
    wiki_id: '0',
  }


  $scope.sendTaskContentEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (re.test(email.email)) {
      $scope.postMethod(email, 'send_content_email').then(function (res) {
        console.log('send_content_email', res);
        bootbox.alert('Email sent successfully.');
      });
    } else {
      bootbox.alert('Email is invalid.');
    }
  }


  $scope.deleteTask = function (i, d) {
    $dialogConfirm(action_messages.standard.delete_task).then(function () {
      console.log('yeahh');

      $scope.postMethod(d, 'delete_task').then(function (res) {
        console.log('delete_task', res);
        var h = $scope.activeHeader;
        $scope.headerTasks[h].tasks.splice(i, 1);
        console.log(i);
        setTimeout(function () {
          $scope.focusHeader(h);
        }, 500);

      });

    }, function () {
      console.log('nope');
    });


  }

  $scope.postMethod = function (data, db) {
    return myHttpService.post(data, db);
  }

  $scope.autoSavHeader = function(cl, index){
    var theader_data = {
      id: (cl.id) ? cl.id : '',
      name: cl.name,
      order: index, //cl.order,
      recall_pack_id: cl.recall_pack_id,
      step_no: cl.step_no

    };
    $scope.postMethod(theader_data, 'save_task_headers').then(function(res){
      cl.id = res.data.id;
      $scope.showToast('success', 'Auto-saved..');
    });
  }
  
  $scope.autoSavTask = function(tsk, index){
    var task_data = {
      id: (tsk.id) ? tsk.id : '',
      question: tsk.question,
      assigned_id: tsk.assigned_id,
      months: (tsk.months) ? tsk.months : '',
      days: (tsk.days) ? tsk.days : '',
      hours: (tsk.hours) ? tsk.hours : '',
      mins: (tsk.mins) ? tsk.mins : '',
      wiki_id: (tsk.wiki_id) ? tsk.wiki_id : '0',
      // due_time: tsk.due_time,
      // due_date: fdate,
      arrangement: index,
      recall_pack_id: tsk.recall_pack_id,
      step_no: tsk.step_no
    };
    $scope.postMethod(task_data, 'save_tasks').then(function(res){
      tsk.id = res.data.id;
      $scope.showToast('success', 'Auto-saved..');
    });
  }

  $scope.saveField = (fld) => {

    var field_data = {
      'id': fld.id,
      'text': fld.text,
      'content_id': fld.content_id,
      'type': fld.type,
      'answer': fld.answer,
    };

    console.log(fld, 'fld');

    $scope.postMethod(field_data, 'save_fields').then(function (res) {
      $scope.showToast('success', 'Changes saved successfully.');
    });
  };

  $scope.editIncidentMode = () => {
    $scope.incidentFormEditing = true;
  };

  $scope.deleteTemplateConfirm = () => {
    bootbox.confirm('Delete task template? You cannot recover tasks and its content.', (r) => {
      if(r){
        $scope.deleteTemplateNow();
      }
    });
  };

  $scope.deleteTemplateNow = () => {
    $scope.postMethod($scope.template, 'delete_template')
      .then((res)=>{
        window.location.href = base_url + 'cc/dashboard';
        $scope.showToast('success', 'Changes saved successfully.');
      });
  };

  $scope.getTemplateName = () => {
    myHttpService.getWithParams('get_template_name', {id: uri_5})
    .then((res)=> {
      if(res.data && res.data.length){
        $scope.template = res.data[0];
      }
    });
  };
  $scope.getTemplateName();

  $scope.templateChange = () => {
    $scope.postMethod($scope.template, 'save_template')
      .then((res)=>{
        $scope.showToast('success', 'Changes saved successfully.');
      });
  };

  $scope.getFormIncidentTitle = () => {
    myHttpService.get('get_incident_title')
      .then((res)=> {
        if(res.data && res.data.length){
          $scope.incidentTitle = res.data[0].name;
        }
      });
  };
  $scope.getFormIncidentTitle();

  $scope.saveFormIncidentTitle = () => {
    if($scope.incidentTitle){
      $scope.postMethod({name: $scope.incidentTitle}, 'add_incident_form');
    }
  };

  $scope.isChanging = false;
  $scope.incidentFormEditing = false;
  $scope.saveChanges = function (autoSave = false) {
    if(!$scope.isChanging){

      $scope.isChanging = true;

      console.log($scope.headerTasks);
      var promises = [];
      $scope.loading = true;
      $scope.saveStandbyShown = false;

      $scope.saveFormIncidentTitle();

      var save_fields = function (content_id, index_content, index_task, index_header) {
        var promises_fields = [];

        var task_length = $scope.headerTasks[index_header].tasks.length;
        var the_content = $scope.headerTasks[index_header].tasks[index_task].contents[index_content];
        var cnt = the_content;
        var field = the_content.field;
        var options = the_content.options;
        var email = the_content.email;

        if ((typeof (field) != 'undefined') && field.length > 0) {
          field.forEach(function (fld, icf, array) {

            var fld = field[icf];
            fld.content_id = content_id;
            var fldsSave = null;

            var field_data = {
              'id': fld.id,
              'text': fld.text,
              'content_id': fld.content_id,
              'type': fld.type,
              'answer': fld.answer,
            };

            console.log(fld, 'fld');

            fldsSave = $scope.postMethod(field_data, 'save_fields');
            promises_fields.push(fldsSave);

          });

        } else if ((typeof (options) != 'undefined') && options.length > 0) {
          options.forEach(function (opts, ico, array) {
            var opts = options[ico];
            opts.content_id = content_id;
            var optsSave = null;

            var opts_data = {
              'id': opts.id,
              'content_id': opts.content_id,
              'text': opts.text,
              'type': opts.type,
              'answer': opts.answer,
            };

            optsSave = $scope.postMethod(opts_data, 'save_options');
            promises_fields.push(optsSave);

          });


        }

        //save if email
        else if (cnt.module == 'email') {
          var eml = cnt.email;
          eml.content_id = content_id;

          var emlSave = null;

          emlSave = $scope.postMethod(eml, 'save_emails');
          promises_fields.push(emlSave);

        }

        $q.all(promises_fields).then(function (results) {

          console.log($scope.myInfo.tasks_review_date, '$scope.myInfo.tasks_review_date');
          if ($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00'){ //!autoSave) {
            $scope.myInfo.tasks_review_date = '121';
            bootbox.alert(action_messages.standard.reviewed_sb_tasks, function () {
              // if ($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00') {
                window.location.href = window.base_url + 'cc/dashboard';
              // }
            });
          }
          $scope.loading = false;
          console.log(promises.length, index_header, task_length, index_task, ', index_header, task_length, index_task');
          if (promises.length == (index_header + 1) && task_length == (index_task + 1)) {


            $scope.showStandbySuccess();
            $scope.saveStandbyShown = true;
            $scope.isChanging = false;

          }

          results.forEach(function (currentValue, index, array) {
            console.log(currentValue.data, index, 'promises_fields');
          });
        });

      }

      var save_contents = function (task_id, index_task, index_header) {
        var promises_contents = [];


        var the_task = $scope.headerTasks[index_header].tasks[index_task];
        console.log(the_task, 'the_task', task_id, index_task, index_header);

        // if(typeof(the_task) != 'undefined'){
        the_task.contents.forEach(function (cnt, index, array) {
          var sortt = index;
          var modulee = cnt.module;
          var checklist_id = task_id;
          var cnt_data = {
            'id': cnt.id,
            'checklist_id': checklist_id,
            'module': modulee,
            'sort': sortt,
            'content': cnt.content,
            'answer': cnt.answer,
          };
          promises_contents.push($scope.postMethod(cnt_data, 'save_contents'));

        });

        $q.all(promises_contents).then(function (results) {
          results.forEach(function (currentValue, index, array) {
            console.log(currentValue.data, index, 'promises_contents');

            if(promises_contents.length == index + 1){
              $scope.showStandbySuccess(autoSave);
              $scope.saveStandbyShown = true;
              $scope.loading = false;
              $scope.isChanging = false;
              $scope.showToast('success', 'Changes saved successfully.'); //Auto-saved..
              $scope.incidentFormEditing = false;
            }

            the_task.contents[index].id = currentValue.data.id;

            save_fields(currentValue.data.id, index, index_task, index_header);
          });
        });
        // }

      }

      save_contents($scope.activeTaskInfo.id, $scope.activeTaskIndex, $scope.activeHeader);

    }

  }

  $scope.showStandbySuccess = function (autoSave = false) {
    console.log(($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00'), $scope.myInfo.tasks_review_date, '$scope.myInfo.tasks_review_date');

    if ($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00'){ //!autoSave) {
      bootbox.alert(action_messages.standard.reviewed_sb_tasks, function () {
        // if ($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00') {
          window.location.href = window.base_url + 'cc/dashboard';
        // }
      });
    }

    if (!$scope.saveStandbyShown) {
      // self.getHeaderTasks(false);

      if ($scope.page_type == 'activetask') {
        // $scope.edit_mode = false;
      }

      // if ($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00'){ //!autoSave) {
      //   bootbox.alert(action_messages.standard.reviewed_sb_tasks, function () {
      //     // if ($scope.myInfo.tasks_review_date == '0000-00-00 00:00:00') {
      //       window.location.href = window.base_url + 'cc/dashboard';
      //     // }
      //   });
      // } else {}

    }
  }

  $scope.addTask = function () {
    var i = $scope.activeHeader;
    var d = self.taskModel;
    var t = Object.assign({}, d);
    t.step_no = $scope.activeHeader + 2;
    console.log(t);

    $scope.headerTasks[i].tasks.push(t);
  }

  $scope.addHeader = function () {
    var d = self.headerModel;
    var t = Object.assign({}, d);
    t.step_no = $scope.headerTasks.length + 2;
    t.show = (!$scope.headerTasks.length) ? true : false;

    console.log(t, $scope.headerTasks.length);
    $scope.headerTasks.push(t);
  }


  $scope.addHeaderKan = function () {
    var d = self.headerModel;
    var t = Object.assign({}, d);
    t.step_no = $scope.headerKanban.length + 2;
    t.order = $scope.headerKanban.length;
    t.name = 'New Header';

    $scope.postMethod(t, 'save_task_headers').then(function (res) {
      console.log('save_task_headers', res);
      t.id = res.data.id;
      t.allowed_type = [
        "header" + t.id
      ];
      $scope.headerKanban.push(t);
    });

    console.log(t, $scope.headerKanban.length);
  }

  $scope.addTaskBan = function () {
    var d = self.taskModel;
    var t = Object.assign({}, d);
    console.log(t, 'dlg.result');

    var dlg = dialogs.create('editTaskModal.html', 'editTaskDialogCtrl', {
      d: t,
      h: $scope.headerKanban
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });
    // console.log(i, i2, i3, d, 'i, i2, i3, ddlg.result'); 

    dlg.result.then(function (data) {
      console.log(data, 'dlg.result');
      $scope.headerKanban[data.header.order].disected_tasks[0].push(data.data);
    }, function () {});

  }

  $scope.updateHeaderKan = function (i, d) {
    var dlg = dialogs.create('editHeaderModal.html', 'editHeaderDialogCtrl', {
      d: d,
      i: i
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      console.log(data, 'dlg.result');
      if (data.delete) {
        $scope.headerKanban.splice(i, 1);
      } else {
        $scope.headerKanban[i].name = data.name;
        $scope.headerKanban[i].id = data.id;
      }

    }, function () {});

  }

  $scope.editTaskModal = function (i, i2, i3, d) {
    var dlg = dialogs.create('editTaskModal.html', 'editTaskDialogCtrl', {
      d: d,
      i: i
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });
    console.log(i, i2, i3, d, 'i, i2, i3, ddlg.result');

    dlg.result.then(function (data) {
      console.log(data, 'dlg.result');
      $scope.headerKanban[i3].disected_tasks[i2][i].question = data.question;
      $scope.headerKanban[i3].disected_tasks[i2][i].id = data.id;
    }, function () {});

  }

  $scope.deleteHeaderKan = function (i, d) {
    $dialogConfirm(action_messages.standard.delete_sb_step).then(function () {

      $scope.postMethod(d, 'delete_header').then(function (res) {
        console.log('delete_header', res);
        $scope.headerKanban.splice(i, 1);
      });

    }, function () {
      console.log('nope');
    });

  }

  $scope.deleteHeader = function () {
    $dialogConfirm(action_messages.standard.delete_sb_step).then(function () {
      var h = $scope.activeHeader;
      var d = $scope.headerTasks[h];
      $scope.postMethod(d, 'delete_header').then(function (res) {
        console.log('delete_header', res);
        $scope.headerTasks.splice(h, 1);
      });
    }, function () {
      console.log('nope');
    });
  }


  $scope.deleteHeaderLive = function (h, d) {
    $dialogConfirm(action_messages.standard.delete_sb_step).then(function () {
      // var h = $scope.activeHeader;
      // var d = $scope.headerTasks[h];
      $scope.postMethod(d, 'delete_header').then(function (res) {
        console.log('delete_header', res);
        $scope.headerTasks.splice(h, 1);
      });
    }, function () {
      console.log('nope');
    });
  }

  $scope.newContentScroll = function (save = false) {
    var $container = $(document).find('.col-contents .scroll-content');
    $container.stop().animate({
      scrollTop: $container[0].scrollHeight
    }, 800);
    if(save){
      $scope.saveChanges(true);
    }
  }



  $scope.removeContent = function (i, content) {
    if (content.id) {
      $scope.postMethod({
        id: content.id
      }, 'delete_content').then(function (res) {
        console.log('delete', res);
        // this.currentTask.contents = this.contents;
        $scope.contents.splice(i, 1);
      });
    } else {
      $scope.contents.splice(i, 1);
    }

  };


  $scope.moveContent = function (i, dir) {
    if (dir == 'up') {
      this.swapArrayElements(this.contents, i, i - 1);
    } else {
      this.swapArrayElements(this.contents, i, i + 1);
    }

  }

  $scope.copyContent = function (c, i) {
    var data = Object.assign({}, c)
    data.id = '';
    this.contents.splice(i, 0, data);
  }

  $scope.swapArrayElements = function (arr, indexA, indexB) {
    var temp = arr[indexA];
    arr[indexA] = arr[indexB];
    arr[indexB] = temp;
    return arr;
  };

  $scope.keyDownSelect = function (e, d, isl, i) {
    if (e.keyCode == 13) {
      console.log(e, d, isl, i, '');
      var newIndex = isl + 1;

      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'option';

      // var data = this.fieldModel('option');
      $scope.contents[i].options.splice(newIndex, 0, t);
      // this.contents.splice(i, 0, data);
      console.log(e, d, isl, i);
    }

  }

  $scope.keyDownSublist = function (e, d, isl, i) {
    if (e.keyCode == 13) {
      console.log(e, d, isl, i, 'keyDownsublist2');
      var newIndex = isl + 1;

      var t = self.fieldModel;
      t = Object.assign({}, t);

      this.contents[i].field.splice(newIndex, 0, t);
      // this.contents.splice(i, 0, data);
    }

  }


  $scope.htmlSafe = function (data) {
    return $sce.trustAsHtml(data);
  }

  $scope.addText = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    $scope.contents.push(t);
    console.log(t, 'addText');
    var h = $scope.activeHeader;
    var tsk = $scope.activeTaskIndex;

    $scope.newContentScroll();

    // $scope.activeTaskInfo.contents = $scope.contents;
    // $scope.headerTasks[h].tasks[tsk].contents = $scope.contents;
  }

  $scope.addImage = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'image';
    $scope.contents.push(t);

    $scope.newContentScroll();

    // $scope.activeTaskInfo.contents = $scope.contents;
    console.log($('.ql-image'));
  }

  $scope.showImagePop = function (c, i) {
    var i2 = $('.ql-image').length - 1;
    $('.ql-image:eq(' + i2 + ')').click();
    console.log(c);
  }

  $scope.videoUrl = function (v) {
    var newv = v;
    if (v.indexOf('youtube.com') > -1) {
      newv = $scope.checkYoutube(v);
    }

    return $sce.trustAsResourceUrl(newv);
    // console.log($sce.trustAsResourceUrl(v));
  }

  $scope.checkYoutube = function (l) {
    var upd = l;
    var partsOfStr = l.split('v=');
    if (partsOfStr.length > 1) {
      upd = 'https://www.youtube.com/embed/' + partsOfStr[1];
    }

    return upd;

  }


  $scope.addVideo = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'video';
    this.contents.push(t);
    // this.currentTask.contents = this.contents;

    $scope.newContentScroll();

    setTimeout(() => {
      var i = $('.ql-video').length - 1;
      $('.ql-video:eq(' + i + ')').click();
    }, 5);
    console.log($('.ql-video'));
  }


  $scope.addSubchecklist = function () {
    var text = [];
    for (var i = 0; i < 3; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      text.push(t);
    }
    console.log(text, 'addSubchecklist');
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    console.log(t, 'sublist');
    this.contents.push(t);

    $scope.newContentScroll(false);

    // self.contents.push(t);
    // self.currentTask.contents = self.contents;
  }

  $scope.addDropdownForm = function () {
    var text = [];
    var opts = [];
    // for(var i = 0; i < 1; i++){
    //     var t = self.fieldModel;
    //     t = Object.assign({}, t);
    //     t.type = 'select';
    //     text.push(t);
    // }
    for (var i = 0; i < 3; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'option';
      opts.push(t);
    }
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'select';
    t.field = text;
    t.options = opts;
    this.contents.push(t);

    $scope.newContentScroll(false);

    // this.currentTask.contents = this.contents;
  }


  $scope.addParagraph = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'paragraph';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);

    $scope.newContentScroll(false);

    // this.currentTask.contents = this.contents;
  }

  $scope.addEmailForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'email';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);

    $scope.newContentScroll(false);

  }

  $scope.addSiteForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'url';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);

    $scope.newContentScroll(false);

  }


  $scope.addFileForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'file';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);

    $scope.newContentScroll(false);

  }

  $scope.addDateForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'date';
      text.push(t);

    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);

    $scope.newContentScroll(false);

  }

  $scope.addSingleLine = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'single';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);

    $scope.newContentScroll(false);

  }


  $scope.addEmail = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'email';
    this.contents.push(t);

    $scope.newContentScroll(false);

  }

  $scope.taskMoved = function (i, p, h) {
    console.log(i, p, h, '$scope.models.lists[p]');
    $scope.headerKanban[h].disected_tasks[p].splice(i, 1);
  }

  $scope.logEvent = function (i, event, task) {
    task.task_answer = {
      answer: "true",
      sort: "0",
      created_at: new Date()
    };
    console.log(i, event, task, '(index, item, external, type)');
    $scope.saveMovedTask(task, i);
  }

  $scope.formatInProgressMoment = function (t) {
    // console.log(t, 'formatInProgressMoment');
    // myHttpService.getWithParams('get_server_time', {})
    //   .then(function(res) {
    //     return moment(t).preciseDiff(res.time); //new Date());
    // });
    var m = moment.preciseDiff(new Date(), t, true);

    var y = m.years ? m.years + ' year ' : '';
    var mo = m.months ? m.months + ' month ' : '';
    var d = m.days ? m.days + ' day ' : '';
    var h = m.hours ? m.hours + ' hour ' : '';
    var mi = m.minutes ? m.minutes + ' minute' : '';

    y = (m.years > 1) ? m.years + 'years ' : y;
    mo = (m.months > 1) ? m.months + 'months ' : mo;
    d = (m.days > 1) ? m.days + 'days ' : d;
    h = (m.hours > 1) ? m.hours + 'hrs ' : h;
    mi = (m.minutes > 1) ? m.minutes + 'mins' : mi;
    var txt = y + mo + d + h + mi;
    
    return txt ? txt : 'less than a minute';

  }
  $scope.formatDoneMoment = function (d) {
    // console.log(d);
    // var m = moment.preciseDiff(d.incident_date, d.task_answer.created_at, true);
    var m = moment.preciseDiff(new Date(), d.task_answer.created_at, true);

    var y = m.years ? m.years + ' years ' : '';
    var mo = m.months ? m.months + ' months ' : '';
    var d = m.days ? m.days + ' days ' : '';
    var h = m.hours ? m.hours + ' hours ' : '';
    var mi = m.minutes ? m.minutes + ' minutes' : '';
    var txt = y + mo + d + h + mi;

    return txt ? txt : 'less than a minute';
  }
  $scope.getDueDiff = function (d, ccll) {
    if (d.task_answer) {
      var m = moment(d.task_answer.created_at);

      var dd = m.add(d.months, 'months').add(d.days, 'days').add(d.hours, 'hours').add(d.mins, 'minutes');
      var bool = moment().isBefore(dd);
      var diff = moment().countdown(dd).toString();

      var cls = (bool) ? '' : 'text-red';
      var text = (bool) ? diff + ' remaining' : diff + ' passed';
      // console.log(text, bool, ccll, 'm(dd, bool;');


      return (ccll == 'text') ? text : cls;

    }
  }
  $scope.completeIncident = function () {
    var status = ($scope.incident.incident_status == '0') ? '1' : '0';
    var mess = (status == '0') ? 'Reactivate incident?' : 'Confirm you want to close this incident?';
    $dialogConfirm(mess).then(function () {
      $scope.loading = true;
      console.log('yeahh');
      var data = {
        status: status,
        incident_id: $scope.incident_id
      };
      $scope.postMethod(data, 'update_incident_status').then(function (res) {
        if(status != '0'){
          window.location.href = base_url + 'cc/dashboard';
        }
        console.log(res, 'update_incident_status');
        $scope.incident.incident_status = status;
        $scope.incident.incident_status_text = (status == '0') ? 'Active' : 'Closed';
        $scope.loading = false;
      });

    }, function () {
      console.log('nope');
    });
  }

  $scope.saveMovedTask = function (t, i) {
    if ($scope.incident_id) {
      var data = {
        task_id: t.id,
        incident_id: $scope.incident_id,
        user_id: $scope.myInfo.login_id,
        answer: false,
        sort: 0
      };

      //check if where dragged
      if (i == 1) { //in progress
        data.sort = 1;
      } else if (i == 2) { //done
        data.answer = true;
      }

      $scope.postMethod(data, 'save_task_answer').then(function (res) {
        console.log(res, 'save_task_answer saveMovedTask');
        $scope.changesCounter++;
      });
    }

  }

  $scope.completeTask = function (t, i) {

    if(t.blocked == '1'){
      bootbox.alert('Updating task status is blocked.');
      return false;
    }

    var h = $scope.activeHeader;
    $scope.headerTasks[h].tasks[i].loading = true;


    var checked = $scope.headerTasks[h].tasks[i].checked;
    var in_progress = $scope.headerTasks[h].tasks[i].in_progress;

    if ($scope.incident_id) {
      var data = {
        task_id: t.id,
        sort: (!checked && !in_progress) ? '1' : '0',
        incident_id: $scope.incident_id,
        user_id: $scope.myInfo.login_id,
        answer: (in_progress) ? true : false
      };

      $scope.postMethod(data, 'save_task_answer').then(function (res) {
        console.log(res, 'save_task_answer completeTask');
        $scope.headerTasks[h].tasks[i].loading = false;
        $scope.headerTasks[h].tasks[i].checked = (in_progress) ? true : false;
        $scope.headerTasks[h].tasks[i].in_progress = (!checked && !in_progress) ? true : false;
        $scope.headerTasks[h].tasks[i].task_answer = res.data;
        $scope.changesCounter++;
      });
    } else {
      $scope.headerTasks[h].tasks[i].checked = !$scope.headerTasks[h].tasks[i].checked;
    }

  }

  $scope.blockTask = function(d, i){
    console.log(d, i, 'blockTask');
    d.blocked = (d.blocked == '0') ? '1' : '0';
    
    var data = {
      task_id: d.id,
      sort: (d.task_answer) ? d.task_answer.sort : '0',
      incident_id: $scope.incident_id,
      user_id: $scope.myInfo.login_id,
      answer: (d.task_answer) ? d.task_answer.answer : false,
      blocked: d.blocked
    };

    $scope.postMethod(data, 'save_task_block').then(function (res) {
      console.log(res, 'save_task_block completeTask');

    });

  }

  $scope.changeTaskStatus = function (t, i, checked, in_progress) {
    var h = $scope.activeHeader;
    $scope.headerTasks[h].tasks[i].loading = true;

    // var checked = checked;
    // var in_progress = in_progress;

    if ($scope.incident_id) {
      var data = {
        task_id: t.id,
        sort: in_progress,
        incident_id: $scope.incident_id,
        user_id: $scope.myInfo.login_id,
        answer: checked
      };

      $scope.postMethod(data, 'save_task_answer').then(function (res) {
        console.log(res, 'save_task_answer completeTask');
        $scope.headerTasks[h].tasks[i].loading = false;
        $scope.headerTasks[h].tasks[i].checked = checked;
        $scope.headerTasks[h].tasks[i].in_progress = (in_progress == '1') ? true : false;
        $scope.headerTasks[h].tasks[i].task_answer = res.data;
        $scope.changesCounter++;
      });
    } else {
      $scope.headerTasks[h].tasks[i].checked = checked;
    }
  }
  $scope.closeAllPopUps = function () {
    $scope.assignPopSettings.isOpen = false;
    $scope.datePopSettings.isOpen = false;
  }

  $scope.showToast = function (className, content) {
    ngToast.create({
      className: (className) ? className : 'info',
      content: (content) ? content : 'Hi',
      dismissButton: true,
      dismissOnClick: true
    });
  }

  $scope.allChecked = false;
  $scope.checkAllTasks = function () {
    var theValue = ($scope.allChecked) ? false : true;
    var promises = [];

    var save_h = function (cl, index) {
      var theader_data = {
        id: (cl.id) ? cl.id : '',
        name: cl.name,
        order: index, //cl.order,
        recall_pack_id: cl.recall_pack_id,
        step_no: cl.step_no

      };
      promises.push(theader_data);
    }


    $scope.headerTasks.forEach(function (cl, index, array) {
      save_h(cl, index);
    });

    $q.all(promises).then(function (results) {
      results.forEach(function (currentValue, index, array) {
        console.log(currentValue.data, index, 'promises');
        save_tasks(index);
      });
    });


    var save_tasks = function (index_header) {
      var promises_tasks = [];

      $scope.headerTasks[index_header].tasks.forEach(function (tsk, index_task, array) {
        // var fdate = $filter('date')(tsk.due_date, "yyyy-MM-dd");

        if ($scope.incident_id) {
          var data = {
            task_id: tsk.id,
            incident_id: $scope.incident_id,
            user_id: $scope.myInfo.login_id,
            answer: theValue
          };
          $scope.headerTasks[index_header].tasks[index_task].loading = true;

          promises_tasks.push($scope.postMethod(data, 'save_task_answer'));

        }

      });

      $q.all(promises_tasks).then(function (results) {
        results.forEach(function (currentValue, index, array) {
          console.log(currentValue.data, index, 'promises_tasks');

          $scope.headerTasks[index_header].tasks[index].checked = theValue;
          $scope.headerTasks[index_header].tasks[index].loading = false;

          if ($scope.headerTasks.length == (index_header + 1) && promises_tasks.length == (index + 1)) {

            $scope.allChecked = false;
            if (theValue) {
              $scope.allChecked = true;
            }
          }
        });
      });

      // console.log('promises_tasks length', promises_tasks.length);

    }

  }


  $scope.commentModel = {
    id: '',
    message: '',
    user_id: '',
    created_at: new Date(),
    author: null
  }

  $scope.saveComment = function (c) {
    console.log(c, $scope.activeTaskInfo, '$scope.activeTaskInfo');
    var message = c;
    $scope.activeTaskInfo.theComment = '';

    var d = $scope.commentModel;
    var t = angular.copy(d);
    t.author = $scope.myInfo;
    t.recall_id = $scope.activeTaskInfo.incident_id;
    t.task_id = $scope.activeTaskInfo.id;
    t.user_id = $scope.myInfo.login_id;
    t.message = c;
    console.log(t);

    $scope.activeTaskInfo.commentSubmitting = true;


    if ($scope.activeTaskInfo.incident_id) {

      $scope.postMethod(t, 'save_task_comment').then(function (res) {
        $scope.activeTaskInfo.commentSubmitting = false;
        $scope.activeTaskInfo.comments.push(t);
      });
    } else {
      $scope.activeTaskInfo.commentSubmitting = false;
      $scope.activeTaskInfo.comments.push(t);
    }

  }


  $scope.getTemplates = function () {

    myHttpService.getWithParams('get_templates', {
        user_type: $scope.logged_admin
      })
      .then(function (res) {
        $scope.templates = res.data; //.message;
        console.log($scope.templates, 'get_templates');
      });
  }
  $scope.getTemplates();

  $scope.templateModal = function () {
    var dlg = dialogs.create('templateModal.html', 'templateDialogCtrl', {
      templates: $scope.templates
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      $scope.newStandbyModal();
      console.log('newStandbyModal');
    }, function () {});
  }


  $scope.newStandbyModal = function () {
    var dlg = dialogs.create('newStandbyModal.html', 'newStandbyDialogCtrl', {}, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      // $scope.incidents.unshift(data);
      console.log(data, 'dlg.result');

      window.location.href = window.base_url + 'webmanager/recall/tasks/0/' + data.id + '?template_name=' + data.name;
    }, function () {});
  }

});

crisisApp.controller('wikiController', function wikiController($scope, myHttpService, $timeout, $compile, $sce, $q, $filter, $dialogConfirm, FileUploader, dialogs) {
  var $ctrl = this;


  $scope.pages = [];
  $scope.parentPages = [];
  $scope.currentPage = null;
  $scope.currIndex = 0;
  $scope.currSubIndex = null;
  $scope.selectedParent = null;
  $scope.theComment = '';
  $scope.showDocSharing = false;
  $scope.currentHash = window.location.hash.substr(1);

  $scope.files = [];
  $scope.uploadTable = false;
  $scope.isFreePlan = isUserPlanFree();

  $scope.myInfo = null;

  $scope.showUploadTable = function (b) {
    $scope.uploadTable = b;
  }
  var uploader = $scope.uploader = new FileUploader({
    url: base_url + 'cc/standbytasks/upload_file',
    removeAfterUpload: true
  });

  $scope.sortOptions = {
    handle: 'img',
    placeholder: "ui-state-highlight-wiki"
  };


  // FILTERS

  // a sync filter
  uploader.filters.push({
    name: 'syncFilter',
    fn: function (item /*{File|FileLikeObject}*/ , options) {
      console.log('syncFilter');
      return this.queue.length < 10;
    },
    'fn': function (item) {
      if (item.size > 16485760) {
        bootbox.alert('<h4>Opps! File size limit is 16mb.</h4>');
      }
      return item.size <= 16485760; // 16 MiB to bytes
    }
  });

  // an async filter
  uploader.filters.push({
    name: 'asyncFilter',
    fn: function (item /*{File|FileLikeObject}*/ , options, deferred) {
      console.log('asyncFilter');
      setTimeout(deferred.resolve, 1e3);
    }
  });

  // CALLBACKS

  uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/ , filter, options) {
    console.info('onWhenAddingFileFailed', item, filter, options);
  };
  uploader.onAfterAddingFile = function (fileItem) {
    console.info('onAfterAddingFile', fileItem);
  };
  uploader.onAfterAddingAll = function (addedFileItems) {
    console.info('onAfterAddingAll', addedFileItems);
  };
  uploader.onBeforeUploadItem = function (item) {
    console.info('onBeforeUploadItem', item);
  };
  uploader.onProgressItem = function (fileItem, progress) {
    console.info('onProgressItem', fileItem, progress);
  };
  uploader.onProgressAll = function (progress) {
    console.info('onProgressAll', progress);
  };
  uploader.onSuccessItem = function (fileItem, response, status, headers) {
    console.info('onSuccessItem', fileItem, response, status, headers);
    if (response.file) {
      $scope.files.push(response.file);
      $scope.showUploadTable(false);
      $scope.getFiles();
    }
  };
  uploader.onErrorItem = function (fileItem, response, status, headers) {
    console.info('onErrorItem', fileItem, response, status, headers);
  };
  uploader.onCancelItem = function (fileItem, response, status, headers) {
    console.info('onCancelItem', fileItem, response, status, headers);
  };
  uploader.onCompleteItem = function (fileItem, response, status, headers) {
    console.info('onCompleteItem', fileItem, response, status, headers);
  };
  uploader.onCompleteAll = function () {
    console.info('onCompleteAll');
  };

  console.info('uploader', uploader);



  $scope.toggleDocSharing = function (b) {
    $scope.showDocSharing = b;
    if (b) {
      $scope.currentPage = null;
      $scope.currIndex = null;

    }
  }

  $scope.uploadShare = function () {
    if($scope.fileAverage >= 100){
      bootbox.alert('To access this feature, please upgrade your plan.');
    } else {
      $('#docShare').click();
      $scope.uploadTable = true;
    }
  }

  $scope.maxFileSize = [
    20 * 1024* 1024,
    200 * 1024* 1024,
    300 * 1024* 1024,
  ];

  $scope.getBillingDetails = () => {
    const data = JSON.parse(localStorage.getItem('crisisflo_billing'));
    return data;
  };

  $scope.checkRemainingSpace = () => {
    const plan = $scope.getBillingDetails();
    const maxSize = $scope.maxFileSize[plan.selected_plan];
    $scope.fileAverage = $scope.totalFileSize/maxSize * 100;
    $scope.fileAverage = ($scope.fileAverage >= 100) ? 100 : $scope.fileAverage;
    $scope.remainingSpace = maxSize - $scope.totalFileSize;
  };

  $scope.getFiles = function () {

    myHttpService.get('get_files')
      .then(function (res) {
        $scope.files = res.data; //.message;
        console.log($scope.files, 'get_files');
        $scope.totalFileSize = $scope.files.reduce(function (accumulator, currentValue) {
          return accumulator + Number(currentValue.file_size);
        }, 0);

        $scope.checkRemainingSpace();
      });
  }
  $scope.getFiles();

  $scope.getPages = function (refocus = true) {
    myHttpService.get('get_pages')
      .then(function (res) {
        $scope.pages = res.data; //.message;
        $scope.parentPages = angular.copy(res.data);
        console.log($scope.pages, 'get_pages');

        if (res.data.length) {
          $scope.currentPage = res.data[0];

          if($scope.currentHash){
            var _index = 0;
            var _page;
            $scope.pages.find((item, index) => {
              if(item.id === $scope.currentHash){
                _index = index;
                _page = item;
              }
            });
            if(_page){
              $scope.activePage(_index, _page);
            }
          }

        }
      });
  }
  $scope.getPages();

  $scope.viewPageHistory = function () {
    var i = $scope.currIndex;
    $scope.currentPage = $scope.pages[i];
    $scope.currentPage.showHistory = true;
  }

  $scope.getMyInfo = function (refocus = true) {
    myHttpService.get('logged_info')
      .then(function (res) {
        $scope.myInfo = res.data; //.message;
        console.log($scope.myInfo, 'logged_info');

        // if(res.data.length){
        //   $scope.currentPage = $scope.pages[0];
        // }
      });
  }
  $scope.getMyInfo();

  $scope.changeParent = function (d) {
    $scope.currentPage.subparent_id = d.id;
    $scope.selectedParent = $scope.parentPages.indexOf(d);
    console.log(d, $scope.selectedParent, '$scope.selectedParent');

  }
  $scope.resetParent = function () {
    $scope.currentPage.subparent_id = 0;
    $scope.selectedParent = null;
    $scope.currentPage.subparent = null;
  }

  $scope.pageModel = {
    edit: true,
    id: '',
    content: '',
    title: '',
    subparent_id: 0,
    created_at: new Date(),
    histories: [],
    comments: [],
    author: null
  }

  $scope.commentModel = {
    id: '',
    message: '',
    user_id: '',
    created_at: new Date(),
    author: null
  }

  $scope.addPage = function () {
    var d = $scope.pageModel;
    var t = angular.copy(d);
    t.author = $scope.myInfo;
    $scope.currentPage = t;
    // $scope.pages.push(t);

    // var i = $scope.pages.length - 1;
    // $scope.currIndex = i;
    // $scope.currentPage = $scope.pages[i];
  }

  $scope.addSubPage = function () {
    var dlg = dialogs.create('selectParentModal.html', 'selectParentDialogCtrl', {
      pages: $scope.pages
    }, {
      size: 'md',
      keyboard: true,
      backdrop: 'static',
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      console.log(data, 'dlg.result');

      var d = $scope.pageModel;
      var t = angular.copy(d);
      t.author = $scope.myInfo;
      // t.edit = false;
      t.subparent_id = data.subparent_id;
      $scope.selectedParent = data.selectedParent;
      $scope.currentPage = t;
    }, function () {});


  }

  $scope.activePage = function (i, d, subi = null) {
    console.log(d);
    $scope.currentPage = d;
    $scope.currSubIndex = subi;
    $scope.currIndex = i;

    $scope.currentPage.subparent = null;
    if (!isNaN(subi)) {
      console.log(subi, 'isnan');
      $scope.currentPage.subparent = $scope.parentPages[i];
    }
  }

  $scope.htmlSafe = function (data) {
    return $sce.trustAsHtml(data);
  }

  $scope.saveComment = function (c) {
    var message = c;
    $scope.currentPage.theComment = '';

    var d = $scope.commentModel;
    var t = angular.copy(d);
    t.author = $scope.myInfo;
    t.page_id = $scope.currentPage.id;
    t.user_id = $scope.myInfo.login_id;
    t.message = c;
    console.log(t);

    $scope.currentPage.commentSubmitting = true;


    $scope.postMethod(t, 'save_comment').then(function (res) {
      $scope.currentPage.commentSubmitting = false;
      $scope.currentPage.comments.push(t);
    });
  }

  $scope.postMethod = function (data, db) {
    return myHttpService.post(data, db);
  }

  $scope.optionsSummer = {
    height: 350
  };


  $scope.publishPage = function (d) {
    $scope.currentPage.publishSubmitting = true;
    $scope.postMethod(d, 'save_page').then(function (res) {

      console.log(res.data, 'save_page');
      if (!d.id) {
        $scope.currentPage.id = res.data.id;

        $scope.pages.push($scope.currentPage);

        var i = $scope.pages.length - 1;
        $scope.currIndex = i;
        $scope.currentPage = $scope.currentPage;
      } else {
        $scope.currentPage.histories.unshift(res.data.history);
        $scope.currentPage.created_at_time = res.data.created_at_time;
        $scope.currentPage.created_at = res.data.created_at;
      }
      $scope.currentPage.publishSubmitting = false;
      $scope.currentPage.edit = false;
    });
  }


  $scope.deletePage = function () {
    $dialogConfirm("Confirm delete page?").then(function () {
      console.log('yeahh');
      // Action if "yes" is pressed
      $scope.postMethod({
        id: $scope.currentPage.id
      }, 'delete_page').then(function (res) {
        console.log('delete', res);

        $scope.pages.splice($scope.currIndex, 1);
        if ($scope.pages.length) {
          $scope.currentPage = $scope.pages[0];
          $scope.currIndex = 0;
        } else {
          $scope.currentPage = null;
        }
      });


    }, function () {
      console.log('nope');

      // action if "no" is pressed
    });
  }


  $scope.deleteFile = function (d, i) {
    $dialogConfirm("Confirm delete file?").then(function () {
      console.log('yeahh');
      // Action if "yes" is pressed
      $scope.postMethod({
        id: d.id
      }, 'delete_file').then(function (res) {
        console.log('delete', res);
        // $scope.files.splice(i, 1);
        $scope.getFiles();

      });

    }, function () {
      console.log('nope');

      // action if "no" is pressed
    });
  }

  $scope.deleteHistory = function (d, i) {
    $dialogConfirm("Confirm delete history?").then(function () {
      console.log('yeahh');
      // Action if "yes" is pressed
      $scope.postMethod({
        id: d.id
      }, 'delete_page').then(function (res) {
        console.log('delete', res);
        $scope.currentPage.histories.splice(i, 1);

      });

    }, function () {
      console.log('nope');

      // action if "no" is pressed
    });
  }

  $scope.viewHistory = function (d) {
    $scope.currentPage = d;
    $scope.currentPage.isHistory = true;
  }

  $scope.showCurrent = function () {
    $scope.currentPage.showHistory = false;
  }

  $scope.restoreHistory = function (d = null) {
    var data = $scope.currentPage;
    if (d) {
      data = d;
    }
    $dialogConfirm("Confirm restore history?").then(function () {
      console.log('yeahh');
      // Action if "yes" is pressed
      $scope.postMethod(data, 'restore_history').then(function (res) {
        var i = $scope.currIndex;
        $scope.currentPage = $scope.pages[i];
        $scope.currentPage.showHistory = false;
        $scope.currentPage.title = data.title;
        $scope.currentPage.content = data.content;
      });

    }, function () {
      console.log('nope');
    });
  }

});



crisisApp.controller('selectParentDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, $sce, $q) {
  //-- Variables --//
  $scope.data = {};

  $scope.data.subparent_id = 0;
  $scope.data.selectedParent = null;
  $scope.data.subparent = null;

  $scope.pages = data.pages;
  console.log($scope.data);

  $scope.changeParent = function (d) {
    $scope.data.subparent_id = d.id;
    $scope.data.selectedParent = $scope.pages.indexOf(d);
    console.log(d, $scope.data.selectedParent, '$scope.data.selectedParent');

  }
  $scope.resetParent = function () {
    $scope.data.subparent_id = 0;
    $scope.data.selectedParent = null;
    $scope.data.subparent = null;
  }
  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $scope.data.loading = true;
    $uibModalInstance.close($scope.data);

  }; // end save

});

crisisApp.controller('showLogDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, $sce, $q) {
  //-- Variables --//
  $scope.data = data.i;
  console.log($scope.data);

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

});

crisisApp.controller('taskKanbanDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, $sce, $q, $filter) {
  //-- Variables --//
  $scope.data = data;
  $scope.activeTaskInfo = data.task;
  $scope.incident_id = data.incident_id;
  $scope.contents = data.task.contents;
  console.log(data, 'taskKanbanDialogCtrl');
  $scope.editMode = true;
  $scope.loading = false;
  $scope.crts = [];
  $scope.myInfo = null;

  $scope.datePopSettings = {
    isOpen: false
  };

  $scope.assignPopSettings = {
    isOpen: false
  };

  $scope.closeAllPopUps = function () {
    $scope.assignPopSettings.isOpen = false;
    $scope.datePopSettings.isOpen = false;
  }

  $scope.getMyInfo = function (refocus = true) {
    myHttpService.get('logged_info')
      .then(function (res) {
        $scope.myInfo = res.data; //.message;
        console.log($scope.myInfo, 'logged_info');

      });
  }
  $scope.getMyInfo();

  $scope.getPages = function (refocus = true) {
    myHttpService.get('get_pages')
      .then(function (res) {
        $scope.pages = res.data; //.message;
        console.log($scope.pages, 'get_pages');
      });
  }
  $scope.getPages();

  $scope.formElements = [
    {
      name: 'Text',
      icon: 'fa-text-width'
    },
    {
      name: 'Image',
      icon: 'fa-picture-o'
    },
    {
      name: 'Video',
      icon: 'fa-video-camera'
    },
    {
      name: 'File',
      icon: 'fa-file-o'
    },
    {
      name: 'Sub checklist',
      icon: 'fa-list'
    },
    {
      name: 'Send Email',
      icon: 'fa-envelope'
    },
  ];
  
  
  $scope.dropValidate = function(){
    return false;
  };

  $scope.onDrop = function($event,$data,array){
    // console.log($event, $data, array);
    $scope.clickFormElement($data);
  };


  $scope.clickFormElement = function(el){
    // console.log(el);
    if(el.name == 'Text'){
      $scope.addText();
    }
    if(el.name == 'Image'){
      $scope.addImage();
    }
    if(el.name == 'Video'){
      $scope.addVideo();
    }
    if(el.name == 'Single Line'){
      $scope.addSingleLine();
    }
    if(el.name == 'Paragraph'){
      $scope.addParagraph();
    }
    if(el.name == 'Email'){
      $scope.addEmailForm();
    }
    if(el.name == 'Url'){
      $scope.addSiteForm();
    }
    if(el.name == 'File'){
      $scope.addFileForm();
    }
    if(el.name == 'Date'){
      $scope.addDateForm();
    }
    if(el.name == 'Dropdown'){
      $scope.addDropdownForm();
    }
    if(el.name == 'Sub checklist'){
      $scope.addSubchecklist();
    }
    if(el.name == 'Send Email'){
      $scope.addEmail();
    }
  };



  $scope.getCrts = function () {
    myHttpService.get('get_crts')
      .then(function (res) {

        $scope.crts = res.data; //.message;
        console.log($scope.crts, 'get_crts');

      });
  }
  $scope.getCrts();

  $scope.blockageUpdated = false;
  $scope.blockTask = function(d, i, autoSave = true){
    if(autoSave){
      d.blocked = (d.blocked == '0') ? '1' : '0';
    
      var data = {
        task_id: d.id,
        sort: (d.task_answer) ? d.task_answer.sort : '0',
        incident_id: $scope.incident_id,
        user_id: $scope.myInfo.login_id,
        answer: (d.task_answer) ? d.task_answer.answer : false,
        blocked: d.blocked
      };
  
      $scope.postMethod(data, 'save_task_block').then(function (res) {
        console.log(res, 'save_task_block completeTask');
  
      });
      $scope.blockageUpdated = true;
    }
    console.log(d, i, 'blockTask');


  }

  $scope.changeTaskStatus = function (t, i, checked, in_progress) {
    var h = $scope.activeHeader;
    $scope.activeTaskInfo.loading = true;

    // var checked = checked;
    // var in_progress = in_progress;

    if ($scope.incident_id) {
      var data = {
        task_id: t.id,
        sort: in_progress,
        incident_id: $scope.incident_id,
        user_id: $scope.myInfo.login_id,
        answer: checked
      };

      $scope.postMethod(data, 'save_task_answer').then(function (res) {
        console.log(res, 'save_task_answer completeTask');
        $scope.activeTaskInfo.loading = false;
        $scope.activeTaskInfo.checked = checked;
        $scope.activeTaskInfo.in_progress = (in_progress == '1') ? true : false;
        $scope.activeTaskInfo.task_answer = res.data;
      });
    } else {
      $scope.activeTaskInfo.checked = checked;
    }
  }

  $scope.commentModel = {
    id: '',
    message: '',
    user_id: '',
    created_at: new Date(),
    author: null
  }

  $scope.saveComment = function (c) {
    console.log(c, $scope.activeTaskInfo, '$scope.activeTaskInfo');
    var message = c;
    $scope.activeTaskInfo.theComment = '';


    var d = $scope.commentModel;
    var t = angular.copy(d);
    t.author = $scope.myInfo;
    t.recall_id = $scope.activeTaskInfo.incident_id;
    t.task_id = $scope.activeTaskInfo.id;
    t.user_id = $scope.myInfo.login_id;
    t.message = c;
    console.log(t);

    $scope.activeTaskInfo.commentSubmitting = true;

    if ($scope.activeTaskInfo.incident_id) {
      $scope.postMethod(t, 'save_task_comment').then(function (res) {
        $scope.activeTaskInfo.commentSubmitting = false;
        $scope.activeTaskInfo.comments.push(t);
      });
    } else {
      $scope.activeTaskInfo.commentSubmitting = false;
      $scope.activeTaskInfo.comments.push(t);
    }

  }

  $scope.sendTaskContentEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (re.test(email.email)) {
      $scope.postMethod(email, 'send_content_email').then(function (res) {
        console.log('send_content_email', res);
        bootbox.alert('Email sent successfully.');
      });
    } else {
      bootbox.alert('Email is invalid.');
    }
  }

  $scope.saveAssigned = function () { //kanban modal
    $scope.assignPopSettings.isOpen = false;

    if ($scope.activeTaskInfo.assigned.crt_email) {
      $scope.activeTaskInfo.assigned_id = $scope.activeTaskInfo.assigned.login_id;
      $scope.activeTaskInfo.assigned_info = $scope.activeTaskInfo.assigned;
    } else {
      $scope.activeTaskInfo.assigned_id = '0';
      $scope.activeTaskInfo.assigned_info = null;
    }
    $scope.save(true);

    console.log('assign save modal kanban');
  }

  $scope.saveDate = function () {
    var fdate = $filter('date')($scope.activeTaskInfo.due_date, "yyyy-MM-dd");
    console.log(fdate, 'fdate');
    // $scope.datePopSettings.isOpen = false;
    $scope.save(true);

  }

  $scope.removeDate = function () {
    // $scope.activeTaskInfo.due_date = null;
    $scope.activeTaskInfo.months = '';
    $scope.activeTaskInfo.days = '';
    $scope.activeTaskInfo.hours = '';
    $scope.activeTaskInfo.mins = '';
  }

  //-- Methods --//
  $scope.cancel = function () {
    if($scope.blockageUpdated){
      $scope.blockTask($scope.activeTaskInfo, $scope.activeTaskIndex);
    }
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function (autoSave = false) { //save kanban modal
    $scope.loading = true;
    // $uibModalInstance.close(res.data);
    // console.log(res.data); 

    var tsk = $scope.activeTaskInfo;

    var task_data = {
      id: (tsk.id) ? tsk.id : '',
      question: tsk.question,
      assigned_id: tsk.assigned_id,
      months: tsk.months,
      days: tsk.days,
      hours: tsk.hours,
      mins: tsk.mins,
      // due_time: tsk.due_time,
      // due_date: fdate,
      // arrangement: index_task,
      recall_pack_id: tsk.recall_pack_id,
      step_no: tsk.step_no
    };

    $scope.postMethod(task_data, 'save_tasks').then(function (res) {

      if (!$scope.contents.length) {
        var dataToSend = {
          contents: $scope.contents,
          task: $scope.activeTaskInfo
        };

        if(!autoSave){
          $uibModalInstance.close(dataToSend);
          $scope.loading = false;          
        }
        else{
          // $scope.showToast('success', 'Auto-saved..');
        }
        
        // console.log(res.data); 
      } else {
        save_contents();
      }

    });

    console.log($scope.data, '$scope.data');

    var save_contents = function () {
      var promises_contents = [];


      // var the_task = $scope.headerTasks[index_header].tasks[index_task];
      // console.log(the_task, 'the_task', task_id, index_task, index_header);

      // if(typeof(the_task) != 'undefined'){
      $scope.contents.forEach(function (cnt, index, array) {
        var sortt = index;
        var modulee = cnt.module;
        var checklist_id = $scope.data.task.id;
        var cnt_data = {
          'id': cnt.id,
          'checklist_id': checklist_id,
          'module': modulee,
          'sort': sortt,
          'content': cnt.content,
        };
        promises_contents.push($scope.postMethod(cnt_data, 'save_contents'));

      });

      $q.all(promises_contents).then(function (results) {
        results.forEach(function (currentValue, index, array) {
          console.log(currentValue.data, index, 'promises_contents');

          save_fields(currentValue.data.id, index);
          if ($scope.contents.length == index + 1) {
            var dataToSend = {
              contents: $scope.contents,
              task: $scope.activeTaskInfo
            };

            if(!autoSave){
              $uibModalInstance.close(dataToSend);
              $scope.loading = false;
            }
            else{
              // $scope.showToast('success', 'Auto-saved..');
            }
            // console.log(res.data); 
          }
        });
      });
      // }

    }

    var save_fields = function (content_id, index_content, index_task, index_header) {
      var promises_fields = [];

      var task_length = 0; //$scope.headerTasks[index_header].tasks.length;
      var the_content = $scope.contents[index_content];
      var cnt = the_content;
      var field = the_content.field;
      var options = the_content.options;
      var email = the_content.email;

      if ((typeof (field) != 'undefined') && field.length > 0) {
        field.forEach(function (fld, icf, array) {

          var fld = field[icf];
          fld.content_id = content_id;
          var fldsSave = null;

          var field_data = {
            'id': fld.id,
            'text': fld.text,
            'content_id': fld.content_id,
            'type': fld.type,
          };

          console.log(fld, 'fld');

          fldsSave = $scope.postMethod(field_data, 'save_fields');
          promises_fields.push(fldsSave);

        });

      } else if ((typeof (options) != 'undefined') && options.length > 0) {
        options.forEach(function (opts, ico, array) {
          var opts = options[ico];
          opts.content_id = content_id;
          var optsSave = null;

          var opts_data = {
            'id': opts.id,
            'content_id': opts.content_id,
            'text': opts.text,
            'type': opts.type,
          };

          optsSave = $scope.postMethod(opts_data, 'save_options');
          promises_fields.push(optsSave);

        });


      }

      //save if email
      else if (cnt.module == 'email') {
        var eml = cnt.email;
        eml.content_id = content_id;

        var emlSave = null;

        emlSave = $scope.postMethod(eml, 'save_emails');
        promises_fields.push(emlSave);

      }

      $q.all(promises_fields).then(function (results) {

        $scope.loading = false;
        // console.log(promises.length, index_header, task_length, index_task, ', index_header, task_length, index_task');
        // if(promises.length == (index_header + 1) && task_length == (index_task + 1)){
        //     $scope.showStandbySuccess();
        //     $scope.saveStandbyShown = true;

        //   }

        // //show save if no content
        // if(promises_fields.length == 0){
        //   if(index_header == 0 && index_task == 0){
        //     $scope.showStandbySuccess();
        //     $scope.saveStandbyShown = true;

        //   }
        // }
        // results.forEach(function(currentValue, index, array){
        //   console.log(currentValue.data, index, 'promises_fields');
        // });
      });

    }


  }; // end save



  //contents actions


  //quill
  $scope.title = 'Quill works'


  self.quillMods = {
    // height: 50,
    toolbar: [
      ['bold', 'italic', 'underline'],
      ['link'],
      [{
        'list': 'ordered'
      }, {
        'list': 'bullet'
      }], // superscript/subscript
      [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }],
      [{
        'header': [1, 2, 3, 4, 5, 6, false]
      }],
      ['clean'],
    ]
  };

  self.quillModsImage = {
    toolbar: [
      ['image']
    ]
  };

  self.quillModsVideo = {
    toolbar: [
      ['video']
    ]
  };

  $(document).on('click', '.ql-editor', function (i) {
    console.log($(this).find('.ql-clean').length, i, 'i');
    $(this).closest('.editor-holder').find('.ql-clean').click();
  });

  $scope.sortableTableOptions = {
    handle: '> .num div',
    placeholder: "ui-state-highlight"
  }

  $scope.sortableContentOptions = {
    handle: '> .move-handle',
    placeholder: "ui-state-highlight"
  }

  $scope.assignCrt = function (i) {
    $scope.isAssigned = i;
  }

  $scope.getQuillModule = function (i) {
    if (i == 'video') {
      return self.quillModsVideo;
    } else if (i == 'image') {
      return self.quillModsImage;
    } else {
      return self.quillMods;
    }
  }


  self.contentModel = {
    id: '',
    show: false,
    content: '',
    email: null,
    sort: 1,
    field: [],
    options: [],
    checklist_id: $scope.activeTask,
    module: 'text'
  }


  self.fieldModel = {
    id: '',
    text: '',
    content_id: 0,
    type: 'sublist'
  }

  $scope.postMethod = function (data, db) {
    return myHttpService.post(data, db);
  }
  
  $scope.optionsSummer = {
    height: 250
  };

  $scope.newContentScroll = function (save = false) {
    // var $container = $(document).find('.modal-body-kbcontent');
    // $container.stop().animate({
    //   scrollTop: $container[0].scrollHeight
    // }, 800);
    if(save){
      $scope.save(true);
    }
  }

  $scope.addText = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    $scope.contents.push(t);
    console.log(t, 'addText');
    var h = $scope.activeHeader;
    var tsk = $scope.activeTaskIndex;

    $scope.newContentScroll();
    // $scope.activeTaskInfo.contents = $scope.contents;
    // $scope.headerTasks[h].tasks[tsk].contents = $scope.contents;
  }


  $scope.removeContent = function (i, content) {
    if (content.id) {
      $scope.postMethod({
        id: content.id
      }, 'delete_content').then(function (res) {
        console.log('delete', res);
        // this.currentTask.contents = this.contents;
        $scope.contents.splice(i, 1);
      });
    } else {
      $scope.contents.splice(i, 1);
    }

  };


  $scope.moveContent = function (i, dir) {
    if (dir == 'up') {
      this.swapArrayElements(this.contents, i, i - 1);
    } else {
      this.swapArrayElements(this.contents, i, i + 1);
    }

  }

  $scope.copyContent = function (c, i) {
    var data = Object.assign({}, c)
    data.id = '';
    this.contents.splice(i, 0, data);
  }

  $scope.swapArrayElements = function (arr, indexA, indexB) {
    var temp = arr[indexA];
    arr[indexA] = arr[indexB];
    arr[indexB] = temp;
    return arr;
  };

  $scope.keyDownSelect = function (e, d, isl, i) {
    if (e.keyCode == 13) {
      console.log(e, d, isl, i, '');
      var newIndex = isl + 1;

      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'option';

      // var data = this.fieldModel('option');
      $scope.contents[i].options.splice(newIndex, 0, t);
      // this.contents.splice(i, 0, data);
      console.log(e, d, isl, i);
    }

  }

  $scope.keyDownSublist = function (e, d, isl, i) {
    if (e.keyCode == 13) {
      console.log(e, d, isl, i, 'keyDownsublist2');
      var newIndex = isl + 1;

      var t = self.fieldModel;
      t = Object.assign({}, t);

      this.contents[i].field.splice(newIndex, 0, t);
      // this.contents.splice(i, 0, data);
    }

  }


  $scope.htmlSafe = function (data) {
    return $sce.trustAsHtml(data);
  }

  $scope.addImage = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'image';
    $scope.contents.push(t);

    $scope.newContentScroll();
    // $scope.activeTaskInfo.contents = $scope.contents;
    console.log($('.ql-image'));
  }

  $scope.showImagePop = function (c, i) {
    // var i2 = $('.ql-image').length - 1;
    // $('.ql-image:eq('+i2+')').click();
    console.log(c, i);
  }

  $scope.videoUrl = function (v) {
    var newv = v;
    if (v.indexOf('youtube.com') > -1) {
      newv = $scope.checkYoutube(v);
    }

    return $sce.trustAsResourceUrl(newv);
    // console.log($sce.trustAsResourceUrl(v));
  }

  $scope.checkYoutube = function (l) {
    var upd = l;
    var partsOfStr = l.split('v=');
    if (partsOfStr.length > 1) {
      upd = 'https://www.youtube.com/embed/' + partsOfStr[1];
    }

    return upd;

  }


  $scope.addVideo = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'video';
    this.contents.push(t);
    // this.currentTask.contents = this.contents;

    $scope.newContentScroll();
    setTimeout(() => {
      var i = $('.ql-video').length - 1;
      $('.ql-video:eq(' + i + ')').click();
    }, 5);
    console.log($('.ql-video'));
  }


  $scope.addSubchecklist = function () {
    var text = [];
    for (var i = 0; i < 3; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      text.push(t);
    }
    console.log(text, 'addSubchecklist');
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    console.log(t, 'sublist');
    this.contents.push(t);
    $scope.newContentScroll();

    // self.contents.push(t);
    // self.currentTask.contents = self.contents;
  }

  $scope.addDropdownForm = function () {
    var text = [];
    var opts = [];
    // for(var i = 0; i < 1; i++){
    //     var t = self.fieldModel;
    //     t = Object.assign({}, t);
    //     t.type = 'select';
    //     text.push(t);
    // }
    for (var i = 0; i < 3; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'option';
      opts.push(t);
    }
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'select';
    t.field = text;
    t.options = opts;
    this.contents.push(t);
    $scope.newContentScroll();
    // this.currentTask.contents = this.contents;
  }


  $scope.addParagraph = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'paragraph';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);
    $scope.newContentScroll();
    // this.currentTask.contents = this.contents;
  }

  $scope.addEmailForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'email';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);
    $scope.newContentScroll();
  }

  $scope.addSiteForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'url';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);
    $scope.newContentScroll();
  }


  $scope.addFileForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'file';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);
    $scope.newContentScroll();
  }

  $scope.addDateForm = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'date';
      text.push(t);

    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);
    $scope.newContentScroll();
  }

  $scope.addSingleLine = function () {
    var text = [];
    for (var i = 0; i < 1; i++) {
      var t = self.fieldModel;
      t = Object.assign({}, t);
      t.type = 'single';
      text.push(t);
    }

    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'list';
    t.field = text;
    this.contents.push(t);
    $scope.newContentScroll();
  }


  $scope.addEmail = function () {
    var t = self.contentModel;
    t = Object.assign({}, t);
    t.module = 'email';
    this.contents.push(t);
    $scope.newContentScroll();
  }



});



crisisApp.controller('editTaskDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, $dialogConfirm) {
  //-- Variables --//
  $scope.headers = data.h;
  $scope.data = data.d;
  $scope.hIndex = data.i;
  $scope.stepnumber = {};

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel


  $scope.save = function () {
    $scope.data.loading = true;

    if ($scope.headers) {
      $scope.data.step_no = $scope.stepnumber.header.step_no;
      $scope.data.header = $scope.stepnumber.header;
    }
    console.log($scope.data, $scope.stepnumber, 'sdfdsf s');

    // return false;
    var task_data = {
      id: ($scope.data.id) ? $scope.data.id : '',
      question: $scope.data.question,
      assigned_id: ($scope.data.assigned_id) ? $scope.data.assigned_id : '',
      months: ($scope.data.months) ? $scope.data.months : '',
      days: ($scope.data.days) ? $scope.data.days : '',
      hours: ($scope.data.hours) ? $scope.data.hours : '',
      mins: ($scope.data.mins) ? $scope.data.mins : '',
      // due_time: $scope.data.due_time,
      // due_date: fdate,
      arrangement: ($scope.hIndex) ? $scope.hIndex : '',
      recall_pack_id: $scope.data.recall_pack_id,
      step_no: $scope.data.step_no
    };

    // promises_tasks.push($scope.postMethod(task_data, 'save_tasks'));

    myHttpService.post(task_data, 'save_tasks')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        var r = {
          data: res.data,
          header: $scope.stepnumber.header
        };
        if ($scope.headers) {
          $uibModalInstance.close(r);
        } else {
          $uibModalInstance.close(res.data);
        }
        console.log(res.data);
      });
  }; // end save

});


crisisApp.controller('editHeaderDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, $dialogConfirm) {
  //-- Variables --//
  $scope.data = data.d;
  $scope.hIndex = data.i;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.postMethod = function (data, db) {
    return myHttpService.post(data, db);
  }

  $scope.delete = function () {
    console.log('nope');

    $dialogConfirm(action_messages.standard.delete_sb_step).then(function () {

      $scope.postMethod($scope.data, 'delete_header').then(function (res) {
        console.log('delete_header', res);
        $uibModalInstance.close({
          delete: true
        });

      });

    }, function () {
      console.log('nope');
    });
  }

  $scope.save = function () {
    $scope.data.loading = true;

    var theader_data = {
      id: ($scope.data.id) ? $scope.data.id : '',
      name: $scope.data.name,
      order: $scope.hIndex, //cl.order,
      recall_pack_id: $scope.data.recall_pack_id,
      step_no: $scope.data.step_no
    };

    myHttpService.post(theader_data, 'save_task_headers')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;

        $uibModalInstance.close(res.data);
        console.log(res.data);
      });
  }; // end save

});


crisisApp.controller('nameDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService) {
  //-- Variables --//
  $scope.data = data;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $scope.data.loading = true;
    myHttpService.post($scope.data, 'save_name')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        $scope.data.full_name = $scope.data.first_name + ' ' + $scope.data.last_name;

        $uibModalInstance.close($scope.data);
        console.log($scope.data);
      });
  }; // end save

});

crisisApp.controller('passwordDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, dialogs) {
  //-- Variables --//
  $scope.data = {
    error: false,
    loading: false,
    login_id: data.login_id,
    current: '',
    password: '',
    password2: ''
  };

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $scope.data.loading = true;

    if ($scope.data.password == $scope.data.password2) {
      myHttpService.post($scope.data, 'save_password')
        .then(function (res) {
          console.log(res);
          $scope.data.loading = false;

          if (res.data.count) {
            $scope.data.error = false;
            $uibModalInstance.close($scope.data);
            console.log($scope.data);
          } else {
            $scope.data.error = true;
          }

        });
    } else {
      $scope.data.loading = false;
      $scope.data.error = true;
    }

  }; // end save

});



crisisApp.controller('uinfoDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, dialogs) {
  //-- Variables --//
  $scope.data = data.user;
  $scope.countries = data.countries;
  $scope.type = data.type;
  console.log($scope.data, '$scope.data dialog');

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel


  $scope.changeCc = function (d) {
    console.log(d);
    $scope.data.countrycode = d.calling_code;
  }

  $scope.save = function () {
    $scope.data.loading = true;
    $scope.data.type = $scope.type;
    $scope.data.crt_mobile = $scope.data.countrycode + $scope.data.crt_digits;

    if ($scope.data.type == 'Location') {
      $scope.data.location = $('.uinfo_geolocation').val();
      $scope.data.location_lat = $('.uinfo_geolocation').siblings('[name="lat"]').val();
      $scope.data.location_lng = $('.uinfo_geolocation').siblings('[name="lng"]').val();
      console.log($('.uinfo_geolocation').val(), $('.uinfo_geolocation').siblings('[name="lat"]').val(), 'uinfo_geolocation');
    }

    myHttpService.post($scope.data, 'save_uinfo')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;

        $uibModalInstance.close($scope.data);
        console.log($scope.data);

      });


  }; // end save

});
crisisApp.controller('emailDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, dialogs) {
  //-- Variables --//
  $scope.data = data;
  console.log($scope.data, '$scope.data dialog');

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $scope.data.loading = true;

    myHttpService.post($scope.data, 'save_email')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;

        if (res.data.count) {
          $scope.data.error = false;
          $uibModalInstance.close($scope.data);
          console.log($scope.data);
        } else {
          $scope.data.error = true;
        }

      });


  }; // end save

});

crisisApp.controller('timezoneDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, dialogs) {
  //-- Variables --//
  $scope.data = data.user;

  $scope.timeZone = data.timeZone;
  console.log($scope.data, $scope.timeZone, '$scope.data dialog');

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.changeTz = function (d) {
    console.log(d);
    $scope.data.timezone = d.ci_timezone;
  }

  $scope.save = function () {
    $scope.data.loading = true;

    myHttpService.post($scope.data, 'save_timezone')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        $uibModalInstance.close($scope.data);
        console.log($scope.data);

      });
  }; // end save

});

crisisApp.controller('paymentDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, dialogs) {
  //-- Variables --//
  $scope.base_url = window.base_url;
  $scope.data = data.user;

  if (typeof ($scope.data.country_details) == 'undefined' || typeof ($scope.data.country_details.country_id) == 'undefined') {
    $scope.data.country_details = {
      country_id: "14",
      iso2: "AU",
      short_name: "Australia",
      long_name: "Commonwealth of Australia",
      iso3: "AUS",
      numcode: "036",
      un_member: "yes",
      calling_code: "61",
      cctld: ".au",
      short_calling: "Australia (+61)"
    };
  }

  $scope.countries = data.countries;
  $scope.payment = {
    name: $scope.data.full_name,
    exp_month: '01',
    exp_year: '2017',
    address_country: 'Australia'
  };

  if (data.paymentInfo) {
    $scope.payment = data.paymentInfo;
  }



  $scope.error = null;

  console.log($scope.data, $scope.countries, $scope.base_url, '$scope.data paymentDialogCtrl');

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.changeCountry = function (d) {
    console.log(d);
    $scope.payment.address_country = d.short_name;
    $scope.payment.country = d.iso2;
  }

  $scope.save = function () {
    $scope.data.loading = true;


    var PublishableKey = 'pk_test_2g76COgjgp2DPnHPHdxrRCZc'; // Replace with your API publishable key
    Stripe.setPublishableKey(PublishableKey);
    Stripe.card.createToken($scope.payment, function (status, response) {
      console.log(response, status, 'stripeResponseHandler');

      if (response.error) { // Problem!
        $scope.error = response.error;
        $scope.data.loading = false;
        console.log($scope.error, 'Problem');
      } else { // Token was created!
        // Get the token ID:
        $scope.error = null;
        var token = response.id;
        console.log('token', token);
        response.card.token = token;
        // response.card.selected_plan = 0;

        myHttpService.post(response.card, 'save_stripe_card')
          .then(function (res) {
            console.log(res);

            $scope.payment.id = res.data.id;
            $scope.payment.client_ip = response.client_ip;
            $scope.payment.created = response.created;
            $scope.payment.token = response.id;
            $scope.payment.livemode = response.livemode;
            $scope.payment.object = response.object;
            $scope.payment.type = response.type;
            myHttpService.post($scope.payment, 'save_form_payment')
              .then(function (res2) {
                console.log(res2);
                $scope.data.loading = false;
                $uibModalInstance.close(res2.data.format_billing);
                console.log(res2.data.format_billing);
                localStorage.setItem('crisisflo_billing', JSON.stringify(res2.data.format_billing));
                isUserPlanFree();
                isPlanUpgradable();
              });

          });

      }
    });


  }; // end save

});

crisisApp.controller('billingInfoDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService, dialogs) {
  //-- Variables --//
  $scope.base_url = window.base_url;

  $scope.data = {
    login_id: data.user.login_id,
    crt_email: data.user.crt_email,
    first_name: data.user.first_name,
    last_name: data.user.last_name,
    alt_email: '',
    company: '',
    receipt: true,
  };

  if (data.billingContact) {
    $scope.data = data.billingContact;
  }

  console.log($scope.data, $scope.countries, $scope.base_url, '$scope.data billingInfoDialogCtrl');

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.changeTz = function (d) {
    console.log(d);
    $scope.data.timezone = d.ci_timezone;
  }

  $scope.save = function () {
    $scope.data.loading = true;

    myHttpService.post($scope.data, 'save_billing_contact')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        $uibModalInstance.close($scope.data);
        console.log($scope.data);
      });
  }; // end save

});

crisisApp.controller('accountController', function accountController($scope, myHttpService, $timeout, $compile, $sce, $q, $filter, $dialogConfirm, dialogs, $rootScope) {

  $scope.myInfo = null;
  $scope.timeZone = null;
  $scope.countries = null;
  $scope.paymentInfo = null;
  $scope.billingContact = null;
  $scope.transactions = null;
  $scope.invoice = null;
  $scope.activeTab = (window.tab_active) ? parseInt(window.tab_active) : 0;

  console.log($scope.activeTab, '$scope.activeTab');
  $scope.setActiveTab = function (index) {
    $scope.activeTab = index;
  }

  $scope.getMyInfo = function (refocus = true) {
    myHttpService.get('logged_info')
      .then(function (res) {
        $scope.myInfo = res.data; //.message;
        console.log($scope.myInfo, 'logged_info');

      });
  }
  $scope.getMyInfo();

  $scope.getTimeZone = function (refocus = true) {
    myHttpService.get('get_timezone')
      .then(function (res) {
        $scope.timeZone = res.data; //.message;
        console.log($scope.timeZone, 'get_timezone');

      });
  }
  $scope.getTimeZone();

  $scope.getCountries = function (refocus = true) {
    myHttpService.get('get_countries')
      .then(function (res) {
        $scope.countries = res.data; //.message;
        console.log($scope.countries, 'get_countries');

      });
  }
  $scope.getCountries();

  $scope.getPaymentInfo = function (refocus = true) {
    myHttpService.get('get_billing_info')
      .then(function (res) {
        $scope.paymentInfo = (typeof (res.data) != 'null' && res.data != 'null') ? res.data : {selected_plan: '0'}; //.message;
        console.log($scope.paymentInfo, 'get_billing_info');
        localStorage.setItem('crisisflo_billing', JSON.stringify($scope.paymentInfo));
      });
  }
  $scope.getPaymentInfo();

  $scope.getBillingContact = function (refocus = true) {
    myHttpService.get('get_billing_contact')
      .then(function (res) {
        $scope.billingContact = (typeof (res.data) != 'null' && res.data != 'null') ? res.data : null; //.message;
        console.log($scope.billingContact, 'get_billing_contact');
      });
  }
  $scope.getBillingContact();

  $scope.getTransactions = function (refocus = true) {
    myHttpService.get('get_transactions')
      .then(function (res) {
        $scope.transaction = (typeof (res.data) != 'null' && res.data != 'null') ? res.data : null; //.message;
        console.log($scope.transaction, 'get_transactions');


        //check invoice
        var i = window.uri_4;
        if (!i) {
          $scope.invoice = $scope.transaction.billing;
        } else {
          $scope.invoice = $scope.transaction.history[i].content;
        }
        console.log($scope.invoice, '$scope.invoice');
      });
  }
  $scope.getTransactions();

  $scope.postMethod = function (data, db) {
    return myHttpService.post(data, db);
  }

  $scope.cancelSubscription = function () {
    var message = 'Your subscription will be cancelled at the end of the current calendar month. Confirm you wish to proceed with cancellation?'; //Confirm you wish to cancel your subscription at expiry date (' + $scope.transaction.billing.next_month_format + ')?';

    $dialogConfirm(message).then(function () {
      console.log('yeahh');

      $scope.postMethod({}, 'cancel_subscription').then(function (res) {
        console.log('cancel_subscription', res);
        $scope.myInfo.subscription_cancelled = '1';
      });
    });
  }


  $scope.reactivateSubscription = function () {
    var message = 'Confirm you wish to reactivate your subscription?';

    $dialogConfirm(message).then(function () {
      console.log('yeahh');

      $scope.postMethod({}, 'reactivate_subscription').then(function (res) {
        console.log('reactivate_subscription', res);
        $scope.myInfo.subscription_cancelled = '0';
      });
    });
  }


  $scope.launch = function (which, type = null, paymentPlan = 'default') {
    switch (which) {
      case 'error':
        // dialogs.error('No', 'yea', {size: 'md'});
        dialogs.notify('Connect Social Account', 'connecting..', {
          size: 'md'
        });
        break;
      case 'notify':
        dialogs.notify('Colol');
        break;
      case 'confirm':
        var dlg = dialogs.confirm();
        dlg.result.then(function (btn) {
          $scope.confirmed = 'You confirmed "Yes."';
        }, function (btn) {
          $scope.confirmed = 'You confirmed "No."';
        });
        break;
      case 'name':
        var dlg = dialogs.create('nameModal.html', 'nameDialogCtrl', $scope.myInfo, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          console.log(data, 'dlg.result');
          $scope.myInfo = data;
        }, function () {});
        break;

      case 'password':
        var dlg = dialogs.create('passwordModal.html', 'passwordDialogCtrl', $scope.myInfo, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          console.log(data, 'dlg.result');
        }, function () {});
        break;

      case 'email':
        var dlg = dialogs.create('emailModal.html', 'emailDialogCtrl', $scope.myInfo, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          $scope.myInfo.crt_email = data.crt_email;
          $scope.myInfo.email = data.crt_email;
          console.log(data, 'dlg.result');
        }, function () {});
        break;

      case 'uinfo':
        var dlg = dialogs.create('uinfoModal.html', 'uinfoDialogCtrl', {
          user: $scope.myInfo,
          countries: $scope.countries,
          type: type
        }, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          $scope.myInfo = data;
          console.log(data, 'dlg.result');
        }, function () {});
        break;

      case 'timezone':
        var dlg = dialogs.create('timezoneModal.html', 'timezoneDialogCtrl', {
          user: $scope.myInfo,
          timeZone: $scope.timeZone
        }, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          $scope.myInfo.timezone = data.timezone;
          $scope.myInfo.timezone_details = data.timezone_details;
          console.log(data, 'dlg.result');
        }, function () {});
        break;

      case 'payment':
        let paymentInfoCopy = ($scope.paymentInfo) ? angular.copy($scope.paymentInfo) : {};
        paymentInfoCopy.selected_plan =  (paymentInfoCopy.selected_plan && paymentPlan == 'default') ? paymentInfoCopy.selected_plan : paymentPlan;

        var dlg = dialogs.create('paymentModal.html', 'paymentDialogCtrl', {
          user: $scope.myInfo,
          countries: $scope.countries,
          paymentInfo: paymentInfoCopy
        }, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          $scope.paymentInfo = data;
          console.log(data, 'dlg.result');
        }, function () {});
        break;

      case 'billingInfo':
        var dlg = dialogs.create('billingInfoModal.html', 'billingInfoDialogCtrl', {
          user: $scope.myInfo,
          billingContact: $scope.billingContact
        }, {
          size: 'md',
          keyboard: true,
          backdrop: true,
          windowClass: 'my-class'
        });

        dlg.result.then(function (data) {
          $scope.billingContact = data;

          console.log(data, 'dlg.result');
        }, function () {});
        break;
    }
  }; // end launch

});



crisisApp.controller('templateDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService) {
  //-- Variables --//
  $scope.data = data;
  $scope.templates = data.templates;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.addTaskTemplate = function () {
    $uibModalInstance.close({});
  }; // end save

});

crisisApp.controller('incidentsDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService) {
  //-- Variables --//
  $scope.data = data;
  $scope.incidents = data.incidents;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.addIncident = function () {
    $uibModalInstance.close({});

  }; // end save

});



crisisApp.controller('newStandbyDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService) {
  //-- Variables --//
  $scope.data = data;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $scope.data.loading = true;
    $scope.data.user_type = window.uri_1;

    console.log($scope.data);

    myHttpService.post($scope.data, 'save_template')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        $uibModalInstance.close(res.data);
        console.log($scope.data);
      });
  }; // end save

});



crisisApp.controller('addTaskTemplateDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService) {
  //-- Variables --//
  $scope.data = data;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $scope.data.loading = true;
    $scope.data.user_type = window.uri_1;

    myHttpService.post($scope.data, 'save_template')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        // $scope.data.full_name = $scope.data.first_name+' '+$scope.data.last_name;

        $uibModalInstance.close(res.data);
        console.log($scope.data);
      });


    // window.location.href = window.base_url + 'cc/recall/manage?description='+$scope.data.description;
  }; // end save

});

crisisApp.controller('raiseDialogCtrl', function ($scope, $uibModalInstance, data, myHttpService) {
  //-- Variables --//
  $scope.data = {
    description: ''
  };
  $scope.templates = data.templates;
  $scope.incident = data.incident;

  //-- Methods --//
  $scope.cancel = function () {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.ok = function() {
    myHttpService.post({
      incident_id: $scope.incident.id,
      module: $scope.incident.module
    }, 'comment_incident')
    .then(function (res) {
      $uibModalInstance.close(true);
    });
  };

  $scope.save = function () {
    $scope.data.loading = true;
    myHttpService.post($scope.data, 'save_incident')
      .then(function (res) {
        console.log(res);
        $scope.data.loading = false;
        // $scope.data.full_name = $scope.data.first_name+' '+$scope.data.last_name;

        $uibModalInstance.close(res.data);
        console.log($scope.data);
      });


    // window.location.href = window.base_url + 'cc/recall/manage?description='+$scope.data.description;
  }; // end save

});

crisisApp.controller('dashboardController', function dashboardController($scope, myHttpService, $timeout, $compile, $sce, $q, $filter, $dialogConfirm, dialogs, $rootScope, ngToast) {
  $scope.templates = [];
  $scope.crts = [];
  $scope.incidents = [];
  $scope.submitted_incidents = [];
  $scope.submitted_incidents_history = [];
  $scope.incidentsLoaded = false;
  $scope.incident_type = window.uri_2;
  console.log(window.uri_2, 'window.uri_2');
  $scope.myInfo = null;

  $scope.getCrts = function () {
    myHttpService.get('get_crts')
      .then(function (res) {

        $scope.crts = res.data; //.message;
        console.log($scope.crts, 'get_crts');
        if(isUserPlanFree() && $scope.crts.length > 3){
          bootboxalert('Number of users exceeded for selected plan', function(){
            window.location.href = base_url + 'cc/crisisteam/managecrt';
          })
        }
      });
  }
  $scope.getCrts();

  $scope.getMyInfo = function (refocus = true) {
    myHttpService.get('logged_info')
      .then(function (res) {
        $scope.myInfo = res.data; //.message;
        console.log($scope.myInfo, 'logged_info');
      });
  }
  $scope.getMyInfo();

  $scope.goToLink = function (link) {
    window.location.href = window.base_url + link;
  }


  $scope.formatElapsedMoment = function (incident, closed = false) {
    // console.log(d);
    // var m = moment.preciseDiff(d.incident_date, d.task_answer.created_at, true);
    var m = (closed) ? moment.preciseDiff(incident.closed_date, incident.initiation_date, true) : moment.preciseDiff(new Date(), incident.initiation_date, true);

    var y = m.years ? m.years + 'year ' : '';
    var mo = m.months ? m.months + 'month ' : '';
    var d = m.days ? m.days + 'day ' : '';
    var h = m.hours ? m.hours + 'hr ' : '';
    var mi = m.minutes ? m.minutes + 'min' : '';
    
    y = (m.years > 1) ? m.years + 'years ' : y;
    mo = (m.months > 1) ? m.months + 'months ' : mo;
    d = (m.days > 1) ? m.days + 'days ' : d;
    h = (m.hours > 1) ? m.hours + 'hrs ' : h;
    mi = (m.minutes > 1) ? m.minutes + 'mins' : mi;
    var txt = y + mo + d + h + mi;
    


    return txt ? txt : 'less than a minute';
  }

  $scope.sortableOptions = {
    // handle: '> .num div',
    placeholder: "ui-state-highlight2",
    'ui-floating': true
  }

  $scope.getIncidents = function () {
    myHttpService.getWithParams('get_incidents', {
        incident_type: $scope.incident_type
      })
      .then(function (res) {
        console.log($scope.incidents, 'get_incidents');

        $scope.incidents = res.data; //.message;
        $scope.incidentsLoaded = true;
        localStorage.setItem('crisisflo_incident_length', $scope.incidents.length);

        // if ($scope.incidents.length) {
        //   $scope.dashItems[0].title = 'Open Incidents';
        //   $scope.dashItems[0].text = $scope.incidents.length;
        // }
      });
  }
  $scope.getIncidents();



  $scope.getSubmittedIncidents = function () {
    myHttpService.getWithParams('get_submitted_incident', {})
      .then(function (res) {
        $scope.submitted_incidents = res.data; //.message;
        console.log($scope.submitted_incidents, 'get_submitted_incident');
      });
  }
  $scope.getSubmittedIncidents();

  $scope.getSubmittedIncidentsHistory = function () {
    myHttpService.getWithParams('get_submitted_incident_history', {})
      .then(function (res) {
        $scope.submitted_incidents_history = res.data; //.message;
        console.log($scope.submitted_incidents_history, 'get_submitted_incident_history');
      });
  }
  $scope.getSubmittedIncidentsHistory();

  
  $scope.showToast = function (className, content) {
    ngToast.create({
      className: (className) ? className : 'info',
      content: (content) ? content : 'Hi',
      dismissButton: true,
      dismissOnClick: true
    });
  }


  $scope.getTemplates = function () {
    myHttpService.getWithParams('get_templates', {
        user_type: $scope.logged_admin
      })
      .then(function (res) {
        $scope.templates = res.data; //.message;
        console.log($scope.templates, 'get_templates');
      });
  }
  $scope.getTemplates();

  $scope.archiveIncident = function(inci){
    myHttpService.post({
      incident_id: inci.id
    }, 'archive_incident')
    .then(function (res) {
      $scope.showToast('success', 'Update successful.');
      $scope.getSubmittedIncidentsHistory();
      $scope.getSubmittedIncidents();
    });
  }

  $scope.addComment = function(inci){
    bootbox.prompt({
      value: inci.module,
      title: "Add comment",
      callback: function(result){ 
        
        console.log(result);
        if (result) {
          myHttpService.post({
            incident_id: inci.id,
            module: result
          }, 'comment_incident')
          .then(function (res) {
            $scope.showToast('success', 'Comment added successfully.');
            $scope.getSubmittedIncidentsHistory();
            $scope.getSubmittedIncidents();
          });
        }
      },
    });
  }

  $scope.incidentFormOpen = function(incident){
    $('.add_incident_btn')[0].click();
  };

  $scope.showFormIncidentFields = function(incident) {
    var dlg = dialogs.create('formIncidentFields.html', 'raiseDialogCtrl', {
      incident: incident,
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      $scope.showToast('success', 'Comment added successfully.');
      $scope.getSubmittedIncidentsHistory();
      $scope.getSubmittedIncidents();
      // $scope.incidents.unshift(data);
      // console.log(data, 'dlg.result');
    }, function () {});
  }


  $scope.raiseTicket = function () {
    var dlg = dialogs.create('raiseModal.html', 'raiseDialogCtrl', {
      templates: $scope.templates
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      $scope.incidents.unshift(data);
      console.log(data, 'dlg.result');
    }, function () {});
  }

  $scope.addTaskTemplateModal = function () {
    var dlg = dialogs.create('addTaskTemplateModal.html', 'addTaskTemplateDialogCtrl', {}, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      window.location.href = window.base_url + 'cc/standbytasks/index/0/' + data.id + '?template_name=' + data.name;
      console.log(data, 'dlg.result');
    }, function () {});
  }

  $scope.incidentsModal = function () {
    var dlg = dialogs.create('incidentsModal.html', 'incidentsDialogCtrl', {
      incidents: $scope.incidents
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      $scope.raiseTicket();
    }, function () {});
  }

  $scope.templateModal = function () {
    var dlg = dialogs.create('templateModal.html', 'templateDialogCtrl', {
      templates: $scope.templates
    }, {
      size: 'md',
      keyboard: true,
      backdrop: true,
      windowClass: 'my-class'
    });

    dlg.result.then(function (data) {
      if(isUserPlanFree()){
        bootbox.alert('To access this feature, you need to update your plan.');
        return false;
      }
        
      $scope.addTaskTemplateModal();
    }, function () {});
  }

  $scope.deleteIncident = function (i, d) {
    $dialogConfirm("Confirm delete incident?").then(function () {
      myHttpService.post(d, 'delete_incident')
        .then(function (res) {
          $scope.incidents.splice(i, 1);
          console.log(res, 'delete_incident');
        });

    }, function () {
      console.log('nope');
    });
  }

  $scope.goToStandby = function () {
    $scope.templateModal();

    // if($scope.templates.length > 1){
    //   $scope.templateModal();
    // }
    // else{
    //   window.location.href = window.base_url+'cc/standbytasks/';
    // }
  }

  $scope.dashAction = function (i) {
    console.log(i, 'sdfdsfds');

    if (i.title == 'Open Incidents') {
      $scope.incidentsModal();
    }
    if (i.title == 'Status') {
      $('#newIncidentModal').modal('show');
    }
    if (i.title == 'My Team') {
      $scope.goToLink('cc/crisisteam/managecrt');
    }
    if (i.title == 'Wiki Space') {
      $scope.goToLink('cc/wiki');
    }
    if (i.title == 'Document Sharing') {
      $scope.goToLink('cc/wiki/sharing');
    }
    if (i.title == 'Standby Tasks') {
      $scope.goToStandby();
    }
    if (i.title == 'Post Incident Review') {
      if(isUserPlanFree()){
        bootbox.alert('To access this feature, you need to update your plan.');
        return false;
      }
        
      $scope.goToLink('cc/log');
    }
    if (i.title == 'Incident Form') {
      $scope.goToLink('cc/standbytasks/incident_form/edit/form');
    }
  }

  $scope.dashItems = [
    // {
    //   title: 'Status',
    //   text: 'Pre-incident',
    //   action: ''
    // },
    {
      title: 'My Team',
      text: '<i class="fa fa-users" aria-hidden="true"></i>',
      action: ''
    },
    {
      title: 'Wiki Space',
      text: '<img src="' + window.base_url + 'assets/img/wiki_icon.png" height="25">',
      // text: '<i class="fa fa-lightbulb-o" aria-hidden="true"></i>',
      action: ''
    },
    {
      title: 'Document Sharing',
      // text: '<i class="fa fa-share-alt" aria-hidden="true"></i>',
      text: '<img src="' + window.base_url + 'assets/img/share-ico.png" height="25">',
      action: ''
    },
    {
      title: 'Standby Tasks',
      text: '<i class="fa fa-power-off" aria-hidden="true"></i>',
      action: ''
    },
    {
      title: 'Post Incident Review',
      text: '<i class="fa fa-eye" aria-hidden="true"></i>',
      // text: '<img src="'+window.base_url+'assets/img/review-icon.png" height="22">',
      action: ''
    },{
      title: 'Incident Form',
      text: '<i class="fa fa-file-text" aria-hidden="true"></i>',
      action: 'add_incident_form_btn'
    },
  ];



});


crisisApp.controller('previewController', function previewController($scope, myHttpService, $timeout, $compile, $sce, $q, $filter, $dialogConfirm, dialogs, $rootScope) {

  $scope.activeTaskInfo = {};
  $scope.contents = [];
  $scope.getTask = function () {
    var d = window.localStorage.getItem('activeTaskInfo');
    var c = window.localStorage.getItem('activeContents');
    $scope.activeTaskInfo = JSON.parse(d);
    $scope.contents = JSON.parse(c);
    console.log(c, $scope.contents, $scope.activeTaskInfo, '$scope.activeTaskInfo');
  }
  $scope.getTask();

  $scope.alertMessage = function () {
    dialogs.notify('Task Preview', 'This version of the task will allow you to see exactly what the assigned user will see when they take the task, however, <strong>no data will be collected or recorded</strong>. The preview will allow you to test if your randomization and skip logic settings will work the way you want.', {
      size: 'md'
    });

  }
  $scope.alertMessage();


  $scope.htmlSafe = function (data) {
    return $sce.trustAsHtml(data);
  }

  $scope.videoUrl = function (v) {
    var newv = v;
    if (v.indexOf('youtube.com') > -1) {
      newv = $scope.checkYoutube(v);
    }

    return $sce.trustAsResourceUrl(newv);
    // console.log($sce.trustAsResourceUrl(v));
  }

  $scope.checkYoutube = function (l) {
    var upd = l;
    var partsOfStr = l.split('v=');
    if (partsOfStr.length > 1) {
      upd = 'https://www.youtube.com/embed/' + partsOfStr[1];
    }

    return upd;

  }

});

crisisApp.controller('groupMessageController', function groupMessageController($scope, myHttpService, $timeout, $compile, $sce, $q, $filter, $dialogConfirm, FileUploader, dialogs) {
  $scope.theMessage = {
    message: ''
  };
  $scope.files = [];
  $scope.messages = [];
  $scope.myInfo = null;
  $scope.false = true;
  $scope.isTyping = null;
  $scope.isFreePlan = isUserPlanFree();

  $scope.getMyInfo = function (refocus = true) {
    myHttpService.get('logged_info')
      .then(function (res) {
        $scope.myInfo = res.data; //.message;
        console.log($scope.myInfo, 'logged_info');
      });
  }
  $scope.getMyInfo();

  $scope.postMethod = function (data, db) {
    return myHttpService.post(data, db);
  }
  $scope.change = function (contents) {
    console.log('contents are changed:', contents, $scope.editable);
    myHttpService.get('group_messages_istyping')
      .then(function (res) {
        console.log(res, 'group_messages_istyping');

      });

  };


  $scope.getMessages = function (refocus = true) {
    myHttpService.get('get_messages')
      .then(function (res) {
        $scope.messages = res.data; //.message;
        console.log($scope.messages, 'get_messages');

        $timeout(function () {
          $(".chat_area").animate({
            scrollTop: $('.chat_area').prop("scrollHeight")
          }, 700);
        }, 200);

      });
  }
  $scope.getMessages();


  $scope.getIsTyping = function (contents) {
    console.log('getIsTyping:');
    myHttpService.get('get_istyping')
      .then(function (res) {
        console.log(res.data, 'get_istyping');
        $scope.isTyping = (res.data.user) ? res.data.user : null;

        if (res.data.unread_message_count > 0) {
          $scope.isTyping = null;
          $scope.getMessages();
        }

      });
  };

  //start checking for typing
  setInterval(function () {
    $scope.getIsTyping();
  }, 5000);


  $scope.sendMessage = function () {
    $scope.loading = true;
    var m = $scope.theMessage;
    console.log($scope.theMessage, m, '$scope.theMessage');

    myHttpService.post(m, 'save_gmessage')
      .then(function (res) {
        console.log(res);

        if ($scope.files.length) {
          var promises = [];

          var save_f = function (cl, index) {
            cl.message_id = res.data.id;
            promises.push($scope.postMethod(cl, 'save_gm_files'));
          }

          $scope.files.forEach(function (cl, index, array) {
            save_f(cl, index);
          });


          $q.all(promises).then(function (results) {
            results.forEach(function (currentValue, index, array) {
              console.log(currentValue.data, index, 'promises');

              if ($scope.files.length == (index + 1)) {

                var d = res.data;
                d.files = $scope.files;

                $scope.messages.push(d);

                $scope.theMessage.message = '';
                $scope.files = [];
                $scope.loading = false;


                $(".chat_area").animate({
                  scrollTop: $('.chat_area').prop("scrollHeight")
                }, 700);
              }

            });
          });
        } else {
          var d = res.data;
          d.files = [];

          $scope.messages.push(d);

          $scope.theMessage.message = '';
          $scope.files = [];
          $scope.loading = false;
          $(".chat_area").animate({
            scrollTop: $('.chat_area').prop("scrollHeight")
          }, 700);

        }




      });

  }


  $scope.deleteFile = function (i) {
    $dialogConfirm("Confirm delete file?").then(function () {
      $scope.files.splice(i, 1);
    }, function () {
      console.log('nope');
    });
  }


  $scope.uploadShare = function () {
    if($scope.isFreePlan){
      bootbox.alert('To access this feature, please upgrade your plan.');
    } else {
      $('#docShare').click();
    }
  }

  var uploader = $scope.uploader = new FileUploader({
    url: base_url + 'cc/standbytasks/upload_gmessage_file',
    removeAfterUpload: true,
    autoUpload: true
  });

  // FILTERS

  // a sync filter
  uploader.filters.push({
    name: 'syncFilter',
    fn: function (item /*{File|FileLikeObject}*/ , options) {
      console.log('syncFilter');
      return this.queue.length < 10;
    },
    'fn': function (item) {
      if (item.size > 16485760) {
        bootbox.alert('<h4>Opps! File size limit is 16mb.</h4>');
      }
      return item.size <= 16485760; // 16 MiB to bytes
    }
  });

  // an async filter
  uploader.filters.push({
    name: 'asyncFilter',
    fn: function (item /*{File|FileLikeObject}*/ , options, deferred) {
      console.log('asyncFilter');
      setTimeout(deferred.resolve, 1e3);
    }
  });

  // CALLBACKS

  uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/ , filter, options) {
    console.info('onWhenAddingFileFailed', item, filter, options);
  };
  uploader.onAfterAddingFile = function (fileItem) {
    console.info('onAfterAddingFile', fileItem);
  };
  uploader.onAfterAddingAll = function (addedFileItems) {
    console.info('onAfterAddingAll', addedFileItems);
  };
  uploader.onBeforeUploadItem = function (item) {
    console.info('onBeforeUploadItem', item);
  };
  uploader.onProgressItem = function (fileItem, progress) {
    console.info('onProgressItem', fileItem, progress);
  };
  uploader.onProgressAll = function (progress) {
    console.info('onProgressAll', progress);
  };
  uploader.onSuccessItem = function (fileItem, response, status, headers) {
    console.info('onSuccessItem', fileItem, response, status, headers);
    if (response.file) {
      $scope.files.push(response.file);
    }
  };
  uploader.onErrorItem = function (fileItem, response, status, headers) {
    console.info('onErrorItem', fileItem, response, status, headers);
  };
  uploader.onCancelItem = function (fileItem, response, status, headers) {
    console.info('onCancelItem', fileItem, response, status, headers);
  };
  uploader.onCompleteItem = function (fileItem, response, status, headers) {
    console.info('onCompleteItem', fileItem, response, status, headers);
  };
  uploader.onCompleteAll = function () {
    console.info('onCompleteAll');
  };

  console.info('uploader', uploader);




  $scope.htmlSafe = function (data) {
    return $sce.trustAsHtml(data);
  }

  $scope.optionsGroupMessage = {
    height: 50,
    disableDragAndDrop: true,
    disableResizeEditor: true,
    popover: {
      air: [
        // ['color', ['color']],
        // ['font', ['bold', 'underline', 'clear']]
      ]
    }

  };
});